//
//  SIXDAVPlayer.h
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 12/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "ViewController.h"
//#import "SIXDAVController1.h"

@interface SIXDAVPlayer : ViewController<AVAudioRecorderDelegate, AVAudioPlayerDelegate,NSURLSessionDelegate>
//@interface SIXDAVPlayer : SIXDAVController<AVAudioRecorderDelegate, AVAudioPlayerDelegate>


@property(strong,nonatomic) AVAudioPlayer *Player;
@property(strong,nonatomic) AVAudioRecorder *Recorder;
@property(strong,nonatomic) AVPlayerItem *playerItem;
@property (strong,nonatomic) ViewController *SIXVCCallBack;
//@property (strong,nonatomic) SIXDAVController1 *SIXVCCallBack;
- (void) initialiseRecorder : (NSURL *)FileUrl;
- (void)recordIO : (int)kDuration;
- (void)recordIO;
- (void)stopRecordIO;
- (void)stopRecordComplete;
- (void)playIO;
- (void)previewRecord;
- (void)pauseRecord;
- (void)deleteRecord;
- (void) stopPlay;
- (void) initialisePlayer : (NSString *)resourcePath;

@property int recordEncoding;


@property(strong,nonatomic) NSTimer *Timer;

//#endif /* SIXDAVPlayer_h */
@end
