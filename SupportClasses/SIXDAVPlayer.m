//
//  SixDAVPlayer.m
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 12/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import "SIXDAVPlayer.h"
#import "AppDelegate.h"



#define ENC_AAC     1
#define ENC_ALAC    2
#define ENC_IMA4    3
#define ENC_ILBC    4
#define ENC_ULAW    5
#define ENC_PCM     6

@interface SIXDAVPlayer()

@end

@implementation SIXDAVPlayer


- (void) initialiseRecorder : (NSURL *)outputFileURL {
    

    _recordEncoding = ENC_AAC;
    [self setRecorderProperty];
    
   
}


- (void) initialisePlayer : (NSString *)resourcePath
{
    
    //NSString* resourcePath = [_ToneDetails objectForKey:@"toneUrl"];
    
    NSLog(@"initialisePlayer>>>>");
    NSString *songurl = [resourcePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSLog(@"song URL = %@",songurl);
    //for testing
    //songurl = @"http://10.84.0.151:9099/CRBT_TELENOR_UP/servlet/MobileStreamMediaServlet?fileId=BkJnfdO1ZMvzMWWsBmcnQcUvUjzmYMgyPHphaOha6GzRoB0whFdUvhvCdTVY2pTeZm1/JOztdnCPFRNB4MBGoUTYNoxdLg9TusWi/fUGD7wmdoUg9PUZ6Gdb8+TQpsK5";
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
   // NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString:songurl]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:songurl]];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *URLsession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [URLsession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(error)
                                              {
                                                  [_SIXVCCallBack onPlayFailure];
                                              }
                                              else
                                              {
                                              _Player = [[AVAudioPlayer alloc] initWithData:data error:&error];
                                              _Player.numberOfLoops = 0;
                                              _Player.delegate = self;
                                              
                                              if ([_Player prepareToPlay] == YES)
                                              {
                                                  NSLog(@"Player is  set");
                                                  [_SIXVCCallBack onPlayStarted];
                                                  [self playIO];
                                              }
                                              else
                                              {
                                                  int errorCode = CFSwapInt32HostToBig ((uint32_t)[error code]);
                                                  NSLog(@"Error: %@ [%4.4s])" , [error localizedDescription], (char*)&errorCode);
                                                  [_SIXVCCallBack onPlayFailure];
                                              }
                                              }
                                            }];
    [postDataTask resume];
    
    
}


-(void) setRecorderProperty
{
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] initWithCapacity:10];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    if(_recordEncoding == ENC_PCM)
    {
        [recordSettings setObject:[NSNumber numberWithInt: kAudioFormatLinearPCM] forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSettings setObject:[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    }
    else
    {
        NSNumber *formatObject;
        
        switch (_recordEncoding) {
            case (ENC_AAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];
                break;
            case (ENC_ALAC):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleLossless];
                break;
            case (ENC_IMA4):
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
                break;
            case (ENC_ILBC):
                formatObject = [NSNumber numberWithInt: kAudioFormatiLBC];
                break;
            case (ENC_ULAW):
                formatObject = [NSNumber numberWithInt: kAudioFormatULaw];
                break;
            default:
                formatObject = [NSNumber numberWithInt: kAudioFormatAppleIMA4];
        }
        
        [recordSettings setObject:formatObject forKey: AVFormatIDKey];
        [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey: AVSampleRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
       // [recordSettings setObject:[NSNumber numberWithInt:12800] forKey:AVEncoderBitRateKey];
        [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSettings setObject:[NSNumber numberWithInt: AVAudioQualityLow] forKey: AVEncoderAudioQualityKey];
    }
    
    
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSError *error = nil;
    _Recorder = [[AVAudioRecorder alloc] initWithURL:appDelegate.karaokeFileUrl settings:recordSettings error:&error];
    _Recorder.delegate = self;
    _Recorder.meteringEnabled = YES;
    
    if ([_Recorder prepareToRecord] == YES){
        NSLog(@"Recorder set");
        //[_Recorder record];
    }else {
        int errorCode = CFSwapInt32HostToBig ((uint32_t)[error code]);
        NSLog(@"Error: %@ [%4.4s])" , [error localizedDescription], (char*)&errorCode);
        
    }
    
    
    //_Recorder = [[AVAudioRecorder alloc] init
    //_Recorder.delegate = self;
    //_Recorder.meteringEnabled = YES;
    //[_Recorder prepareToRecord];

    
    
}

-(void) recordIO : (int) kDuration {
    
    NSLog(@"recordIO >>>");
    [self startTimmer];
    if (!_Recorder.recording)
    {
        //_Status = RECORD;
        NSLog(@"Recording");
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [_Recorder recordForDuration:kDuration];
       // [_RecordButton setTitle:@"PAUSE" forState:UIControlStateNormal];
        
    }
    else
    {
        NSLog(@"Stop");
       // [self]
        [_Recorder stop];
    }
    
}


-(void) recordIO {
    
    NSLog(@"recordIO >>>");
    [self startTimmer];
    if (!_Recorder.recording)
    {
        //_Status = RECORD;
        NSLog(@"Recording");
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [_Recorder record];
        // [_RecordButton setTitle:@"PAUSE" forState:UIControlStateNormal];
        
    }
    else
    {
        NSLog(@"Stop");
        // [self]
        [_Recorder stop];
    }
    
}


- (void) stopRecordIO
{
    NSLog(@"stopRecordIO>>>");
        if(_Recorder.recording)
        {
          NSLog(@"stoppingRecordIO");
            [_Recorder stop];
        }
    //[_Recorder stop];
}

-(void) stopRecordComplete
{
    NSLog(@"stopRecordComplete");
    [_Recorder stop];
}

- (void) stopPlay
{
    if(_Player.isPlaying)
    {
        NSLog(@"Stop playIO");
        [_Player stop];
        
    }
}

- (void)deleteRecord
{
    if(_Recorder.recording)
    {
        NSLog(@"stopRecordIO");
        [_Recorder deleteRecording];
    }
}

- (void) pauseRecord
{
    if(_Recorder.recording)
    {
        NSLog(@"pauseRecord");
        [_Recorder pause];
    }
}

- (void) previewRecord
{
    NSLog(@"Preview Record");
    [_Recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    if (!_Recorder
        .recording){
        _Player = [[AVAudioPlayer alloc] initWithContentsOfURL:_Recorder.url error:nil];
        [_Player setDelegate:self];
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
    [_Player play];
    }
    
}

-(void) playIO {
    NSLog(@"playIO");
    
    [self startTimmer];
    if(!_Player.playing)
    {
        
        NSLog(@"_Player = %@",_Player);
        [_Player play];
      
    }
    else
    {
        NSLog(@"Stop playIO");
        [_Player stop];
      
    }
}


-(void)startTimmer
{
    _Timer =  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats:NO];
    
}

-(void) updateCountdown
{
    //if([_Recorder isRecording])
    if(_Recorder.currentTime == 0.00)
    {
        NSLog(@"Invalidate tIMER");
        [_Timer invalidate];
        
    }
    NSLog(@"RecordCurrentTime = %f",_Recorder.currentTime);
    /*
    {
        
        int minutes = floor(_Recorder.currentTime/60);
        int seconds = _Recorder.currentTime - (minutes * 60);
        
        NSString *time = [[NSString alloc]
                          initWithFormat:@"%02d:%02d",
                          minutes, seconds];
       // _TimeLabel.text = time;
    }*/
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    //[_RecordButton setTitle:@"PREVIEW" forState:UIControlStateNormal];
    //[_StopConfirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
   // [_StopConfirmButton setEnabled:YES];
    NSLog(@"audioRecorderDidFinishRecording >>>");
    [_SIXVCCallBack onRecordingStopped];
    
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
   // [_RecordButton setTitle:@"PREVIEW" forState:UIControlStateNormal];
    //[_StopConfirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
    //[_StopConfirmButton setEnabled:YES];
    
    [_SIXVCCallBack onPlayStopped];
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}


@end
