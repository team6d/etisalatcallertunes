//
//  NotificationViewController.swift
//  NotificationContent
//
//  Created by SKY on 14/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import AVKit

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    @IBOutlet var addButton: UIButton?
    @IBOutlet var cancelButton: UIButton?
    
    var player: AVAudioPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        cornerRadius()
        
    }
    
    func cornerRadius() {
        addButton?.layer.cornerRadius = 5.0
        cancelButton?.layer.cornerRadius = 5.0
        
    }
    
    @IBAction func cancelButtonAction() {
        TestDelete.cancelFunction()
        if #available(iOS 12.0, *) {
            self.extensionContext?.dismissNotificationContentExtension()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func addButtonAction() {
        TestDelete.addFunction()
        self.label?.text = "Downloading....."
        playUrl()
    }
    
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        
    }
    
    func playUrl() {
        
                let urlStr = "https://callertunes.etisalat.ae/servlet/MobileStreamMediaServlet?fileId=JP0pty9iYIO9U%204tDVjc3o97SAWzJj1fTO9eb7I7ccCSvtxOpEhxZXibt1J7a5vYAedONX44ZB7fRp5KT5lW1Q=="
                guard let url = URL(string: urlStr) else { return }
                //Player.shared.streamTuneFromURL(url: url, autoPlay: true, complition: nil)
        //let urlstring = "http://radio.spainmedia.es/wp-content/uploads/2015/12/tailtoddle_lo4.mp3"
        let url1 = URL(string: urlStr)
        print("the url = \(url1!)")
        downloadFileFromURL(url: url1!)
    }
    
    func downloadFileFromURL(url:URL){

        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            self?.play(url: URL! )
            
        })

        downloadTask.resume()

    }
    
    func play(url:URL) {
        print("playing \(url)")

        do {
            self.player = try AVAudioPlayer(contentsOf: url)
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
            DispatchQueue.main.async {
                self.label?.text = "playing"
            }
            
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }

    }
    

}
