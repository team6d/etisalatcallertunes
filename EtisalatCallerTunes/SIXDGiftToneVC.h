//
//  SIXDGiftToneVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 10/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
@import ContactsUI;

@interface SIXDGiftToneVC : ViewController<UITextFieldDelegate,UIPickerViewDelegate, CNContactPickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *SubView;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;

@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;
- (IBAction)onClickedCloseButton:(UIButton *)sender;
- (IBAction)onSelectedContactButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *MobileNumberView;
@property (weak, nonatomic) IBOutlet UITextField *MobileNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *MobileNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;

@property(strong,nonatomic)NSArray *PhoneNumbers;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property(nonatomic) BOOL IsGetTonePriceCalled;

@property(nonatomic) BOOL IsSourceGiftPlaylist;
@property(nonatomic) BOOL IsSourceSalati;

@property(nonatomic) BOOL ShouldDismissView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PriceLabelHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PopUpViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtwConfirmButtonAndPriceLabel;

@property(strong,nonatomic) NSString* PackName;

@end
