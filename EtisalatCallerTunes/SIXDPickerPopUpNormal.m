//
//  SIXDPickerPopUpNormal.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 26/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDPickerPopUpNormal.h"
#import "AppDelegate.h"
//#import "SIXDStylist.h"
#import "SIXDHeader.h"

@interface SIXDPickerPopUpNormal ()

@end

@implementation SIXDPickerPopUpNormal

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    
     [_PickerView setValue:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:@"textColor"];
    [_PickerView reloadAllComponents];
    
   // _TimeVC.SelectTimeTypeFromPopUp = -1;
    
   // _TimeVC.SelectTimeTypeFromPopUp = -1;
    
    [_GroupVC.NumberTextField setInputView:nil];
    
    [self localisation];
    
    if([_InputList count] > 0)
    {
        _selectedTimeType = [_InputList objectAtIndex:0];
        if(_GroupVC.GroupIdList.count > 0 && _SOURCE_TYPE == FOR_PEOPLE)
        {
            [_GroupVC.MainSettingsVC.SettingsDataDict setObject:[_GroupVC.GroupIdList objectAtIndex:0] forKey:@"groupId"];
        }
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    //if picker = Time type
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    //if picker = Time type
    return _InputList.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_InputList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    NSLog(@"row = %ld", (long)row);
    _TextField.text = [_InputList objectAtIndex:row];
    _selectedTimeType = [_InputList objectAtIndex:row];

    NSLog(@"grpIddList = %@",_GroupVC.GroupIdList);
    NSString *grpId = [_GroupVC.GroupIdList objectAtIndex:row];
    NSLog(@"grpId = %@", grpId);
    if([grpId isEqual:NULL ])
    {
        NSLog(@"No grp id present");
    }
    else{
        [_GroupVC.MainSettingsVC.SettingsDataDict setObject:grpId forKey:@"groupId"];
        NSLog(@"grpId = %@",grpId);
    }
    _selectedRow = row;
    NSLog(@"thePickerView Value = %@>>", _selectedTimeType);
    /*
    if(row == 0)
    {
        _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_FULL_DAY;
    }
    
    else if(row == 1)
    {
        _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_SPECIFIC_TIME;
    }
    
    else if(row == 2)
    {
        _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_SPECIFIC_DATE;
    }
     */
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_SOURCE_TYPE==FOR_TIME)
        _InputList = @[NSLocalizedStringFromTableInBundle(@"Full Day",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"Specific Time",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"Specific Date",nil,appDelegate.LocalBundel,nil)];
    else
    {
        _InputList = [_GroupVC.GroupList mutableCopy];
        NSLog(@"_InputList = %@",_InputList);
    }
    
    [_SaveButton setTitle:NSLocalizedStringFromTableInBundle(@"SAVE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
}

- (IBAction)onClickedSaveButton:(id)sender
{
    if(_SOURCE_TYPE == FOR_TIME)
    {
        _TimeVC.TypeBottomLabel.text=_selectedTimeType;
        _TimeVC.Source = SRC_NORMAL_PICKER;
        
        if(_selectedRow == 0)
        {
            _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_FULL_DAY;
        }
        
        else if(_selectedRow == 1)
        {
            _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_SPECIFIC_TIME;
        }
        
        else if(_selectedRow == 2)
        {
            _TimeVC.SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_SPECIFIC_DATE;
        }
        
        
        [_TimeVC viewWillAppear:false];
        
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
        
       
    }
    else
    {
        _GroupVC.NumberTextField.text=_selectedTimeType;
        [_GroupVC viewWillAppear:false];
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
}

- (IBAction)onClickedCloseButton:(id)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

@end
