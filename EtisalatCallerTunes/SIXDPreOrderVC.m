//
//  SIXDPreOrderVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDPreOrderVC.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "UIImageView+WebCache.h"
#import "SIXDPlayerVC.h"
#import "SIXDBuyToneVC.h"
#import "SIXDStylist.h"

@interface SIXDPreOrderVC ()

@end

@implementation SIXDPreOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_PreOrder"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_HeaderImage sd_setImageWithURL:appDelegate.CategoryImagePath placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _BannerDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary *Dict = [_BannerDetails objectAtIndex:indexPath.row];
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    /*
    cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"artistName"]];
    cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
     */
    
    NSString *String1 = [SIXDGlobalViewController HTMLConversion:[Dict objectForKey:@"artistName"]];
    cell.TitleLabel.text = [String1 capitalizedString];
    NSString *String2 = [SIXDGlobalViewController HTMLConversion:[Dict objectForKey:@"toneName"]];
    cell.SubTitleLabel.text = [String2 capitalizedString];
    
    UIImage* image = [[UIImage imageNamed:@"justforyoubuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.FirstButton setImage:image forState:UIControlStateNormal];
    [cell.FirstButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    cell.FirstButton.tag = indexPath.row;
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    
    int x_Position;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        x_Position=0;
    }
    else
    {
        x_Position=50-1;
    }
    
    UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    
    [cell.FirstButton addSubview:leftBorder1];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Commented because not sure whether PRE-BOOKING has toneUrl or not.
    /*
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_BannerDetails;
    ViewController.CurrentToneIndex = indexPath.row;
    [self.navigationController pushViewController:ViewController animated:YES];
     */
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}


- (IBAction)onClickedBuyButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails= [_BannerDetails objectAtIndex:sender.tag];
    ViewController.IsSourcePreBooking = TRUE;
    [self presentViewController:ViewController animated:YES completion:nil];
}
@end
