//
//  SIXDCopyPlaylistVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 13/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCopyPlaylistVC.h"
#import "SIXDGlobalViewController.h"
#import "AppDelegate.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "SIXDPlaylistBuyVC.h"
#import "SIXDBuyToneVC.h"

@interface SIXDCopyPlaylistVC ()

@end

@implementation SIXDCopyPlaylistVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger NumberofItems = _Playlist_listToneApk.count/3;
    
    if(_Playlist_listToneApk.count%3 != 0)
    {
        NumberofItems++;
    }
    NSLog(@"NumberofItems = %ld",(long)NumberofItems);
    return NumberofItems;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.CollectionView.tag = indexPath.row;
    
    cell.FirstRow = [NSMutableArray new];
    
    NSInteger index = indexPath.row*3;
    
    for (int i=0; i<3; i++)
    {
        if(index < _Playlist_listToneApk.count)
        {
            [cell.FirstRow addObject:[_Playlist_listToneApk objectAtIndex:index]];
            index++;
        }
    }
    
    cell.Source = MUSICBOX;
    
    NSLog(@"Section index = %ld",(long)indexPath.section);
    // cell.FirstRow=_categoryList;
    [cell.CollectionView reloadData];
    return cell;
    
}

- (IBAction)onClickedAccessoryButton:(UIButton *)sender {
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlaylistBuyVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlaylistBuyVC"];
    ViewController.PlaylistDetails = [_Playlist_listToneApk objectAtIndex:sender.tag];
    [self.navigationController pushViewController:ViewController animated:YES];
}

- (IBAction)onClickedBuyButton:(UIButton *)sender {
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails= [_Playlist_listToneApk objectAtIndex:sender.tag];
    ViewController.IsSourceBuyPlaylist = TRUE;
    [self presentViewController:ViewController animated:YES completion:nil];
}
@end
