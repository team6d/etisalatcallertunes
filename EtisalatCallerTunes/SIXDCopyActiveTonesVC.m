//
//  SIXDCopyActiveTonesVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 13/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCopyActiveTonesVC.h"
#import "AppDelegate.h"
#import "SIXDCopyPlaylistVC.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDBuyToneVC.h"
#import "SIXDPlayerVC.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"
#import "UIImageView+WebCache.h"

@interface SIXDCopyActiveTonesVC ()

@end

@implementation SIXDCopyActiveTonesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _Tunes_listToneApk.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *toneDetails = [_Tunes_listToneApk objectAtIndex:indexPath.row];
    
    //***set like count *********
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([toneDetails objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[toneDetails objectForKey:@"toneId"] :[[toneDetails objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
    }
    else
        cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count enda *********
    
    cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[toneDetails objectForKey:@"artistName"]];
    cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[toneDetails objectForKey:@"toneName"]];
    
    NSString *imageurl = [[toneDetails objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSURL *ImageURL = [NSURL URLWithString:imageurl];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    cell.TitleLabel.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    cell.SubTitleLabel.textColor = UIColorFromRGB(CLR_BUY);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    int x_Position;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        x_Position=0;
    }
    else
    {
        x_Position=50-1;
    }
    
    UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    
    [cell.LikeButton addSubview:leftBorder1];
    
    UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
    
    [cell.BuyButton addSubview:leftBorder2];
    
    UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    [cell.AccessoryButton addSubview:leftBorder3];
    
    UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    image = [[UIImage imageNamed:@"justforyoubuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.BuyButton setImage:image forState:UIControlStateNormal];
    [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    //LIKE SETTINGS
    NSLog(@"CustomTV::Calling likeSettingsSession >>>");
    [SIXDStylist loadLikeUnlikeSetings:cell :[toneDetails objectForKey:@"toneId"]];
    
    cell.BuyButton.tag = indexPath.row;
    cell.LikeButton.tag = indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

- (IBAction)onClickedMoreButton:(UIButton *)sender {
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_Tunes_listToneApk;
    ViewController.CurrentToneIndex = sender.tag;
    [self.navigationController pushViewController:ViewController animated:YES];
}

- (IBAction)onClickedBuyButton:(UIButton *)sender {
    
    NSMutableDictionary *toneDetails = [_Tunes_listToneApk objectAtIndex:sender.tag];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    
    ViewController.ToneDetails= toneDetails;
    
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    
    NSLog(@"Wishlist::Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
   
    NSMutableDictionary *TuneDetails = [_Tunes_listToneApk objectAtIndex:sender.tag];
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
    /*
    NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
     */
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_Tunes_listToneApk;
    ViewController.CurrentToneIndex = indexPath.row;
    [self.navigationController pushViewController:ViewController animated:YES];
}

@end
