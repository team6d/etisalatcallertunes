//
//  SIXDListTonesVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
@class SIXDSubCategories;

@interface SIXDListTonesVC : ViewController

@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(nonatomic,strong)NSMutableArray *TuneList;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property(nonatomic) int Source;
@property(nonatomic) NSString *SubCategoryTitle;
@property(strong,nonatomic) SIXDSubCategories *SubCategoriesVC;

-(void)loadTones;
- (IBAction)onClickedBuyButton:(UIButton *)sender;

@end
