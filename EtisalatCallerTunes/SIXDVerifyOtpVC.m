//
//  SIXDVerifyOtpVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDVerifyOtpVC.h"
#import "SIXDStylist.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDForgotPasswordVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDForgotPasswordVC.h"

@interface SIXDVerifyOtpVC ()

@end

@implementation SIXDVerifyOtpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Forget_Password_Get_OTP"];
    
    [SIXDStylist setBackGroundImage:self.view];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [_Number setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    _Number.textColor = UIColorFromRGB(CLR_PLACEHOLDER);
    _Number.text = appDelegate.MobileNumber;
    _Number.userInteractionEnabled=FALSE;
    _Number.enablesReturnKeyAutomatically = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _Number.delegate = self;
    _Number.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    _Number.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    _NumberView.layer.borderWidth = 1.0f;
    _NumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
    _NumberView.alpha=0.95;
    
    _TopSpaceToTextField.constant=19;
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"RESET MY PASSWORD",nil,appDelegate.LocalBundel,nil);
    _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
    
    
    [_GetOtpButton setTitle:NSLocalizedStringFromTableInBundle(@"GET VERIFICATION CODE (OTP)",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _ActivityIndicator.hidden=YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedGetOtpButton:(UIButton *)sender
{
    NSLog(@"API_SendOtp call>>>");
    [self API_SendOtp];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_Number resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return NO;
}

-(void)API_SendOtp
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@",appDelegate.MobileNumber,Rno,appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/send-otp-wp",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_SendOtp onSuccessResponseRecived = %@",Response);
    Response = [Response objectForKey:@"responseMap"];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDForgotPasswordVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDForgotPasswordVC"];
    ViewController.secToc = [Response objectForKey:@"secToc"];
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *message =[Response objectForKey:@"message"];
    [SIXDGlobalViewController setAlert:message :self];
    NSLog(@"API_SendOtp onFailureResponseRecived = %@",Response);
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) :self];
    
    NSLog(@"API_SendOtp onFailedToConnectToServer");
}
- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
