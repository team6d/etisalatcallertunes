//
//  AppDelegate.h
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 16/11/17.
//.  Copyright © 2017 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIXDHomePageVC.h"
#import "SIXDMainTabController.h"
@import UserNotifications;
@import Firebase;
@import Messages;


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *BaseMiddlewareURL;
@property (strong, nonatomic) NSString *AccessToken;
@property (strong, nonatomic) NSString *RefreshToken;
@property (strong, nonatomic) NSString *DeviceId;
@property (strong, nonatomic) NSString *MobileNumber;
@property (strong, nonatomic) NSString *Language;

@property (strong, nonatomic) NSString *BaseImageURL;

@property(strong,nonatomic)NSArray *PromotionList;

@property(strong,nonatomic) NSMutableDictionary *AppSettings;

@property(nonatomic) NSBundle *LocalBundel;

@property(nonatomic) int APIFlag;
@property(nonatomic)  short OpenURL_SDK_Type;

@property (strong, nonatomic) SIXDHomePageVC *HomeObj;
@property (strong, nonatomic) SIXDMainTabController *MainTabController;

@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) NSString *PlaceHolderFont;


//For Subcategories - saving Category image and Name - as same data is used in Multiple pages
@property (weak, nonatomic) NSURL *CategoryImagePath;
@property (weak, nonatomic) NSString *CategoryName;
//to save category ID of subCategories and also used to differtentiate tune and playlist in case of deeplinking
@property (nonatomic) int CategoryId;

//To check if App os opened from Deeplink/Universal link
@property (nonatomic) BOOL IsAppOpenedFromURL;
//used to save toneID recieved from deeplinking/Universal link.
@property (strong, nonatomic) NSString *ToneID;

@property (strong,nonatomic) NSURL *karaokeFileUrl;

//Register and My profile categories
@property(strong,nonatomic) NSMutableArray *CategoryList;
//end

//Used to save selected playlist button index in wishlist Page(Playlist Tab)
@property (strong, nonatomic) NSMutableArray *SelectionButtonTagArray;

@property int DeviceSize;

@property (strong, nonatomic) NSString *Password;
@property(nonatomic,strong) NSString *FCMToken;
-(void)testingGiftPopupDelete;

@end
