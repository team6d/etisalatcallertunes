//
//  SIXDBlacklistVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
@import ContactsUI;

@interface SIXDBlacklistVC : ViewController<CNContactPickerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *InfoView;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
- (IBAction)onSelectedContactButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *BlacklistNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *Name;

@property(strong,nonatomic)NSArray *PhoneNumbers;

- (IBAction)onSelectedAddButton:(id)sender;

@property(strong,nonatomic) NSMutableArray *Blacklist;
@property (weak, nonatomic) IBOutlet UIView *AlertView;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *AddButton;

@property (weak, nonatomic) IBOutlet UIView *BlackListNumberView;
@property (weak, nonatomic) IBOutlet UIButton *BlackListNumberErrorIcon;
@property (weak, nonatomic) IBOutlet UIView *NameView;
@property (weak, nonatomic) IBOutlet UIButton *NameErrorIcon;

@property(nonatomic) BOOL IsInitialLoad;

@property(nonatomic) NSInteger ButtonTag;
@end
