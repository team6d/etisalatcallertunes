//
//  SIXDCopyActiveTonesVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 13/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDCopyActiveTonesVC : ViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(strong,nonatomic) NSMutableArray *Tunes_listToneApk;
@property (strong, nonatomic) NSString *MobileNumber;
@end
