//
//  SIXDValidations.h
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 16/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#ifndef SIXDValidations_h
#define SIXDValidations_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalInterfaceForValidations : NSObject

+ (BOOL)fn_isValidEmail:NSString;
+ (BOOL) fnG_ValidateNames:NSString;
+ (BOOL) fnG_ValidatePhoneNumber:NSString;
+ (BOOL) fnG_ValidatePassword:NSString;




@end

//#define fn_isValidEmail(Email) [ \
BOOL stricterFilter = NO;\
NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";\
NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";\
NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;\
NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];\
//return [emailTest  evaluateWithObject:Email];\
]




#endif /* SIXDValidations_h */
