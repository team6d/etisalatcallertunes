//
//  SIXDTopChartsVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDGenericTableView.h"

@interface SIXDTopChartsVC : ViewController
//@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;

@property (strong, nonatomic) IBOutlet UILabel *FirstLabel;
@property (strong, nonatomic) IBOutlet UILabel *SecondLabel;
@property (strong, nonatomic) IBOutlet UILabel *ThirdLabel;
@property (strong, nonatomic) IBOutlet UILabel *FourthLabel;


@property(strong,nonatomic) NSArray *CategoryIDs;

@property(nonatomic,strong)NSMutableArray *TopPicks;
@property(nonatomic,strong)NSMutableArray *BestOfMonth;
@property(nonatomic,strong)NSMutableArray *BestOfWeek;
@property(nonatomic,strong)NSMutableArray *MostLiked;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;


@property (weak, nonatomic) IBOutlet UILabel *TopChartsLabel;
@property(nonatomic,strong) SIXDGenericTableView *GenericTableViewVC;
-(void)loadTopCharts;
@end
