//
//  SIXDHttpRequestHandler.m
//  6dCRBTApp
//
//  Created by Six Dee Technologies on 12/28/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import "SIXDHttpRequestHandler.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "SIXDHeader.h"

@interface SIXDHttpRequestHandler ()

@end

@implementation SIXDHttpRequestHandler

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_HeaderDictionary = [NSMutableDictionary new];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendHttpPostRequest:(ViewController*)ParentView
{
    //_IsMultiPartPost = false;
    _MultiPartReqType = 0;
    _ParentView = ParentView;
    if (![self connected])
    {
        NSLog(@"Not connected");
        [_ParentView onInternetBroken];
        return;
    }
    else
    {
        NSLog(@"Connected");
    }
    
    NSData *postData;
    if(!_IsURLEncodingNotRequired)
    {
        NSString *Request = [_Request stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
         postData = [Request dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    }
    else
    {
        postData = [_Request dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[_Request length]];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_FinalURL]];
    
    NSLog(@"POST HTTP : Final URL = %@",_FinalURL);
    NSLog(@"Request = %@",_Request);

    [request setHTTPMethod:@"POST"];
   
    [request setValue:APP_ID forHTTPHeaderField:@"appId"];
  /*
    //temp 2202
    NSString *boundary = @"-------------------------------------9b9ff40e-8821-477f-a24d-d20c5a6587b2";
    NSLog(@"Setting content type as multipart/form-data ");
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    //temp ends
   */
    
    [request setValue:APP_VERSION forHTTPHeaderField:@"appVersion"];
    [request setValue:VERSION_CODE forHTTPHeaderField:@"versionCode"];
    [request setValue:appDelegate.AccessToken forHTTPHeaderField:@"accessToken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:appDelegate.DeviceId forHTTPHeaderField:@"deviceId"];
    [request setValue:OS forHTTPHeaderField:@"os"];
    [request setValue:appDelegate.Language forHTTPHeaderField:@"language"];
    
    
    request.timeoutInterval = 30;
    /*
    if(appDelegate.APIFlag==PASSWORD_VALIDATE)
    {
        NSLog(@"here APIflag= %d SecurityToken=%@",appDelegate.APIFlag,appDelegate.SecurityToken);
        [request setValue:appDelegate.SecurityToken forHTTPHeaderField:@"securityCounter"];
    }
    */
    
    
    
    if(_InvokedAPI == MULTI_PART_KARAOKE)
    {
        // _IsMultiPartPost = true;
        _MultiPartReqType = MULTI_PART_KARAOKE; // for screwed up logic
        
        //NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        //NSString *boundary = @"----1010101010";
        
        NSString *hdrr = [_HeaderDictionary objectForKey:@"clientTxnId"];
        
        //@"777ea203-562a-4948-aead-910421a15184";
        [request setValue:hdrr forHTTPHeaderField:@"clientTxnId"];
        
        hdrr = [_HeaderDictionary objectForKey:@"serviceId"];
        [request setValue:hdrr forHTTPHeaderField:@"serviceId"];
        
        hdrr = [_HeaderDictionary objectForKey:@"toneName"];
        hdrr  = [hdrr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [request setValue:hdrr forHTTPHeaderField:@"toneName"];
        
        hdrr = [_HeaderDictionary objectForKey:@"aPartyMsisdn"];
        [request setValue:hdrr forHTTPHeaderField:@"aPartyMsisdn"];
        
        hdrr = [_HeaderDictionary objectForKey:@"channelId"];
        [request setValue:hdrr forHTTPHeaderField:@"channelId"];
        
        hdrr = [_HeaderDictionary objectForKey:@"isCopy"];
        [request setValue:hdrr forHTTPHeaderField:@"isCopy"];
        
        hdrr = [_HeaderDictionary objectForKey:@"language"];
        [request setValue:hdrr forHTTPHeaderField:@"language"];
        
        hdrr = [_HeaderDictionary objectForKey:@"priority"];
        [request setValue:hdrr forHTTPHeaderField:@"priority"];
        
        hdrr = [_HeaderDictionary objectForKey:@"tonePath"];
        [request setValue:hdrr forHTTPHeaderField:@"tonePath"];
        
        hdrr = @"1";
        [request setValue:hdrr forHTTPHeaderField:@"versionCode"];
        
        NSString *boundary = @"9b9ff40e-8821-477f-a24d-d20c5a6587b2";
        NSLog(@"Setting content type as multipart/form-data ");
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept-Encoding"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)_RequestData.length] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:_RequestData];
        
        /**
         [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)_Request.length] forHTTPHeaderField:@"Content-Length"];
         [request setHTTPBody:postData];
         */
        
        //temp
        // [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        //[_RequestData appendData:[NSData dataWithData:postData]];
        //[request setHTTPBody:_RequestData];
        //[request setHTTPBody:postData];
    }
    
    else if(_InvokedAPI == MULTI_PART_EDIT_PROFILE)
    {
        NSLog(@"MULTI_PART_EDIT_PROFILE");
        //_IsMultiPartPost = true;
        _MultiPartReqType = MULTI_PART_EDIT_PROFILE; // for screwed up logic
        //NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        //NSString *boundary = @"----1010101010";
        
        NSString *hdrr = [_HeaderDictionary objectForKey:@"clientTxnId"];
        
        //@"777ea203-562a-4948-aead-910421a15184";
        [request setValue:hdrr forHTTPHeaderField:@"clientTxnId"];
        
        hdrr = [_HeaderDictionary objectForKey:@"identifier"];
        [request setValue:hdrr forHTTPHeaderField:@"identifier"];
        
        hdrr = [_HeaderDictionary objectForKey:@"servType"];
        [request setValue:hdrr forHTTPHeaderField:@"servType"];
        
        hdrr = [_HeaderDictionary objectForKey:@"language"];
        [request setValue:hdrr forHTTPHeaderField:@"language"];
        
        [request setValue:appDelegate.MobileNumber forHTTPHeaderField:@"msisdn"];
        
        hdrr = [_HeaderDictionary objectForKey:@"servType"];
        [request setValue:hdrr forHTTPHeaderField:@"servType"];
        
        hdrr = [_HeaderDictionary objectForKey:@"previewImage"];
        [request setValue:hdrr forHTTPHeaderField:@"previewImage"];
        
        NSString *boundary = @"9b9ff40e-8821-477f-a24d-d20c5a6587b2";
        NSLog(@"Setting content type as multipart/form-data ");
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept-Encoding"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)_RequestData.length] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:_RequestData];
        
    }
    else
    {
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self decodeResponse:data :response :error];
                                              
                                          }];
    [postDataTask resume];

    
}




-(void)sendHttpGETRequest:(ViewController*)ParentView :(NSString*)searchKey
{
   
    _ParentView = ParentView;
    if (![self connected])
    {
        NSLog(@"Not connected");
        [_ParentView onInternetBroken];
        return;
    }
    else
    {
        NSLog(@"Connected");
    }
    
    
   // _FinalURL = [_FinalURL stringByReplacingOccurrencesOfString:@"language=English" withString:@"language=1"];
    //_FinalURL = [_FinalURL stringByReplacingOccurrencesOfString:@"language=Arabic" withString:@"language=2"];
    
   // _ParentView = ParentView;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_FinalURL]];
    
    NSLog(@"GET HTTP : Final URL = %@ ",_FinalURL);
    
    
    if(searchKey != nil)
    {
        if(appDelegate.APIFlag==CDP_SEARCH_ARTIST)
        {
            
            _SearchKey = searchKey;
            searchKey = [searchKey stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            [request setValue:searchKey forHTTPHeaderField:@"artistKey"];
           
        }
        else
        {
            
            //NSLog(@"SearchKey  before = %@",searchKey);
            _SearchKey = searchKey;
            searchKey = [searchKey stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            searchKey = [searchKey stringByReplacingOccurrencesOfString:@"%E2%80%8F" withString:@""];
            [request setValue:searchKey forHTTPHeaderField:@"searchKey"];
           // NSLog(@"SearchKey sfter encoding = %@",searchKey);
            
           
        }
            
    }
    else
    {
        _SearchKey = nil;
    }
   
    request.timeoutInterval = 30;
    
    [request setHTTPMethod:@"GET"];
    [request setValue:APP_ID forHTTPHeaderField:@"appId"];
    [request setValue:APP_VERSION forHTTPHeaderField:@"appVersion"];
    [request setValue:VERSION_CODE forHTTPHeaderField:@"versionCode"];
    [request setValue:appDelegate.AccessToken forHTTPHeaderField:@"accessToken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:appDelegate.DeviceId forHTTPHeaderField:@"deviceId"];
    [request setValue:OS forHTTPHeaderField:@"os"];
    [request setValue:appDelegate.Language forHTTPHeaderField:@"language"];
    

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self decodeResponse:data :response :error];
                                        
                                          }];
    [postDataTask resume];
    
}




-(void)decodeResponse:(NSData*)data : (NSURLResponse *)httpresponse : (NSError *)error
{
    if(error != nil)
    {
        [_ParentView onFailedToConnectToServer];
        return;
    }
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) httpresponse;
    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    _HttpStatusCode = [httpResponse statusCode];
    
    NSError *err;
    NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
    
    if(_HttpStatusCode == 401)
    {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
        [[NSUserDefaults standardUserDefaults] synchronize];
/*
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController ;
        ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSignInVC"];
        [_ParentView presentViewController:ViewController animated:YES completion:nil];
 */
        
        NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
        for (UINavigationController *Temp in VCArray)
        {
            [Temp popToRootViewControllerAnimated:false];
        }
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
        [appDelegate.window.rootViewController.view removeFromSuperview];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        appDelegate.window.rootViewController = navController;
        [appDelegate.window makeKeyAndVisible];
        
        return;
        
    }
    
    if(_HttpStatusCode == 430)
    {
        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"UPDATE",nil,appDelegate.LocalBundel,nil)
                                      message:[response objectForKey:@"message"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* OkBtn = [UIAlertAction
                                    actionWithTitle:NSLocalizedStringFromTableInBundle(@"OK", nil,appDelegate.LocalBundel,nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        /*
                                        NSURL *iTunesLink = [NSURL URLWithString:[appDelegate.AppSettings objectForKey:@"UPDATE_URL"]];
                                        if(iTunesLink==NULL)
                                        {
                                            iTunesLink = [NSURL URLWithString:@"https://www.apple.com/in/ios/app-store/"];
                                        }
                                        NSLog(@"iTunesLink = %@",iTunesLink);
                                        [[UIApplication sharedApplication] openURL:iTunesLink options:@{} completionHandler:nil];
                                        
                                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        
                                        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
                                        [appDelegate.window.rootViewController.view removeFromSuperview];
                                        appDelegate.window.rootViewController = ViewController;
                                        [appDelegate.window makeKeyAndVisible];
                                         */
                                        
                                        NSMutableDictionary *responseMap = [response objectForKey:@"responseMap"];
                                        
                                        NSURL *iTunesLink = [NSURL URLWithString:[responseMap objectForKey:@"downloadUrl"]];
                                        if(iTunesLink==NULL)
                                        {
                                            iTunesLink = [NSURL URLWithString:@"https://www.apple.com/in/ios/app-store/"];
                                        }
                                        NSLog(@"iTunesLink = %@",iTunesLink);
                                        [[UIApplication sharedApplication] openURL:iTunesLink options:@{} completionHandler:nil];
                                        
                                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        
                                        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
                                        [appDelegate.window.rootViewController.view removeFromSuperview];
                                        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
                                        navController.navigationBarHidden = YES;
                                        appDelegate.window.rootViewController = navController;
                                        [appDelegate.window makeKeyAndVisible];
                                    }];
        
        UIAlertAction *CancelBtn = [UIAlertAction
                                       actionWithTitle:NSLocalizedStringFromTableInBundle(@"CANCEL", nil,appDelegate.LocalBundel,nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                           
                                           UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                           UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
                                           [appDelegate.window.rootViewController.view removeFromSuperview];
                                           UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
                                           navController.navigationBarHidden = YES;
                                           appDelegate.window.rootViewController = navController;
                                           [appDelegate.window makeKeyAndVisible];
                                       }];
        
        [alert addAction:OkBtn];
        [alert addAction:CancelBtn];
        [self.ParentView presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    if(_HttpStatusCode == 498)
    {
        switch (_HttpsReqType)
        {
            //case MULTIPLE_GET:
            case POST:
            case GET:
                [self regenerateToken];
                break;
                
            case REGENERATETOKEN:
            case MULTIPLE:
                [_ParentView onFailedToConnectToServer];
                break;
                
            default:
                [_ParentView onFailedToConnectToServer];
                break;
        }
        
        return;
    }
    
    
    
    if(err != nil)
    {
        NSLog(@"Received error %@",err);
        [_ParentView onFailedToConnectToServer];
        return;
    }
    
    switch(_InvokedAPI)
    {
            
        case RegenerateToken:
        {
            [self decodeRegenerateTokenResponse:response];
            break;
        }
        case LogOutAPI:
        {
            break;
        }
        default:
        {
            NSString *status = [response objectForKey:@"statusCode"];
            if([status isEqualToString:@"SC0000"])
            {
                NSLog(@"Recieved Success Response");
                if(_HttpsReqType == MULTIPLE )
                {
                    NSString *URL= [NSString stringWithFormat:@"%@",[httpResponse URL]];
                    [_ParentView onSuccessResponseRecived:response : URL];
                }
                else
                {
                    [_ParentView onSuccessResponseRecived:response];
                }
            }
            else
            {
                NSLog(@"Recieved Failure Response");
                [_ParentView onFailureResponseRecived:response];
            }
        }
            break;
            
    }
    

}

-(void)regenerateToken
{
    
    _InvokedAPI = RegenerateToken;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *post = [NSString stringWithFormat:@"refreshToken=%@",appDelegate.RefreshToken];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/regen-token",appDelegate.BaseMiddlewareURL];
    NSLog(@"regenerateToken URL = %@",URL);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URL]];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:APP_ID forHTTPHeaderField:@"appId"];
    [request setValue:APP_VERSION forHTTPHeaderField:@"appVersion"];
    [request setValue:VERSION_CODE forHTTPHeaderField:@"versionCode"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:appDelegate.AccessToken forHTTPHeaderField:@"accessToken"];
    [request setValue:appDelegate.DeviceId forHTTPHeaderField:@"deviceId"];
    [request setValue:OS forHTTPHeaderField:@"os"];
    [request setValue:appDelegate.Language forHTTPHeaderField:@"language"];

    [request setHTTPBody:postData];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [self decodeResponse:data :response :error];
                                              
                                          }];
    [postDataTask resume];
    
    
}


-(void)decodeRegenerateTokenResponse:(NSMutableDictionary*) response
{
    NSDictionary *arr = [response objectForKey:@"responseMap"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *status = [response objectForKey:@"statusCode"];
    if([status isEqualToString:@"SC0000"])
    {
        NSLog(@"regenerateToken responseMap = %@",arr);
        NSString *str1;
        
        str1 = [arr objectForKey:@"refreshToken"];
        if(str1.length > 0)
            appDelegate.RefreshToken = str1;
        
        str1 = [arr objectForKey:@"accessToken"];
        if(str1.length > 0)
            appDelegate.AccessToken = str1;
        
        str1 = [arr objectForKey:@"expiry"];
        double ExipryTime = [str1 doubleValue];
        ExipryTime = ExipryTime/1000;
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.AccessToken forKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.RefreshToken forKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
        ExipryTime = Time + ExipryTime - 40 ;
        
        NSString *Str = [NSString stringWithFormat:@"%f",ExipryTime];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] setObject:Str forKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        switch (_HttpsReqType)
        {
            case POST:
                _InvokedAPI = 0;
                if(_MultiPartReqType != 0)
                {
                    _InvokedAPI = _MultiPartReqType;
                }
                /*
                if(_IsMultiPartPost == true)
                {
                    _InvokedAPI = MULTI_PART_KARAOKE;
                }*/
                [self sendHttpPostRequest:_ParentView];
                
                break;
                
            case GET:
                _InvokedAPI = 0;
                [self sendHttpGETRequest:_ParentView :_SearchKey];
                break;
            
            case REGENERATETOKEN:
                [_ParentView onSuccessResponseRecived:nil :nil];
                break;
                
            default:
                [_ParentView onFailedToConnectToServer];
                break;
        }
    }
    else
    {
        [_ParentView onFailedToConnectToServer];
    }
}

-(void)LogOut
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    _InvokedAPI = LogOutAPI;
    
    NSString *post = [NSString stringWithFormat:@"refreshToken=%@",appDelegate.RefreshToken];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/logout",appDelegate.BaseMiddlewareURL];
    NSLog(@"logOut URL = %@",URL);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URL]];
    
       
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:APP_ID forHTTPHeaderField:@"appId"];
    [request setValue:APP_VERSION forHTTPHeaderField:@"appVersion"];
    [request setValue:VERSION_CODE forHTTPHeaderField:@"versionCode"];
    [request setValue:appDelegate.AccessToken forHTTPHeaderField:@"accessToken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:appDelegate.DeviceId forHTTPHeaderField:@"deviceId"];
    [request setValue:OS forHTTPHeaderField:@"os"];
    [request setValue:appDelegate.Language forHTTPHeaderField:@"language"];
    
    [request setHTTPBody:postData];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSLog(@"response = %@",response);
                                             // [self decodeResponse:data :response :error];
                                              
                                          }];
    [postDataTask resume];
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


//added for testing
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

-(void)API_ResendOtp
{    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     
     SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
     
     ReqHandler.HttpsReqType = GET;
     
     NSString *URL = [NSString stringWithFormat:@"%@/crbt/generate-otp",appDelegate.BaseMiddlewareURL];
     ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?msisdn=%@&language=%@",URL,appDelegate.MobileNumber,appDelegate.Language];
     
     [ReqHandler sendHttpGETRequest:self :nil];
    
}

/*
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        //if ([trustedHosts containsObject:challenge.protectionSpace.host])
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}
*/

-(BOOL)connectedToNetwork :(NSString *)path {
    NSURL* url = [[NSURL alloc] initWithString:path];
    NSData* data = [NSData dataWithContentsOfURL:url];
    if (data != nil)
        return YES;
    return NO;
}

-(BOOL)getHostStatus : (NSString *)path
{
    Reachability *reachability = [Reachability reachabilityWithHostName:path];
    /*
    if(!reachability)
    {
        NSLog(@"host = %@ not rechable", path);
        //return NO;
    }
    
    return YES;
     */
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NSLog(@"networkStatus = %ld", (long)networkStatus);
    return networkStatus != NotReachable;
}

-(void)getPreferedCategoryList
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

  
   //NSString *path = [NSString stringWithFormat:@"https://callertunes.etisalat.ae/settings.json"];
    //NSString * path = [NSString stringWithFormat:@"https://callertunes.etisalat.ae/CRBT_ETISALAT_GUI/en/app-settings.json"];
    NSString * path = [NSString stringWithFormat:@"https://callertunes.etisalat.ae/en/app-settings.json"];
    
    
   BOOL Valid =  [self connectedToNetwork:path];

    //BOOL Valid = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
    NSLog(@"Valid = %d", Valid);
    if(!Valid)
    {
        NSLog(@"path not found");
         path = [NSString stringWithFormat:@"https://callertunes.etisalat.ae/CRBT_ETISALAT_GUI/en/app-settings.json"];
    }
    
    NSLog(@"path = %@",path);
  
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(error)
                                              {
                                                  NSLog(@"error");
                                                 [_ParentView onFailureResponseRecived:nil];
                                              }
                                              else
                                              {
                                                  NSError *err = nil;
                                                  NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
            
                                                  
                                                  if(err != nil)
                                                  {
                                                      NSLog(@"Recievd error %@",err);
                                                  }
                                                  /*
                                                  response = [response objectForKey:@"responseMap"];
                                                  NSLog(@"Result = %@",response);
                                                  NSMutableDictionary *registrationCategories = [response objectForKey:@"registrationCategories"];
                                                  appDelegate.CategoryList = [[registrationCategories objectForKey:@"categories"]mutableCopy];
                                                   */
                                                  response = [response objectForKey:@"responseMap"];
                                                  NSLog(@"PreAppSettingsResult = %@",response);
                                                  NSMutableDictionary *registrationCategories = [response objectForKey:@"appSettings"];
                                                  appDelegate.CategoryList = [[registrationCategories objectForKey:@"categories"]mutableCopy];
                                                  [_ParentView onSuccessResponseRecived:nil];
                                              }
                                              
                                          }];
    [postDataTask resume];
    
}
//end

/*
 NSString *songurl = [resourcePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 NSString *songurl = [resourcePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
*/

@end
