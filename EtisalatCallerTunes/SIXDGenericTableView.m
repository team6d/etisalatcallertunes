//
//  SIXDGenericTableView.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/25/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

//#import "SIXDGenericTableView.h"
#import "SIXDNestedTVCell.h"
#import "CustomLabel.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDSubCategories.h"
#import "SIXDCustomButton.h"
#import "SIXDGenericPlayList.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"


@interface SIXDGenericTableView ()

@end

@implementation SIXDGenericTableView

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"_TuneList.count 1111 = %ld",_TuneList.count);
   // return _TuneList.count;
    if(_Source != TOPCHARTS)
    {
        return _TuneList.count;
    }
    else if(_Source == TOPCHARTS)
    {
        return 1;
    }
    else
        return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIXDNestedTVCell *cell = (SIXDNestedTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //copy only contents required to fill only row
    if(_Source == TOPCHARTS)
    {
        cell.TuneList = [_TuneList mutableCopy];
    }
    else
    {
        cell.TuneList = [[_TuneList objectAtIndex:indexPath.section] mutableCopy];
    }

    cell.Sorce = _Source;
    cell.CollectionViewCellSize =  CGSizeMake(self.view.frame.size.width-38,335);
    cell.CollectionView.frame = CGRectMake(0,0, self.view.frame.size.width, cell.CollectionViewCellSize.height);
    [cell.CollectionView layoutIfNeeded];
    
    [cell.CollectionView reloadData];
    
    if(_Source != TOPCHARTS && [appDelegate.Language isEqualToString:@"Arabic"])
    {
        
        unsigned long index = cell.TuneList.count/5;
        if(cell.TuneList.count%5 != 0)
        {
            index++;
        }
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:(index-1) inSection:0];
        if([cell.CollectionView numberOfItemsInSection:0] >= index)
        {
            [cell.CollectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
        [cell.CollectionView reloadData];
        
    }
    else
    {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:0];
        if([cell.CollectionView numberOfItemsInSection:0] > 0)
        {
            [cell.CollectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
    }
    return cell;
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //for testing
    
    SIXDHeading_12 *title;
    UIButton *SeeAll;
    NSDictionary *Temp = [_CategoryList objectAtIndex:section];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        title = [[SIXDHeading_12 alloc] initWithFrame:CGRectMake(30, 0,[UIScreen mainScreen].bounds.size.width-60,55)];
        SeeAll =[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-75,0,50,55)];
        title.textAlignment = NSTextAlignmentLeft;
        [SeeAll setImage:[UIImage imageNamed:@"seeallarrow"] forState:UIControlStateNormal];
         title.text = [[Temp objectForKey:@"categoryName"]uppercaseString];
    }
    else
    {
        title = [[SIXDHeading_12 alloc] initWithFrame:CGRectMake(85,0,[UIScreen mainScreen].bounds.size.width-110,55)];
        SeeAll =[[UIButton alloc] initWithFrame:CGRectMake(30,0,50,55)];
        title.textAlignment = NSTextAlignmentRight;
        [SeeAll setImage:[UIImage imageNamed:@"seeallarrow_Arabic"] forState:UIControlStateNormal];
        
        title.text = [SIXDGlobalViewController HTMLConversion:[Temp objectForKey:@"categoryName"]];
        
    }
    
    NSMutableAttributedString *titleString;
    NSMutableAttributedString *WhiteDot;
    NSMutableAttributedString *FinalString = [NSMutableAttributedString new];
    
    titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
    WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
    
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
    }
    else
    {
        titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
    }
    */
    [FinalString appendAttributedString:WhiteDot];
    [FinalString appendAttributedString:titleString];
    [FinalString appendAttributedString:WhiteDot];
    [SeeAll setAttributedTitle:FinalString forState:UIControlStateNormal];
    
    CGRect frame = tableView.frame;
    /*[SeeAll setTitle:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [SeeAll.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:8]];
    [SeeAll setTitleColor:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED) forState:UIControlStateNormal];
     */
    SeeAll.transform = CGAffineTransformScale(SeeAll.transform, -1.0f, 1.0f);
    SeeAll.titleLabel.transform = CGAffineTransformScale(SeeAll.titleLabel.transform, -1.0f, 1.0f);
    SeeAll.imageView.transform = CGAffineTransformScale(SeeAll.imageView.transform, -1.0f, 1.0f);
    SeeAll.tag = section;
    [SeeAll addTarget:self action:@selector(onSelectedSeeAllButton:) forControlEvents: UIControlEventTouchUpInside];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(30, 0, [UIScreen mainScreen].bounds.size.width, frame.size.height)];
    
    [headerView addSubview:title];
    [headerView addSubview:SeeAll];

    return headerView;
}

-(void)onSelectedSeeAllButton:(UIButton*)SeeAllButton
{
    
    NSLog(@"onSelectedSeeAllButton >>> SeeAllButton.tag = %ld",(long)SeeAllButton.tag);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDGenericPlayList *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGenericPlayList"];
    
   NSDictionary *Temp = [_CategoryList objectAtIndex:SeeAllButton.tag];
    
    ViewController.SubCategoryName = [Temp objectForKey:@"categoryName"];

    NSString *screenName = [NSString stringWithFormat:@"Subcategories_%@_%@",[SIXDGlobalViewController HTMLConversion:appDelegate.CategoryName],[SIXDGlobalViewController HTMLConversion:ViewController.SubCategoryName]];
    [SIXDStylist setGAScreenTracker:screenName];
    
    ViewController.IsSegmentTabRequired = true;
    
    appDelegate.CategoryId = [[Temp objectForKey:@"subCdpCategoryId"]intValue];
    NSLog(@"CDP Subcategory ID = %d",appDelegate.CategoryId);
    
    [_SubCategoriesVC.navigationController pushViewController:ViewController animated:YES];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //return 80;
    if(_Source != TOPCHARTS)
    {
        return 55;
    }
    else
    {
        return 0;
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)viewDidAppear:(BOOL)animated
{/*
    CGSize size ;
    size.width = self.TableView.frame.size.width;
    
    if(_Source != TOPCHARTS)
    {
         size.height = (((67*3)+55)*_TuneList.count);
    }
    else
    {
        size.height = (67*3);
    }
    
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.contentSize = _TableView.frame.size;
    
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
  */
}

-(void)loadSubCategories
{
    NSLog(@"loadSubCategories >>>");
    if(_Source != TOPCHARTS)
    {
        CGSize size ;
        size.width = self.TableView.frame.size.width;
        size.height = (((67*5)+55)*_TuneList.count);
       // size.height = (((67*5)+55)*_SubCategoriesVC.CategoryDetails.count);
        self.view.frame = CGRectMake(0, 0, size.width,size.height);
        _TableView.frame = CGRectMake(0, 0, size.width,size.height);
        _TableView.contentSize = _TableView.frame.size;
        [_TableView reloadData];
        self.preferredContentSize = size;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    else
    {
        [_TableView reloadData];
    }
}
@end
