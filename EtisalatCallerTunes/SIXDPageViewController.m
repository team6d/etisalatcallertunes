//
//  ViewController.m
//  6dCRBTApp
//
//  Created by Shahnawaz Lone on 8/1/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "SIXDPageViewController.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"

@interface SIXDPageViewController ()

@end

@implementation SIXDPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _CurrentIndex = 0;

    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    [_ActivityIndicator setHidden:true];
    
    //reducing the size of dots by 20%
    _PageControl.transform = CGAffineTransformMakeScale(0.7, 0.7);
    
    if(_Source == DEALS)
    {
        [self loadPromotionList];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SIXDPageContentVC*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound))
    {
        
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SIXDPageContentVC*) viewController).pageIndex;
    
    if (index == NSNotFound)
    {
        
        return nil;
    }
    
    index++;
    if (index == [_BannerDetailsList count])
    {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    
    if([pendingViewControllers count]>0)
    {
        NSUInteger index =[(SIXDPageContentVC*)[pendingViewControllers objectAtIndex:0] pageIndex];
        
        [_PageControl setCurrentPage:index];
        [self setPageControlDots];
        _CurrentIndex = index;
        
        
    }
}


- (SIXDPageContentVC *)viewControllerAtIndex:(NSUInteger)index
{
    if (([_BannerDetailsList count] == 0) || (index > [_BannerDetailsList count])) {
        
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    SIXDPageContentVC *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SIXDPageContentVC"];
    pageContentViewController.pageIndex = index;
    pageContentViewController.Source = _Source;
    
    pageContentViewController.BannerDetails = [_BannerDetailsList objectAtIndex:index];
    
    return pageContentViewController;
    
}



-(void)getBanner
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/banner",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@",URL,appDelegate.Language];
    
    [ReqHandler sendHttpGETRequest:self :nil];
    
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived <<<<< Banner = %@",Response);
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];

    Response = [Response objectForKey:@"responseMap"];
    _BannerDetailsList = [[Response objectForKey:@"banners"]mutableCopy];
    
    // NEW SALATI BANNER - START
    // READ ME : SALATI BANNER
    // attribute : bannerPath|bannerOrder|type|searchKey|status
    
    NSMutableDictionary *BannerDict = [NSMutableDictionary new];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"PROMOTIONAL_BANNER"];
    if(DummyArray.count > 0)
    {
        [BannerDict setObject:[DummyArray objectAtIndex:0] forKey:@"bannerPath"];
        [BannerDict setObject:[DummyArray objectAtIndex:1] forKey:@"bannerOrder"];
        [BannerDict setObject:[DummyArray objectAtIndex:2] forKey:@"type"];
        [BannerDict setObject:[DummyArray objectAtIndex:3] forKey:@"searchKey"];
        [BannerDict setObject:[DummyArray objectAtIndex:4] forKey:@"status"];
        NSLog(@"salati BannerDict = %@",BannerDict);
    }
    
    NSString *status = [BannerDict objectForKey:@"status"];
    
    if([status isEqualToString:@"YES"])
    {
        [_BannerDetailsList addObject:BannerDict];
    }
    else
    {
        
    }
    
    //NEW SALATI BANNER - END
    
    if(_BannerDetailsList.count > 0)
    {
        
        SIXDPageContentVC *startingViewController = [self viewControllerAtIndex:0];
        
        
        NSArray *viewControllers = @[startingViewController];
        
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        [self addChildViewController:_pageViewController];
        [self.view addSubview:_pageViewController.view];
        [self.pageViewController didMoveToParentViewController:self];
        
        [self.view bringSubviewToFront:_PageControl];
        _PageControl.numberOfPages = _BannerDetailsList.count;
        [self setPageControlDots];
        
    }
    NSLog(@"onSuccessResponseRecived <<<<< Banner");
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (void)scrollingTimer
{
    if(_BannerDetailsList.count == 0)
        return;
    
    // NSLog(@"Changing Page Automatically");
    _CurrentIndex++;
    if(_CurrentIndex >= _BannerDetailsList.count)
    {
        _CurrentIndex = 0;
    }
    
    [_PageControl setCurrentPage:_CurrentIndex];
    [self setPageControlDots];
    
    SIXDPageContentVC *nextViewController = [self viewControllerAtIndex:_CurrentIndex];
    if (nextViewController == nil)
    {
        _CurrentIndex = 0;
        nextViewController = [self viewControllerAtIndex:_CurrentIndex];
    }
    [self.pageViewController setViewControllers:@[nextViewController]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [self setPageControlDots];
    NSLog(@"viewDidAppear >>> banner");
    if(_Timer)
    {
        [_Timer invalidate];
        _Timer = nil;
    }
    else
    {
        _Timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    }
    
}

-(void)loadPromotionList
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.PromotionList.count > 0)
    {
    _BannerDetailsList = [NSMutableArray new];
        NSLog(@"PromotionList = %@",appDelegate.PromotionList);
    for (int i =0; i<appDelegate.PromotionList.count ; i++)
    {
        
        //NSURL *url =[NSURL URLWithString:[appDelegate.PromotionList objectAtIndex:i]];
        //TEMP
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[appDelegate.PromotionList objectAtIndex:i] forKey:@"bannerPath"];
        [_BannerDetailsList addObject:Dict];
    
    }
    
    SIXDPageContentVC *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:_PageControl];
    _PageControl.numberOfPages = _BannerDetailsList.count;
    [self setPageControlDots];
    }
    
}


-(void)loadBanner
{
    NSLog(@"loadBanner>>>");
    if(_BannerDetailsList.count <= 0)
    {
         NSLog(@"here 11111>>>");
        [self.view addSubview:_overlayview];
        [_ActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_ActivityIndicator];
        [self getBanner];
    }
    NSLog(@"loadBanner<<<<");
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    if([_Timer isValid])
    {
        [_Timer invalidate];
        _Timer = nil;
    }
}

-(void)setPageControlDots
{
    for (int i = 0; i < _PageControl.numberOfPages; i++) {
        UIView* dot = [_PageControl.subviews objectAtIndex:i];
        if (i == _PageControl.currentPage) {
            dot.backgroundColor = [UIColor whiteColor];
            dot.layer.cornerRadius = dot.frame.size.height / 2;
        } else {
            dot.backgroundColor = [UIColor clearColor];
            dot.layer.cornerRadius = dot.frame.size.height / 2;
            dot.layer.borderColor = [UIColor whiteColor].CGColor;
            dot.layer.borderWidth = 1;
        }
    }
    
}



@end
