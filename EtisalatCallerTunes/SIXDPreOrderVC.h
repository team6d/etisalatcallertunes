//
//  SIXDPreOrderVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDPreOrderVC : ViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property(nonatomic,strong) NSMutableArray *BannerDetails;
- (IBAction)onClickedBuyButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *HeaderImage;

@end
