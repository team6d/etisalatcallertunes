//
//  SIXDKaraokeVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 24/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDAVPlayer.h"
#import "WebKit/WebKit.h"

#define KARAOKE_UPLOAD              701
#define GET_TONE_PRICE_KARAOKE      702


@interface SIXDKaraokeVC : ViewController<UITextFieldDelegate,UIScrollViewDelegate,WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *DIYImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (weak, nonatomic) IBOutlet UIView *TuneNameView;
@property (weak, nonatomic) IBOutlet UITextField *TuneTextField;
@property (weak, nonatomic) IBOutlet UILabel *TuneLabel;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

@property (weak, nonatomic) IBOutlet UIButton *uploadButton;
- (IBAction)onClickedUploadButton:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;

@property (weak, nonatomic) IBOutlet UIButton *PlayStopButton;
@property (weak, nonatomic) IBOutlet UIButton *RecordButton;
@property (weak, nonatomic) IBOutlet UIButton *DeleteButton;

@property (weak, nonatomic) IBOutlet UIButton *TnCCheckBox;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (strong,nonatomic)NSString *RecordFileName;

@property int currentState;
@property (strong,nonatomic) SIXDAVPlayer *SIXDAVPlayer;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;


- (IBAction)onSelectedCloseButton:(id)sender;
- (IBAction)onSelectedTermsAndConditionButton:(id)sender;

@property (nonatomic)BOOL isRecordStarted;
@property (nonatomic)BOOL isPlaying;
@property BOOL isRecordingStopped;
@property BOOL isCheckBoxSelected;

@property(strong,nonatomic)NSData *karaokeData;
@property (strong,nonatomic)UIView *AlertView;
@property(nonatomic) BOOL IsAPICalled;
@property (weak, nonatomic) IBOutlet UIButton *Terms_Button;
@property (weak, nonatomic) IBOutlet UIView *TnCSubView;

@property(nonatomic) int APIInvoked;

//@property(nonatomic) BOOL IsInitialLoad;

@end

