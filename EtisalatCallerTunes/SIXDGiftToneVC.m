//
//  SIXDGiftToneVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 10/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDGiftToneVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"

@interface SIXDGiftToneVC ()

@end

@implementation SIXDGiftToneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    [_MobileNumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    _MobileNumberTextField.keyboardType = ASCII_NUM_KEYBOARD;

    [self localisation];
    
    _MobileNumberLabel.hidden = YES;
    _MobileNumberTextField.delegate = self;
    _MobileNumberTextField.enablesReturnKeyAutomatically = NO;

    _MobileNumberView.layer.borderWidth = 1.0f;
    _MobileNumberView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    //_PriceLabel.text = NSLocalizedStringFromTableInBundle(@"?AED",nil,appDelegate.LocalBundel,nil);
    [_AlertLabel setHidden:true];
    _AlertLabel.textColor = [UIColor redColor];
    
    
    if(appDelegate.DeviceSize == iPHONE_X)
    {
        _PopUpViewHeight.constant = _PopUpViewHeight.constant + 20;
    }
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"GET PRICE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    if(_IsSourceGiftPlaylist)
    {
        _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT A PLAYLIST",nil,appDelegate.LocalBundel,nil);
        _TopLabel.text = [SIXDGlobalViewController CapitalisationConversion:[_ToneDetails objectForKey:@"description"]];
        _BottomLabel.text = @"";
        
        //_PopUpViewHeight.constant = _PopUpViewHeight.constant -18;
        NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImage"]];
        [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    }
    else
    {
        _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT A TUNE",nil,appDelegate.LocalBundel,nil);
        NSString *String1 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"artistName"]];
        _TopLabel.text = [String1 capitalizedString];
        NSString *String2 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"toneName"]];
        _BottomLabel.text = [String2 capitalizedString];
        
        NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
        [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    }
    
    
     [SIXDStylist addKeyBoardDoneButton:_MobileNumberTextField :self];

    
    
}

-(void) dismissKeyboard
{
    [_MobileNumberTextField resignFirstResponder];
    if(_MobileNumberTextField.text.length==0)
    {
        _AlertLabel.hidden=YES;
        _MobileNumberLabel.hidden=YES;
        [SIXDStylist setTextViewNormal:_MobileNumberTextField :_MobileNumberView :nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedCloseButton:(UIButton *)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    [_MobileNumberTextField resignFirstResponder];
    
    NSString * str = [_MobileNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_MobileNumberTextField.text length])];

    
    if(_MobileNumberTextField.text.length <= 0)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        [_AlertLabel setHidden:false];
        [SIXDStylist setTextViewAlert:_MobileNumberTextField :_MobileNumberView :nil];
        return;
    }
    
    if(![SIXDStylist isEtisalatValidNumber:_MobileNumberTextField.text]) // checking with local Regular expr for etisalat num
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
        [_AlertLabel setHidden:false];
        [SIXDStylist setTextViewAlert:_MobileNumberTextField :_MobileNumberView :nil];
        return;
    }
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *Aparty = [appDelegate.MobileNumber substringFromIndex:1];
    
    if([str rangeOfString:Aparty].location != NSNotFound )
    {
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"*be generous and gift to another number than yours",nil,appDelegate.LocalBundel,nil);
        [_AlertLabel setHidden:false];
        [SIXDStylist setTextViewAlert:_MobileNumberTextField :_MobileNumberView :nil];
        return;
    }
    
    if(_ShouldDismissView)
    {
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [SIXDStylist setTextViewNormal:_MobileNumberTextField :_MobileNumberView :nil];
        [_AlertLabel setHidden:true];
        
        [_ActivityIndicator startAnimating];
        _ConfirmButton.alpha = 0.3;
        [_ConfirmButton setUserInteractionEnabled:false];
        [_SubView setUserInteractionEnabled:false];
        
        NSString *toneId = [_ToneDetails objectForKey:@"toneId"];
        if(toneId.length <= 0)
        {
            toneId = [_ToneDetails objectForKey:@"toneCode"];
        }
        
        NSString *GAEventDetails;
        if (_BottomLabel.text.length <= 0)
        {
          GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_TopLabel.text,_TopLabel.text,toneId];
        }
        else
        {
            GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,toneId];
        }
        
        
        if(!_IsGetTonePriceCalled)
        {
            if(_IsSourceSalati)
            {
                [SIXDStylist setGAEventTracker:@"Salati" :@"Gift salati request" :nil];
            }
            else
            {
                [SIXDStylist setGAEventTracker:@"Gift" :@"Request gift price" :GAEventDetails];
            }
            [self API_GetTonePriceAndValidateNumber];
        }
        else
        {
            if(_IsSourceSalati)
            {
                [SIXDStylist setGAEventTracker:@"Salati" :@"Gift salati confirm" :nil];
            }
            else
            {
                [SIXDStylist setGAEventTracker:@"Gift" :@"Confirm gift" :GAEventDetails];
            }
            [self API_SendGift];
        }
    }
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT A TUNE",nil,appDelegate.LocalBundel,nil);
    _MobileNumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
    
    [_MobileNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Receiver Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    [SIXDStylist setTextViewNormal:_MobileNumberTextField :_MobileNumberView :nil];
    

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _SpaceBtwConfirmButtonAndPriceLabel.constant = 38;
    }
    
    if(_MobileNumberTextField.text.length ==0)
    {
        _MobileNumberTextField.text = @"+971-";
    }
    
    _TopSpaceToTextField.constant=19;
    _MobileNumberLabel.hidden=NO;
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_MobileNumberTextField :_MobileNumberView :nil];
    
    [_MobileNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _SpaceBtwConfirmButtonAndPriceLabel.constant = 8;
    }
    
    if([_MobileNumberTextField.text isEqualToString:@"+971-"])
    {
        _MobileNumberTextField.text = @"";
    }

    if(_MobileNumberTextField.text.length==0)
    {
        [_MobileNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Receiver Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        
        _TopSpaceToTextField.constant=0;
    }
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    _PhoneNumbers = contact.phoneNumbers;
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
}

- (IBAction)onSelectedContactButton:(id)sender
{
    NSLog(@"onSelectedContactButton");
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    
    [self dismissKeyboard];
    //_MobileNumberTextField.text = @"";
    
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
    
    
}

-(void)contactPickerDidCancel:(CNContactPickerViewController *)picker
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if(_MobileNumberTextField.text.length==0)
    {
        [_MobileNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
         _MobileNumberLabel.hidden = YES;
        _TopSpaceToTextField.constant=0;
        
    }
}

-(void)ShowMultipleNumbers
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                if(_MobileNumberTextField.text.length==0)
                                     {
                                         [_MobileNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
                                         
                                         _MobileNumberLabel.hidden = YES;
                                         _TopSpaceToTextField.constant=0;
                                         
                                     }
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _MobileNumberTextField.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        
        [self presentViewController:alert animated:YES completion:nil];

        
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _MobileNumberTextField.text = [label.value stringValue];
    }
    
    if(_MobileNumberTextField.text > 0)
    {
        _MobileNumberLabel.hidden=NO;
        _TopSpaceToTextField.constant=19;
    }
    
}

-(void)API_SendGift
{
    NSLog(@"Inside API_SendGift");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SET_TONE;
    
 
    NSString * str = [_MobileNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_MobileNumberTextField.text length])];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    NSString *toneId = [_ToneDetails objectForKey:@"toneId"];
    if(toneId.length==0)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    
    
    ReqHandler.Request = [NSString stringWithFormat:@"msisdnA=%@&msisdnB=%@&toneId=%@&toneName=%@&language=%@&priority=0&packName=%@",appDelegate.MobileNumber,str,toneId,[_ToneDetails objectForKey:@"toneName"],appDelegate.Language,_PackName];

    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/send-gift",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_GetTonePriceAndValidateNumber
{
    if(_IsSourceGiftPlaylist)
    {
        [SIXDStylist setGAScreenTracker:@"Gift_Playlist"];
    }
    else if(_IsSourceSalati)
    {
        [SIXDStylist setGAScreenTracker:@"Gift_Salati"];
    }
    else
    {
        [SIXDStylist setGAScreenTracker:@"Gift_Tune"];
    }
    
    NSLog(@"Inside API_GetTonePriceAndValidateNumber");
    [ _MobileNumberTextField setUserInteractionEnabled:false];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_TONE_PRICE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSString * str = [_MobileNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_MobileNumberTextField.text length])];
    
    NSMutableArray *MemberListArray = [NSMutableArray new];
    NSMutableDictionary *ContactDict = [NSMutableDictionary new];
    [ContactDict setObject:str  forKey:@"bPartyMsisdn"];
    [MemberListArray addObject:ContactDict];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:MemberListArray options:0 error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else
    {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
  
   // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&serviceId=1&aPartyMsisdn=0508035305&toneId=32343&validationIdentifier=2&channelId=1&bPartyMsisdnList=%@",Rno,MemberDetails];
    NSString *toneId;
    
    if(_IsSourceGiftPlaylist)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    else
    {
        toneId = [_ToneDetails objectForKey:@"toneId"];
    }
    
    
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=%@&validationIdentifier=2&channelId=%@&bPartyMsisdnList=%@",Rno,appDelegate.Language, appDelegate.MobileNumber,toneId,CHANNEL_ID,MemberDetails];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_GetTonePriceAndValidateNumber onSuccessResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(appDelegate.APIFlag==GET_TONE_PRICE)
    {
        [ _AlertButton setHidden:YES];
        [_MobileNumberTextField setUserInteractionEnabled:NO];
        _MobileNumberView.userInteractionEnabled=FALSE;
        _MobileNumberView.layer.borderWidth = 1.0f;
        _MobileNumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
        _MobileNumberView.backgroundColor = UIColorFromRGB(CLR_DISABLE_TEXTFIELD);
        
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        NSLog(@"GET_TONE_PRICE");
        
        NSMutableArray *responseDetailsList = [responseMap objectForKey:@"responseDetails"];
        NSMutableDictionary *responseDetails = [responseDetailsList objectAtIndex:0];

        int statusCodes = [[responseDetails objectForKey:@"statusCodes"]intValue];
        
        NSString *Price;
       
        int temp = [[responseDetails objectForKey:@"amount"] intValue];
        NSMutableDictionary *Dict = [appDelegate.AppSettings objectForKey:@"VALIDITY"];
        Price = [Dict objectForKey:[responseDetails objectForKey:@"packName"]];
        _PackName = [responseDetails objectForKey:@"packName"];
        Price = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
        
        switch (statusCodes)
        {
            case 0:
            {
                _IsGetTonePriceCalled = true;
                 _PriceLabel.text = Price;
                [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                }
                 break;
            
           /*
            case 1:
                NSLog(@"Tone already present");
                break;
                
            case 2:
                NSLog(@"VIP limit crossed");
                break;
             */
            default:
                {
                    NSString *Price=@"";
            
                        _PriceLabel.text = Price;
                        Price = [NSString stringWithFormat:@"%@\n%@",Price,[responseDetails objectForKey:@"statusDesc"]];
                 
                    
                    /* [_ActivityIndicator startAnimating];
                     _ConfirmButton.alpha = 0.3;
                     [_ConfirmButton setUserInteractionEnabled:false];
                     */
                   /// _PriceLabel.text = Price;
                    _PriceLabelHeight.constant = _PriceLabelHeight.constant+16;
                    _PopUpViewHeight.constant = _PopUpViewHeight.constant+16;
                    _PriceLabel.text = Price;
                }
                break;
        }
        
    }
    else
    {
       
       
        NSString *message = [Response objectForKey: @"message"];
        
        if(message.length <= 0)
        {
            //HARD CODED
            message = NSLocalizedStringFromTableInBundle(@"General_Success_Message",nil,appDelegate.LocalBundel,nil);
        }
        
        NSMutableAttributedString *attributedMessage =[[NSMutableAttributedString alloc] initWithString:message];
        
        _AlertLabel.textColor = [UIColor darkGrayColor];
    
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        [style setLineSpacing:10];
        [style setAlignment:NSTextAlignmentCenter];
        [attributedMessage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,message.length)];
        _AlertLabel.attributedText = attributedMessage;
        //[_AlertLabel sizeToFit];
        [_AlertLabel setHidden:false];
        [_PriceLabel setHidden:true];
        _ShouldDismissView = true;

        [_MobileNumberView setHidden:true];

        [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];
   
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.text = [Response objectForKey:@"message"];
    if(_AlertLabel.text.length <= 0)
    {
        _AlertLabel.text =NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    }
    
    //[_AlertLabel sizeToFit];
    _AlertLabel.textAlignment = NSTextAlignmentCenter;
    [_AlertLabel setHidden:false];
    
    [_MobileNumberView setHidden:true];
    [_PriceLabel setHidden:true];
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _ShouldDismissView = true;
    
   /*
    if(appDelegate.APIFlag != GET_TONE_PRICE)
    {
        [_MobileNumberView setHidden:true];
        [_PriceLabel setHidden:true];
        [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        _ShouldDismissView = true;
    }
    else
    {
         [ _MobileNumberTextField setUserInteractionEnabled:true];
    }
   */
   
    
}


-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _AlertLabel.text =NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    
    //_AlertLabel.text =[NSString stringWithFormat:@"Failed to connect to server."];
    _AlertLabel.textAlignment = NSTextAlignmentCenter;
    //[_AlertLabel sizeToFit];
    [_AlertLabel setHidden:false];
    if(appDelegate.APIFlag != GET_TONE_PRICE)
    {
        [_PriceLabel setHidden:true];
         [_MobileNumberView setHidden:true];
        [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        _ShouldDismissView = true;
    }
    else
    {
        [ _MobileNumberTextField setUserInteractionEnabled:true];
    }
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"STRING = %@ range=%lu", string,(unsigned long)range.location);
    if(textField.text.length == 5 && string.length == 0 && [textField.text isEqualToString:@"+971-"])
    {
        return NO;
    }
    else if(textField.text.length == 15 && string.length == 1)
    {
        return NO;
    }
    else
        return YES;
}



@end
