//
//  SIXDPlayingToMyCallersVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDPlayingToMyCallersVC.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDCommonPopUp_Text.h"
#import "UIImageView+WebCache.h"
#import "SIXDMainSettingsVC.h"
#import "SIXDStylist.h"

@interface SIXDPlayingToMyCallersVC ()

@end

@implementation SIXDPlayingToMyCallersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_PlayingToMyCalllers"];
    
    _IsRefreshRequired = true;
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"Playing to my callers_CAPS",nil,appDelegate.LocalBundel,nil);
    
    _PlayingTunesList = [NSMutableArray new];
    
    
    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no tunes currently playing",nil,appDelegate.LocalBundel,nil) :20];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return _PlayingTunesList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    NSMutableDictionary *Dict = [_PlayingTunesList objectAtIndex:indexPath.row];
    
    cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"Live",nil,appDelegate.LocalBundel,nil);
    
    /*
    if([[Dict objectForKey:@"serviceName"] isEqualToString:@"GroupCallerSetting"])
    {
        cell.TitleLabel.text = [Dict objectForKey:@""];
        cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"1000 years",nil,appDelegate.LocalBundel,nil);
        
        UIImage *image = [[UIImage imageNamed:@"playingtomycallersbell"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.BuyButton setImage:image forState:UIControlStateNormal];
        [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    }
    */
     
    //else
   
    // temp ignoring the status rbt
    /*
    if([[Dict objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"])
    {
        return cell;
    }
    */
    
    if([[Dict objectForKey:@"serviceName"] isEqualToString:@"AllCaller"])
    {

        NSString *ShuffleStatus = [Dict objectForKey:@"isShuffle"];
        if(ShuffleStatus)
        {
           if([ShuffleStatus isEqualToString:@"T"])
               cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"Shuffle",nil,appDelegate.LocalBundel,nil);
               
        }
       
    }
    
    UIImage *image = [[UIImage imageNamed:@"playingtomycallersbell"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.BuyButton setImage:image forState:UIControlStateNormal];
    [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"artistName"]];
    cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
    
    
    
    if(cell.TitleLabel.text.length <= 0)
    {
        cell.TitleLabel.text = cell.SubTitleLabel.text;
        cell.SubTitleLabel.text = @"";
    }
    
    NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    int x_Position;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        x_Position=0;
    }
    else
    {
        x_Position=50-1;
    }
    
    UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
    [cell.BuyButton addSubview:leftBorder2];
    
    UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    [cell.AccessoryButton addSubview:leftBorder3];
    
    cell.BuyButton.tag = indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    if(_IsRefreshRequired)
    {
        [self API_ListTones];
        _IsRefreshRequired = false;
    }
    
}

-(void)API_ListTones
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/list-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&startIndex=0&endIndex=20&rbtMode=0",URL,appDelegate.Language,appDelegate.MobileNumber];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [_AlertView setHidden:TRUE];
    // added by shahnawaz on 25th march
    [_PlayingTunesList removeAllObjects];
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    NSMutableArray *listToneApkArray = [responseMap objectForKey:@"listToneApk"];
    NSLog(@"listToneApkArray = %@",listToneApkArray);
    for(int i=0;i<listToneApkArray.count;i++)
    {
        NSMutableDictionary *DummyDict = [listToneApkArray objectAtIndex:i];
        NSMutableDictionary *ToneDetails = [NSMutableDictionary new];
        
        if([DummyDict objectForKey:@"toneDetails"])
        {
            NSMutableArray *Details = [DummyDict objectForKey:@"toneDetails"];
            ToneDetails = [[Details objectAtIndex:0] mutableCopy];
        
            if([DummyDict objectForKey:@"serviceName"])
            {
                [ToneDetails setObject:[DummyDict objectForKey:@"serviceName"] forKey:@"serviceName"];
                if([[DummyDict objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"]) // ignoring status rbt if time is not set
                {
                    NSLog(@"statusRBT");
                    if(([[ToneDetails objectForKey:@"customiseStartDate"] isEqualToString:@"0"] || [[ToneDetails objectForKey:@"customiseEndDate"] isEqualToString:@"0"])  && ([[ToneDetails objectForKey:@"weeklyDays"] isEqualToString:@"0"]) && ([[ToneDetails objectForKey:@"startDayMonthly"] isEqualToString:@"0"] || [[ToneDetails objectForKey:@"endDayMonthly"] isEqualToString:@"0"]) && ([[ToneDetails objectForKey:@"yearlyStartMonth"] isEqualToString:@"0"] || [[ToneDetails objectForKey:@"yearlyEndMonth"] isEqualToString:@"0"]))
                    {
                        NSLog(@"continue");
                        continue;
                    }
                }
            }
            
            
            if([DummyDict objectForKey:@"groupId"])
                [ToneDetails setObject:[DummyDict objectForKey:@"groupId"] forKey:@"groupId"];
            
            if([DummyDict objectForKey:@"groupName"])
                [ToneDetails setObject:[DummyDict objectForKey:@"groupName"] forKey:@"groupName"];
            
            if([DummyDict objectForKey:@"msisdnB"])
                [ToneDetails setObject:[DummyDict objectForKey:@"msisdnB"] forKey:@"msisdnB"];
            [_PlayingTunesList addObject:ToneDetails];
            
        }
        
    }
    if(_PlayingTunesList.count==0)
        [_AlertView setHidden:FALSE];
    [_TableView reloadData];
    
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [_AlertView setHidden:FALSE];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [_AlertView setHidden:FALSE];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}


- (IBAction)onClickedDeleteButton:(UIButton *)sender
{
    [self LoadSettingsPage:sender.tag];
}

- (void) decodeResposeForShowSettings : (long) tag
{
    NSLog(@"decodeResposeForShowSettings >>>");
    
    NSMutableDictionary *Dict = [_PlayingTunesList objectAtIndex:tag];
    
    NSString *customiseStartDate = [Dict objectForKey:@"customiseStartDate"];
     NSString *customiseEndDate = [Dict objectForKey:@"customiseEndDate"];
    
    if(![customiseStartDate isEqualToString:@"0"] || ![customiseEndDate isEqualToString:@"0"])
    {
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self LoadSettingsPage:indexPath.row];
}

-(void)LoadSettingsPage:(unsigned long)tag
{
    NSLog(@"LoadSettingsPage >>");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    SIXDMainSettingsVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainSettingsVC"];
    
    // [self decodeResposeForShowSettings:sender.tag];
    ViewController.ScreenType = PLAYING_TO_CALLERS_SETTINGS_SCREEN;
    
    // ********start decoding
    
    NSMutableDictionary *Dict = [_PlayingTunesList objectAtIndex:tag];
    
    ViewController.SettingsDataDict = [NSMutableDictionary new];
    ViewController.PlayingToMyCallerVC = self;
    
    
    //NSString *Start = [Dict objectForKey:@"customiseStartDate"];
    //NSString *End = [Dict objectForKey:@"customiseEndDate"];
   /*
    if([[Dict objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"])
    {
        NSLog(@"STATUSRBT");
        NSString *string = [Dict objectForKey:@"statusRbtStartDay"];
        if(string.length != 0)
        {
            //NSArray *Array = [string componentsSeparatedByString:@" "];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy mm:ss"];
            
            //string = [Array objectAtIndex:0];
            //NSDate *dateFromString = [dateFormatter dateFromString:string];
            __block NSDate *dateFromString;
            NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
            [detector enumerateMatchesInString:string
                                       options:kNilOptions
                                         range:NSMakeRange(0, [string length])
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
             { dateFromString = result.date; }];
            
            NSLog(@"dateFromString = %@",dateFromString);
            
            NSString *FinalDate = [dateFormatter stringFromDate:dateFromString];
            NSLog(@"dateFromStringd = %@",FinalDate);
        }
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"statusRbtStartDay"] forKey:@"customizeStartDate"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"statusRbtEndDay"] forKey:@"customizeEndDate"];
        
        
        
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseStartTime"] forKey:@"monthlyStartTime"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseEndTime"] forKey:@"monthlyEndTime"];
        ViewController.TimeSettingsType = SETTINGS_BETWEEN_DATES;
    }
    */
    if([[Dict objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"])
    {
        if(([[Dict objectForKey:@"customiseStartDate"] isEqualToString:@"0"] || [[Dict objectForKey:@"customiseEndDate"] isEqualToString:@"0"])  && ([[Dict objectForKey:@"weeklyDays"] isEqualToString:@"0"]) && ([[Dict objectForKey:@"startDayMonthly"] isEqualToString:@"0"] || [[Dict objectForKey:@"endDayMonthly"] isEqualToString:@"0"]) && ([[Dict objectForKey:@"yearlyStartMonth"] isEqualToString:@"0"] || [[Dict objectForKey:@"yearlyEndMonth"] isEqualToString:@"0"]))
        {
            
        }
    }
    
  if(![[Dict objectForKey:@"customiseStartDate"] isEqualToString:@"0"] || ![[Dict objectForKey:@"customiseEndDate"] isEqualToString:@"0"])
    {
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseStartDate"] forKey:@"customizeStartDate"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseEndDate"] forKey:@"customizeEndDate"];
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseStartTime"] forKey:@"monthlyStartTime"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"customiseEndTime"] forKey:@"monthlyEndTime"];
        ViewController.TimeSettingsType = SETTINGS_BETWEEN_DATES;
    }
    
    else if(![[Dict objectForKey:@"weeklyDays"] isEqualToString:@"0"])
    {
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"weeklyDays"] forKey:@"weeklyDays"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"startTimeWeekly"] forKey:@"weeklyStartTime"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"endTimeWeekly"] forKey:@"weeklyEndTime"];
        
        if([[Dict objectForKey:@"startTimeWeekly"] isEqualToString:@"00:00:00"] && [[Dict objectForKey:@"endTimeWeekly"] isEqualToString:@"23:59:59"])
        {
            ViewController.TimeSettingsType = SETTINGS_WEEKLY_TIME;
        }
        else
            ViewController.TimeSettingsType = SETTINGS_SPECIFIC_TIME;
    }
    /*
     else if(![[Dict objectForKey:@"weeklyDays"] isEqualToString:@"0"])
     {
     [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"weeklyDays"] forKey:@"weeklyDays"];
     [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"startTimeWeekly"] forKey:@"weeklyStartTime"];
     [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"endTimeWeekly"] forKey:@"weeklyEndTime"];
     }
     */
    else if(![[Dict objectForKey:@"startDayMonthly"] isEqualToString:@"0"] || ![[Dict objectForKey:@"endDayMonthly"] isEqualToString:@"0"])
    {
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"startDayMonthly"] forKey:@"startDayMonthly"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"endDayMonthly"] forKey:@"endDayMonthly"];
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"startTimeMonthly"] forKey:@"monthlyStartTime"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"endTimeMonthly"] forKey:@"monthlyEndTime"];
        
        ViewController.TimeSettingsType = SETTINGS_MONTLY_TIME;
    }
    else if(![[Dict objectForKey:@"yearlyStartMonth"] isEqualToString:@"0"] || ![[Dict objectForKey:@"yearlyEndMonth"] isEqualToString:@"0"])
    {
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyStartMonth"] forKey:@"yearlyStartMonth"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyEndMonth"] forKey:@"yearlyEndMonth"];
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyStartDay"] forKey:@"yearlyStartDay"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyEndDay"] forKey:@"yearlyEndDay"];
        
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyStartTime"] forKey:@"yearlyStartTime"];
        [ViewController.SettingsDataDict setObject:[Dict objectForKey:@"yearlyEndTime"] forKey:@"yearlyEndTime"];
        
        ViewController.TimeSettingsType = SETTINGS_YEARLY_TIME;
    }
    
    
    
    //*************end decoding
    ViewController.ToneDetails= [_PlayingTunesList objectAtIndex:tag];
    [self.navigationController pushViewController:ViewController animated:YES];
    NSLog(@"onClickedMore(Delete)Button >>");
    _ButtonTag = tag;
}

@end
