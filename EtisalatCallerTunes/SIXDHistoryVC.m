//
//  SIXDHistoryVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 25/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDHistoryVC.h"
#import "AppDelegate.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"


@interface SIXDHistoryVC ()

@end

@implementation SIXDHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    _DetailsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _TransactionTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _ItemLabel.text = NSLocalizedStringFromTableInBundle(@"Item",nil,appDelegate.LocalBundel,nil);
    _TransactionLabel.text = NSLocalizedStringFromTableInBundle(@"Transaction",nil,appDelegate.LocalBundel,nil);
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"HISTORY",nil,appDelegate.LocalBundel,nil);
    if([appDelegate.Language isEqualToString:@"English"])
    {
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(15, _InfoView.frame.size.height - 1.0f, _InfoView.frame.size.width, 0.5)];
        bottomBorder.backgroundColor = UIColorFromRGB(CLR_TEXTFIELDBORDER);
        [_InfoView addSubview:bottomBorder];
    }
    else
    {
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _InfoView.frame.size.height - 1.0f, _InfoView.frame.size.width-15, 0.5)];
        bottomBorder.backgroundColor = UIColorFromRGB(CLR_TEXTFIELDBORDER);
        [_InfoView addSubview:bottomBorder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.restorationIdentifier isEqualToString:@"SIXDHistoryTransactions"])
        return _TransactionList.count;
    else
        return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if([self.restorationIdentifier isEqualToString:@"SIXDHistoryTransactions"])
    {
        
        NSMutableDictionary *Dict = [_TransactionList objectAtIndex:indexPath.row];
        
        NSString *str = [SIXDGlobalViewController URLDecode:[Dict objectForKey:@"englishToneName"]];
        //cell.TitleLabel.text = [Dict objectForKey:@"englishToneName"];
        cell.TitleLabel.text = str;
        
        NSString *string = [NSString stringWithFormat:@"%@ %@",[Dict objectForKey:@"toneType"],[Dict objectForKey:@"transactionType"]];
        cell.SubTitleLabel.text = [string stringByReplacingOccurrencesOfString:@"N/A" withString:@""];
        cell.accessoryView.hidden=NO;
        cell.accessoryView.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        
        UIImageView *sampleImage;
        if([appDelegate.Language isEqualToString:@"English"])
        {
            sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow"]];
        }
        else
        {
            sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow_Arabic"]];
        }
        [cell setAccessoryView:sampleImage];
        
    }
    else
    {
        
        if(indexPath.row==0)
        {
            [cell.AccessoryButton setImage:[UIImage imageNamed:@"history_item"] forState:UIControlStateNormal];
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"Item",nil,appDelegate.LocalBundel,nil);
            NSString *str = [SIXDGlobalViewController URLDecode:[_DetailsList objectForKey:@"englishToneName"]];
            cell.SubTitleLabel.text = str;
        }
        else if(indexPath.row==1)
        {
            [cell.AccessoryButton setImage:[UIImage imageNamed:@"history_tranaction"] forState:UIControlStateNormal];
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"Transaction",nil,appDelegate.LocalBundel,nil);
            NSString *string = [NSString stringWithFormat:@"%@ %@",[_DetailsList objectForKey:@"toneType"],[_DetailsList objectForKey:@"transactionType"]];
            cell.SubTitleLabel.text = [string stringByReplacingOccurrencesOfString:@"N/A" withString:@""];
        }
        else if(indexPath.row==2)
        {
            [cell.AccessoryButton setImage:[UIImage imageNamed:@"history_datetime"] forState:UIControlStateNormal];
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"Date/Time",nil,appDelegate.LocalBundel,nil);
            
            NSString *string = [_DetailsList objectForKey:@"subscriptionDate"];
            if(string.length != 0)
            {
                NSArray *Array = [string componentsSeparatedByString:@" "];
            
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd/MM/yyyy"];
            
                string = [Array objectAtIndex:0];
                //NSDate *dateFromString = [dateFormatter dateFromString:string];
                __block NSDate *dateFromString;
                NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingAllTypes error:nil];
                [detector enumerateMatchesInString:string
                                           options:kNilOptions
                                             range:NSMakeRange(0, [string length])
                                        usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                { dateFromString = result.date; }];
                
                NSLog(@"dateFromString = %@",dateFromString);
              
                NSString *FinalDate = [dateFormatter stringFromDate:dateFromString];
                NSLog(@"dateFromStringd = %@",FinalDate);
                
                string = [Array objectAtIndex:1];
                Array = [string componentsSeparatedByString:@":"];
                FinalDate = [NSString stringWithFormat:@"%@ - %@:%@",FinalDate,[Array objectAtIndex:0],[Array objectAtIndex:1]];
                cell.SubTitleLabel.text = FinalDate;
            }
            else
            {
                cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"NA",nil,appDelegate.LocalBundel,nil);
            }
        }
        else
        {
            [cell.AccessoryButton setImage:[UIImage imageNamed:@"history_from"] forState:UIControlStateNormal];
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"FROM",nil,appDelegate.LocalBundel,nil);
            cell.SubTitleLabel.text = [_DetailsList objectForKey:@"channel"];
        }
        
        cell.SubView.layer.borderWidth = 0.5f;
        cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.restorationIdentifier isEqualToString:@"SIXDHistoryTransactions"])
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDHistoryVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDHistoryDetails"];
        ViewController.DetailsList = [_TransactionList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else
    {
        NSLog(@"In Transaction Details TVC");
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}



-(void)API_ViewTransactionsSCM
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType=GET;
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/view-transactions-scm?msisdn=%@&fromDate=%@&toDate=%@&language=%@",appDelegate.BaseMiddlewareURL,appDelegate.MobileNumber,_fromDateString,_dateString,appDelegate.Language];
    NSLog(@"ViewTransactionsSCM final URl = %@",ReqHandler.FinalURL);
    
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    //todays date
    if([self.restorationIdentifier isEqualToString:@"SIXDHistoryTransactions"])
    {
        [SIXDStylist setGAScreenTracker:@"History_ListTransactions"];
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        _dateString = [dateFormat stringFromDate:today];
        NSLog(@"todays date: %@", _dateString);
        
        //30 days before
        NSDate *currentDate = [NSDate date];
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:-30];
        NSDate *thirtyDaysAgo = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
        
        _fromDateString = [dateFormat stringFromDate:thirtyDaysAgo];
        NSLog(@"thirty days ago date: %@", _fromDateString);
        
        NSLog(@"API_ViewTransactionsSCM call >>");
        [self API_ViewTransactionsSCM];
    }
    else
    {
        [SIXDStylist setGAScreenTracker:@"History_TransactionDetails"];
    }
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_ListTones onSuccessResponseRecived = %@",Response);
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];

    _TransactionList =[[responseMap objectForKey:@"transactionDetailsList"]mutableCopy];
    [_TransactionTableView reloadData];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}
- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
