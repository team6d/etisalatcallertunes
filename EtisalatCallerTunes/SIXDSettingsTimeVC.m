//
//  SIXDSettingsTimeVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSettingsTimeVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "SIXDPickerPopUpNormal.h"
#import "SIXDDatePickerPopUpVC.h"
#import "SIXDTimePickerPopUP.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDDatePickerPopUpVC.h"
#import "SIXDStylist.h"

#define BUTTON_DISABLE_ALPHA    0.6

@interface SIXDSettingsTimeVC ()

@end

@implementation SIXDSettingsTimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Tune_Settings_WhenToPlay"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if([_ToneDetails objectForKey:@"artistName"])
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"artistName"] capitalizedString];
        _BottomLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
    else
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
    
    
    _BottomButtonArray = [NSMutableArray new];
    _RepeatButtonActionsList = [NSMutableArray new];
   // _RepeatButtonActionsDict = [NSMutableDictionary new];
    _Source =0;
    _SelectTimeTypeFromPopUp = POPUP_TIME_TYPE_FULL_DAY;
    _AlertLabel.hidden = YES;
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    _DatePickerPopUpVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
    _DatePickerPopUpVC.TimeVC = self;
    
     StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    _TimePickerPopUP = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTimePickerPopUP"];
    _TimePickerPopUP.TimeVC = self;
    
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    [_MainImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    _TypeTopLabel.text =  NSLocalizedStringFromTableInBundle(@"Select Time Type",nil,appDelegate.LocalBundel,nil);
    _TypeBottomLabel.text = NSLocalizedStringFromTableInBundle(@"Full Day",nil,appDelegate.LocalBundel,nil);
    _RepeatLabel.text = NSLocalizedStringFromTableInBundle(@"REPEAT",nil,appDelegate.LocalBundel,nil);
    
    
    _TimeTypeView.layer.borderColor =[UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _TimeTypeView.layer.borderWidth = 1;
    _StartView.layer.borderColor =[UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _StartView.layer.borderWidth = 1;
    _EndView.layer.borderColor =[UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _EndView.layer.borderWidth = 1;
    
     _FontName = @"ArialMT";
    _TopSpaceToEndlabel.constant = 15;
    [_StartTopLabel setFont:[UIFont fontWithName:_FontName size:14]];
    [_EndTopLabel setFont:[UIFont fontWithName:_FontName size:14]];
    _TopSpaceToStartLabel.constant = 15;
    
    _StartBottomLabel.text = @"";
    _EndBottomLabel.text = @"";

    
    //[_Ty setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: UIColorFromRGB(CLR_PLACEHOLDER)}]];
    
    [self localisation];
    
    //VIEW TAP ACTIONS
    UITapGestureRecognizer *GiftTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleStartViewTap:)];
    [self.StartView addGestureRecognizer:GiftTap];
    
    UITapGestureRecognizer *WishlistTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleEndViewTap:)];
    [self.EndView addGestureRecognizer:WishlistTap];
    
    UITapGestureRecognizer *TAFTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTimeTypeViewTap:)];
    [self.TimeTypeView addGestureRecognizer:TAFTap];
}

- (void)onClickedScrollButton:(id)sender
{
    UIButton *tempButton= (UIButton *)sender;
    NSString *tagString = [NSString stringWithFormat:@"%ld", tempButton.tag];
    NSLog(@"tagString = %@",tagString);
    
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
            NSLog(@"onClickedScrollButton::POPUP_TIME_TYPE_SPECIFIC_DATE >>>");
            if([self addActionsToRepeatButtons :tagString]) //TRUE = ADD SUCCESSFUL
            {
                NSLog(@"Adding action 1");
                [tempButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                tempButton.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
                tempButton.enabled = YES;
                tempButton.alpha =1.0;
                /*
                if(tempButton.tag != 0)
                {
                    
                    NSLog(@"Disabling no ");
                    UIButton *button1 = [_BottomButtonArray objectAtIndex:0];
                    
                    [button1 setTitleColor:UIColorFromRGB(CLR_ETISALAT_GREEN) forState:UIControlStateNormal];
                    button1.backgroundColor=[UIColor whiteColor];
                    [_RepeatButtonActionsList removeObject:@"0"];
                    button1.enabled = YES;
                 
                }
                else
                {
                 */
                 for(int i =0; i < _BottomButtonArray.count; i++)
                    {
                        NSLog(@"disabling buttons");
                        if(i == tempButton.tag)
                        {
                            continue;
                        }
                        UIButton *button = [_BottomButtonArray objectAtIndex:i];
                        [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
                        button.backgroundColor=[UIColor whiteColor];
                        NSString *temp = [NSString stringWithFormat:@"%d",i];
                        [_RepeatButtonActionsList removeObject:temp];
                        button.enabled = YES;
                        button.alpha = 1.0;
                        
                        switch (_RepeatButtonProperty)
                        {
                            case TIME_REPEAT_DISABLE_MONTHLY:
                                if(i ==1) // dont allow monthly to enable
                                {
                                    button.enabled = NO;
                                    button.alpha = BUTTON_DISABLE_ALPHA;
                                }
                                break;
                                
                        }
                        
                        
                    }
                
                NSLog(@"ButtonAction Array = %@", _RepeatButtonActionsList);
            }
            else
            {
                [tempButton setTitleColor:UIColorFromRGB(CLR_ETISALAT_GREEN) forState:UIControlStateNormal];
                tempButton.backgroundColor=[UIColor whiteColor];
                tempButton.enabled = YES;
                tempButton.alpha = 1.0;
            }
           //[_RepeatButtonActionsArray
            break;
            
        case POPUP_TIME_TYPE_FULL_DAY:
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
            NSLog(@"onClickedScrollButton::POPUP_TIME_TYPE_FULL_DAY >>>");
            if([self addActionsToRepeatButtons :tagString]) //TRUE = ADD SUCCESSFUL
            {
                NSLog(@"Adding action 1");
                [tempButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                tempButton.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
                tempButton.enabled = YES;
                tempButton.alpha = 1.0;
            }
            else
            {
                NSLog(@"removing action 1");
                [tempButton setTitleColor:UIColorFromRGB(CLR_ETISALAT_GREEN) forState:UIControlStateNormal];
                tempButton.backgroundColor=[UIColor whiteColor];
                tempButton.enabled = YES;
                tempButton.alpha = 1.0;
            }
            
            break;
        default:
            break;
    }
    
    
    
    NSLog(@"onClickedScrollButton = %ld",(long)tempButton.tag);
}
            
- (BOOL)addActionsToRepeatButtons : (NSString *) tag
{
    //if(_RepeatButtonActionsList.count <=0)
    //    return YES;
    
    for(NSString *existingButtonAction in _RepeatButtonActionsList)
    {
        if([existingButtonAction isEqualToString:tag])
        {
            [_RepeatButtonActionsList removeObject:tag];
            return NO;
        }
    }
    [_RepeatButtonActionsList addObject:tag];
     NSLog(@"ButtonArray object %@",_RepeatButtonActionsList);
    return YES;

}

   - (void)setNewDatePickerObject
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    _DatePickerPopUpVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
    
    _DatePickerPopUpVC.TimeVC = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"SIXDSettngs::viewWillAppear>>");
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"SETTINGS",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
    
    [self setScreenLabels];
    
    switch (_Source)
    {
        case SRC_NORMAL_PICKER:
            _StartBottomLabel.text = @"";
            _EndBottomLabel.text = @"";
              [self setScreenDetails];
            break;
            
        case SRC_DATE_PICKER:
            [self setRepeatButtonProperty];
            break;
            
        case SRC_TIME_PICKER:
           // [self setRepeatButtonProperty];
            break;
            
        default:
            [self setScreenDetails];
            break;
    }

}

- (void) setScreenLabels
{
    NSLog(@"setScreenLabels>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"_StartBottomLabel = %@   %@",_StartBottomLabel.text,_EndBottomLabel.text);
    if(_StartBottomLabel.text.length <= 0)
    {
       
        _TopSpaceToStartLabel.constant = 15;
        [_StartTopLabel setFont:[UIFont fontWithName:_FontName size:14]];
    }
    else
    {

        _TopSpaceToStartLabel.constant = 6;
        [_StartTopLabel setFont:[UIFont fontWithName:_FontName size:11]];
    }
    
    if(_EndBottomLabel.text.length <= 0)
    {
        _TopSpaceToEndlabel.constant = 15;
        [_EndTopLabel setFont:[UIFont fontWithName:_FontName size:14]];
    }
    else
    {
        _TopSpaceToEndlabel.constant = 6;
        [_EndTopLabel setFont:[UIFont fontWithName:_FontName size:11]];
    }
    
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_FULL_DAY:
            [_StartView setHidden:YES];
            [_EndView setHidden:YES];
            
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
            _StartTopLabel.text =  NSLocalizedStringFromTableInBundle(@"Start Time",nil,appDelegate.LocalBundel,nil);
            _EndTopLabel.text =  NSLocalizedStringFromTableInBundle(@"End Time",nil,appDelegate.LocalBundel,nil);
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
            _StartTopLabel.text =  NSLocalizedStringFromTableInBundle(@"Start Date&Time",nil,appDelegate.LocalBundel,nil);
            _EndTopLabel.text =  NSLocalizedStringFromTableInBundle(@"End Date&Time",nil,appDelegate.LocalBundel,nil);
            
            break;
            
        default:
            /*
            [_StartTopLabel setHidden:YES];
            [_StartBottomLabel setHidden:YES];
             */
            break;
    }
}

-(void)repeatButtonUIInitialState
{
    NSLog(@"repeatButtonUIInitialState");
    
    UIButton *button = [_BottomButtonArray objectAtIndex:0];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    button = [_BottomButtonArray objectAtIndex:1];
    [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
    button.backgroundColor=[UIColor whiteColor];
    
    
    button = [_BottomButtonArray objectAtIndex:2];
    [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
    button.backgroundColor=[UIColor whiteColor];
    
}
- (void)setRepeatButtonProperty
{
    NSLog(@"setRepeatButtonProperty >>");
    
    [self repeatButtonUIInitialState];
    
    switch (_RepeatButtonProperty) {
        case TIME_REPEAT_DISABLE_MONTHLY:
        {
             NSLog(@"TIME_REPEAT_DISABLE_MONTHLY");
            UIButton *button = [_BottomButtonArray objectAtIndex:1];
            [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
            button.backgroundColor=[UIColor whiteColor];
            button.enabled = NO;
            button.alpha = BUTTON_DISABLE_ALPHA;
            
            button = [_BottomButtonArray objectAtIndex:2];
           // [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //button.backgroundColor= UIColorFromRGB(CLR_ETISALAT_GREEN);
            
            [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
            button.backgroundColor=[UIColor whiteColor];
            
            button.enabled = YES;
            button.alpha = 1.0;
            
        }
            break;
            
        case TIME_REPEAT_DISABLE_ALL:
            NSLog(@"TIME_REPEAT_DISABLE_ALL");
            for(int i =1; i < _BottomButtonArray.count; i++)
            {
                UIButton *button = [_BottomButtonArray objectAtIndex:i];
               [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
                button.backgroundColor=[UIColor whiteColor];
                button.enabled = NO;
                button.alpha = BUTTON_DISABLE_ALPHA;
            }
            break;
        default:
            NSLog(@"defaultt");
            for(int i =0; i < _BottomButtonArray.count; i++)
            {
                UIButton *button = [_BottomButtonArray objectAtIndex:i];
               // [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                //button.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
                if(i ==0)
                {
                    button.enabled = YES;
                    button.alpha = 1.0;
                    continue;
                }
                [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
                button.backgroundColor=[UIColor whiteColor];
                
                button.enabled = YES;
                button.alpha = 1.0;
            
            }
            
            break;
    }
}

- (void) setScreenDetails
{
    NSLog(@"setScreenDetails >>");
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_FULL_DAY:
        {
              [self createButtonsOnScreen];
        }
            break;
        /*
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
        {
          
        }
        break;
         */
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
        {
            [_StartView setHidden:NO];
            [_EndView setHidden:NO];
            
            [self createButtonsOnScreen];
        }
        default:
            break;
    }

}


- (void) createButtonsOnScreen
{
    NSLog(@"SIXDSettings :  createButtonsOnScreen >>>");
    [_RepeatButtonActionsList removeAllObjects];
    //[_SubScrollView removeFromSuperview];
    
    NSMutableArray *buttonNamesArray = [NSMutableArray new];
    int ButtonLabelTextSize = 0;
    int ButtonWidth = 0;
    int noOfButtonsOnScreen = 0;
    
    for(int i=0;i<_BottomButtonArray.count;i++)
    {
        UIButton *button = [_BottomButtonArray objectAtIndex:i];
        [button removeFromSuperview];
    }
    [_BottomButtonArray removeAllObjects];
    
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_FULL_DAY:
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
            noOfButtonsOnScreen = 7;
            [buttonNamesArray addObject:@"S"];
            [buttonNamesArray addObject:@"M"];
            [buttonNamesArray addObject:@"T"];
            [buttonNamesArray addObject:@"W"];
            [buttonNamesArray addObject:@"T"];
            [buttonNamesArray addObject:@"F"];
            [buttonNamesArray addObject:@"S"];
            
            ButtonLabelTextSize = 15;
            ButtonWidth = 38;
            noOfButtonsOnScreen = 7;
            break;
            
            
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
            [buttonNamesArray addObject:@"No"];
            [buttonNamesArray addObject:@"Monthly"];
            [buttonNamesArray addObject:@"Yearly"];
            
            ButtonLabelTextSize = 15;
            ButtonWidth = 102;
            noOfButtonsOnScreen = 3;
            
            
            //x = 20;
            break;
            
           
             /*
             case TIME_TYPE_SPECIF_TIME:
             noOfButtonsOnScreen = 3;
             break;
             */
            
        default:
            break;
    }
    
    
    int x = 0;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for (int i = 0; i < noOfButtonsOnScreen; i++)
    {
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(x,8,ButtonWidth,40)];
        NSString *buttonName = [buttonNamesArray objectAtIndex:i];
        NSString * str = NSLocalizedStringFromTableInBundle(buttonName,nil,appDelegate.LocalBundel,nil);
        
        [button setTitleColor:(UIColorFromRGB(CLR_ETISALAT_GREEN)) forState:UIControlStateNormal];
        button.backgroundColor=[UIColor whiteColor];
        
        [button setTitle:[NSString stringWithFormat:@"%@", str] forState:UIControlStateNormal];
        [button setTag:i];
        button.titleLabel.font = [UIFont fontWithName:@"ArialMT" size:ButtonLabelTextSize];
        /*
        if([appDelegate.Language isEqualToString:@"English"])
        {
            button.titleLabel.font = [UIFont fontWithName:@"ArialMT" size:ButtonLabelTextSize];
        }
        else
        {
            button.titleLabel.font = [UIFont fontWithName:@"GESSTextMedium-Medium" size:ButtonLabelTextSize];
        }
        */
        button.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
        button.layer.borderWidth = 0.5;
        button.layer.cornerRadius = 0;
        button.clipsToBounds = YES;
        
        [button addTarget:self action:@selector(onClickedScrollButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
    
        switch (_SelectTimeTypeFromPopUp)
        {
            case POPUP_TIME_TYPE_SPECIFIC_DATE:
                NSLog(@"POPUP_TIME_TYPE_SPECIFIC_DATE >>");
                if(i == 1 || i ==2)
                {
                    NSLog(@"setting color >>");
                   
                    button.enabled = NO;
                    button.alpha = BUTTON_DISABLE_ALPHA;
                }
                if(i == 0)
                {
                   
                   // NSString *tagstr = [NSString stringWithFormat:@"%ld",button.tag];
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                     button.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
                    [_RepeatButtonActionsList addObject:@"0"];
                    
                     button.enabled = NO;
                    //button.alpha = BUTTON_DISABLE_ALPHA;
                }
                
                break;
                
            case POPUP_TIME_TYPE_FULL_DAY:
                NSLog(@"POPUP_TIME_TYPE_FULL_DAY >>");
                break;
                
                
                
        }
        
        
        [_SubScrollView addSubview:button];

        x += button.frame.size.width+10;
        
        [_BottomButtonArray addObject:button];
        
        /*
        switch (_SCREENKEY) {
            case TIME_TYPE_SPECIF_DATE:
                switch (_RepeatButtonProperty) {
                    case TIME_REPEAT_DISABLE_MONTHLY:
                        if(i == 1)
                        {
                            button.enabled = NO;
                        }
                        break;
                    
                    case TIME_REPEAT_DISABLE_ALL:
                        if(i == 1 || i ==2)
                        {
                            button.enabled = NO;
                        }
                        break;
                        
                    default:
                        break;
                }
                break;
                
            default:
                break;
        }
         */
       
    }
    
        
    
    _SubScrollView.contentSize = CGSizeMake(x, _SubScrollView.frame.size.height);
    
    [_MainScrollView bringSubviewToFront:_SubScrollView];
    
}
-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _InfoLabel.text = NSLocalizedStringFromTableInBundle(@"SELECT WHEN YOU WANT TO PLAY IT",nil,appDelegate.LocalBundel,nil);
}



- (IBAction)onClickedConfirmButton:(id)sender {
    
    NSLog(@"SettingsTV::onClickedConfirmButton >>>");
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![self checkConditions])
    {
        NSLog(@"Empty fields");
        //_AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        //_AlertLabel.hidden=NO;
        return;
    }
    
    _MainSettingsVC.onClickedConfirmFromSettingsPopUP = TRUE;
    
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_FULL_DAY:
            _MainSettingsVC.TimeSettingsType = SETTINGS_WEEKLY_TIME;
            [self setFullDaySettings];
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
            _MainSettingsVC.TimeSettingsType = SETTINGS_SPECIFIC_TIME;
            // _MainSettingsVC.TimeSettingsType = SETTINGS_WEEKLY_TIME;
            [self setWeeklySettings];
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
        {
            _MainSettingsVC.BelowCenterBottomLabel.text = _StartBottomLabel.text;
            _MainSettingsVC.BelowRightBottomLabel.text = _EndBottomLabel.text;
            
            NSLog(@"RepeatButtonActionsList = %@", _RepeatButtonActionsList);
            for(int i=0;i<_RepeatButtonActionsList.count;i++)
            {
                NSString *temp = [_RepeatButtonActionsList objectAtIndex:i];
                if([temp isEqualToString:@"0"])
                {
                    NSLog(@"SETTINGS_BETWEEN_DATES");
                    _MainSettingsVC.TimeSettingsType = SETTINGS_BETWEEN_DATES;
                    [self setBetweenDateSettings];
                }
                
                if([temp isEqualToString:@"1"])
                {
                    NSLog(@"SETTINGS_MONTLY_TIME");
                    _MainSettingsVC.TimeSettingsType = SETTINGS_MONTLY_TIME;
                    [self setMonthlyTimeSettings];
                }
                
                if([temp isEqualToString:@"2"])
                {
                     NSLog(@"SETTINGS_YEARLY_TIME");
                    _MainSettingsVC.TimeSettingsType = SETTINGS_YEARLY_TIME;
                    [self setYearlyTimeSettings];
                }
            }
            break;
        }
        default:
            NSLog(@"SelectTimeTypeFromPopUp invalid type");
            
    }
    _MainSettingsVC.ScreenType = END_SETTINGS_SCREEN;
    [_MainSettingsVC.navigationController popViewControllerAnimated:NO];
    
    
}



- (BOOL) checkConditions
{
    NSLog(@"checkConditions");
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_FULL_DAY:
            return YES;
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
            if([_StartBottomLabel.text isEqualToString:@""])
            {
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                 _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
                [SIXDStylist setTextViewAlert:nil :_StartView :nil];
                return NO;
            
            }
            else if([_EndBottomLabel.text isEqualToString:@""])
            {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                 _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
                [SIXDStylist setTextViewAlert:nil :_EndView :nil];
            }
                else
                return YES;
            break;
    }
    return NO;
}

/*
- (void) setEmptyFieldAlert
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
    //_AlertLabel.hidden=NO;
    [SIXDStylist setTextViewAlert:_NumberTextField :_NumberView :nil];
}

- (void) clearAlert
{
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_NumberTextField :_NumberView :nil];
}
 */

- (void) setWeeklySettings
{
    NSLog(@"setWeeklySettings >>");
    
    NSString *weeklyDays = @"";
    int weekDayInt;
    if(_RepeatButtonActionsList.count <=0)
    {
        weeklyDays = @"1,2,3,4,5,6,7";
    }
    else
    {
        for(int i=0;i<_RepeatButtonActionsList.count;i++)
        {
            weekDayInt = [[_RepeatButtonActionsList objectAtIndex:i] intValue];
            weekDayInt ++;
            weeklyDays = [weeklyDays stringByAppendingString: [NSString stringWithFormat:@"%d,",weekDayInt]];
        }
        
        if ([weeklyDays length] > 0) {
            weeklyDays = [weeklyDays substringToIndex:[weeklyDays length] - 1];
        }
    }
    
    [_MainSettingsVC.SettingsDataDict setObject:_StartBottomLabel.text forKey:@"weeklyStartTime"];
    [_MainSettingsVC.SettingsDataDict setObject:_EndBottomLabel.text forKey:@"weeklyEndTime"];
    [_MainSettingsVC.SettingsDataDict setObject:weeklyDays forKey:@"weeklyDays"];
    
    NSLog(@"WeekDays Selected = %@", weeklyDays);
}

- (void) setFullDaySettings
{
    NSLog(@"setFullDaySettings >>");
    
    NSString *weeklyDays = @"";
    int weekDayInt;
    
    if(_RepeatButtonActionsList.count <=0)
    {
        weeklyDays = @"1,2,3,4,5,6,7";
    }
    else
    {
        for(int i=0;i<_RepeatButtonActionsList.count;i++)
        {
            weekDayInt = [[_RepeatButtonActionsList objectAtIndex:i] intValue];
            weekDayInt ++;
            weeklyDays = [weeklyDays stringByAppendingString: [NSString stringWithFormat:@"%d,",weekDayInt]];
        }
        
        if ([weeklyDays length] > 0) {
            weeklyDays = [weeklyDays substringToIndex:[weeklyDays length] - 1];
        }
    }
    [_MainSettingsVC.SettingsDataDict setObject:@"00:00" forKey:@"weeklyStartTime"];
    
    [_MainSettingsVC.SettingsDataDict setObject:@"23:59" forKey:@"weeklyEndTime"];
    
    [_MainSettingsVC.SettingsDataDict setObject:weeklyDays forKey:@"weeklyDays"];
    
    NSLog(@"MainSettingsVC.SettingsDataDict = %@", _MainSettingsVC.SettingsDataDict);
}

- (void) setBetweenDateSettings
{
    NSLog(@"setBetweenDateSettings >>");
    
    NSString *str = [_StartBottomLabel.text substringWithRange:NSMakeRange(6, 4)];
    str = [str stringByAppendingString:[_StartBottomLabel.text substringWithRange:NSMakeRange(2, 4)]];
    str = [str stringByAppendingString:[_StartBottomLabel.text substringWithRange:NSMakeRange(0, 2)]];
    [_MainSettingsVC.SettingsDataDict setObject: str forKey:@"customizeStartDate"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(6, 4)];
    str = [str stringByAppendingString:[_EndBottomLabel.text substringWithRange:NSMakeRange(2, 4)]];
    str = [str stringByAppendingString:[_EndBottomLabel.text substringWithRange:NSMakeRange(0, 2)]];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"customizeEndDate"];
    
    str = @"";
    str = [_StartBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
   [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"monthlyStartTime"];
   
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"monthlyEndTime"];
    
    NSLog(@"Settings Selected = %@", _MainSettingsVC.SettingsDataDict);
}

- (void) setMonthlyTimeSettings
{
    NSLog(@"setMonthlyTimeSettings >>");
    
    NSString *str = [_StartBottomLabel.text substringWithRange:NSMakeRange(0, 2)];
    [_MainSettingsVC.SettingsDataDict setObject: str forKey:@"startDayMonthly"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(0, 2)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"endDayMonthly"];
    
    str = @"";
    str = [_StartBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"monthlyStartTime"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"monthlyEndTime"];
    
     NSLog(@"Settings Selected = %@", _MainSettingsVC.SettingsDataDict);
}

- (void) setYearlyTimeSettings
{
    NSLog(@"setYearlyTimeSettings >>");
    
    NSString *str = [_StartBottomLabel.text substringWithRange:NSMakeRange(3, 2)];
    [_MainSettingsVC.SettingsDataDict setObject: str forKey:@"yearlyStartMonth"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(3, 2)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"yearlyEndMonth"];
    
    str = @"";
    str = [_StartBottomLabel.text substringWithRange:NSMakeRange(0, 2)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"yearlyStartDay"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(0, 2)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"yearlyEndDay"];
    
    str = @"";
    str = [_StartBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"yearlyStartTime"];
    
    str = @"";
    str = [_EndBottomLabel.text substringWithRange:NSMakeRange(11, 5)];
    [_MainSettingsVC.SettingsDataDict setObject:str forKey:@"yearlyEndTime"];
    
    NSLog(@"Settings Selected = %@", _MainSettingsVC.SettingsDataDict);
}


- (void)handleStartViewTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleStartViewTap");
    
    //UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    
        _AlertLabel.hidden=YES;
        [SIXDStylist setTextViewNormal:nil :_StartView :nil];
    
       
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
        {
            NSLog(@"POPUP_TIME_TYPE_SPECIFIC_TIME >>");
           // SIXDTimePickerPopUP *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTimePickerPopUP"];
            _TimePickerPopUP.TimeVC =self;
            [_TimePickerPopUP.SegmentTab setSelectedSegmentIndex:0];
            _TimePickerPopUP.typeFlag=0;
            [self presentViewController:_TimePickerPopUP animated:YES completion:nil];
        }
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
        {
            NSLog(@"POPUP_TIME_TYPE_SPECIFIC_DATE >>");
            //SIXDDatePickerPopUpVC *ViewController  = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
           // ViewController.TimeVC =self;
            [_DatePickerPopUpVC.SegmentTab setSelectedSegmentIndex:0];
            _DatePickerPopUpVC.typeFlag=0;
            [self presentViewController:_DatePickerPopUpVC animated:YES completion:nil];
        }
            break;
    }

    
}

- (void)handleEndViewTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleEndViewTap");
   
    //UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];

    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:nil :_EndView :nil];
    
    switch (_SelectTimeTypeFromPopUp)
    {
        case POPUP_TIME_TYPE_SPECIFIC_TIME:
        {
            NSLog(@"POPUP_TIME_TYPE_SPECIFIC_TIME >>");
           // SIXDTimePickerPopUP *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTimePickerPopUP"];
            _TimePickerPopUP.TimeVC =self;
            [_TimePickerPopUP.SegmentTab setSelectedSegmentIndex:1];
            _TimePickerPopUP.typeFlag=1;
            [self presentViewController:_TimePickerPopUP animated:YES completion:nil];
        }
            break;
            
        case POPUP_TIME_TYPE_SPECIFIC_DATE:
        {
            NSLog(@"POPUP_TIME_TYPE_SPECIFIC_DATE >>");
           // SIXDDatePickerPopUpVC *ViewController  = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
           // _DatePickerPopUpVC.TimeVC =self;
            [_DatePickerPopUpVC.SegmentTab setSelectedSegmentIndex:1];
            _DatePickerPopUpVC.typeFlag=1;
            [self presentViewController:_DatePickerPopUpVC animated:YES completion:nil];
        }
            break;
    }
    
    
    
}

- (void)handleTimeTypeViewTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleTimeTypeViewTap");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    SIXDPickerPopUpNormal *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPickerPopUpNormal"];
    ViewController.TimeVC =self;
    ViewController.SOURCE_TYPE=FOR_TIME;
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    NSLog(@"responseMap = %@",responseMap);
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"onFailureResponseRecived = %@",Response);
    NSString *message=[Response objectForKey:@"message"];
    
    [SIXDGlobalViewController setAlert:message :self];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"onFailedToConnectToServer");
    [SIXDGlobalViewController setAlert:@"Please check your network connectivity" :self];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
