//
//  SIXDChangeProfileDetailsVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/23/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDChangeProfileDetailsVC.h"
#import "AppDelegate.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"
#import "CustomLabel.h"
#import "SIXDProfileVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDValidations.h"



@interface SIXDChangeProfileDetailsVC ()

@end

@implementation SIXDChangeProfileDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.categorySubscription = [[CategorySubscription alloc]init];
    [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.95]];
    _TitleLabel.text = self.title;
    _AlertLabel.hidden = true;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"UPDATE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    if([_ServiceType isEqualToString:UPDATE_CATAGORIES])
    {
        [SIXDStylist setGAScreenTracker:@"Edit_Profile_PreferedCategory"];
       
        _SubButtonList = [NSMutableDictionary new];
        _SelectedCategoryIDs = [NSMutableArray new];
        NSArray *Temp =  [_OldValue componentsSeparatedByString:@","];
        for (int i=0; i<Temp.count; i++)
        {
            [_SelectedCategoryIDs addObject:[Temp objectAtIndex:i]];
        }
        
        _SubView.layer.borderWidth = 1.0f;
        _SubView.layer.borderColor = [UIColorFromRGB(CLR_DISABLE_TEXTFIELD) CGColor];
        _SubView.backgroundColor = [UIColor whiteColor];
        
        _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"Preferences*",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden = false;
       _AlertLabel.textColor = UIColorFromRGB(CLR_TEXTFIELDLABEL);
        
       
        int x = 0;
        for (int i = 0; i <appDelegate.CategoryList.count ; i++)
        {
            
            UIImageView *MainButton = [[UIImageView alloc] initWithFrame:CGRectMake(x,3,60,60)];
            UIButton *SubButton = [[UIButton alloc] initWithFrame:CGRectMake(x,3,60,30)];
            CustomLabel_7 *buttonlabel = [[CustomLabel_7 alloc] initWithFrame:CGRectMake(x,30,60,20)];
            
            SubButton.userInteractionEnabled = FALSE;
            
            [SubButton setImage:[UIImage imageNamed:@"preference_uncheck"] forState:UIControlStateNormal];
            
            NSMutableDictionary *Dict = [appDelegate.CategoryList objectAtIndex:i];
            
            NSString * urlStr = [[Dict objectForKey:@"imageURL"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            [MainButton sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"Default.png"]];
            
            
            MainButton.tag = [[Dict objectForKey:@"id"] intValue];
            buttonlabel.text=NSLocalizedStringFromTableInBundle([Dict objectForKey:@"name"],nil,appDelegate.LocalBundel,nil);
            buttonlabel.textAlignment=NSTextAlignmentCenter;
            buttonlabel.textColor = [UIColor whiteColor];
            
            [_SubButtonList setObject:SubButton forKey:[Dict objectForKey:@"id"]];
            
           // [MainButton addTarget:self action:@selector(onClickedScrollButton:) forControlEvents:UIControlEventTouchUpInside];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(onClickedScrollButton:)];
            [MainButton setUserInteractionEnabled:true];
            [MainButton addGestureRecognizer:tap];

            
            [_ScrollView addSubview:MainButton];
            [_ScrollView addSubview:SubButton];
            [_ScrollView addSubview:buttonlabel];
            x += MainButton.frame.size.width+5;
            
            
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = MainButton.bounds;
            gradient.colors = @[(id)[UIColorFromRGB(CLR_BLACK) CGColor], (id)[UIColorFromRGB(CLR_BLACK) CGColor]];
            gradient.startPoint = CGPointMake(1, 1);
            gradient.endPoint = CGPointMake(1, 1);
            gradient.opacity = 0.6;
            
            [MainButton.layer insertSublayer:gradient atIndex:0];
            [MainButton bringSubviewToFront:SubButton];
            [MainButton bringSubviewToFront:buttonlabel];
            
        }
        
        for(int i=0;i<_SelectedCategoryIDs.count;i++)
        {
            UIButton *SubTemp = [_SubButtonList objectForKey:[_SelectedCategoryIDs objectAtIndex:i]];
            if(SubTemp)
            [SubTemp setImage:[UIImage imageNamed:@"preference_check"] forState:UIControlStateNormal];
            
        }
        
        _ScrollView.contentSize = CGSizeMake(x, _ScrollView.frame.size.height);
    }
    
    if([_ServiceType isEqualToString:@"UPDATE_USER_NAME"])
    {
        [SIXDStylist setGAScreenTracker:@"Edit_Profile_Username"];
    }
    else if([_ServiceType isEqualToString:@"UPDATE_EMAIL"])
    {
        [SIXDStylist setGAScreenTracker:@"Edit_Profile_Email"];
    }
    
        
}

- (void)onClickedScrollButton:(UITapGestureRecognizer*)sender
    {
        NSLog(@"sender = %@",sender);
        UIImageView *temp= (UIImageView *)sender.view;
        
        NSLog(@"onClickedScrollButton = %ld",(long)temp.tag);
        
        BOOL IsSelected = true;
        
        for(int i=0;i<_SelectedCategoryIDs.count;i++)
        {
            if(temp.tag==[[_SelectedCategoryIDs objectAtIndex:i] intValue])
            {
                UIButton *SubTemp = [_SubButtonList objectForKey:[_SelectedCategoryIDs objectAtIndex:i]];
                [SubTemp setImage:[UIImage imageNamed:@"preference_uncheck"] forState:UIControlStateNormal];
                IsSelected = false;
                [_SelectedCategoryIDs removeObjectAtIndex:i];
                [self.categorySubscription unsubscribeCategoryWithId:(long)temp.tag];
                break;
            }
        }
        
        if(IsSelected)
        {
            NSString *tagValue = [NSString stringWithFormat:@"%ld",(long)temp.tag];
            UIButton *SubTemp = [_SubButtonList objectForKey:tagValue];
            [SubTemp setImage:[UIImage imageNamed:@"preference_check"] forState:UIControlStateNormal];
            [_SelectedCategoryIDs addObject:tagValue];
            
        }
        
}
    
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder ];
    return NO;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    [_SelectedTextField resignFirstResponder];
}

- (IBAction)onSelectedConfirmButton:(UIButton *)sender
{
    [_SelectedTextField resignFirstResponder];
    
    if(_ShouldDismissView)
    {
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    
    if(![_ServiceType isEqualToString:UPDATE_CATAGORIES])
    {
        if([self validateTextFieldDetails])
        {
            [_ActivityIndicator startAnimating];
            [_ConfirmButton setUserInteractionEnabled:false];
            _ConfirmButton.alpha = 0.3;
            [self editProfile];
        }
    }
    else
    {
        if(_SelectedCategoryIDs.count != 0)
        {
            [_ActivityIndicator startAnimating];
            [_ConfirmButton setUserInteractionEnabled:false];
            _ConfirmButton.alpha = 0.3;
            [self editProfile];
        }
    }
    
}
- (IBAction)onSelectedCloseButton:(UIButton *)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
   
    [self changeCellColor:false:1];
    [_AlertLabel setHidden:true];
    
    _SelectedTextField = textField;
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    cell.TitleLabel.hidden=NO;
    [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    cell.TopSpaceToTextField.constant=19;
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if(textField.text.length==0)
    {
        NSString *string = [_PlaceHolderList objectAtIndex:textField.tag];
        [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(string,nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        cell.TitleLabel.hidden=YES;
        cell.TopSpaceToTextField.constant=0;
    }
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _TitleLabel.text = NSLocalizedStringFromTableInBundle(self.title,nil,appDelegate.LocalBundel,nil);
    
    _AlertLabel.text = @"";

    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"UPDATE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.FirstTextField.tag=indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    

    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle([_PlaceHolderList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil);
    
    [cell.FirstTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];

    if(indexPath.row == 1)
    {
        _SelectedTextField = cell.FirstTextField;
        cell.SubView.layer.borderWidth = 1.0f;
        cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
        [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_PlaceHolderList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        cell.TitleLabel.hidden = YES;
        cell.FirstTextField.delegate = self;
        cell.FirstTextField.enablesReturnKeyAutomatically = NO;
        UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
        [cell.AccessoryButton setHidden:true];
        
    }
    else
    {
        cell.SubView.layer.borderWidth = 1.0f;
        cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
        cell.SubView.backgroundColor = UIColorFromRGB(CLR_ATHENS_GRAY);
        cell.SubView.alpha=0.95;
        cell.FirstTextField.text = _OldValue;
        cell.FirstTextField.userInteractionEnabled = false;
        [SIXDStylist setTextViewNormal:cell.FirstTextField :nil :nil];
        cell.TopSpaceToTextField.constant=19;
         [cell.AccessoryButton setHidden:true];
       
    }

   
    return cell;
    
}


-(void)editProfile
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= REFERRAL;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];

    
    /*
    For userName:UPDATE_USER_NAME
    Categories: UPDATE_CATAGORIES
    Email : UPDATE_EMAIL
    Profilepic : UPDATE_PROFILE_PIC
*/
   /* name or
    categoryId or
    email or
    previewImage
    */
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
     ReqHandler.HttpsReqType = POST;
    
    if([_ServiceType isEqualToString:@"UPDATE_USER_NAME"])
    {
        ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateUserName&language=%@&msisdn=%@&servType=%@&name=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,_ServiceType,_SelectedTextField.text];
        //ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateUserName&language=%@&servType=%@&name=%@",Rno,appDelegate.Language,_ServiceType,_SelectedTextField.text];
    }
    else if ([_ServiceType isEqualToString:@"UPDATE_CATAGORIES"])
    {
        NSString *FinalIds = @"";
        for(int i=0;i<_SelectedCategoryIDs.count;i++)
        {
            FinalIds = [FinalIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[_SelectedCategoryIDs objectAtIndex:i]]];
        }
        if ([FinalIds length] > 0) {
            FinalIds = [FinalIds substringToIndex:[FinalIds length] - 1];
        }
        ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateCategories&language=%@&msisdn=%@&servType=%@&categoryId=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,_ServiceType,FinalIds];
        //ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateCategories&language=%@&servType=%@&categoryId=%@",Rno,appDelegate.Language,_ServiceType,FinalIds];
        
    }
    else
    {
        ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateEmail&language=%@&msisdn=%@&servType=%@&email=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,_ServiceType,_SelectedTextField.text];
       // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=UpdateEmail&language=%@&servType=%@&email=%@",Rno,appDelegate.Language,_ServiceType,_SelectedTextField.text];
    }
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/edit-profile",appDelegate.BaseMiddlewareURL];
    
   
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    
    if([_ServiceType isEqualToString:@"UPDATE_USER_NAME"])
    {
        [_ProfileVC.ProfileDetails removeObjectForKey:@"User Name*"];
        [_ProfileVC.ProfileDetails setObject:_SelectedTextField.text forKey:@"User Name*"];
        
    }
    else if ([_ServiceType isEqualToString:@"UPDATE_CATAGORIES"])
    {
        [_ProfileVC.ProfileDetails removeObjectForKey:@"categories"];
        NSString *FinalIds = @"";
        for(int i=0;i<_SelectedCategoryIDs.count;i++)
        {
            FinalIds = [FinalIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[_SelectedCategoryIDs objectAtIndex:i]]];
        }
        if ([FinalIds length] > 0) {
            FinalIds = [FinalIds substringToIndex:[FinalIds length] - 1];
        }
        
        [_ProfileVC.ProfileDetails setObject:FinalIds forKey:@"categories"];
        
    }
    else
    {
       [_ProfileVC.ProfileDetails removeObjectForKey:@"User Email*"];
        [_ProfileVC.ProfileDetails setObject:_SelectedTextField.text forKey:@"Email*"];
    }
    
    [_ProfileVC.TableView reloadData];
    StoreManager.shared.isCategorySubscribed = false;
    
    [self.categorySubscription getProfileDetail];
    
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    
    _AlertLabel.text =NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    _AlertLabel.textAlignment = NSTextAlignmentCenter;
    [_AlertLabel setHidden:false];
    _ShouldDismissView =true;
     [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
}

-(void)onFailedToConnectToServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
    
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    
    _AlertLabel.text =[NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil)];
    _AlertLabel.textAlignment = NSTextAlignmentCenter;

    _ShouldDismissView =true;
     [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];

}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(BOOL)validateTextFieldDetails
{
    BOOL Status = TRUE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
        if(_SelectedTextField.text.length <= 0)
        {
            [self changeCellColor:true:1];
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
            Status = false;
    
        }
        else
        {
            if([_ServiceType isEqualToString:@"UPDATE_USER_NAME"])
            {
                Status = [GlobalInterfaceForValidations fnG_ValidateNames:_SelectedTextField.text];
                if(Status == FALSE)
                {
                    [self changeCellColor:true:1];
                    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"INVALID_NAME_CHANGE_PROFILE",nil,appDelegate.LocalBundel,nil);
                    [_AlertLabel setHidden:false];
                }
            }
            else
            {
                Status = [GlobalInterfaceForValidations fn_isValidEmail:_SelectedTextField.text];
                if(Status == FALSE)
                {
                    [self changeCellColor:true:1];
                    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"INVALID_EMAIL_ID_CHANGE_PROFILE",nil,appDelegate.LocalBundel,nil);
                    [_AlertLabel setHidden:false];
                }
            }
            
            
        }
    
    return Status;
    
}


-(void)changeCellColor:(BOOL)Error :(int)index
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:index inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if(Error)
    {
        [SIXDStylist setTextViewAlert:cell.FirstTextField :cell.SubView :cell.AccessoryButton];
         [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_PlaceHolderList objectAtIndex:cell.AccessoryButton.tag],nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        
    }
    else
    {
        
        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        [SIXDStylist setTextViewNormal:cell.FirstTextField :cell.SubView :nil];
         [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_PlaceHolderList objectAtIndex:cell.AccessoryButton.tag],nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
