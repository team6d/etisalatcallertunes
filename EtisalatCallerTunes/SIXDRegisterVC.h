//
//  SIXDRegisterVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDRegisterVC : ViewController<UITextFieldDelegate,UIWebViewDelegate,NSURLSessionDelegate>
@property (weak, nonatomic) IBOutlet UIButton *BackButton;

@property (weak, nonatomic) IBOutlet UIView *NumberView;
@property (weak, nonatomic) IBOutlet UIView *PasswordView;
@property (weak, nonatomic) IBOutlet UIView *ConfirmPasswordView;
@property (weak, nonatomic) IBOutlet UIView *NameView;
@property (weak, nonatomic) IBOutlet UIView *EmailView;
- (IBAction)onClickBackButton:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *NumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ConfirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UITextField *EmailTextField;

@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *PasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *ConfirmPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *EmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;

@property (weak, nonatomic) IBOutlet UIButton *GetOtpButton;
@property (weak, nonatomic) IBOutlet UIButton *NumberButton;
@property (weak, nonatomic) IBOutlet UIButton *PasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *NameButton;
@property (weak, nonatomic) IBOutlet UIButton *EmailButton;


@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;

- (IBAction)onClickedGetOtpButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToNumber;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToPassword;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToConfirmPassword;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToEmail;


@property (weak, nonatomic) IBOutlet UILabel *ItemInfoLabel;

@property(nonatomic) BOOL ValidationFlag;

@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *securityCounter;
@property (nonatomic) NSString *EncryptedPassword;
@property (strong, nonatomic) NSString *SectionToken;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

//scrolling buttons
@property(strong,nonatomic) NSMutableDictionary *SubButtonList;
@property(strong,nonatomic) NSMutableArray *SelectedCategoryIDs;


//turning red categories
//@property (strong, nonatomic) UIButton *MainButton;
@property(strong,nonatomic) NSMutableArray *MainButtonList;
@property(strong,nonatomic) NSMutableDictionary *SelectedCategoryNames;



@end
