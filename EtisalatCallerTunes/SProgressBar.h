//
//  SProgressBar.h

//
//  Created by Shahnawaz Nazir on 05/02/18.
//  Copyright © 2018 Shahnawaz Nazir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SProgressBar : UIView

@property (nonatomic) double percent;
@property (weak, nonatomic) IBOutlet UIButton *RecordButton;

@end


