//
//  SIXDTimePickerPopUP.m
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 07/03/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDTimePickerPopUP.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDStylist.h"

#define START_DATE_SEGMENT  0
#define END_DATE_SEGMENT    1

@interface SIXDTimePickerPopUP ()

@end

@implementation SIXDTimePickerPopUP

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    
    [SIXDStylist setGAScreenTracker:@"Tune_Settings_SetSpecificTime"];
    
    [_SubView setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
   // _StartTimePicker.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"ar_AE"];
    
    _StartTimePicker.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_GB"];
    _EndTimePicker.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_GB"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_SaveButton setTitle:NSLocalizedStringFromTableInBundle(@"SAVE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    _AlertLabel.hidden = YES;
    
    [self setSegmentTabColor];
    
    if(_typeFlag ==START_DATE_SEGMENT)
    {
        NSLog(@"switch0");
        _typeFlag =  START_DATE_SEGMENT;
        // _DatePicker.hidden=NO;
        //_EndDatePicker.hidden=YES;
        // [self.view bringSubviewToFront:_DatePicker];
        // [_TextField setInputView:_DatePicker];
 
        _StartTimePicker.hidden = NO;
        _EndTimePicker.hidden =YES;
        
        [self.view bringSubviewToFront:_StartTimePicker];
 
    }
    
    else {
        NSLog(@"switch1");
        _typeFlag = END_DATE_SEGMENT;
        //  _DatePicker.hidden=YES;
        // _EndDatePicker.hidden=NO;
        //[self.view bringSubviewToFront:_EndDatePicker];
        //[_TextField setInputView:_EndDatePicker];

        _StartTimePicker.hidden = YES;
        _EndTimePicker.hidden =NO;
        [self.view bringSubviewToFront:_EndTimePicker];
        // [_TextField setInputView:_EndDatePicker];
        }
    
     [_StartTimePicker setValue:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:@"textColor"];
     [_EndTimePicker setValue:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:@"textColor"];

    // Do any additional setup after loading the view.
}


-(void)setSegmentTabColor
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                nil];
    [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
      
    }
    else
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"GESSTextLight-Light" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
    }
    */
    
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:NSForegroundColorAttributeName];
    [_SegmentTab setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
   
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"START",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"END",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    
    [_SegmentTab setNeedsUpdateConstraints];
    [_SegmentTab layoutIfNeeded];
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    
    Temp = [array objectAtIndex:0];
    UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 4)];
    
    
    Temp = [array objectAtIndex:1];
    UILabel *Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 4)];

    UILabel *Border = [[UILabel alloc] initWithFrame:CGRectMake(25,120,[UIScreen mainScreen].bounds.size.width-50 ,0.6)];
    
    Border.backgroundColor = UIColorFromRGB(CLR_BUY);
    [_SubView addSubview:Border];
    
    
        _SecondLabel = Line;
        [_SegmentTab insertSubview:Line aboveSubview:Temp];
        
        _FirstLabel = Line1;
        [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
    
    
    
    _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissViewControllerMain
{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickedCloseButton:(id)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickedSaveButton:(id)sender
{
    NSLog(@"SIXDTimePickerPopup:: onClickedSaveButton >>>");
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if((_StartTimePicker.date == NULL) || (_EndTimePicker.date == NULL))
    {
        NSString *err;
        if(_StartTimePicker.date == NULL)
            err = @"*please select a valid start time";
        
       else if(_EndTimePicker.date == NULL)
            err = @"*please select a valid end time";
        
        NSLog(@"NULL TIME");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(err,nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        return;
    }
    
    BOOL timeDiffStatus = [self remaningTime : _StartTimePicker.date : _EndTimePicker.date];
    NSLog(@"fromTime =%@ endTime = %@ timeDiff = %d",_StartTimePicker.date,_EndTimePicker.date, timeDiffStatus);
    
    if(!timeDiffStatus)
    {
        NSLog(@"INVALID TIME");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"*please select a valid start time",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        return;
    }
    
     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    
    NSString *BigTimeString = [dateFormat stringFromDate:_StartTimePicker.date];
    _TimeVC.StartBottomLabel.text = BigTimeString;
   
    BigTimeString = [dateFormat stringFromDate:_EndTimePicker.date];
    _TimeVC.EndBottomLabel.text = BigTimeString;

    NSLog(@"BigTimeString = %@",BigTimeString);
    _TimeVC.Source = SRC_TIME_PICKER;
    
    [_TimeVC viewWillAppear:false];
    
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    _AlertLabel.hidden = YES;

    if(_typeFlag ==START_DATE_SEGMENT)
    {
        NSLog(@"switch0");
        _typeFlag =  START_DATE_SEGMENT;
        // _DatePicker.hidden=NO;
        //_EndDatePicker.hidden=YES;
        // [self.view bringSubviewToFront:_DatePicker];
        // [_TextField setInputView:_DatePicker];
        
        _StartTimePicker.hidden = NO;
        _EndTimePicker.hidden =YES;
        
        _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
        
        
        [self.view bringSubviewToFront:_StartTimePicker];
        
    }
    
    else {
        NSLog(@"switch1");
        _typeFlag = END_DATE_SEGMENT;
        //  _DatePicker.hidden=YES;
        // _EndDatePicker.hidden=NO;
        //[self.view bringSubviewToFront:_EndDatePicker];
        //[_TextField setInputView:_EndDatePicker];
        
        _StartTimePicker.hidden = YES;
        _EndTimePicker.hidden =NO;
        _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
        _SecondLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        [self.view bringSubviewToFront:_EndTimePicker];
        // [_TextField setInputView:_EndDatePicker];
    }
}

-(IBAction)onSegmentValueChanged:(UISegmentedControl *)sender
{
    switch ([sender selectedSegmentIndex])
    {
        case 0:
            
            NSLog(@"switch0");
            _typeFlag =  START_DATE_SEGMENT;
            // _DatePicker.hidden=NO;
            //_EndDatePicker.hidden=YES;
           // [self.view bringSubviewToFront:_DatePicker];
           // [_TextField setInputView:_DatePicker];
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            
            _StartTimePicker.hidden = NO;
            _EndTimePicker.hidden =YES;
            
           
            [self.view bringSubviewToFront:_StartTimePicker];
   
            
            break;
            
        case 1:
            NSLog(@"switch1");
            _typeFlag = END_DATE_SEGMENT;
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
            //  _DatePicker.hidden=YES;
            // _EndDatePicker.hidden=NO;
            //[self.view bringSubviewToFront:_EndDatePicker];
            //[_TextField setInputView:_EndDatePicker];
            
            _StartTimePicker.hidden = YES;
            _EndTimePicker.hidden =NO;
            [self.view bringSubviewToFront:_EndTimePicker];
           // [_TextField setInputView:_EndDatePicker];
      
            break;
            
        default:
            NSLog(@"switch default statement for segment control");
            
    }
}

- (BOOL)remaningTime:(NSDate*)startDate :(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
   // NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    NSLog(@"days = %ld",days);
    NSLog(@"hour = %ld",hour);
    NSLog(@"minutes = %ld",minutes);
   // durationString = [NSString stringWithFormat:@"%ld",(long)hour];
    //return durationString;
    
    //if(days < 0)
      //  return NO;
    
     if(hour < 0)
        return NO;
    if(hour >0)
        return YES;
    if(minutes <= 0)
          return NO;
    
    return YES;
    
    /*
    
     if(days>0)
     {
   
         durationString=[NSString stringWithFormat:@"%ld",(long)days];
         return durationString;
     }
    
     if(hour>0)
     {

         durationString=[NSString stringWithFormat:@"%ld hour",(long)hour];
         return durationString;
     }
     if(minutes>0)
     {
  
         durationString = [NSString stringWithFormat:@"%ld minute",(long)minutes];
         return durationString;
     }
    
    return @"";
     */
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
}

@end
