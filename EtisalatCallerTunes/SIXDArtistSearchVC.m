//
//  SIXDArtistSearchVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDArtistSearchVC.h"
#import "SIXDGenericPlayList.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"

@interface SIXDArtistSearchVC ()

@end

@implementation SIXDArtistSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _PerPageCount = [[appDelegate.AppSettings objectForKey:@"FETCH_LIMIT"]intValue];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_TempResponseCount >= _PerPageCount)
        return _ArtistData.count+1;
    else
        return _ArtistData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(indexPath.row < _ArtistData.count)
    {
        NSMutableDictionary *Dict = [_ArtistData objectAtIndex:indexPath.row];
        NSString *string = [[Dict objectForKey:@"matchedParam"] capitalizedString];
        NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:string attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
        
        cell.TitleLabel.attributedText = titleString;
        cell.TitleLabel.hidden = false;
        /*
        if([appDelegate.Language isEqualToString:@"English"])
        {
            NSMutableDictionary *Dict = [_ArtistData objectAtIndex:indexPath.row];
            NSString *string = [[Dict objectForKey:@"matchedParam"] capitalizedString];
            NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:string attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
            
            cell.TitleLabel.attributedText = titleString;
            cell.TitleLabel.hidden = false;
        }
        else
        {
            NSMutableDictionary *Dict = [_ArtistData objectAtIndex:indexPath.row];
            NSString *string = [[Dict objectForKey:@"matchedParam"] capitalizedString];
            NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:string attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:10]}];
            
            cell.TitleLabel.attributedText = titleString;
            cell.TitleLabel.hidden = false;
        }
        */
        [cell.ActivityIndicator stopAnimating];
    }
    else
    {
        cell.TitleLabel.hidden = true;
        [cell.ActivityIndicator startAnimating];
        [cell.contentView bringSubviewToFront:cell.ActivityIndicator];
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SIXDStylist setGAScreenTracker:@"Search_Artist"];

    NSMutableDictionary *Dict = [_ArtistData objectAtIndex:indexPath.row];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDGenericPlayList *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGenericPlayList"];
    ViewController.IsSegmentTabRequired=TRUE;
    ViewController.searchKey=[Dict objectForKey:@"matchedParam"];
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(void)API_CDPSpecificLanguageSearch
{
    if(_SearchKey.length > 0)
    {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = CDP_SEARCH_NORMAL;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/specific-search-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=0&pageNo=%lu&perPageCount=20&toneId=0&sortBy=Order_By",URL,appDelegate.Language,(unsigned long)_PageNumber];
    
    [ReqHandler sendHttpGETRequest:self :_SearchKey];
    }
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"Response = %@",Response);
    
    Response = [Response objectForKey:@"responseMap"];
    
    
     NSMutableDictionary *countList = [Response objectForKey:@"countList"];
    
    NSMutableArray *TempPlaylist = [[countList objectForKey:@"artistDetailList"]mutableCopy];
    
    _TempResponseCount = TempPlaylist.count;
    _PageNumber += _TempResponseCount;
    
    _IsLoading = false;
    [_TableView setUserInteractionEnabled:true];
    
    [_TableView beginUpdates];
    
    if(TempPlaylist.count < _PerPageCount)
    {
        NSLog(@"Deleceted Row");
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_ArtistData.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    }
    
    for(int i=0;i<TempPlaylist.count;i++)
    {
        [_ArtistData addObject:[TempPlaylist objectAtIndex:i]];
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_ArtistData.count-1) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        [_TableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    }
    [_TableView endUpdates];
    
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    //TO DO
    /*
    if(_IsLoading == false)
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    }
     */
    
    if(_IsLoading)
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_ArtistData.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        _TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
        
    }
    
    /*
     NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count) inSection:0];
     NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
     [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
     */
    
}

-(void)onFailedToConnectToServer
{
    //TO DO
    /*
    if(_IsLoading == false)
    {
        [SIXDGlobalViewController showCommonFailureResponse:self :nil];
    }
     */
    if(_IsLoading)
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_ArtistData.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        _TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
        
    }
    /*
     NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count) inSection:0];
     NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
     [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
     */
}

- (void)onInternetBroken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == _ArtistData.count)
    {
        if(!_IsLoading)
        {
            if(_TempResponseCount >= _PerPageCount)
            {
                [_TableView setUserInteractionEnabled:false];
                _IsLoading = true;
                [self API_CDPSpecificLanguageSearch];
            
            }
        
    }
    }
    
}


@end
