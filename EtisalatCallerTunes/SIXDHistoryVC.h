//
//  SIXDHistoryVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 25/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDHistoryVC : ViewController
@property (weak, nonatomic) IBOutlet UITableView *DetailsTableView;
@property (weak, nonatomic) IBOutlet UITableView *TransactionTableView;
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (strong, nonatomic) NSString *dateString;
@property (strong, nonatomic) NSString *fromDateString;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property(strong,nonatomic)NSMutableArray *TransactionList;
@property(strong,nonatomic)NSMutableDictionary *DetailsList;
@property (weak, nonatomic) IBOutlet UILabel *ItemLabel;
@property (weak, nonatomic) IBOutlet UILabel *TransactionLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UIView *InfoView;

@end
