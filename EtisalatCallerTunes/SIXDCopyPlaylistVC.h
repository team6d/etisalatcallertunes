//
//  SIXDCopyPlaylistVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 13/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDCopyPlaylistVC : ViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(strong,nonatomic) NSMutableArray *Playlist_listToneApk;
//@property(nonatomic) int CopyCount;

- (IBAction)onClickedAccessoryButton:(UIButton *)sender;
- (IBAction)onClickedBuyButton:(UIButton *)sender;

@end
