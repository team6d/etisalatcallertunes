//
//  SIXDNestedTVCell.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDNestedTVCell.h"
#import "SIXDNestedCVCell.h"
#import "SIXDHeader.h"

@implementation SIXDNestedTVCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark -
#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   
    NSInteger NumberofItems = _TuneList.count/5;
    
    if(_TuneList.count%5 != 0)
    {
        NumberofItems++;
    }
    
    return NumberofItems;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"cellForItemAtIndexPath  NestedTVCell>>>>");
    
    SIXDNestedCVCell *cell = (SIXDNestedCVCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.TuneList = _TuneList;
    cell.section = indexPath.row;
    
    /*
    cell.CVContentList = [NSMutableArray new];
    for (int i = 0;i<3; i++)
    {
        if(_CopyCount < _TuneList.count)
        {
            [cell.CVContentList addObject:[_TuneList objectAtIndex:_CopyCount]];
            _CopyCount++;
        }
    }
    */
    
     [cell.TableView reloadData];
    
    return cell;
}

-(void)cellTapped
{
    NSLog(@"touched cell");
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //TO DO
    //play or pause song
    /*
    if(_Source == LISTTONEHORIZONTAL || _Source == LISTTONEVERTICAL)
    {
        NSLog(@"didSelectItemAtIndexPath SIXDListTonesVC >>");
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
    else
    {
        //TEMP to test wishlist
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDWishlistVC"];
        
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
     */
}


#pragma mark UICollectionViewFlowLayoutDelegate
/*
 -(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
 {
 
 return CGSizeMake(collectionView.frame.size.width/3-1,collectionView.frame.size.height );
 
 // return image.size;
 }
 */
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue>>>>");
}


- (IBAction)onSeletedAccessoryButton:(UIButton *)sender
{
    
}
- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    
}

- (IBAction)onSelectedPlayButton:(id)sender
{
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
        //return CGSizeMake(_CollectionView.frame.size.width-38,201);
        return CGSizeMake(_CollectionView.frame.size.width-38,335);
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    
    NSLog(@"scrollViewDidEndDragging>>> velocity = %f",velocity.x);
    
     CGFloat kMaxIndex = 4;
    // CGFloat kMaxIndex = 8;
    
    
    //working
    // CGFloat targetX = scrollView.contentOffset.x + velocity.x * 60.0;
    
    CGFloat targetX = scrollView.contentOffset.x + velocity.x * 160;
    
    
    //CGFloat targetIndex = round(targetX / (kCellWidth + kCellSpacing));
    CGFloat targetIndex = round(targetX / (_CollectionViewCellSize.width));
    if (targetIndex < 0)
        targetIndex = 0;
    if (targetIndex > kMaxIndex)
        targetIndex = kMaxIndex;
    
    targetContentOffset->x = targetIndex * (_CollectionViewCellSize.width);
    
}


@end
