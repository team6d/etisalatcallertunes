//
//  SIXDAlertViewVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 08/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
//#import "SIXDSignInVC.h"

@interface SIXDAlertViewVC : ViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *AlertButtonTop;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;
- (IBAction)onClickedAlertButton:(UIButton *)sender;


//pre-register screen
@property (weak, nonatomic) IBOutlet UIButton *CreateAccountButton;

@property (weak, nonatomic) IBOutlet UIView *NumberView;
@property (weak, nonatomic) IBOutlet UITextField *NumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;

- (IBAction)onClickedCreateAccountButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;
@property (weak, nonatomic) IBOutlet UIButton *BackButton;
- (IBAction)onClickBackButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *EncryptedPassword;


@end
