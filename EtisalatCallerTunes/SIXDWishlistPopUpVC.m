//
//  SIXDWishlistPopUpVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDWishlistPopUpVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"


@interface SIXDWishlistPopUpVC ()

@end

@implementation SIXDWishlistPopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"WISHLIST",nil,appDelegate.LocalBundel,nil);
    NSString *String1 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"artistName"]];
    _TopLabel.text = [String1 capitalizedString];
    NSString *String2 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"toneName"]];
    _BottomLabel.text = [String2 capitalizedString];
    
    if(appDelegate.DeviceSize == iPHONE_X)
    {
        _PopUpViewHeight.constant = _PopUpViewHeight.constant + 20;
    }
    
    if(_WishListType == 2)
    {
         _TopLabel.text = [_ToneDetails objectForKey:@"channelName"];
        if(_TopLabel.text.length <= 0)
        {
            _TopLabel.text = [_ToneDetails objectForKey:@"description"];
        }
    }
    
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    _AlertLabel.text = @"";
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _ConfirmButton.hidden = TRUE;
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
     NSLog(@"ToneDetails = %@",_ToneDetails);
    [_AlertLabel setHidden:YES];
    [self addToWishlistAPI];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickedCloseButton:(UIButton *)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    NSLog(@"click OK button");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
}

- (void) addToWishlistAPI
{
    [_ActivityIndicator startAnimating];
    [self.SubView bringSubviewToFront:_ActivityIndicator];
    
    NSLog(@"Inside addToWishlistAPI");
    NSLog(@"Wishlst tone details = %@", _ToneDetails);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    

    NSString *ToneURL = [_ToneDetails objectForKey:@"toneUrl"] ;
    if(ToneURL.length <= 0)
    {
        ToneURL = [_ToneDetails objectForKey:@"previewContent"] ;
    }
    
    NSRange match;
    match = [ToneURL rangeOfString:@"fileId="]; //length = 7 = match.length
    
    if(match.location!=NSNotFound)
    {
       ToneURL = [ToneURL substringWithRange:NSMakeRange(match.location+match.length,(ToneURL.length-(match.location+match.length)))];
    }
   
    NSString *ImageURL = [_ToneDetails objectForKey:@"previewImageUrl"] ;
    if(ImageURL.length <= 0)
    {
        ImageURL = [_ToneDetails objectForKey:@"previewImage"] ;
    }
    
    match = [ImageURL rangeOfString:@"fileId="]; //length = 7 = match.length
    
    if(match.location!=NSNotFound)
    {
        ImageURL = [ImageURL substringWithRange:NSMakeRange(match.location+match.length,(ImageURL.length-(match.location+match.length)))];
    }
    
    NSString *CategoryID;
    CategoryID =[NSString stringWithFormat:@"%@",[_ToneDetails objectForKey:@"categoryId"]];
    
    NSLog(@"category = %@ length = %ld", CategoryID, CategoryID.length);
    
    if([_ToneDetails objectForKey:@"categoryId"])
    {
       
        CategoryID =[NSString stringWithFormat:@"%@",[_ToneDetails objectForKey:@"categoryId"]];
    }
    
    else if([_ToneDetails objectForKey:@"category"])
    {
       
        CategoryID =[NSString stringWithFormat:@"%@",[_ToneDetails objectForKey:@"category"]];
    }
    else
        CategoryID = @"0";
        
   
    NSString *toneId;
    toneId =[_ToneDetails objectForKey:@"toneId"];
    if (toneId.length <= 0) {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    
    NSString *Artist;
    Artist =[_ToneDetails objectForKey:@"artistName"];
    if (Artist.length <= 0) {
        Artist = [_ToneDetails objectForKey:@"artist"];
    }
    
    NSString *ToneName;
    ToneName =[_ToneDetails objectForKey:@"toneName"];
    if (ToneName.length <= 0) {
        ToneName = [_ToneDetails objectForKey:@"channelName"];
        if (ToneName.length <= 0)
        {
            ToneName = [_ToneDetails objectForKey:@"description"];
        }
    }
    
    NSString *LikeCount;
    LikeCount =[_ToneDetails objectForKey:@"likeCount"];
    if (LikeCount.length <= 0)
    {
        LikeCount = @"0";
    }
    
    
    if(_IsSourceSalati)
    {
        ToneURL=@"";
        ImageURL=@"";
    }
    
    
    if([toneId rangeOfString:@"|"].location != NSNotFound)
    {
         ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@&identifier=AddToWishList&contentId=%@&contentName=%@&path=%@&previewImage=%@&album=%@&artist=%@&price=0&wishlistType=%d&likeCount=%@",appDelegate.MobileNumber,Rno1,appDelegate.Language,toneId,ToneName,ToneURL,ImageURL,ToneName, Artist,_WishListType,LikeCount];
    }
    else
    {
    
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@&identifier=AddToWishList&contentId=%@|%@&contentName=%@&path=%@&previewImage=%@&album=%@&artist=%@&price=0&wishlistType=%d&likeCount=%@",appDelegate.MobileNumber,Rno1,appDelegate.Language,toneId,CategoryID,ToneName,ToneURL,ImageURL,ToneName, Artist,_WishListType,LikeCount];
    }
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/add-to-wishlist",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_SubscriberValidation onSuccessResponseRecived = %@",Response);
    
    [_ActivityIndicator stopAnimating];
    
    _ConfirmButton.hidden = FALSE;
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    NSString *status = [responseMap objectForKey:@"status"];
    NSString *message = [Response objectForKey:@"message"];
    
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = message;
    
    if([status isEqualToString:@"200"])
    {
        NSLog(@"Success");
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_SubscriberValidation onFailureResponseRecived = %@",Response);
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.hidden = FALSE;
    
    NSString *message = [Response objectForKey:@"message"];
    NSLog(@"message = %@", message);
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = message;
}

-(void)onFailedToConnectToServer
{
    NSLog(@"API_SubscriberValidation onFailedToConnectToServer");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.hidden = FALSE;
    
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.hidden = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
@end

