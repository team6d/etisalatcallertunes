//
//  SIXDMyTunesLibraryVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 30/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDMyTunesLibraryVC : ViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet UIView *SegmentView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;

@property (nonatomic) int rbtMode;      //400-ordinary tunes    //300-playlist

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property(strong,nonatomic) NSMutableArray *Tunes_listToneApk;
@property(strong,nonatomic) NSMutableArray *Playlist_listToneApk;
@property(strong,nonatomic) NSMutableArray *KaraokeList;
@property(nonatomic) NSInteger ButtonTag;

@property (weak, nonatomic) IBOutlet UIButton *InfoButton;
@property (weak, nonatomic) IBOutlet UIButton *SetButton;

- (IBAction)onClickedInfoButton:(UIButton *)sender;
- (IBAction)onClickedSetButton:(UIButton *)sender;


@property(strong,nonatomic) NSMutableArray *Tunes_ButtonTagArray;
@property(strong,nonatomic) NSMutableArray *Playlist_ButtonTagArray;
@property(strong,nonatomic) NSMutableArray *Karaoke_ButtonTagArray;
@property (strong, nonatomic) IBOutlet UIView *AlertView;


@property(nonatomic) int ActionType;
//To check if reloadimng of API is required or not
@property(nonatomic) BOOL IsRefreshRequired;

@end
