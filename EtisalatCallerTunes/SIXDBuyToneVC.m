//
//  SIXDBuyToneVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 27/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDBuyToneVC.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "UIImageView+WebCache.h"
#import "SIXDStylist.h"

@interface SIXDBuyToneVC ()

@end

@implementation SIXDBuyToneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _ShouldDismissView = false;
    
    if(_IsSourceBuyPlaylist)
    {
        [SIXDStylist setGAScreenTracker:@"Buy_Playlist"];
        
        _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BUY A PLAYLIST",nil,appDelegate.LocalBundel,nil);
        _TopLabel.text = [SIXDGlobalViewController CapitalisationConversion:[_ToneDetails objectForKey:@"description"]];
        _BottomLabel.text = @"";
        _SpaceBetweenBottomLabelAndButton.constant = _SpaceBetweenBottomLabelAndButton.constant - 18;
        _PopUpViewHeight.constant = _PopUpViewHeight.constant -18;
        NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImage"]];
        [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@",_TopLabel.text,[_ToneDetails objectForKey:@"toneCode"]];
        [SIXDStylist setGAEventTracker:@"Buy playlist" :@"Buy playlist request" :GAEventDetails];
        
    }
    else
    {
        _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"BUY A TUNE",nil,appDelegate.LocalBundel,nil);
        NSString *String1 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"artistName"]];
        _TopLabel.text = [String1 capitalizedString];
        NSString *String2 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"toneName"]];
        _BottomLabel.text = [String2 capitalizedString];
    
        NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
        [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        if(!_IsSourceStatus && !_IsSourcePreBooking && !_IsSourceBuyPlaylist)
        {
            [SIXDStylist setGAScreenTracker:@"Buy_Tune"];
            
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Buy tune" :@"Buy tune request" :GAEventDetails];
        }
        
    }
    
    if(_IsSourcePreBooking)
    {
         [SIXDStylist setGAScreenTracker:@"Buy_Prebook"];
    }
    else if(_IsSourceStatus)
    {
        [SIXDStylist setGAScreenTracker:@"Buy_StatusTune"];
    }
   
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    
    [_AlertLabel setHidden:true];
    _SpaceBetweenBottomLabelAndButton.constant = _SpaceBetweenBottomLabelAndButton.constant - 18;
    _PopUpViewHeight.constant = _PopUpViewHeight.constant -18;
    
    [_ActivityIndicator startAnimating];
    _ConfirmButton.alpha = 0.3;
    [_ConfirmButton setUserInteractionEnabled:false];
    [_SubView setUserInteractionEnabled:false];
    
    [self API_GetTonePriceAndValidateNumber];
}

-(void)API_GetTonePriceAndValidateNumber
{
    NSLog(@"Inside API_GetTonePriceAndValidateNumber");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_TONE_PRICE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    
    NSString *toneId;
    
    if(_IsSourceBuyPlaylist)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    else
    {
        toneId = [_ToneDetails objectForKey:@"toneId"];
    }
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=%@&validationIdentifier=3&channelId=%@",Rno,appDelegate.Language, appDelegate.MobileNumber,toneId,CHANNEL_ID];

    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_SetTone
{
    
    
    NSLog(@"Inside API_SetTone");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= BUY_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&toneId=%@&toneName=%@&language=%@&priority=0",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],[_ToneDetails objectForKey:@"toneName"],appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/set-tone",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_BuyMusicChannel
{
    NSLog(@"Inside API_BuyMusicChannel");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= BUY_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&toneId=%@&clientTxnId=%@&language=%@&serviceId=7&channelId=%@&priority=0&paymentMode=2",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneCode"],Rno1,appDelegate.Language,CHANNEL_ID];
   // ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=0508035301&toneId=234567&clientTxnId=%@&language=%@&serviceId=7&channelId=1&priority=0&paymentMode=2",Rno1,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/buy-music-channel",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_PreBooking
{
    NSLog(@"Inside API_PreBooking");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= BUY_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&toneId=%@&toneName=%@&language=%@&priority=0&clientTxnId=%@&channelId=%@&serviceId=1&packName=%@",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],[_ToneDetails objectForKey:@"toneName"],appDelegate.Language,Rno,CHANNEL_ID,_PackName];

    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/prebook-activation",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_StatusBuy
{
    

    NSLog(@"Inside API_StatusBuy");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= BUY_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&toneId=%@&numOfDays=0&numOfDays=0",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"]];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/status-pack-activation",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    if(_ShouldDismissView)
    {
         [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [_ActivityIndicator startAnimating];
        _ConfirmButton.alpha = 0.3;
        [_ConfirmButton setUserInteractionEnabled:false];
        [_SubView setUserInteractionEnabled:false];
        
        if(_IsSourcePreBooking)
        {
            [self API_PreBooking];
        }
        else if(_IsSourceBuyPlaylist)
        {
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@",_TopLabel.text,[_ToneDetails objectForKey:@"toneCode"]];
            [SIXDStylist setGAEventTracker:@"Buy playlist" :@"Buy playlist confirm" :GAEventDetails];
            
            [self API_BuyMusicChannel];
        }
        else if(_IsSourceStatus)
        {
            [self API_StatusBuy];
        }
        else
        {
           NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Buy tune" :@"Buy tune confirm" :GAEventDetails];
            [self API_SetTone];
        }
        
    }
}

- (IBAction)onClickedCloseButton:(UIButton *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}



-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_GetTonePriceAndValidateNumber onSuccessResponseRecived = %@",Response);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    
    if(appDelegate.APIFlag==GET_TONE_PRICE)
    {
        NSMutableArray *responseDetailsList = [responseMap objectForKey:@"responseDetails"];
        NSMutableDictionary *responseDetails = [responseDetailsList objectAtIndex:0];
        
        if(_IsSourceBuyPlaylist)
        {
            int temp = [[responseDetails objectForKey:@"amount"] intValue];
            if(temp==0)
            {
                _PriceLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTableInBundle(@"AED",nil,appDelegate.LocalBundel,nil),temp];
            }
            else
            {
                NSString *Price = [appDelegate.AppSettings objectForKey:@"MC_PRICE"];
                _PriceLabel.text = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
            }
        }
        else
        {
            int statusCodes = [[responseDetails objectForKey:@"statusCodes"]intValue];
            
            NSString *Price;
        
            int temp = [[responseDetails objectForKey:@"amount"] intValue];
            NSMutableDictionary *Dict = [appDelegate.AppSettings objectForKey:@"VALIDITY"];
            Price = [Dict objectForKey:[responseDetails objectForKey:@"packName"]];
            
            _PackName = [responseDetails objectForKey:@"packName"];
            
            Price = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
            
            switch (statusCodes)
            {
                case 0:
                    {
                        //SUCCESS
                    }
                    break;
              
                default:
                    {
                        Price = [NSString stringWithFormat:@"\n%@",[responseDetails objectForKey:@"statusDesc"]];
                       /* [_ActivityIndicator startAnimating];
                        _ConfirmButton.alpha = 0.3;
                        [_ConfirmButton setUserInteractionEnabled:false];
                        */
                        _PriceLabel.text = Price;
                        _PriceLabelHeight.constant = _PriceLabelHeight.constant+16;
                        _PopUpViewHeight.constant = _PopUpViewHeight.constant+16;
                    }
                    break;
            }
            
            _PriceLabel.text = Price;
        }
        
    }
    else// if(appDelegate.APIFlag==BUY_TONE||appDelegate.APIFlag==PREBOOK_FLAG_HERE)
    {
        
        NSString *Message = [Response objectForKey: @"message"];
        
        _PopUpViewHeight.constant = _PopUpViewHeight.constant -_PriceLabelHeight.constant;
        _PriceLabelHeight.constant = 0;
        _AlertLabel.text = Message;
        [_AlertLabel sizeToFit];
        [_AlertLabel setHidden:false];
        [_PriceLabel setHidden:true];
        _SpaceBetweenBottomLabelAndButton.constant = _SpaceBetweenBottomLabelAndButton.constant+60;
        _PopUpViewHeight.constant = _PopUpViewHeight.constant+60;
        _ShouldDismissView = true;
        [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
       
        
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];
    
    _PopUpViewHeight.constant = _PopUpViewHeight.constant -_PriceLabelHeight.constant;
    _PriceLabelHeight.constant = 0;
    
    _AlertLabel.text = [Response objectForKey:@"message"];
    
    if(_AlertLabel.text.length <= 0)
    {
        _AlertLabel.text =NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    }
    
    [_AlertLabel sizeToFit];
    [_AlertLabel setHidden:false];
    [_PriceLabel setHidden:true];
    _SpaceBetweenBottomLabelAndButton.constant = _SpaceBetweenBottomLabelAndButton.constant+60;
    _PopUpViewHeight.constant = _PopUpViewHeight.constant+60;
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _ShouldDismissView = true;
    
}

-(void)onFailedToConnectToServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_SubView setUserInteractionEnabled:true];
    
    _PopUpViewHeight.constant = _PopUpViewHeight.constant -_PriceLabelHeight.constant;
    _PriceLabelHeight.constant = 0;
    _AlertLabel.text =NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    [_AlertLabel sizeToFit];
    [_AlertLabel setHidden:false];
    [_PriceLabel setHidden:true];
    _SpaceBetweenBottomLabelAndButton.constant = _SpaceBetweenBottomLabelAndButton.constant+30;
    _PopUpViewHeight.constant = _PopUpViewHeight.constant+30;
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _ShouldDismissView = true;
    
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
    //testing
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

@end
