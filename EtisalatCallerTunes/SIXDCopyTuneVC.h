//
//  SIXDCopyTuneVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDCopyPlaylistVC.h"
#import "SIXDCopyActiveTonesVC.h"
@import ContactsUI;

@interface SIXDCopyTuneVC : ViewController<UITextFieldDelegate,UIPickerViewDelegate, CNContactPickerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property(strong,nonatomic) SIXDCopyActiveTonesVC *TonesVC;
@property(strong,nonatomic) SIXDCopyPlaylistVC *PlaylistVC;
@property (weak, nonatomic) IBOutlet UIView *TonesContainer;
@property (weak, nonatomic) IBOutlet UIView *PlaylistContainer;
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet UIView *SegmentView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *InfoLabel;

@property (weak, nonatomic) IBOutlet UIView *CopyNumberView;
@property (weak, nonatomic) IBOutlet UITextField *CopyNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *CopyNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *CopyNumberButton;
- (IBAction)onClickedAddContactButton:(id)sender;

@property (nonatomic) int rbtMode;
@property (nonatomic) int typeFlag;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UIButton *ShowListButton;
- (IBAction)onClickedShowListButton:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property(strong,nonatomic)NSArray *PhoneNumbers;
@property (weak, nonatomic) IBOutlet UIView *AlertView;


@property(strong,nonatomic) NSString *BPartyMsisdn;
-(void)fetchTunesAndPlaylist;




@end
