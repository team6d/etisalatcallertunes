//
//  SIXDWishlistVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//




#import "SIXDWishlistVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "UIImageView+WebCache.h"
#import "SIXDPlayerVC.h"
#import "SIXDBuyToneVC.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDPlaylistBuyVC.h"
#import "SIXDSalatiVC.h"

#define PLAYLIST_IDENTIFIER     2
#define TUNES_IDENTIFIER        1
#define PREBOOK_IDENTIFIER      3


@interface SIXDWishlistVC ()

@end

@implementation SIXDWishlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _typeFlag=TUNES_IDENTIFIER;
    
    _IsInitialLoad = true;

     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"I WISH",nil,appDelegate.LocalBundel,nil);
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _Tunes_ButtonTagArray = [NSMutableArray new];
    _Playlist_ButtonTagArray = [NSMutableArray new];
   // _Prebook_ButtonTagArray = [NSMutableArray new];
    appDelegate.SelectionButtonTagArray = [NSMutableArray new];
    
    UIImage *image = [[UIImage imageNamed:@"delete_GV"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_DeleteButton setImage:image forState:UIControlStateNormal];
    [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    
    
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    //adding lines for selected tab
    Temp = [array objectAtIndex:0];
    UILabel *Line;
    UILabel *Line1;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+80, 11, 1, 7)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+160, 11, 1, 7)];
        
    }
    else
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+75, 11, 1, 7)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+170, 11, 1, 7)];
    }
    Line.backgroundColor = [UIColor grayColor];
    Line1.backgroundColor = [UIColor grayColor];
    [_SegmentTab insertSubview:Line aboveSubview:Temp];
    
    Temp = [array objectAtIndex:1];
    [_SegmentTab insertSubview:Line1 aboveSubview:Temp];

    
    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no wish at the moment",nil,appDelegate.LocalBundel,nil) :0];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
    
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"TUNES",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"PLAYLIST",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"PREORDERED",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    //To Make navigation bar opaque
    /*
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
     [self.navigationController.navigationBar setBarTintColor: [UIColor whiteColor]];
     */
}

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender {
    switch ([sender selectedSegmentIndex])
    {
        case 0:
            
            [SIXDStylist setGAScreenTracker:@"View_Wishlist_Tunes"];
            
            NSLog(@"switch0");
            _typeFlag= TUNES_IDENTIFIER;
            
            if(_WishlistTuneArray.count<=0)
            {
                [_AlertView setHidden:false];
                _TunesTableView.hidden=YES;
                _PlaylistTableView.hidden = YES;
                _PreBookTableView.hidden = YES;
                _DeleteButton.hidden = true;
            }
            else
            {
                _DeleteButton.hidden = false;
                [_AlertView setHidden:true];
                [_TunesTableView reloadData];
                _TunesTableView.hidden=NO;
                _PlaylistTableView.hidden = YES;
                _PreBookTableView.hidden = YES;
                if(_Tunes_ButtonTagArray.count <= 0)
                {
                    [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
                }
                else
                {
                    [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                }
            }
            break;
            
        case 1:
            
            [SIXDStylist setGAScreenTracker:@"View_Wishlist_Playlist"];
            NSLog(@"switch1");
            _typeFlag=PLAYLIST_IDENTIFIER;
            
            if(_WishlistPlaylist.count<=0)
            {
                _DeleteButton.hidden = true;
                 [_AlertView setHidden:false];
                _TunesTableView.hidden=YES;
                _PlaylistTableView.hidden = YES;
                _PreBookTableView.hidden = YES;
            }
            else
            {
                _DeleteButton.hidden = false;
                [_AlertView setHidden:true];
                [_PlaylistTableView reloadData];
                _TunesTableView.hidden=YES;
                _PlaylistTableView.hidden = NO;
                _PreBookTableView.hidden = YES;
                if(_Playlist_ButtonTagArray.count <= 0)
                {
                    [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
                }
                else
                {
                    [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                }
                
            }
            
            break;
            
        case 2:
            
            [SIXDStylist setGAScreenTracker:@"View_Wishlist_Preordered"];
            NSLog(@"switch2");
            _typeFlag = PREBOOK_IDENTIFIER;
            
            if(_WishlistPreBooklist.count<=0)
            {
                _DeleteButton.hidden = true;
                [_AlertView setHidden:false];
                _TunesTableView.hidden=YES;
                _PlaylistTableView.hidden = YES;
                _PreBookTableView.hidden = YES;
            }
            else
            {
                _DeleteButton.hidden = true;
                [_AlertView setHidden:true];
                [_PreBookTableView reloadData];
                _TunesTableView.hidden=YES;
                _PlaylistTableView.hidden = YES;
                _PreBookTableView.hidden = NO;
               
            }
            break;
            
        default:
            NSLog(@"switch default statement for segment control");
            
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //NSLog(@"scrollViewDidScroll");
    [SIXDGlobalViewController ChangeScrollPosition: scrollView :_ImageHeight];
    /*
    int action = [SIXDGlobalViewController ChangeScrollPositionAndHide : scrollView :_ImageHeight];
    
    if(action == 1)
        _HeaderLabel.hidden = YES;
    
    else if (action ==2)
        _HeaderLabel.hidden = NO;
     */
}


-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
       NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
        //self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"MY WISHLIST",nil,appDelegate.LocalBundel,nil);
    }
    else
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"GESSTextMedium-Medium" size:17]}];
       // self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"MY WISHLIST",nil,appDelegate.LocalBundel,nil);
    }
     */
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_IsInitialLoad)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.APIFlag = VIEW_WISHLIST_TUNES;
        [self API_ViewWishList];
        _IsInitialLoad = false;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger NumberofItems = 0;
    if(_typeFlag == PLAYLIST_IDENTIFIER)
    {

        NumberofItems = _WishlistPlaylist.count/3;
    
        if(_WishlistPlaylist.count%3 != 0)
        {
             ++NumberofItems;
        }
    }
    else if(_typeFlag == PREBOOK_IDENTIFIER)
    {
        NumberofItems = _WishlistPreBooklist.count;
    }
    else
    {
        NumberofItems = _WishlistTuneArray.count;
    }
    
    return NumberofItems;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    cell.Source = WISHLIST;
    if(_typeFlag == PLAYLIST_IDENTIFIER)
    {
        NSLog(@"Loading playlist");
        cell.CollectionView.tag = indexPath.row;
    
        cell.FirstRow = [NSMutableArray new];
        NSInteger index = indexPath.row*3;
        for (int i=0; i<3; i++)
        {
            if(index < _WishlistPlaylist.count)
            {
                [cell.FirstRow addObject:[_WishlistPlaylist objectAtIndex:index]];
                index++;
            }
        }
        
        NSLog(@"Section index = %ld",(long)indexPath.section);
        [cell.CollectionView reloadData];
        return cell;
    }
    
    if(_typeFlag == PREBOOK_IDENTIFIER)
    {
        if(indexPath.row <_WishlistPreBooklist.count)
        {
            NSMutableDictionary *DetailsTemp = [_WishlistPreBooklist objectAtIndex:indexPath.row];
            
            cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[DetailsTemp objectForKey:@"artistName"]];
            cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[DetailsTemp objectForKey:@"albumName"]];
            NSURL *ImageURL = [NSURL URLWithString:[[DetailsTemp objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
            [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
            //[cell.AccessoryButton setImage:[UIImage imageNamed:@"preorder_inactive"] forState:UIControlStateNormal];
            cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"PREORDERED",nil,appDelegate.LocalBundel,nil);
            
            UIImage* image = [[UIImage imageNamed:@"delete"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
            [cell.FirstButton setImage:[UIImage imageNamed:@"mytuneslibraryinactive"] forState:UIControlStateNormal];
            
           /*
            [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocrossunselect"] forState:UIControlStateNormal];
            
            for(int i=0;i<_Prebook_ButtonTagArray.count;i++)
            {
                NSString *tagStr = [_Prebook_ButtonTagArray objectAtIndex:i];
                long buttonTag = [tagStr longLongValue];
                if(buttonTag == indexPath.row)
                {
                    [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocross"] forState:UIControlStateNormal];
                }
            }
            cell.SelectionButton.tag = indexPath.row;
            */
            int x_Position;
            //since wishlist has different button size.
            if([appDelegate.Language isEqualToString:@"English"])
            {
                x_Position=0;
        
                UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 52)];
                leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
                [cell.FirstButton addSubview:leftBorder2];
                UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 52)];
                leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
                [cell.AccessoryButton addSubview:leftBorder3];
            }
            else
            {
                UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(49, 0, 0.5, 52)];
                leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
                [cell.FirstButton addSubview:leftBorder2];
                UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(49, 0, 0.5, 52)];
                leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
                [cell.AccessoryButton addSubview:leftBorder3];
            }
            cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
            cell.SubView.layer.borderWidth = 0.5;
        }
    }
    else
    {
    if(indexPath.row <_WishlistTuneArray.count)
    {
       

        NSMutableDictionary *DetailsTemp = [_WishlistTuneArray objectAtIndex:indexPath.row];
        
        cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[DetailsTemp objectForKey:@"artistName"]];
        cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[DetailsTemp objectForKey:@"albumName"]];
        
        NSURL *ImageURL = [NSURL URLWithString:[[DetailsTemp objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
        
        image = [[UIImage imageNamed:@"topchartsbuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.BuyButton setImage:image forState:UIControlStateNormal];
        [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
        
        
        //LIKE SETTINGS
        NSLog(@"CustomTV::Calling likeSettingsSession >>>");
        [SIXDStylist loadLikeUnlikeSetings:cell :[DetailsTemp objectForKey:@"toneId"]];
      
        
        //***set like count *********
        UILabel *TempLabel = [UILabel new];
        SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
        if([DetailsTemp objectForKey:@"likeCount"])
        {
            [likeSettingsSession setLikeCount:[DetailsTemp objectForKey:@"toneId"] :[[DetailsTemp objectForKey:@"likeCount"]intValue] :TempLabel];
        }
        else
            TempLabel.text =  LIKE_COUNNT_DEFAULT;
        //***set like count enda *********
        
        int likeCount = [TempLabel.text intValue];
        NSString *title;
        if(likeCount == 1)
        {
            title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"like",nil,appDelegate.LocalBundel,nil)];
        }
        else
        {
            title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
        }
        
        
        [cell.LikeButton setTitle:title forState:UIControlStateNormal];
        
        [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocrossunselect"] forState:UIControlStateNormal];
        
        for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
        {
            NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
            long buttonTag = [tagStr longLongValue];
            if(buttonTag == indexPath.row)
            {
                [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocross"] forState:UIControlStateNormal];
            }
        }
        int x_Position;
        //since wishlist has different button size.
        if([appDelegate.Language isEqualToString:@"English"])
        {
            x_Position=0;
            UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 52)];
            leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
            
            [cell.LikeButton addSubview:leftBorder1];
            
            UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 52)];
            leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
            
            [cell.BuyButton addSubview:leftBorder2];
            
            UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 52)];
            leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
            [cell.AccessoryButton addSubview:leftBorder3];
        }
        else
        {
            
            UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(59, 0, 0.5, 52)];
            leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
            
            [cell.LikeButton addSubview:leftBorder1];
            
            UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(49, 0, 0.5, 52)];
            leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
            
            [cell.BuyButton addSubview:leftBorder2];
            
            UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(49, 0, 0.5, 52)];
            leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
            [cell.AccessoryButton addSubview:leftBorder3];
        }
        
        
        
    
        cell.BuyButton.tag = indexPath.row;
        cell.LikeButton.tag = indexPath.row;
        cell.AccessoryButton.tag = indexPath.row;
        cell.SelectionButton.tag = indexPath.row;
        
        cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
        cell.SubView.layer.borderWidth = 0.5;
    
    }
    }
    return cell;
}

-(void)API_ViewWishList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    if(appDelegate.APIFlag == VIEW_WISHLIST_PLAYLIST)
    {
         ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&msisdn=%@&wishlistType=2&identifier=ViewWishListItems",Rno,appDelegate.MobileNumber];
    }
    else
    {
        ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&msisdn=%@&wishlistType=1&identifier=ViewWishListItems",Rno,appDelegate.Language,appDelegate.MobileNumber];
    }
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/view-wishlist",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_RemoveFromWishList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=REMOVE_WISHLIST;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSMutableDictionary *TempDict = [NSMutableDictionary new];
    
    NSString *WishlistDeleteIds = @"";
    int WishlistType = 0;
    
    if(_SegmentTab.selectedSegmentIndex == 0)
    {
        for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
        {
            NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
            TempDict  = [_WishlistTuneArray objectAtIndex:buttonTag];
            WishlistDeleteIds = [WishlistDeleteIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[TempDict objectForKey:@"id"]]];
            WishlistType = 1;
        }
    }
    else if(_SegmentTab.selectedSegmentIndex == 1)
    {
        for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
        {
            NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
            TempDict  = [_WishlistPlaylist objectAtIndex:buttonTag];
            WishlistDeleteIds = [WishlistDeleteIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[TempDict objectForKey:@"id"]]];
            WishlistType = 2;
        }
    }
    if ([WishlistDeleteIds length] > 0)
    {
        WishlistDeleteIds = [WishlistDeleteIds substringToIndex:[WishlistDeleteIds length] - 1];
    }
    
    ReqHandler.HttpsReqType = POST;
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&msisdn=%@&wishlistType=%d&identifier=DeleteFromWishList&catagoryId=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,WishlistType,WishlistDeleteIds];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-from-wishlist",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==VIEW_WISHLIST_TUNES )
    {
        
        NSLog(@"VIEW_WISHLIST_TUNES onSuccessResponseRecived = %@",Response);
        Response =  [Response objectForKey:@"responseMap"];
        
                _WishlistTuneArray = [SIXDGlobalViewController convertToToneDetailsDefault:[[Response objectForKey:@"toneDetailsList"]mutableCopy]];
        
        NSLog(@"VIEW_WISHLIST_TUNES after conversion = %@",_WishlistTuneArray);
        
        appDelegate.APIFlag = VIEW_WISHLIST_PLAYLIST;
        [_TunesTableView reloadData];
        [self API_ViewWishList];
       
    }
    else if (appDelegate.APIFlag==VIEW_WISHLIST_PLAYLIST )
    {
        NSLog(@"VIEW_WISHLIST_PLAYLIST onSuccessResponseRecived = %@",Response);
        Response =  [Response objectForKey:@"responseMap"];
        _WishlistPlaylist = [SIXDGlobalViewController convertToToneDetailsDefault:[[Response objectForKey:@"toneDetailsList"]mutableCopy]];
        
        NSLog(@"VIEW_WISHLIST_PLAYLIST after conversion = %@",_WishlistPlaylist);
        [_PlaylistTableView reloadData];

        appDelegate.APIFlag = VIEW_WISHLIST_PREBOOK;
        // [self API_ViewWishList];
        [self API_Prebooking_Listing_Cancelling:VIEW_WISHLIST_PREBOOK];
        
    }
    else if (appDelegate.APIFlag==VIEW_WISHLIST_PREBOOK )
    {
        NSLog(@"VIEW_WISHLIST_PREBOOK onSuccessResponseRecived = %@",Response);
        Response =  [Response objectForKey:@"responseMap"];
        _WishlistPreBooklist = [SIXDGlobalViewController convertToToneDetailsDefault:[[Response objectForKey:@"responseDataList"]mutableCopy]];
        NSLog(@"VIEW_WISHLIST_PREBOOK after conversion = %@",_WishlistPreBooklist);
        [_PreBookTableView reloadData];

    }
    else
    {
        NSLog(@"onSuccessResponseRecived = %@",Response);
        if(_SegmentTab.selectedSegmentIndex == 0)
        {
            NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
            for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
            {
                NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
                long buttonTag = [tagStr longLongValue];
                [indexSet addIndex:buttonTag];
                
            }
            [_Tunes_ButtonTagArray removeAllObjects];
            [_WishlistTuneArray removeObjectsAtIndexes:indexSet];
        
        }
        if(_SegmentTab.selectedSegmentIndex == 1)
        {
            NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
            for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
            {
                NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
                long buttonTag = [tagStr longLongValue];
                [indexSet addIndex:buttonTag];
            
            }
            [_Playlist_ButtonTagArray removeAllObjects];
            [appDelegate.SelectionButtonTagArray removeAllObjects];
            [_WishlistPlaylist removeObjectsAtIndexes:indexSet];
        }
        else
        {
            if(_SelectedPreBookButtonTag != -1)
            {
                [_WishlistPreBooklist removeObjectAtIndex:_SelectedPreBookButtonTag];
                [_PreBookTableView reloadData];
                 _SelectedPreBookButtonTag = -1;
            }
        }
    }
    
    [self onSegmentValueChanged:_SegmentTab];
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"WishList onFailureResponseRecived = %@",Response);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    if((_SegmentTab.selectedSegmentIndex == 0 && (appDelegate.APIFlag!=VIEW_WISHLIST_PLAYLIST && appDelegate.APIFlag!=VIEW_WISHLIST_PREBOOK)) || (_SegmentTab.selectedSegmentIndex == 2 && appDelegate.APIFlag != VIEW_WISHLIST_PREBOOK))
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    }
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onSelectedDeleteButton:(UIButton *)sender {

    if((_SegmentTab.selectedSegmentIndex == 0 && _Tunes_ButtonTagArray.count > 0) ||(_SegmentTab.selectedSegmentIndex == 1 && _Playlist_ButtonTagArray.count > 0) || _SegmentTab.selectedSegmentIndex == 2)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.ParentView = self;
        if(_SegmentTab.selectedSegmentIndex ==0 || _SegmentTab.selectedSegmentIndex ==2)
        {
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"You are about to delete selected tunes",nil,appDelegate.LocalBundel,nil);
            
        }
        else{
            
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"You are about to delete selected playlists",nil,appDelegate.LocalBundel,nil);
        }
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil);
        
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    
}

- (IBAction)onSelectedCheckBox4Delete:(UIButton *)sender {
    NSLog(@"onSelectedCheckBox4Delete >>>");
    
    //_ButtonTag = sender.tag;
    bool tagPresent = NO;
    
    for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
        long buttonTag = [tagStr longLongValue];
        if(buttonTag == sender.tag)
        {
            [_Tunes_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [sender setImage:[UIImage imageNamed:@"radiocrossunselect" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Tunes_ButtonTagArray addObject:[NSString stringWithFormat:@"%ld", (long)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"radiocross" ] forState:UIControlStateNormal];
    }
    
    if(_Tunes_ButtonTagArray.count <= 0)
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    }
    else
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    }
    
    NSLog(@"Tag List = %@", _Tunes_ButtonTagArray);

}


- (IBAction)onSelectedCheckBox4Delete_playlist:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"onSelectedCheckBox4Delete >>>");
    bool tagPresent = NO;
    
    for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
        long buttonTag = [tagStr longLongValue];
        if(buttonTag == sender.tag)
        {
            [_Playlist_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [appDelegate.SelectionButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            
            [sender setImage:[UIImage imageNamed:@"radiocrossunselect" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Playlist_ButtonTagArray addObject:[NSString stringWithFormat:@"%ld", (long)sender.tag]];
        [appDelegate.SelectionButtonTagArray addObject:[NSString stringWithFormat:@"%ld", (long)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"radiocross" ] forState:UIControlStateNormal];
    }
    
    if(_Playlist_ButtonTagArray.count <= 0)
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    }
    else
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    }
    
    NSLog(@"Tag List playlist = %@", _Playlist_ButtonTagArray);
    
}

/*
- (IBAction)onSelectedCheckBox4Delete_PreBook:(UIButton *)sender
{
    NSLog(@"onSelectedCheckBox4Delete_PreBook >>>");
    bool tagPresent = NO;
    for(int i=0;i<_Prebook_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Prebook_ButtonTagArray objectAtIndex:i];
        long buttonTag = [tagStr longLongValue];
        if(buttonTag == sender.tag)
        {
            [_Prebook_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [sender setImage:[UIImage imageNamed:@"radiocrossunselect" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Prebook_ButtonTagArray addObject:[NSString stringWithFormat:@"%ld", (long)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"radiocross" ] forState:UIControlStateNormal];
    }
    
    if(_Prebook_ButtonTagArray.count <= 0)
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    }
    else
    {
        [_DeleteButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    }
    
    NSLog(@"Tag List prebook = %@", _Prebook_ButtonTagArray);
    
}

*/
- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"Wishlist::Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
     NSMutableDictionary *TuneDetails = [_WishlistTuneArray objectAtIndex:sender.tag];
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
    /*
    NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
     */
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TunesTableView cellForRowAtIndexPath:cellIndexpath];
    UILabel *TempLabel = [UILabel new];
    TempLabel.text = [TuneDetails objectForKey:@"likeCount"];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :TempLabel];
    
    int likeCount = [TempLabel.text intValue];
    NSString *title;
    if(likeCount == 1)
    {
        title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"like",nil,appDelegate.LocalBundel,nil)];
    }
    else
    {
        title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    }
    
   // NSString *title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    [cell.LikeButton setTitle:title forState:UIControlStateNormal];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}

- (IBAction)onSelectedBuyButton:(UIButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary *Dict = [_WishlistTuneArray objectAtIndex:sender.tag];
    NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"SALATI"];
    
    if([[Dict objectForKey:@"toneId"] isEqualToString:[DummyArray objectAtIndex:0]])
    {
        NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
        appDelegate.MainTabController.selectedIndex = 2;
        UINavigationController *Temp = [VCArray objectAtIndex:2];
        [Temp popToRootViewControllerAnimated:false];
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
        ViewController.ToneDetails= [_WishlistTuneArray objectAtIndex:sender.tag];
        [self presentViewController:ViewController animated:YES completion:nil];
    }
}

- (IBAction)onSelectedPlaylistBuyButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails= [_WishlistPlaylist objectAtIndex:sender.tag];
    ViewController.IsSourceBuyPlaylist = TRUE;
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onSelectedAccessoryButton:(UIButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary *Dict = [_WishlistTuneArray objectAtIndex:sender.tag];
    NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"SALATI"];
    
    if([[Dict objectForKey:@"toneId"] isEqualToString:[DummyArray objectAtIndex:0]])
    {
        NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
        appDelegate.MainTabController.selectedIndex = 2;
        UINavigationController *Temp = [VCArray objectAtIndex:2];
        [Temp popToRootViewControllerAnimated:false];
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        ViewController.PlayList=_WishlistTuneArray;
        ViewController.CurrentToneIndex = sender.tag;
        [self.navigationController pushViewController:ViewController animated:YES];
    }
}

-(void)onSelectedPopUpConfirmButton
{
    if(_SegmentTab.selectedSegmentIndex == 2)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.APIFlag = PREBOOK_DELETION;
        [self API_Prebooking_Listing_Cancelling:PREBOOK_DELETION];
    }
    else
    {
        [self API_RemoveFromWishList];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == _TunesTableView)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSMutableDictionary *Dict = [_WishlistTuneArray objectAtIndex:indexPath.row];
        NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"SALATI"];
        
        if([[Dict objectForKey:@"toneId"] isEqualToString:[DummyArray objectAtIndex:0]])
        {
            NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
            appDelegate.MainTabController.selectedIndex = 2;
            UINavigationController *Temp = [VCArray objectAtIndex:2];
            [Temp popToRootViewControllerAnimated:false];
        }
        else
        {
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
            ViewController.PlayList=_WishlistTuneArray;
            ViewController.CurrentToneIndex = indexPath.row;
            [self.navigationController pushViewController:ViewController animated:YES];
        }
    }
}

- (IBAction)onClickedAccessoryButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlaylistBuyVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlaylistBuyVC"];
    ViewController.PlaylistDetails = [_WishlistPlaylist objectAtIndex:sender.tag];
    [self.navigationController pushViewController:ViewController animated:YES];
}


-(void)API_Prebooking_Listing_Cancelling :(int)Action
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/prebooking-listing-cancelling",appDelegate.BaseMiddlewareURL];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    if(Action == VIEW_WISHLIST_PREBOOK)
    {
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?clientTxnId=%@&channelId=%@&serviceId=19&activityId=4&aPartyMsisdn=%@",URL,Rno,CHANNEL_ID,appDelegate.MobileNumber];
    }
    else
    {
       // NSString *WishlistDeleteIds;
        NSMutableDictionary *TempDict = [NSMutableDictionary new];
        TempDict  = [_WishlistPreBooklist objectAtIndex:_SelectedPreBookButtonTag];
        
       /* for(int i=0;i<_Prebook_ButtonTagArray.count;i++)
        {
            NSString *tagStr = [_Prebook_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
        
            TempDict  = [_WishlistPreBooklist objectAtIndex:_SelectedPreBookButtonTag];
            WishlistDeleteIds = [WishlistDeleteIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[TempDict objectForKey:@"id"]]];

    
        if ([WishlistDeleteIds length] > 0)
        {
            WishlistDeleteIds = [WishlistDeleteIds substringToIndex:[WishlistDeleteIds length] - 1];
        }
        */
        
        [_ActivityIndicator startAnimating];
        [self.tabBarController.view addSubview:_overlayview];
        
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?clientTxnId=%@&channelId=%@&serviceId=19&activityId=1&aPartyMsisdn=%@&toneId=%@",URL,Rno,CHANNEL_ID,appDelegate.MobileNumber,[TempDict objectForKey:@"toneId"]];
        
    }
    [ReqHandler sendHttpGETRequest:self :nil];
    
}

- (IBAction)onSelectedPreBookDeleteButton:(UIButton *)sender
{
    _SelectedPreBookButtonTag = sender.tag;
    [self onSelectedDeleteButton:sender];
}

@end
