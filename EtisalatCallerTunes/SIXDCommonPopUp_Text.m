//
//  SIXDCommonPopUp_Text.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/15/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCommonPopUp_Text.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"

@interface SIXDCommonPopUp_Text ()

@end

@implementation SIXDCommonPopUp_Text

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    //_SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    _AlertButton.hidden = true;
    
    
    // setting pop up image view
    if(self.title.length <= 0)
    {
        _AlertMessage = [NSString stringWithFormat:@"%@",_AlertMessage];
        if(_AlertImageName.length > 1)
        {
            _AlertButton.hidden = false;
            [_AlertButton setImage:[UIImage imageNamed:_AlertImageName] forState:UIControlStateNormal];
            [self.view bringSubviewToFront:_AlertButton];
        }
    }
    //end
    
    
    NSMutableAttributedString *attributedMessage =[[NSMutableAttributedString alloc] initWithString:_AlertMessage];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:8];
    [style setAlignment:NSTextAlignmentCenter];
    [attributedMessage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,_AlertMessage.length)];
                                
    _AlertLabel.attributedText = attributedMessage;
    
    [_ConfirmButton setTitle:_ConfirmButtonText forState:UIControlStateNormal];
    _AlertTitle.text = self.title;
    
    
    //reducing height of pop up if title and image is not present
    if(self.title.length <= 0)
    {
        _AlertTitle.hidden = true;
        _PopUpViewHeight.constant = _PopUpViewHeight.constant - 60;
      
    }
    //end
    
    
    //used for pop up with more text and no buuton
    if([self.title isEqualToString:@" "])
    {
        if(appDelegate.DeviceSize == iPHONE_X)
        {
            //added not tested
            _TopSpaceFromAlertLabel.constant = -30;
        }
        else
        {
            _TopSpaceFromAlertLabel.constant = 0;
        }
       
    }
    //end
    
    if(_HideCloseButton)
    {
        [_CloseButton setHidden:true];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)putOverlayView
{
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSelectedCloseButton:(UIButton *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)onSelectedConfirmButton:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
    if(_ParentView)
    {
        [_ParentView onSelectedPopUpConfirmButton];
    }
}
@end
