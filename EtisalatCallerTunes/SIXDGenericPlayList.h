//
//  SIXDGenericPlayList.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/11/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SIXDGenericPlayList : ViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *SegmentView;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopHeadingLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;

@property(nonatomic) BOOL IsSegmentTabRequired;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SegmentTabHeight;
@property (strong, nonatomic) UIView *overlayview;

@property(nonatomic,strong) NSMutableArray *TuneList_1;
@property(nonatomic,strong) NSMutableArray *TuneList_2;
@property(nonatomic,strong) NSMutableArray *TuneList_3;

@property(nonatomic) NSUInteger TempResponseTuneList_1Count;
@property(nonatomic) NSUInteger TempResponseTuneList_2Count;
@property(nonatomic) NSUInteger TempResponseTuneList_3Count;

@property(nonatomic) NSUInteger PageNumber_TuneList_1;
@property(nonatomic) NSUInteger PageNumber_TuneList_2;
@property(nonatomic) NSUInteger PageNumber_TuneList_3;


@property(nonatomic,strong) NSMutableDictionary *SearchTagDictionary;

@property(nonatomic) BOOL IsLoading;

@property(nonatomic) NSUInteger PerPageCount;
@property(nonatomic) NSUInteger Selected_TempResponseCount;
@property(nonatomic) NSUInteger Selected_PageNumber;
@property(nonatomic,strong) NSMutableArray *Selected_TuneList;
@property(nonatomic,strong) NSString *searchKey;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;


@property(nonatomic,strong) NSString *SubCategoryName;
@property(nonatomic) BOOL IsSourceBanner;
@end
