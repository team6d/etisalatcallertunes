//
//  SIXDCustomCVCell.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/21/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIXDCustomCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ArtistImage;

@property (weak, nonatomic) IBOutlet UILabel *ArtistLabel;
@property (weak, nonatomic) IBOutlet UILabel *SongLabel;

@property (weak, nonatomic) IBOutlet UIButton *AccessoryButton;
@property (weak, nonatomic) IBOutlet UIButton *LikeButton;
@property (weak, nonatomic) IBOutlet UIButton *BuyButton;
@property (weak, nonatomic) IBOutlet UIButton *SeeMore;
@property (weak, nonatomic) IBOutlet UIImageView *GradientImageView;
@property (weak, nonatomic) IBOutlet UIButton *SelectionButton;
@property (weak, nonatomic) IBOutlet UIView *SubView;
@property (weak, nonatomic) IBOutlet UILabel *LikeLabel;
@property (strong, nonatomic) CAGradientLayer *theViewGradient;



@end
