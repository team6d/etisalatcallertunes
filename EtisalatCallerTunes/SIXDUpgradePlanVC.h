//
//  SIXDUpgradePlanVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SIXDUpgradePlanVC : ViewController<UITableViewDelegate,UITableViewDataSource>

@property(strong,nonatomic) NSArray *ImageList;
@property(strong,nonatomic) NSArray *DescriptionList;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *UpgradeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
- (IBAction)onSelectedUpgradeButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;

@property (strong,nonatomic)UIView *overlayview;


@end
