//
//  SIXDPickerPopUpNormal.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 26/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDSettingsTimeVC.h"
#import "SIXDSettingsGroupVC.h"

@interface SIXDPickerPopUpNormal : ViewController<UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *PickerView;
@property (weak, nonatomic) IBOutlet UIButton *SaveButton;
- (IBAction)onClickedSaveButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *SubView;
//@property(strong,nonatomic)NSArray *TimeTypeList;
@property(strong,nonatomic)NSArray *InputList;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;
- (IBAction)onClickedCloseButton:(id)sender;
@property(strong,nonatomic) SIXDSettingsTimeVC *TimeVC;
@property(strong,nonatomic) SIXDSettingsGroupVC *GroupVC;
@property (weak, nonatomic) UITextField *TextField;
@property (weak, nonatomic) NSString *selectedTimeType;
@property (nonatomic) int SOURCE_TYPE;

@property long selectedRow;

@end
