//
//  SIXDChangePasswordVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDChangePasswordVC : ViewController<UITextFieldDelegate,UIWebViewDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;

@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;


- (IBAction)onClickedConfirmButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) UITextField *NumberTextField;

@property(strong,nonatomic)NSMutableArray *TextFieldList;

@property(strong,nonatomic)NSArray *PlaceHolderList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TableViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *EncryptedOldPassword;
@property (nonatomic) NSString *EncryptedNewPassword;



@property(strong,nonatomic) UIView *AlertView;


@end
