//
//  CategoryListVC.m
//  6dCRBTApp
//
//  Created by Shahnawaz Lone on 9/15/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import "CategoryListVC.h"
#import "AppDelegate.h"
#import "CustomLabel.h"
#import "SIXDCustomCVCell.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"

#import "SIXDHttpRequestHandler.h"

@interface CategoryListVC ()

@end

@implementation CategoryListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _Index=0;
    _IsResponseRecieved = false;
    //[self loadCategories];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];

}

-(void)getCategories
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/categories",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@",URL,appDelegate.Language];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
   // AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"Response category list = %@",Response);
    
    _IsResponseRecieved = true;
    
    Response = [Response objectForKey:@"responseMap"];
    _CategoryList = [[Response objectForKey:@"categories"] mutableCopy];
    
    CGSize devicesize = [[UIScreen mainScreen] bounds].size ;
    CGSize size ;
    size.width = devicesize.width;
    size.height = devicesize.width/2;
    size.height = ((devicesize.width/2)*(_CategoryList.count/2)+55);
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    
    _TableView.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.contentSize = _TableView.frame.size;
    _TableViewHeight.constant = _TableView.contentSize.height;
    _TableViewWidth.constant = devicesize.width;
    
    [_TableView reloadData];
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [self viewDidLoad];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [self viewDidLoad];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
    if(_IsResponseRecieved)
    {
        NSLog(@"Response category list count= %lu",(unsigned long)_CategoryList.count);
    NSLog(@"No of Sections = %lu",(unsigned long)_CategoryList.count/2);
    return _CategoryList.count/2;
    }
    else
    {
        return 0;
    }
    
   
}

-(void) hideAndDisableLeftNavigationItem
{
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
}


-(void) showAndEnableLeftNavigationItem
{
    
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor redColor]];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
}


- (IBAction)dismissPresentView:(id)sender
{
    
    NSLog(@"dismissPresentView >>>>");
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIView *headerView;
    
    if(section == 0)
    {
        
        NSString * str = NSLocalizedStringFromTableInBundle(@"TUNES",nil,appDelegate.LocalBundel,nil);
        
        SIXDHeading_12 *title;
       
        
        if([appDelegate.Language isEqualToString:@"English"])
        {
            title = [[SIXDHeading_12 alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 55)];
            
        }
        else
        {
            title = [[SIXDHeading_12 alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 55)];
            
        }
       
        [title setTextColor:UIColorFromRGB(CLR_GREY_HEADING)];
        [title setBackgroundColor:[UIColor clearColor]];
        title.textAlignment = NSTextAlignmentCenter;
        
        title.text = str;
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width,55)];
        
        [headerView addSubview:title];
        
    }
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
    {
        return 55;
    }
    else
    {
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.TableView.frame.size.width/2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath >> category cell");
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    /*
    CGSize devicesize = [[UIScreen mainScreen] bounds].size ;
    
    cell.frame = CGRectMake(0, 0,devicesize.width,devicesize.width/2);
    cell.contentView.frame = CGRectMake(0, 0,devicesize.width,devicesize.width/2);
    */
    cell.FirstRow = [NSMutableArray new];
    //cell.FirstRow = [_CategoryList objectAtIndex:indexPath.section];
    cell.Source = CATEGORIES;
    
    if(_Index < _CategoryList.count)
    {
        
        [cell.FirstRow insertObject:[_CategoryList objectAtIndex:_Index] atIndex:0];
        _Index++;
    }
    
    if(_Index < _CategoryList.count)
    {
        [cell.FirstRow insertObject:[_CategoryList objectAtIndex:_Index] atIndex:1];
        _Index++;
    }
    
    [cell.CollectionView reloadData];
    
    
    return cell;
}


-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear >>> Category listVC");
}

-(void)loadCategories
{
    
    if(!_IsResponseRecieved)
    {
        if(_CategoryList.count == 0)
        {
           
            [self.view addSubview:self.overlayview];
            [_ActivityIndicator startAnimating];
            [self.view bringSubviewToFront:_ActivityIndicator];
            [self getCategories];
        }
        else
        {
            CGSize devicesize = [[UIScreen mainScreen] bounds].size ;
            
        
            NSLog(@"Category List = %@",_CategoryList);
            _IsResponseRecieved = true;
           
            
            CGSize size ;
            size.width = devicesize.width;
            size.height = devicesize.width/2;
            size.height = ((devicesize.width/2)*(_CategoryList.count/2)+55);
            self.view.frame = CGRectMake(0, 0, size.width,size.height);
            
            _TableView.frame = CGRectMake(0, 0, size.width,size.height);
            _TableView.contentSize = _TableView.frame.size;
            _TableViewHeight.constant = _TableView.contentSize.height;
            _TableViewWidth.constant = devicesize.width;
            
            [_TableView reloadData];
            
            self.preferredContentSize = size;
            self.automaticallyAdjustsScrollViewInsets = NO;
            
        }
    }

}
@end
