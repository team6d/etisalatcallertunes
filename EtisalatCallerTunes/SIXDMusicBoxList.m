//
//  SIXDMusicBoxList.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDMusicBoxList.h"
#import "SIXDGlobalViewController.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDPlaylistBuyVC.h"
#import "SIXDBuyToneVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDStylist.h"

@interface SIXDMusicBoxList ()

@end

@implementation SIXDMusicBoxList

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Playlist"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _CopyCount = 0;
    [self.view addSubview:_overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"PLAYLISTS",nil,appDelegate.LocalBundel,nil);
    
    _InfoLabelTop.text =  NSLocalizedStringFromTableInBundle(@"WITH EVERY NEW CALL, A DIFFERENT TONE…",nil,appDelegate.LocalBundel,nil);
    
    _InfoLabelBottom.attributedText =  [SIXDGlobalViewController getAttributedText:NSLocalizedStringFromTableInBundle(@"Discover Callertunes Playlist. Hand Picked tones compilation or Albums that shuffle each call you receive",nil,appDelegate.LocalBundel,nil)];
    /*
    [_MainImageView sd_setImageWithURL:appDelegate.CategoryImagePath placeholderImage:[UIImage imageNamed:@"Default.png"]];
    */
    
    [self getMusicChannelList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger NumberofItems = _categoryList.count/3;
    
    if(_categoryList.count%3 != 0)
    {
        NumberofItems++;
    }
    
    return NumberofItems;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.CollectionView.tag = indexPath.row;
    
    cell.FirstRow = [NSMutableArray new];
    NSInteger index = indexPath.row*3;
    
    for (int i=0; i<3; i++)
    {
        if(index < _categoryList.count)
        {
            [cell.FirstRow addObject:[_categoryList objectAtIndex:index]];
            index++;
        }
    }
    
    cell.Source = MUSICBOX;
    
    NSLog(@"Section index = %ld",(long)indexPath.section);
    [cell.CollectionView reloadData];
    return cell;
}

-(void)getMusicChannelList
{
    NSLog(@"getMusicChannelList>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    NSString *Rno2 = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"toneCode=&type=MB&isAll=T&contentTypeId=2&clientTxnId=%@&clientSessionId=%@&language=%@",Rno1,Rno2,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-play-list",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    
    NSMutableDictionary *responseMap =[Response objectForKey:@"responseMap"];
    _categoryList=[responseMap objectForKey:@"categoryList"];
    
    NSLog(@"getMusicChannelList onSuccessResponseRecived _categoryList = %@ \n _categoryList.count = %lu",_categoryList,(unsigned long)_categoryList.count);
    [_TableView reloadData];
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    NSLog(@"onFailureResponseRecived Response = %@",Response);
    
}

-(void)onFailedToConnectToServer
{
     NSLog(@"onFailedToConnectToServer");
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
    
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
- (IBAction)onClickedBuyButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails=[_categoryList objectAtIndex:sender.tag];
    ViewController.IsSourceBuyPlaylist = true;
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onSelectedMoreButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlaylistBuyVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlaylistBuyVC"];
    ViewController.PlaylistDetails = [_categoryList objectAtIndex:sender.tag];
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(IBAction)onSelectedLikeButton:(UIButton *)sender
{
    
    NSLog(@"MusicBox::Calling onSelectedLikeButton SenderTag = %ld>>>",(long)sender.tag);
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    // NSDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
    //  NSMutableDictionary *Dict = [_toneList objectAtIndex:indexPath.row];
    //  NSMutableDictionary *TuneDetails = [[_toneList objectAtIndex:sender.tag ]mutableCopy];
    NSMutableDictionary *TuneDetails = [[_categoryList objectAtIndex:sender.tag] mutableCopy];
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneCode"];
    NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
    
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    //  NSIndexPath *indexPath;
    //indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    //indexPath = [_CollectionView indexpath]
    // indexPath = [_CollectionView indexPathForItemAtPoint:[_CollectionView convertPoint:sender.center fromView:sender.superview]];
    
    // NSLog(@"index path = %ld", (long)indexPath.row);
    //SIXDCustomCVCell *cell = (SIXDCustomCVCell*)[_CollectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
            // [cell.LikeButton setImage:image forState:UIControlStateNormal];
            //[cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            /*
             NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
             SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
             
             [cell.LikeButton setImage:image forState:UIControlStateNormal];
             [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
             */
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}

@end

