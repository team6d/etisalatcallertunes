//
//  UILabel+FontAppearance.m
//  6dCRBTApp
//
//  Created by Six Dee Technologies on 11/29/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import "UILabel+FontAppearance.h"

@implementation UILabel (FontAppearance)
-(void)setAppearanceFont:(UIFont *)font {
    if (font)
        [self setFont:font];
}

-(UIFont *)appearanceFont {
    return self.font;
}
@end

@implementation UITextField (FontAppearance)
-(void)setAppearanceFont:(UIFont *)font {
    if (font)
        [self setFont:font];
}

-(UIFont *)appearanceFont {
    return self.font;
}
@end
