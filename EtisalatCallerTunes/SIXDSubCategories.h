//
//  SIXDSubCategories.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "SIXDListTonesVC.h"
#import "SIXDGenericTableView.h"

@interface SIXDSubCategories : ViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SubCategoryFirstContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SubCategorySecondContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SubCategoryThirdContainerHeight;
@property (weak, nonatomic) IBOutlet UIView *FirstContainer;
@property (weak, nonatomic) IBOutlet UIView *SecondContainer;
@property (weak, nonatomic) IBOutlet UIView *ThirdContainer;

@property (weak, nonatomic) IBOutlet UIImageView *CategoryImage;
@property (weak, nonatomic) IBOutlet UILabel *CategoryNameLabel;

@property(strong,nonatomic) SIXDListTonesVC *ListTonesSecondVC;
@property(strong,nonatomic) SIXDListTonesVC *ListTonesFirstVC;
@property(strong,nonatomic) SIXDGenericTableView *GenericTableVC;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *SubCategoryActivityIndicator;
@property(strong,nonatomic) NSMutableArray *CategoryDetails;
@property (strong, nonatomic) NSMutableArray *SubCategoryDetails;
@property (nonatomic) BOOL IsOverlayViewAdded;
@property (nonatomic) int ResponseCount;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *FirstActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *SecondActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property(nonatomic) BOOL IsRegenerateTokenInvoked;
@property (weak, nonatomic) IBOutlet UIView *AlertView;

@property(strong,nonatomic) NSMutableDictionary *SuccessIdsDict;

//Index given to GenericTableViewVC
@property(nonatomic) int SubCategoryStartIndex;





@end
