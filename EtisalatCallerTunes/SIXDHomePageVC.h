//
//  SIXDHomePageVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDListTonesVC.h"
#import "SIXDTopChartsVC.h"
#import "SIXDPageViewController.h"
#import "CategoryListVC.h"

@interface SIXDHomePageVC : ViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *BannerContainer;
@property (weak, nonatomic) IBOutlet UIView *SongListContainer;
@property (weak, nonatomic) IBOutlet UIView *SongListContainer2;
@property (weak, nonatomic) IBOutlet UIView *TopChartContainer;
@property (weak, nonatomic) IBOutlet UIView *CategoryContainer;

@property(strong,nonatomic) SIXDPageViewController *BannerVC;
@property(strong,nonatomic) SIXDListTonesVC *ListTonesHorizontalVC;
@property(strong,nonatomic) SIXDListTonesVC *ListTonesVerticalVC;
@property(strong,nonatomic) SIXDTopChartsVC *TopChartsVC;
@property(strong,nonatomic) CategoryListVC *CategoryVC;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *CategoryViewHeight;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

-(void)loadHomePage;
-(void)GetSettings;


//load all HOME PAGE API later if app is opened from deeplinking URL - Once settings response is recieved Player page is opened on coming back to home page all other APIs are invoked.
@property(nonatomic) BOOL IsLoaded;
//end


//UIRefreshControl handling parameters
@property(strong,nonatomic) UIRefreshControl *refreshControl;
@property(nonatomic) BOOL IsRefreshing;
@property(nonatomic) BOOL CanRefresh;
//end

-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response;

@end
