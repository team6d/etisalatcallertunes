//
//  SIXDSocialSharePopUpVC.h
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 25/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIXDSocialSharePopUpVC : UIViewController
- (IBAction)onClickShareOnFacebook:(id)sender;
- (IBAction)onClickShareOnTwitter:(id)sender;
- (IBAction)onClickedShareOnWhatsApp:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *WhatsAppButton;

- (IBAction)onClickedCloseButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *SubView;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *XpositionofFacebookButton;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;

@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;
@property int Source;

@end
