//
//  SIXDSignInVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDSignInVC : ViewController<UITextFieldDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *NumberView;
@property (weak, nonatomic) IBOutlet UIView *PasswordView;

@property (weak, nonatomic) IBOutlet UITextField *NumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTextField;

@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *PasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *BackButton;
- (IBAction)onClickBackButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
@property (weak, nonatomic) IBOutlet UIButton *ForgotButton;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;
- (IBAction)onClickedForgotButton:(UIButton *)sender;
- (IBAction)onClickedAlertButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToNumber;

@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *EncryptedPassword;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
//-(void)API_GetSecurityToken;
@end
