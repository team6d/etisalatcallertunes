//
//  SIXDBuyToneVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 27/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"


@interface SIXDBuyToneVC : ViewController
@property (weak, nonatomic) IBOutlet UIView *SubView;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;
- (IBAction)onClickedCloseButton:(UIButton *)sender;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PriceLabelHeight;
@property(nonatomic) BOOL IsSourceBuyPlaylist;
@property(nonatomic) BOOL IsSourcePreBooking;
@property(nonatomic) BOOL IsSourceStatus;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBetweenBottomLabelAndButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PopUpViewHeight;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property(nonatomic) BOOL ShouldDismissView;

@property(strong,nonatomic) NSString* PackName;

@end
