//
//  AppDelegate.m
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 16/11/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <TwitterKit/TWTRKit.h>
#import "SIXDLikeContentSetting.h"
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIEcommerceFields.h>
#import <GoogleAnalytics/GAIFields.h>
#import "SDImageCacheConfig.h"
#import "EtisalatCallerTunes-Swift.h"
#import "SDImageCacheConfig.h"
#import "SDImageCache.h"
#import "SIXDPlayerVC.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@import AdSupport;
@import Firebase;
@import FirebaseInstanceID;

static const NSInteger kARRMaxCacheAge = 60 * 60 * 2; // 2 hours
//static const NSInteger kARRMaxCacheAge = 60 * 10; // 10 mins
//


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"StoreManager.shared.countNum %ld",(long)[StoreManager.shared countNum]);
    _IsAppOpenedFromURL = false;
     [self setupSDWebImageCache];
    IQKeyboardManager.sharedManager.enable = NO;
    IQKeyboardManager.sharedManager.enableAutoToolbar = NO;
    IQKeyboardManager.sharedManager.shouldShowToolbarPlaceholder = false;
    IQKeyboardManager.sharedManager.keyboardDistanceFromTextField = 50.0;
//    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
//    IQKeyboardManager.shared.keyboardDistanceFromTextField = 100
//    IQKeyboardManager.shared.enable = true
    //GOOGLE ANALYTICS
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-118281125-1"];
    
    //[NotificationViewHelper setLocalNotification];
    [NotificationViewHelper addNotificationObserver];
    [NotificationButton addButtonOnNotification];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-118281125-1"];
    [tracker setAllowIDFACollection:YES];
   
    //_PromotionList = @[@"https://callertunes.etisalat.ae/en/Deals/Try_and_Buy_Deals.jpg"];
    
   /* NSString *str = [[UIApplication sharedApplication] preferredContentSizeCategory];
    NSLog(@"here 0 here = %@",[[UIApplication sharedApplication] preferredContentSizeCategory]);
   
    [[UIApplication sharedApplication] setValue:@"UICTContentSizeCategoryXXXL" forKey:@"preferredContentSizeCategory"];
    */
    
    //if([UIApplication sharedApplication] preferredContentSizeCategory)
    // Override point for customization after application launch.
    
   // NSLog(@"height = %f",[UIApplication sharedApplication].statusBarFrame.size.height);
    
    //**********Crashlytics********************
    [Fabric with:@[[Crashlytics class]]];
    
    
    //**********Google Firebase****************
    
    
    [FIRApp configure];
    [self registorForRemoteNotification:application];
    [FIRMessaging messaging].delegate = self;
    
    //[[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
    
    //local
    //_BaseMiddlewareURL =@"https://10.0.0.27:9444/Middleware/api/adapter/v1";
    
    //test set-up
    //_BaseMiddlewareURL =@"http://10.0.10.47:8830/Middleware/api/adapter/v1";
    
    //nat
    //_BaseMiddlewareURL =@"http://49.206.240.154:8831/Middleware/api/adapter/v1";
    
    //production
    //_BaseMiddlewareURL =@"https://86.96.162.55/Middleware/api/adapter/v1";
   
    //prod 2
    _BaseMiddlewareURL =@"https://www.callertunes.etisalat.ae/Middleware/api/adapter/v1";
    
    _PlaceHolderFont = @"ArialMT";
    //end
    
    //Facebook
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    /*[[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
     */
   
    
    //*******TWITTER
    
    [[Twitter sharedInstance] startWithConsumerKey:@"4jc6daOnPvscKbP5SLmf3hdEJ" consumerSecret:@"ffIVhpwCr6D1sS4qX4tOT9sC5r8jM44RonUfKNuQNWrziCHQWm"];

    /*
    GESSTextMedium-Medium
    GESSTextLight-Light
    */
    //Get Font Name
    /*
     for (NSString *familyName in [UIFont familyNames])
     {
     for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName])
     {
     NSLog(@"%@", fontName);
     }
     }
    */
    //end
    
    
    /*
    //FireBase Configuration For Push Notification
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    UNAuthorizationOptions authOptions =
    UNAuthorizationOptionAlert
    | UNAuthorizationOptionSound
    | UNAuthorizationOptionBadge;
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
    }];

    [FIRMessaging messaging].delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)  name:kFIRInstanceIDTokenRefreshNotification object:nil];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    _FCMToken = [[FIRInstanceID instanceID] token];
    NSLog(@"FCMToken = %@",_FCMToken);
    //end
    */
    
    
    CGSize devicesize = [[UIScreen mainScreen] bounds].size ;
    NSLog(@"devicesize.height = %f",devicesize.height);
    
    if(devicesize.width <= 320)
    {
        _DeviceSize = SMALL;

    }
    else if(devicesize.height == 812)
    {
       
        _DeviceSize = iPHONE_X;
    }
    else
    {   //TO DO
        _DeviceSize = MEDIUM;
    }
    
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    NSString *AppLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguage"];
    
    if([AppLanguage isEqualToString:@"en"])
    {
        _Language = @"English";
        _LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentLeft];
        
    }
    else if ([AppLanguage isEqualToString:@"ar"])
    {
        _Language = @"Arabic";
        _LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentRight];
        
    }
    else
    {
        _Language = @"English";
        AppLanguage = @"en";
        [[NSUserDefaults standardUserDefaults] setObject:AppLanguage forKey:@"SelectedLanguage"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        _LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentLeft];
        
        
    }
    
    
    //saving default pack name - will be updated in HomePage, ProfileDetails
    NSString *PackName = @"CRBT_WEEKLY";
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PACK_NAME"];
    [[NSUserDefaults standardUserDefaults] setObject:PackName forKey:@"PACK_NAME"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //end
    
    
    [SIXDStylist loadCustomFonts];
    
    NSString *state = [[NSUserDefaults standardUserDefaults] objectForKey:@"State"];
   // state = @"LOGIN";
    
    if([state isEqualToString:@"LOGIN"])
    {
        
        _AccessToken  = [[NSUserDefaults standardUserDefaults] objectForKey:@"AccessToken"];
        
        _RefreshToken =  [[NSUserDefaults standardUserDefaults] objectForKey:@"RefreshToken"];
        
        _DeviceId = [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"];
        
        _MobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"MobileNumber"];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
        
        //UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
       // UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
        
        //self.window.rootViewController = ViewController;
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
        
    }
    else
    {
        
        
        UIDevice *device = [UIDevice currentDevice];
        
        _DeviceId = [[device identifierForVendor]UUIDString];
        
        NSLog(@"_DeviceId = %@",_DeviceId);
        
        [[NSUserDefaults standardUserDefaults] setObject:_DeviceId forKey:@"DeviceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
    }
    
   // [self searchToneBasedOnToneid];
   // [self testingGiftPopupDelete];
  //  [self searchToneBasedOnToneid];
    
    return YES;
}
/*
-(void)searchTuneBasedOnToneid {
    SearchToneIdController *searchController = [[SearchToneIdController alloc] init];
    searchController.toneid = @"955511";
    [self.window.rootViewController presentViewController:searchController animated:true completion:nil];
    
}
 */

//-(void)testingGiftPopupDelete {
//    NSString * urlStr = @"https://callertunes.etisalat.ae/servlet/MobileStreamMediaServlet?fileId=JP0pty9iYIO9U+4tDVjc3o97SAWzJj1fezn5jq7tVXeEaSBg+Vb24XlNSAsBD5G8NpKzr6xTUyRp8KQK1WZfmFLWODhdfUngepgTnsGIoTw=";
//
//    NotificationViewHelper *giftView = [[NotificationViewHelper alloc] init];
//    GiftViewModel *info = [[GiftViewModel alloc] init];
//    [info addDataWithUrl:urlStr title:@"TuneTitle" subTitle:@"TuneSubTitle" likeCount:@"20" code: @"53453"];
//    [giftView showGiftViewWithInfo:info];
//}



-(void)registorForRemoteNotification:(UIApplication *)application {
   
      [UNUserNotificationCenter currentNotificationCenter].delegate = self;
      UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
          UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
      [[UNUserNotificationCenter currentNotificationCenter]
          requestAuthorizationWithOptions:authOptions
          completionHandler:^(BOOL granted, NSError * _Nullable error) {
          NSLog(@"registorForRemoteNotification...>>>>>>>>>>>>>>>>>>>>>>");
            // ...
          }];

    [application registerForRemoteNotifications];
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
   
    NSLog(@"urllll = %@",url);
    
    switch (_OpenURL_SDK_Type) {
        case FACEBOOK:
        {
            BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                          openURL:url
                                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                            ];
            
            // Add any custom logic here.
            _OpenURL_SDK_Type = 0;
           // [[Twitter sharedInstance] application:application openURL:url options:options];
            return handled;
            
        }
        break;
            
        case TWITTER:
        {
            NSLog(@"url = %@",url);
            /*
            NSString *str = [NSString stringWithFormat:@"%@",url];
            str = [str stringByReplacingOccurrencesOfString:@"twitterkit-qopwhgp15xrmhdjzfuw8gckls://" withString:@"twitterkit-QOPWhGP15xRMHdJZFUw8GckLS://"];
            url = [NSURL URLWithString:str];
            
            NSLog(@"after convertion url = %@",url);
            */
            BOOL handled = [[Twitter sharedInstance] application:application openURL:url options:options];
            NSLog(@"handled = %d",handled);
            _OpenURL_SDK_Type = 0;
            return handled;
            break;
            
        }
        default:
        {
            //for Google analytics campaigns
            NSLog(@"for Google analytics campaigns>>>");
            NSString *urlString = [url absoluteString];
            
            id<GAITracker> tracker = [[GAI sharedInstance] trackerWithName:@"tracker"
                                                                trackingId:@"UA-118281125-1"];
            
            // setCampaignParametersFromUrl: parses Google Analytics campaign ("UTM")
            // parameters from a string url into a Map that can be set on a Tracker.
            GAIDictionaryBuilder *hitParams = [[GAIDictionaryBuilder alloc] init];
            
            // Set campaign data on the map, not the tracker directly because it only
            // needs to be sent once.
            [hitParams setCampaignParametersFromUrl:urlString];
            
            // Campaign source is the only required campaign field. If previous call
            // did not set a campaign source, use the hostname as a referrer instead.
            if(![hitParams get:kGAICampaignSource] && [url host].length !=0) {
                // Set campaign data on the map, not the tracker.
                [hitParams set:@"referrer" forKey:kGAICampaignMedium];
                [hitParams set:[url host] forKey:kGAICampaignSource];
            }
            NSDictionary *hitParamsDict = [hitParams build];
            // A screen name is required for a screen view.
            [tracker set:kGAIScreenName value:@"screen name"];
            
            // SDK Version 3.08 and up.
            [tracker send:[[[GAIDictionaryBuilder createScreenView] setAll:hitParamsDict] build]];
            //end
            
            return YES;
            
        }
            break;
    }
    
    
    
    
    
    return NO;
    
    
}




- (void)applicationWillResignActive:(UIApplication *)application {
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    NSLog(@"applicationDidEnterBackground >>>");

      //  SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    /// commented after app went to bg and got hung 29-may-2018
    //likeSettingsSession.Source = SRC_PERIODIC;
       // [likeSettingsSession sendTuneListAndDownload];
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    NSLog(@"applicationWillEnterForeground >>>");
    NSLog(@"StoreManager.shared.countNum %ld",(long)[StoreManager.shared countNum]);
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    //likeSettingsSession.Source = SRC_PERIODIC;
    [likeSettingsSession sendTuneListAndDownload];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


//Enable Push Notification

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    //StoreManager.shared.fcmToken =
    NSString *uuid = UIDevice.currentDevice.identifierForVendor.UUIDString;
    //StoreManager.shared
    [NotificationSetup addDeviceDetailWithToken:fcmToken id:uuid];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}


-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
    FIRMessaging.messaging.APNSToken = deviceToken;
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"DEVICE TOKEN = %@",token);
    [NotificationViewHelper getDeviceTokenWithDeviceToken:deviceToken];
}

- (void)tokenRefreshNotification:(NSNotification *)notification
{
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    _FCMToken = refreshedToken;
    NSLog(@"InstanceID token: %@", refreshedToken);
     NSString *uuid = UIDevice.currentDevice.identifierForVendor.UUIDString;
    [NotificationSetup addDeviceDetailWithToken:_FCMToken id:uuid];
   
}

- (void)messaging:(FIRMessaging *)messaging didRefreshRegistrationToken:(NSString *)fcmToken
{
    _FCMToken = fcmToken;
}

- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"applicationReceivedRemoteMessage FIRMessagingRemoteMessage >>> ");
    // Print full message
    NSLog(@"%@", remoteMessage.appData);
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //TO DO
    //Handle Notification
    NSLog(@"didReceiveRemoteNotification %@",userInfo);
    NSLog(@"didReceiveRemoteNotification");

    if ([[userInfo valueForKey:@"type"]  isEqual: @"promotion"]) {
        [self displayTunePramotion: userInfo];
    } else if ([[userInfo valueForKey:@"type"]  isEqual: @"gift"]) {
        [self giftTypeNotification: userInfo];
    } else if ([[userInfo valueForKey:@"type"]  isEqual: @"toneId"]) {
        [self searchToneBasedOnToneid:[userInfo valueForKey:@"toneId"]];
    }
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)searchToneBasedOnToneid:(NSString *)toneId  {
    
    SearchToneId *search = [[SearchToneId alloc] init];
    [search searchToneidWithToneId: toneId complition:^(NSMutableDictionary * result) {
       
        NSMutableArray *convetredResult = [[NSMutableArray alloc] initWithObjects:result, nil];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        ViewController.PlayList = convetredResult;
        ViewController.CurrentToneIndex = 0;
        [self.HomeObj.navigationController pushViewController:ViewController animated:true];
    }];
}


-(void)displayTunePramotion:(NSDictionary *)userInfo {
    NSString * toneUrl = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"toneUrl"]];
    if ([userInfo valueForKey:@"toneUrl"]){
        NSLog(@" key is there");
    } else {
        toneUrl = @"";
    }
    NSLog(@"toneUrl  >>>>>>>>>>>%@",toneUrl);
    
    if ([toneUrl isEqualToString:@""]) {
        NSString * toneid = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"toneId"]];
        SearchToneId *search = [[SearchToneId alloc] init];
        [search searchToneidWithToneId: toneid complition:^(NSMutableDictionary * result) {
            NSMutableArray *convetredResult = [[NSMutableArray alloc] initWithObjects:result, nil];
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
            ViewController.PlayList = convetredResult;
            ViewController.CurrentToneIndex = 0;
            [self.HomeObj.navigationController pushViewController:ViewController animated:true];
        }];
        return;
    }
    [self moveToTunePromotion:userInfo];
}

-(void)moveToTunePromotion:(NSDictionary *)userInfo {
    double delayInSeconds = 0.50;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
       UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        ViewController.PlayList= [TestTuneDetail getTuneDetailWithUserInfo:userInfo];
             ViewController.CurrentToneIndex = 0;
             [self.HomeObj.navigationController pushViewController:ViewController animated:true];
    });
}

-(void)giftTypeNotification:(NSDictionary *)userInfo {
    NSString * toneUrl = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"toneUrl"]];
       if ([userInfo valueForKey:@"toneUrl"]){
           NSLog(@" key is there");
       } else {
           toneUrl = @"";
       }
       NSLog(@"toneUrl  >>>>>>>>>>>%@",toneUrl);
       
       if ([toneUrl isEqualToString:@""]) {
           NSString * toneid = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"toneId"]];
           SearchToneId *search = [[SearchToneId alloc] init];
           [search searchToneidWithToneId: toneid complition:^(NSMutableDictionary * result) {
               [self displayGiftPopupScreen: result];
           }];
           return;
       }
    [self displayGiftPopupScreen: userInfo];
}

-(void)displayGiftPopupScreen:(NSDictionary *)userInfo {
    NSString *imageUrl = [userInfo valueForKey:@"previewImageUrl"];
    NSString *artistName = [userInfo valueForKey:@"artistName"];
    NSString *likeCount = [userInfo valueForKey:@"likeCount"];
    NSString *message = [userInfo valueForKey:@"message"];
    NSString *toneId = [userInfo valueForKey:@"toneId"];
    NSString *toneName = [userInfo valueForKey:@"toneName"];
    NSString *toneUrl = [userInfo valueForKey:@"toneUrl"];
    NotificationViewHelper *giftView = [[NotificationViewHelper alloc] init];
       GiftViewModel *info = [[GiftViewModel alloc] init];
       [info addDataWithUrl:toneUrl title:artistName subTitle:toneName likeCount:likeCount code:toneId imageUrl:imageUrl];
       [giftView showGiftViewWithInfo:info];
}

- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    NSLog(@"didChangePreferredContentSize>>> %@",notification);
    NSString *state = [[NSUserDefaults standardUserDefaults] objectForKey:@"State"];
    // state = @"LOGIN";
    [SIXDStylist loadCustomFonts];
    if([state isEqualToString:@"LOGIN"])
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
        NSLog(@"didChangePreferredContentSize<<<");
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
    }
}


- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler
{
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb])
    {
        _ToneID = @"";
        NSString *myUrl = [userActivity.webpageURL absoluteString];
        myUrl = [myUrl stringByReplacingOccurrencesOfString:@"%26" withString:@"&"];
        NSLog(@"Deeplinking URL = %@",myUrl);
        /*
         https://callertunes.etisalat.ae/PopulateSongs/SalatiPage?action=loadSalatiPage&request_locale=en
         
         https://callertunes.etisalat.ae/PopulateSongs/Playlist?action=loadSelectedPlayListPage&toneCode=6015090009&request_locale=en
         
         https://callertunes.etisalat.ae/PopulateSongs/AllTune?searchKey=265585&bannerTypeId=3&request_locale=en
         */
        NSArray *arr = [myUrl componentsSeparatedByString:@"?"];
        if(arr != nil)
        {
            if(arr.count >= 2)
            {
                myUrl = [arr objectAtIndex:1];
                if([myUrl containsString:@"loadSalatiPage"])
                {
                    _ToneID = @"0";
                }
                else
                {
                    myUrl = [arr objectAtIndex:1];
                    NSArray *arr1 = [myUrl componentsSeparatedByString:@"&"];
                    if(arr1 != nil)
                    {
                        if(arr1.count >= 1)
                        {
                            myUrl = [arr1 objectAtIndex:0];
                            NSArray *arr2 = [myUrl componentsSeparatedByString:@"="];
                            if(arr2 != nil)
                            {
                                if(arr2.count >= 1)
                                {
                                    if([[arr2 objectAtIndex:1] isEqualToString:@"loadSelectedPlayListPage"])
                                    {
                                        myUrl = [arr1 objectAtIndex:1];
                                        NSArray *arr3 = [myUrl componentsSeparatedByString:@"="];
                                        if(arr3 != nil)
                                        {
                                            if(arr3.count >= 1)
                                            {
                                                _ToneID = [NSString stringWithFormat:@"%@",[[arr3 objectAtIndex:1]copy]];
                                                 NSLog(@"_ToneID = %@",_ToneID);
                                                _CategoryId = PLAYLIST;
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        _ToneID = [NSString stringWithFormat:@"%@",[[arr2 objectAtIndex:1]copy]];
                                        _CategoryId = TUNE;
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
        }
        
        if(_ToneID.length >= 1)
        {
            NSLog(@"_TuneID = %@",_ToneID);
            _IsAppOpenedFromURL = true;
            NSString *state = [[NSUserDefaults standardUserDefaults] objectForKey:@"State"];
            if([state isEqualToString:@"LOGIN"])
            {
                UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
                
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
                navController.navigationBarHidden = YES;
                self.window.rootViewController = navController;
                [self.window makeKeyAndVisible];
            }
        }
    }
    return YES;
}

- (void)setupSDWebImageCache
{
    [SDImageCache sharedImageCache].config.maxCacheAge = kARRMaxCacheAge;
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSLog(@"willPresentNotification %@",notification);
    completionHandler(UNNotificationPresentationOptionAlert);
}
- (BOOL)application:(UIApplication *)application
          openURL:(NSURL *)url
sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // [self displayGiftPopupView: userInfo];
    return YES;
}

@end

