//
//  SIXDPlayerVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/11/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>
#import "SIXDMyTunesLibraryVC.h"
#import "SIXDAVPlayer.h"

@interface SIXDPlayerVC : ViewController<NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *LikeButton;
@property (weak, nonatomic) IBOutlet UIButton *BuyButton;

@property (weak, nonatomic) IBOutlet UIButton *PlayPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *NextButton;
@property (weak, nonatomic) IBOutlet UIButton *PreviousButton;
@property (weak, nonatomic) IBOutlet UIView *UpperView;
@property (weak, nonatomic) IBOutlet UIView *LowerView;

//@property(strong,nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic)BOOL IsPlayerInitialised;
@property(nonatomic) BOOL isPlayPaused;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;
@property(strong,nonatomic) NSMutableArray *PlayList;
@property (nonatomic) NSInteger CurrentToneIndex;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property (weak, nonatomic) IBOutlet UIView *GiftView;
@property (weak, nonatomic) IBOutlet UIView *WishlistView;
@property (weak, nonatomic) IBOutlet UIView *TAFView;
@property (weak, nonatomic) IBOutlet UIView *ShareView;

@property (weak, nonatomic) IBOutlet UIImageView *GiftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *WishlistImageView;
@property (weak, nonatomic) IBOutlet UIImageView *TAFImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ShareImageView;

@property (weak, nonatomic) IBOutlet UILabel *GiftLabel;
@property (weak, nonatomic) IBOutlet UILabel *WishlistLabel;
@property (weak, nonatomic) IBOutlet UILabel *TAFLabel;
@property (weak, nonatomic) IBOutlet UILabel *ShareLabel;

- (IBAction)onClickedBuyButton:(UIButton *)sender;
- (IBAction)onClickedLikeButton:(UIButton *)sender;
- (IBAction)onClickedPlayPauseButton:(UIButton *)sender;
- (IBAction)onClickedNextButton:(UIButton *)sender;
- (IBAction)onClickedPreviousButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UpperViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ToneImageheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ToneImageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtwImageAndPlayButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *GiftViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *WishListViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TAFViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ShareViewWidth;
@property (weak, nonatomic) IBOutlet UILabel *LikeTextLabel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wishlistButtonCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtwWishlistAndGiftView;

@property(strong,nonatomic) UIVisualEffectView *BlurEffect;


@property(nonatomic) bool IsDeletePage;
@property(strong,nonatomic) SIXDMyTunesLibraryVC *MYTunesLibraryVC;

@property(strong,nonatomic) SIXDAVPlayer *SIXDAVPlayer_Audio;

@end
