//
//  SIXDTuneSearchVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDTuneSearchVC : ViewController
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(strong,nonatomic) NSMutableArray *toneList;
@property(nonatomic) NSUInteger TempResponseCount;
@property(nonatomic) NSUInteger PageNumber;
@property(nonatomic) NSUInteger PerPageCount;

@property(strong,nonatomic) NSString* SearchKey;

- (IBAction)onSelectedBuyButton:(UIButton*)sender;

- (IBAction)onSelectedAccessoryButton:(UIButton*)sender;

@property(nonatomic) BOOL IsLoading;

@end
