//
//  SIXDKaraokeVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 24/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDKaraokeVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDStylist.h"
#import "SIXDHttpRequestHandler.h"
#import "SProgressBar.h"
#import "SIXDWishlistPopUpVC.h"
#import "SIXDCommonPopUp_Text.h"

// Macros used locally
#define KRK_TnCERROR            1000
#define KRK_TXTFIELDEMPTY       1001

#define INITIAL_STATE           2000
#define START_RECORDING         2001
#define STOP_RECORDING          2002
#define PREVIEW_RECORDING       2003
#define PREVIEW_STOPPED         2004
#define PAUSE_RECORDING         2005

#define kRECORD_DURATION         20.0



#define MULTI_PART_KARAOKE          4
//#define MULTI_PART_FILE_UPLOAD          5


@interface SIXDKaraokeVC ()

@end

@implementation SIXDKaraokeVC{
    SProgressBar* m_PBView;
    NSTimer* m_timer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //_IsInitialLoad=TRUE;
    
    
    [SIXDStylist setGAScreenTracker:@"View_Karaoke"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *urlString = [appDelegate.AppSettings objectForKey:@"DIY_URL"];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    //CGRect frame = CGRectMake(25, _ScrollView.frame.origin.y+25, [UIScreen mainScreen].bounds.size.width-50, _ScrollView.frame.size.height-50);
    WKWebView *TnCWebView = [[WKWebView alloc] initWithFrame:self.TnCSubView.frame configuration:theConfiguration];

    
    TnCWebView.navigationDelegate = self;
    NSURL *nsurl=[NSURL URLWithString:urlString];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [TnCWebView loadRequest:nsrequest];
    [_TnCSubView addSubview:TnCWebView];
    
    _TnCSubView.alpha = 0.95;
    
    _TnCSubView.hidden = YES;
    
    [_TuneTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    [self InitialCondition];
    [self localisation];
    
    _IsAPICalled =false;
    _TuneLabel.hidden = YES;
    _TuneTextField.delegate = self;
    _TuneTextField.enablesReturnKeyAutomatically = NO;
    _isRecordStarted=FALSE;
    _isPlaying = false;
    _isRecordingStopped =  FALSE;
    
    _ScrollView.delegate = self;
    
    // [_RecordButton.imageView removeFromSuperview];
    
    _isCheckBoxSelected = NO;
    
    _TopSpaceToTextField.constant = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    
    UIImage *btnImage = [UIImage imageNamed:@"karaokerecord"];
    [_RecordButton setImage:btnImage forState:UIControlStateNormal];
    
    btnImage = [UIImage imageNamed:@"karaokeplay"];
    [_PlayStopButton setImage:btnImage forState:UIControlStateNormal];
    
    btnImage = [UIImage imageNamed:@"uncheckbox"];
    [_TnCCheckBox setImage : btnImage forState:UIControlStateNormal];
    
    btnImage = [UIImage imageNamed:@"karaokecancel"];
    [_DeleteButton setImage:btnImage forState:UIControlStateNormal];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _ActivityIndicator.hidden=YES;
    
    [self setButtons:INITIAL_STATE];
    _currentState = INITIAL_STATE;
    _SIXDAVPlayer = [[SIXDAVPlayer alloc]init];
    _AlertLabel.hidden=YES;
    _SIXDAVPlayer.SIXVCCallBack = self;
    [self initializeRecord];
    
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        _Terms_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

- (void)initializeRecord
{
    // Set the audio file
    
    _RecordFileName = [NSString stringWithFormat:@"Audio-%@",[self dateString]];
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               _RecordFileName,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.karaokeFileUrl = [NSURL fileURLWithPathComponents:pathComponents];
    
    [_SIXDAVPlayer initialiseRecorder : outputFileURL];
    
    NSLog(@"pathComponents = %@",pathComponents);
}
- (void) initiateSProgress {
    /*
     CGRect frame = _SaveButtonOutlet.imageView.frame;
     NSLog(@"here0 ");
     m_testView = [[TestView alloc] initWithFrame:frame];
     m_testView.percent = 0;
     NSLog(@"here1 ");
     m_testView.SaveButtonOutlet= _SaveButtonOutlet;
     */
    
    
    //CGRect frame = CGRectMake(5, 50, 200, 200);
    // _RecordButton.frame = CGRectMake(5, 50, 200, 200);
    CGRect frame = _RecordButton.imageView.frame; //***
    //CGRect frame = _RecordButton.frame;
    m_PBView = [[SProgressBar alloc] initWithFrame:frame]; //***
    m_PBView.percent = 0; //***
    m_PBView.RecordButton = _RecordButton; //***
    
    
    m_PBView.RecordButton.imageView.layer.masksToBounds = YES;
    m_PBView.RecordButton.imageView.layer.cornerRadius = m_PBView.RecordButton.imageView.frame.size.width / 2;
    
    [m_PBView.RecordButton.imageView addSubview:m_PBView]; //*** for creating rect
    //[self.view addSubview:m_PBView]; // testt
}


- (void)increamentSpin
{
    //  increament our percentage, do so, and redraw the view
    // int incrementPoints = 100/kRECORD_DURATION;
    NSLog(@"percenttt = %f",m_PBView.percent);
    if (m_PBView.percent < 100) {
        //m_PBView.percent = m_PBView.percent + incrementPoints; //m_PBView.percent + 1
        m_PBView.percent = m_PBView.percent + 1;
        [m_PBView setNeedsDisplay];
    }
    else {
        [m_timer invalidate];
        m_timer = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    NSLog(@"dismissKeyboard>>>");
    
    [_TuneTextField resignFirstResponder];
    if(_TuneTextField.text.length==0)
    {
        [self InitialCondition];
    }
}

- (IBAction)onClickDeleteButton:(id)sender
{
    if(_isPlaying)
    {
        [_SIXDAVPlayer stopPlay]; // for stopping record
        
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.AlertMessage = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"Are you sure you want to delete?",nil,appDelegate.LocalBundel,nil)];
    ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil);
    ViewController.ParentView = self;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    
    
    // [self setAlert];
    /*
     [self setButtons:INITIAL_STATE];
     _currentState = INITIAL_STATE;
     _TuneTextField.text = @"";
     */
}


- (IBAction)onClickPlayStopButton : (id)sender
{
    NSLog(@"Play");
    
    
    
    if(_isPlaying)
    {
        NSLog(@"onClickPlayStopButton >> playStopIO");
        UIImage *btnImage = [UIImage imageNamed:@"karaokeplay.png"];
        [_PlayStopButton setImage:btnImage forState:UIControlStateNormal];
        
        [_SIXDAVPlayer playIO]; // same func will stop play if it is currently playing.. need to change
        _isPlaying = false;
        [self setButtons:PREVIEW_STOPPED];
       // _isRecordingStopped
        
        
    }
    else
    {
        NSLog(@"onClickPlayStopButton >> previewRecord");
        UIImage *btnImage = [UIImage imageNamed:@"karaokepause.png"];
        [_PlayStopButton setImage:btnImage forState:UIControlStateNormal];
        [self setButtons:PREVIEW_RECORDING];
        
        [_SIXDAVPlayer previewRecord];
        _isPlaying = true;
        
        
    }
    
}

- (void)setButtons : (int) kSTATE
{
    NSLog(@"setButtons >>>");
    UIImage *btnImage = nil;
    switch(kSTATE)
    {
        case INITIAL_STATE:
            [_DeleteButton setHidden:YES];
            [_PlayStopButton setHidden:YES];
            [_RecordButton setEnabled:YES];
            [_TnCCheckBox setEnabled:YES];
            break;
            
        case START_RECORDING:
            [_DeleteButton setHidden:YES];
            [_uploadButton setEnabled:NO];
            _uploadButton.alpha = 0.7;
            [_PlayStopButton setHidden:YES];
            // [_RecordButton setEnabled:NO];
            break;
            
        case PAUSE_RECORDING:
            [_PlayStopButton setHidden:NO];
            [_DeleteButton setHidden:NO];
            [_uploadButton setEnabled:YES];
            _uploadButton.alpha = 1;
            break;
            
        case STOP_RECORDING:
            [_PlayStopButton setHidden:NO];
            [_DeleteButton setHidden:NO];
            [_uploadButton setEnabled:YES];
            _uploadButton.alpha = 1;
            [_RecordButton setEnabled:NO];
            break;
            
        case PREVIEW_RECORDING:
            [_RecordButton setEnabled:NO];
            [_uploadButton setEnabled:NO];
            _uploadButton.alpha = 0.7;
            break;
            
        case PREVIEW_STOPPED:
            [_uploadButton setEnabled:YES];
            _uploadButton.alpha = 1;
            
            btnImage = [UIImage imageNamed:@"karaokeplay.png"];
            [_PlayStopButton setImage:btnImage forState:UIControlStateNormal];
            [_PlayStopButton setEnabled:YES];
             [_RecordButton setEnabled:YES];
            if(_isRecordingStopped)
            {
                [_RecordButton setEnabled:NO];
            }
            [_DeleteButton setEnabled:YES];
           
            break;
            
        default:
            break;
    }
    NSLog(@"setButtons <<<");
}


- (IBAction)onClickRecordButton : (id)sender
{
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Permission granted");
            
            [_AlertLabel setHidden:YES];
            NSLog(@"onClickRecordButton >>>");
            
            NSLog(@"isRecordStarted = %d",_isRecordStarted);
            
            if(!_isRecordStarted)
            {
                NSLog(@"Stating to record");
                if(_currentState == PAUSE_RECORDING)
                {
                    NSLog(@"duration = %f",kRECORD_DURATION - _SIXDAVPlayer.Recorder.currentTime);
                    // [_SIXDAVPlayer recordIO:(kRECORD_DURATION - _SIXDAVPlayer.Recorder.currentTime)];
                    [self resumeSpinning];
                    [_SIXDAVPlayer recordIO:(kRECORD_DURATION)];
                }
                else
                {
                    [self initiateSProgress];
                    [self startSpinning];
                    [_SIXDAVPlayer recordIO:kRECORD_DURATION];
                }
                //[self startSpinning];
                [self setButtons:START_RECORDING];
                _currentState = START_RECORDING;
                _isRecordStarted = TRUE;
                
            }
            else
            {
                
                NSLog(@"pause   record");
                [self setButtons:PAUSE_RECORDING];
                _currentState = PAUSE_RECORDING;
                _isRecordStarted = FALSE;
                [self pauseSpinning];
                [_SIXDAVPlayer pauseRecord];
                
            }
        }
        else
        {
            NSLog(@"Permission denied");
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSLog(@"onClickedDeletGroupButton >>");
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
            SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
            ViewController.ParentView = self;
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Please go to settings and allow access for microphone",nil,appDelegate.LocalBundel,nil);
            ViewController.AlertImageName = @"mytunelibrarypopupinfo";
            ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
            
            [self presentViewController:ViewController animated:YES completion:nil];
        }
    }];
    
    
}



- (void)cancelButtonPressed{
    // write your implementation for cancel button here.
    
    NSLog(@"cancel pressed");
    // [self viewDidLoad];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)okButtonPressed{
    // write your implementation for cancel button here.
    
    // [self setButtons:INITIAL_STATE];
    //_currentState = INITIAL_STATE;
    _TuneTextField.text = @"";
    [m_PBView removeFromSuperview];
    _isRecordStarted=FALSE;
    [_RecordButton setEnabled:YES];
    [_SIXDAVPlayer deleteRecord];
    [self viewDidLoad];
}

- (IBAction)onClickedUploadButton:(id)sender
{
    NSLog(@"SIXDKaraokeVC::onClickedUploadButton >>>");
    //NSString *str = [_MobileNumberTextField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_currentState == INITIAL_STATE) // NO recording present
    {
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"ERR_NO_TUNES_RECORDED",nil,appDelegate.LocalBundel,nil);
        // _AlertLabel.text =@"You have not recorded any tunes";
        _AlertLabel.hidden = NO;
        return;
    }
    
    switch ([self checkUploadConditions]) {
        case KRK_TnCERROR:
        {
            //NSString *AlertString = @"Please select TnC";
            //[SIXDGlobalViewController setAlert:AlertString :self : GLOBAL_ALERT_ACTION_SELF_DISMISS];
            
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"ERR_AGREE_TERMS",nil,appDelegate.LocalBundel,nil);
            UIImage *btnImage = [UIImage imageNamed:@"karaokecheckboxerror.png"];
            [_TnCCheckBox setImage:btnImage forState:UIControlStateNormal];
            _AlertLabel.hidden=NO;
        }
            break;
            
        case KRK_TXTFIELDEMPTY:
        {
            //NSString *AlertString = @"Please provide name for the Tune";
            //[SIXDGlobalViewController setAlert:AlertString :self : GLOBAL_ALERT_ACTION_SELF_DISMISS];
            
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            _AlertLabel.hidden=NO;
            [SIXDStylist setTextViewAlert:_TuneTextField :_TuneNameView :_AlertButton];
        }
            break;
            
        default:
            [SIXDStylist setGAEventTracker:@"Karaoke" :@"Upload karaoke" :nil];
            
            _AlertLabel.hidden = YES;
            [self setButtons:INITIAL_STATE];
            // [self uploadKaroake];
            [self uploadKaraoke4];
            
            [SIXDStylist setTextViewNormal:_TuneTextField :_TuneNameView :_AlertButton];
            break;
    }
    
}

-(int)checkUploadConditions
{
    
    if([_TuneTextField.text isEqualToString:@""]) // Check if tune name is not provided
        return KRK_TXTFIELDEMPTY; // defined locally in the class
    
    if(!_isCheckBoxSelected) // check if TnC not selected
        return KRK_TnCERROR; //defined locally in the class
    
    return true;
    
}

- (IBAction)onCheckTnCCheckBox
{
    NSLog(@"onCheckTnCCheckBox >>");
    //[_TnCCheckBox setTintColor:[UIColor greenColor]];
    if(_isCheckBoxSelected == NO)
    {
        _isCheckBoxSelected = YES;
        UIImage *btnImage = [UIImage imageNamed:@"karaokecheckboxselect"];
        [_TnCCheckBox setImage : btnImage forState:UIControlStateNormal];
    }
    else
    {
        _isCheckBoxSelected = NO;
        UIImage *btnImage = [UIImage imageNamed:@"uncheckbox"];
        [_TnCCheckBox setImage : btnImage forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem_MainNavigationPage:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    // Kick off a timer for SProgress to count it down
    /*
    [_AlertView removeFromSuperview];
    _IsAPICalled = false;
    [_ScrollView setHidden:false];
    _AlertView =nil;
     */
    //_IsInitialLoad=FALSE;
    [self API_GetTonePriceAndValidateNumber];
    
}

- (void)startSpinning
{
    // Kick off a timer for SProgress to count it down
    NSLog(@"startSpinning >>>>>>>");
    float Sduration = (float)kRECORD_DURATION/100.0f;
    
    NSLog(@"Sduration = %f", Sduration);
    m_timer = [NSTimer scheduledTimerWithTimeInterval:Sduration target:self selector:@selector(increamentSpin) userInfo:nil repeats:YES];
}

- (void) pauseSpinning
{
    NSLog(@"StopSPINNING >>");
    
    [m_timer invalidate];
}

- (void) resumeSpinning
{
    NSLog(@"resumeSpinning");
    
   // float Sduration = ((float)kRECORD_DURATION - _SIXDAVPlayer.Recorder.currentTime )/100.0f;
     float Sduration = (float)kRECORD_DURATION/100.0f;
    
    NSLog(@"Sduration resumed = %f", Sduration);
    m_timer = [NSTimer scheduledTimerWithTimeInterval:Sduration target:self selector:@selector(increamentSpin) userInfo:nil repeats:YES];
}


-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    /*
    NSString *str = [appDelegate.AppSettings objectForKey:@"KARAOKE_PRICE"];
    _PriceLabel.text = str;
    NSLog(@"Karaoke Price = %@",_PriceLabel.text);
     */
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"KARAOKE",nil,appDelegate.LocalBundel,nil);
    _TopLabel.text = NSLocalizedStringFromTableInBundle(@"DO IT YOURSELF!",nil,appDelegate.LocalBundel,nil);
    
    _InfoLabel.attributedText = [SIXDGlobalViewController getAttributedText:NSLocalizedStringFromTableInBundle(@"KARAOKE_INFO",nil,appDelegate.LocalBundel,nil)];
    
    _TuneLabel.text = NSLocalizedStringFromTableInBundle(@"Tune Name*",nil,appDelegate.LocalBundel,nil);
    
    [_TuneTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Tune Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    [_uploadButton setTitle:NSLocalizedStringFromTableInBundle(@"UPLOAD",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _uploadButton.alpha = 1;
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_AlertButton setImage:image forState:UIControlStateNormal];
    
    NSAttributedString *titleString;
    NSMutableAttributedString *titleString0;
    
    titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Terms & Conditions",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
    
    titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"I have read and agree to the",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
    
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Terms & Conditions",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
        
        titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"I have read and agree to the",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:10]}];
    }
    else
    {
        titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Terms & Conditions",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:10]}];
        
        titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"I have read and agree to the",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:10]}];
    }
    */
    
    [titleString0 appendAttributedString:titleString];
    
    [_Terms_Button setAttributedTitle:titleString0 forState:UIControlStateNormal];
    
}

-(void)InitialCondition
{
    _AlertLabel.hidden=YES;
    _TuneLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_TuneTextField : _TuneNameView: _AlertButton];
}

-(void)uploadKaraoke4
{
    _APIInvoked = KARAOKE_UPLOAD;
    
    
    NSLog(@"uploadKaroake >>>");
    if(_currentState == PAUSE_RECORDING)
        [_SIXDAVPlayer stopRecordComplete];
    
    [_SIXDAVPlayer stopPlay];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _IsAPICalled = true;
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
        
    NSData *data = [[NSData alloc] initWithContentsOfURL: appDelegate.karaokeFileUrl.absoluteURL];
    NSLog(@"Data = %@",data);
    
    
    //#NSUInteger len = [data length];
    NSUInteger len = [data length];
    Byte *byteData= (Byte*)malloc(len);
    [data  getBytes:byteData length:len];
    
    
    // memcpy(byteData, [data bytes], len);
    NSLog(@"len = %lu", (unsigned long)len);
    NSLog(@"ByteData = %s", byteData);
    
    NSString *byteStr = [NSString stringWithFormat:@"%s",byteData];
    
    NSLog(@"byteStr = %@",byteStr);
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HeaderDictionary = [NSMutableDictionary new]; // Required for sending data in header
    
    ReqHandler.HttpsReqType = POST;
    
    //NSString *tonePath = @"/opt/prompts/";
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    //from here
    NSMutableData *postData = [NSMutableData data];
    
    ReqHandler.InvokedAPI = MULTI_PART_KARAOKE;
    

    
    NSString *boundary = @"--9b9ff40e-8821-477f-a24d-d20c5a6587b2";
    [postData appendData:[boundary dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString  *contentDisposition  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"toneFile", @"5.mp4"];
    // NSString  *contentDisposition  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n; name=toneName; filename=tempname\r\n", @"toneFile", @"5.mp4"];
   // NSString  *contentDisposition  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\" name=\"%@\"; filename=\"%@\"\r\n", @"toneFile", @"5.mp4",@"toneName",@"TEmp"];
    [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    contentDisposition  = [NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\nContent-Type: audio/M4A;\r\n\r\n"];
    [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postData appendData:data];
    
    
    [postData appendData:[[NSString stringWithFormat:@"\r\n%@--\r\n",boundary]dataUsingEncoding:NSUTF8StringEncoding]];
    
    /*
    //for testing
    NSString *toneName = [_TuneTextField.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    toneName  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"toneName",toneName];
    [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
    contentDisposition  = [NSString stringWithFormat:@"Content-Type: text/plain;\r\n\r\n"];
    [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
    
     [postData appendData:[[NSString stringWithFormat:@"\r\n%@--\r\n",boundary]dataUsingEncoding:NSUTF8StringEncoding]];
    
    //[postData appendData:[[NSString stringWithFormat:@"\r\n%@--\r\n",boundary]dataUsingEncoding:NSUTF8StringEncoding]];
    
    //end
    */

    
    //************************************************************
    
    ReqHandler.RequestData = postData;
    
    [ReqHandler.HeaderDictionary setObject:Rno forKey:@"clientTxnId"];
    [ReqHandler.HeaderDictionary setObject: @"4" forKey:@"serviceId"];
    [ReqHandler.HeaderDictionary setObject:_TuneTextField.text forKey:@"toneName"];
    [ReqHandler.HeaderDictionary setObject:appDelegate.MobileNumber forKey:@"aPartyMsisdn"];
    [ReqHandler.HeaderDictionary setObject:@"1" forKey:@"channelId"];
    [ReqHandler.HeaderDictionary setObject:@"0"forKey:@"isCopy"];
    [ReqHandler.HeaderDictionary setObject:appDelegate.Language forKey:@"language"];
    [ReqHandler.HeaderDictionary setObject:@"0" forKey:@"priority"];
    [ReqHandler.HeaderDictionary setObject:@"/dummyTonePath"forKey:@"tonePath"];
    
    
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/upload-karoake", appDelegate.BaseMiddlewareURL];
    // ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/upload-karoake?toneName=%@&", appDelegate.BaseMiddlewareURL,toneName];
    
    [ReqHandler sendHttpPostRequest:self];
}
-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"SIXDKaraokeVC onSuccessResponseRecived = %@",Response);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    if(_APIInvoked==GET_TONE_PRICE_KARAOKE)
    {
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        NSLog(@"GET_TONE_PRICE");
        
        NSMutableArray *responseDetailsList = [responseMap objectForKey:@"responseDetails"];
        NSMutableDictionary *responseDetails = [responseDetailsList objectAtIndex:0];
        
        //commented on 2nd august 2018 - shahal - in accordance with change in PRICE_POINT_VALUE
        /*
        int statusCodes = [[responseDetails objectForKey:@"statusCodes"]intValue];
        
        NSString *Price;
        
        int temp = [[responseDetails objectForKey:@"amount"] intValue];
        NSMutableDictionary *Dict = [appDelegate.AppSettings objectForKey:@"VALIDITY"];
        Price = [Dict objectForKey:[responseDetails objectForKey:@"packName"]];
        Price = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
        
        switch (statusCodes)
        {
            case 0:
            {
                _PriceLabel.text = Price;
            }
                break;

            default:
            {
                NSString *Price=@"";
                _PriceLabel.text = Price;
                Price = [NSString stringWithFormat:@"%@\n%@",Price,[responseDetails objectForKey:@"statusDesc"]];
                
                _PriceLabel.text = Price;
            }
                break;
        }
        */
        
        
        int temp = [[responseDetails objectForKey:@"amount"] intValue];
        if(temp==0)
        {
            _PriceLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTableInBundle(@"AED",nil,appDelegate.LocalBundel,nil),temp];
        }
        else
        {
            NSString *Price = [appDelegate.AppSettings objectForKey:@"DIY_PRICE"];
            _PriceLabel.text = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
        }

    }
    else
    {
    
        NSString *message = [Response objectForKey: @"message"];
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        NSString *status = [responseMap objectForKey:@"status"];
        
        if([status isEqualToString:@"200"])
        {
            NSLog(@"Success");
            /*
             [self setButtons:INITIAL_STATE];
             [self initializeRecord];
             _isRecordStarted=FALSE;
             */
        }
        
        //added
        
        if(message.length <= 0)
        {
            message = NSLocalizedStringFromTableInBundle(@"General_Success_Message",nil,appDelegate.LocalBundel,nil);
        }
        //showing alert
        _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :message :false :_ScrollView.frame :self];
        [_ScrollView setHidden:true];
        [self.view addSubview:_AlertView];
        
        //end
        
        
        //NSMutableDictionary *responseMap =[Response objectForKey:@"responseMap"];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_SubscriberValidation onFailureResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    if(_APIInvoked==GET_TONE_PRICE_KARAOKE)
    {
        return;
    }
    else
    {
        [self setButtons:STOP_RECORDING];
        
        NSString *message = [Response objectForKey: @"message"];
        //showing alert
        _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :message  :true :_ScrollView.frame :self];
        [_ScrollView setHidden:true];
        [self.view addSubview:_AlertView];
    }
    
    
    
}

-(void)onFailedToConnectToServer
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"API_SubscriberValidation onFailedToConnectToServer");
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [self setButtons:STOP_RECORDING];
    
    //showing alert
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil)  :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];
    //end
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _TopSpaceToTextField.constant=19;
    _TuneLabel.hidden=NO;
    
    [SIXDStylist setTextViewNormal:_TuneTextField :_TuneNameView :_AlertButton];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        [_ScrollView setContentOffset:CGPointMake(0,120) animated:YES];
    }
   /* else
    {
        //_ScrollView.contentOffset = CGPointMake(0, _TuneTextField.frame.origin.y+50);
       [_ScrollView setContentOffset:CGPointMake(0,70) animated:YES];
    }
    */
    
    [_TuneTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_TuneTextField.text.length==0)
    {
        [_TuneTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Tune Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        [self InitialCondition];
        _TopSpaceToTextField.constant=0;
    }
    [_ScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    // _ScrollView.contentOffset = CGPointMake(0, 0);
}

- (NSString *) dateString
{
    // return a formatted string for a file name
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"ddMMMYY_hhmmssa";
    return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".m4a"];
}

-(void) onRecordingStopped
{
    NSLog(@"SIXDKaraokeVC::onRecordingStopped >>>");
    _isRecordingStopped = TRUE;
    
    [self setButtons : STOP_RECORDING];
}

- (void) onPlayStopped
{
    NSLog(@"SIXDKaraokeVC::onPlayStopped >>>");
    [self setButtons:PREVIEW_STOPPED];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
     [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    [textField resignFirstResponder ];
    return NO;
    
}


- (IBAction)onSelectedCloseButton:(id)sender
{
    [_TnCSubView setHidden:true];
    [_CloseButton setHidden:true];
}

- (IBAction)onSelectedTermsAndConditionButton:(id)sender
{
    [_TnCSubView setHidden:false];
    [self.view bringSubviewToFront:_TnCSubView];
    
    [_CloseButton setHidden:false];
}


-(void)onSelectedPopUpConfirmButton
{
    NSLog(@"onSelectedPopUpConfirmButton >>>");
   
    if(!_IsAPICalled)
    {
        NSLog(@"IsAPICalled no");
        [self okButtonPressed];
    }
    else
    {
        /*
        [self setButtons:INITIAL_STATE];
        [self initializeRecord];
        _isRecordStarted=FALSE;
        */
         _IsAPICalled = false;
        [_ScrollView setHidden:false];
        [_AlertView removeFromSuperview];
        _AlertView = nil;
        
        [self okButtonPressed];
       // [_SIXDAVPlayer deleteRecord];
        //[self viewDidLoad];
    }
    
}

-(void)API_GetTonePriceAndValidateNumber
{
    _APIInvoked = GET_TONE_PRICE_KARAOKE;
    //[self.tabBarController.view addSubview:self.overlayview];
    //[_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=&validationIdentifier=3&channelId=%@&isDIYTone=true",Rno,appDelegate.Language, appDelegate.MobileNumber,CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

@end

