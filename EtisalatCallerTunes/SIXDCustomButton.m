//
//  SIXDCustomButton.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 01/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCustomButton.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"

@implementation SIXDCustomButton



-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor=UIColorFromRGB(CLR_ETISALAT_GREEN);
    [self.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [self.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    }
    else
    {
        [self.titleLabel setFont:[UIFont fontWithName:@"GESSTextLight-Light" size:14]];
    }
    */
}

@end


@implementation SIXDButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:10]];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [self.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:10]];
    }
    else
    {
        [self.titleLabel setFont:[UIFont fontWithName:@"GESSTextLight-Light" size:10]];
    }
     */
}

@end

