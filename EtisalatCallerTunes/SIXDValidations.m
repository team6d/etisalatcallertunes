//
//  SIXDValidations.m
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 17/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SIXDValidations.h"


@implementation GlobalInterfaceForValidations


+ (BOOL) fn_isValidEmail:(NSString*) Email
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest  evaluateWithObject:Email];
}

+ (BOOL) fnG_ValidateNames:(NSString*)InputString
{
   
        NSError *error             = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                      @"[a-zA-Z ]" options:0 error:&error];
        
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:InputString options:0 range:NSMakeRange(0, [InputString length])];
        
        return numberOfMatches == InputString.length;
}

+ (BOOL) fnG_ValidatePhoneNumber:(NSString*)phoneNumber
{
   NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


//password_condition - key to check the password condition statement
+ (BOOL) fnG_ValidatePassword:(NSString*)password
{
    NSString *phoneRegex = @"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[-?@#$%^+=._!*~|])(?=\\S+$).{8,}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:password];
}

@end
