//
//  SIXDCreateAccountVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 08/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDCreateAccountVC : ViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *OtpView;

@property (weak, nonatomic) IBOutlet UITextField *OtpTextField;

@property (weak, nonatomic) IBOutlet UILabel *OtpLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;


@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
@property (weak, nonatomic) IBOutlet UIButton *ResendButton;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;
- (IBAction)onClickedResendButton:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

@property (strong, nonatomic) NSString *SectionToken;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@end
