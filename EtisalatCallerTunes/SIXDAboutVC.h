//
//  SIXDAboutVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "WebKit/WebKit.h"

@interface SIXDAboutVC : ViewController<WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UIView *MainView;

@end
