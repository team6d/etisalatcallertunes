//
//  SIXDStylist.m
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 17/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "CustomLabel.h"
#import "UILabel+FontAppearance.h"
#import "SIXDLikeContentSetting.h"
#import "AppDelegate.h"

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>
//#import "SIXDCustomTVCell.h"


@implementation SIXDStylist

+ (void)loadLikeUnlikeSetings : (SIXDCustomTVCell *)cell : (NSString*)toneId
{
    NSLog(@"loadLikeUnlikeSetings");
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    // if([likeSettingsSession isToneLikedFromDownloadedList:[Dict objectForKey:@"toneId"]])
    int likeButtonAction = [likeSettingsSession setToneButtonLikedUnliked:toneId];
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }

}

+ (void)loadLikeUnlikeButtonSetings  : (UIButton*)LikeButton : (NSString*)toneId
{
    NSLog(@"loadLikeUnlikeSetings");
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    // if([likeSettingsSession isToneLikedFromDownloadedList:[Dict objectForKey:@"toneId"]])
    int likeButtonAction = [likeSettingsSession setToneButtonLikedUnliked:toneId];
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            UIImage* image = [[UIImage imageNamed:@"playlistsolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            [LikeButton setImage:image forState:UIControlStateNormal];
            [LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            
            UIImage* image = [[UIImage imageNamed:@"playlistoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [LikeButton setImage:image forState:UIControlStateNormal];
            [LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}

+(void)setBackgroundGradient:(UIView *)mainView
{
    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor blackColor] CGColor], (id)[[UIColor clearColor] CGColor],[[UIColor clearColor] CGColor], nil];
    grad.startPoint = CGPointMake(1, 1);
    grad.endPoint = CGPointMake(1, 0);
    mainView.alpha = 0.8;
    [mainView.layer insertSublayer:grad atIndex:0];
    
}


+(void)setBackGroundImage:(UIView *)mainView
{
    
    UIGraphicsBeginImageContext(mainView.frame.size);
    [[UIImage imageNamed:@"BackgroundDefault"] drawInRect:mainView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    mainView.backgroundColor = [UIColor colorWithPatternImage:image];
     
}

+(void)setTextViewAlert:(UITextField *)mainTextField :(UIView *)mainView :(UIButton *)mainButton
{
    
    mainView.layer.borderWidth = 1.0f;
    mainView.layer.borderColor = [[UIColor redColor] CGColor];
    
    mainTextField.tintColor = UIColorFromRGB(CLR_ALERTRED);
    mainTextField.textColor = UIColorFromRGB(CLR_ALERTRED);
    
    [mainButton.imageView setTintColor:UIColorFromRGB(CLR_ALERTRED)];
    mainButton.hidden=NO;
    
}

+(void)setTextViewNormal:(UITextField *)mainTextField :(UIView *)mainView :(UIButton *)mainButton
{
    
    mainView.layer.borderWidth = 1.0f;
    mainView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    mainTextField.tintColor = UIColorFromRGB(CLR_PLACEHOLDER);
    mainTextField.textColor = UIColorFromRGB(CLR_PLACEHOLDER);
    
    mainButton.hidden=YES;
     
}

+(void)loadCustomFonts
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *HeadingFont ;
    NSString *Font ;
    NSString *TextFieldFont ;
    UIFont *Scaledfont;
    
    if([appDelegate.Language isEqualToString:@"English"])
    {
        HeadingFont =  @"ArialMT";
        Font = @"ArialMT";
        TextFieldFont = @"ArialMT";
        
        UILabel *TempLabel =  [CustomLabel_5 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:5];
        [self setDefaultScaledFont:Scaledfont :TempLabel];
        
        
        UILabel *TempLabel1 =  [CustomLabel_7 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:7];
        [self setDefaultScaledFont:Scaledfont :TempLabel1];
        
        
        
        UILabel *TempLabel2 =  [CustomLabel_8 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:8];
        [self setDefaultScaledFont:Scaledfont :TempLabel2];
        
        UILabel *TempLabel3 =  [CustomLabel_9 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:9];
        [self setDefaultScaledFont:Scaledfont :TempLabel3];
        
        
        
        UILabel *TempLabel4 =  [CustomLabel_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:10];
        [self setDefaultScaledFont:Scaledfont :TempLabel4];
        
        
        
        UILabel *TempLabel5 =  [CustomLabel_11 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:11];
        [self setDefaultScaledFont:Scaledfont :TempLabel5];
        
        
        
        UILabel *TempLabel6 =  [CustomLabel_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:12];
        [self setDefaultScaledFont:Scaledfont :TempLabel6];
        
        
        UILabel *TempLabel7 =  [CustomLabel_13 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:13];
        [self setDefaultScaledFont:Scaledfont :TempLabel7];
        
        UILabel *TempLabel8 =  [CustomLabel_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel8];
        
        UILabel *TempLabel9 =  [CustomLabel_15 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:15];
        [self setDefaultScaledFont:Scaledfont :TempLabel9];
        
        UILabel *TempLabel10 =  [CustomLabel_16 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:16];
        [self setDefaultScaledFont:Scaledfont :TempLabel10];
        
        UILabel *TempLabel11 =  [CustomLabel_17 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:17];
        [self setDefaultScaledFont:Scaledfont :TempLabel11];
        
        UILabel *TempLabel12 =  [SIXDHeading_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:10];
        [self setDefaultScaledFont:Scaledfont :TempLabel12];
        
        UILabel *TempLabel13 =  [SIXDHeading_16 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:16];
        [self setDefaultScaledFont:Scaledfont :TempLabel13];
        
        UILabel *TempLabel14 =  [CustomLabel_Lato_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:10];
        [self setDefaultScaledFont:Scaledfont :TempLabel14];
        
        
        UILabel *TempLabel15 =  [CustomLabel_Lato_11 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:11];
        [self setDefaultScaledFont:Scaledfont :TempLabel15];
        
        UILabel *TempLabel16 =  [CustomLabel_Lato_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:12];
        [self setDefaultScaledFont:Scaledfont :TempLabel16];
        
        UILabel *TempLabel17 =  [CustomLabel_Lato_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel17];
        
        UILabel *TempLabel18 =  [SIXDHeading_17 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:17];
        [self setDefaultScaledFont:Scaledfont :TempLabel18];
        
        UILabel *TempLabel19 =  [SIXDPopUpHeading_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel19];
        
        UILabel *TempLabel20 =  [SIXDMenuLabel appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel20];
        
        UILabel *TempLabel21 =  [SIXDHeading_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:12];
        [self setDefaultScaledFont:Scaledfont :TempLabel21];
        
        UILabel *TempLabel22 =  [SIXDHeading_13 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:13];
        [self setDefaultScaledFont:Scaledfont :TempLabel22];
        
        /*
         UILabel *label_22 =  [SIXDLikeLabel appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
         [label_22 setAppearanceFont:[UIFont fontWithName:HeadingFont size:7] ];
         */
        
        [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_SELECTED) } forState:UIControlStateSelected];
        [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED) , NSFontAttributeName:[UIFont fontWithName:TextFieldFont size:9.0f]} forState:UIControlStateNormal];
        [[UISegmentedControl appearance] setBackgroundColor:[UIColor whiteColor] ];
        [[UISegmentedControl appearance] setTintColor: [UIColor clearColor]];
        
        [[UITextField appearance] setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        
        [[UIActivityIndicatorView appearance] setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [[UIActivityIndicatorView appearance] setColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    }
    else
    {
        /*
        HeadingFont =  @"GESSTextMedium-Medium";
        Font = @"GESSTextLight-Light";
        TextFieldFont = @"GESSTextLight-Light";
         */
        HeadingFont =  @"ArialMT";
        Font = @"ArialMT";
        TextFieldFont = @"ArialMT";
        
        UILabel *TempLabel =  [CustomLabel_5 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:6];
        [self setDefaultScaledFont:Scaledfont :TempLabel];
        
        
        UILabel *TempLabel1 =  [CustomLabel_7 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:8];
        [self setDefaultScaledFont:Scaledfont :TempLabel1];
        
        
        
        UILabel *TempLabel2 =  [CustomLabel_8 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:9];
        [self setDefaultScaledFont:Scaledfont :TempLabel2];
        
        UILabel *TempLabel3 =  [CustomLabel_9 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:10];
        [self setDefaultScaledFont:Scaledfont :TempLabel3];
        
        
        
        UILabel *TempLabel4 =  [CustomLabel_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:11];
        [self setDefaultScaledFont:Scaledfont :TempLabel4];
        
        
        
        UILabel *TempLabel5 =  [CustomLabel_11 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:12];
        [self setDefaultScaledFont:Scaledfont :TempLabel5];
        
        
        
        UILabel *TempLabel6 =  [CustomLabel_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:13];
        [self setDefaultScaledFont:Scaledfont :TempLabel6];
        
        
        UILabel *TempLabel7 =  [CustomLabel_13 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont = [UIFont fontWithName:Font size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel7];
        
        UILabel *TempLabel8 =  [CustomLabel_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:15];
        [self setDefaultScaledFont:Scaledfont :TempLabel8];
        
        UILabel *TempLabel9 =  [CustomLabel_15 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:16];
        [self setDefaultScaledFont:Scaledfont :TempLabel9];
        
        UILabel *TempLabel10 =  [CustomLabel_16 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:17];
        [self setDefaultScaledFont:Scaledfont :TempLabel10];
        
        UILabel *TempLabel11 =  [CustomLabel_17 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:18];
        [self setDefaultScaledFont:Scaledfont :TempLabel11];
        
        UILabel *TempLabel12 =  [SIXDHeading_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:11];
        [self setDefaultScaledFont:Scaledfont :TempLabel12];
        
        UILabel *TempLabel13 =  [SIXDHeading_16 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:17];
        [self setDefaultScaledFont:Scaledfont :TempLabel13];
        
        UILabel *TempLabel14 =  [CustomLabel_Lato_10 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:11];
        [self setDefaultScaledFont:Scaledfont :TempLabel14];
        
        
        UILabel *TempLabel15 =  [CustomLabel_Lato_11 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:12];
        [self setDefaultScaledFont:Scaledfont :TempLabel15];
        
        UILabel *TempLabel16 =  [CustomLabel_Lato_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:13];
        [self setDefaultScaledFont:Scaledfont :TempLabel16];
        
        UILabel *TempLabel17 =  [CustomLabel_Lato_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:TextFieldFont size:15];
        [self setDefaultScaledFont:Scaledfont :TempLabel17];
        
        UILabel *TempLabel18 =  [SIXDHeading_17 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:18];
        [self setDefaultScaledFont:Scaledfont :TempLabel18];
        
        UILabel *TempLabel19 =  [SIXDPopUpHeading_14 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:15];
        [self setDefaultScaledFont:Scaledfont :TempLabel19];
        
        UILabel *TempLabel20 =  [SIXDMenuLabel appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:Font size:15];
        [self setDefaultScaledFont:Scaledfont :TempLabel20];
        
        UILabel *TempLabel21 =  [SIXDHeading_12 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:13];
        [self setDefaultScaledFont:Scaledfont :TempLabel21];
        
        UILabel *TempLabel22 =  [SIXDHeading_13 appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
        Scaledfont =[UIFont fontWithName:HeadingFont size:14];
        [self setDefaultScaledFont:Scaledfont :TempLabel22];
        
        /*
         UILabel *label_22 =  [SIXDLikeLabel appearanceWhenContainedInInstancesOfClasses:@[UIViewController.class]];
         [label_22 setAppearanceFont:[UIFont fontWithName:HeadingFont size:7] ];
         */
        
        [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_SELECTED) } forState:UIControlStateSelected];
        [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED) , NSFontAttributeName:[UIFont fontWithName:TextFieldFont size:10.0f]} forState:UIControlStateNormal];
        [[UISegmentedControl appearance] setBackgroundColor:[UIColor whiteColor] ];
        [[UISegmentedControl appearance] setTintColor: [UIColor clearColor]];
        
        [[UITextField appearance] setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        
        [[UIActivityIndicatorView appearance] setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [[UIActivityIndicatorView appearance] setColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    }
    
    
    
    
    
   
    
}


+(bool)isEtisalatValidNumber : (NSString *)number
{
    //Its capable to take inputs +97150 3827741, +97-150-3827741, +97150 - 3827741,050 3827741, 050 - 3827741
   // NSString *someRegexp = @"^(\\+97[\\s]{0,1}[\\-]{0,1}[\\s]{0,1}1|0)50[\\s]{0,1}[\\-]{0,1}[\\s]{0,1}[1-9]{1}[0-9]{6}$";
      NSString *someRegexp = @"^(\\+97[\\s]{0,1}[\\-]{0,1}[\\s]{0,1}[\\s]{0,1}1|0)[\\s]{0,1}[\\-]{0,1}[0-9]{0,1}5[\\s]{0,1}[\\-]{0,1}[\\s]{0,1}[0-9]{1,2}[\\-]{0,1}[\\s]{0,1}[0-9]{1,3}[\\s]{0,1}[\\-]{0,1}[0-9]{4,5}$";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", someRegexp];
    
    if ([myTest evaluateWithObject: number]){
        NSLog(@"Matches");
        return YES;
    }
    //^(\+97[\s]{0,1}[\-]{0,1}[\s]{0,1}1|0)50[\s]{0,1}[\-]{0,1}[\s]{0,1}[1-9]{1}[0-9]{6}$
    return NO;
}


+(void)addKeyBoardDoneButton : (UITextField*)TextField : (ViewController*)ParentView
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIToolbar *toolBar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    toolBar1.barStyle = UIBarButtonItemStyleDone;
    toolBar1.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem* doneButton1 = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Done",nil,appDelegate.LocalBundel,nil) style:UIBarButtonItemStylePlain target:ParentView action:@selector(dismissKeyboard)];
    
    [doneButton1 setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [UIColor whiteColor], NSForegroundColorAttributeName,
                                         nil]
                               forState:UIControlStateNormal];
    
    [toolBar1 setItems:[NSArray arrayWithObjects: doneButton1, nil] animated:NO];
    [TextField setInputAccessoryView:toolBar1];
    
}


/*
 //adding bottomline
 CALayer *border = [CALayer layer];
 CGFloat borderWidth = 2;
 border.borderColor = [UIColor whiteColor].CGColor;
 border.frame = CGRectMake(0, _MobileNumber.frame.size.height - borderWidth, _MobileNumber.frame.size.width, _MobileNumber.frame.size.height);
 border.borderWidth = borderWidth;
 [_MobileNumber.layer addSublayer:border];
 _MobileNumber.layer.masksToBounds = YES;
 _Border=border;
 */

/*
 //animation code
 [UIView transitionWithView:_SendOtpButton
 duration:0.5f
 options:UIViewAnimationOptionTransitionCrossDissolve
 animations:^{
 
 _BottomSpaceToOtpButton.constant = _KeyBoardHeight;
 
 _MobileNumberLabel.hidden=NO;
 
 } completion:nil];
 */

+(void)setDefaultScaledFont:(UIFont*)ScaledFont : (UILabel*)TempLabel
{
    //NORMAL
    // [TempLabel setAppearanceFont:ScaledFont];
    //end
    
    
    //Change Font According to Phone Font Size
    
    if (@available(iOS 11.0, *)) {
        [TempLabel setAppearanceFont:[[UIFontMetrics defaultMetrics] scaledFontForFont:ScaledFont]];
        //[TempLabel setAdjustsFontForContentSizeCategory:true];
    } else {
        // Fallback on earlier versions
        [TempLabel setAppearanceFont:ScaledFont];
    }
    
    //end
    
}

+(void)setGAScreenTracker:(NSString *)screenName
{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+(void)setGAEventTracker:(NSString *)Category :(NSString *)Action :(NSString *)Label
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:Category
                                                          action:Action
                                                           label:Label
                                                           value:nil] build]];
}
@end
