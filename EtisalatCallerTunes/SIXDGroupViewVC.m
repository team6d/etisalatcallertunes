//
//  SIXDGroupViewVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDGroupViewVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDCustomTVCell.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDGroupConfiguration.h"
#import "SIXDStylist.h"

#import <QuartzCore/QuartzCore.h>

@interface SIXDGroupViewVC ()

@end

@implementation SIXDGroupViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_My_Caller_Group_Details"];
    
    _TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _GroupNameLabel.text = [_GroupsList objectForKey:@"groupName"];
    
    NSString *details = [_GroupsList objectForKey:@"msisdnB"];
    _CountDict = [details componentsSeparatedByString:@","];
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"MY CALLER GROUP",nil,appDelegate.LocalBundel,nil);
    
    /*
    [_EditButton setImage:[UIImage imageNamed:@"edit2"] forState:UIControlStateNormal];
    _EditButton.tintColor = UIColorFromRGB(CLR_BUY);
    [_DeleteButton setImage:[UIImage imageNamed:@"delete_GV"] forState:UIControlStateNormal];
    _DeleteButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
     */
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _CountDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSArray *Filer = [[_CountDict objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
    cell.TitleLabel.text = [Filer objectAtIndex:1];
    cell.SubTitleLabel.text =[Filer objectAtIndex:0];
    cell.TitleLabel.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    cell.SubTitleLabel.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        cell.TitleLabel.textAlignment = NSTextAlignmentRight;
        cell.SubTitleLabel.textAlignment = NSTextAlignmentLeft;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)API_DeleteGroup
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=DELETE_GROUP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&groupId=%@&groupName=%@&language=%@",appDelegate.MobileNumber,[_GroupsList objectForKey:@"groupId"],[_GroupsList objectForKey:@"groupName"],appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-group",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    NSLog(@"SIXDGroupViewVC responseMap = %@",responseMap);
    if(appDelegate.APIFlag==DELETE_GROUP)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onClickedEditGroupButton:(id)sender
{
    NSLog(@"onClickedEditGroupButton >>");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDGroupConfiguration *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGroupConfiguration"];
    ViewController.GroupInfoDictionary = _GroupsList;
    ViewController.SOURCE=FROM_EDIT_GROUP;
    ViewController.GroupViewVC = self;
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(void)onSelectedPopUpConfirmButton
{
    NSLog(@"API_DeleteGroup >>");
    [self API_DeleteGroup];
}


- (IBAction)onClickedDeletGroupButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //to be corrected.
    NSLog(@"onClickedDeletGroupButton >>");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.ParentView = self;
    //ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"This group and its tunes settings will be removed",nil,appDelegate.LocalBundel,nil);
    ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"This group and its tunes settings will be removed",nil,appDelegate.LocalBundel,nil);
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
 ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    [self presentViewController:ViewController animated:YES completion:nil];
    
    //testing animated presentation from TOP to BOTTOM
    /*
    ViewController.view.frame = CGRectMake(ViewController.view.frame.origin.x,
                                                -ViewController.view.frame.size.height,
                                                ViewController.view.frame.size.width,
                                                ViewController.view.frame.size.height);
    
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^(void){
        
        ViewController.view.frame = CGRectMake(ViewController.view.frame.origin.x,
                                                    0,
                                                    ViewController.view.frame.size.width,
                                                    ViewController.view.frame.size.height);
        [self.view addSubview:ViewController.view];
        
    } completion:^(BOOL finished){
        
        [ViewController.view removeFromSuperview];
        
        [self presentViewController:ViewController animated:NO completion:nil];
        
    }];
    */
}





@end
