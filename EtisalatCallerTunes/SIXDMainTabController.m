//
//  SIXDMainTabController.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDMainTabController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "CustomLabel.h"
#import "SIXDSearchVC.h"

@interface SIXDMainTabController ()

@end

@implementation SIXDMainTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor: [UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.opaque = YES;
    
    
    [self.tabBar setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    [[UITabBar appearance] setUnselectedItemTintColor:UIColorFromRGB(CLR_BUY)];
    
    [self.tabBar setBarTintColor:UIColorFromRGB(CLR_TABBAR_TINT)];
    
    for(UITabBarItem * tabBarItem in self.tabBar.items){
        tabBarItem.title = @"";
        tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_logo"]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"myaccount"] landscapeImagePhone:[UIImage imageNamed:@"myaccount"] style:UIBarButtonItemStylePlain target:self action:@selector(onClikedMyAccountButton)];
    [self.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
     
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.MainTabController = self;
    
    
}

-(void)onClikedMyAccountButton
{
    NSLog(@"onClikedMyAccountButton>>>");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSLog(@"self.selectedIndex = %lu",(unsigned long)self.selectedIndex);
    
    //added logic to refresh search results only on tab change
    if(self.selectedIndex == 1)
    {
        NSArray *VCArray = self.viewControllers ;
        UINavigationController *Temp = [VCArray objectAtIndex:1];
        SIXDSearchVC *VC = [[Temp viewControllers] objectAtIndex:0];
        VC.IsRefreshRequired = true;
    }
    //end
    
    NSArray *VCArray = self.viewControllers ;
    UINavigationController *Temp = [VCArray objectAtIndex:item.tag];
    [Temp popToRootViewControllerAnimated:false];
    
}

@end
