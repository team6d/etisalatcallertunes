//
//  SIXDChangeProfileDetailsVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/23/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "EtisalatCallerTunes-Swift.h"

@class SIXDProfileVC;

@interface SIXDChangeProfileDetailsVC : ViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onSelectedConfirmButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property(strong,nonatomic) SIXDProfileVC *ProfileVC;

@property (weak, nonatomic) IBOutlet  UILabel *TitleLabel;
- (IBAction)onSelectedCloseButton:(UIButton *)sender;

@property(strong,nonatomic) UITextField* SelectedTextField;
@property(nonatomic) BOOL ShouldDismissView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;


@property (weak, nonatomic) IBOutlet UIButton *CloseButton;
@property(strong,nonatomic) NSArray *PlaceHolderList;


@property(nonatomic) NSString* ServiceType;
@property(nonatomic) NSString* OldValue;
@property(strong, nonatomic) CategorySubscription *categorySubscription;
//For Category Selection Page
@property (weak, nonatomic) IBOutlet UIView *SubView;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;

@property(strong,nonatomic) NSMutableArray *SelectedCategoryIDs;
@property(strong,nonatomic) NSMutableDictionary *SubButtonList;


@end
