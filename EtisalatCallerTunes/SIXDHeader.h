//
//  SIXDHeader.h
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 15/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#ifndef SIXDHeader_h
#define SIXDHeader_h




#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



//DEVICE SIZE
#define SMALL       1
#define MEDIUM      2
#define LARGE       3
#define iPHONE_X    4



//colours
#define CLR_WHITE                           0xffffff
#define CLR_BLACK                           0x000000
#define CLR_ATHENS_GRAY                     0xeceff4
#define CLR_CELL_BORDER                     0xDFE3E6
#define CLR_GREY_HEADING                    0x282A2B
#define CLR_ETISALAT_GREEN                  0x719e2a
#define CLR_GREEN_SELECTED_TAB_TEXT         0xC3CC3F
#define CLR_GREEN_SELECTED_TAB_LINE         0xB7C431
#define CLR_TABBAR_TINT                     0xE2E2DB
#define CLR_RED                             0xFF0000
#define CLR_FAQ_1                           0xF1F1EB
#define CLR_FAQ_2                           0xFFFFFF
#define CLR_BUY                             0x353738
#define CLR_BANNER_IMAGE_TINT               0x151515
#define CLR_LIGHT_GRAY                      0xaeafaf
#define CLR_DISABLE_GRAY                    0xDDDEE1
#define CLR_DIM_GRAY                        0x696969
#define CLR_BoxBorder                       0xd1d2d1

#define CLR_SEGMENT_TAB_SELECTED            0x719e19
#define CLR_SEGMENT_TAB_UNSELECTED          0x6B6B6B

//LOGIN SCREEN COLOURS
#define  CLR_PLACEHOLDER                    0x616b1e
#define  CLR_TEXTFIELDLABEL                 0x858585
#define  CLR_ALERTRED                       0xFF0000
#define  CLR_TEXTFIELDBORDER                0xD1D2D1
#define  CLR_DISABLE_TEXTFIELD              0xE3ECD1



//Pop Up background color
#define POPUP_BACKGROUND                    0xF1F1EB

//LOGIN SCREEN FONTS
#define  FONT_TEXTFIELD                     @"ArialMT"




//Global Alert Actions
#define GLOBAL_ALERT_ACTION_SELF_LOAD       100
#define GLOBAL_ALERT_ACTION_SELF_DISMISS    101


//temp like count

#define LIKE_COUNNT_DEFAULT    @"0"
#define DEFAULT_CATEGORY_ID     @"0"


//Home Page Containers // SOURCE
#define LISTTONEHORIZONTAL              1
#define LISTTONEVERTICAL                2
#define TOPCHARTS                       3
#define CATEGORIES                      4
#define PLAYLIST                        5
#define MUSICBOX                        6
#define SUBCATEGORIES                   7
#define DEALS                           8
#define CHANGE_APP_LANGUAGE             9
#define CHANGE_NOTIFICATION_LANGUAGE    10
#define WISHLIST                        11
#define TUNE                            12
#define SALATI                          13
#define LISTTONEHORIZONTAL_SC           14
#define LISTTONEVERTICAL_SC             15



//SERVICE TYPE FOR EDIT PROFILE

#define UPDATE_USER_NAME    @"UPDATE_USER_NAME"
#define UPDATE_CATAGORIES   @"UPDATE_CATAGORIES"
#define UPDATE_EMAIL        @"UPDATE_EMAIL"
#define UPDATE_PROFILE_PIC  @"UPDATE_PROFILE_PIC"




//API IDENTIFIERS
#define SETTINGS                            1
#define GET_TONES                           2
#define GET_CATEGORIES                      3
#define PASSWORD_CHANGE                     4
#define CREATE_ACCOUNT                      5
#define PASSWORD_VALIDATE                   6
#define SECURITY_TOKEN                      7
#define REGISTRATION                        8
#define GET_TONE_PRICE                      9
#define BUY_TONE                            10
#define SET_TONE                            11
#define ADD_BLACKLIST                       12
#define REMOVE_BLACKLIST                    13
#define VIEW_BLACKLIST                      14
#define BUY_PLAYLIST                        15
#define CREATE_GROUP                        16
#define EDIT_GROUP                          17
#define DELETE_GROUP                        18
#define LIST_TONES_TONES                    19
#define LIST_TONES_PLAYLIST                 20
#define LIST_KARAOKE                        21
#define COPY_TUNE                           22
#define REFERRAL                            23
#define GET_PROFILE_DETAILS                 24
#define GET_PACK_DETAILS                    25
#define CDP_SEARCH_ARTIST                   26
#define VIEW_WISHLIST_TUNES                 27
#define REMOVE_WISHLIST                     28
#define GET_PREFERED_CATEGORY_STATIC_LIST   29
#define UPDATE_TUNE_LIKE_LIST               30
#define CDP_SEARCH_NORMAL                   31
#define CHECK_OTP                           32
#define SEND_OTP                            33
#define VIEW_WISHLIST_PLAYLIST              34
#define DELETE_TONE                         35
#define ADDTONE_TO_SHUFFLE                  36
#define DELETETONE_FROM_SHUFFLE             37
#define BANNER_SEARCH_ARTIST_SONG_PROMO     38
#define GET_BANNER_DETAILS_PB               39
#define GET_BANNER_DETAILS_MB               40
#define UPLOAD_IMAGE                        41
#define VIEW_WISHLIST_PREBOOK               42
#define PREBOOK_DELETION                    43
#define GET_BANNER_MUSICBOX                 44
#define UPGRADE_VIP                         45

/*
#define KARAOKE_UPLOAD                      45
#define GET_TONE_PRICE_KARAOKE              46
 
#define SALATI_SUBSCRIBE                    47
#define SALATI_CHECK                        48
#define GET_TONE_PRICE_SALATI               49
 */




//SOURCE IDENTIFIERS
#define FROM_CREATE_GROUP   100
#define FROM_EDIT_GROUP     101

#define FOR_TIME            201
#define FOR_PEOPLE          202



//REQUEST PARAMS
//#define APP_ID          @"com.sixdee.mytone"
// prod : com.sixdee.uaecallertunes
#define APP_ID          @"com.sixdee.callertunes_etisalat"
#define APP_VERSION     @"1.2"
#define VERSION_CODE    @"5.0"
#define OS              @"ios"

//OTHER CONFIGURATIONS
#define CHANNEL_ID                  @"9"
#define DEFAULT_GROUP_TONE_ID       @"9784582"





//SCROLLVIEW INSETS
#define MAX_IMAGE_HEIGHT            250
#define MIN_IMAGE_HEIGHT            64
//#define MIN_IMAGE_HEIGHT_IPHONE_X   85


//LIKE Settings return for filling likeHeart button

#define MAKE_LIKE_BUTTON_SOLID      1201
#define MAKE_LIKE_BUTTON_OPAQUE     1202

//Like settings actions
#define DO_NOT_SEND_LIKE_UNLIKE_LIST 1210
#define DOWNLOAD_TUNE_LIST          1211


//Time based settings to check what buttins to display

#define TIME_REPEAT_DISABLE_MONTHLY     300
#define TIME_REPEAT_DISABLE_YEARLY      301
#define TIME_REPEAT_DISABLE_ALL         302


//--- // Sorce for time based

#define SRC_DATE_PICKER 320
#define SRC_NORMAL_PICKER 321
#define SRC_TIME_PICKER     322

//SelectTimeTypeFromPopUp

#define POPUP_TIME_TYPE_FULL_DAY        900
#define POPUP_TIME_TYPE_SPECIFIC_TIME   901
#define POPUP_TIME_TYPE_SPECIFIC_DATE   902
#endif /* SIXDHeader_h */

// Settings screen type
#define FIRST_SETTINGS_SCREEN                       31
#define END_SETTINGS_SCREEN                         32
#define PLAYING_TO_CALLERS_SETTINGS_SCREEN          33

//time settings type

#define SETTINGS_DAILY_TIME         1
#define SETTINGS_WEEKLY_TIME        2
#define SETTINGS_MONTLY_TIME        3
#define SETTINGS_YEARLY_TIME        4
#define SETTINGS_BETWEEN_DATES      7
#define SETTINGS_SPECIFIC_TIME      5

//user settings type

#define SETTINGS_USER_DEDICATED    100
#define SETTINGS_USER_GROUPS       101
#define SETTINGS_USER_ALL_CALLERS       102

//sorce for like settings
#define SRC_HOME_PAGE 10
#define SRC_LOGOUT      11
#define SRC_PERIODIC      12

//social media type

#define FACEBOOK    1
#define TWITTER     2

// Keyboard type for ASCII

#define ASCII_NUM_KEYBOARD  UIKeyboardTypeASCIICapableNumberPad
#define ASCII_TXT_KEYBOARD  UIKeyboardTypeASCIICapable


