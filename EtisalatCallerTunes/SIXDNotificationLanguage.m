//
//  SIXDNotificationLanguage.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/12/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDNotificationLanguage.h"
#import "AppDelegate.h"
#import "SIXDStylist.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDProfileVC.h"

@interface SIXDNotificationLanguage ()

@end

@implementation SIXDNotificationLanguage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Edit_Profile_NotificationLanguage"];
    
     [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.95]];
    _AlertLabel.hidden=YES;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                nil];
    [_SegmentBar setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_TEXTFIELDBORDER) forKey:NSForegroundColorAttributeName];
    [_SegmentBar setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentBar setTitleTextAttributes:attributes forState:UIControlStateNormal];
        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_TEXTFIELDBORDER) forKey:NSForegroundColorAttributeName];
        [_SegmentBar setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    }
    else
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"GESSTextMedium-Medium" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentBar setTitleTextAttributes:attributes forState:UIControlStateNormal];
        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_TEXTFIELDBORDER) forKey:NSForegroundColorAttributeName];
        [_SegmentBar setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    }
    */
    _SegmentBar.layer.cornerRadius = 5;
    _SegmentBar.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER)  CGColor];
    _SegmentBar.layer.borderWidth = 1.0f;
    _SegmentBar.layer.masksToBounds = YES;
    
    [_SegmentBar setBackgroundColor:[UIColor whiteColor]];
    [_SegmentBar setBackgroundColor:[UIColor whiteColor]];
    [_SegmentBar setTintColor:UIColorFromRGB(CLR_PLACEHOLDER)];
    //[_SegmentBar setTintColor:[UIColor clearColor]];
    
    
    
    [_SegmentBar setTitle:NSLocalizedStringFromTableInBundle(@"English",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentBar setTitle:NSLocalizedStringFromTableInBundle(@"Arabic",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"UPDATE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
   
    /*
    NSArray *array = [_SegmentBar subviews];
    UIView *Temp;
    Temp.backgroundColor = UIColorFromRGB(CLR_PLACEHOLDER);
    */
    
    if(_Source == CHANGE_APP_LANGUAGE)
    {
        _TitleLabel.text = NSLocalizedStringFromTableInBundle(@"CHANGE LANGUAGE",nil,appDelegate.LocalBundel,nil);
        NSString *AppLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguage"];
        if([AppLanguage isEqualToString:@"en"])
        {
            [_SegmentBar setSelectedSegmentIndex:0];
            //Temp = [array objectAtIndex:0];
        }
        else
        {
            [_SegmentBar setSelectedSegmentIndex:1];
            //Temp = [array objectAtIndex:1];
        }
    }
    else
    {
        _TitleLabel.text = NSLocalizedStringFromTableInBundle(@"NOTIFICATION LANGUAGE",nil,appDelegate.LocalBundel,nil);
        //TO DO
        if([_NotificationLanguage isEqualToString:@"ENGLISH"])
        {
            [_SegmentBar setSelectedSegmentIndex:0];
            //Temp = [array objectAtIndex:0];
        }
        else
        {
            [_SegmentBar setSelectedSegmentIndex:1];
            //Temp = [array objectAtIndex:1];
        }
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender
{
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_Source == CHANGE_APP_LANGUAGE)
    {
    switch (_SegmentBar.selectedSegmentIndex)
    {
        case 0:
            appDelegate.Language = @"English";
            [self reloadApp :@"en"];
            break;
            
        case 1:
            appDelegate.Language = @"Arabic";
            [self reloadApp :@"ar"];
            break;
            
        default:
            break;
    }
    }
    else
    {
        //TO DO
    }
     */
    _AlertLabel.hidden=YES;
}

-(void)reloadApp: (NSString*)Language
{
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    
    /*
     [appDelegate.SuperView.navigationController popToRootViewControllerAnimated:NO];
     
     [appDelegate.SuperView.navigationController.view removeFromSuperview];
     */
    
    [[NSUserDefaults standardUserDefaults] setObject:Language forKey:@"SelectedLanguage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if([Language isEqualToString:@"en"])
    {
        appDelegate.Language= @"English";
        appDelegate.LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentLeft];
        
        
    }
    else if ([Language isEqualToString:@"ar"])
    {
        appDelegate.Language = @"Arabic";
        appDelegate.LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentRight];
        
    }
    
    [SIXDStylist loadCustomFonts];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
    
     UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
           navController.navigationBarHidden = YES;
           appDelegate.window.rootViewController = navController;
    [appDelegate.window makeKeyAndVisible];
    
}


-(void)editProfile
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Selectedlanguage;
    if(_SegmentBar.selectedSegmentIndex == 0)
    {
        Selectedlanguage = @"1";
    }
    else
    {
        Selectedlanguage = @"2";
    }
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&channelId=%@&notificationlanguage=%@&aPartyMsisdn=%@",Rno,CHANNEL_ID,Selectedlanguage,appDelegate.MobileNumber];
   
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/change-language",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    if(_Source == CHANGE_NOTIFICATION_LANGUAGE)
    {
        NSLog(@"CHANGE_NOTIFICATION_LANGUAGE >>>");
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(_SegmentBar.selectedSegmentIndex == 0)
        {
            [_ProfileVC.ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"English",nil,appDelegate.LocalBundel,nil) forKey:@"NotificationLanguage"];
        }
        else
        {
            [_ProfileVC.ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"Arabic",nil,appDelegate.LocalBundel,nil) forKey:@"NotificationLanguage"];
        }
        
        [_ProfileVC.TableView reloadData];
        
    }
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    [_ActivityIndicator stopAnimating];
    
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    //[SIXDGlobalViewController showCommonFailureResponse:(ViewController *)self.presentingViewController :Message];
    _AlertLabel.hidden=NO;
    _AlertLabel.text =Message;
}


-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[SIXDGlobalViewController showCommonFailureResponse:self :nil];
    _AlertLabel.hidden=NO;
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :(ViewController *)self.presentingViewController];
}


- (IBAction)onSelectedConfirmButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.hidden=YES;
    if(_Source == CHANGE_APP_LANGUAGE)
    {
        if(([appDelegate.Language isEqualToString:@"English"]&&_SegmentBar.selectedSegmentIndex==0)||([appDelegate.Language isEqualToString:@"Arabic"]&&_SegmentBar.selectedSegmentIndex==1))
        {
            [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
            return;
        }
        else
        {
            switch (_SegmentBar.selectedSegmentIndex)
            {
                case 0:
                    NSLog(@"Index 0 English");
                    appDelegate.Language = @"English";
                    [self reloadApp :@"en"];
                    break;
                    
                case 1:
                    NSLog(@"Index 1 Arabic");
                    appDelegate.Language = @"Arabic";
                    [self reloadApp :@"ar"];
                    break;
                    
                default:
                    break;
            }
        }
    }
    else
    {
        [_ActivityIndicator startAnimating];
        _ConfirmButton.alpha = 0.3;
        [_ConfirmButton setUserInteractionEnabled:false];
        [self editProfile];
    }
    
}

- (IBAction)onSelectedCloseButton:(UIButton *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}


@end

