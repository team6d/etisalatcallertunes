//
//  SIXDNestedCVCell.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIXDNestedCVCell : UICollectionViewCell<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSMutableArray *TuneList;
@property(nonatomic) NSInteger section;

@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property int totalToneLikeCount;
@end
