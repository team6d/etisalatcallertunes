//
//  SIXDPageContentVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/21/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDPageContentVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDPreOrderVC.h"
#import "SIXDGenericPlayList.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"
#import "SIXDMusicChannelVC.h"
#import "SIXDSalatiVC.h"

@interface SIXDPageContentVC ()

@end

@implementation SIXDPageContentVC
 
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 
     self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
     self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
     
     //for testing
     NSURL *ImageURL = [NSURL URLWithString:[_BannerDetails objectForKey:@"bannerPath"]];
     //NSURL *ImageURL = [NSURL URLWithString:@"hjvgsdvugvcdsbvc"];
     [self.PageContentImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
     if(_Source != DEALS)
     {

         [_PageContentImage setUserInteractionEnabled:YES];
         UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedBannerImage:)];
         [singleTap setNumberOfTapsRequired:1];
         [_PageContentImage addGestureRecognizer:singleTap];
     }
 }
 
 - (void)didReceiveMemoryWarning {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }
 
 -(void)selectedBannerImage:(UIGestureRecognizer *)recognizer
 {
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     
     if([[_BannerDetails objectForKey:@"type"] isEqualToString:@"Pre-Booking"])
     {
         [SIXDStylist setGAScreenTracker:@"Prebook_Banner"];
         appDelegate.APIFlag=GET_BANNER_DETAILS_PB;
         [self API_GetBannerDetails];
     }
     else if ([[_BannerDetails objectForKey:@"type"] isEqualToString:@"Manual"])
     {
         [SIXDStylist setGAScreenTracker:@"Manual_Banner"];
         appDelegate.APIFlag=GET_BANNER_DETAILS_MB;
         [self API_GetBannerDetails];
     }
     else if ([[_BannerDetails objectForKey:@"type"] isEqualToString:@"Music Box"])
     {
         [SIXDStylist setGAScreenTracker:@"MusicChannel_Banner"];
         appDelegate.APIFlag=GET_BANNER_MUSICBOX;
         [self getMusicChannelList];
     }
     else if ([[_BannerDetails objectForKey:@"type"] isEqualToString:@"SALATI"])
     {
         [SIXDStylist setGAScreenTracker:@"Salati_Banner"];
         [appDelegate.MainTabController setSelectedIndex:2];
     }
     else
     {
         [SIXDStylist setGAScreenTracker:@"Featured_Banner"];
         appDelegate.APIFlag = BANNER_SEARCH_ARTIST_SONG_PROMO;
         [self API_BannerSearch];
     }
}

-(void)API_BannerSearch
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.view addSubview:self.overlayview];
    [appDelegate.HomeObj.ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:appDelegate.HomeObj.ActivityIndicator];
    
    NSLog(@"API_BannerSearch API >>");
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/banner-search?language=%@&type=%@&pageNo=1&perPageCount=20",appDelegate.BaseMiddlewareURL,appDelegate.Language,[_BannerDetails objectForKey:@"type"]];
    NSLog(@"API_BannerSearch final URL = %@",ReqHandler.FinalURL);
    ReqHandler.HttpsReqType = GET;
    [ReqHandler sendHttpGETRequest:self :[_BannerDetails objectForKey:@"searchKey"]];
}

-(void)API_GetBannerDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.view addSubview:self.overlayview];
    [appDelegate.HomeObj.ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:appDelegate.HomeObj.ActivityIndicator];
    
    NSLog(@"API_PreBooking API >>");
    
    //READ ME:
    //PREBOOKING   : ARABIC:5   ENGLISH:4
    //MANUAL BANNER: ARABIC:6   ENGLISH:3
    
    int type;
    if([[_BannerDetails objectForKey:@"type"] isEqualToString:@"Pre-Booking"])
    {
        if([appDelegate.Language isEqualToString:@"English"])
        {
            type=4;
        }
        else
        {
            type=5;
        }
    }
    else
    {
        if([appDelegate.Language isEqualToString:@"English"])
        {
            type=3;
        }
        else
        {
            type=6;
        }
    }
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"identifier=GetBannerSongs&bannerOrder=%@&bannerType=%d&language=%@",[_BannerDetails objectForKey:@"bannerOrder"],type,appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-banner-details",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)getMusicChannelList
{
    NSLog(@"getMusicChannelList>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.view addSubview:self.overlayview];
    [appDelegate.HomeObj.ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:appDelegate.HomeObj.ActivityIndicator];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    NSString *Rno2 = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"toneCode=%@&type=MB&isAll=T&contentTypeId=2&clientTxnId=%@&clientSessionId=%@&language=%@",[_BannerDetails objectForKey:@"searchKey"],Rno1,Rno2,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-play-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.HomeObj.ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSLog(@"onSuccessResponseRecived = %@",Response);
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    
    if(appDelegate.APIFlag == BANNER_SEARCH_ARTIST_SONG_PROMO)
    {
        _BannerDetailsList =[[responseMap objectForKey:@"searchList"]mutableCopy];
        SIXDGenericPlayList  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGenericPlayList"];
        ViewController.Selected_TuneList = _BannerDetailsList;
        ViewController.SubCategoryName = [_BannerDetails objectForKey:@"type"];
        appDelegate.CategoryName = [_BannerDetails objectForKey:@"type"];
        appDelegate.CategoryImagePath = [_BannerDetails objectForKey:@"bannerPath"];
        ViewController.IsSegmentTabRequired = false;
        ViewController.IsSourceBanner = TRUE;
        
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
    else if(appDelegate.APIFlag == GET_BANNER_DETAILS_MB)
    {
        _BannerDetailsList =[[responseMap objectForKey:@"bannerDetailsList"]mutableCopy];
        SIXDGenericPlayList  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGenericPlayList"];
        ViewController.SubCategoryName = [_BannerDetails objectForKey:@"type"];
        appDelegate.CategoryName = [_BannerDetails objectForKey:@"type"];
        appDelegate.CategoryImagePath = [_BannerDetails objectForKey:@"bannerPath"];
        ViewController.IsSegmentTabRequired = false;
        ViewController.IsSourceBanner = TRUE;
        
        NSMutableArray *Array = [NSMutableArray new];
        for(int i=0;i<_BannerDetailsList.count;i++)
        {
            NSMutableDictionary *DummyDict = [NSMutableDictionary new];
            NSMutableDictionary *Dict = [_BannerDetailsList objectAtIndex:i];
            
            [DummyDict setValue:[Dict objectForKey:@"contentName"] forKey:@"toneName"];
            [DummyDict setValue:[Dict objectForKey:@"artist"] forKey:@"artistName"];
            [DummyDict setValue:[Dict objectForKey:@"contentId"] forKey:@"toneId"];
            [DummyDict setValue:[NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_TONE_URL"],[Dict objectForKey:@"path"]] forKey:@"toneUrl"];
            [DummyDict setValue:[NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_IMAGE_URL"],[Dict objectForKey:@"previewImage"]] forKey:@"previewImageUrl"];
            [Array addObject:DummyDict];
        }
        NSLog(@"Manual Banner response modified = %@",Array);
        ViewController.Selected_TuneList = Array;
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
    else if(appDelegate.APIFlag == GET_BANNER_DETAILS_PB)
    {
        _BannerDetailsList =[[responseMap objectForKey:@"bannerDetailsList"]mutableCopy];
        SIXDPreOrderVC  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPreOrderVC"];
        appDelegate.CategoryImagePath = [_BannerDetails objectForKey:@"bannerPath"];
        
        NSMutableArray *Array = [NSMutableArray new];
        for(int i=0;i<_BannerDetailsList.count;i++)
        {
            NSMutableDictionary *DummyDict = [NSMutableDictionary new];
            NSMutableDictionary *Dict = [_BannerDetailsList objectAtIndex:i];
            
            [DummyDict setValue:[Dict objectForKey:@"contentName"] forKey:@"toneName"];
            [DummyDict setValue:[Dict objectForKey:@"artist"] forKey:@"artistName"];
            [DummyDict setValue:[Dict objectForKey:@"contentId"] forKey:@"toneId"];
            [DummyDict setValue:[NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_TONE_URL"],[Dict objectForKey:@"path"]] forKey:@"toneUrl"];
            [DummyDict setValue:[NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_IMAGE_URL"],[Dict objectForKey:@"previewImage"]] forKey:@"previewImageUrl"];
            [Array addObject:DummyDict];
        }
        NSLog(@"Pre-Booking response modified = %@",Array);
        ViewController.BannerDetails = Array;
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
    else if(appDelegate.APIFlag == GET_BANNER_MUSICBOX)
    {
        _BannerDetailsList =[[responseMap objectForKey:@"categoryList"]mutableCopy];
        SIXDMusicChannelVC  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMusicChannelVC"];
        ViewController.IsSourceBanner = TRUE;
        ViewController.PlayListDetails = [_BannerDetailsList objectAtIndex:0];
        
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.HomeObj.ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:appDelegate.HomeObj :Message];
    NSLog(@"onFailureResponseRecived Response = %@",Response);
}

-(void)onFailedToConnectToServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.HomeObj.ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.HomeObj.ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
