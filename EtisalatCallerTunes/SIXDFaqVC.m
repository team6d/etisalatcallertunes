//
//  SIXDFaqVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDFaqVC.h"
#import "AppDelegate.h"
#import "SIXDCustomTVCell.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDStylist.h"

@interface SIXDFaqVC ()

@end

@implementation SIXDFaqVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_FAQ"];
    
    [self.navigationController.navigationBar setBarTintColor: [UIColor whiteColor]];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _tableView.contentInset = UIEdgeInsetsMake(0,0,50,0);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"FAQ",nil,appDelegate.LocalBundel,nil);
    
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    //[_ActivityIndicator setHidden:true];
    
    
    
    _SelectedRow = -1;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    _Questions = [NSMutableArray new];
    _Answers = [NSMutableArray new];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 200;
}

- (NSString *)stringByRemovingControlCharacters: (NSString *)inputString
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [inputString rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:inputString];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return inputString;
}


-(void) hideAndDisableLeftNavigationItem
{
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}





-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError>>>");
    [self showPopUp];
}

-(void)showPopUp
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.ParentView = self;
    ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _Questions.count;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    NSString *Temp1 = [NSString stringWithFormat:@"\n%@\n",[[_Questions objectAtIndex:indexPath.row]capitalizedString]];
    cell.TitleLabel.text = Temp1 ;
    
    //[cell.Artist sizeToFit];
    
    
    
    if(_SelectedRow == indexPath.row)
    {
        cell.SubTitleLabel.text = [[_Answers objectAtIndex:indexPath.row]capitalizedString];
        
        NSString *Temp = [NSString stringWithFormat:@"\n%@\n\n",[[_Answers objectAtIndex:indexPath.row]capitalizedString]];
        cell.SubTitleLabel.text = Temp;
        [cell.SubTitleLabel sizeToFit];
        
    }
    else
    {
        cell.SubTitleLabel.text = @"";
        [cell.SubTitleLabel sizeToFit];
        
    }
    
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_SelectedRow != indexPath.row)
    {
        
        UIImage  *image = [[UIImage imageNamed:@"accoudionopen.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        
        [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_LIGHT_GRAY)];
        
    }
    else
    {
        
        UIImage *image = [[UIImage imageNamed:@"accoudionclose.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_LIGHT_GRAY)];
        
        
    }
    cell.AccessoryButton.tag = indexPath.row;
    
    
    if([appDelegate.Language isEqualToString:@"English"])
    {
        
        cell.TitleLabel.textAlignment = NSTextAlignmentLeft;
        cell.SubTitleLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    else
    {
        
        cell.TitleLabel.textAlignment = NSTextAlignmentRight;
        cell.SubTitleLabel.textAlignment = NSTextAlignmentRight;
        
    }
    
    if(indexPath.row%2==0)
    {
        cell.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
    }
    else
    {
        cell.backgroundColor = UIColorFromRGB(CLR_FAQ_2);
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}



- (IBAction)onClickedDownButton:(UIButton *)sender
{
    
    if(_SelectedRow == sender.tag)
    {
        _SelectedRow = -1;
        [self.tableView reloadData];
        
    }
    else
    {
        _SelectedRow = sender.tag;
        
        UIImage *image = [[UIImage imageNamed:@"accoudionclose.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [sender setImage:image forState:UIControlStateNormal];
        [sender.imageView setTintColor:[UIColor whiteColor]];
        [self.tableView reloadData];
    }
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_SelectedRow == indexPath.row)
    {
        _SelectedRow = -1;
        [self.tableView reloadData];
        
    }
    else
    {
        _SelectedRow = indexPath.row;
        [self.tableView reloadData];
    }
    
}


-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *path = [NSString stringWithFormat:@"%@faq_iosformat.json",[appDelegate.AppSettings objectForKey:@"FAQURL"]];
    
    //added 03082017
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              [_ActivityIndicator stopAnimating];
                                              [self.overlayview removeFromSuperview];
                                              
                                              if(error)
                                              {
                                                  [self showPopUp];
                                              }
                                              else
                                              {
                                                  NSError *err = nil;
                                                  NSMutableDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
                                                  
                                                  
                                                  if(err != nil)
                                                  {
                                                      NSLog(@"Recievd error %@",err);
                                                      [self showPopUp];
                                                  }
                                                  
                                                  response = [response objectForKey:@"responseMap"];
                                                  NSLog(@"Result = %@",response);
                                                  NSMutableArray *FAQList = [response objectForKey:@"faqList"];
                                                  for(int i=0;i<FAQList.count;i++)
                                                  {
                                                      NSMutableDictionary *Dict = [FAQList objectAtIndex:i];
                                                      [_Questions addObject:[Dict objectForKey:@"question"]];
                                                      [_Answers addObject:[Dict objectForKey:@"answer"]];
                                                      
                                                  }
                                                  [_tableView reloadData];
                                              }
                                              
                                          }];
    [postDataTask resume];
    
 
    
}

/*
- (IBAction)onSelectedBackButton:(id)sender
{
    [self LoadMainTabController:0];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
}
 */

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    NSLog(@"HERE didReceiveChallenge URLSession>>>");
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        //if ([trustedHosts containsObject:challenge.protectionSpace.host])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

-(void)onSelectedPopUpConfirmButton
{
    [self.navigationController popViewControllerAnimated:false];
}

@end
