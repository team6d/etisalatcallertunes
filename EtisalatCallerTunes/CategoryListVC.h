//
//  CategoryListVC.h
//  6dCRBTApp
//
//  Created by Shahnawaz Lone on 9/15/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface CategoryListVC : ViewController <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(strong,nonatomic) NSMutableArray *CategoryList;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property(nonatomic) NSInteger Index;
@property(nonatomic) BOOL IsResponseRecieved;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TableViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TableViewWidth;

-(void)loadCategories;
@end
