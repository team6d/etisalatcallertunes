//
//  SIXDSignInVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSignInVC.h"
#import "SIXDStylist.h"
#import "SIXDHeader.h"
#import "SIXDVerifyOtpVC.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"

@interface SIXDSignInVC ()

@end

@implementation SIXDSignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [SIXDStylist setGAScreenTracker:@"Sign_In"];
    
    [SIXDStylist setBackGroundImage:self.view];
    
    
    [_NumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    _NumberTextField.textColor = UIColorFromRGB(CLR_PLACEHOLDER);
    
    [_PasswordTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    _PasswordTextField.keyboardType = ASCII_TXT_KEYBOARD;
    
    [self InitialCondition];
    [self localisation];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _NumberTextField.text = appDelegate.MobileNumber;
    _NumberTextField.userInteractionEnabled=FALSE;
    
    _PasswordTextField.enablesReturnKeyAutomatically = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _NumberTextField.delegate = self;
    _PasswordTextField.delegate = self;
   
    _AlertLabel.hidden = YES;
    _PasswordLabel.hidden = YES;
    _TopSpaceToNumber.constant=19;
    
    _NumberView.layer.borderWidth = 1.0f;
    _NumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
    _NumberView.alpha=0.95;
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_PasswordTextField resignFirstResponder];

    if(_PasswordTextField.text.length==0)
    {
        [self InitialCondition];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_PasswordTextField.text.length==0)
    {
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_AlertButton];
    }
    else
    {
        [SIXDStylist setGAEventTracker:@"Sign in" :@"Sign in confirmation" :nil];
        _AlertLabel.text=@"";
        NSLog(@"API_GetSecurityToken call >>");
        [_PasswordTextField resignFirstResponder];
        [self API_GetSecurityToken];
    }
}

- (IBAction)onClickedForgotButton:(UIButton *)sender
{
    [self InitialCondition];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDVerifyOtpVC"];
    [self.navigationController pushViewController:ViewController animated:true];
    //[self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onClickedAlertButton:(UIButton *)sender
{
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _TopSpaceToTextField.constant=19;
    _PasswordLabel.hidden=NO;
    [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: UIColorFromRGB(CLR_PLACEHOLDER)}]];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_PasswordTextField.text.length==0)
    {
        [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _PasswordLabel.hidden=YES;
        _TopSpaceToTextField.constant=0;
    }
}

-(void)API_PasswordValidation
{
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=PASSWORD_VALIDATE;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.IsURLEncodingNotRequired = true;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&language=%@&clientTxnId=%@&type=ValidateDetails&securityCounter=%@&encryptedPassword=%@&os=ios&notificationTrackingId=%@",appDelegate.MobileNumber,appDelegate.Language,Rno,_SecurityToken,_EncryptedPassword,appDelegate.FCMToken];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/password-validation",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_GetSecurityToken
{
    NSLog(@"API_GetSecurityToken SIXDSignInVC >>");
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SECURITY_TOKEN;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/security-token",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?",URL];
    [ReqHandler sendHttpGETRequest:self :nil];
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==SECURITY_TOKEN)
    {
    NSLog(@"API_GetSecurityToken onSuccessResponseRecived = %@",Response);
        
        
        _SecurityToken =[responseMap objectForKey:@"securityCounter"];
        appDelegate.SecurityToken=_SecurityToken;
        [self EncryptPassord];
        
    }
    else
    {
        [_ActivityIndicator stopAnimating];
        [self.overlayview removeFromSuperview];
        
        NSLog(@"API_PasswordValidation onSuccessResponseRecived = %@",Response);
        [self InitialCondition];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSString *str1 = [responseMap objectForKey:@"accessToken"];
        
        if(str1.length > 0)
            appDelegate.AccessToken = str1;
        
        str1 = [responseMap objectForKey:@"deviceId"];
        
        
        double ExipryTime;
        str1 = [responseMap objectForKey:@"expiry"];
        ExipryTime = [str1 doubleValue];
        ExipryTime = ExipryTime/1000;
        
        //ExipryTime = atol(str1);
        
        
        str1 = [responseMap objectForKey:@"otp"];
        
       
        
        str1 = [responseMap objectForKey:@"refreshToken"];
        
        if(str1.length > 0)
            appDelegate.RefreshToken = str1;
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.AccessToken forKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.RefreshToken forKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MobileNumber"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.MobileNumber forKey:@"MobileNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
        //ExipryTime = Time + ExipryTime;
        ExipryTime = Time + ExipryTime - 10 ;
        
        NSString *Str = [NSString stringWithFormat:@"%f",ExipryTime];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] setObject:Str forKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
                
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
        [[NSUserDefaults standardUserDefaults] setObject:@"LOGIN" forKey:@"State"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        /*
        
         UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
          
          UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
          
          //UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
         // UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDDatePickerPopUpVC"];
          
          self.window.rootViewController = ViewController;
          [self.window makeKeyAndVisible];
        
        */
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
        //[self presentViewController:ViewController animated:YES completion:nil];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
        navController.navigationBarHidden = YES;
        appDelegate.window.rootViewController = navController;
        //appDelegate.window.rootViewController = ViewController;
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *message =[Response objectForKey:@"message"];
    _AlertLabel.text= message;
    _AlertLabel.hidden=NO;
    
    [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_AlertButton];
    
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) :self];
}

-(void)EncryptPassord
{
    NSURL *jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt" withExtension:@"js"];
    NSString *jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    NSString *oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main.js";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:nil ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt.min" withExtension:@"js"];
    jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main1.js";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    NSString *Temp = [NSString stringWithFormat:@"%@%@",_PasswordTextField.text,_SecurityToken];
    
    NSLog(@"Password = %@",Temp);
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"SearchKey" withString:Temp];
    _webView = [UIWebView new];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    _webView.delegate = self;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *pwd = [_webView stringByEvaluatingJavaScriptFromString:@"getLanguage()"];
   
   /* _EncryptedPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,(CFStringRef)pwd,
                                                                                               NULL,
                                                                                               (CFStringRef)@"+",
                                                                                               kCFStringEncodingUTF8 ));
    */
    
    
   /* NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@":/?&=;+!@#$()',*% "
                                   ].invertedSet;
    */
    
    
    // Forcefully converting '+' to URLencoding form
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
   _EncryptedPassword = [pwd
                            stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                            ];
    
    
    NSLog(@"_EncryptedPassword = %@",_EncryptedPassword);
    
    //end
    
    
    //will not work, '+' is allowed in URLQueryAllowedCharacterSet('+' implies space), hence will not convert '+' to encoded form.
   //_EncryptedPassword = [pwd stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSLog(@"API_PasswordValidation call >>");
    [self API_PasswordValidation];
    
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"SIGN IN",nil,appDelegate.LocalBundel,nil);
    _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
    _PasswordLabel.text = NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil);
    
    [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Oops! I forgot my password",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:FONT_TEXTFIELD size:13]}];
    
    [_ForgotButton setAttributedTitle:titleString forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_AlertButton setImage:image forState:UIControlStateNormal];
}

-(void)InitialCondition
{
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_PasswordTextField :_PasswordView :_AlertButton];
}


- (IBAction)onClickBackButton:(id)sender {
    
    //[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"onInternetBroken::");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
@end
