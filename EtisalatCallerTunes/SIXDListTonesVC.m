//
//  SIXDListTonesVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDListTonesVC.h"
#import "AppDelegate.h"
#import "CustomLabel.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDPlayerVC.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDSubCategories.h"
#import "SIXDBuyToneVC.h"
#import "SIXDGenericPlayList.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"

@interface SIXDListTonesVC ()

@end

@implementation SIXDListTonesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    /*
    //added
    CGSize size ;
    size.width = self.TableView.frame.size.width;
    
    if([self.restorationIdentifier isEqualToString:@"HorizontalListTonesVC"])
        size.height = (190*1)+55;
    else
        size.height = (138*1)+55;
    
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.contentSize = _TableView.frame.size;
    
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
    */
    //for testing
    //[self loadTones];
    //end
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     CGRect frame = tableView.frame;
    
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, frame.size.height)];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHeading_13 *title;
    UIButton *SeeAll;
    
    if(_TuneList.count != 0)
    {
    
    if([appDelegate.Language isEqualToString:@"English"])
    {
        title = [[SIXDHeading_13 alloc] initWithFrame:CGRectMake(30, 0,[UIScreen mainScreen].bounds.size.width-60,55)];
         SeeAll =[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-75,0,50,55)];
        title.textAlignment = NSTextAlignmentLeft;
        [SeeAll setImage:[UIImage imageNamed:@"seeallarrow"] forState:UIControlStateNormal];
        
    }
    else
    {    title = [[SIXDHeading_13 alloc] initWithFrame:CGRectMake(85,0,[UIScreen mainScreen].bounds.size.width-110,55)];
        SeeAll =[[UIButton alloc] initWithFrame:CGRectMake(30, 0,50,55)];
        title.textAlignment = NSTextAlignmentRight;
        [SeeAll setImage:[UIImage imageNamed:@"seeallarrow_Arabic"] forState:UIControlStateNormal];
    }
   
    [title setTextColor:UIColorFromRGB(CLR_GREY_HEADING)];
    [title setBackgroundColor:[UIColor clearColor]];
    
    
            //for testing
    if(_Source != SUBCATEGORIES)
    {
        title = [[SIXDHeading_13 alloc] initWithFrame:CGRectMake(30, 0,[UIScreen mainScreen].bounds.size.width-60,55)];
        [title setTextColor:UIColorFromRGB(CLR_GREY_HEADING)];
        [title setTextAlignment:NSTextAlignmentCenter];
        
        if([self.restorationIdentifier isEqualToString:@"VerticalListTonesVC"])
            title.text = [appDelegate.AppSettings objectForKey:@"JustForYouNAME"];
        else
            title.text = [appDelegate.AppSettings objectForKey:@"FeaturedCategoryNAME"];
        
        //"FeaturedCategory_ENGLISH"
        //attribute = "New Releases,NewReleasesArabic,SpecialOffer";
        
        
    }
    else
    {
        title.text = [_SubCategoryTitle uppercaseString];
    }
    
    
    if(_Source == SUBCATEGORIES)
    {
        NSMutableAttributedString *titleString;
        NSMutableAttributedString *WhiteDot;
        NSMutableAttributedString *FinalString = [NSMutableAttributedString new];
        
        titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
        
        /*
        if([appDelegate.Language isEqualToString:@"English"])
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
            WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
        }
        else
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
            WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
        }
         */
        
        /*
        titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED)}];
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@". ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:[UIColor clearColor]}];
         */
        
        [FinalString appendAttributedString:WhiteDot];
        [FinalString appendAttributedString:titleString];
        [FinalString appendAttributedString:WhiteDot];
        [SeeAll setAttributedTitle:FinalString forState:UIControlStateNormal];
     /*
    [SeeAll setTitle:NSLocalizedStringFromTableInBundle(@"SEE ALL",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [SeeAll.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:8]];
    [SeeAll setTitleColor:UIColorFromRGB(CLR_SEGMENT_TAB_UNSELECTED) forState:UIControlStateNormal];
    */
    SeeAll.transform = CGAffineTransformScale(SeeAll.transform, -1.0f, 1.0f);
    SeeAll.titleLabel.transform = CGAffineTransformScale(SeeAll.titleLabel.transform, -1.0f, 1.0f);
    SeeAll.imageView.transform = CGAffineTransformScale(SeeAll.imageView.transform, -1.0f, 1.0f);
    SeeAll.tag = section;
    [SeeAll addTarget:self action:@selector(onSelectedSeeAllButton:) forControlEvents: UIControlEventTouchUpInside];
        [headerView addSubview:SeeAll];
        
    }
    
    }
    
    [headerView addSubview:title];
    
    return headerView;
    
}

-(void)getTones :(NSString*)Identifier
{
    [self.view addSubview:_overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    
    /*
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =   [NSString stringWithFormat:@"%@?language=%@&identifier=%@",URL,appDelegate.Language,Identifier];
     */
    
    /*
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&pageNo=1&perPageCount=20&sortBy=Order_By&alignBy=ASC",URL,appDelegate.Language,Identifier];
     */
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/get-recommendation-songs",appDelegate.BaseMiddlewareURL];
    
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&clientTxnId=%@&identifier=%@",URL,appDelegate.Language,appDelegate.MobileNumber,Rno,Identifier];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.CollectionView.tag = indexPath.row;
    
    cell.FirstRow = [_TuneList mutableCopy];
    
    if(_Source != SUBCATEGORIES)
    {
        if([self.restorationIdentifier isEqualToString:@"VerticalListTonesVC"])
            cell.Source = LISTTONEVERTICAL;
        else
            cell.Source = LISTTONEHORIZONTAL;
    }
    else
    {
        if([self.restorationIdentifier isEqualToString:@"VerticalListTonesVC"])
            cell.Source = LISTTONEVERTICAL_SC;
        
        else
            cell.Source = LISTTONEHORIZONTAL_SC;
        
    }
    
    [cell.CollectionView reloadData];
    NSLog(@"Section index = %ld",(long)indexPath.section);
    
    
    return cell;
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived getTones>> %@",Response);

    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    
    Response = [Response objectForKey:@"responseMap"];
    //_TuneList = [[Response objectForKey:@"searchList"]mutableCopy];
    _TuneList = [[Response objectForKey:@"recommendationSongsList"]mutableCopy];
    [_TableView reloadData];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    NSLog(@"onFailureResponseRecived getTones>>");
    //[self loadTones];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    NSLog(@"onFailedToConnectToServer getTones>>");
   // [self loadTones];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*
    CGSize size ;
    size.width = self.TableView.frame.size.width;
    
    if([self.restorationIdentifier isEqualToString:@"HorizontalListTonesVC"])
        size.height = (190*1)+55;
    else
        size.height = (138*1)+55;
    
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.contentSize = _TableView.frame.size;
    
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
     */
    
}

-(void)loadTones
{
    NSLog(@"loadTones >>");
    /*
    CGSize size ;
    size.width = self.TableView.frame.size.width;
    
    if([self.restorationIdentifier isEqualToString:@"HorizontalListTonesVC"])
        size.height = (190*1)+55;
    else
        size.height = (138*1)+55;
    
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.frame = CGRectMake(0, 0, size.width,size.height);
    _TableView.contentSize = _TableView.frame.size;
    */
    if(_TuneList.count == 0)
    {
        //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString * Identifier;
        if([self.restorationIdentifier isEqualToString:@"VerticalListTonesVC"])
        {
            //Identifier = [appDelegate.AppSettings objectForKey:@"JustForYouID"];
            Identifier = @"JUST_FOR_YOU";
        }
        else
        {
            //Identifier = [appDelegate.AppSettings objectForKey:@"FeaturedCategoryID"];
            Identifier = @"NewReleases";
        }
        
        [self getTones:Identifier];
    }
    else
    {
        [self.TableView reloadData];
    }
    
    /*
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
     */
    
}

- (IBAction)onClickedBuyButton:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails=[_TuneList objectAtIndex:sender.tag];
    [self presentViewController:ViewController animated:YES completion:nil];
}


-(void)onSelectedSeeAllButton:(UIButton*)SeeAllButton
{
    
    NSLog(@"onSelectedSeeAllButton >>> SeeAllButton.tag = %ld",(long)SeeAllButton.tag);
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDGenericPlayList *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGenericPlayList"];
    ViewController.IsSegmentTabRequired = false;
    ViewController.Selected_TuneList = [_TuneList mutableCopy];
    ViewController.SubCategoryName = _SubCategoryTitle;
   
    NSString *screenName = [NSString stringWithFormat:@"Subcategories_%@_%@",[SIXDGlobalViewController HTMLConversion:appDelegate.CategoryName],[SIXDGlobalViewController HTMLConversion:ViewController.SubCategoryName]];
    
    [SIXDStylist setGAScreenTracker:screenName];
    [_SubCategoriesVC.navigationController pushViewController:ViewController animated:YES];
    
}
- (IBAction)onSelectedMoreButton:(UIButton *)sender
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_TuneList;
    ViewController.CurrentToneIndex=sender.tag;
    [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
}
- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    //TO DO
    NSLog(@"SIXDListTonesVC::onSelectedLikeButton >>>>");
    
}

/*
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectItemAtIndexPath SIXDListTonesVC >>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    
    [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
}
 */

@end
