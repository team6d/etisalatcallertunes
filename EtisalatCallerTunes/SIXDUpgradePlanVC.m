//
//  SIXDUpgradePlanVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDUpgradePlanVC.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDCustomTVCell.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "SIXDCommonPopUp_View.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"

@interface SIXDUpgradePlanVC ()

@end

@implementation SIXDUpgradePlanVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Upgrade_Plan"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"GET ON THE VIP LIST",nil,appDelegate.LocalBundel,nil);
    [_UpgradeButton setTitle:NSLocalizedStringFromTableInBundle(@"UPGRADE NOW",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _DescriptionList = @[@"Unlimited download",@"Keep up to 25 tunes in your library",@"Exclusive special promotions"];
    _ImageList = @[@"up1",@"up2",@"up3"];
    //_PriceLabel.text = [appDelegate.AppSettings objectForKey:@"UPGRADEPLANPRICE"];
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    // Do any additional setup after loading the view.
    
    [self API_GetTonePriceAndValidateNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle([_DescriptionList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil);
    
    [cell.AccessoryButton setImage:[UIImage imageNamed:[_ImageList objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
   
    return cell;
}


/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :@"UPGRADE PLAN" : CLR_WHITE];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSelectedUpgradeButton:(id)sender
{
    
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=UPGRADE_VIP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
     NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&clientTxnId=%@&language=%@&packName=CRBT_VIP&priority=0&channelId=%@&serviceId=9",appDelegate.MobileNumber,Rno,appDelegate.Language,CHANNEL_ID];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/pack-upgrade",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
    
}

-(void)API_GetTonePriceAndValidateNumber
{
    NSLog(@"VIP API_GetTonePriceAndValidateNumber >>");
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=GET_TONE_PRICE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=&validationIdentifier=3&channelId=%@&PackName=CRBT_VIP",Rno,appDelegate.Language, appDelegate.MobileNumber,CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"onSuccessResponseRecived >>> = %@",Response);
 
    if(appDelegate.APIFlag==UPGRADE_VIP)
    {
        [SIXDStylist setGAEventTracker:@"VIP" :@"Upgrade Plan" :nil];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_View *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_View"];
        ViewController.AlertMessage = [NSString stringWithFormat:@"%@", [Response objectForKey:@"message"]];
        ViewController.title = NSLocalizedStringFromTableInBundle(@"UPGRADE PLAN",nil,appDelegate.LocalBundel,nil);
        ViewController.AlertImageName = @"salatiloadng";
        ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
        ViewController.IsErrorFlag = FALSE;
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        NSLog(@"getTonePrice Success");
        
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        
        NSMutableArray *responseDetailsList = [responseMap objectForKey:@"responseDetails"];
        NSMutableDictionary *responseDetails = [responseDetailsList objectAtIndex:0];
        
        int temp = [[responseDetails objectForKey:@"amount"] intValue];
        NSString *Price = [appDelegate.AppSettings objectForKey:@"VIP_PRICE"];
        _PriceLabel.text = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"onFailureResponseRecived >>> = %@",Response);
    if(appDelegate.APIFlag==UPGRADE_VIP)
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_View *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_View"];
        
        ViewController.AlertMessage = Message;
        ViewController.title = NSLocalizedStringFromTableInBundle(@"UPGRADE PLAN",nil,appDelegate.LocalBundel,nil);
        ViewController.AlertImageName = @"Fill 3";
        ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"GO BACK",nil,appDelegate.LocalBundel,nil);
        ViewController.IsErrorFlag = TRUE;
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        NSLog(@"getTonePrice onFailureResponseRecived >>");
        return;
    }
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    //[_ActivityIndicator stopAnimating];
    //[self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
@end
