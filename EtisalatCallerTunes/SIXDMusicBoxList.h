//
//  SIXDMusicBoxList.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDMusicBoxList : ViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet UIView *InfoView;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabelTop;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabelBottom;
@property (weak, nonatomic) IBOutlet UIView *ContainerView;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property(strong,nonatomic) NSMutableArray *categoryList;
@property(nonatomic) int CopyCount;
@end
