//
//  SIXDCopyTuneVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCopyTuneVC.h"
#import "SIXDGlobalViewController.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"
#import "SIXDCommonPopUp_Text.h"

@interface SIXDCopyTuneVC ()

@end

@implementation SIXDCopyTuneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    //localisation
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"COPY TUNES",nil,appDelegate.LocalBundel,nil);
    [_ShowListButton setTitle:NSLocalizedStringFromTableInBundle(@"SHOW LIST OF TUNES",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _CopyNumberLabel.text = NSLocalizedStringFromTableInBundle(@"Copy from this Number*",nil,appDelegate.LocalBundel,nil);
    [_CopyNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Copy from this Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
   
    _CopyNumberTextField.keyboardType = ASCII_NUM_KEYBOARD;
    
    NSString *title = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"COPYTUNE_1",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"COPYTUNE_2",nil,appDelegate.LocalBundel,nil)];
    
    _InfoLabel.attributedText = [SIXDGlobalViewController getAttributedText:title];
    
    _CopyNumberTextField.enablesReturnKeyAutomatically = NO;
    _CopyNumberTextField.delegate = self;
    _CopyNumberView.layer.borderWidth = 1.0f;
    _CopyNumberLabel.hidden=YES;
    NSLog(@"ViewController.BPartyMsisdn  before loading= %@",_BPartyMsisdn);

    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no tunes to copy",nil,appDelegate.LocalBundel,nil) :0];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
     _typeFlag=0;
    
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"TUNES",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"PLAYLIST",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];

    
    if(_BPartyMsisdn.length > 0)
    {
        [SIXDStylist setGAScreenTracker:@"CopyTunes_List_BPartyTunes"];
        
        NSLog(@"_BPartyMsisdn = %@",_BPartyMsisdn);
        NSArray *array = [_SegmentTab subviews];
        UIView *Temp;
        Temp = [array objectAtIndex:0];
        UILabel *Line;
        if([appDelegate.Language isEqualToString:@"English"])
        {
            Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+80, 11, 1, 7)];
            /*
            if(appDelegate.DeviceSize == iPHONE_X || appDelegate.DeviceSize == SMALL)
            {
                Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+80, 11, 1, 7)];
            }
            else
            {
                Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 12, 1, 6)];
            }*/
        }
        
        else if([appDelegate.Language isEqualToString:@"Arabic"])
        {
            Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+88, 11, 1, 7)];
            /*
            if(appDelegate.DeviceSize == iPHONE_X || appDelegate.DeviceSize == SMALL)
            {
                Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+85, 11, 1, 7)];
            }
            else
            {
                Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 12, 1, 6)];
            }*/
        }
        
        
        Line.backgroundColor = [UIColor grayColor];
        [_SegmentTab insertSubview:Line aboveSubview:Temp];
        
        _ShowListButton.hidden=YES;
        _SegmentTab.hidden=NO;
        [self.view bringSubviewToFront:_SegmentTab];
        _CopyNumberButton.hidden=YES;
        _CopyNumberView.userInteractionEnabled=FALSE;
        _CopyNumberView.layer.borderWidth = 1.0f;
        _CopyNumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
        _CopyNumberView.backgroundColor = UIColorFromRGB(CLR_DISABLE_TEXTFIELD);
        _CopyNumberTextField.text = _BPartyMsisdn;
        _CopyNumberView.alpha=0.95;
        _TopSpaceToTextField.constant=19;
        _CopyNumberLabel.hidden=NO;
        _TonesContainer.hidden=false;
        [self fetchTunesAndPlaylist];
        
    }
    else
    {
         [SIXDStylist setGAScreenTracker:@"CopyTunes_Enter_BPartyNumber"];
        
        _CopyNumberView.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
        [_CopyNumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
        [SIXDStylist setTextViewNormal:_CopyNumberTextField :_CopyNumberView :nil];
        _TopSpaceToTextField.constant=0;
        _AlertLabel.hidden=YES;
        _PlaylistContainer.hidden=YES;
        _TonesContainer.hidden=YES;
        _SegmentTab.hidden=YES;
        [self.SegmentView bringSubviewToFront:_ShowListButton];
        _CopyNumberButton.hidden=NO;
        _CopyNumberView.userInteractionEnabled=TRUE;
        UIImage* image = [[UIImage imageNamed:@"giftatuneaddcontact"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_CopyNumberButton setImage:image forState:UIControlStateNormal];
        _CopyNumberButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        
        
        [SIXDStylist addKeyBoardDoneButton:_CopyNumberTextField :self];
        
    }

}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    [_CopyNumberTextField resignFirstResponder];
    if(_CopyNumberTextField.text.length==0)
    {
        _AlertLabel.hidden=YES;
        [SIXDStylist setTextViewNormal:_CopyNumberTextField :_CopyNumberView :nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender {
    
    switch ([sender selectedSegmentIndex]) {
        case 0:
            NSLog(@"switch0");
            _typeFlag=0;
            
            if(_TonesVC.Tunes_listToneApk.count <= 0)
            {
                [self changeAlertText];
                [_AlertView setHidden:false];
            
            }
            else
            {
                [_AlertView setHidden:true];
            }
            _TonesContainer.hidden=NO;
            _PlaylistContainer.hidden=YES;

            break;
        case 1:
            NSLog(@"switch1");
            _typeFlag=1;
            if(_PlaylistVC.Playlist_listToneApk.count <= 0)
            {
                [self changeAlertText];
                [_AlertView setHidden:false];
            }
            else
            {
                [_AlertView setHidden:true];
                
            }
            _TonesContainer.hidden=YES;
            _PlaylistContainer.hidden=NO;
            
            break;
            
        default:
            NSLog(@"switch default statement for segment control");
            
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"Tunes"])
    {
        _TonesVC = (SIXDCopyActiveTonesVC *) [segue destinationViewController];
        _TonesVC.ImageHeight=_ImageHeight;
    }
    else if ([segueName isEqualToString: @"Playlist"])
    {
        _PlaylistVC = (SIXDCopyPlaylistVC *) [segue destinationViewController];
        _PlaylistVC.ImageHeight=_ImageHeight;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
}


-(void)API_ListTones
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString * str = [_CopyNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_CopyNumberTextField.text length])];
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/list-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&startIndex=0&endIndex=20&rbtMode=%d",URL,appDelegate.Language,str,_rbtMode];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"API_ListTones onSuccessResponseRecived = %@",Response);
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    _TonesVC.MobileNumber = _CopyNumberTextField.text;
    if(appDelegate.APIFlag==LIST_TONES_TONES)
    {
        _TonesVC.Tunes_listToneApk=[responseMap objectForKey:@"listToneApk"];
        _TonesVC.Tunes_listToneApk = [SIXDGlobalViewController convertListToneResponse:_TonesVC.Tunes_listToneApk];
        
        //added on 4/05/2018 - removing content if isCopy is false
       NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
       for(int i = 0;i<_TonesVC.Tunes_listToneApk.count;i++)
        {
            NSMutableDictionary *Temp = [_TonesVC.Tunes_listToneApk objectAtIndex:i];
            if(![[Temp objectForKey:@"isCopy"] isEqualToString:@"T"])
            {
                [indexSet addIndex:i];
            }
            
        }
        [_TonesVC.Tunes_listToneApk removeObjectsAtIndexes:indexSet];
        //end
        
        appDelegate.APIFlag=LIST_TONES_PLAYLIST;
        _rbtMode=300;
        [self API_ListTones];
        
        _TonesContainer.hidden=NO;
        [_TonesVC.TableView reloadData];
        [_SegmentTab setSelectedSegmentIndex:0];
        [self onSegmentValueChanged:_SegmentTab];
        
    }
    else if(appDelegate.APIFlag==LIST_TONES_PLAYLIST)
    {
        _PlaylistVC.Playlist_listToneApk=[responseMap objectForKey:@"listToneApk"];
        _PlaylistVC.Playlist_listToneApk = [SIXDGlobalViewController convertListToneResponse:_PlaylistVC.Playlist_listToneApk];
        
        //added on 4/05/2018 - removing content if isCopy is false
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        for(int i = 0;i<_PlaylistVC.Playlist_listToneApk.count;i++)
        {
            NSMutableDictionary *Temp = [_PlaylistVC.Playlist_listToneApk objectAtIndex:i];
            if(![[Temp objectForKey:@"isCopy"] isEqualToString:@"T"])
            {
                [indexSet addIndex:i];
            }
            
        }
        [_PlaylistVC.Playlist_listToneApk removeObjectsAtIndexes:indexSet];
        //end
        
        NSLog(@"_PlaylistVC.Playlist_listToneApk = %@",_PlaylistVC.Playlist_listToneApk);
        [_PlaylistVC.TableView reloadData];
    }
    else
    {
        [SIXDGlobalViewController setAlert:@"Copy Tune Successful" :self];
    }
    
    [_SegmentTab setSelectedSegmentIndex:0];
    
    /*_ShowListButton.hidden=YES;
    _SegmentTab.hidden=NO;
    [self.view bringSubviewToFront:_SegmentTab];
    _CopyNumberButton.hidden=YES;
    _CopyNumberView.userInteractionEnabled=FALSE;
    _CopyNumberView.layer.borderWidth = 1.0f;
    _CopyNumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
    _CopyNumberView.backgroundColor = UIColorFromRGB(CLR_DISABLE_TEXTFIELD);
    _CopyNumberView.alpha=0.95;
     */
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    if(_SegmentTab.selectedSegmentIndex == 0 && appDelegate.APIFlag!=LIST_TONES_PLAYLIST)
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.AlertMessage = Message;
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
        ViewController.ParentView = self;
        [self presentViewController:ViewController animated:YES completion:nil];
    }
   
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onClickedShowListButton:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString * str = [_CopyNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_CopyNumberTextField.text length])];
    
     NSString *Aparty = [appDelegate.MobileNumber substringFromIndex:1];
    
    if(str.length ==0)
    {
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_CopyNumberTextField :_CopyNumberView :nil];
    }
    
    if(str.length<9)
    {
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_CopyNumberTextField :_CopyNumberView :nil];
    }
    else if(![SIXDStylist isEtisalatValidNumber:_CopyNumberTextField.text])
    {
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_CopyNumberTextField :_CopyNumberView :nil];
    }
    else if([str rangeOfString:Aparty].location != NSNotFound )
    {
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"You cannot copy from your own number",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_CopyNumberTextField :_CopyNumberView :nil];
    }
    else
    {
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCopyTuneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCopyTuneVC"];
        ViewController.BPartyMsisdn = str;
        NSLog(@"ViewController.BPartyMsisdn = %@",ViewController.BPartyMsisdn);
        
        [self.navigationController pushViewController:ViewController animated:YES];
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _TopSpaceToTextField.constant=19;
    _CopyNumberLabel.hidden=NO;
    
    if(_CopyNumberTextField.text.length ==0)
    {
        _CopyNumberTextField.text = @"+971-";
    }
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 150;
    }
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_CopyNumberTextField :_CopyNumberView :nil];
    [_CopyNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    if([_CopyNumberTextField.text isEqualToString:@"+971-"])
    {
        _CopyNumberTextField.text = @"";
    }
    
    if(_CopyNumberTextField.text.length==0)
    {
        [_CopyNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Copy from this Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _CopyNumberLabel.hidden=YES;
        _TopSpaceToTextField.constant=0;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"STRING = %@ range=%lu", string,(unsigned long)range.location);
    if(textField.text.length == 5 && string.length == 0 && [textField.text isEqualToString:@"+971-"])
    {
        return NO;
    }
    else if(textField.text.length == 15 && string.length == 1)
    {
        return NO;
    }
    else
        return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)onClickedAddContactButton:(id)sender
{
    NSLog(@"onSelectedContactButton");
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    
   // _CopyNumberTextField.text = @"";
    
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
}


- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    _PhoneNumbers = contact.phoneNumbers;
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
}

-(void)contactPickerDidCancel:(CNContactPickerViewController *)picker
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_CopyNumberTextField.text.length==0)
    {
        [_CopyNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Copy from this Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _CopyNumberLabel.hidden = YES;
        _TopSpaceToTextField.constant=0;
        
    }
}

-(void)ShowMultipleNumbers
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     if(_CopyNumberTextField.text.length==0)
                                     {
                                         [_CopyNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Copy from this Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
                                         
                                         _CopyNumberLabel.hidden = YES;
                                         _TopSpaceToTextField.constant=0;
                                         
                                     }
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _CopyNumberTextField.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _CopyNumberTextField.text = [label.value stringValue];
    }
    
    if(_CopyNumberTextField.text.length > 0)
    {
        _CopyNumberLabel.hidden=NO;
        _TopSpaceToTextField.constant=19;
    }
}

-(void)changeAlertText
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIView *AlertView = [_AlertView.subviews objectAtIndex:0];
    for (UIView *view in AlertView.subviews)
    {
        if([view isKindOfClass:[UILabel class]])
        {
            UILabel *Label = (UILabel *)view;
            if(Label.tag == 1)
            {
                if(_SegmentTab.selectedSegmentIndex == 0)
                {
                    Label.text = NSLocalizedStringFromTableInBundle(@"no tunes to copy",nil,appDelegate.LocalBundel,nil);
                }
                else
                {
                    Label.text = NSLocalizedStringFromTableInBundle(@"no playlists to copy",nil,appDelegate.LocalBundel,nil);
                }
            }
        }
    }
}

-(void)fetchTunesAndPlaylist
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.hidden=YES;
   // [SIXDStylist setTextViewNormal:_CopyNumberTextField :_CopyNumberView :nil];
    appDelegate.APIFlag=LIST_TONES_TONES;
    _rbtMode=400;
    [self API_ListTones];
}

-(void)onSelectedPopUpConfirmButton
{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
