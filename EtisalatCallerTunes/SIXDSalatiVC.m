//
//  SIXDSalatiVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDSalatiVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SIXDGiftToneVC.h"
#import "SIXDWishlistPopUpVC.h"
#import "SIXDTellAFriendVC.h"
#import "SIXDSocialSharePopUpVC.h"
#import "SIXDStylist.h"

#import "EtisalatCallerTunes-Swift.h"

//READ ME
//"attribute":"toneId|toneName|artistName|toneUrl|previewImageUrl|price"

@interface SIXDSalatiVC ()

@end

@implementation SIXDSalatiVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Salati"];
    
    _IsInitialLoad = true;
    [_ScrollView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
    CGSize devicesize = [[UIScreen mainScreen] bounds].size ;
    if(devicesize.width <= 320)
    {
        _GiftViewWidth.constant = 65;
        _WishListViewWidth.constant = 65;
        _TAFViewWidth.constant = 65;
        _ShareViewWidth.constant = 65;
        
    }

    [self setViewBorder:_GiftView];
    [self setViewBorder:_WishlistView];
    [self setViewBorder:_TAFView];
    [self setViewBorder:_ShareView];
    
    _SIXDAVPlayer_Audio = [[SIXDAVPlayer alloc]init];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
   
    
    [_PlayButton setImage:[UIImage imageNamed:@"salatiplay"] forState:UIControlStateNormal];
    [_PlayButton setImage:[UIImage imageNamed:@"salatipause"] forState:UIControlStateSelected];
    
    //_PriceLabel.text = [NSString stringWithFormat:@"%@",[_ToneDetails objectForKey:@"price"]];
    
    [_SubscribeButton setTitle:NSLocalizedStringFromTableInBundle(@"SUBSCRIBE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
   
    _ScrollView.delegate=self;
    
    
    UITapGestureRecognizer *GiftTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleGiftTap:)];
    [self.GiftView addGestureRecognizer:GiftTap];
    
    UITapGestureRecognizer *WishlistTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleWishlistTap:)];
    [self.WishlistView addGestureRecognizer:WishlistTap];
    
    UITapGestureRecognizer *TAFTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTAFTap:)];
    [self.TAFView addGestureRecognizer:TAFTap];
    
    UITapGestureRecognizer *ShareTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleShareTap:)];
    [self.ShareView addGestureRecognizer:ShareTap];
    
    _GiftLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT_SMALL",nil,appDelegate.LocalBundel,nil);
    _WishlistLabel.text = NSLocalizedStringFromTableInBundle(@"WISHLIST_SMALL",nil,appDelegate.LocalBundel,nil);
    _TAFLabel.text = NSLocalizedStringFromTableInBundle(@"Tell a Friend",nil,appDelegate.LocalBundel,nil);
    _ShareLabel.text = NSLocalizedStringFromTableInBundle(@"SHARE_SMALL",nil,appDelegate.LocalBundel,nil);
    
    _IsCheckSalatiStatusInvoked = false;
    
}

- (void)handleGiftTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"GiftSalati_GetPrice"];
    
    
    NSLog(@"handleGiftTap");
    if(_PlayButton.selected)
    {
    [_SIXDAVPlayer_Audio stopPlay];
    _PlayButton.selected = !_PlayButton.selected;
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDGiftToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGiftToneVC"];
    ViewController.ToneDetails=_ToneDetails;
    ViewController.IsSourceSalati = true;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleWishlistTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"Salati_AddToWishlist"];
    [SIXDStylist setGAEventTracker:@"Salati" :@"Add to wishlist" :nil];
    
    NSLog(@"handleWishlistTap");
    if(_PlayButton.selected)
    {
    [_SIXDAVPlayer_Audio stopPlay];
    _PlayButton.selected = !_PlayButton.selected;
    }
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDWishlistPopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDWishlistPopUpVC"];
    ViewController.ToneDetails=_ToneDetails;
    ViewController.WishListType = 1;
    ViewController.IsSourceSalati=true;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleTAFTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"Salati_TellAFriend"];
    [SIXDStylist setGAEventTracker:@"Salati" :@"Tell a friend" :nil];
    NSLog(@"handleTAFTap");
    if(_PlayButton.selected)
    {
    [_SIXDAVPlayer_Audio stopPlay];
    _PlayButton.selected = !_PlayButton.selected;
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDTellAFriendVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTellAFriendVC"];
    ViewController.ToneDetails=_ToneDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleShareTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"Salati_Share"];
    [SIXDStylist setGAEventTracker:@"Salati" :@"Share" :nil];
    
    NSLog(@"handleShareTap");
    if(_PlayButton.selected)
    {
    [_SIXDAVPlayer_Audio stopPlay];
    _PlayButton.selected = !_PlayButton.selected;
    }
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDSocialSharePopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSocialSharePopUpVC"];
    ViewController.ToneDetails=_ToneDetails;
    ViewController.Source = SALATI;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedSubscribeButton:(UIButton *)sender
{
    [SIXDStylist setGAEventTracker:@"Salati" :@"Subscribe salati" :nil];
   
    NSLog(@"onClickedSubscribeButton >>");
    if(_PlayButton.selected)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _PlayButton.selected = !_PlayButton.selected;
    }
    
    
    
    [self API_SalatiActivation];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}


-(void)FillToneDetailsDict
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"SALATI"];
    if(_ToneDetails.count <= 0)
    {
        if(DummyArray.count > 0)
        {
            _ToneDetails = [NSMutableDictionary new];
            [_ToneDetails setObject:[DummyArray objectAtIndex:0] forKey:@"toneId"];
            [_ToneDetails setObject:[DummyArray objectAtIndex:1] forKey:@"toneName"];
            [_ToneDetails setObject:[DummyArray objectAtIndex:2] forKey:@"artistName"];
            [_ToneDetails setObject:[DummyArray objectAtIndex:3] forKey:@"toneUrl"];
            [_ToneDetails setObject:[DummyArray objectAtIndex:4] forKey:@"previewImageUrl"];
            [_ToneDetails setObject:[DummyArray objectAtIndex:5] forKey:@"price"];
            NSLog(@"salati _ToneDetails = %@",_ToneDetails);
        }
    }
    _MainHeadingLabel.text = NSLocalizedStringFromTableInBundle(@"SALATI",nil,appDelegate.LocalBundel,nil);
    [self.view bringSubviewToFront:_MainHeadingLabel];
    _TopLabel.text = [appDelegate.AppSettings objectForKey:@"SALATI_HEADER"];
    
    _InfoLabel.text = [appDelegate.AppSettings objectForKey:@"SALATI_INFO"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem_MainNavigationPage:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    [self FillToneDetailsDict];
    
}

-(void)API_SalatiActivation
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    _APIInvoked = SALATI_SUBSCRIBE;
    NSLog(@"API_SalatiActivation >>");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&language=%@&clientTxnId=%@&serviceId=15&channelId=%@&activityId=1&paymentMode=2&priority=0",appDelegate.MobileNumber,appDelegate.Language,Rno,CHANNEL_ID];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/salati-activation",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSLog(@"onSuccessResponseRecived = %@",Response);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_APIInvoked==GET_TONE_PRICE_SALATI)
    {
        
        NSLog(@"salati GTP response = %@",Response);
         NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
         NSLog(@"GET_TONE_PRICE");
        
         NSMutableArray *responseDetailsList = [responseMap objectForKey:@"responseDetails"];
         NSMutableDictionary *responseDetails = [responseDetailsList objectAtIndex:0];
        
        //commented on 2nd august 2018 - shahal - in accordance with change in PRICE_POINT_VALUE
        /*
         int statusCodes = [[responseDetails objectForKey:@"statusCodes"]intValue];
        
         NSString *Price;
        
         int temp = [[responseDetails objectForKey:@"amount"] intValue];
        
         NSMutableDictionary *Dict = [appDelegate.AppSettings objectForKey:@"VALIDITY"];
         Price = [Dict objectForKey:[responseDetails objectForKey:@"packName"]];
         Price = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
        
         switch (statusCodes)
         {
             case 0:
             {
                 _PriceLabel.text = Price;
             }
             break;
             
             default:
             {
                 NSString *Price;
                 _PriceLabel.text = Price;
                 Price = [NSString stringWithFormat:@"%@\n%@",Price,[responseDetails objectForKey:@"statusDesc"]];
             
                 _PriceLabel.text = Price;
             }
                 break;
         }
         */
        
        int temp = [[responseDetails objectForKey:@"amount"] intValue];
        if(temp==0)
        {
            _PriceLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTableInBundle(@"AED",nil,appDelegate.LocalBundel,nil),temp];
        }
        else
        {
            NSString *Price = [appDelegate.AppSettings objectForKey:@"SALATI_PRICE"];
            _PriceLabel.text = [Price stringByReplacingOccurrencesOfString:@"$price$" withString:[NSString stringWithFormat:@"%d",temp]];
        }
    }
    else
    {
        if(_IsCheckSalatiStatusInvoked)
        {
    
            _IsCheckSalatiStatusInvoked = false;
            Response =  [Response objectForKey:@"responseMap"];
            NSMutableArray *Array;
            Array = [[Response objectForKey:@"responseDataList"]mutableCopy];
            if(Array == nil)
            {
              Array =  [[Response objectForKey:@"salatiStatusList"]mutableCopy];
            }
            Response = [Array objectAtIndex:0];
            if([[Response objectForKey:@"salatiStatus"] isEqualToString:@"A"]||[[Response objectForKey:@"salatiStatus"] isEqualToString:@"G"]||[[Response objectForKey:@"salatiStatus"] isEqualToString:@"S"]||[[Response objectForKey:@"salatiStatus"] isEqualToString:@"P"])
            {
                 _IsInitialLoad = false;
                [_SubscribeButton setTitle:NSLocalizedStringFromTableInBundle(@"ALREADY SUBSCRIBED",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                [_SubscribeButton setUserInteractionEnabled:false];
                //_SubscribeButton.alpha = 0.7;
                _PriceLabel.hidden = true;
            }
            else
            {
                if(_IsInitialLoad)
                {
                    [self.tabBarController.view addSubview:_overlayview];
                    [_ActivityIndicator startAnimating];
                    [self.view bringSubviewToFront:_ActivityIndicator];
                }
                [self API_GetTonePriceAndValidateNumber];
                _IsInitialLoad = false;
                [_SubscribeButton setTitle:NSLocalizedStringFromTableInBundle(@"SUBSCRIBE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                [_SubscribeButton setUserInteractionEnabled:true];
                _SubscribeButton.alpha = 1;
                _PriceLabel.hidden = false;
            }
            
        }
        else
        {
           
            NSString *message = [Response objectForKey:@"message"];
            if(message.length <= 0)
            {
                message = @"Your request is in process.You will be notified shortly by sms. ";
            }
            //showing alert
            _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :message :false :_ScrollView.frame :self];
            [_ScrollView setHidden:true];
            [self.view addSubview:_AlertView];
            //end
        }
    }

}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
     NSLog(@"API_SalatiActivation onFailureResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_IsCheckSalatiStatusInvoked)
    {
         //_IsInitialLoad = false;
        _IsCheckSalatiStatusInvoked = false;
        [_SubscribeButton setTitle:NSLocalizedStringFromTableInBundle(@"SUBSCRIBE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        [_SubscribeButton setUserInteractionEnabled:true];
        _SubscribeButton.alpha = 1;
        _PriceLabel.hidden = false;
        
        if(_IsInitialLoad)
        {
            [self.tabBarController.view addSubview:_overlayview];
            [_ActivityIndicator startAnimating];
            [self.view bringSubviewToFront:_ActivityIndicator];
        }
        [self API_GetTonePriceAndValidateNumber];
        
        return;
    }
    
    if(_APIInvoked==GET_TONE_PRICE_SALATI)
    {
        _IsInitialLoad = false;
        return;
    }
    
    NSString *message = [Response objectForKey:@"message"];
   
    //showing alert
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :message  :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];

}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    if(_IsCheckSalatiStatusInvoked)
    {
        _IsCheckSalatiStatusInvoked = false;
        if(_IsInitialLoad)
        {
            [self.tabBarController.view addSubview:_overlayview];
            [_ActivityIndicator startAnimating];
            [self.view bringSubviewToFront:_ActivityIndicator];
        }
        _IsInitialLoad = false;
        [self API_GetTonePriceAndValidateNumber];
        return;
    }

    NSLog(@"salati onFailedToConnectToServer");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //showing alert
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil)  :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];
}

- (void)onInternetBroken
{
    if(_IsCheckSalatiStatusInvoked)
    {
        _IsCheckSalatiStatusInvoked = false;
        return;
    }
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)setViewBorder:(UIView *)myView
{
    myView.layer.borderWidth = 0.5f;
    myView.layer.borderColor = [UIColorFromRGB(CLR_BoxBorder) CGColor];
}

- (void)viewDidAppear:(BOOL)animated
{
    [_AlertView removeFromSuperview];
    [_ScrollView setHidden:false];
    _AlertView = nil;
    if(_IsInitialLoad)
    {
        [self.tabBarController.view addSubview:_overlayview];
        [_ActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_ActivityIndicator];
    }
    if(!_IsCheckSalatiStatusInvoked)
    {
        [self checkSalatiSubscriptionStatus];
    }
}

-(void)onSelectedPopUpConfirmButton
{
    [_ScrollView setHidden:false];
    [_AlertView removeFromSuperview];
    _AlertView = nil;
    [self checkSalatiSubscriptionStatus];
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear >>>");
    if(_PlayButton.selected)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _PlayButton.selected = !_PlayButton.selected;
    }
}

-(void)onPlayStarted
{
    NSLog(@"onPlayStarted>>>");
    dispatch_async(dispatch_get_main_queue(), ^{
        [_ActivityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:true];
    });
    
}


- (IBAction)onClickedPlayButton:(UIButton *)sender
{
    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//      [appDelegate testingGiftPopupDelete];
//      return;
      
    NameTuneListController* nameTuneListController = (NameTuneListController*)[[UIStoryboard storyboardWithName:@"NameTune" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"NameTuneListController"];
    [self.navigationController pushViewController:nameTuneListController animated:YES];


    return;
    
    [SIXDStylist setGAEventTracker:@"Salati" :@"Play salati" :nil];
    
    NSLog(@"onSelectedPlayPauseButton>>>");
    [_SIXDAVPlayer_Audio stopPlay];
    
    sender.selected = !sender.selected;
    if(sender.selected)
    {
        NSLog(@"playing>>>>");
        [_ActivityIndicator startAnimating];
        [self.view setUserInteractionEnabled:false];
        _SIXDAVPlayer_Audio.SIXVCCallBack = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [_SIXDAVPlayer_Audio initialisePlayer :[_ToneDetails objectForKey:@"toneUrl"]];
           // [_SIXDAVPlayer_Audio playIO];
        });
        
    }
    else
    {
        [_SIXDAVPlayer_Audio stopPlay];
        NSLog(@"play stop>>>>");
        
    }
    
}

- (void)onPlayStopped
{
    NSLog(@"onPlayStopped>>>");
    _PlayButton.selected = !_PlayButton.selected;
}

-(void)onPlayFailure
{
    NSLog(@"onPlayFailure");
    
    dispatch_async(dispatch_get_main_queue(), ^{
      [_ActivityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:true];
    });
    
}

-(void)checkSalatiSubscriptionStatus
{
    _APIInvoked=SALATI_CHECK;
    _IsCheckSalatiStatusInvoked = true;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/salati-status",appDelegate.BaseMiddlewareURL];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?clientTxnId=%@&channelId=%@&serviceId=15&activityId=5&aPartyMsisdn=%@",URL,Rno,CHANNEL_ID,appDelegate.MobileNumber];
    [ReqHandler sendHttpGETRequest:self :nil];
    
}

-(void)API_GetTonePriceAndValidateNumber
{
    _APIInvoked=GET_TONE_PRICE_SALATI;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=%@&validationIdentifier=3&channelId=%@",Rno,appDelegate.Language, appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

@end
