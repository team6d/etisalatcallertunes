//
//  SIXDGroupsVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDGroupsVC.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGroupViewVC.h"
#import "SIXDGroupConfiguration.h"
#import "SIXDStylist.h"

@interface SIXDGroupsVC ()

@end

@implementation SIXDGroupsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [SIXDStylist setGAScreenTracker:@"View_My_Caller_Groups"];
    
    _TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"MY CALLER GROUP",nil,appDelegate.LocalBundel,nil);
    [_CreateButton setTitle:NSLocalizedStringFromTableInBundle(@"CREATE A GROUP",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    _GroupNameLabel.text = NSLocalizedStringFromTableInBundle(@"GROUP NAME",nil,appDelegate.LocalBundel,nil);

     _InfoLabel.attributedText = [SIXDGlobalViewController getAttributedText:NSLocalizedStringFromTableInBundle(@"GROUP_INFO",nil,appDelegate.LocalBundel,nil)];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no groups at the moment",nil,appDelegate.LocalBundel,nil) :20];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"_Groups_listToneApk.count = %ld",_Groups_listToneApk.count);
    return _Groups_listToneApk.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableDictionary *Dict = [_Groups_listToneApk objectAtIndex:indexPath.row];
    cell.TitleLabel.text = [Dict objectForKey:@"groupName"];
    cell.TitleLabel.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [cell.AccessoryButton setImage:[UIImage imageNamed:@"menuarrow"] forState:UIControlStateNormal];
    }
    else
    {
         [cell.AccessoryButton setImage:[UIImage imageNamed:@"menuarrow_Arabic"] forState:UIControlStateNormal];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDGroupViewVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGroupViewVC"];
    ViewController.GroupsList = [_Groups_listToneApk objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)API_ListTones
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/list-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&startIndex=0&endIndex=20&rbtMode=2",URL,appDelegate.Language,appDelegate.MobileNumber];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    _Groups_listToneApk=[responseMap objectForKey:@"listToneApk"];
    NSLog(@"_Groups_listToneApk = %@",_Groups_listToneApk);
    
    if(_Groups_listToneApk.count <= 0)
    {
        [_AlertView setHidden:false];
    }
    else
    {
        [_AlertView setHidden:true];
    }
    [_TableView reloadData];
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_ListTones onFailureResponseRecived = %@",Response);
    [_AlertView setHidden:false];
   // NSString *message=[Response objectForKey:@"message"];
   // [SIXDGlobalViewController setAlert:message :self];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_ListTones onFailedToConnectToServer");
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onClickedCreateGroupButton:(id)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDGroupConfiguration *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGroupConfiguration"];
    ViewController.SOURCE=FROM_CREATE_GROUP;
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"API_ListTones GROUP call>>");
    [self API_ListTones];
}

@end
