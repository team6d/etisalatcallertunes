//
//  SIXDBlacklistVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDBlacklistVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDStylist.h"

@interface SIXDBlacklistVC ()

@end

@implementation SIXDBlacklistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Blacklist"];
    
    _IsInitialLoad = true;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"BLACKLIST",nil,appDelegate.LocalBundel,nil);
    [self.view bringSubviewToFront:_HeadingLabel];
    
     _Name.enablesReturnKeyAutomatically = NO;

    
     _InfoLabel.attributedText = [SIXDGlobalViewController getAttributedText:NSLocalizedStringFromTableInBundle(@"Create and manage your blacklist to disable specific numbers to hear your tunes when they call you.",nil,appDelegate.LocalBundel,nil)];
    
    
    [_AddButton setTitle:NSLocalizedStringFromTableInBundle(@"ADD",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    [_BlacklistNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Blacklist Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
    [_Name setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Name",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
    
    _ButtonTag = -1;
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _BlacklistNumberTextField.delegate = self;
    
     _BlacklistNumberTextField.keyboardType = ASCII_NUM_KEYBOARD;
    
    _Name.delegate = self;
    
    _Blacklist = [NSMutableArray new];
    
    [_BlacklistNumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_Name setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    _BlackListNumberView.layer.borderWidth = 1.0f;
    _BlackListNumberView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    _NameView.layer.borderWidth = 1.0f;
    _NameView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    
   // [_NameErrorIcon setImage:[UIImage imageNamed:@"Fill 3"] forState:UIControlStateNormal];
   // [_BlackListNumberErrorIcon setImage:[UIImage imageNamed:@"Fill 3"] forState:UIControlStateNormal];
    _NameErrorIcon.hidden =true;
    _BlackListNumberErrorIcon.hidden =true;
    
    
    
    [SIXDStylist addKeyBoardDoneButton:_BlacklistNumberTextField :self];

    
    
    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no blacklist at the moment",nil,appDelegate.LocalBundel,nil) :20];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
    
    _AlertLabel.hidden = true;
    
    NSLog(@"API_ViewBlackList call >>");
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_IsInitialLoad)
    {
      [self API_ViewBlackList];
    _IsInitialLoad = false;
        
    }
    
}

-(void)dismissKeyboard
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    [_BlacklistNumberTextField resignFirstResponder];
    [_NameView resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _Blacklist.count;
    //for testing
    //return 1;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    [textField resignFirstResponder];
    return NO;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 150;
    }
    [self Initialise];
}


-(void)Initialise
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.hidden = true;
    _BlackListNumberView.layer.borderWidth = 1.0f;
    _BlackListNumberView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _NameView.layer.borderWidth = 1.0f;
    _NameView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _NameErrorIcon.hidden =true;
    _BlackListNumberErrorIcon.hidden =true;
    [_BlacklistNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Blacklist Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
    [_Name setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Name",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    cell.TitleLabel.layer.borderWidth = 1.0f;
    cell.TitleLabel.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
    cell.SubTitleLabel.layer.borderWidth = 1.0f;
    cell.SubTitleLabel.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
    
    cell.TitleLabel.textColor = UIColorFromRGB(CLR_PLACEHOLDER);
    cell.SubTitleLabel.textColor = UIColorFromRGB(CLR_PLACEHOLDER);

    cell.DetailLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
    [cell.FirstButton setUserInteractionEnabled:false];
    
    [cell.SecondButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    [cell.SecondButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    cell.SecondButton.tag=indexPath.row;
    
    
   
    
    if(indexPath.row < _Blacklist.count)
    {
        NSMutableDictionary *DetailsTemp = [_Blacklist objectAtIndex:indexPath.row];
        //cell.TitleLabel.text = [NSString stringWithFormat:@"   %@",[DetailsTemp objectForKey:@"bPartyMsisdn"]];
        //cell.SubTitleLabel.text =[NSString stringWithFormat:@"   %@",[DetailsTemp objectForKey:@"bPartyName"]];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSMutableAttributedString *titleString;
        NSMutableAttributedString *WhiteDot;
        NSMutableAttributedString *FinalString = [NSMutableAttributedString new];
        
        titleString = [[NSMutableAttributedString alloc] initWithString:[DetailsTemp objectForKey:@"bPartyMsisdn"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@".   ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor clearColor]}];
        
        [FinalString appendAttributedString:WhiteDot];
        [FinalString appendAttributedString:titleString];
        [FinalString appendAttributedString:WhiteDot];
        cell.TitleLabel.attributedText = FinalString;
        
        
        
        FinalString = [NSMutableAttributedString new];
        
        titleString = [[NSMutableAttributedString alloc] initWithString:[DetailsTemp objectForKey:@"bPartyName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        /*
        if([appDelegate.Language isEqualToString:@"English"])
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:[DetailsTemp objectForKey:@"bPartyName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        }
        else
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:[DetailsTemp objectForKey:@"bPartyName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
        }
         */
        WhiteDot = [[NSMutableAttributedString alloc] initWithString:@".   ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor clearColor]}];
        [FinalString appendAttributedString:WhiteDot];
        [FinalString appendAttributedString:titleString];
        [FinalString appendAttributedString:WhiteDot];
        cell.SubTitleLabel.attributedText = FinalString;
        
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}


-(void)API_AddToBlackList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=ADD_BLACKLIST;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSString * str = [_BlacklistNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_BlacklistNumberTextField.text length])];
    
    //NSString *BlackListName = [_Name.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
        NSMutableArray *MemberListArray = [NSMutableArray new];
    
  
        NSMutableDictionary *ContactDict = [NSMutableDictionary new];
       [ContactDict setObject:str  forKey:@"bPartyMsisdn"];
        [ContactDict setObject:_Name.text  forKey:@"bPartyName"];
    
        [MemberListArray addObject:ContactDict];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:MemberListArray
                                                       options:0
                                                         error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    

    
    ReqHandler.HttpsReqType = POST;
    //ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=0508035304&channelId=%@&memberDetailList=%@",Rno,CHANNEL_ID,MemberDetails];
     ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&aPartyMsisdn=%@&channelId=%@&memberDetailList=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,CHANNEL_ID,MemberDetails];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/add-to-black-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];

}

-(void)API_RemoveFromBlackList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=REMOVE_BLACKLIST;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSMutableDictionary *Temp = [NSMutableDictionary new];
    Temp =[_Blacklist objectAtIndex:_ButtonTag];
    
    NSMutableArray *MemberListArray = [NSMutableArray new];
    NSMutableDictionary *ContactDict = [NSMutableDictionary new];
    [ContactDict setObject:[Temp objectForKey:@"bPartyMsisdn"]  forKey:@"bPartyMsisdn"];
    //[ContactDict setObject:[Temp objectForKey:@"bPartyName"]  forKey:@"bPartyName"];
    
    [MemberListArray addObject:ContactDict];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:MemberListArray options:0 error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    ReqHandler.HttpsReqType = POST;
    //ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=0508035304&channelId=%@&memberDetailList=%@",Rno,CHANNEL_ID,MemberDetails];
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&aPartyMsisdn=%@&channelId=%@&memberDetailList=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,CHANNEL_ID,MemberDetails];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/remove-from-black-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];

}

-(void)API_ViewBlackList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=VIEW_BLACKLIST;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    //ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=0508035304&channelId=%@&serviceId=1&activityId=4&rbtMode=101",Rno,CHANNEL_ID];
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&aPartyMsisdn=%@&channelId=%@&serviceId=1&activityId=4&rbtMode=101",Rno,appDelegate.Language,appDelegate.MobileNumber,CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/view-black-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==VIEW_BLACKLIST)
    {
        NSLog(@"VIEW_BLACKLIST onSuccessResponseRecived = %@",Response);
       Response =  [Response objectForKey:@"responseMap"];
       _Blacklist = [[Response objectForKey:@"bPartyDetailsList"]mutableCopy];
        
        NSLog(@"_Blacklist = %@",_Blacklist);
        
    }
    else if (appDelegate.APIFlag==ADD_BLACKLIST)
    {
        NSLog(@"ADD_BLACKLIST onSuccessResponseRecived = %@",Response);
        
        //TEMP REMOVE - for testing
        NSString * str = [_BlacklistNumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_BlacklistNumberTextField.text length])];
        NSMutableDictionary *DetailsTemp = [NSMutableDictionary new];
        [DetailsTemp setObject:str forKey:@"bPartyMsisdn"];
        [DetailsTemp setObject:_Name.text forKey:@"bPartyName"];
        [_Blacklist addObject:DetailsTemp];
        _BlacklistNumberTextField.text=@"";
        _Name.text=@"";
        //end
        
        
    }
    else
    {
        NSLog(@"REMOVE_BLACKLIST onSuccessResponseRecived = %@",Response);
        [_Blacklist removeObjectAtIndex:_ButtonTag];
    }
    
    if(_Blacklist.count <= 0)
    {
        [_AlertView setHidden:false];
    }
    else
    {
        [_AlertView setHidden:true];
    }
    
    [_TableView reloadData];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"BLACKLIST onFailureResponseRecived = %@",Response);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==VIEW_BLACKLIST)
    {
         [_AlertView setHidden:false];
    }
    else
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    }
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onSelectedContactButton:(UIButton *)sender
{
    NSLog(@"onSelectedContactButton");
    [self Initialise];
    
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    
   //_BlacklistNumberTextField.text = @"";
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
    
    
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    _PhoneNumbers = contact.phoneNumbers;
    _Name.text = [NSString stringWithFormat:@"%@ %@" ,contact.givenName,contact.familyName];
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
}

-(void)ShowMultipleNumbers
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _BlacklistNumberTextField.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _BlacklistNumberTextField.text = [label.value stringValue];
    }
    
}


- (IBAction)onSelectedAddButton:(id)sender
{
    BOOL Status = true;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_BlacklistNumberTextField.text.length < 5 || _Name.text.length <= 0)
    {
        
        if(_BlacklistNumberTextField.text.length < 5)
        {
            _BlackListNumberView.layer.borderWidth = 1.0f;
            _BlackListNumberView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
            _BlackListNumberErrorIcon.hidden =false;
            
            [_BlacklistNumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Blacklist Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
            
            if(_BlacklistNumberTextField.text.length <= 0)
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            }
            else
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"*please enter a valid number",nil,appDelegate.LocalBundel,nil);
            }
           
        }
        
        
        if(_Name.text.length <= 0)
        {
            _NameView.layer.borderWidth = 1.0f;
            _NameView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
            _NameErrorIcon.hidden =false;
             [_Name setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Name",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        }
        
        [_AlertLabel setHidden:false];
        Status = false;
        
        
        
    }
    else
    {
        
        for(int i=0;i<_Blacklist.count;i++)
        {
            
                NSDictionary *temp = [_Blacklist objectAtIndex:i];
                NSString *MSISDN = [NSString stringWithFormat:@"%@",[temp objectForKey:@"bPartyMsisdn"]];
            
                Status = [SIXDGlobalViewController isMSISDNSame :_BlacklistNumberTextField.text :  MSISDN];
                
                if(!Status)
                {
                    _BlackListNumberView.layer.borderWidth = 1.0f;
                    _BlackListNumberView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
                    _BlackListNumberErrorIcon.hidden =false;
                    _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"Already added member",nil,appDelegate.LocalBundel,nil);
                    _AlertLabel.hidden=NO;
                    break;
                }
                
            }
    }

    if(Status)
    {
        [self API_AddToBlackList];
    }
    
}


- (IBAction)onSelectedDeleteButton:(UIButton *)sender
{
    NSLog(@"API_RemoveFromBlackList >>>");
    _ButtonTag = sender.tag;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Are you sure you want to delete?",nil,appDelegate.LocalBundel,nil);
    ViewController.title = @"";
     ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText = @"OK";
    ViewController.ParentView = self;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    
}


-(void)onSelectedPopUpConfirmButton
{
    [self API_RemoveFromBlackList];
}

@end
