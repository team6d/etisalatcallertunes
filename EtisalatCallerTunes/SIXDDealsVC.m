//
//  SIXDDealsVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 15/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDDealsVC.h"
#import "SIXDCustomTVCell.h"
#import "AppDelegate.h"
#import "SIXDPlayerVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDHeader.h"
#import "SIXDPageViewController.h"
#import "SIXDGlobalViewController.h"
#import "SIXDStylist.h"


@interface SIXDDealsVC ()

@end

@implementation SIXDDealsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Deals"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(appDelegate.PromotionList.count <= 0)
    {
        NSLog(@"Deals empty");
        _MainContainer.hidden = true;
        UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"DEALS_INFO",nil,appDelegate.LocalBundel,nil) :0];
        [_AlertView addSubview:Alert];
        [self.view addSubview:_AlertView];
        [_AlertView setHidden:false];
        _BannerImage.hidden = false;
        _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"DEALS",nil,appDelegate.LocalBundel,nil);
        
    }
    else
    {
        _MainContainer.hidden = false;
        _HeadingLabel.hidden = true;
        _AlertView.hidden = true;
        _BannerImage.hidden = true;
    
        
    }
    
    // Do any additional setup after loading the view.
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem_MainNavigationPage:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.PromotionList.count > 0)
    {
        NSString *Font;
        Font = @"ArialMT";
        /*
        if([appDelegate.Language isEqualToString:@"Arabic"])
            Font = @"GESSTextMedium-Medium";
        else
            Font = @"ArialMT";
        */
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:Font size:17]}];
        self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"DEALS",nil,appDelegate.LocalBundel,nil);
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"DEALS"])
    {
       SIXDPageViewController *ViewController = (SIXDPageViewController *) [segue destinationViewController];
        ViewController.Source = DEALS;
        [self addChildViewController:ViewController];
    }
}

@end
