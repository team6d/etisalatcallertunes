//
//  SIXDNestedCVCell.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDNestedCVCell.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "SIXDPlayerVC.h"
#import "SIXDBuyToneVC.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDStylist.h"
#import "SIXDGlobalViewController.h"


@implementation SIXDNestedCVCell

- (id)init
{
    _totalToneLikeCount = 0;
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return _CVContentList.count;
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if((_section*5)+(indexPath.row) < _TuneList.count)
    {
       
       NSMutableDictionary *Dict = [_TuneList objectAtIndex:(_section*5)+(indexPath.row)];
    
        //***set like count *********
        SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
        if([Dict objectForKey:@"likeCount"])
        {
            [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
        }
        else
            cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
        //***set like count enda *********
        
        
        cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"artistName"]];
   
        cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
    
       
        NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
        [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    image = [[UIImage imageNamed:@"topchartsbuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.BuyButton setImage:image forState:UIControlStateNormal];
    [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
        NSLog(@"NestedCV::Calling likeSettingsSession >>>");
        [SIXDStylist loadLikeUnlikeSetings:cell :[Dict objectForKey:@"toneId"]];
        
       
        int x_Position;
        if([appDelegate.Language isEqualToString:@"English"])
        {
            x_Position=0;
        }
        else
        {
            x_Position=50-1;
        }
        UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder1.backgroundColor = /*[UIColor blueColor];*/UIColorFromRGB(CLR_FAQ_1);
        
        [cell.LikeButton addSubview:leftBorder1];
        
        UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder2.backgroundColor = /*[UIColor blueColor];*/UIColorFromRGB(CLR_FAQ_1);
        
        [cell.BuyButton addSubview:leftBorder2];
        
        UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder3.backgroundColor = /*[UIColor blueColor];*/UIColorFromRGB(CLR_FAQ_1);
         [cell.AccessoryButton addSubview:leftBorder3];
    
    
    cell.BuyButton.tag = indexPath.row;
    cell.LikeButton.tag = indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    cell.SubView.backgroundColor = [UIColor whiteColor];
    
    [self.contentView bringSubviewToFront:cell.contentView];
    [cell.contentView bringSubviewToFront:cell.SubView];
        [cell.SubView setHidden:false];
    }
    else
    {
        [cell.SubView setHidden:true];
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath SIXDNestedCVCell >>");
    if((_section*5)+(indexPath.row) < _TuneList.count)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        
        ViewController.PlayList= _TuneList;
        ViewController.CurrentToneIndex = (_section*5)+(indexPath.row);
        ViewController.CurrentToneIndex = ViewController.CurrentToneIndex;
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
        NSLog(@"didSelectRowAtIndexPath SIXDNestedCVCell <<");
    }
}

- (IBAction)onSelectedBuyButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    
    //commented as CVContentlist is empty in subcategories
   // ViewController.ToneDetails=[_CVContentList objectAtIndex:sender.tag];
    
    ViewController.ToneDetails= [_TuneList objectAtIndex:(_section*5)+(sender.tag)];
    
    [appDelegate.HomeObj.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onSelectedLikeButton:(UIButton*)sender
{
    NSLog(@"NestedCV::Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    NSMutableDictionary *TuneDetails = [[_TuneList objectAtIndex:(_section*5)+(sender.tag)]mutableCopy];
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
    //NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
         UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
          
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
       
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
        }
            break;
    
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
}



- (IBAction)onSelectedMoreButton:(UIButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_TuneList;
    ViewController.CurrentToneIndex = (_section*5)+(sender.tag);
    [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    
}


@end
