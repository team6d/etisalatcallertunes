//
//  SIXDLoginVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDLoginVC : ViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *MobileNumberView;

@property (weak, nonatomic) IBOutlet UITextField *MobileNumber;

@property (weak, nonatomic) IBOutlet UIButton *SendOtpButton;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

@property (weak, nonatomic) IBOutlet UILabel *MobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;

- (IBAction)onClickedSendOtpButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@end
