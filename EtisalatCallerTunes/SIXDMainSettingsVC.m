//
//  SIXDMainSettingsVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDMainSettingsVC.h"
#import "AppDelegate.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDSettingsTimeVC.h"
#import "SIXDSettingsGroupVC.h"
#import "SIXDStylist.h"
#import "SIXDCommonPopUp_Text.h"

@interface SIXDMainSettingsVC ()

@end

@implementation SIXDMainSettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _isStatusRBT = FALSE;
    _onClickedConfirmFromSettingsPopUP = false;
    
    if(_ScreenType == FIRST_SETTINGS_SCREEN)
    {
        [SIXDStylist setGAScreenTracker:@"Tune_Settings"];
        _SettingsDataDict = [NSMutableDictionary new];
        _TimeBasedViewHeight.constant = 0;
        [_TimeSettingView setHidden:YES];
        [_SetTuneButton setBackgroundColor:[UIColor grayColor]];
    }
    else if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
    {
        NSLog(@"_toneDetails = %@",_ToneDetails);
        [SIXDStylist setGAScreenTracker:@"Settings_StopPlayingToCallers"];
        _UserSettingsType = -1;
        _TimeBasedViewHeight.constant = 137;
        [_TimeSettingView setHidden:NO];
        [_SetTuneButton setBackgroundColor:(UIColorFromRGB(CLR_ETISALAT_GREEN))];
    }
    
    if([_ToneDetails objectForKey:@"artistName"])
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"artistName"] capitalizedString];
        _BottomLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
    else
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
    
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    [_MainImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    [self localisation];
    
     //for filling TimeSettings view
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     _BelowLeftTopLabel.text = NSLocalizedStringFromTableInBundle(@"Time",nil,appDelegate.LocalBundel,nil);
     _BelowCenterTopLabel.text = NSLocalizedStringFromTableInBundle(@"FROM",nil,appDelegate.LocalBundel,nil);
     _BelowRightTopKLabel.text = NSLocalizedStringFromTableInBundle(@"To",nil,appDelegate.LocalBundel,nil);
     _BelowTopLabel.text = NSLocalizedStringFromTableInBundle(@"REPEAT",nil,appDelegate.LocalBundel,nil);
    
    UITapGestureRecognizer *TimeSettingsTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTimeSettingsViewTap)];
    [_TimeSettingView addGestureRecognizer:TimeSettingsTap];
    
    
}

-(void)handleTimeSettingsViewTap
{
    if(_ScreenType ==PLAYING_TO_CALLERS_SETTINGS_SCREEN)
    {
        NSLog(@"PLAYING_TO_CALLERS_SETTINGS_SCREEN  >>");
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
        SIXDSettingsTimeVC *SIXDSettingsTimeVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsTimeVC"];
        SIXDSettingsTimeVC.ToneDetails = _ToneDetails;
        SIXDSettingsTimeVC.MainSettingsVC = self;
        [self.navigationController pushViewController:SIXDSettingsTimeVC animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 2; 
    switch (_ScreenType) {
        case FIRST_SETTINGS_SCREEN:
        case PLAYING_TO_CALLERS_SETTINGS_SCREEN:
            NSLog(@"No of rows = 2");
            rows =2;
            break;
            
        case END_SETTINGS_SCREEN:
            NSLog(@"No of rows = 1");
            rows = 1;
            break;
            
        default:
            NSLog(@"default");
            break;
    }
    return rows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSLog(@"MainSettings::cellForRowAtIndexPath");
   // UserSettingsType
   // TimeSettingsType
    
    switch(_ScreenType)
    {
        
    case FIRST_SETTINGS_SCREEN:
        {
            if(indexPath.row==0)
            {
                NSLog(@"first settings screen");
                NSString *StatusRBT = [_ToneDetails objectForKey:@"isStatusRbt"];
                if(StatusRBT)
                {
                    if([StatusRBT isEqualToString:@"T"])
                    {
                        [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsstatus"] forState:UIControlStateNormal];
                        cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                        cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
                        [cell.SubTitleLabel setHidden:false];
                        cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"All",nil,appDelegate.LocalBundel,nil);
                        cell.userInteractionEnabled = false;
                        _UserSettingsType = SETTINGS_USER_ALL_CALLERS;
                        _isStatusRBT = TRUE;
                        
                        
                    }
                    else
                    {
                        _isStatusRBT = FALSE;
                        [cell.SubTitleLabel setHidden:YES];
                        cell.TitleLabel.text =  NSLocalizedStringFromTableInBundle(@"SELECT TO WHOM YOU WANT TO PLAY IT",nil,appDelegate.LocalBundel,nil);
                        [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingscallers"] forState:UIControlStateNormal];
                        cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                    }
                }
                else
                {
                    _isStatusRBT = FALSE;
                    [cell.SubTitleLabel setHidden:YES];
                    cell.TitleLabel.text =  NSLocalizedStringFromTableInBundle(@"SELECT TO WHOM YOU WANT TO PLAY IT",nil,appDelegate.LocalBundel,nil);
                    [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingscallers"] forState:UIControlStateNormal];
                    cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                }
                
            }
            else
            {
                NSLog(@"first settings screen 2");
         
                    [cell.SubTitleLabel  setHidden:YES];
                    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"SELECT WHEN YOU WANT TO PLAY IT",nil,appDelegate.LocalBundel,nil);
                    [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsclock"] forState:UIControlStateNormal];
                    cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                    cell.userInteractionEnabled = true;
            }
            
        }
        break;
    
        case END_SETTINGS_SCREEN:
        {
            
            if(indexPath.row==0)
            {
                
                NSString *StatusRBT = [_ToneDetails objectForKey:@"isStatusRbt"];
                if(StatusRBT)
                {
                    if([StatusRBT isEqualToString:@"T"])
                    {
                        [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsstatus"] forState:UIControlStateNormal];
                        cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                        cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
                        [cell.SubTitleLabel setHidden:false];
                        cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"All",nil,appDelegate.LocalBundel,nil);
                        cell.userInteractionEnabled = false;
                        _UserSettingsType = SETTINGS_USER_ALL_CALLERS;
                        _isStatusRBT = TRUE;
                    }
                    else
                    {
                        NSLog(@"EndSettings ");
                        [cell.SubTitleLabel setHidden:YES];
                        cell.TitleLabel.text =  NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
                        [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsstatus"] forState:UIControlStateNormal];
                        cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                        _isStatusRBT = FALSE;
                    }
                }
                else
                {
                    NSLog(@"EndSettings ");
                    [cell.SubTitleLabel setHidden:YES];
                    cell.TitleLabel.text =  NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
                    [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsstatus"] forState:UIControlStateNormal];
                    cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                    _isStatusRBT = FALSE;
                }
                
            }
    
            
        }
            break;
            
        case PLAYING_TO_CALLERS_SETTINGS_SCREEN:
        {
            
            [cell setUserInteractionEnabled:FALSE];
            if(indexPath.row==0)
            {
                //cell.TitleLabel.text = @"STATUS";
                NSString *ShuffleStatus = [_ToneDetails objectForKey:@"isShuffle"];
                if(ShuffleStatus)
                {
                    if([ShuffleStatus isEqualToString:@"T"])
                        cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"Shuffle",nil,appDelegate.LocalBundel,nil);
                    
                }
                else
                {
                    cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"live",nil,appDelegate.LocalBundel,nil);
                }
                
                cell.TitleLabel.text =  NSLocalizedStringFromTableInBundle(@"STATUS",nil,appDelegate.LocalBundel,nil);
                [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingsstatus"] forState:UIControlStateNormal];
                cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                [cell.SubTitleLabel  setHidden:NO];
            
                
            }
            else
            {
                cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
                [cell.SubTitleLabel  setHidden:NO];
                if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"AllCaller"])
                {
                    cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"All",nil,appDelegate.LocalBundel,nil);
                }
                else if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"])
                {
                     cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"All",nil,appDelegate.LocalBundel,nil);
                }
                else if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"SpecialCallerSetting"])
                {
                    cell.SubTitleLabel.text =[_ToneDetails objectForKey:@"msisdnB"];
                }
                else
                {
                     cell.SubTitleLabel.text =[_ToneDetails objectForKey:@"groupName"];
                }

                [cell.AccessoryButton setImage:[UIImage imageNamed:@"settingscallers"] forState:UIControlStateNormal];
                cell.AccessoryButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
                
            }
            
            
        }
            break;
    }
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    /*
    switch(_ScreenType)
    {
            
        case FIRST_SETTINGS_SCREEN:
        {
            NSLog(@"FIRST_SETTINGS_SCREEN row = %ld",(long)indexPath.row);
            if(indexPath.row == 0)
            {
                SIXDSettingsGroupVC *SIXDSettingsGroupVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsGroupVC"];
                SIXDSettingsGroupVC.ToneDetails = _ToneDetails;
                SIXDSettingsGroupVC.MainSettingsVC = self;
                [self.navigationController pushViewController:SIXDSettingsGroupVC animated:NO];

            }
            else if(indexPath.row == 1)
            {
                SIXDSettingsTimeVC *SIXDSettingsTimeVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsTimeVC"];
                SIXDSettingsTimeVC.ToneDetails = _ToneDetails;
                SIXDSettingsTimeVC.MainSettingsVC = self;
                [self.navigationController pushViewController:SIXDSettingsTimeVC animated:NO];
            }
        }
            break;
            
            
        case END_SETTINGS_SCREEN:
        {
            
        }
            break;
    }
     */
    _onClickedConfirmFromSettingsPopUP = false;
    if(indexPath.row == 0)
    {
        SIXDSettingsGroupVC *SIXDSettingsGroupVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsGroupVC"];
        SIXDSettingsGroupVC.ToneDetails = _ToneDetails;
        SIXDSettingsGroupVC.MainSettingsVC = self;
        [self.navigationController pushViewController:SIXDSettingsGroupVC animated:NO];
        
    }
    else if(indexPath.row == 1)
    {
        SIXDSettingsTimeVC *SIXDSettingsTimeVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsTimeVC"];
        SIXDSettingsTimeVC.ToneDetails = _ToneDetails;
        SIXDSettingsTimeVC.MainSettingsVC = self;
        [self.navigationController pushViewController:SIXDSettingsTimeVC animated:NO];
    }
    
}

-(void)localisation
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    switch (_ScreenType)
    {
        case FIRST_SETTINGS_SCREEN:
             [_SetTuneButton setTitle:NSLocalizedStringFromTableInBundle(@"SET THE TUNE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            break;
            
        case END_SETTINGS_SCREEN:
            [_SetTuneButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            break;
            
        case PLAYING_TO_CALLERS_SETTINGS_SCREEN:
            [_SetTuneButton setTitle:NSLocalizedStringFromTableInBundle(@"STOP PLAYING TO MY CALLERS",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
    
}
- (IBAction)onClickedSetTuneButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSLog(@"onClickedDelete Button >>");
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.ParentView = self;
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Are you sure you want to stop it?",nil,appDelegate.LocalBundel,nil);
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil);
        _PopUpFlag=0;
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        
        NSString *GAEventDetails;
        if (_BottomLabel.text.length <= 0)
        {
            GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_TopLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
        }
        else
        {
            GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
        }
        
        [SIXDStylist setGAEventTracker:@"Settings" :@"Set the tune" :GAEventDetails];
        
        SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
        NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
        ReqHandler.HttpsReqType = POST;
        
        _httpRequestString = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=%@&toneId=%@&language=%@&priority=0&channelId=%@",Rno,appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],appDelegate.Language,CHANNEL_ID];
        
        [self fillHttpRequestData];
        
        ReqHandler.Request = _httpRequestString;
        switch(_UserSettingsType)
        {
            case SETTINGS_USER_DEDICATED:
                ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/dedicated-user-tone-addition-with-time-setting",appDelegate.BaseMiddlewareURL];
                break;
                
            case SETTINGS_USER_GROUPS:
                ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/add-tone-to-group-with-time-setting",appDelegate.BaseMiddlewareURL];
                break;
                
            case SETTINGS_USER_ALL_CALLERS:
                ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/time-based-setting-for-already-activated",appDelegate.BaseMiddlewareURL];
                break;
        }
        
        [self.tabBarController.view addSubview:self.overlayview];
        [_ActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_ActivityIndicator];
        [ReqHandler sendHttpPostRequest:self];
    }
    
}


- (void) fillHttpRequestData
{
    NSLog(@"fillHttpRequestData >>>");
    
    
    [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
    
    switch(_UserSettingsType)
    {
        case SETTINGS_USER_DEDICATED:
            _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&bPartyMsisdn=%@&serviceId=13&activityId=1&packName=%@",[_SettingsDataDict objectForKey:@"bPartyMsisdn"],[[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"]]];
            break;
            
        case SETTINGS_USER_GROUPS:
        {
            NSMutableArray *ToneDetailsArray = [NSMutableArray new];
        
            [ToneDetailsArray addObject:[_ToneDetails objectForKey:@"toneId"]];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ToneDetailsArray options:0 error:&error];
            NSString *ToneDetails;
            if (! jsonData) {
                NSLog(@"Got an error: %@", error);
            } else {
                ToneDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }

            
            _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&groupId=%@&groupName=%@&groupDesc=%@&serviceId=2&activityId=13&toneDetails=%@",[_SettingsDataDict objectForKey:@"groupId"],[_SettingsDataDict objectForKey:@"groupName"],[_SettingsDataDict objectForKey:@"groupName"],ToneDetails]];
        }
            break;
          
       
        case SETTINGS_USER_ALL_CALLERS:
            if(_isStatusRBT)
            {
                 _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&serviceId=17&activityId=1&mode=24"]];
            }
            else
            {
                _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&serviceId=17&activityId=1"]];
            }
            break;
    }
    
    switch (_TimeSettingsType)
    {
        case SETTINGS_DAILY_TIME:
            _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&timeType=%d&startTime=%@&endTime=%@",SETTINGS_DAILY_TIME,[_SettingsDataDict objectForKey:@"startTime"] ,[_SettingsDataDict objectForKey:@"endTime"]]];
            break;
           
        case SETTINGS_WEEKLY_TIME:
        case SETTINGS_SPECIFIC_TIME:
             _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&timeType=%d&weeklyDays=%@&weeklyStartTime=%@&weeklyEndTime=%@",SETTINGS_WEEKLY_TIME,[_SettingsDataDict objectForKey:@"weeklyDays"] ,[_SettingsDataDict objectForKey:@"weeklyStartTime"],[_SettingsDataDict objectForKey:@"weeklyEndTime"]]];
            break;
            
        case SETTINGS_MONTLY_TIME:
            _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&timeType=%d&startDayMonthly=%@&endDayMonthly=%@&monthlyStartTime=%@&monthlyEndTime=%@",SETTINGS_MONTLY_TIME,[_SettingsDataDict objectForKey:@"startDayMonthly"] ,[_SettingsDataDict objectForKey:@"endDayMonthly"],[_SettingsDataDict objectForKey:@"monthlyStartTime"],[_SettingsDataDict objectForKey:@"monthlyEndTime"]]];
            break;
            
        case SETTINGS_YEARLY_TIME:
             _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&timeType=%d&yearlyStartMonth=%@&yearlyEndMonth=%@&yearlyStartDay=%@&yearlyEndDay=%@&yearlyStartTime=%@&yearlyEndTime=%@",SETTINGS_YEARLY_TIME,[_SettingsDataDict objectForKey:@"yearlyStartMonth"] ,[_SettingsDataDict objectForKey:@"yearlyEndMonth"],[_SettingsDataDict objectForKey:@"yearlyStartDay"],[_SettingsDataDict objectForKey:@"yearlyEndDay"],[_SettingsDataDict objectForKey:@"yearlyStartTime"],[_SettingsDataDict objectForKey:@"yearlyEndTime"]]];
            break;
            
        case SETTINGS_BETWEEN_DATES:
            _httpRequestString = [_httpRequestString stringByAppendingString: [NSString stringWithFormat:@"&timeType=%d&customizeStartDate=%@&customizeEndDate=%@&customizeStartTime=%@&customizeEndTime=%@",SETTINGS_BETWEEN_DATES,[_SettingsDataDict objectForKey:@"customizeStartDate"] ,[_SettingsDataDict objectForKey:@"customizeEndDate"],[_SettingsDataDict objectForKey:@"monthlyStartTime"],[_SettingsDataDict objectForKey:@"monthlyEndTime"]]];
            break;
            
        default :
            NSLog(@"Settings type mismatch");
            break;
    }
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived>>> Response = %@",Response);
    _PopUpFlag=1;
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
    {
        _PlayingToMyCallerVC.IsRefreshRequired = true;
    }
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.AlertMessage = Message;
    ViewController.title = @"";
    ViewController.HideCloseButton = true;
    ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    ViewController.ParentView = self;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
     */
    [self.navigationController popViewControllerAnimated:false];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"MainSettings onFailureResponseRecived = %@",Response);
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"SETTINGS",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
    
    if(_ScreenType == FIRST_SETTINGS_SCREEN)
    {
        _TimeBasedViewHeight.constant = 0;
        [_TimeSettingView setHidden:YES];
        [_SetTuneButton setBackgroundColor:[UIColor grayColor]];
        [_SetTuneButton setUserInteractionEnabled:NO];
    }
    else
    {
        if(_UserSettingsType != -1)
        {
            NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:0 inSection:0] ;
            SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
            
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"CALLERS",nil,appDelegate.LocalBundel,nil);
            [cell.SubTitleLabel setHidden:false];
            
            switch (_UserSettingsType)
            {
                case SETTINGS_USER_ALL_CALLERS:
                    cell.SubTitleLabel.text = NSLocalizedStringFromTableInBundle(@"All",nil,appDelegate.LocalBundel,nil);
                    break;
                
                case SETTINGS_USER_GROUPS:
                    cell.SubTitleLabel.text =[_SettingsDataDict objectForKey:@"groupName"] ;
                    break;
                    
                case SETTINGS_USER_DEDICATED:
                    cell.SubTitleLabel.text = [_SettingsDataDict objectForKey:@"bPartyMsisdnLabel"] ;
                    break;
                    
                default:
                    break;
            }
        }
    
        if(_TimeSettingsType != -1)
        {
            if(_ScreenType != PLAYING_TO_CALLERS_SETTINGS_SCREEN)
            {
                //show only 1 row in table view
                _TableViewHeight.constant = 90;
            }
            NSLog(@"Filling Time Settings View");
            switch (_TimeSettingsType)
            {
                case SETTINGS_WEEKLY_TIME:
                     NSLog(@"In SETTINGS_WEEKLY_TIME");
                    _BelowLeftBottomLabel.text = NSLocalizedStringFromTableInBundle(@"Full Day",nil,appDelegate.LocalBundel,nil);
                    [self fillTimeSettingsView:[_SettingsDataDict objectForKey:@"weeklyStartTime"] :[_SettingsDataDict objectForKey:@"weeklyEndTime"] :[_SettingsDataDict objectForKey:@"weeklyDays"]];
                    
                    [_TimeSettingView setHidden:NO];
                    _TimeBasedViewHeight.constant = 137;
                     break;
                    
                    
                case SETTINGS_SPECIFIC_TIME:
                     NSLog(@"In SETTINGS_SPECIFIC_TIME");
                    _BelowLeftBottomLabel.text = NSLocalizedStringFromTableInBundle(@"Specific Time",nil,appDelegate.LocalBundel,nil);
                    [self fillTimeSettingsView:[_SettingsDataDict objectForKey:@"weeklyStartTime"] :[_SettingsDataDict objectForKey:@"weeklyEndTime"] :[_SettingsDataDict objectForKey:@"weeklyDays"]];
                    [_TimeSettingView setHidden:NO];
                    _TimeBasedViewHeight.constant = 137;
            
                    break;
                    
                    
                case SETTINGS_BETWEEN_DATES:
                     NSLog(@"In SETTINGS_BETWEEN_DATES");
                    if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
                    {
                        _BelowCenterBottomLabel.text = [NSString stringWithFormat:@"%@ %@",[_SettingsDataDict objectForKey:@"customizeStartDate"],[_SettingsDataDict objectForKey:@"monthlyStartTime"]];
                        
                        _BelowRightBottomLabel.text = [NSString stringWithFormat:@"%@ %@",[_SettingsDataDict objectForKey:@"customizeEndDate"],[_SettingsDataDict objectForKey:@"monthlyEndTime"]];
                        
                    }
                    _BelowLeftBottomLabel.text =NSLocalizedStringFromTableInBundle(@"Specific Date",nil,appDelegate.LocalBundel,nil);
                    _BelowBottomLabel.text = NSLocalizedStringFromTableInBundle(@"NO",nil,appDelegate.LocalBundel,nil);
                    [_TimeSettingView setHidden:NO];
                    _TimeBasedViewHeight.constant = 137;
                    
                    break;
                    
                case SETTINGS_MONTLY_TIME:
                     NSLog(@"In SETTINGS_MONTLY_TIME");
                    if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
                    {
                        _BelowCenterBottomLabel.text = [NSString stringWithFormat:@"%@ %@",[_SettingsDataDict objectForKey:@"startDayMonthly"],[_SettingsDataDict objectForKey:@"monthlyStartTime"]];
                        
                        _BelowRightBottomLabel.text = [NSString stringWithFormat:@"%@ %@",[_SettingsDataDict objectForKey:@"endDayMonthly"],[_SettingsDataDict objectForKey:@"monthlyEndTime"]];
                        
                    }
                    _BelowLeftBottomLabel.text =NSLocalizedStringFromTableInBundle(@"Specific Date",nil,appDelegate.LocalBundel,nil);
                    _BelowBottomLabel.text =NSLocalizedStringFromTableInBundle(@"Monthly",nil,appDelegate.LocalBundel,nil);
                    [_TimeSettingView setHidden:NO];
                    _TimeBasedViewHeight.constant = 137;
                    break;
                    
                case SETTINGS_YEARLY_TIME:
                     NSLog(@"In SETTINGS_YEARLY_TIME");
                    if(_ScreenType == PLAYING_TO_CALLERS_SETTINGS_SCREEN)
                    {
                        _BelowCenterBottomLabel.text = [NSString stringWithFormat:@"%@-%@ %@",[_SettingsDataDict objectForKey:@"yearlyStartDay"],[_SettingsDataDict objectForKey:@"yearlyStartMonth"],[_SettingsDataDict objectForKey:@"yearlyStartTime"]];
                        
                        _BelowRightBottomLabel.text = [NSString stringWithFormat:@"%@-%@ %@",[_SettingsDataDict objectForKey:@"yearlyEndDay"],[_SettingsDataDict objectForKey:@"yearlyEndMonth"],[_SettingsDataDict objectForKey:@"yearlyEndTime"]];
                        
                    }
                    _BelowLeftBottomLabel.text =NSLocalizedStringFromTableInBundle(@"Specific Date",nil,appDelegate.LocalBundel,nil);
                    _BelowBottomLabel.text = NSLocalizedStringFromTableInBundle(@"Yearly",nil,appDelegate.LocalBundel,nil);
                    [_TimeSettingView setHidden:NO];
                    _TimeBasedViewHeight.constant = 137;
                    break;
                    
                    
                default:
                    NSLog(@"In deafault");
                    [_TimeSettingView setHidden:YES];
                    _TimeBasedViewHeight.constant = 0;
                    
                    break;
            }
            
            
            
        }
        
        if(_UserSettingsType != -1 && _TimeSettingsType != -1)
        {
            [_SetTuneButton setBackgroundColor:(UIColorFromRGB(CLR_ETISALAT_GREEN))];
            [_SetTuneButton setUserInteractionEnabled:YES];
        }
        
        if(_UserSettingsType == -1 && _TimeSettingsType != -1 && _onClickedConfirmFromSettingsPopUP)
        {
            _onClickedConfirmFromSettingsPopUP = false;
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
            SIXDSettingsGroupVC *SIXDSettingsGroupVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsGroupVC"];
            SIXDSettingsGroupVC.ToneDetails = _ToneDetails;
            SIXDSettingsGroupVC.MainSettingsVC = self;
            [self.navigationController pushViewController:SIXDSettingsGroupVC animated:NO];
        
        }
        else if (_UserSettingsType != -1 && _TimeSettingsType == -1 && _onClickedConfirmFromSettingsPopUP)
        {
            _onClickedConfirmFromSettingsPopUP = false;    
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
            SIXDSettingsTimeVC *SIXDSettingsTimeVC = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSettingsTimeVC"];
            SIXDSettingsTimeVC.ToneDetails = _ToneDetails;
            SIXDSettingsTimeVC.MainSettingsVC = self;
            [self.navigationController pushViewController:SIXDSettingsTimeVC animated:NO];
        }
        
    }
   // [_TableView reloadData];
}


-(void)fillTimeSettingsView : (NSString*) StartDate :(NSString*) EndDate : (NSString*) SelectedDaysString
{
    //for testing
    _BelowCenterBottomLabel.text = StartDate;
    _BelowRightBottomLabel.text = EndDate;
    
    NSArray *Days = @[@"S  ",@"M  ",@"T  ",@"W  ",@"T  ",@"F  ",@"S  "];
    //NSArray *SelectedDays = @[@"1",@"4"];
    
    NSAttributedString *titleString;     NSMutableAttributedString *titleString0 = [NSMutableAttributedString new];
    
    NSArray *SelectedDays = [SelectedDaysString componentsSeparatedByString:@","];

    for (int i=0; i<Days.count; i++)
    {
        BOOL IsSelected  = false;
        for (int j=0; j<SelectedDays.count; j++)
        {
            if(i+1 == [[SelectedDays objectAtIndex:j]intValue])
            {
                IsSelected = true;
            }
        }
        
        if(IsSelected)
        {
            titleString = [[NSAttributedString alloc] initWithString:[Days objectAtIndex:i] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN)}];
        }
        else
        {
            titleString = [[NSAttributedString alloc] initWithString:[Days objectAtIndex:i] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER)}];
        }
        [titleString0 appendAttributedString:titleString];
        
    }
    _BelowBottomLabel.attributedText = titleString0;
    
    //end
    
}

-(void)onSelectedPopUpConfirmButton
{
    NSString *GAEventDetails;
    if (_BottomLabel.text.length <= 0)
    {
        GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_TopLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    }
    else
    {
        GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    }
    
    [SIXDStylist setGAEventTracker:@"Settings" :@"Stop play to my callers" :GAEventDetails];

    [self stopPlayingToMyCallers];
}

-(void)stopPlayingToMyCallers
{
    
    if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"AllCaller"])
    {
        [self API_removeToneFromShuffle];
    }
    else if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"SpecialCallerSetting"])
    {
        [self API_removeToneFromDedication];
    }
    else if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"GroupCallerSetting"])
    {
        [self API_removeToneFromGroup];
    }
    else if([[_ToneDetails objectForKey:@"serviceName"] isEqualToString:@"STATUSRBT"])
    {
        [self API_deleteToneStatusRBT];
    }
}

-(void)API_deleteToneStatusRBT
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
   
    NSString *PackName = [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
    NSString *timeType = @"2";
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&msisdn=%@&toneId=%@&language=%@&priority=0&channelId=%@&activityId=3&serviceId=17&mode=24&packName=%@&timeType=%@",Rno,appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],appDelegate.Language,CHANNEL_ID,PackName,timeType];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/time-based-activation",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
    
}

-(void)API_removeToneFromGroup
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    NSMutableArray * ToneID = [NSMutableArray new];
    [ToneID addObject:[_ToneDetails objectForKey:@"toneId"]];
    
    //ADD LIST
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ToneID
                                                       options:0
                                                         error:&error];
    NSString *ToneIDString;
    if (! jsonData) {
        NSLog(@"Add Got an error: %@", error);
    } else {
        ToneIDString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&groupName=%@&groupId=%@&toneDetails=%@&language=%@&activityId=10&serviceId=2",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"groupName"],[_ToneDetails objectForKey:@"groupId"],ToneIDString,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/edit-group",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_removeToneFromDedication
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&bpartyMsisdn=%@&toneId=%@&language=%@",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"msisdnB"],[_ToneDetails objectForKey:@"toneId"],appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-dedication",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_removeToneFromShuffle
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSMutableArray * ToneID = [NSMutableArray new];
    NSMutableDictionary *toneIdDict = [NSMutableDictionary new];
    [toneIdDict setObject:[_ToneDetails objectForKey:@"toneId"] forKey:@"toneId"];
    [ToneID addObject:toneIdDict];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ToneID
                                                       options:0
                                                         error:&error];
    NSString *ToneIDString;
    if (! jsonData) {
        NSLog(@"Add Got an error: %@", error);
    } else {
        ToneIDString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    


    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    /*
    if(_isStatusRBT)
    {
        ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=%@&toneIdList=%@&language=%@&priority=0&channelId=%@&activityId=1&serviceId=1&mode=24",Rno,appDelegate.MobileNumber,ToneIDString,appDelegate.Language,CHANNEL_ID];
    }
    else
    {
     */
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=%@&toneIdList=%@&language=%@&priority=0&channelId=%@&activityId=1&serviceId=1",Rno,appDelegate.MobileNumber,ToneIDString,appDelegate.Language,CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-from-shuffle",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

@end
