//
//  SIXDGroupsVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDGroupsVC : ViewController
@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *InfoView;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *GroupNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *CreateButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property (weak, nonatomic) IBOutlet UIView *AlertView;
@property(strong,nonatomic) NSMutableArray *Groups_listToneApk;
- (IBAction)onClickedCreateGroupButton:(id)sender;



@end
