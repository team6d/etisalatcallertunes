//
//  SIXDMusicChannelVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 28/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDAVPlayer.h"

@interface SIXDMusicChannelVC : ViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property (weak, nonatomic) IBOutlet UIButton *LikeButton;

@property (weak, nonatomic) IBOutlet UIImageView *PlayListImage;
@property(strong,nonatomic) NSMutableDictionary *PlayListDetails;

@property(strong,nonatomic) NSMutableArray *TuneList;
- (IBAction)onSelectedBuyButton:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *PlayListDetailsView;

- (IBAction)onSelectedLikeButton:(UIButton *)sender;

- (IBAction)onSelectedPlayPauseButton:(UIButton *)sender;

@property(strong,nonatomic) SIXDAVPlayer *SIXDAVPlayer_Audio;

@property(strong,nonatomic) UIButton *SelectedButton;
@property (weak, nonatomic) IBOutlet UILabel *PlayListName;
@property (weak, nonatomic) IBOutlet UILabel *LikeLabel;

@property(nonatomic) BOOL IsSourceBanner;

@end
