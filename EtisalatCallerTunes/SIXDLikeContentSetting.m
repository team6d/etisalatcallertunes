//
//  SIXDLikeContentSetting.m
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 26/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDLikeContentSetting.h"
#import "SIXDHttpRequestHandler.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"

#define API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES 1
#define API_SEND_LIKE_UNLIKE_LIST 2




@interface SIXDLikeContentSetting ()

@end

@implementation SIXDLikeContentSetting


- (void)viewDidLoad {
}

static SIXDLikeContentSetting *singletonObject = nil;

+ (id) sharedSampleSingletonClass
{
    if (! singletonObject) {
        
        singletonObject = [[SIXDLikeContentSetting alloc] init];
    }
    return singletonObject;
}

- (id)init
{
    _AccessFlag = FALSE;
    
    if (! singletonObject) {
        
        NSLog(@"SIXDLikeContentSetting:: init >>>");
        
        _Source = 0;
        _AccessFlag = FALSE;
        singletonObject = [super init];
        //temp
       // _UserLikedToneListDownloaded = [@[@"32335",@"32331",@"32334",@"32343"] mutableCopy];
        _UserLikedToneListDownloaded = [NSMutableArray new];
        _UserLikeToneListLocal = [NSMutableArray new];
        _UserUNLikeToneListLocal = [NSMutableArray new];
        _LikeToneCategoryIdDict = [NSMutableDictionary new];
        _UNLikeToneCategoryIdDict = [NSMutableDictionary new];
        
        // Uncomment the following line to see how many times is the init method of the class is called
        // NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return singletonObject;
}

- (void) setLikeCount: (NSString *) toneId : (int)LikeCount : (UILabel *)likeLabel
{
    NSLog(@"setLikeCount >>>");
    if([self isToneIdPresentIn:_UserUNLikeToneListLocal :toneId])
    {
        NSLog(@"Decrement like count");
        -- LikeCount;
    }
    else if([self isToneIdPresentIn:_UserLikeToneListLocal :toneId])
    {
        NSLog(@"Increment like count");
        ++ LikeCount;
    }
    NSLog(@"Like count = %d",LikeCount);
    
    /**********************************************************/
     if(LikeCount>1000)
     {
         NSString *likeString;
         if(LikeCount>1000000)
         {
             likeString = [NSString stringWithFormat:@"%dM",LikeCount/1000000];
             likeLabel.text = likeString;
         }
         else
         {
             likeString = [NSString stringWithFormat:@"%dK",LikeCount/1000];
             likeLabel.text = likeString;
         }
     }
    else if(LikeCount < 0)
    {
        likeLabel.text = @"0";
    }
    else
    {
     /**********************************************************/
        likeLabel.text = [NSString stringWithFormat:@"%d",LikeCount];
    }
    
}

- (int) likeTone : (NSString *) toneId : (NSString *) categoryId : (UILabel *)likeLabel
{
    NSLog(@"likeTone::Test-TotalLikeCount toneId = %@,categoryId = %@",toneId,categoryId);
    NSLog(@"_UNLikeToneCategoryIdDict = %@",_UNLikeToneCategoryIdDict);
      NSLog(@"_LikeToneCategoryIdDict = %@",_LikeToneCategoryIdDict);
    if(!toneId || !categoryId)
    {
        NSLog(@"toneId = %@ categoryId = %@ returnting..", toneId, categoryId);
        return 0;
    }
    
    NSRange match1  = [likeLabel.text rangeOfString:@"M"];
    NSRange match2  = [likeLabel.text rangeOfString:@"K"];
    
    if(match1.location == 0)
    {
        match1.location = NSNotFound;
    }
    if(match2.location == 0)
    {
        match2.location = NSNotFound;
    }

    int likeCount = [likeLabel.text intValue];
    NSLog(@"likeLabel = %@ likeCount = %d", likeLabel.text, likeCount);
    if([self isToneIdPresentIn:_UserLikedToneListDownloaded :toneId])
    {
        if([self isToneIdPresentIn:_UserUNLikeToneListLocal :toneId])
        {
            [_UserUNLikeToneListLocal removeObject:toneId];
            [_UNLikeToneCategoryIdDict removeObjectForKey:toneId];
            if(match1.location == NSNotFound && match2.location == NSNotFound)
            {
                if(likeCount < 1000)
                    likeLabel.text = [NSString stringWithFormat:@"%d",++likeCount];
            }
           // NSLog(@"likelABEL = %@", likeLabel.text);
            return MAKE_LIKE_BUTTON_SOLID;
        }
        else
        {
            [_UserUNLikeToneListLocal addObject:toneId];
            [_UNLikeToneCategoryIdDict setObject:categoryId forKey:toneId];
            //make opaque
            if(match1.location == NSNotFound && match2.location == NSNotFound)
            {
                if(likeCount < 1000)
                    likeLabel.text = [NSString stringWithFormat:@"%d",--likeCount];
                if(likeCount < 0)
                {
                     likeLabel.text = @"0";
                }
            }
            return MAKE_LIKE_BUTTON_OPAQUE;
        }
    }
    else if(([self isToneIdPresentIn:_UserLikeToneListLocal :toneId]))
    {
        [_UserLikeToneListLocal removeObject:toneId];
        [_LikeToneCategoryIdDict removeObjectForKey:toneId];
        if(match1.location == NSNotFound && match2.location == NSNotFound)
        {
            if(likeCount < 1000)
                likeLabel.text = [NSString stringWithFormat:@"%d",--likeCount];
            if(likeCount < 0)
            {
                likeLabel.text = @"0";
            }
        }
        return MAKE_LIKE_BUTTON_OPAQUE;
        //opaque
        
    }
    else
    {
      
        [_UserLikeToneListLocal addObject:toneId];
         [_LikeToneCategoryIdDict setObject:categoryId forKey:toneId];
        if(match1.location == NSNotFound && match2.location == NSNotFound)
        {
            if(likeCount < 1000)
                likeLabel.text = [NSString stringWithFormat:@"%d",++likeCount];
        }
        return MAKE_LIKE_BUTTON_SOLID;
            
        //fill
    }
    return 0;
}


- (int) setToneButtonLikedUnliked : (NSString *) toneId
{
    NSLog(@"likeTone::setToneButtonLikedUnlikedtoneId = %@",toneId);
    
    if(!toneId)
        return 0;
    
    if([self isToneIdPresentIn:_UserLikedToneListDownloaded :toneId])
    {
        if([self isToneIdPresentIn:_UserUNLikeToneListLocal :toneId])
        {

            return MAKE_LIKE_BUTTON_OPAQUE;
        }
        return MAKE_LIKE_BUTTON_SOLID;
    }
    else if(([self isToneIdPresentIn:_UserLikeToneListLocal :toneId]))
    {
        return MAKE_LIKE_BUTTON_SOLID;
    }
    else
    {
        return MAKE_LIKE_BUTTON_OPAQUE;
    }
    return 0;
}


- (bool)isToneLikedFromDownloadedList : (NSString *)newToneId
{
    NSLog(@"isToneLikedFromDownloadedList>>>");
    for(NSString *existingToneId in _UserLikedToneListDownloaded)
    {
        NSLog(@"existingToneId = %@ , newToneId = %@", existingToneId,newToneId);
        if([existingToneId isEqualToString:newToneId])
        {
            NSLog(@"return true");
            return YES;
        }
    }
     NSLog(@"return false");
    return NO;
}

-(bool) isToneIdPresentIn : (NSMutableArray *)ToneIdList :(NSString*) toneId
{
    for(NSString *existingToneId in ToneIdList)
    {
        if([existingToneId isEqualToString:toneId])
        {
            return YES;
            break;
        }
    }
    return FALSE;
}

 - (void) sendLikeUnLikeList
{
    
    NSLog(@"sendLikeUnLikeList::Test-TotalLikeCount");
    
    _API_Identifier = API_SEND_LIKE_UNLIKE_LIST;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSString *FinalLikeList = @"";
    NSString *FinalUNLikeList = @"";
    NSLog(@"_UserLikeToneListLocal = %@",_UserLikeToneListLocal);
    for(int i =0; i<_UserLikeToneListLocal.count ; i++)
    {
        NSString *categoryId = [_LikeToneCategoryIdDict objectForKey:[_UserLikeToneListLocal objectAtIndex:i]];
        FinalLikeList = [FinalLikeList stringByAppendingString: [NSString stringWithFormat:@"%@|%@,",[_UserLikeToneListLocal objectAtIndex:i],categoryId]];
    }
    
    if ([FinalLikeList length] > 0)
    {
        FinalLikeList = [FinalLikeList substringToIndex:[FinalLikeList length] - 1];
    }
    
    NSLog(@"_UserUNLikeToneListLocal count = %ld",_UserUNLikeToneListLocal.count);
    
    for(int i =0; i<_UserUNLikeToneListLocal.count ; i++)
    {
        NSString *categoryId = [_UNLikeToneCategoryIdDict objectForKey:[_UserUNLikeToneListLocal objectAtIndex:i]];
        /*
        //this code is of no use.
        if(categoryId.length==0)
        {
            categoryId = [_UNLikeToneCategoryIdDict objectForKey:@"baseCategoryId"];
        }
        */
        FinalUNLikeList = [FinalUNLikeList stringByAppendingString: [NSString stringWithFormat:@"%@|%@,",[_UserUNLikeToneListLocal objectAtIndex:i],categoryId]];
    }
    NSLog(@"hereee1");
    if ([FinalUNLikeList length] > 0)
    {
        FinalUNLikeList = [FinalUNLikeList substringToIndex:[FinalUNLikeList length] - 1];
    }
    NSLog(@"hereee2");
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&aPartyMsisdn=%@&channelId=%@&likeToneId=%@&unlikeToneId=%@",Rno,appDelegate.Language,appDelegate.MobileNumber,CHANNEL_ID,FinalLikeList,FinalUNLikeList];
    
   // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&msisdn=%@&wishlistType=1&identifier=DeleteFromWishList&catagoryId=%@",Rno,appDelegate.MobileNumber,WishlistDeleteIds];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/like-unlike",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

- (void) downloadSubscriberLikedTunes
{
    _API_Identifier = API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES;
    
    NSLog(@"downloadSubscriberLikedTunes>>>");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    
    ReqHandler.HttpsReqType = GET;
   // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=%@&channelId=%@",Rno,appDelegate.MobileNumber,CHANNEL_ID];
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/listing-liked-tone",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?clientTxnId=%@&aPartyMsisdn=%@&channelId=%@&language=%@",URL, Rno,appDelegate.MobileNumber,CHANNEL_ID,appDelegate.Language];
    
    
    // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&msisdn=%@&wishlistType=1&identifier=DeleteFromWishList&catagoryId=%@",Rno,appDelegate.MobileNumber,WishlistDeleteIds];
    
   // ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/listing-liked-tone",appDelegate.BaseMiddlewareURL];
   [ReqHandler sendHttpGETRequest:self :nil];
}

- (void)sendTuneListAndDownload
{
    if (_Source == SRC_LOGOUT)
    {
        _NextAction = 0;
    }
    else
    {
        _NextAction = DOWNLOAD_TUNE_LIST;
    }
    
    if(_UserLikeToneListLocal.count > 0 || _UserUNLikeToneListLocal.count  > 0)
    {
        [self sendLikeUnLikeList];
    }
    
    else if(_Source == SRC_LOGOUT)
    {
        [self LogOutSession];
    }
    //_Source = 0;
}


- (void) firstLikeTunesDownloadRequest // first time call from homevc
{
    [self downloadSubscriberLikedTunes];
    
    _NextAction = DO_NOT_SEND_LIKE_UNLIKE_LIST;
    _Source = SRC_HOME_PAGE;
}

- (void)clearLocalData
{
    NSLog(@"SIXDLikeContentSetting::clearLocalData >>>");
    [_UserLikeToneListLocal removeAllObjects];
    [_UserUNLikeToneListLocal removeAllObjects];
    
    [_LikeToneCategoryIdDict removeAllObjects];
    [_UNLikeToneCategoryIdDict removeAllObjects];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
   AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSLog(@"LIKE onSuccessResponseRecived = %@",Response);
        Response =  [Response objectForKey:@"responseMap"];
    
        switch(_API_Identifier)
        {
            case API_SEND_LIKE_UNLIKE_LIST:
                [self clearLocalData];
                if (_Source == SRC_LOGOUT)
                {
                    [self LogOutSession];
                }
                if(_NextAction == DOWNLOAD_TUNE_LIST)
                {
                    [self downloadSubscriberLikedTunes];
                }
            break;
            
            case API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES:
                NSLog(@"API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES");
                
               // Response = [Response objectForKey:@"responseMap"];
                _LikedToneList = [[Response objectForKey:@"likedToneList"]mutableCopy];
                NSLog(@"_LikedToneList =%@",_LikedToneList);
                for(int i=0;i<_LikedToneList.count;i++)
                {
                    NSMutableDictionary *Templist = [_LikedToneList objectAtIndex:i];
                    [_UserLikedToneListDownloaded addObject:[Templist objectForKey:@"toneId"]];
                }
                NSLog(@"_UserLikedToneListDownloaded = %@",_UserLikedToneListDownloaded);
                if(_Source == SRC_HOME_PAGE)
                {
                    [appDelegate.HomeObj GetSettings];
                    _Source = -1; // ADDED TO HANDLE APP BG HANGING 29-may2018 by ashwini
                }
                /*
                else if (_Source == SRC_LOGOUT)
                {
                    [self LogOutSession];
                }
                */
               
            break;
            
            default:
                NSLog(@"No matching case : API_Identifier = %d",_API_Identifier);
                break;
        }
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    switch(_API_Identifier)
    {
        case API_SEND_LIKE_UNLIKE_LIST:
            NSLog(@"API_SEND_LIKE_UNLIKE_LIST");
            if(_Source == SRC_HOME_PAGE)
            {
                [appDelegate.HomeObj GetSettings];
            }
            else if (_Source == SRC_LOGOUT)
            {
                [self LogOutSession];
            }
            break;
            
        case API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES:
            NSLog(@"API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES");
            if(_Source == SRC_HOME_PAGE)
            {
                [appDelegate.HomeObj GetSettings];
                 _Source = -1; // ADDED TO HANDLE APP BG HANGING 29-may2018 by ashwini
            }
            else if (_Source == SRC_LOGOUT)
            {
                [self LogOutSession];
            }
            break;
    }
    
    NSLog(@"LIKE  onFailureResponseRecived = %@",Response);
}

-(void)onFailedToConnectToServer
{
    NSLog(@"LIKE  onFailedToConnectToServer");
    

    NSLog(@"API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    switch(_API_Identifier)
    {
        case API_DOWNLOAD_SUBSCRIBER_LIKED_TUNES:
        case API_SEND_LIKE_UNLIKE_LIST:
        if(_Source == SRC_HOME_PAGE)
        {
            [appDelegate.HomeObj GetSettings];
             _Source = -1; // ADDED TO HANDLE APP BG HANGING 29-may2018 by ashwini
        }
        else if (_Source == SRC_LOGOUT)
        {
            [self LogOutSession];
        }
            
        break;
    }
}

- (void)LogOutSession
{
    [self clearLocalData];
    
    //added
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    [ReqHandler LogOut];
    //end
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    for (UINavigationController *Temp in VCArray)
    {
        [Temp popToRootViewControllerAnimated:false];
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
    [appDelegate.window.rootViewController.view removeFromSuperview];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
    navController.navigationBarHidden = YES;
    appDelegate.window.rootViewController = navController;
    [appDelegate.window makeKeyAndVisible];
}
- (void)onInternetBroken
{
 
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
