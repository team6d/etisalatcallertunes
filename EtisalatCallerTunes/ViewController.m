//
//  ViewController.m
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 07/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)regenerateTokenFailed
{
    
}
-(void)regenerateTokenSuccess
{
    
}
-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response
{
    
}
-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response :(NSString*) URL
{
    
}
-(void)onFailureResponseRecived:(NSMutableDictionary*)Response
{
    
}
-(void)onFailedToConnectToServer
{
    
}
-(void)onInternetBroken
{
    
}

-(void)onRecordingStopped
{
    
}

- (void)onPlayStopped
{
    
}

-(void)onSelectedPopUpConfirmButton
{
    
}
-(void)onPlayStarted
{
    
}

-(void)onPlayFailure
{
    
}
@end
