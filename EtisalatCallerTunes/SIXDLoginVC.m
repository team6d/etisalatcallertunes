//
//  SIXDLoginVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDLoginVC.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"


@interface SIXDLoginVC ()

@end

@implementation SIXDLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [SIXDStylist setGAScreenTracker:@"Login_ValidateNumber"];
    
    [SIXDStylist setBackGroundImage:self.view];
    
    [_MobileNumber setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    _MobileNumber.keyboardType = ASCII_NUM_KEYBOARD;
 
    [self InitialCondition];
    [self localisation];
    
    _MobileNumberLabel.hidden = YES;
    _MobileNumber.delegate = self;
    _MobileNumber.enablesReturnKeyAutomatically = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
     [SIXDStylist addKeyBoardDoneButton:_MobileNumber :self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear >>>");
    if(![self authoriseApp])
    {
       // [self viewDidAppear:YES];
        return;
        
    }
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_MobileNumber resignFirstResponder];
    if(_MobileNumber.text.length==0)
    {
        [self InitialCondition];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)onClickedSendOtpButton:(UIButton *)sender
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(![self authoriseApp])
    {
        //[self viewDidLoad];
        return;
        
    }
    
    if(_MobileNumber.text.length==14)
    {
        _AlertLabel.text=@"";
        _AlertLabel.hidden=YES;
        
        
        NSLog(@"API_SubscriberValidation call >>");
        [_MobileNumber resignFirstResponder];
        [self API_SubscriberValidation];
    }
    else
    {
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_MobileNumber :_MobileNumberView :_AlertButton];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(![self authoriseApp])
    {
        //[self viewDidLoad];
        return;
        
    }
    _TopSpaceToTextField.constant=19;
    _MobileNumberLabel.hidden=NO;
    
    if(_MobileNumber.text.length <5)
    {
        _MobileNumber.text = @"+971-";
    }
    
   [_MobileNumber setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSLog(@"STRING = %@", string);
   
    if([textField.text rangeOfString:@"+971-"].location == NSNotFound)
    {
        NSLog(@"Return NO");
        _MobileNumber.text = @"+971-";
        return NO;
    }
    if(textField.text.length == 5 && string.length == 0)
    {
        return NO;
    }
    else if(textField.text.length == 15 && string.length == 1)
    {
        return NO;
    }
    else
        return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_MobileNumber.text.length==5)
    {
        _MobileNumber.text = @"";
        [_MobileNumber setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        [self InitialCondition];
        _TopSpaceToTextField.constant=0;
    }
}

-(void)API_SubscriberValidation
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.MobileNumber = _MobileNumber.text;
    appDelegate.MobileNumber = [appDelegate.MobileNumber stringByReplacingOccurrencesOfString:@"+971-" withString:@"0"];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&language=%@&clientTxnId=%@&type=CheckMsisdnSendOTP",appDelegate.MobileNumber,appDelegate.Language,Rno];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/subscriber-validation",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"API_SubscriberValidation onSuccessResponseRecived = %@",Response);
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSMutableDictionary *responseMap =[Response objectForKey:@"responseMap"];
    NSString *str = [responseMap objectForKey:@"respCode"];
    
    
    
    if([str isEqualToString:@"000"] || [str isEqualToString:@"SC0000"] )
    {
        NSLog(@"registered");
        [SIXDStylist setTextViewNormal:_MobileNumber : _MobileNumberView: _AlertButton];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSignInVC"];
        [self.navigationController pushViewController:ViewController animated:true];
        //[self presentViewController:ViewController animated:YES completion:nil];
        
    }
    else if([str isEqualToString:@"100"])
    {
        NSLog(@"non-registered etisalat");
        [SIXDStylist setTextViewNormal:_MobileNumber : _MobileNumberView: _AlertButton];
         UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPreRegisterVC"];
         [self presentViewController:ViewController animated:YES completion:nil];
        
    }
    //To be removed when web portal updates the API.
    else if([str isEqualToString:@"002"])
    {
        NSLog(@"password expired/ blocked by admin");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"Your password has expired. please reset your password to login",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_MobileNumber :_MobileNumberView :_AlertButton];
        
    }
    else
    {
        NSLog(@"non-registered non-etisalat");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_MobileNumber :_MobileNumberView :_AlertButton];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *message =[Response objectForKey:@"message"];
    [SIXDGlobalViewController setAlert:message :self];
    
    NSLog(@"API_SubscriberValidation onFailureResponseRecived = %@",Response);
    
    
}

-(void)onFailedToConnectToServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_SubscriberValidation onFailedToConnectToServer");
    [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) :self];
}

- (void)onInternetBroken
{
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"SIGN IN/REGISTER",nil,appDelegate.LocalBundel,nil);
    _MobileNumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
    
    [_MobileNumber setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    [_SendOtpButton setTitle:NSLocalizedStringFromTableInBundle(@"NEXT",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_AlertButton setImage:image forState:UIControlStateNormal];
}

-(void)InitialCondition
{
    _AlertLabel.hidden=YES;
    _MobileNumberLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_MobileNumber : _MobileNumberView: _AlertButton];
}

- (BOOL) authoriseApp
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([SIXDGlobalViewController isUnAuthorisedApp])
    {
         NSLog(@"App is unauthorised");
        NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"APP_UNAUTHORISED",nil,appDelegate.LocalBundel,nil);
        [SIXDGlobalViewController setAlert:AlertMessage :self];
        return NO;
    }
    NSLog(@"App is authorised");
    return YES;
}


@end
