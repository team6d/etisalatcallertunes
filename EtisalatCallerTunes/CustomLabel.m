//
//  InfoLabel_13pt.m
//  6dCRBTApp
//
//  Created by Six Dee Technologies on 12/12/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import "CustomLabel.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"

@implementation CustomLabel_5
@end
@implementation CustomLabel_7
@end
@implementation CustomLabel_8
@end
@implementation CustomLabel_9
@end
@implementation CustomLabel_10
@end
@implementation CustomLabel_11
@end
@implementation CustomLabel_12
@end
@implementation CustomLabel_13
@end
@implementation CustomLabel_14
@end
@implementation CustomLabel_15
@end
@implementation CustomLabel_16
@end
@implementation CustomLabel_17
@end

@implementation SIXDHeading_17
@end
@implementation SIXDHeading_16
@end
@implementation SIXDHeading_10
@end
@implementation SIXDHeading_12
@end
@implementation SIXDHeading_13
@end
@implementation SIXDPopUpHeading_14
@end
@implementation SIXDMenuLabel
@end
/*
@implementation SIXDLikeLabel
@end
*/
@implementation CustomLabel_Lato_10
@end
@implementation CustomLabel_Lato_11
@end
@implementation CustomLabel_Lato_12
@end
@implementation CustomLabel_Lato_14
@end


@implementation SIXDAlertLabel
-(void)awakeFromNib
{
    [super awakeFromNib];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.textColor=UIColorFromRGB(CLR_RED);
    //self.font = [UIFont fontWithName:@"Lato-Medium" size:11];
    self.font = [UIFont fontWithName:@"ArialMT" size:10];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
    else
    {
        self.font = [UIFont fontWithName:@"GESSTextLight-Light" size:10];
    }
     */
    
}
@end

@implementation SIXDTextFieldLabel
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    self.font = [UIFont fontWithName:@"ArialMT" size:10];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
    else
    {
        self.font = [UIFont fontWithName:@"GESSTextLight-Light" size:10];
    }
     */
}

@end

@implementation SIXDLikeLabel
-(void)awakeFromNib
{
    [super awakeFromNib];
    
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    self.font = [UIFont fontWithName:@"ArialMT" size:7];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        //self.font = [UIFont fontWithName:@"NeoTechAlt-Medium" size:7];
        self.font = [UIFont fontWithName:@"ArialMT" size:7];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:7];
    }
     */
}

@end

@implementation SIXDNumericLabel10
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"ArialMT" size:10];
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    /*
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
     */
}

@end

@implementation SIXDNumericLabel11
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"ArialMT" size:11];
    /*
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:11];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:11];
    }
     */
}

@end

@implementation SIXDNumericLabel13
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"ArialMT" size:13];
    
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:13];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:13];
    }
     */
}

@end

@implementation SIXDNumericLabel15
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"ArialMT" size:15];
    
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    if([appDelegate.Language isEqualToString:@"English"])
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:15];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:15];
    }
     */
}

@end

@implementation Pricelabel_Lato_8
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"ArialMT" size:10];
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.textColor=UIColorFromRGB(CLR_TEXTFIELDLABEL);
    if([appDelegate.Language isEqualToString:@"English"])
    {
        //self.font = [UIFont fontWithName:@"Lato-Medium" size:9];
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
    else
    {
        self.font = [UIFont fontWithName:@"ArialMT" size:10];
    }
     */
}

@end

