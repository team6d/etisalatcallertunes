//
//  SIXDPlaylistBuyVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDMyTunesLibraryVC.h"

@interface SIXDPlaylistBuyVC : ViewController<NSURLSessionDelegate>
- (IBAction)onClickedBuyButton:(UIButton *)sender;
- (IBAction)onClickedLikeButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;

@property (weak, nonatomic) IBOutlet UIButton *LikeButton;
@property (weak, nonatomic) IBOutlet UIButton *BuyButton;

@property (weak, nonatomic) IBOutlet UIView *GiftView;
@property (weak, nonatomic) IBOutlet UIView *WishlistView;
@property (weak, nonatomic) IBOutlet UIView *TAFView;
@property (weak, nonatomic) IBOutlet UIView *ShareView;

@property (weak, nonatomic) IBOutlet UILabel *GiftLabel;
@property (weak, nonatomic) IBOutlet UILabel *WishlistLabel;
@property (weak, nonatomic) IBOutlet UILabel *TAFLabel;
@property (weak, nonatomic) IBOutlet UILabel *ShareLabel;
@property (weak, nonatomic) IBOutlet UIView *UpperView;
@property (weak, nonatomic) IBOutlet UIView *LowerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UpperViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *GiftViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *WishListViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TAFViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ShareViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBetweenLikeAndtextlabel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ToneImageheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ToneImageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtwImageAndTextlabel;

@property (weak, nonatomic) IBOutlet UILabel *LikeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wishlistButtonCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SpaceBtwWishlistAndGiftView;

@property(nonatomic) bool IsDeletePage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (strong,nonatomic)UIView *overlayview;

@property(strong,nonatomic) NSMutableDictionary *PlaylistDetails;

@property(strong,nonatomic) SIXDMyTunesLibraryVC *MYTunesLibraryVC;

@end
