//
//  SIXDGlobalViewController.m
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 16/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//






#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import <CommonCrypto/CommonDigest.h>
#import "AppDelegate.h"
#import "CustomLabel.h"
#import "SIXDCustomButton.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDSearchVC.h"
//#import <Google/Analytics.h>
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>



//#import "NSString+MD5.h"

@interface SIXDGlobalViewController ()

@end

@implementation SIXDGlobalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
/*
+ (void)setLoginButtonStyle:(UIButton *)LoginButton
{
    LoginButton.layer.cornerRadius=25;
    LoginButton.clipsToBounds = YES;
    LoginButton.titleLabel.textColor = UIColorFromRGB(0Xffffff);
}

+(void)setButtonGradient:(UIButton *)LoginButton
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = LoginButton.layer.bounds;
    
    gradientLayer.colors = [NSArray arrayWithObjects:
                            (id)[UIColor colorWithRed:247/255.0 green:160/255.0 blue:8/255.0 alpha:1].CGColor,
                            (id)[UIColor colorWithRed:247/255.0 green:110/255.0 blue:36/255.0 alpha:1].CGColor,
                            nil];
    
    
    
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
    gradientLayer.startPoint = CGPointMake(0.0, 0.5);
    gradientLayer.endPoint = CGPointMake(1.0, 0.5);
    
    gradientLayer.cornerRadius = LoginButton.layer.cornerRadius;
    [LoginButton.layer addSublayer:gradientLayer];
}
*/

+(BOOL) isUnAuthorisedApp
{
#if !(TARGET_IPHONE_SIMULATOR)
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/MobileSubstrate.dylib"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/bin/bash"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/usr/sbin/sshd"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/etc/apt"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/private/var/lib/apt/"] ||
        [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.example.package"]])  {
        return YES;
    }
    
    FILE *f = NULL ;
    if ((f = fopen("/bin/bash", "r")) ||
        (f = fopen("/Applications/Cydia.app", "r")) ||
        (f = fopen("/Library/MobileSubstrate/MobileSubstrate.dylib", "r")) ||
        (f = fopen("/usr/sbin/sshd", "r")) ||
        (f = fopen("/etc/apt", "r")))  {
        fclose(f);
        return YES;
    }
    fclose(f);
    
    NSError *error;
    NSString *stringToBeWritten = @"This is a test.";
    [stringToBeWritten writeToFile:@"/private/jailbreak.txt" atomically:YES encoding:NSUTF8StringEncoding error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:@"/private/jailbreak.txt" error:nil];
    if(error == nil)
    {
        return YES;
    }
    
#endif
    
    return NO;
}


+(NSString *)formatStringAsVisa:(NSString*)aNumber
{
    NSMutableString *newStr = [NSMutableString new];
    for (NSUInteger i = 0; i < [aNumber length]; i++)
    {
        if (i > 0 && i % 4 == 0)
            [newStr appendString:@" "];
        unichar c = [aNumber characterAtIndex:i];
        [newStr appendString:[[NSString alloc] initWithCharacters:&c length:1]];
    }
    return newStr;
}
+(NSString *)generateRandomNumber
{
    
    int randomNumber = arc4random();
    if(randomNumber<0)
        randomNumber = randomNumber * -1;
    NSString *randomString = [NSString stringWithFormat:@"%d",randomNumber];
    return randomString;
}

+(void)setAlert:(NSString *)AlertMessage : (UIViewController*)ViewController
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:AlertMessage
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* OKButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    [alert addAction:OKButton];
    [ViewController presentViewController:alert animated:YES completion:nil];
}

+(void)setAlert:(NSString *)AlertMessage : (UIViewController*)ViewController :(int)Action
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:AlertMessage
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* OKButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   switch(Action)
                                   {
                                       case GLOBAL_ALERT_ACTION_SELF_LOAD:
                                           [ViewController viewDidLoad];
                                           break;
                                    
                                       case GLOBAL_ALERT_ACTION_SELF_DISMISS:
                                           [ViewController.navigationController popViewControllerAnimated:NO];
                                           break;
                                       default:
                                           break;
                                           
                                   }
                               }];
    [alert addAction:OKButton];
    [ViewController presentViewController:alert animated:YES completion:nil];
}


+ (NSString *)MD5:(NSString*)inputString {
    
    const char * pointer = [inputString UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [string appendFormat:@"%02x",md5Buffer[i]];
    
    return string;
}


+(void)ChangeScrollPosition : (UIScrollView *)scrollView : (NSLayoutConstraint*)ImageHeight
{
        CGFloat currentOffset = scrollView.contentOffset.y;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
        int MIN_HEIGHT = MIN_IMAGE_HEIGHT;
    
        if(appDelegate.DeviceSize == iPHONE_X)
        {
            MIN_HEIGHT = MIN_HEIGHT+20;
        }
            
        if(ImageHeight.constant >= MIN_HEIGHT)
        {
            
            if (currentOffset > MAX_IMAGE_HEIGHT)
            {
                
                currentOffset = MAX_IMAGE_HEIGHT;
                ImageHeight.constant = MIN_HEIGHT;
                
            }
            
            ImageHeight.constant = ImageHeight.constant - currentOffset;
            
            
            if(ImageHeight.constant > MIN_HEIGHT)
            {
                
                CGPoint TVOffset = scrollView.contentOffset;
                
                TVOffset.y = 0 ;
                scrollView.contentOffset = TVOffset;
                currentOffset = 0;
                
            }
           
        }
    
    
    if(ImageHeight.constant > MAX_IMAGE_HEIGHT)
    {
        ImageHeight.constant = MAX_IMAGE_HEIGHT;
    }
    if(ImageHeight.constant < MIN_HEIGHT)
    {
        ImageHeight.constant = MIN_HEIGHT;
    }
    
    /*
    NSLog(@"HERE 11");
    //to Cancel bounce effect
    CGFloat maxOffset = (scrollView.contentSize.height - scrollView.frame.size.height);
    CGFloat originOffset = 0;
    // Don't scroll below the last cell.
    if (scrollView.contentOffset.y >= maxOffset)
    {
        NSLog(@"HERE 22");
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, maxOffset);
        
        // Don't scroll above the first cell.
    } else if (scrollView.contentOffset.y <= originOffset)
    {
        NSLog(@"HERE 33");
        scrollView.contentOffset = CGPointZero;
    }
   */
    //end
}

+(int)ChangeScrollPositionAndHide : (UIScrollView *)scrollView : (NSLayoutConstraint*)ImageHeight
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    int MIN_HEIGHT = MIN_IMAGE_HEIGHT;
    
    if(appDelegate.DeviceSize == iPHONE_X)
    {
        MIN_HEIGHT = MIN_HEIGHT+20;
    }
    
    if(ImageHeight.constant >= MIN_HEIGHT)
    {
        
        if (currentOffset > MAX_IMAGE_HEIGHT)
        {
            
            currentOffset = MAX_IMAGE_HEIGHT;
            ImageHeight.constant = MIN_HEIGHT;
            
        }
        
        ImageHeight.constant = ImageHeight.constant - currentOffset;
        
        
        if(ImageHeight.constant > MIN_HEIGHT)
        {
            
            CGPoint TVOffset = scrollView.contentOffset;
            
            TVOffset.y = 0 ;
            scrollView.contentOffset = TVOffset;
            currentOffset = 0;
            
        }
        
    }
    
    NSLog(@"ImageHeight.contant = %f MIN_HEIGHT = %d MAX HEIGHT = %d", ImageHeight.constant,MIN_HEIGHT,MAX_IMAGE_HEIGHT);
    
    if(ImageHeight.constant > MAX_IMAGE_HEIGHT)
    {
        ImageHeight.constant = MAX_IMAGE_HEIGHT;
    }
    if(ImageHeight.constant < MIN_HEIGHT)
    {
        ImageHeight.constant = MIN_HEIGHT;
    }
    if(ImageHeight.constant <= MIN_HEIGHT)
        return 1;
    else if (ImageHeight.constant > MIN_HEIGHT)
        return 2;

    return 0;
}

+(void) showAndEnableRightNavigationItem :(ViewController*) ViewController
{
    
    ViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"myaccount"] landscapeImagePhone:[UIImage imageNamed:@"myaccount"] style:UIBarButtonItemStylePlain target:self action:@selector(onSelectedContactButton)];
    [ViewController.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ViewController.navigationItem.rightBarButtonItem setEnabled:YES];
    
}

+(void) showAndEnableLeftNavigationItem :(ViewController*) ViewController
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        ViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton"] landscapeImagePhone:[UIImage imageNamed:@"backbutton"] style:UIBarButtonItemStylePlain target:self action:@selector(onSelectedBackButton)];
    }
    else
    {
         ViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton_Arabic"] landscapeImagePhone:[UIImage imageNamed:@"backbutton_Arabic"] style:UIBarButtonItemStylePlain target:self action:@selector(onSelectedBackButton)];
    }
    [ViewController.navigationItem.leftBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ViewController.navigationItem.leftBarButtonItem setEnabled:YES];

}

+(void) showAndEnableLeftNavigationItem_MainNavigationPage :(ViewController*) ViewController
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"English"])
    {
    ViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton"] landscapeImagePhone:[UIImage imageNamed:@"backbutton"] style:UIBarButtonItemStylePlain target:self action:@selector(onSelectedBackButton_MainNavigationPage)];
    }
    else
    {
         ViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backbutton_Arabic"] landscapeImagePhone:[UIImage imageNamed:@"backbutton_Arabic"] style:UIBarButtonItemStylePlain target:self action:@selector(onSelectedBackButton_MainNavigationPage)];
    }
    [ViewController.navigationItem.leftBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ViewController.navigationItem.leftBarButtonItem setEnabled:YES];
    
}


+(void)onSelectedBackButton_MainNavigationPage
{
    NSLog(@"onSelectedBackButton>>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //added logic to refresh search results when back button is pressed from search Page
    if(appDelegate.MainTabController.selectedIndex == 1)
    {
        NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
        UINavigationController *Temp = [VCArray objectAtIndex:1];
        SIXDSearchVC *VC = [[Temp viewControllers] objectAtIndex:0];
        VC.IsRefreshRequired = true;
    }
    //end
    appDelegate.MainTabController.selectedIndex = 0;
   
}

+(void)onSelectedBackButton
{
    NSLog(@"onSelectedBackButton>>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    UINavigationController *Temp = [VCArray objectAtIndex:appDelegate.MainTabController.selectedIndex];
    [Temp popViewControllerAnimated:NO];
}

+(void)setNavigationBarOpaque:(ViewController*) ViewController
{

    [ViewController.navigationController.navigationItem.leftBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ViewController.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    //added
    [ViewController.navigationController.navigationBar setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    //end
    
    [ViewController.navigationController.navigationItem.leftBarButtonItem setEnabled:YES];
    [ViewController.navigationController.navigationBar setBarTintColor: [UIColor whiteColor]];
    ViewController.navigationController.navigationBar.translucent = NO;
    ViewController.navigationController.navigationBar.opaque = YES;
    
}

+(void)setNavigationBarTransparent:(ViewController*) ViewController
{
    [ViewController.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    [ViewController.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    
    //added
    [ViewController.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //end
    
    [ViewController.navigationController.navigationItem.leftBarButtonItem setEnabled:YES];
    
    [ViewController.navigationController.navigationBar setBarTintColor: [UIColor clearColor]];
    ViewController.navigationController.navigationBar.translucent = YES;
    ViewController.navigationController.navigationBar.opaque = NO;
    
}

+(void)setNavigationBarTransparentWithImage:(ViewController*) ViewController :(NSString*)ImageName
{
        
    [ViewController.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    [ViewController.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    [ViewController.navigationController.navigationItem.leftBarButtonItem setEnabled:YES];
    
    //added
    [ViewController.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    //end
    
    [ViewController.navigationController.navigationBar setBarTintColor: [UIColor clearColor]];
    ViewController.navigationController.navigationBar.translucent = YES;
    ViewController.navigationController.navigationBar.opaque = NO;
    
    //Setting imgae from URL to navigation Bar
     /*NSString *ImageURL = [ImageName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]]];
    
    if(!aImage)
    {
        aImage = [UIImage imageNamed:@"Default"];
    }
    [[UINavigationBar appearance] setBackgroundImage:myImage forBarMetrics:UIBarMetricsDefault];
    */
    
    [ViewController.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:ImageName] forBarMetrics:UIBarMetricsDefault];
    
    
}


+ (NSString *)replaceFirstOccurenceOfAString : (NSString*) originalString : (NSString *)occurence : (NSString*)replacementString
{
    //Number = [Number stringByReplacingOccurrencesOfString:@"0" withString:@"+971"];
    //return Number;
    
    NSRange rOriginal = [originalString rangeOfString:occurence];
    if (NSNotFound != rOriginal.location) {
        return [originalString stringByReplacingCharactersInRange:rOriginal withString:replacementString];
    }
    return originalString;
}

+(void)setNavigationBarTiTle:(ViewController*) ViewController :(NSString*) Title : (int) TitleColor
{
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [ViewController.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:UIColorFromRGB(TitleColor),
       NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
    ViewController.navigationItem.title= NSLocalizedStringFromTableInBundle(Title,nil,appDelegate.LocalBundel,nil);
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [ViewController.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(TitleColor),
         NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
         ViewController.navigationItem.title= NSLocalizedStringFromTableInBundle(Title,nil,appDelegate.LocalBundel,nil);
    }
    else
    {
        [ViewController.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(TitleColor),
           NSFontAttributeName:[UIFont fontWithName:@"GESSTextMedium-Medium" size:17]}];
        ViewController.navigationItem.title= NSLocalizedStringFromTableInBundle(Title,nil,appDelegate.LocalBundel,nil);
    }
     */
    
}

+(void)onSelectedContactButton
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    UINavigationController *Temp = [VCArray objectAtIndex:appDelegate.MainTabController.selectedIndex];
    
    //added logic to refresh search results only on tab change
    if(appDelegate.MainTabController.selectedIndex == 1)
    {
        NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
        UINavigationController *Temp = [VCArray objectAtIndex:1];
        SIXDSearchVC *VC = [[Temp viewControllers] objectAtIndex:0];
        VC.IsRefreshRequired = true;
    }
    //end
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    
    UIViewController  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMyAccountMenuVC"];
    [Temp pushViewController:ViewController animated:NO];
    
}

+(NSMutableArray*)convertToToneDetailsDefault: (NSMutableArray*)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *Default = [NSMutableArray new];
    for (int i =0; i<Response.count; i++)
    {
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        NSMutableDictionary *Temp = [Response objectAtIndex:i];
        NSString *Value;

        Value = [Temp objectForKey:@"contentName"];
        if(Value.length>0)
            [Dict setValue:Value forKey:@"albumName"];
       else
       {
           Value = [Temp objectForKey:@"toneName"];
           if(Value.length>0)
               [Dict setValue:Value forKey:@"albumName"];
       }
        
         Value = [Temp objectForKey:@"artist"];
         if(Value.length>0)
             [Dict setValue:Value forKey:@"artistName"];
        
        Value = [Temp objectForKey:@"contentId"];
        
        if(Value.length>0)
        {
            NSArray *NewValues = [NSArray new];
            NewValues = [Value componentsSeparatedByString:@"|"];
            if(NewValues.count >= 1)
            {
                
                [Dict setValue:[NewValues objectAtIndex:0] forKey:@"toneId"];
                [Dict setValue:[NewValues objectAtIndex:0] forKey:@"toneCode"];
            }
            
            if(NewValues.count >= 2)
            {
               
                [Dict setValue:[NewValues objectAtIndex:1] forKey:@"categoryId"];
            }
           
        }
        else
        {
            Value = [Temp objectForKey:@"toneId"];
            if(Value.length>0)
            [Dict setValue:Value forKey:@"toneId"];
        }
       
         Value = [Temp objectForKey:@"path"];
        if(Value.length <= 0)
        {
             Value = [Temp objectForKey:@"tonePath"];
        }
         Value = [NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_TONE_URL"],Value];
         if(Value.length>0)
             [Dict setValue:Value forKey:@"toneUrl"];
       
        
        Value = [Temp objectForKey:@"previewImageUrl"];
        if(Value.length <= 0)
        {
            Value = [Temp objectForKey:@"imagePath"];
        }
        Value = [NSString stringWithFormat:@"%@%@",[appDelegate.AppSettings objectForKey:@"WISHLIST_IMAGE_URL"],Value];
        if(Value.length>0)
        [Dict setValue:Value forKey:@"previewImageUrl"];
       
        
        Value = [Temp objectForKey:@"contentName"];
        if(Value.length>0)
        {
            [Dict setValue:Value forKey:@"toneName"];
            [Dict setValue:Value forKey:@"description"];
            [Dict setValue:Value forKey:@"channelName"];
        }
        
        
        Value = [Temp objectForKey:@"id"];
        if(Value.length>0)
            [Dict setValue:Value forKey:@"id"];
        
        Value = [Temp objectForKey:@"likeCount"];
        if(Value.length>0)
            [Dict setValue:Value forKey:@"likeCount"];
        else
            [Dict setValue:@"0" forKey:@"likeCount"];
        
        [Default addObject:Dict];
        
    }
    
    return Default;
}

+(UIView*)showDefaultAlertMessage :(NSString*)Message : (float)yPosition
{
    UIView *MainView = [[UIView alloc] initWithFrame:CGRectMake(25,yPosition, [UIScreen mainScreen].bounds.size.width-50,60)];
    UIButton *Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 5, [UIScreen mainScreen].bounds.size.width-50,30)];
    Button.userInteractionEnabled = false;
    
    UIImage* image = [[UIImage imageNamed:@"Fill 3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [Button setImage:image forState:UIControlStateNormal];
    [Button.imageView setTintColor:[UIColor whiteColor]];
    
    CustomLabel_10 *Alertlabel = [[CustomLabel_10 alloc] initWithFrame:CGRectMake(0, 25, [UIScreen mainScreen].bounds.size.width-50,30)];
    Alertlabel.text = Message;
    Alertlabel.textAlignment = NSTextAlignmentCenter;
    Alertlabel.textColor = [UIColor whiteColor];
    Alertlabel.tag = 1;
    
    [MainView setBackgroundColor:[UIColor orangeColor]];
    
    [MainView addSubview:Button];
    [MainView addSubview:Alertlabel];
    return MainView;
    
}

+(UIView*)showDefaultAlertWithAction :(NSString*)Message :(BOOL)Error :(CGRect)BackgrondViewFrame : (ViewController*)ParentView
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *SuperView;
    
    if(BackgrondViewFrame.size.width != 0)
    {
        SuperView = [[UIView alloc] initWithFrame:BackgrondViewFrame];
    }
    else
    {
       SuperView = [[UIView alloc] initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width-50,200)];
    }
    
    UIView *MainView = [[UIView alloc] initWithFrame:CGRectMake(25,50, [UIScreen mainScreen].bounds.size.width-50,70)];
    UIButton *Button = [[UIButton alloc] initWithFrame:CGRectMake(0,5, [UIScreen mainScreen].bounds.size.width-50,30)];
    Button.userInteractionEnabled = false;
    
    CustomLabel_10 *Alertlabel = [[CustomLabel_10 alloc] initWithFrame:CGRectMake(0, 27, [UIScreen mainScreen].bounds.size.width-50,40)];
    
    /*
    if(!Error)
    {
    Message = [Message stringByReplacingOccurrencesOfString:@"." withString:@".\n"];
    }
    */
    
    Alertlabel.text = Message;
    Alertlabel.textAlignment = NSTextAlignmentCenter;
    Alertlabel.numberOfLines = 0;
    Alertlabel.lineBreakMode = NSLineBreakByWordWrapping;
    Alertlabel.textColor = [UIColor whiteColor];
    Alertlabel.tag = 1;
    
    if(Error)
    {
    
    UIImage* image = [[UIImage imageNamed:@"Fill 3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [Button setImage:image forState:UIControlStateNormal];
        [MainView setBackgroundColor:[UIColor orangeColor]];
    }
    else
    {
        UIImage* image = [[UIImage imageNamed:@"salatiloadng"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Button setImage:image forState:UIControlStateNormal];
        [MainView setBackgroundColor:UIColorFromRGB(CLR_PLACEHOLDER)];
    }
        
        
    [Button.imageView setTintColor:[UIColor whiteColor]];

    [MainView addSubview:Button];
    [MainView addSubview:Alertlabel];
    [SuperView addSubview:MainView];
    
    
    SIXDCustomButton *ActionButton = [[SIXDCustomButton alloc] initWithFrame:CGRectMake(25,140, [UIScreen mainScreen].bounds.size.width-50,47)];
    
    ActionButton.tag = 2;
    [ActionButton.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [ActionButton.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    }
    else
    {
        [ActionButton.titleLabel setFont:[UIFont fontWithName:@"GESSTextLight-Light" size:14]];
    }
     */
    [ActionButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [ActionButton setBackgroundColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ActionButton.titleLabel setTextColor:UIColorFromRGB(CLR_WHITE)];
    
    [ActionButton addTarget:ParentView action:@selector(onSelectedPopUpConfirmButton) forControlEvents:UIControlEventTouchUpInside];
    [ActionButton setUserInteractionEnabled:true];
    
    [SuperView addSubview:ActionButton];
    
    return SuperView;
    
}

+(UIView*)showDefaultAlertWithAction_Tick :(NSString*)Message :(BOOL)Error :(CGRect)BackgrondViewFrame : (ViewController*)ParentView
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *SuperView;
    
    if(BackgrondViewFrame.size.width != 0)
    {
        SuperView = [[UIView alloc] initWithFrame:BackgrondViewFrame];
    }
    else
    {
        SuperView = [[UIView alloc] initWithFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width-50,200)];
    }
    
    UIView *MainView = [[UIView alloc] initWithFrame:CGRectMake(25,50, [UIScreen mainScreen].bounds.size.width-50,70)];
    UIButton *Button = [[UIButton alloc] initWithFrame:CGRectMake(0,5, [UIScreen mainScreen].bounds.size.width-50,30)];
    Button.userInteractionEnabled = false;
    
    CustomLabel_10 *Alertlabel = [[CustomLabel_10 alloc] initWithFrame:CGRectMake(0, 27, [UIScreen mainScreen].bounds.size.width-50,40)];
    
    /*
    if(!Error)
    {
        Message = [Message stringByReplacingOccurrencesOfString:@"." withString:@".\n"];
    }
    */
    
    Alertlabel.text = Message;
    Alertlabel.textAlignment = NSTextAlignmentCenter;
    Alertlabel.numberOfLines = 0;
    Alertlabel.lineBreakMode = NSLineBreakByWordWrapping;
    Alertlabel.textColor = [UIColor whiteColor];
    Alertlabel.tag = 1;
    
    if(Error)
    {
        
        UIImage* image = [[UIImage imageNamed:@"Fill 3"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Button setImage:image forState:UIControlStateNormal];
        [MainView setBackgroundColor:[UIColor orangeColor]];
    }
    else
    {
        UIImage* image = [[UIImage imageNamed:@"Tickie"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [Button setImage:image forState:UIControlStateNormal];
        [MainView setBackgroundColor:UIColorFromRGB(CLR_PLACEHOLDER)];
    }
    
    
    [Button.imageView setTintColor:[UIColor whiteColor]];
    
    [MainView addSubview:Button];
    [MainView addSubview:Alertlabel];
    [SuperView addSubview:MainView];
    
    
    SIXDCustomButton *ActionButton = [[SIXDCustomButton alloc] initWithFrame:CGRectMake(25,140, [UIScreen mainScreen].bounds.size.width-50,47)];
    
    ActionButton.tag = 2;
    [ActionButton.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [ActionButton.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    }
    else
    {
        [ActionButton.titleLabel setFont:[UIFont fontWithName:@"GESSTextLight-Light" size:14]];
    }
     */
    [ActionButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [ActionButton setBackgroundColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [ActionButton.titleLabel setTextColor:UIColorFromRGB(CLR_WHITE)];
    
    [ActionButton addTarget:ParentView action:@selector(onSelectedPopUpConfirmButton) forControlEvents:UIControlEventTouchUpInside];
    [ActionButton setUserInteractionEnabled:true];
    
    [SuperView addSubview:ActionButton];
    
    return SuperView;
    
}

+(NSMutableArray *)convertListToneResponse: (NSMutableArray *)Response
{
    NSMutableArray *WantedArray = [NSMutableArray new];
    for (int i =0; i<Response.count; i++)
    {
        NSMutableDictionary *DummyDict = [Response objectAtIndex:i];
        NSMutableArray *DummyArray = [DummyDict objectForKey:@"toneDetails"];
        NSMutableDictionary *toneDetails = [[DummyArray objectAtIndex:0]mutableCopy];
        
        if([toneDetails objectForKey:@"toneId"])
        [toneDetails setValue:[toneDetails objectForKey:@"toneId"] forKey:@"toneCode"];
        
        if([toneDetails objectForKey:@"toneName"])
        {
            [toneDetails setValue:[toneDetails objectForKey:@"toneName"] forKey:@"description"];
            [toneDetails setValue:[toneDetails objectForKey:@"toneName"] forKey:@"channelName"];
        }
        
        
        [WantedArray addObject:toneDetails];
    }
    return  WantedArray;
}

+(NSAttributedString*) getAttributedText :(NSString*)message{
    
     NSMutableAttributedString *attributedMessage =[[NSMutableAttributedString alloc] initWithString:message];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:3];
    [style setAlignment:NSTextAlignmentCenter];
    [attributedMessage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,message.length)];
    return attributedMessage;
}

+(void)showCommonFailureResponse :(ViewController*)ParentView : (NSString*) Message
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    if(Message.length <= 0)
    {
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    }
    else
    {
        NSLog(@"1111");
        ViewController.AlertMessage = Message;
    }
    ViewController.title = @"";
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    [ParentView.tabBarController presentViewController:ViewController animated:YES completion:nil];
    
}

+(NSString*)HTMLConversion:(NSString *)str
{
    
    NSAttributedString *Attr_Str=[[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]
                                                                  options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                            NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                       documentAttributes:nil error:nil];
    
    
    return Attr_Str.string;
    
}

+(NSString *)CapitalisationConversion:(NSString *)str
{
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    str = [str capitalizedString];
   /* if([appDelegate.Language isEqualToString:@"English"])
    {
        str = [str capitalizedString];
    }
    else
    {
        
    }
    */
    return str;
}

+(BOOL) Custom_shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string : (UITextField*) textField
{
    switch (range.location)
    {
        case 0:
            if(string.length == 0 && ([textField.text characterAtIndex:0] == '+'))
                return NO;
            break;
        
        case 1:
            if(string.length == 0 && ([textField.text characterAtIndex:0] == '9'))
                return NO;
            break;
            
        case 2:
            if(string.length == 0 && ([textField.text characterAtIndex:0] == '7'))
                return NO;
            break;
            
        case 3:
            if(string.length == 0 && ([textField.text characterAtIndex:0] == '1'))
                return NO;
            break;
            
        case 4:
            if(string.length == 0 && ([textField.text characterAtIndex:0] == '-'))
                return NO;
            break;
            
        default:
            break;
    }
    
    
    return YES;
}

+(BOOL)isMSISDNSame : (NSString *) AParty : (NSString *) BParty
{
     AParty = [AParty stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [AParty length])];
     BParty = [BParty stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [BParty length])];
    
    AParty = [AParty stringByReplacingOccurrencesOfString:@"+9715" withString:@"5"];
    BParty = [BParty stringByReplacingOccurrencesOfString:@"+9715" withString:@"5"];
    
    if([AParty rangeOfString:@"0"].location == 0)
    {
       AParty = [AParty substringFromIndex:1];
    }
    else if ([BParty rangeOfString:@"0"].location == 0)
    {
        BParty = [BParty substringFromIndex:1];
    }
    
    if([AParty rangeOfString:BParty].location != NSNotFound )
    {
        return false;
    }
    else if ([BParty rangeOfString:AParty].location != NSNotFound )
    {
        return false;
    }
    else
    {
        return true;
    }
}
+(NSString*)URLDecode:(NSString *)str
{
    NSString *result = [str stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByRemovingPercentEncoding];
    return result;
}
@end
