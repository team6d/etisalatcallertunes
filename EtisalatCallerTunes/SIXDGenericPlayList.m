//
//  SIXDGenericPlayList.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/11/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDGenericPlayList.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDBuyToneVC.h"
#import "SIXDPlayerVC.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"


@interface SIXDGenericPlayList ()

@end

@implementation SIXDGenericPlayList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _SearchTagDictionary = [NSMutableDictionary new];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *DummyArray = [appDelegate.AppSettings objectForKey:@"FILTER_PARAMS"];
    if(DummyArray.count > 0)
    {
        [_SearchTagDictionary setObject:[DummyArray objectAtIndex:0] forKey:@"MOST_RECENT"];
        [_SearchTagDictionary setObject:[DummyArray objectAtIndex:1] forKey:@"BEST_SELLERS"];
        [_SearchTagDictionary setObject:[DummyArray objectAtIndex:2] forKey:@"MOST_LIKED"];
    }
    
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    //[_BannerImage sd_setImageWithURL:appDelegate.CategoryImagePath placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
        if(_searchKey.length > 0)
        {
            _HeadingLabel.text = [_searchKey uppercaseString];
            _TopHeadingLabel.text=  NSLocalizedStringFromTableInBundle(@"ARTISTS",nil,appDelegate.LocalBundel,nil);
            _BannerImage.image = [UIImage imageNamed:@"Artist"];
            //_TopHeadingLabel.text = [_searchKey uppercaseString];
        }
        else
        {
            _TopHeadingLabel.text = [SIXDGlobalViewController HTMLConversion:[appDelegate.CategoryName uppercaseString]];
            _HeadingLabel.text =[SIXDGlobalViewController HTMLConversion:[_SubCategoryName uppercaseString]];
            
            [_BannerImage sd_setImageWithURL:appDelegate.CategoryImagePath placeholderImage:[UIImage imageNamed:@"Default.png"]];
        }
    
    _PerPageCount = [[appDelegate.AppSettings objectForKey:@"FETCH_LIMIT"]intValue];
    
    if(!_IsSegmentTabRequired)
    {
        [_SegmentTab setHidden:true];
        _SegmentTabHeight.constant = 42;
    }
    else
    {
    
        _PageNumber_TuneList_1 =  1;
        _PageNumber_TuneList_2 = 1;
        _PageNumber_TuneList_3 = 1;
        
        _TempResponseTuneList_1Count = 0;
        _TempResponseTuneList_2Count = 0;
        _TempResponseTuneList_3Count = 0;
        
        _Selected_PageNumber =  1;
        _Selected_TempResponseCount = 0;

        
        [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"MOST RECENT",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
        [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"BEST SELLERS",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
        [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"MOST LIKED",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:2];
        
        NSArray *array = [_SegmentTab subviews];
        UIView *Temp;
        
        //adding lines for selected tab
        Temp = [array objectAtIndex:0];
        UILabel *Line;
        UILabel *Line1;
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+83, 18, 1, 6)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+167, 18, 1, 6)];
        /*
        if(appDelegate.DeviceSize == iPHONE_X || appDelegate.DeviceSize == SMALL)
        {
            Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+83, 18, 1, 6)];
            Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+167, 18, 1, 6)];
        }
        else
        {
            Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 18, 1, 6)];
           Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 18, 1, 6)];
        }
         */
        
       // UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 18, 1, 6)];
        Line.backgroundColor = [UIColor grayColor];
        [_SegmentTab insertSubview:Line aboveSubview:Temp];
        
        Temp = [array objectAtIndex:1];
       // UILabel *Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 18, 1, 6)];
        Line1.backgroundColor = [UIColor grayColor];
        [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
        
        
        [_ActivityIndicator startAnimating];
        [self.view addSubview:_overlayview];
        [self.view bringSubviewToFront:_ActivityIndicator];
    
        [self sortSubCategory:0];
        
    }
    
    
    if(_IsSourceBanner)
    {
        _HeadingLabel.hidden = TRUE;
        _TopHeadingLabel.hidden = TRUE;
        _SegmentTabHeight.constant = 20;
    }
    else
    {
        _HeadingLabel.hidden = FALSE;
        _TopHeadingLabel.hidden = FALSE;
    }
    
    //adding tint
    UIView *BackgroundGradient = [[UIView alloc]initWithFrame:CGRectMake(_BannerImage.frame.origin.x, _BannerImage.frame.origin.y, [UIScreen mainScreen].bounds.size.width, _BannerImage.frame.size.height)];
    [BackgroundGradient setBackgroundColor:UIColorFromRGB(CLR_BANNER_IMAGE_TINT)];
    BackgroundGradient.alpha = 0.3;
    [_BannerImage addSubview:BackgroundGradient];
    [self.view bringSubviewToFront:_HeadingLabel];
    //end
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     //return _Selected_TuneList.count;
    if(_Selected_TempResponseCount >= _PerPageCount)
        return _Selected_TuneList.count+1;
    else
        return _Selected_TuneList.count;
 
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    if(_Selected_TuneList.count > indexPath.row)
    {
    
    NSMutableDictionary *Dict = [_Selected_TuneList objectAtIndex:indexPath.row];
   
        /*
    if([Dict objectForKey:@"likeCount"])
        cell.LikeLabel.text = [Dict objectForKey:@"likeCount"];
    else
        cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
      */
    
    //***set like count *********
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([Dict objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
    }
    else
        cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count enda *********
    
    NSString *String1 = [SIXDGlobalViewController HTMLConversion:[Dict objectForKey:@"artistName"]];
    cell.TitleLabel.text = [String1 capitalizedString];
    NSString *String2 = [SIXDGlobalViewController HTMLConversion:[Dict objectForKey:@"toneName"]];
    cell.SubTitleLabel.text = [String2 capitalizedString];
    
    
    
    
    NSString *imageurl = [[Dict objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
    NSURL *ImageURL = [NSURL URLWithString:imageurl];
        
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];

    UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    image = [[UIImage imageNamed:@"topchartsbuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.BuyButton setImage:image forState:UIControlStateNormal];
    [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
        //LIKE SETTINGS
        NSLog(@"CustomTV::Calling likeSettingsSession >>>");
        [SIXDStylist loadLikeUnlikeSetings:cell :[Dict objectForKey:@"toneId"]];
        
     
        //LIKE SETTINGS
        int x_Position;
        if([appDelegate.Language isEqualToString:@"English"])
        {
            x_Position=0;
        }
        else
        {
            x_Position=50-1;
        }
        
    UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    
    [cell.LikeButton addSubview:leftBorder1];
    
    UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
    
    [cell.BuyButton addSubview:leftBorder2];
    
    UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    [cell.AccessoryButton addSubview:leftBorder3];
    
    
    
    cell.BuyButton.tag = indexPath.row;
    cell.LikeButton.tag = indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
        [cell.SubView setHidden:false];
        [cell.ActivityIndicator stopAnimating];
    }
    else
    {
        [cell.SubView setHidden:true];
        [cell.ActivityIndicator startAnimating];
    }
    
    return cell;
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

-(void)sortSubCategory: (int) Index
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *Identifier = @"";
    switch (Index) {
        case 0:
            Identifier = [_SearchTagDictionary objectForKey:@"MOST_RECENT"];
            break;
            
        case 1:
            Identifier = [_SearchTagDictionary objectForKey:@"BEST_SELLERS"];
            break;
            
        case 2:
            Identifier = [_SearchTagDictionary objectForKey:@"MOST_LIKED"];
            break;
            
        default:
            break;
            
    }
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];

    if(_searchKey.length <= 0)
    {
        
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%d&pageNo=%lu&perPageCount=%lu&sortBy=%@",URL,appDelegate.Language,appDelegate.CategoryId,(unsigned long)_Selected_PageNumber,(unsigned long)_PerPageCount,Identifier];
         [ReqHandler sendHttpGETRequest:self :nil];
    }
    else
    {
        appDelegate.APIFlag = CDP_SEARCH_ARTIST;
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&pageNo=%lu&perPageCount=%lu&sortBy=%@&categoryId=0",URL,appDelegate.Language,(unsigned long)_Selected_PageNumber,(unsigned long)_PerPageCount,Identifier];
        [ReqHandler sendHttpGETRequest:self :_searchKey];
        
    }
    
    
   
}


-(void)ReloadContents : (NSInteger) Row
{
    NSLog(@"ReloadContents >>>");
    //COPY CONTENTS TO categoryList of genericTableView class
    
    switch (Row)
    {
        case 0:
            _Selected_TuneList = [_TuneList_1 mutableCopy];
            break;
            
        case 1:
            _Selected_TuneList = [_TuneList_2 mutableCopy];
            break;
            
        case 2:
            _Selected_TuneList = [_TuneList_3 mutableCopy] ;
            break;
            
        default:
            break;
    }
    
    [_TableView reloadData];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    
    [_SegmentTab setUserInteractionEnabled:true];
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    
    NSLog(@"Response top charts = %@",Response);
    
    Response = [Response objectForKey:@"responseMap"];
    
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            [self updateDataInTableView : 0 :Response];
            break;
            
        case 1:
            
            [self updateDataInTableView : 1 :Response];
            
            break;
            
            
        case 2:
            
           [self updateDataInTableView : 2 :Response];

            break;
            
       
        default:
            break;
    }
    
    

}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)updateDataInTableView :(int) SelectedIndex :(NSMutableDictionary *)Response
{
    NSLog(@"Response =%@",Response);
    
    switch (SelectedIndex)
    {
        case 0:
        {
            if(_TuneList_1.count <= 0)
            {
            
                _TuneList_1= [[Response objectForKey:@"searchList"]mutableCopy];
            
                _TempResponseTuneList_1Count = _TuneList_1.count;
                _PageNumber_TuneList_1 += _TempResponseTuneList_1Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_1;
                _Selected_TempResponseCount = _TempResponseTuneList_1Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                [self ReloadContents:SelectedIndex];
            }
            else
            {
                NSMutableArray *TempPlaylist = [[Response objectForKey:@"searchList"] mutableCopy];
                
                _TempResponseTuneList_1Count = TempPlaylist.count;
                _PageNumber_TuneList_1 += _TempResponseTuneList_1Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_1;
                _Selected_TempResponseCount = _TempResponseTuneList_1Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                
                [_TableView beginUpdates];
                if(TempPlaylist.count < _PerPageCount)
                {
                    NSLog(@"Deleceted Row");
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_1.count) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                
                for(int i=0;i<TempPlaylist.count;i++)
                {
                    //add new data to both seleted tunelist and the tunelist
                    [_TuneList_1 addObject:[TempPlaylist objectAtIndex:i]];
                    [_Selected_TuneList addObject:[TempPlaylist objectAtIndex:i]];
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_1.count-1) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                [_TableView endUpdates];
            }
        }
            break;
            
        case 1:
        {
            if(_TuneList_2.count <= 0)
            {
                
                _TuneList_2=[[Response objectForKey:@"searchList"]mutableCopy];
                
                _TempResponseTuneList_2Count = _TuneList_2.count;
                _PageNumber_TuneList_2 += _TempResponseTuneList_2Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_2;
                _Selected_TempResponseCount = _TempResponseTuneList_2Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                [self ReloadContents:SelectedIndex];
            }
            else
            {
                NSMutableArray *TempPlaylist = [[Response objectForKey:@"searchList"] mutableCopy];
                
                _TempResponseTuneList_2Count = TempPlaylist.count;
                _PageNumber_TuneList_2 += _TempResponseTuneList_2Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_2;
                _Selected_TempResponseCount = _TempResponseTuneList_2Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                
                [_TableView beginUpdates];
                if(TempPlaylist.count < _PerPageCount)
                {
                    NSLog(@"Deleceted Row");
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_2.count) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                
                for(int i=0;i<TempPlaylist.count;i++)
                {
                    //add new data to both seleted tunelist and the tunelist
                    [_TuneList_2 addObject:[TempPlaylist objectAtIndex:i]];
                    [_Selected_TuneList addObject:[TempPlaylist objectAtIndex:i]];
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_2.count-1) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                [_TableView endUpdates];
            }
        }
            break;
            
            
        case 2:
        {
            if(_TuneList_3.count <= 0)
            {
                
                _TuneList_3=[[Response objectForKey:@"searchList"]mutableCopy];
                
                _TempResponseTuneList_3Count = _TuneList_3.count;
                _PageNumber_TuneList_3 += _TempResponseTuneList_3Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_3;
                _Selected_TempResponseCount = _TempResponseTuneList_3Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                [self ReloadContents:SelectedIndex];
            }
            else
            {
                NSMutableArray *TempPlaylist = [[Response objectForKey:@"searchList"] mutableCopy];
                
                _TempResponseTuneList_3Count = TempPlaylist.count;
                _PageNumber_TuneList_3 += _TempResponseTuneList_3Count;
                
                _Selected_PageNumber = _PageNumber_TuneList_3;
                _Selected_TempResponseCount = _TempResponseTuneList_3Count;
                
                _IsLoading = false;
                [_TableView setUserInteractionEnabled:true];
                
                [_TableView beginUpdates];
                if(TempPlaylist.count < _PerPageCount)
                {
                    NSLog(@"Deleceted Row");
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_3.count) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                
                for(int i=0;i<TempPlaylist.count;i++)
                {
                    //add new data to both seleted tunelist and the tunelist
                    [_TuneList_3 addObject:[TempPlaylist objectAtIndex:i]];
                    [_Selected_TuneList addObject:[TempPlaylist objectAtIndex:i]];
                    
                    NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_TuneList_3.count-1) inSection:0];
                    NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
                    [_TableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                }
                [_TableView endUpdates];
            }
        }
            break;
            
            
        default:
            break;
    }
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == _Selected_TuneList.count)
    {
        if(!_IsLoading)
        {
            if(_Selected_TempResponseCount >= _PerPageCount)
            {
                [_TableView setUserInteractionEnabled:false];
                _IsLoading = true;
                [self sortSubCategory:(int)_SegmentTab.selectedSegmentIndex];
                
            }
            
        }
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    [_SegmentTab setUserInteractionEnabled:true];
    NSLog(@"SIXDGenericPlayList::onFailureResponseRecived >>> Response %@", Response);
    if(_IsLoading == false)
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    }
    else
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_Selected_TuneList.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        _Selected_TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
    
    }
    
    NSLog(@"Unable to load Contents");
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    [_SegmentTab setUserInteractionEnabled:true];
    if(_IsLoading == false)
    {
        [SIXDGlobalViewController showCommonFailureResponse:self :nil];
    }
    else
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_Selected_TuneList.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
         _Selected_TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
        
    }
    
    NSLog(@"onFailedToConnectToServer>>>");
    
}
- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender
{
    switch ([sender selectedSegmentIndex])
    {
            
        case 0:
            
            _Selected_PageNumber = _PageNumber_TuneList_1;
            _Selected_TempResponseCount = _TempResponseTuneList_1Count;
            
            if(_TuneList_1.count <= 0)
            {
                [_ActivityIndicator startAnimating];
                [self.view addSubview:_overlayview];
                [self.view bringSubviewToFront:_ActivityIndicator];
                [self sortSubCategory :0];
            }
            else
            {
                [self ReloadContents :0];
            }
            
            break;
            
        case 1:
            
            _Selected_PageNumber = _PageNumber_TuneList_2;
            _Selected_TempResponseCount = _TempResponseTuneList_2Count;
            if(_TuneList_2.count <= 0)
            {
                [_ActivityIndicator startAnimating];
                [self.view addSubview:_overlayview];
                [self.view bringSubviewToFront:_ActivityIndicator];
                [self sortSubCategory :1];
            }
            else
            {
                [self ReloadContents :1];
            }
            break;
            
        case 2:
            
            _Selected_PageNumber = _PageNumber_TuneList_3;
            _Selected_TempResponseCount = _TempResponseTuneList_3Count;
            
            if(_TuneList_3.count <= 0)
            {
                [_ActivityIndicator startAnimating];
                [self.view addSubview:_overlayview];
                [self.view bringSubviewToFront:_ActivityIndicator];
                [self sortSubCategory :2];
            }
            else
            {
                [self ReloadContents :2];
            }
            break;
            
       
            
        default:
            break;
    }
}

- (IBAction)onSelectedBuyButton:(UIButton*)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails= [_Selected_TuneList objectAtIndex:sender.tag];
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)onSelectedAccessoryButton:(UIButton*)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_Selected_TuneList;
    ViewController.CurrentToneIndex = sender.tag;
    [self.navigationController pushViewController:ViewController animated:YES];
}

- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    
    NSLog(@"GenericPlayList::Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    NSMutableDictionary *TuneDetails = [_Selected_TuneList objectAtIndex:sender.tag];
    
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
    /*
    NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
     */
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    //int likeCount = [cell.LikeLabel.text intValue];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
            //likeCount++;
            
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            //likeCount--;
            
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
    //cell.LikeLabel.text = [NSString stringWithFormat:@"%d",likeCount];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_Selected_TuneList;
    ViewController.CurrentToneIndex = indexPath.row;
    [self.navigationController pushViewController:ViewController animated:YES];
}

@end
