//
//  SIXDSubCategories.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSubCategories.h"
#import "SIXDNestedTVCell.h"
#import "SIXDGlobalViewController.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "UIImageView+WebCache.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"

@interface SIXDSubCategories ()

@end

@implementation SIXDSubCategories

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *Category_GA_TrackingName = @"Categories";
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _ScrollView.delegate = self;

    _SuccessIdsDict = [NSMutableDictionary new];
    _SubCategoryStartIndex = 2; //to exclude Latest and Trending in GenericTableView's List
    
    [_CategoryImage sd_setImageWithURL:appDelegate.CategoryImagePath placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        NSString *string = [SIXDGlobalViewController HTMLConversion:appDelegate.CategoryName];
        _CategoryNameLabel.text =  string;
        Category_GA_TrackingName = [NSString stringWithFormat:@"%@_%@",Category_GA_TrackingName,string];
    }
    else
    {
        _CategoryNameLabel.text = [appDelegate.CategoryName uppercaseString];
        Category_GA_TrackingName = [NSString stringWithFormat:@"%@_%@",Category_GA_TrackingName,_CategoryNameLabel.text];
    }
    
    [SIXDStylist setGAScreenTracker:Category_GA_TrackingName];
    
    NSLog(@"_CategoryDetails = %@",_CategoryDetails);
    
    _ResponseCount = 0;
    
    _SubCategoryDetails = [NSMutableArray new];
    
    NSLog(@"Sub Category Count = %lu",(unsigned long)_CategoryDetails.count);
    for(int i=0;i<_CategoryDetails.count;i++)
    {
        NSMutableArray *myarray = [NSMutableArray new];
        [myarray addObject:@0];
        [_SubCategoryDetails addObject:myarray];
    }
    
    _IsOverlayViewAdded = false;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpiryTime"];
    NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
    double ExpiryTime = [str doubleValue];
    if(Time >= ExpiryTime)
    {
        
        //[self.tabBarController.view addSubview:self.overlayview];
        [self.view bringSubviewToFront:_FirstActivityIndicator];
        [_SubCategoryActivityIndicator startAnimating];
        [_FirstActivityIndicator startAnimating];
        [_SecondActivityIndicator startAnimating];
        _IsOverlayViewAdded = true;
        ReqHandler.HttpsReqType = REGENERATETOKEN;
        ReqHandler.ParentView = self;
        _IsRegenerateTokenInvoked = true;
        [ReqHandler regenerateToken];
        
    }
    else
    {
        
        //[self.tabBarController.view addSubview:self.overlayview];
        [_SubCategoryActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_FirstActivityIndicator];
        [_FirstActivityIndicator startAnimating];
        [_SecondActivityIndicator startAnimating];
        _IsOverlayViewAdded = true;
        for(int i =0;i<_CategoryDetails.count;i++)
        {
            [self getSubCategories:i];
        }
    }
    
    
    UIView *Alert = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"no category content to display",nil,appDelegate.LocalBundel,nil) :20];
    [_AlertView addSubview:Alert];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
    
    
    //adding tint to category image
    UIView *BackgroundGradient = [[UIView alloc]initWithFrame:CGRectMake(_CategoryImage.frame.origin.x, _CategoryImage.frame.origin.y, [UIScreen mainScreen].bounds.size.width, _CategoryImage.frame.size.height)];
    [BackgroundGradient setBackgroundColor:UIColorFromRGB(CLR_BANNER_IMAGE_TINT)];
    BackgroundGradient.alpha = 0.3;
    [_CategoryImage addSubview:BackgroundGradient];
    [self.view bringSubviewToFront:_CategoryNameLabel];
    //end
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)preferredContentSizeDidChangeForChildContentContainer:(id<UIContentContainer>)container
{
    NSLog(@"preferredContentSizeDidChangeForChildContentContainer>>>>");
    /*
     
     NSLog(@"container info = %@",container);
     NSRange match = [str rangeOfString:@"SIXDListTonesVC"];
     if(match.location!=NSNotFound)
     {
     
     [UIView animateWithDuration:1.0 animations:^{
     
     _FirstContainer.frame = _ListTonesFirstVC.TableView.frame ;
     _SubCategoryFirstContainerHeight.constant = _ListTonesFirstVC.TableView.frame.size.height;
     
     _ListTonesFirstVC.view.frame = CGRectMake(0, 0,_FirstContainer.frame.size.width,_FirstContainer.frame.size.height);
     
     [_FirstContainer layoutSubviews];
     
     _SecondContainer.frame = _ListTonesSecondVC.TableView.frame ;
     _SubCategorySecondContainerHeight.constant = _ListTonesSecondVC.TableView.frame.size.height;
     
     _ListTonesSecondVC.view.frame = CGRectMake(0, 0,_SecondContainer.frame.size.width,_SecondContainer.frame.size.height);
     
     [_SecondContainer layoutSubviews];
     
     } completion:nil];
     
     return;
     }
     */
    NSString *str= [NSString stringWithFormat:@"%@",container];
    NSRange match  = [str rangeOfString:@"SIXDGenericTableView"];
    
    if(match.location!=NSNotFound)
    {
       // [UIView animateWithDuration:1.0 animations:^{
            
            _ThirdContainer.frame = _GenericTableVC.TableView.frame ;
            
            _SubCategoryThirdContainerHeight.constant = _GenericTableVC.TableView.frame.size.height;
            
            _GenericTableVC.view.frame = CGRectMake(0, 0,_ThirdContainer.frame.size.width,_SubCategoryThirdContainerHeight.constant);
            
            [_ThirdContainer layoutSubviews];
            
       // } completion:nil];
        return;
    }
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    
    if ([segueName isEqualToString: @"FirstContainer"])
    {
        _ListTonesFirstVC= (SIXDListTonesVC *) [segue destinationViewController];
        _ListTonesFirstVC.Source = SUBCATEGORIES;
        _ListTonesFirstVC.SubCategoriesVC = self;
        // [self addChildViewController:_ListTonesFirstVC];
    }
    else if ([segueName isEqualToString: @"SecondContainer"])
    {
        _ListTonesSecondVC = (SIXDListTonesVC *) [segue destinationViewController];
        _ListTonesSecondVC.Source = SUBCATEGORIES;
        _ListTonesSecondVC.SubCategoriesVC = self;
        // [self addChildViewController:_ListTonesSecondVC];
    }
    else if ([segueName isEqualToString: @"ThirdContainer"])
    {
        _GenericTableVC = (SIXDGenericTableView *) [segue destinationViewController];
        _GenericTableVC.SubCategoriesVC = self;
        _GenericTableVC.Source = SUBCATEGORIES;
        //[_GenericTableVC loadSubCategories];
        // [self addChildViewController:_GenericTableVC];
    }
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

-(void)getSubCategories :(int)RowIndex
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = MULTIPLE;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];
    
    NSDictionary *SubCategoryDetails = [_CategoryDetails objectAtIndex:RowIndex];
    
    if(RowIndex==0||RowIndex==1)
    {
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&TempID=%@&pageNo=1&perPageCount=20&sortBy=Order_By&alignBy=ASC",URL,appDelegate.Language,[SubCategoryDetails objectForKey:@"cdpCategoryId"],[SubCategoryDetails objectForKey:@"categoryId"]];
        // ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&TempID=%@&pageNo=1&perPageCount=20",URL,appDelegate.Language,[SubCategoryDetails objectForKey:@"cdpCategoryId"],[SubCategoryDetails objectForKey:@"categoryId"]];
    }
    else
    {
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&TempID=%@&pageNo=1&perPageCount=20&sortBy=Order_By",URL,appDelegate.Language,[SubCategoryDetails objectForKey:@"cdpCategoryId"],[SubCategoryDetails objectForKey:@"categoryId"]];
    }
    
    [ReqHandler sendHttpGETRequest:self :nil];
    
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response : (NSString*)URL
{
    if(_IsRegenerateTokenInvoked)
    {
        if(!_IsOverlayViewAdded)
        {
            //[self.tabBarController.view addSubview:self.overlayview];
            [self.view bringSubviewToFront:_FirstActivityIndicator];
            [_SubCategoryActivityIndicator startAnimating];
            [_FirstActivityIndicator startAnimating];
            _IsOverlayViewAdded = true;
        }
        _IsRegenerateTokenInvoked = false;
        
        for(int i =0;i<_CategoryDetails.count;i++)
        {
            [self getSubCategories:i];
        }
        
        
    }
    else
    {
        
        _ResponseCount++;
        
        int RowIndex = [self fetchArrayIndexFromURL:URL];
        
        NSString *RowIndexString = [NSString stringWithFormat:@"%d",RowIndex];
        [_SuccessIdsDict setValue:RowIndexString forKey:RowIndexString];
        
        Response = [Response objectForKey:@"responseMap"];
        [_SubCategoryDetails replaceObjectAtIndex:RowIndex withObject:[Response objectForKey:@"searchList"]];
        
        if(RowIndex == 0)
        {
            [self loadFirstContainerContents];
        }
        else if (RowIndex == 1)
        {
            [self loadSecondContainerContents];
        }
    
        
        if(_ResponseCount == _CategoryDetails.count)
        {
            if(![_SuccessIdsDict objectForKey:@"0"] && !_FirstContainer.hidden)
            {
                [self loadFirstContainerContents];
            }
            if(![_SuccessIdsDict objectForKey:@"1"] && !_SecondContainer.hidden)
            {
                [self loadSecondContainerContents];
            }
            
            [_SubCategoryActivityIndicator stopAnimating];
            [self RestructureSubCtaegoryList];
        
        }
    }
    
}


-(void)loadSubCategoryIndex: (int)RowIndex
{
    
    
   // [_GenericTableVC.TableView reloadData];
   // [_GenericTableVC loadSubCategories];
    
}


-(int)fetchArrayIndexFromURL : (NSString*) URL
{
    NSRange match;
    int RowIndex=-1;
    for (int i=0; i<_CategoryDetails.count; i++)
    {
        NSDictionary *Dict = [_CategoryDetails objectAtIndex:i];
        //added TempID tag to identify index if cdpCategoryId is not unique among subcategories
        NSString *str1 = [NSString stringWithFormat:@"categoryId=%@&TempID=%@",[Dict objectForKey:@"cdpCategoryId"],[Dict objectForKey:@"categoryId"]];
        match = [URL rangeOfString:str1];
        
        if(match.location!=NSNotFound)
        {
            RowIndex=i;
            NSLog(@"Row Index decoding details= %d",RowIndex);
            break;
        }
        
    }
    return RowIndex;
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    if(_IsRegenerateTokenInvoked)
    {
        [self onFailedToConnectToServer];
    }
    else
    {
        _ResponseCount++;
        if(_ResponseCount == _CategoryDetails.count)
        {
            if(![_SuccessIdsDict objectForKey:@"0"] && !_FirstContainer.hidden)
            {
                [self loadFirstContainerContents];
            }
            if(![_SuccessIdsDict objectForKey:@"1"] && !_SecondContainer.hidden)
            {
                [self loadSecondContainerContents];
            }
            [_SubCategoryActivityIndicator stopAnimating];
            [self RestructureSubCtaegoryList];
        }
        
    }
}

-(void)onFailedToConnectToServer
{

    if(_IsRegenerateTokenInvoked)
    {
        [SIXDGlobalViewController showCommonFailureResponse:self :nil];
    }
    else
    {
        _ResponseCount++;
        if(_ResponseCount == _CategoryDetails.count)
        {
            [self.overlayview removeFromSuperview];
            
            if(![_SuccessIdsDict objectForKey:@"0"] && !_FirstContainer.hidden)
            {
                [self loadFirstContainerContents];
            }
            if(![_SuccessIdsDict objectForKey:@"1"] && !_SecondContainer.hidden)
            {
                [self loadSecondContainerContents];
            }
            [_SubCategoryActivityIndicator stopAnimating];
            [self RestructureSubCtaegoryList];
        }
    }
}



-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear SIXDSubCategories >>");
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:NO];
    
}


-(void)loadFirstContainerContents
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [_FirstActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    if(![_SuccessIdsDict objectForKey:@"0"])
    {
        [_FirstContainer setHidden:true];
        _SubCategoryFirstContainerHeight.constant = 0;
        [_CategoryDetails removeObjectAtIndex:0];
        [_SubCategoryDetails removeObjectAtIndex:0];
    }
    
    if(!_FirstContainer.hidden)
    {
        _ListTonesFirstVC.TuneList = [_SubCategoryDetails objectAtIndex:0];
        NSDictionary *Temp = [_CategoryDetails objectAtIndex:0];
        if([appDelegate.Language isEqualToString:@"Arabic"])
        {
            NSString *string = [SIXDGlobalViewController HTMLConversion:[Temp objectForKey:@"categoryName"]];
            _ListTonesFirstVC.SubCategoryTitle= string;
        }
        else
        {
            _ListTonesFirstVC.SubCategoryTitle= [Temp objectForKey:@"categoryName"];
        }
        [_ListTonesFirstVC loadTones];
        
    }
    else
    {
        _SubCategoryStartIndex --;
    }
    
}

-(void)loadSecondContainerContents
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_SecondActivityIndicator stopAnimating];
    
    if(![_SuccessIdsDict objectForKey:@"1"])
    {
        
        [_SecondContainer setHidden:true];
        _SubCategorySecondContainerHeight.constant = 0;
        [_CategoryDetails removeObjectAtIndex:1];
        [_SubCategoryDetails removeObjectAtIndex:1];
        
    }
    
    if(!_SecondContainer.hidden)
    {
        int index = 1;
        if(_FirstContainer.hidden)
            index = 0;
        
        _ListTonesSecondVC.TuneList = [_SubCategoryDetails objectAtIndex:index];
        
        NSDictionary *Temp = [_CategoryDetails objectAtIndex:index];
        if([appDelegate.Language isEqualToString:@"Arabic"])
        {
            Temp = [_CategoryDetails objectAtIndex:index];
            NSString *string = [SIXDGlobalViewController HTMLConversion:[Temp objectForKey:@"categoryName"]];
            _ListTonesSecondVC.SubCategoryTitle= string;
        }
        else
        {
            _ListTonesSecondVC.SubCategoryTitle= [Temp objectForKey:@"categoryName"];
        }
        [_ListTonesSecondVC loadTones];
    }
    else
    {
        _SubCategoryStartIndex --;
    }
    
}


-(void)RestructureSubCtaegoryList
{
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (int i=_SubCategoryStartIndex; i<_CategoryDetails.count; i++)
    {
        NSString *Key = [NSString stringWithFormat:@"%d",i];
        if(![_SuccessIdsDict objectForKey:Key])
        {
            [indexSet addIndex:i];
        }
    }
    
    if(indexSet.count > 0)
    {
        [_CategoryDetails removeObjectsAtIndexes:indexSet];
        [_SubCategoryDetails removeObjectsAtIndexes:indexSet];

    }
    
    _GenericTableVC.TuneList = [NSMutableArray new];
    _GenericTableVC.CategoryList = [NSMutableArray new];
    for (int i=_SubCategoryStartIndex; i<_SubCategoryDetails.count; i++)
    {
        [_GenericTableVC.TuneList addObject:[_SubCategoryDetails objectAtIndex:i]];
        [_GenericTableVC.CategoryList addObject:[_CategoryDetails objectAtIndex:i]];
            
    }
    
    if(_GenericTableVC.TuneList.count > 0)
    {
        [_GenericTableVC loadSubCategories];
    }
    
    if(_SubCategoryDetails.count <= 0)
    {
        [_AlertView setHidden:false];
        [_FirstContainer setHidden:true];
        [_SecondContainer setHidden:true];
        [_ThirdContainer setHidden:true];
    }
    
}
@end
