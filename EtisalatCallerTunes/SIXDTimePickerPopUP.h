//
//  SIXDTimePickerPopUP.h
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 07/03/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIXDSettingsTimeVC.h"
#import "CustomLabel.h"

@interface SIXDTimePickerPopUP : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *StartTimePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *EndTimePicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
@property (weak, nonatomic) IBOutlet UIButton *SaveButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;
@property (weak, nonatomic) IBOutlet SIXDAlertLabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIView *SubView;

- (IBAction)onClickedCloseButton:(id)sender;
- (IBAction)onClickedSaveButton:(id)sender;

@property(strong,nonatomic) SIXDSettingsTimeVC *TimeVC;
@property (nonatomic) int typeFlag;

@property (strong, nonatomic) IBOutlet UILabel *FirstLabel;
@property (strong, nonatomic) IBOutlet UILabel *SecondLabel;

@end
