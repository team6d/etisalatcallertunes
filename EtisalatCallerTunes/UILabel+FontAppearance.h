//
//  UILabel+FontAppearance.h
//  6dCRBTApp
//
//  Created by Six Dee Technologies on 11/29/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FontAppearance)
@property (nonatomic, copy) UIFont * appearanceFont UI_APPEARANCE_SELECTOR;
@end

@interface UITextField (FontAppearance)
@property (nonatomic, copy) UIFont * appearanceFont UI_APPEARANCE_SELECTOR;
@end
