//
//  SIXDMyAccountMenuVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/22/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDMyAccountMenuVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDNotificationLanguage.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"

@interface SIXDMyAccountMenuVC ()

@end

@implementation SIXDMyAccountMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [SIXDStylist setGAScreenTracker:@"View_MyAccountMenu"];

    _TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    
    self.MenuList = [[NSArray alloc] initWithObjects:@"MY WISHLIST", @"STATUS", @"MY PROFILE", @"MY TUNES", @"MY CALLER GROUP",@"BLACKLIST",@"COPY TUNES",@"HISTORY", @"CHANGE PASSWORD", @"ABOUT CALLER TUNES", @"FAQ",@"LOG OUT",@"LANGUAGE CHANGE", nil];
    
    self.MenuImageList = [[NSArray alloc] initWithObjects:@"mamywishlist.png", @"mastatus.png", @"myprofile", @"mamytunes", @"mamycallersgroup",@"mablacklist",@"macopytunes",@"mahistory", @"machangepassword", @"maaboutcallerstunes", @"mafandQ",@"malogout",@"malanguage", nil];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.MenuList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil);
    
    /*
     if(indexPath.row==11)
     {
     if([appDelegate.Language isEqualToString:@"English"])
     {
     NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:14]}];
     cell.TitleLabel.attributedText = titleString;
     }
     else
     {
     NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"NeoTechAlt" size:14]}];
     cell.TitleLabel.attributedText = titleString;
     }
     }
     else
     {
     if([appDelegate.Language isEqualToString:@"English"])
     {
     NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"NeoTechAlt" size:14]}];
     cell.TitleLabel.attributedText = titleString;
     }
     else
     {
     NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:14]}];
     cell.TitleLabel.attributedText = titleString;
     }
     }
     */
     

    
    NSString *imageName = [_MenuImageList objectAtIndex:indexPath.row];
    [cell.AccessoryButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [cell.AccessoryButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    UIImageView *sampleImage;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow"]];
    }
    else
    {
        sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow_Arabic"]];
    }
    [cell setAccessoryView:sampleImage];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row==0)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDWishlistVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==1)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDStatusVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==2)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDProfileVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==3)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMyTunesVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==4)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGroupsVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==5)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBlacklistVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==6)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCopyTuneVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==7)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDHistoryTransactions"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==8)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDChangePasswordVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==9)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDAboutVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==10)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDFaqVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if(indexPath.row==11)
    {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"You are going to be logged out of Caller Tunes",nil,appDelegate.LocalBundel,nil);
        ViewController.title = @"";
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil);
        ViewController.ParentView = self;
        _Common_TextVC = ViewController;
        [self presentViewController:ViewController animated:YES completion:nil];
        /*
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
        [self presentViewController:ViewController animated:YES completion:nil];
        
        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        appdelegate = [[AppDelegate alloc] init];
        */
        
    }
    else
    {
        /*
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDNotificationLanguage *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDNotificationLanguage"];
        ViewController.Source = CHANGE_APP_LANGUAGE;
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
         */
        [self changeAppLanguage];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
   // [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"myaccount"] landscapeImagePhone:[UIImage imageNamed:@"myaccount"] style:UIBarButtonItemStylePlain target:self action:nil];
    [self.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"MY ACCOUNT",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
    
}


-(void)onSelectedPopUpConfirmButton
{
    [SIXDStylist setGAEventTracker:@"Log out" :@"Confirm log out" :nil];
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    likeSettingsSession.Source = SRC_LOGOUT;
  //  [_Common_TextVC putOverlayView];
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    [likeSettingsSession sendTuneListAndDownload];
    
    
    
    /*
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    for (UINavigationController *Temp in VCArray)
    {
        [Temp popToRootViewControllerAnimated:false];
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
    [appDelegate.window.rootViewController.view removeFromSuperview];
    appDelegate.window.rootViewController = ViewController;
    [appDelegate.window makeKeyAndVisible];
     */
}

-(void)changeAppLanguage
{
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    
    if([appDelegate.Language isEqualToString:@"English"])
    {
        appDelegate.Language = @"Arabic";
        [self reloadApp :@"ar"];
    }
    else
    {
        appDelegate.Language = @"English";
        [self reloadApp :@"en"];
    }
}

-(void)reloadApp: (NSString*)Language
{
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);

    [[NSUserDefaults standardUserDefaults] setObject:Language forKey:@"SelectedLanguage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if([Language isEqualToString:@"en"])
    {
        appDelegate.Language= @"English";
        appDelegate.LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentLeft];
        
        
    }
    else if ([Language isEqualToString:@"ar"])
    {
        appDelegate.Language = @"Arabic";
        appDelegate.LocalBundel = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITextField appearance] setTextAlignment:NSTextAlignmentRight];
        
    }
    
    [SIXDStylist loadCustomFonts];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
    appDelegate.window.rootViewController = ViewController;
    [appDelegate.window makeKeyAndVisible];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
}

@end
