//
//  SIXDForgotPasswordVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDForgotPasswordVC : ViewController<UITextFieldDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *OtpView;
@property (weak, nonatomic) IBOutlet UIView *NewPasswordView;
@property (weak, nonatomic) IBOutlet UIView *ConfirmPasswordView;

@property (weak, nonatomic) IBOutlet UITextField *OtpTextField;
@property (weak, nonatomic) IBOutlet UITextField *NewPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *ConfirmPasswordTextField;

@property (weak, nonatomic) IBOutlet UILabel *OtpLabel;
@property (weak, nonatomic) IBOutlet UILabel *NewPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *ConfirmPasswordLabel;

@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;

@property (weak, nonatomic) IBOutlet UIButton *ResetPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *ResendButton;
@property (weak, nonatomic) IBOutlet UIButton *OtpButton;
@property (weak, nonatomic) IBOutlet UIButton *NewPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmPasswordButton;

- (IBAction)onClickedResetPasswordButton:(UIButton *)sender;
- (IBAction)onClickedResendButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToOtp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToNewPassword;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToConfirmPassword;

@property(nonatomic) BOOL ImageSetFlag;
@property (strong, nonatomic) NSString *SecurityToken;
@property (strong, nonatomic) NSString *secToc;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *EncryptedPassword;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@end
