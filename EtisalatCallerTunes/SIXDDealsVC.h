//
//  SIXDDealsVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 15/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDDealsVC : ViewController
@property (weak, nonatomic) IBOutlet UIView *MainContainer;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;


@property (weak, nonatomic) IBOutlet UIView *AlertView;

@end
