//
//  SIXDSongCatcherVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSongCatcherVC.h"

#import "ACRCloudRecognition.h"
#import "ACRCloudConfig.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDPlayerVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDStylist.h"

@interface SIXDSongCatcherVC ()

@end

@implementation SIXDSongCatcherVC
{
    ACRCloudRecognition         *_client;
    ACRCloudConfig          *_config;
    UITextView              *_resultTextView;
    NSTimeInterval          startTime;
    __block BOOL    _start;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Song_Catcher"];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //tochange
    [_MainButton setImage:[UIImage imageNamed:@"B2"] forState:UIControlStateNormal];
    _ListeningLabel.text=NSLocalizedStringFromTableInBundle(@"CLICK TO CATCH",nil,appDelegate.LocalBundel,nil);
    
    _CheckFlag=START_CHECK;
    _AlertButton.hidden=YES;
    
    _MainButton.layer.cornerRadius = _MainButton.frame.size.width/2;
    _MainButton.clipsToBounds = YES;
    
    _start = NO;
    
    _config = [[ACRCloudConfig alloc] init];
    
    _config.accessKey = @"2d0dc921a83af9fd85175582407838f3";
    _config.accessSecret = @"zgtbrwHe6k73oWtoBkwchJ62EaS3JSBS0pcT9rVw";
    _config.host = @"identify-ap-southeast-1.acrcloud.com";
    _config.protocol = @"http";
    
    //if you want to identify your offline db, set the recMode to "rec_mode_local"
    _config.recMode = rec_mode_remote;
    _config.audioType = @"recording";
    _config.requestTimeout = 10;
    
    if (_config.recMode == rec_mode_local || _config.recMode == rec_mode_both)
        _config.homedir = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"acrcloud_local_db"];
    
    __weak typeof(self) weakSelf = self;
    
    _config.stateBlock = ^(NSString *state) {
        [weakSelf handleState:state];
    };
    
    _config.volumeBlock = ^(float volume) {
        //do some animations with volume
        [weakSelf handleVolume:volume];
    };
    
    _config.resultBlock = ^(NSString *result, ACRCloudResultType resType) {
        [weakSelf handleResult:result resultType:resType];
    };
    
    //if you want to get the result and fingerprint, uncoment this code, comment the code "resultBlock".
    //_config.resultFpBlock = ^(NSString *result, NSData* fingerprint) {
    //    [weakSelf handleResultFp:result fingerprint:fingerprint];
    //};
    
    _client = [[ACRCloudRecognition alloc] initWithConfig:_config];
    
    //start pre-record
    [_client startPreRecord:3000];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)onClickedMainButton:(id)sender {
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Permission granted");
            if(_CheckFlag==START_CHECK)
            {
                NSLog(@"started recording");
                [self startACR];
                _CheckFlag=STOP_CHECK;
            }
            else
            {
                NSLog(@"stopped recording");
                [self stopSongRecognition];
                _CheckFlag=START_CHECK;
            }
        }
        else {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
            SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
            ViewController.ParentView = self;
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Please go to settings and allow access for microphone",nil,appDelegate.LocalBundel,nil);
            ViewController.AlertImageName = @"mytunelibrarypopupinfo";
            ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
            
            [self presentViewController:ViewController animated:YES completion:nil];
        }
    }];
    
    
}

-(void)newPulseEffect : (id)sender {
    
    UIButton *button = _MainButton;
    //UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    //button.frame = CGRectMake(0, 0, 100, 100);
    //button.frame = _TouchButton.frame;
    //button.center = self.view.center;
    [button setClipsToBounds:YES];
    //*[button setTitle:@"Button" forState:UIControlStateNormal];
    
    UIView *c = [[UIView alloc] initWithFrame:_MainButton.bounds];
    c.backgroundColor = [UIColor blueColor];
   // c.layer.cornerRadius = 50;
    c.layer.cornerRadius = _MainButton.layer.cornerRadius/2;
    [_MainButton addSubview:c];
    [_MainButton sendSubviewToBack:c];
    
    UIView *f = [[UIView alloc] initWithFrame:_MainButton.bounds];
    //f.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.3];
    f.backgroundColor = UIColorFromRGB(CLR_RED);
    f.layer.cornerRadius = 5;
    [_MainButton addSubview:f];
    [_MainButton sendSubviewToBack:f];
    
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = .5;
    pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulseAnimation.autoreverses = YES;
    pulseAnimation.repeatCount = MAXFLOAT;
    [c.layer addAnimation:pulseAnimation forKey:@"a"];
    [button.titleLabel.layer addAnimation:pulseAnimation forKey:@"a"];
    
    CABasicAnimation *fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fade.toValue = @0;
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.toValue = @2;
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[fade,pulse];
    group.duration = 1.0;
    group.repeatCount = MAXFLOAT;
    [f.layer addAnimation:group forKey:@"g"];
    
    [self.view addSubview:_MainButton];
    
    
}


-(void)handleResultFp:(NSString *)result
          fingerprint:(NSData*)fingerprint
{
    NSLog(@"handleResultFp");
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"%@", result);
        
        // the fingerprint is nil when can't generate fingerprint from pcm data.
        if (fingerprint) {
            NSLog(@"fingerprint data length = %ld", (unsigned long)fingerprint.length);
        }
        [_client stopRecordRec];
        _start = NO;
    });
}

-(void)handleResult:(NSString *)result
         resultType:(ACRCloudResultType)resType
{
    NSLog(@"handleResult>>");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSError *error = nil;
        
        NSData *jsonData = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        NSString *r = nil;
        
        NSLog(@"%@", result);
        
        if ([[jsonObject valueForKeyPath: @"status.code"] integerValue] == 0) {
            if ([jsonObject valueForKeyPath: @"metadata.music"]) {
                NSDictionary *meta = [jsonObject valueForKeyPath: @"metadata.music"][0];
                NSString *title = [meta objectForKey:@"title"];
                _SearchKeyTitle=title;
                NSString *artist = [meta objectForKey:@"artists"][0][@"name"];
                NSString *album = [meta objectForKey:@"album"][@"name"];
                NSString *play_offset_ms = [meta objectForKey:@"play_offset_ms"];
                NSString *duration = [meta objectForKey:@"duration_ms"];
                
                _SearchKeyArtist = artist;
                NSLog(@"_SearchKeyArtist = %@",_SearchKeyArtist);
                
                NSArray *ra = @[[NSString stringWithFormat:@"title:%@", title],
                                [NSString stringWithFormat:@"artist:%@", artist],
                                [NSString stringWithFormat:@"album:%@", album],
                                [NSString stringWithFormat:@"play_offset_ms:%@", play_offset_ms],
                                [NSString stringWithFormat:@"duration_ms:%@", duration]];
                r = [ra componentsJoinedByString:@"\n"];
            }
            if ([jsonObject valueForKeyPath: @"metadata.custom_files"]) {
                NSDictionary *meta = [jsonObject valueForKeyPath: @"metadata.custom_files"][0];
                NSString *title = [meta objectForKey:@"title"];
                NSString *audio_id = [meta objectForKey:@"audio_id"];
                
                r = [NSString stringWithFormat:@"title : %@\naudio_id : %@", title, audio_id];
            }
            if ([jsonObject valueForKeyPath: @"metadata.streams"]) {
                NSDictionary *meta = [jsonObject valueForKeyPath: @"metadata.streams"][0];
                NSString *title = [meta objectForKey:@"title"];
                NSString *title_en = [meta objectForKey:@"title_en"];
                
                r = [NSString stringWithFormat:@"title : %@\ntitle_en : %@", title,title_en];
            }
            if ([jsonObject valueForKeyPath: @"metadata.custom_streams"]) {
                NSDictionary *meta = [jsonObject valueForKeyPath: @"metadata.custom_streams"][0];
                NSString *title = [meta objectForKey:@"title"];
                
                r = [NSString stringWithFormat:@"title : %@", title];
            }
            if ([jsonObject valueForKeyPath: @"metadata.humming"]) {
                NSArray *metas = [jsonObject valueForKeyPath: @"metadata.humming"];
                NSMutableArray *ra = [NSMutableArray arrayWithCapacity:6];
                for (id d in metas) {
                    NSString *title = [d objectForKey:@"title"];
                    NSString *score = [d objectForKey:@"score"];
                    NSString *sh = [NSString stringWithFormat:@"title : %@  score : %@", title, score];
                    
                    [ra addObject:sh];
                }
                r = [ra componentsJoinedByString:@"\n"];
            }
            
            self.resultView.text = r;
            NSLog(@"r=%@",r);
            _FindStatus=TRUE;
            _CheckFlag=START_CHECK;
            [self stopSongRecognition];
            [self finalProcess];
        } else {
            self.resultView.text = result;
            NSLog(@"result = %@",result);
            _FindStatus=FALSE;
            _CheckFlag=START_CHECK;
            [self stopSongRecognition];
            [self finalProcess];
            
            
        }
        
        [_client stopRecordRec];
        _start = NO;
        
    });
}

-(void)handleVolume:(float)volume
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.volumeLabel.text = [NSString stringWithFormat:@"Volume : %f",volume];
        
    });
}

-(void)handleState:(NSString *)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.stateLabel.text = [NSString stringWithFormat:@"State : %@",state];
    });
}

-(void)startSongRecognition
{
    _AlertButton.hidden=YES;
    CABasicAnimation *theAnimation;
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.0];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.5];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateTransformx"];
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.5];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateTransformy"];
    
    
    if (_start) {
        return;
    }
    
    self.resultView.text = @"";
    self.costLabel.text = @"";
    
    [_client startRecordRec];
    _start = YES;
    
    startTime = [[NSDate date] timeIntervalSince1970];
}

- (void)startACR
{
    [_MainButton setImage:[UIImage imageNamed:@"B2"] forState:UIControlStateNormal];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _ListeningLabel.text =NSLocalizedStringFromTableInBundle(@"LISTENING",nil,appDelegate.LocalBundel,nil);
    
    _AlertButton.hidden = YES;
    
    CABasicAnimation *theAnimation;
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.0];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.1];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateTransformx"];
    
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    theAnimation.duration=1.0;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.1];
    [_MainButton.layer addAnimation:theAnimation forKey:@"animateTransformy"];
    
    if (_start) {
        return;
    }
    
    
    self.resultView.text = @"";
    self.costLabel.text = @"";
    
    [_client startRecordRec];
    _start = YES;
    
    startTime = [[NSDate date] timeIntervalSince1970];
}


-(void)stopSongRecognition
{
    [_MainButton setImage:[UIImage imageNamed:@"B2"] forState:UIControlStateNormal];
    
    
    [_MainButton.layer removeAllAnimations];
    if(_client) {
        [_client stopRecordRec];
    }
    _start = NO;
    
    NSLog(@"stopSongRecognition");
}
- (IBAction)onClickedCloseButton:(id)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)finalProcess
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_FindStatus==TRUE)
    {
        [self API_ACRSearchCDP];
    }
    else
    {        
        [_MainButton setImage:[UIImage imageNamed:@"B2"] forState:UIControlStateNormal];
        
        _AlertButton.hidden=NO;
        [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"NO_MATCH_FOUND_SPACE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        _ListeningLabel.text=NSLocalizedStringFromTableInBundle(@"TRY AGAIN",nil,appDelegate.LocalBundel,nil);
    }
}

-(void)API_ACRSearchCDP
{
    NSLog(@"API_ACRSearchCDP");
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "] invertedSet];
    NSString *FinalSearchKey = [[_SearchKeyTitle componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = CDP_SEARCH_NORMAL;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/specific-search-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=0&pageNo=1&perPageCount=1&toneId=0&sortBy=Order_By",URL,appDelegate.Language];
    
    NSLog(@"FinalSearchKey = %@",FinalSearchKey);
    
    [ReqHandler sendHttpGETRequest:self :FinalSearchKey];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"API_ACRSearchCDP onSuccessResponseRecived = %@",Response);
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    NSMutableArray *songList = [[responseMap objectForKey:@"songList"]mutableCopy];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(songList.count==0)
    {
        _AlertButton.hidden=NO;
        [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"NO_MATCH_FOUND_SPACE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        _ListeningLabel.text=NSLocalizedStringFromTableInBundle(@"TRY AGAIN",nil,appDelegate.LocalBundel,nil);
        
        [_MainButton setImage:[UIImage imageNamed:@"B2"] forState:UIControlStateNormal];
    }
    else
    {
        [SIXDStylist setGAScreenTracker:@"Song_Catcher_Results"];
        NSLog(@"song found here>>");
        SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        /*
        NSMutableArray *DummyArray = [NSMutableArray new];
        [DummyArray addObject:[songList objectAtIndex:0]];
        ViewController.PlayList=DummyArray;
        */
        ViewController.PlayList = [self findBestMatch: songList];
        ViewController.CurrentToneIndex=0;
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
        [_SearchVCObj.navigationController pushViewController:ViewController animated:true];
    }
    
}

- (NSMutableArray*)findBestMatch : (NSMutableArray*)SongList
{
    NSLog(@"findBestMatch >>>");
    NSLog(@"SongList = %@",SongList);
    //Exact Match
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", searchText];
    NSLog(@"Predicate _SearchKeyArtist = %@",_SearchKeyArtist);
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", _SearchKeyArtist];
    
    NSMutableArray *ArtistNames = [NSMutableArray new];
    for( int i=0 ;i< SongList.count ;i++)
    {
        NSDictionary *Dict = [SongList objectAtIndex:i];
        NSString *str1 = [NSString stringWithFormat:@"%@$%d",[Dict objectForKey:@"artistName"],i];
        [ArtistNames addObject:str1];
    }
    
    NSArray *results = [ArtistNames filteredArrayUsingPredicate:predicate];
    
    NSLog(@"Results = %@",results);
    NSMutableArray *Match = [NSMutableArray new];
    
    if(results.count >= 1)
    {
        NSArray *myArray = [[results objectAtIndex:0] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"$"]];
        int index = [[myArray objectAtIndex:1]intValue];
        [Match addObject:[SongList objectAtIndex:index]];
        NSLog(@"Final Best Result = %@",Match);
    }
    else
    {
         [Match addObject:[SongList objectAtIndex:0]];
    }
    
    return Match;
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", _SearchKeyTitle];
    
    
    
    
}
-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    [SIXDStylist setGAEventTracker:@"SongCatcher" :@"CDP_SongNotFound" :_SearchKeyTitle];
    
    NSLog(@"API_ACRSearchCDP onFailureResponseRecived = %@",Response);
    _AlertButton.hidden=NO;
    NSString *message=[Response objectForKey:@"message"];
    [_AlertButton setTitle:message forState:UIControlStateNormal];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"API_ACRSearchCDP onFailedToConnectToServer");
    _AlertButton.hidden=NO;
    [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
}

- (void)onInternetBroken
{    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end

