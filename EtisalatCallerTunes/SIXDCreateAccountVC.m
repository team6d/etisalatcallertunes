//
//  SIXDCreateAccountVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 08/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCreateAccountVC.h"
#import "SIXDStylist.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDAlertViewVC.h"
#import "SIXDVerifyOtpVC.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"

@interface SIXDCreateAccountVC ()

@end

@implementation SIXDCreateAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [SIXDStylist setGAScreenTracker:@"Registration_Enter_OTP"];
   
    [SIXDStylist setBackGroundImage:self.view];
    
    [_OtpTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self InitialCondition];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _OtpTextField.enablesReturnKeyAutomatically = NO;
    _OtpTextField.delegate = self;
    _AlertLabel.hidden = YES;
    
    _OtpLabel.hidden = YES;
    
    //[_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"VERIFICATION CODE (OTP)",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Verification Code*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"CREATE MY ACCOUNT",nil,appDelegate.LocalBundel,nil);
    _OtpLabel.text = NSLocalizedStringFromTableInBundle(@"Verification Code*",nil,appDelegate.LocalBundel,nil);
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"REGISTER",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Resend Verification Code (OTP)",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:FONT_TEXTFIELD size:13]}];
    [_ResendButton setAttributedTitle:titleString forState:UIControlStateNormal];
    [_ResendButton setTintColor:[UIColor whiteColor]];
    
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_AlertButton setImage:image forState:UIControlStateNormal];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _ActivityIndicator.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_OtpTextField.text.length==0)
    {
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_OtpTextField :_OtpView :_AlertButton];
    }
    else
    {
        [SIXDStylist setGAEventTracker:@"Account creation" :@"Register" :nil];
        NSLog(@"API_OtpCheck call >>");
        [self API_OtpCheck];
        [self InitialCondition];
    }
}
- (IBAction)onClickedResendButton:(UIButton *)sender
{
    [self InitialCondition];
    /*
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDVerifyOtpVC"];
    [self presentViewController:ViewController animated:YES completion:nil];
    */
    NSLog(@"API_SendOtp call>>");
    [self API_SendOtp];
}
- (IBAction)onClickedRobotButton:(UIButton *)sender{
    
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_OtpTextField resignFirstResponder];
    
    if(_OtpTextField.text.length==0||_OtpTextField.text.length==4)
    {
        _AlertLabel.hidden=YES;
        [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_AlertButton];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _TopSpaceToTextField.constant=19;
    _OtpLabel.hidden=NO;
    [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_OtpTextField.text.length==0)
    {
        [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"VERIFICATION CODE (OTP)",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _OtpLabel.hidden=YES;
        _TopSpaceToTextField.constant=0;
    }
}

- (void)InitialCondition
{
    _AlertLabel.hidden = YES;
    [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_AlertButton];
}

-(void)API_OtpCheck
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=CHECK_OTP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&otp=%@&clientTxnId=%@&language=%@&type=ValidateDetails&secToc=%@",appDelegate.MobileNumber,_OtpTextField.text,Rno,appDelegate.Language,_SectionToken];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/otp-check",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_SendOtp
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SEND_OTP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@",appDelegate.MobileNumber,Rno,appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/send-otp-wp",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==SEND_OTP)
    {
        Response = [Response objectForKey:@"responseMap"];
        _SectionToken = [Response objectForKey:@"secToc"];
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDAlertViewVC"];
        appDelegate.APIFlag=CREATE_ACCOUNT;
        [self presentViewController:ViewController animated:YES completion:nil];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSString *message = [Response objectForKey:@"message"];
    NSLog(@"message = %@", message);
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = message;
   // [SIXDGlobalViewController setAlert:message :self];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    //[SIXDGlobalViewController setAlert:@"Please check your network connectivity" :self];
    
    [_AlertLabel setHidden:NO];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

@end
