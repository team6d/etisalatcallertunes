//
//  SIXDStatusVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDAVPlayer.h"

@interface SIXDStatusVC : ViewController
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
- (IBAction)onClickedBuyButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property(nonatomic,strong) NSMutableArray *StatusDetails;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
- (IBAction)onSelectedPlayPauseButton:(UIButton *)sender;


@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property(strong,nonatomic) SIXDAVPlayer *SIXDAVPlayer_Audio;
@property(strong,nonatomic) UIButton *SelectedButton;
@end
