//
//  SIXDPlaylistBuyVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDPlaylistBuyVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDBuyToneVC.h"
#import "SIXDGiftToneVC.h"
#import "SIXDTellAFriendVC.h"
#import "SIXDWishlistPopUpVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDSocialSharePopUpVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"

@interface SIXDPlaylistBuyVC ()

@end

@implementation SIXDPlaylistBuyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_UpperView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    [_LowerView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        
        if(_IsDeletePage)
        {
         _wishlistButtonCenterX.constant = -38;
            _WishlistView.hidden = YES;
            _WishListViewWidth.constant = 0;
        _SpaceBtwWishlistAndGiftView.constant = 0;
            
        }
        else
        {
           _WishListViewWidth.constant = 65;
        }
        
        _TopSpaceToImage.constant = 80;
        _GiftViewWidth.constant = 65;
        _TAFViewWidth.constant = 65;
        _ShareViewWidth.constant = 65;
        _ToneImageheight.constant = _ToneImageheight.constant -30;
        _ToneImageWidth.constant = _ToneImageWidth.constant-30;
    _SpaceBtwImageAndTextlabel.constant = 20;
    _SpaceBetweenLikeAndtextlabel.constant = 0;
        _UpperViewHeight.constant = _UpperViewHeight.constant - 80;
    }
    else
    {
        if(appDelegate.DeviceSize == iPHONE_X)
        {
            _UpperViewHeight.constant = 530;
        }
        
        if(_IsDeletePage)
        {
            _wishlistButtonCenterX.constant = -48;
            _WishlistView.hidden = YES;
            _WishListViewWidth.constant = 0;
            _SpaceBtwWishlistAndGiftView.constant = 0;
            
        }
    }
    
    [self.view bringSubviewToFront:_BuyButton];
    [self localisation];
    
    [self setViewBorder:_GiftView];
    [self setViewBorder:_WishlistView];
    [self setViewBorder:_TAFView];
    [self setViewBorder:_ShareView];
    
    
    NSURL *ImageURL = [NSURL URLWithString:[_PlaylistDetails objectForKey:@"previewImage"]];
    [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    _TopLabel.text = [[_PlaylistDetails objectForKey:@"channelName"] uppercaseString];
    
    
    UITapGestureRecognizer *GiftTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleGiftTap:)];
    [self.GiftView addGestureRecognizer:GiftTap];
    
    UITapGestureRecognizer *WishlistTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleWishlistTap:)];
    [self.WishlistView addGestureRecognizer:WishlistTap];
    
    UITapGestureRecognizer *TAFTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTAFTap:)];
    [self.TAFView addGestureRecognizer:TAFTap];
    
    UITapGestureRecognizer *ShareTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleShareTap:)];
    [self.ShareView addGestureRecognizer:ShareTap];
    
   
    //LIKE SETTINGS
    NSLog(@"Player::Calling likeSettingsSession >>>");
    [SIXDStylist loadLikeUnlikeButtonSetings:_LikeButton :[_PlaylistDetails objectForKey:@"toneCode"]];
    
    //***set like count *********
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([_PlaylistDetails objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[_PlaylistDetails objectForKey:@"toneCode"] :[[_PlaylistDetails objectForKey:@"likeCount"]intValue] :_LikeLabel];
    }
    else
       _LikeLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count enda *********
    
    
    //added on 4/05/2018 - hiding lower view if "isCopy" = false
    if(_IsDeletePage)
    {
        if(![[_PlaylistDetails objectForKey:@"isCopy"] isEqualToString:@"T"])
        {
            [_LowerView setHidden:true];
        }
    }
    //end
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedBuyButton:(UIButton *)sender{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if(!_IsDeletePage)
    {
        [SIXDStylist setGAEventTracker:@"Playlist" :@"Buy playlist request" :[NSString stringWithFormat:@"%@-%@",[_PlaylistDetails objectForKey:@"description"],[_PlaylistDetails objectForKey:@"toneCode"]]];
        SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
        ViewController.ToneDetails = _PlaylistDetails;
        ViewController.IsSourceBuyPlaylist = true;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
          AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
            SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
            ViewController.ParentView = self;
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Are you sure you want to delete?",nil,appDelegate.LocalBundel,nil);
            ViewController.AlertImageName = @"mytunelibrarypopupinfo";
            ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
            
            [self presentViewController:ViewController animated:YES completion:nil];
    }
    
}

- (void)handleGiftTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"GiftPlaylist_GetPrice"];
    [SIXDStylist setGAEventTracker:@"Playlist" :@"Gift playlist request" :[NSString stringWithFormat:@"%@-%@",[_PlaylistDetails objectForKey:@"description"],[_PlaylistDetails objectForKey:@"toneCode"]]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDGiftToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGiftToneVC"];
    
    ViewController.IsSourceGiftPlaylist = true;
    ViewController.ToneDetails = _PlaylistDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

-(void)handleWishlistTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"AddToWishlist_Playlist"];
    [SIXDStylist setGAEventTracker:@"Playlist" :@"Add to wishlist" :[NSString stringWithFormat:@"%@-%@",[_PlaylistDetails objectForKey:@"description"],[_PlaylistDetails objectForKey:@"toneCode"]]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDWishlistPopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDWishlistPopUpVC"];
    ViewController.WishListType = 2; //Playlist
    ViewController.ToneDetails = [_PlaylistDetails mutableCopy];
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleTAFTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"TellAFriend_Playlist"];
    [SIXDStylist setGAEventTracker:@"Playlist" :@"Tell a friend" :[NSString stringWithFormat:@"%@-%@",[_PlaylistDetails objectForKey:@"description"],[_PlaylistDetails objectForKey:@"toneCode"]]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDTellAFriendVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTellAFriendVC"];
    ViewController.ToneDetails = [_PlaylistDetails mutableCopy];
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleShareTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"Share_Playlist"];
    [SIXDStylist setGAEventTracker:@"Playlist" :@"Share" :[NSString stringWithFormat:@"%@-%@",[_PlaylistDetails objectForKey:@"description"],[_PlaylistDetails objectForKey:@"toneCode"]]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDSocialSharePopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSocialSharePopUpVC"];
    ViewController.Source = PLAYLIST;
    ViewController.ToneDetails=_PlaylistDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onClickedLikeButton:(UIButton *)sender
{
    // _LikeButton.selected = !_LikeButton.selected;
    NSLog(@"Player::Calling onSelectedLikeButton >>>");
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    //NSMutableDictionary *TuneDetails = [_WishlistTuneArray objectAtIndex:sender.tag];
    
    NSString *tuneId = [_PlaylistDetails objectForKey:@"toneCode"];
    NSString *categoryId = [_PlaylistDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [_PlaylistDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
    
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :_LikeLabel];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"playlistsolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@",_TopLabel.text,[_PlaylistDetails objectForKey:@"toneCode"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"playlistoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear SIXDPlaylistBuyVC >>");
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    
    [self.navigationController.navigationItem.leftBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    [self.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    [self.navigationController.navigationItem.leftBarButtonItem setEnabled:YES];
    [self.navigationController.navigationBar setBarTintColor: [UIColor clearColor]];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.opaque = NO;
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(!_IsDeletePage)
    {
        [_BuyButton setTitle:NSLocalizedStringFromTableInBundle(@"BUY",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    }
    else
    {
            [_BuyButton setTitle:NSLocalizedStringFromTableInBundle(@"DELETE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    }
    
    _GiftLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT_SMALL",nil,appDelegate.LocalBundel,nil);
    _WishlistLabel.text = NSLocalizedStringFromTableInBundle(@"WISHLIST_SMALL",nil,appDelegate.LocalBundel,nil);
    _TAFLabel.text = NSLocalizedStringFromTableInBundle(@"Tell a Friend",nil,appDelegate.LocalBundel,nil);
    _ShareLabel.text = NSLocalizedStringFromTableInBundle(@"SHARE_SMALL",nil,appDelegate.LocalBundel,nil);
    
    
   // [_LikeButton setImage:[UIImage imageNamed:@"playlistsolid"] forState:UIControlStateSelected];
    //[_LikeButton setImage:[UIImage imageNamed:@"playlistoutline"] forState:UIControlStateNormal];
}

-(void)setViewBorder:(UIView *)myView
{
    myView.layer.borderWidth = 0.5f;
    myView.layer.borderColor = [UIColorFromRGB(CLR_BoxBorder) CGColor];
}

-(void)API_DeleteTone
{
    
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=DELETE_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    NSLog(@"ViewController.PlaylistDetails = %@",_PlaylistDetails);
    NSString *PackName =  [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&toneId=%@&language=%@&packName=%@",appDelegate.MobileNumber,[_PlaylistDetails objectForKey:@"toneId"],appDelegate.Language,PackName];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-tone",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    _MYTunesLibraryVC.IsRefreshRequired = true;
    [self.navigationController popViewControllerAnimated:false];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    NSLog(@"onFailureResponseRecived Response = %@",Response);
    
}

-(void)onFailedToConnectToServer
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)onSelectedPopUpConfirmButton
{
    NSString *toneId = [_PlaylistDetails objectForKey:@"toneId"];
    if(toneId.length <= 0)
    {
        toneId = [_PlaylistDetails objectForKey:@"toneCode"];
    }
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@",_TopLabel.text,toneId];
    [SIXDStylist setGAEventTracker:@"Settings" :@"Delete playlist" :GAEventDetails];
    [self API_DeleteTone];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    NSString *urlStr = [[_PlaylistDetails objectForKey:@"previewImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              UIImage *image1;
                                              if(error)
                                              {
                                                  image1 = [UIImage imageNamed:@"Default.png"];
                                              }
                                              else
                                              {
                                                  if(data != NULL)
                                                  {
                                                      image1 =[UIImage imageWithData: data];
                                                     
                                                  }
                                              }
                                            UIGraphicsBeginImageContext(_UpperView.frame.size);
                                                  [image1 drawInRect:_UpperView.bounds];
                                                  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                                                  UIGraphicsEndImageContext();
                                                  [_UpperView setBackgroundColor:[UIColor colorWithPatternImage:image]];
                                                  
                                                  
                                                  UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
                                                  UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                                                  blurEffectView.tag = 2;
                                                  blurEffectView.frame =  _UpperView.bounds;
                                                  blurEffectView.alpha = 0.85;
                                                  blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                                    
                                                  [_UpperView insertSubview:blurEffectView atIndex:0];
                                                  
                                                  
                                                  
                                                  [_UpperView setBackgroundColor:[UIColor colorWithPatternImage:image]];
                                                  
                                              
                                              
                                          }];
    [postDataTask resume];
    
   
    
    
}

@end
