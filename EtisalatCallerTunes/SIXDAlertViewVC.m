//
//  SIXDAlertViewVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 08/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDAlertViewVC.h"
#import "SIXDStylist.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDSignInVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHttpRequestHandler.h"

@interface SIXDAlertViewVC ()

@end

@implementation SIXDAlertViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [SIXDStylist setBackGroundImage:self.view];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([self.restorationIdentifier isEqualToString:@"SIXDAlertViewVC"])
    {
        if(appDelegate.APIFlag==PASSWORD_CHANGE)
        {
            [SIXDStylist setGAScreenTracker:@"Forget_Password_Success"];
            [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"GO TO SIGN IN",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            [_AlertButtonTop setTitle:NSLocalizedStringFromTableInBundle(@"your new password has been set successfully",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            [_AlertButtonTop setImage:[UIImage imageNamed:@"Tickie"] forState:UIControlStateNormal];
        }
        else if(appDelegate.APIFlag==CREATE_ACCOUNT)
        {
            //added
             [SIXDStylist setGAScreenTracker:@"Registration_Success"];
            //end
            
            [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            [_AlertButtonTop setTitle:NSLocalizedStringFromTableInBundle(@"your account has been created successfully",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            [_AlertButtonTop setImage:[UIImage imageNamed:@"Tickie"] forState:UIControlStateNormal];
        }
    }
    else
    {
        [_NumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
        _NumberTextField.textColor = UIColorFromRGB(CLR_PLACEHOLDER);
                
        _NumberTextField.text = appDelegate.MobileNumber;
        
        _NumberTextField.userInteractionEnabled=FALSE;
        
        _NumberTextField.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        _NumberTextField.textColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        
        _NumberView.layer.borderWidth = 1.0f;
        _NumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
        _NumberView.alpha=0.95;
        
        _TopSpaceToTextField.constant=19;
        
        _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"REGISTER",nil,appDelegate.LocalBundel,nil);
        _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
        
        
        [_CreateAccountButton setTitle:NSLocalizedStringFromTableInBundle(@"CREATE MY ACCOUNT",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        [_AlertButton setTitle:NSLocalizedStringFromTableInBundle(@"this number is not yet registered",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        
        [SIXDStylist setTextViewNormal:_NumberTextField :nil :nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedAlertButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if(appDelegate.APIFlag==PASSWORD_CHANGE)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSignInVC"];
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    else if(appDelegate.APIFlag==CREATE_ACCOUNT)
    {
        [SIXDStylist setGAEventTracker:@"Account creation" :@"Account created" :nil];
        [self API_GetSecurityToken];
         
        /*
         UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCreateAccountVC"];
         SIXDSignInVC *svc = (SIXDSignInVC*)ViewController;
        [svc API_GetSecurityToken];
         */
        
    }
}

- (IBAction)onClickedCreateAccountButton:(UIButton *)sender
{
    [SIXDStylist setGAEventTracker:@"Account creation" :@"Create my account" :nil];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDRegisterVC"];
    [self presentViewController:ViewController animated:YES completion:nil];
}

- (IBAction)onClickBackButton:(id)sender {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)API_GetSecurityToken
{
    NSLog(@"API_GetSecurityToken >>");
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SECURITY_TOKEN;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/security-token",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?",URL];
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)API_PasswordValidation
{
    NSLog(@"API_PasswordValidation >>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=PASSWORD_VALIDATE;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.IsURLEncodingNotRequired = true;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&language=%@&clientTxnId=%@&type=ValidateDetails&securityCounter=%@&encryptedPassword=%@&os=ios&notificationTrackingId=%@",appDelegate.MobileNumber,appDelegate.Language,Rno,_SecurityToken,_EncryptedPassword,appDelegate.FCMToken];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/password-validation",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==SECURITY_TOKEN)
    {
        NSLog(@"API_GetSecurityToken onSuccessResponseRecived = %@",Response);
        _SecurityToken =[responseMap objectForKey:@"securityCounter"];
        appDelegate.SecurityToken=_SecurityToken;
        [self EncryptPassord];
        
    }
    else
    {
        [_ActivityIndicator stopAnimating];
        [self.overlayview removeFromSuperview];
        
        NSLog(@"API_PasswordValidation onSuccessResponseRecived = %@",Response);
        //[self InitialCondition];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *str1 = [responseMap objectForKey:@"accessToken"];
        
        if(str1.length > 0)
            appDelegate.AccessToken = str1;
        
        str1 = [responseMap objectForKey:@"deviceId"];
        
        
        double ExipryTime;
        str1 = [responseMap objectForKey:@"expiry"];
        ExipryTime = [str1 doubleValue];
        ExipryTime = ExipryTime/1000;
        
        //ExipryTime = atol(str1);
        
        
        str1 = [responseMap objectForKey:@"otp"];
        
        
        
        str1 = [responseMap objectForKey:@"refreshToken"];
        
        if(str1.length > 0)
            appDelegate.RefreshToken = str1;
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.AccessToken forKey:@"AccessToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.RefreshToken forKey:@"RefreshToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MobileNumber"];
        [[NSUserDefaults standardUserDefaults] setObject:appDelegate.MobileNumber forKey:@"MobileNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
        //ExipryTime = Time + ExipryTime;
        ExipryTime = Time + ExipryTime - 10 ;
        
        NSString *Str = [NSString stringWithFormat:@"%f",ExipryTime];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] setObject:Str forKey:@"ExpiryTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
        [[NSUserDefaults standardUserDefaults] setObject:@"LOGIN" forKey:@"State"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainTabController"];
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *message =[Response objectForKey:@"message"];
    /*
    _AlertLabel.text= message;
    _AlertLabel.hidden=NO;
    
    [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_AlertButton];
    */
    [SIXDGlobalViewController setAlert:message :self];
    
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) :self];
}

-(void)EncryptPassord
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURL *jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt" withExtension:@"js"];
    NSString *jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    NSString *oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main.js";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:nil ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt.min" withExtension:@"js"];
    jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main1.js";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    NSString *Temp = [NSString stringWithFormat:@"%@%@",appDelegate.Password,_SecurityToken];
    
    NSLog(@"Password = %@",Temp);
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"SearchKey" withString:Temp];
    _webView = [UIWebView new];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    _webView.delegate = self;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *pwd = [_webView stringByEvaluatingJavaScriptFromString:@"getLanguage()"];
    /*
    _EncryptedPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,(CFStringRef)pwd,
                                                                                               NULL,
                                                                                               (CFStringRef)@"+",
                                                                                               kCFStringEncodingUTF8 ));
     */
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    _EncryptedPassword = [pwd
                          stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                          ];
    NSLog(@"API_PasswordValidation call >>");
    [self API_PasswordValidation];
    
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"onInternetBroken::");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}


@end
