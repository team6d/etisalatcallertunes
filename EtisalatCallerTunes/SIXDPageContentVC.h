//
//  SIXDPageContentVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/21/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDPageContentVC : ViewController
@property (strong, nonatomic) IBOutlet UIImageView *PageContentImage;
@property (strong, nonatomic) NSDictionary *BannerDetails;
@property (strong, nonatomic) NSMutableArray *BannerDetailsList;
@property NSUInteger pageIndex;

@property NSURL *imageFile;
@property(nonatomic) int Source;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@end
