//
//  SIXDSettingsGroupVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDMainSettingsVC.h"
@import ContactsUI;

@interface SIXDSettingsGroupVC : ViewController<UITextFieldDelegate,CNContactPickerDelegate>

#define ALLCALLERS      1
#define GROUP           2
#define SPL_CALLERS     3

@property (weak, nonatomic) IBOutlet UIView *UpperView;
@property (weak, nonatomic) IBOutlet UIImageView *MainImage;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;

@property (weak, nonatomic) IBOutlet UIView *NumberView;
@property (weak, nonatomic) IBOutlet UITextField *NumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *NumberButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToNumber;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onClickedConfirmButton:(id)sender;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;

@property (weak, nonatomic) IBOutlet UIButton *AllCallersButton;
@property (weak, nonatomic) IBOutlet UIButton *CallerGroupsButton;
@property (weak, nonatomic) IBOutlet UIButton *SpecialCallerButton;

@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UILabel *ExampleLabel;
@property(strong,nonatomic)NSArray *PhoneNumbers;

- (IBAction)onClickedAllCallersButton:(id)sender;
- (IBAction)onClickedCallerGroupsButton:(id)sender;
- (IBAction)onClickedSpecialCallerButtonButton:(id)sender;

//@property (nonatomic) int SelectedButton;
@property (weak, nonatomic) UIButton *SelectedButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property(strong,nonatomic)NSMutableArray *GroupList;
@property(strong,nonatomic)NSMutableArray *GroupIdList;
@property(strong,nonatomic) SIXDMainSettingsVC *MainSettingsVC;
- (IBAction)onSelectedContactButton:(UIButton *)sender;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *VerticalSpace1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *VerticalSpace2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *VerticalSpace3;



@end
