//
//  SIXDTellAFriendVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 29/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDTellAFriendVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHttpRequestHandler.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"

@interface SIXDTellAFriendVC ()

@end

@implementation SIXDTellAFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    [self localisation];
    
    _TextFieldList = [NSMutableArray new];
    
    for (int i=0; i<3; i++)
    {
        [_TextFieldList addObject:@0];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _AlertLabel.textColor = [UIColor redColor];
    
    NSLog(@"_ToneDetails TAF = %@",_ToneDetails);
    
   
    
}

-(void) dismissKeyboard
{
    [_NumberTextField resignFirstResponder];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_NumberTextField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    if(_NumberTextField.text.length==0)
    {
        [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Friend Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        cell.TitleLabel.hidden=YES;
        cell.TopSpaceToTextField.constant=0;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    
    [_NumberTextField resignFirstResponder];

    if(_ShouldDismissView)
    {
         [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    
   if([self validateTextFieldDetails])
   {
       NSString *toneId = [_ToneDetails objectForKey:@"toneId"];
       if(toneId.length <= 0)
       {
           toneId = [_ToneDetails objectForKey:@"toneCode"];
       }
       NSString *GAEventDetails;
       if (_BottomLabel.text.length <= 0)
       {
           GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_TopLabel.text,_TopLabel.text,toneId];
       }
       else
       {
           GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,toneId];
       }
       
       [SIXDStylist setGAEventTracker:@"Tell a friend" :@"Confirm tell a friend" :GAEventDetails];
       
       [_ActivityIndicator startAnimating];
       [_ConfirmButton setUserInteractionEnabled:false];
       _ConfirmButton.alpha = 0.3;
       [self ValidateNumbers];
   }
    
}
- (IBAction)onClickedCloseButton:(UIButton *)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self changeCellColor:false:0];
    [self changeCellColor:false:1];
    [self changeCellColor:false:2];
    [_AlertLabel setHidden:true];
    
   // [_ScrollView setContentOffset:CGPointMake(0,160) animated:YES];
    [_ScrollView setContentOffset:CGPointMake(0,180) animated:YES];
    _NumberTextField=textField;
    
    if(_NumberTextField.text.length ==0)
    {
        _NumberTextField.text = @"+971-";
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
        cell.TitleLabel.hidden=NO;
        [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        cell.TopSpaceToTextField.constant=19;
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if([textField.text isEqualToString:@"+971-"])
    {
        textField.text = @"";
    }
    
    if(textField.text.length==0)
    {
        [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Friend Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        cell.TitleLabel.hidden=YES;
        cell.TopSpaceToTextField.constant=0;
    }
}

-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"TELL A FRIEND",nil,appDelegate.LocalBundel,nil);
    //screen setup
    
    _TopLabel.text = [[_ToneDetails objectForKey:@"artistName"] capitalizedString];
    _BottomLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    
    
    if(_TopLabel.text.length <= 0)
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"description"] capitalizedString];
    }
    
    _AlertLabel.text = @"";
 
    NSString *urlstring = [[_ToneDetails objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    if(urlstring.length <= 0)
    {
      urlstring = [[_ToneDetails objectForKey:@"previewImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    }
    
    
    
    [_ToneImage sd_setImageWithURL:[NSURL URLWithString:urlstring] placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    NSString *message = NSLocalizedStringFromTableInBundle(@"TELL A FRIEND DESCRIPTION",nil,appDelegate.LocalBundel,nil);
    
    NSMutableAttributedString *attributedMessage =[[NSMutableAttributedString alloc] initWithString:message];
    
    _AlertLabel.textColor = [UIColor darkGrayColor];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    [style setLineSpacing:3];
    [style setAlignment:NSTextAlignmentCenter];
    [attributedMessage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,message.length)];
    _DescriptionLabel.attributedText = attributedMessage ;
    
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    _PhoneNumbers = contact.phoneNumbers;
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
}

-(void)contactPickerDidCancel:(CNContactPickerViewController *)picker
{
     [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
}

-(void)ShowMultipleNumbers
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_NumberTextField.tag inSection:0] ;
                                     SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
                                     if(_NumberTextField.text.length==0)
                                     {
                                         NSLog(@"ShowMultipleNumbers >>>  formating textfield...1111 ");
                                         
                                         
                                         [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Friend Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
                                         cell.TitleLabel.hidden=YES;
                                         cell.TopSpaceToTextField.constant=0;
                                     }
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _NumberTextField.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _NumberTextField.text = [label.value stringValue];
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_NumberTextField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if(_NumberTextField.text > 0)
    {
        cell.TitleLabel.hidden=NO;
        cell.TopSpaceToTextField.constant=19;
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.FirstTextField.tag=indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);

    [cell.FirstTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    cell.FirstTextField.keyboardType = ASCII_NUM_KEYBOARD;
    
    [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Friend Number*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    [SIXDStylist addKeyBoardDoneButton:cell.FirstTextField :self];
   
    UIImage* image = [[UIImage imageNamed:@"giftatuneaddcontact"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.AccessoryButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
   
    
    if(indexPath.row < _TextFieldList.count)
    {
        [_TextFieldList replaceObjectAtIndex:indexPath.row withObject:cell.FirstTextField];
    }
    
    cell.TitleLabel.hidden = YES;
    cell.FirstTextField.delegate = self;
    cell.FirstTextField.enablesReturnKeyAutomatically = NO;
    
    cell.SubView.layer.borderWidth = 1.0f;
    cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];

    return cell;
    
}

- (IBAction)onClickedContactButton:(UIButton*)sender
{
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    _NumberTextField = cell.FirstTextField;
    
    [self dismissKeyboard];
    
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
    
    [self changeCellColor:false:0];
    [self changeCellColor:false:1];
    [self changeCellColor:false:2];
    [_AlertLabel setHidden:true];
    
}


-(void)ValidateNumbers
{
    NSLog(@"Inside API_GetTonePriceAndValidateNumber");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_TONE_PRICE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    NSMutableArray *MemberListArray = [NSMutableArray new];
    UITextField * TextField;
    
   
    NSString * str;
    
    for(int i=0;i<_TextFieldList.count;i++)
    {
        TextField = [_TextFieldList objectAtIndex:i];
        
        str = [TextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [TextField.text length])];
        
        NSMutableDictionary *ContactDict = [NSMutableDictionary new];
        [ContactDict setObject:str  forKey:@"bPartyMsisdn"];
        [MemberListArray addObject:ContactDict];
    }
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:MemberListArray options:0 error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else
    {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString *toneId;
    
    if(_IsSourcePlaylist)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    else
    {
        toneId = [_ToneDetails objectForKey:@"toneId"];
    }
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&serviceId=1&aPartyMsisdn=%@&toneId=%@&validationIdentifier=1&channelId=%@&bPartyMsisdnList=%@",Rno,appDelegate.Language, appDelegate.MobileNumber,toneId,CHANNEL_ID,MemberDetails];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-tone-price",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)Referral
{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= REFERRAL;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    /*
    NSMutableArray *MemberListArray = [NSMutableArray new];
    UITextField * TextField;
    
    for(int i=0;i<_TextFieldList.count;i++)
    {
        TextField = [_TextFieldList objectAtIndex:i];
        NSMutableDictionary *ContactDict = [NSMutableDictionary new];
        [ContactDict setObject:TextField.text  forKey:@"msisdn"];
        //TO DO
        [ContactDict setObject:TextField.text  forKey:@"amount"];
        [ContactDict setObject:TextField.text  forKey:@"toneId"];
        [ContactDict setObject:TextField.text  forKey:@"packName"];
     
        [MemberListArray addObject:ContactDict];
    }
    */
    
    
    //for testing REMOVE
    NSMutableArray *MemberListArray = [NSMutableArray new];
    
    for (int i=0; i<3; i++)
    {
        
        NSMutableDictionary *Dict = [_BPartyList objectAtIndex:i];
        
        NSMutableDictionary *Dict1 = [NSMutableDictionary new];

        [Dict1 setObject:[Dict objectForKey:@"bPartyMsisdn"] forKey:@"msisdn"];
        [Dict1 setObject:@"1.0" forKey:@"amount"];
        [Dict1 setObject:[Dict objectForKey:@"toneId"] forKey:@"toneId"];
        [Dict1 setObject:[Dict objectForKey:@"packName"] forKey:@"packName"];
        [Dict1 setObject:@"1" forKey:@"languageId"];
        [Dict1 setObject:@"customerContent_temp" forKey:@"customerContent"];
        
        [MemberListArray addObject:Dict1];
        
    }
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:MemberListArray options:0 error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else
    {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    ReqHandler.HttpsReqType = POST;
    
    NSString *toneId;
    NSString *toneName;
    
    if(_IsSourcePlaylist)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
        toneName = [_ToneDetails objectForKey:@"description"];
    }
    else
    {
        toneId = [_ToneDetails objectForKey:@"toneId"];
        toneName = [_ToneDetails objectForKey:@"toneName"];
    }
    
    
    ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&toneId=%@&toneName=%@&msisdnList=%@&language=%@",appDelegate.MobileNumber,toneId,toneName,MemberDetails,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/referral",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(appDelegate.APIFlag==GET_TONE_PRICE)
    {
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        
       _BPartyList = [[responseMap objectForKey:@"responseDetails"]mutableCopy];
        
        int Status = -1;
        for(int i=0;i<3;i++)
        {
            NSMutableDictionary *responseDetails = [_BPartyList objectAtIndex:i];
            int statusCodes = [[responseDetails objectForKey:@"statusCodes"]intValue];
            if(statusCodes != 0)
            {
                Status = i;
                [self changeCellColor:true:i];
                 _AlertLabel.text = [responseDetails objectForKey:@"statusDesc"];
                [_AlertLabel setHidden:false];
                
            }
                
        }
        
        if(Status == -1)
        {
            [self Referral];
        }
        
        
    }
    else
    {
        NSString *message = [Response objectForKey: @"message"];
        
        if(message.length <= 0)
        {
            message = NSLocalizedStringFromTableInBundle(@"General_Success_Message",nil,appDelegate.LocalBundel,nil);
        }
        
        NSMutableAttributedString *attributedMessage =[[NSMutableAttributedString alloc] initWithString:message];
        
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
        [style setLineSpacing:3];
        [style setAlignment:NSTextAlignmentCenter];
        [attributedMessage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,message.length)];
        _DescriptionLabel.attributedText = attributedMessage;
        
        [_AlertLabel setHidden:true];
        _ShouldDismissView = true;
        [_ConfirmButton setTitle:@"OK" forState:UIControlStateNormal];
        
        _PopUpViewHeight.constant = _PopUpViewHeight.constant - _TableViewHeight.constant;
        _TableViewHeight.constant = 0;
        
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"onFailureResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    
    _AlertLabel.text =[Response objectForKey:@"message"];
    //[_AlertLabel sizeToFit];
    _AlertLabel.textAlignment = NSTextAlignmentCenter;
    [_AlertLabel setHidden:false];
    
    if(appDelegate.APIFlag == REFERRAL)
    {
        _PopUpViewHeight.constant = _PopUpViewHeight.constant - _TableViewHeight.constant;
        _TableViewHeight.constant = 0;
        [_ConfirmButton setTitle:@"OK" forState:UIControlStateNormal];
        _ShouldDismissView = true;
    }
    else
    {
       
    }
    
}

-(void)onFailedToConnectToServer
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
   
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"oops! something went wrong, please try again",nil,appDelegate.LocalBundel,nil);
    _AlertLabel.textAlignment = NSTextAlignmentCenter;
    [_AlertLabel setHidden:false];
    if(appDelegate.APIFlag == REFERRAL)
    {
        _PopUpViewHeight.constant = _PopUpViewHeight.constant - _TableViewHeight.constant;
        _TableViewHeight.constant = 0;
        [_ConfirmButton setTitle:@"OK" forState:UIControlStateNormal];
        _ShouldDismissView = true;
    }
    else
    {

    }
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(BOOL)validateTextFieldDetails
{
    UITextField * TextField;
    BOOL Status = TRUE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   
    
    for(int i=0;i<_TextFieldList.count;i++)
    {
        TextField = [_TextFieldList objectAtIndex:i];
        if(TextField.text.length <= 0)
        {
            [self changeCellColor:true:i];
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
            Status = false;
        }
    }
    
    if(Status)
    {
        TextField = [_TextFieldList objectAtIndex:0];
        UITextField * TextField1 = [_TextFieldList objectAtIndex:1];
        UITextField * TextField2 = [_TextFieldList objectAtIndex:2];
        
         NSString * str1 = [TextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [TextField.text length])];
        
         NSString * str2 = [TextField1.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [TextField1.text length])];
        
         NSString * str3 = [TextField2.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [TextField2.text length])];
        
        
        
        
        if(![SIXDStylist isEtisalatValidNumber:TextField.text]) // checking with local Regular expr for etisalat num
        {
            [self changeCellColor:true:0];
            Status =false;
        }
        
        if(![SIXDStylist isEtisalatValidNumber:TextField1.text]) // checking with local Regular expr for etisalat num
        {
            [self changeCellColor:true:1];
            Status =false;
        }
        
        if(![SIXDStylist isEtisalatValidNumber:TextField2.text]) // checking with local Regular expr for etisalat num
        {
            [self changeCellColor:true:2];
            Status =false;
        }
        
        if(!Status)
        {
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
        }
        
        if(Status)
        {
            
           
            NSString *Aparty = [appDelegate.MobileNumber substringFromIndex:1];
            if([str1 rangeOfString:Aparty].location != NSNotFound )
            {
                [self changeCellColor:true:0];
                Status =false;
            }
            if([str2 rangeOfString:Aparty].location != NSNotFound )
            {
                [self changeCellColor:true:1];
                Status =false;
            }
            if([str3 rangeOfString:Aparty].location != NSNotFound )
            {
                [self changeCellColor:true:2];
                Status =false;
            }
            
            if(!Status)
            {
                 _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"*don’t tell yourself tell a friend",nil,appDelegate.LocalBundel,nil);
                [_AlertLabel setHidden:false];
            }
        //end
        }
    
        if(Status)
        {
            if([str1 isEqualToString:str2])
            {
                [self changeCellColor:true:0];
                [self changeCellColor:true:1];
                Status =false;
            }
            if([str1 isEqualToString:str3])
            {
                [self changeCellColor:true:0];
                [self changeCellColor:true:2];
                Status =false;
            }
            if([str2 isEqualToString:str3])
            {
                [self changeCellColor:true:1];
                [self changeCellColor:true:2];
                Status =false;
            }
        
            if(!Status)
            {
                _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"Please enter different numbers",nil,appDelegate.LocalBundel,nil);
                [_AlertLabel setHidden:false];
            }
        }
        
    }
    
    return Status;

}


-(void)changeCellColor:(BOOL)Error :(int)index
{
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:index inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if(Error)
    {
        [SIXDStylist setTextViewAlert:cell.FirstTextField :cell.SubView :cell.AccessoryButton];
        
    }
    else
    {

        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        [SIXDStylist setTextViewNormal:cell.FirstTextField :cell.SubView :nil];
    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length == 5 && string.length == 0 && [textField.text isEqualToString:@"+971-"])
    {
        return NO;
    }
    else if(textField.text.length == 15 && string.length == 1)
    {
        return NO;
    }
    else
        return YES;
}



@end
