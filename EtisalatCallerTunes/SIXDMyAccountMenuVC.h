//
//  SIXDMyAccountMenuVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/22/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "SIXDCommonPopUp_Text.h"

@interface SIXDMyAccountMenuVC : ViewController <UITableViewDelegate,UITableViewDataSource>

@property(strong,nonatomic) NSArray *MenuList;
@property(strong,nonatomic) NSArray *MenuImageList;
@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property(strong,nonatomic) SIXDCommonPopUp_Text *Common_TextVC;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@end
