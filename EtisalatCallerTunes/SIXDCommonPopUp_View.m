//
//  SIXDCommonPopUp_View.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/15/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDCommonPopUp_View.h"
#import "SIXDHeader.h"

@interface SIXDCommonPopUp_View ()

@end

@implementation SIXDCommonPopUp_View

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    _AlertLabel.text = _AlertMessage ;
    
    [_AlertButton setImage:[UIImage imageNamed:_AlertImageName] forState:UIControlStateNormal];
    
    [_ConfirmButton setTitle:_ConfirmButtonText forState:UIControlStateNormal];
    _AlertTitle.text = self.title;
    if(_IsErrorFlag)
    {
        _AlertView.backgroundColor = [UIColor orangeColor];
    }
    else
    {
        _AlertView.backgroundColor = UIColorFromRGB(CLR_PLACEHOLDER);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSelectedConfirmButton:(UIButton *)sender
{
    if(_ParentView)
    {
        [_ParentView onSelectedPopUpConfirmButton];
    }
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)onSelectedCloseButton:(UIButton *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}
@end
