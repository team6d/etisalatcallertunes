//
//  SIXDSearchVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 28/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDSearchVC.h"
#import "SIXDSongCatcherVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"

@interface SIXDSearchVC ()

@end

@implementation SIXDSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Search_EnterSeachKeyword"];
    
    _IsRefreshRequired = TRUE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _MainBtn_xPosition.constant = 0;
        _YAlignmentToClickToCatchButton.constant = 32;
    }
    else if(appDelegate.DeviceSize == iPHONE_X)
    {
        _MainBtn_xPosition.constant = -125;
        _YAlignmentToClickToCatchButton.constant = -90;
    }
    
    CGSize size = CGSizeMake(self.view.frame.size.width,self.TopView.frame.size.height);
    UIGraphicsBeginImageContext(size);
    [[UIImage imageNamed:@"search1"] drawInRect:CGRectMake(_TopView.frame.origin.x,_TopView.frame.origin.x, size.width, size.height)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.TopView.backgroundColor = [UIColor colorWithPatternImage:image];
    _SongCatcherButton.layer.cornerRadius = _SongCatcherButton.frame.size.width/2;
    _SongCatcherButton.clipsToBounds = YES;
    
    [_SongCatcherButton setImage:[UIImage imageNamed:@"clicktocatch"] forState:UIControlStateNormal];
    
    
        _SearchBar.delegate=self;
        _SearchBar.enablesReturnKeyAutomatically = NO;
        
        _ClickToCatchLabel.text=NSLocalizedStringFromTableInBundle(@"CLICK TO CATCH",nil,appDelegate.LocalBundel,nil);
        
        _HeadingLabel.text=NSLocalizedStringFromTableInBundle(@"TYPE IT, SAY IT OR CATCH IT",nil,appDelegate.LocalBundel,nil);
        
    _SearchResultsLabel.text=NSLocalizedStringFromTableInBundle(@"SEARCH_RESULTS",nil,appDelegate.LocalBundel,nil);
        
    UITextField *searchField = [_SearchBar valueForKey:@"searchField"];
    
    [searchField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEARCH_PLACEHOLDER_TEXT",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_TEXTFIELDLABEL),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11]}]];
    [searchField setFont:[UIFont fontWithName:@"ArialMT" size:11]];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [searchField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEARCH_PLACEHOLDER_TEXT",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_TEXTFIELDLABEL),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11]}]];
        [searchField setFont:[UIFont fontWithName:@"ArialMT" size:11]];
    }
    else
    {
        [searchField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"SEARCH_PLACEHOLDER_TEXT",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_TEXTFIELDLABEL),NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:11]}]];
        [searchField setFont:[UIFont fontWithName:@"GESSTextLight-Light" size:11]];
    }
     */
    searchField.backgroundColor = [UIColor clearColor];
    
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"TUNES",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"ARTISTS",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    
    //adding lines for selected tab
    Temp = [array objectAtIndex:0];
    UILabel *Line;
    Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+49, 11, 1, 7)];
    /*
    if(appDelegate.DeviceSize == iPHONE_X || appDelegate.DeviceSize == SMALL)
    {
        
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+47, 11, 1, 7)];
    }
    else
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 12, 1, 6)];
     
     */
    
    Line.backgroundColor = [UIColor grayColor];
    [_SegmentTab insertSubview:Line aboveSubview:Temp];
    
    [self InitialCondition];
    
    float yPosition = 217;
    _AlertView = [SIXDGlobalViewController  showDefaultAlertMessage:NSLocalizedStringFromTableInBundle(@"oops! no match found",nil,appDelegate.LocalBundel,nil) :yPosition];
    [self.view addSubview:_AlertView];
    [_AlertView setHidden:true];
    
}

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender {
    switch ([sender selectedSegmentIndex])
    {
        case 0:
            [SIXDStylist setGAScreenTracker:@"Search_Results_Tunes"];
            NSLog(@"switch0");
            _typeFlag=0;
            
            if(_TunesVC.toneList.count > 0)
            {
                [_AlertView setHidden:true];
            }
            else
            {
                [_AlertView setHidden:false];
            }
            
            [self.view bringSubviewToFront:_AlertView];
            [_TunesVC.TableView reloadData];
            _TunesContainer.hidden=NO;
            _ArtistContainer.hidden=YES;
            
            break;
        case 1:
            [SIXDStylist setGAScreenTracker:@"Search_Results_Artists"];
            NSLog(@"switch1");
            _typeFlag=1;
            if(_ArtistsVC.ArtistData.count > 0)
            {
                [_AlertView setHidden:true];
            }
            else
            {
                [_AlertView setHidden:false];
            }
            [self.view bringSubviewToFront:_AlertView];
            [_ArtistsVC.TableView reloadData];
            _TunesContainer.hidden=YES;
            _ArtistContainer.hidden=NO;

            break;
            
        default:
            NSLog(@"switch default statement for segment control");
            
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(searchBar.text.length>0)
    {
        NSLog(@"searchBar has a valid text");
        _SongCatcherButton.hidden=YES;
        _SegmentTab.hidden=NO;
        _SegmentView.hidden=NO;
        _typeFlag=0;
        _TunesContainer.hidden=NO;
        _ArtistContainer.hidden=YES;
        _HeadingLabel.hidden=YES;
        _ClickToCatchLabel.hidden=YES;
        [searchBar resignFirstResponder];
        
        _BannerImageHeight.constant = 145;
        _TopSpaceToSearchBar.constant = 40;
    
        _SearchResultsLabel.text=[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"RESULTS_FOR",nil,appDelegate.LocalBundel,nil),[searchBar.text uppercaseString]];
        
        [_AlertView setHidden:true];
        NSLog(@"API_CDPSpecificLanguageSearch call>>");
        [self API_CDPSpecificLanguageSearch];
    }
    else
    {
        [searchBar resignFirstResponder];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"TunesVC"])
    {
        _TunesVC = (SIXDTuneSearchVC *) [segue destinationViewController];
        [self addChildViewController:_TunesVC];
    }
    else if ([segueName isEqualToString: @"ArtistsVC"])
    {
        _ArtistsVC = (SIXDArtistSearchVC *) [segue destinationViewController];
        [self addChildViewController:_ArtistsVC];
    }
}

- (IBAction)onClickedSongCatcher:(UIButton *)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDSongCatcherVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSongCatcherVC"];
    ViewController.SearchVCObj = self;
    [self presentViewController:ViewController animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear >> Search..");
    if(_IsRefreshRequired)
        [self InitialCondition];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem_MainNavigationPage:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
       NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
    self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"SEARCH",nil,appDelegate.LocalBundel,nil);
    
}

-(void)InitialCondition
{
    
    //set initial height of banner image
    _BannerImageHeight.constant = 292;
    _TopSpaceToSearchBar.constant = 94;
    //end
    
    _SegmentView.hidden=YES;
    _TunesContainer.hidden=YES;
    _ArtistContainer.hidden=YES;
    
    _SearchBar.text = @"";
    
    [_TunesVC.toneList removeAllObjects];
    [_ArtistsVC.ArtistData removeAllObjects];
    [_TunesVC.TableView reloadData];
    [_ArtistsVC.TableView reloadData];
    
    
    [_SongCatcherButton setHidden:false];
    [_ClickToCatchLabel setHidden:false];
    [_HeadingLabel setHidden:false];
    
    [_AlertView setHidden:true];
    
    [self.view bringSubviewToFront:_SongCatcherButton];
    [self.view bringSubviewToFront:_ClickToCatchLabel];
    
}

-(void)API_CDPSpecificLanguageSearch
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = CDP_SEARCH_NORMAL;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/specific-search-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=0&pageNo=1&perPageCount=%@&toneId=0&sortBy=Order_By",URL,appDelegate.Language,[appDelegate.AppSettings objectForKey:@"FETCH_LIMIT"]];
    
    //NSString *URL = [NSString stringWithFormat:@"%@/crbt/cdp-specific-language-search",appDelegate.BaseMiddlewareURL];
    //ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=0&pageNo=1&perPageCount=%@&toneId=0&searchLanguage=%@",URL,appDelegate.Language,[appDelegate.AppSettings objectForKey:@"FETCH_LIMIT"],appDelegate.Language];
    
    NSString *SearchKey=_SearchBar.text;

    [ReqHandler sendHttpGETRequest:self :SearchKey];
    
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
   
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    NSLog(@"CDPSpecificLanguageSearch Response = %@",Response);
    [_SegmentTab setSelectedSegmentIndex:0];
    
    _IsRefreshRequired = FALSE;
    
    
    NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
    //_TunesVC.toneList = [[responseMap objectForKey:@"toneList"]mutableCopy];
    _TunesVC.toneList = [[responseMap objectForKey:@"songList"]mutableCopy];
    _typeFlag=0;
    
    _TunesVC.PageNumber =_TunesVC.toneList.count;
    _TunesVC.TempResponseCount = _TunesVC.toneList.count;
    _TunesVC.SearchKey = _SearchBar.text;
    [_TunesVC.TableView setContentOffset:CGPointZero];
    [_TunesVC.TableView reloadData];
    
    //added to change scroll position to first cell in tableview
    if(_TunesVC.toneList.count > 0)
    {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:0];
       
        [_TunesVC.TableView scrollToRowAtIndexPath:nextItem atScrollPosition:UITableViewScrollPositionTop animated:false];
        
    }
    //end

    NSMutableDictionary *countList = [responseMap objectForKey:@"countList"];
    _ArtistsVC.ArtistData = [[countList objectForKey:@"artistDetailList"]mutableCopy];
    _ArtistsVC.PageNumber =_ArtistsVC.ArtistData.count;
    _ArtistsVC.TempResponseCount = _ArtistsVC.ArtistData.count;
    _ArtistsVC.SearchKey = _SearchBar.text;
    [_ArtistsVC.TableView reloadData];
    
    //showing artist tab if tone list is empty
    if(_TunesVC.toneList.count == 0 && _ArtistsVC.ArtistData.count > 0)
    {
        _typeFlag=1;
        _TunesContainer.hidden=YES;
        _ArtistContainer.hidden=NO;
         [_SegmentTab setSelectedSegmentIndex:1];
    }
    
    if((_TunesVC.toneList.count <= 0) && (_ArtistsVC.ArtistData.count <= 0))
    {
        [_AlertView setHidden:false];
        [self.view bringSubviewToFront:_AlertView];
    }
   

}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [_AlertView setHidden:false];
    
}

-(void)onFailedToConnectToServer
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}
- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}



@end
