//
//  SIXDDatePickerPopUpVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 27/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDSettingsTimeVC.h"
#import "FSCalendar.h"
#import "CustomLabel.h"

@interface SIXDDatePickerPopUpVC : ViewController <FSCalendarDelegate, FSCalendarDataSource>
@property (weak, nonatomic) IBOutlet UIDatePicker *DatePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *EndDatePicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
@property (weak, nonatomic) IBOutlet UIButton *SaveButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;
- (IBAction)onClickedCloseButton:(id)sender;
- (IBAction)onClickedSaveButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *SubView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;

@property (weak, nonatomic) IBOutlet FSCalendar *fsCalendar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TimePickerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BottomSpaceToSaveButton;

@property (weak, nonatomic) IBOutlet FSCalendar *fsCalendarEnd;
@property (weak, nonatomic) IBOutlet SIXDAlertLabel *AlertLabel;

@property(strong,nonatomic) SIXDSettingsTimeVC *TimeVC;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) UITextField *TextField;
@property (weak, nonatomic) UITextField *EndTextField;
@property (weak, nonatomic) UIToolbar *ToolBar;
@property (nonatomic) int typeFlag;

//@property (weak,nonatomic)NSString *fromDate;
//@property (weak,nonatomic)NSString *toDate;

@property (weak,nonatomic)NSDate *fromDate;
@property (weak,nonatomic)NSDate *toDate;

@property (strong, nonatomic) IBOutlet UILabel *FirstLabel;
@property (strong, nonatomic) IBOutlet UILabel *SecondLabel;
@end
