//
//  SIXDSocialSharePopUpVC.m
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 25/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSocialSharePopUpVC.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
//#import "Social/Social.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <TwitterKit/TWTRComposer.h>
#import <TwitterKit/TWTRKit.h>
#import "SIXDStylist.h"

@import Firebase;
//@import FirebaseDynamicLinks;


@interface SIXDSocialSharePopUpVC ()

@end

@implementation SIXDSocialSharePopUpVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //[self logoutTwitter];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _SubView.backgroundColor = [UIColor whiteColor];
    [self.view setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.95]];
    
    _HeaderLabel.text = NSLocalizedStringFromTableInBundle(@"SHARE",nil,appDelegate.LocalBundel,nil);
    if(_Source == PLAYLIST) // playlist
    {
        _TopLabel.text = [SIXDGlobalViewController CapitalisationConversion:[_ToneDetails objectForKey:@"channelName"]];
    }
    else
    {
        NSString *String1 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"artistName"]];
        _TopLabel.text = [String1 capitalizedString];
        NSString *String2 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"toneName"]];
        _BottomLabel.text = [String2 capitalizedString];
    }
    
    
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    _AlertLabel.text = @"";
    
    
    NSURL * whatsappURL = [NSURL URLWithString:@"whatsapp://"];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        if([appDelegate.Language isEqualToString:@"Arabic"])
        {
            _XpositionofFacebookButton.constant = 20+38;
        }
    }
    else
    {
         NSLog(@"WhatsApp is not installed");
        _WhatsAppButton.hidden = true;
        if([appDelegate.Language isEqualToString:@"Arabic"])
        {
            _XpositionofFacebookButton.constant = 20;
        }
        else
        {
            _XpositionofFacebookButton.constant = _XpositionofFacebookButton.constant + 38;
        }
    }
    
    UIImage* image = [[UIImage imageNamed:@"buypopupclose"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_CloseButton setImage:image forState:UIControlStateNormal];
    _CloseButton.tintColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    
    [_AlertLabel setHidden:YES];
   
}

-(void)sendGAEvent: (int) Type
{
    NSString *toneId = [_ToneDetails objectForKey:@"toneId"];
    if(toneId.length <= 0)
    {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    
    NSString *GAEventDetails;
    if (_BottomLabel.text.length <= 0)
    {
        GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_TopLabel.text,_TopLabel.text,toneId];
    }
    else
    {
        GAEventDetails  = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,toneId];
    }
    
    switch (Type)
    {
        case FACEBOOK:
             [SIXDStylist setGAEventTracker:@"Social media share" :@"Facebook share click" :GAEventDetails];
            break;
        
        case TWITTER:
             [SIXDStylist setGAEventTracker:@"Social media share" :@"Twitter share click" :GAEventDetails];
            break;
            
        default:
            break;
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickedCloseButton:(UIButton *)sender
{
    NSLog(@"Dismissed modal view");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}



- (IBAction)onClickShareOnFacebook:(id)sender {
    
    [self sendGAEvent:FACEBOOK];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.OpenURL_SDK_Type = FACEBOOK;
   
    
     FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    // content.contentURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    
    NSString *msg = [self constructURL];
     content.contentURL = [NSURL URLWithString:msg];
     
     //content.contentURL = [NSURL URLWithString:@"http://developers.facebook.com"];
    
     FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
     dialog.fromViewController = self;
     dialog.shareContent = content;
     dialog.mode = FBSDKShareDialogModeNative;//FBSDKShareDialogModeFeedWeb;
    // [dialog show];
     
     
     [FBSDKShareDialog showFromViewController:self
                                 withContent:content delegate:nil];
     
    
}

-(BOOL)isFacebookAppInstalled {
    static NSString *const canOpenFacebookURL = @"fbauth2";
    NSURLComponents *components = [[NSURLComponents alloc] init];
    components.scheme = canOpenFacebookURL;
    components.path = @"/";
    return [[UIApplication sharedApplication]
            canOpenURL:components.URL];
}


- (IBAction)onClickShareOnTwitter:(id)sender {
    
    [self sendGAEvent:TWITTER];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.OpenURL_SDK_Type = TWITTER;
    NSLog(@"share on twitter");
    [self shareOnTwitter2];
}

- (IBAction)onClickedShareOnWhatsApp:(id)sender
{
    NSString *msg = [self constructURL];
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"&"
                                   ].invertedSet;
    msg = [msg
                stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                ];
    
    NSString *postdata = @"";
    
    if(_Source == PLAYLIST) // playlist
    {
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
        
    }
    else if (_Source == SALATI)
    {
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
    }
    else
    {
        postdata = [NSString stringWithFormat:@"Play %@ by %@ to your callers with #Etisalat #Callertunes",_BottomLabel.text, _TopLabel.text];
    }
    
   
    
    NSString *username = @"o_5u6jrocchb";
    NSString *apiKey = @"R_ad4116aacb0648c6bb1af33db4866efb";
   
    NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.bit.ly/v3/shorten?login=%@&apikey=%@&longUrl=%@&format=txt", username, apiKey, msg]] encoding:NSUTF8StringEncoding error:nil];
   
    NSLog(@"shortURL from bitly = %@",shortURL);
    
    postdata = [NSString stringWithFormat:@"%@ Discover it here %@",postdata,shortURL];
    
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",postdata];
    
    NSLog(@"urlWhats = %@",urlWhats);
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL:whatsappURL options:@{} completionHandler:nil];
    }
    else
    {
        // Cannot open whatsapp
        _AlertLabel.text = @"Unable to open Whatsapp";
        [_AlertLabel setHidden:false];
    }
    
}

-(NSString*)constructURL
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *language = @"en";
    NSString *ToneID = [_ToneDetails objectForKey:@"toneCode"];
    if(ToneID.length <= 0)
    {
        ToneID = [_ToneDetails objectForKey:@"toneId"];
    }
    if(![appDelegate.Language isEqualToString:@"English"])
    {
        language = @"ar";
    }
    
    NSString *msg = @"";
    switch (_Source)
    {
        case PLAYLIST:
            msg =[NSString stringWithFormat:@"https://callertunes.etisalat.ae/PopulateSongs/Playlist?action=loadSelectedPlayListPage&toneCode=%@&request_locale=%@",ToneID,language];
            break;
            
        case SALATI:
            msg =[NSString stringWithFormat:@"https://callertunes.etisalat.ae/PopulateSongs/SalatiPage?action=loadSalatiPage&request_locale=%@",language];
            break;
            
        case TUNE:
            msg =[NSString stringWithFormat:@"https://callertunes.etisalat.ae/PopulateSongs/AllTune?searchKey=%@&bannerTypeId=3&request_locale=%@",ToneID,language];
            break;
            
        default:
            break;
    }
    
    return msg;
    
}

- (void)shareOnTwitter2
{
    //[self logoutTwitter];
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *postdata;
    
    if(_Source == PLAYLIST) // playlist
    {
        
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
       
    }
    else if (_Source == SALATI)
    {
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
    }
    else
    {
        postdata = [NSString stringWithFormat:@"Play %@ by %@ to your callers with #Etisalat #Callertunes",_BottomLabel.text, _TopLabel.text];
    }

    postdata = [NSString stringWithFormat:@"%@ Discover it here %@",postdata,[self constructURL]];
    
    NSLog(@"postdata = %@",postdata);
    
    [composer setText:postdata];
    //[composer setImage:_ToneImage.image];
    
    if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers])
    {
        
        TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
        [self.presentingViewController presentViewController:composer animated:YES completion:nil];
         [self shareOnTwitter];
    }
    else
    {
        
        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
            if (session)
            {
                NSLog(@"signed in as %@",[session userName]);
                appDelegate.OpenURL_SDK_Type = TWITTER;
                [self shareOnTwitter2];
               
            }
            else
            {
                NSLog(@"error %@",[error localizedDescription]);
               
            }
        }];
    }
}


- (void)shareOnTwitter
{
    // [self logoutTwitter];
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
      NSString *postdata;
    
    if(_Source == PLAYLIST) // playlist
    {
        
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
        
    }
    else if (_Source == SALATI)
    {
        postdata = [NSString stringWithFormat:@"Play %@ to your callers with #Etisalat #Callertunes", _TopLabel.text];
    }
    else
    {
        postdata = [NSString stringWithFormat:@"Play %@ by %@ to your callers with #Etisalat #Callertunes",_BottomLabel.text, _TopLabel.text];
    }
    
    postdata = [NSString stringWithFormat:@"%@ Discover it here %@",postdata,[self constructURL]];
    
    NSLog(@"postdata = %@",postdata);
    
    [composer setText:postdata];
   // [composer setImage:_ToneImage.image];
   // [composer setURL:[NSURL URLWithString:[_ToneDetails objectForKey:@"toneUrl"]]];
    NSLog(@"share on twitter");
    
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled)
        {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}




- (void)logoutTwitter
{
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    
    [store logOutUserID:userID];
}

-(BOOL)isTwitterAppInstalled
{
    static NSString *const canOpenTwitterURL = @"twitter";
    NSURLComponents *components = [[NSURLComponents alloc] init];
    components.scheme = canOpenTwitterURL;
    components.path = @"/";
    return [[UIApplication sharedApplication]
            canOpenURL:components.URL];
}
@end
