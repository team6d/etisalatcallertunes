//
//  SIXDHomePageVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDHomePageVC.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDPageViewController.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDStylist.h"
#import "SIXDPlayerVC.h"
#import "EtisalatCallerTunes-Swift.h"

@interface SIXDHomePageVC ()

@end

@implementation SIXDHomePageVC{
    NSTimer *m_Timer;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    _IsLoaded = false;
    
    [SIXDStylist setGAScreenTracker:@"View_HomePage"];
    
    _IsRefreshing =false;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.HomeObj=self;
    _CanRefresh = true;
    _ScrollView.delegate = self;
    _ScrollView.alwaysBounceVertical = true;
    _refreshControl = [[UIRefreshControl alloc]init];
    [_ScrollView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventAllEvents];
    CategorySubscription *categorySubscription = [[CategorySubscription alloc]init];
    [categorySubscription getProfileDetail];
    [TopicSubscription subscribeTopicWithTopic:@"promotion"];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedLanguage"] isEqualToString:@"en"])
    {
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_logo"]];
        [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    }
    else
    {
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_logo"]];
        [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    }
    

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    NSLog(@"GetSettings call >>");
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    [self updateUserTuneLikeList];
    
}


-(void)refreshData
{
    NSLog(@"refreshData>>>");
    
    // set IsRefreshing flag only on action triggered from UIRefreshControl
    if(_IsLoaded)
    {
        _IsRefreshing = true;
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpiryTime"];
    NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
    double ExpiryTime = [str doubleValue];
    NSLog(@"ExpiryTime = %f str = %@",ExpiryTime,str);
    if(Time >= ExpiryTime)
    {
        [self.tabBarController.view addSubview:self.overlayview];
        [_ActivityIndicator startAnimating];
        
        SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
        [self.view bringSubviewToFront:_ActivityIndicator];
        appDelegate.APIFlag = REGENERATETOKEN;
        ReqHandler.HttpsReqType = REGENERATETOKEN;
        ReqHandler.ParentView = self;
        [ReqHandler regenerateToken];
        
    }
    else
    {       NSLog(@"loadHomePage after refersh control>>>");
            [self loadHomePage];
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear SIXDHomePageVC >>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.HomeObj=self;
    [self.navigationController.navigationBar setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    if(!_IsLoaded && !appDelegate.IsAppOpenedFromURL && appDelegate.AppSettings.count > 0)
    {
        NSLog(@"refreshData>>> is being called...");
        [self refreshData];
    }

}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear SIXDHomePageVC >>>");
    
    if(![self authoriseApp])
    {
        [self LogoutSession];
        return;
        
    }
    
    
}

-(void)LogoutSession
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    /*
     UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     UIViewController *ViewController ;
     ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSignInVC"];
     [_ParentView presentViewController:ViewController animated:YES completion:nil];
     */
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    for (UINavigationController *Temp in VCArray)
    {
        [Temp popToRootViewControllerAnimated:false];
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
    [appDelegate.window.rootViewController.view removeFromSuperview];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:ViewController];
    navController.navigationBarHidden = YES;
    appDelegate.window.rootViewController = navController;
    [appDelegate.window makeKeyAndVisible];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)preferredContentSizeDidChangeForChildContentContainer:(id<UIContentContainer>)container
{
NSLog(@"preferredContentSizeDidChangeForChildContentContainer>>>>");
    
    NSString *str= [NSString stringWithFormat:@"%@",container];
    NSLog(@"container info = %@",container);
    NSRange match = [str rangeOfString:@"CategoryListVC"];
    /*
     NSRange match = [str rangeOfString:@"SIXDListTonesVC"];
    if(match.location!=NSNotFound)
    {
        
        [UIView animateWithDuration:1.0 animations:^{
            
            _SongListContainer.frame = _ListTonesHorizontalVC.TableView.frame ;
            _ListToneFirstContainerHeight.constant = _ListTonesHorizontalVC.TableView.frame.size.height;
            
            _ListTonesHorizontalVC.view.frame = CGRectMake(0, 0,_SongListContainer.frame.size.width,_SongListContainer.frame.size.height);
            
            [_SongListContainer layoutSubviews];
            
            _SongListContainer2.frame = _ListTonesVerticalVC.TableView.frame ;
            _ListToneSecondContainerHeight.constant = _ListTonesVerticalVC.TableView.frame.size.height;
            
            _ListTonesVerticalVC.view.frame = CGRectMake(0, 0,_SongListContainer2.frame.size.width,_SongListContainer2.frame.size.height);
            
            [_SongListContainer2 layoutSubviews];
            
            
            NSLog(@"height 1 = %f",_ListTonesHorizontalVC.view.frame.size.height);
             } completion:nil];
        return;
    }
     */
    /*
    match = [str rangeOfString:@"SIXDTopChartsVC"];
    if(match.location!=NSNotFound)
    {
        
        [UIView animateWithDuration:1.0 animations:^{
                
                _TopChartContainer.frame = _TopChartsVC.view.frame ;
                
               // _TopChartViewHeight.constant = _TopChartsVC.view.frame.size.height;
            
                _TopChartViewHeight.constant = 423;
                
                _TopChartsVC.view.frame = CGRectMake(0, 0,_TopChartContainer.frame.size.width,_TopChartViewHeight.constant);
                [_TopChartContainer layoutSubviews];
                NSLog(@"height 2 = %f",_TopChartsVC.view.frame.size.height);
                
            } completion:nil];
            
        return;
    }
     */
    if(match.location!=NSNotFound)
    {
        
        [UIView animateWithDuration:1.0 animations:^{
            
            _CategoryContainer.frame = _CategoryVC.TableView.frame ;
            
            _CategoryViewHeight.constant = _CategoryVC.TableView.frame.size.height;
            
            _CategoryVC.view.frame = CGRectMake(0, 0,_CategoryContainer.frame.size.width,_CategoryViewHeight.constant);
            [_CategoryContainer layoutSubviews];
            NSLog(@"height 3 = %f",_CategoryVC.view.frame.size.height);
            
        } completion:nil];
        
        return;
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"BannerVC"])
    {
        _BannerVC = (SIXDPageViewController *) [segue destinationViewController];
        //[self addChildViewController:_BannerVC];
    }
    else if ([segueName isEqualToString: @"ListToneHorizontal"])
    {
        _ListTonesHorizontalVC = (SIXDListTonesVC *) [segue destinationViewController];
        //[self addChildViewController:_ListTonesHorizontalVC];
    }
    else if ([segueName isEqualToString: @"ListToneVertical"])
    {
        _ListTonesVerticalVC = (SIXDListTonesVC *) [segue destinationViewController];
        // [self addChildViewController:_ListTonesVerticalVC];
    }
    else if ([segueName isEqualToString: @"SIXDTopChartsVC"])
    {
        _TopChartsVC = (SIXDTopChartsVC *) [segue destinationViewController];
        // [self addChildViewController:_TopChartsVC];
    }
    else if ([segueName isEqualToString: @"CategoryListVC"])
    {
        _CategoryVC = (CategoryListVC *) [segue destinationViewController];
        // [self addChildViewController:_CategoryVC];
    }
}

//API
-(void)GetSettings
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = SETTINGS;
    NSLog(@"Inside GetSettings API >>");
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/settings?msisdn=%@&language=%@",appDelegate.BaseMiddlewareURL,appDelegate.MobileNumber,appDelegate.Language];
    NSLog(@"GetSettings final URL = %@",ReqHandler.FinalURL);
    ReqHandler.HttpsReqType = GET;
    [ReqHandler sendHttpGETRequest:(ViewController*)self :nil];
}

//DECODING
-(void)onFailureResponseRecived:(NSMutableDictionary*)Response
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag == UPDATE_TUNE_LIKE_LIST)
    {
        [self loadHomePage];
    }
    else
    {

    }
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    
    NSLog(@"SIXDHomePageVC onFailedToConnectToServer");
}

-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"SIXDHomePageVC onSuccessResponseRecived Reponse = %@",Response);
    
    if(appDelegate.APIFlag == SETTINGS)
    {
       
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpiryTime"];
        NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
        double ExpiryTime = [str doubleValue];
        NSLog(@"ExpiryTime = %f str = %@",ExpiryTime,str);
        
        [self getAppSettings:Response];
        
        if(appDelegate.IsAppOpenedFromURL)
        {
            if([appDelegate.ToneID isEqualToString:@"0"])
            {
                appDelegate.MainTabController.selectedIndex = 2;
                appDelegate.IsAppOpenedFromURL = false;
                return;
            }
            else
            {
                UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *ViewController;
                NSLog(@"Tune id in home page = %@",appDelegate.ToneID);
                if(appDelegate.CategoryId == TUNE)
                {
                    ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
                }
                else
                {
                     ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMusicChannelVC"];
                }
                [self.navigationController pushViewController:ViewController animated:YES];
                return;
            }
               
               
            
        }
        
        if(Time >= ExpiryTime)
        {
            [self.tabBarController.view addSubview:self.overlayview];
            [_ActivityIndicator startAnimating];
            [self.view bringSubviewToFront:_ActivityIndicator];
            
            SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
            
            appDelegate.APIFlag = REGENERATETOKEN;
            ReqHandler.HttpsReqType = REGENERATETOKEN;
            ReqHandler.ParentView = self;
            [ReqHandler regenerateToken];
            
        }
        else
        {
            [self loadHomePage];
        }
    }
    else if(appDelegate.APIFlag == GET_PACK_DETAILS)
    {
        
        Response = [Response objectForKey:@"responseMap"];
        NSMutableDictionary *Dict = [[Response objectForKey:@"packStatusDetails"]mutableCopy];
        NSString *PackName = [Dict objectForKey:@"packName"];
        if(PackName.length > 0)
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PACK_NAME"];
            [[NSUserDefaults standardUserDefaults] setObject:PackName forKey:@"PACK_NAME"];
            [[NSUserDefaults standardUserDefaults] synchronize];
           
        }
        
    }
    else
    {
        [_overlayview removeFromSuperview];
        [_ActivityIndicator stopAnimating];
        [self loadHomePage];
    }
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (void) updateUserTuneLikeList
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = UPDATE_TUNE_LIKE_LIST;
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    [likeSettingsSession firstLikeTunesDownloadRequest];
}

- (void) backgroundUpdateUserTuneList // as of now timer to run this thread is blocked 04-04-2018
{
    NSLog(@"backgroundUpdateUserTuneList");
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    likeSettingsSession.Source = SRC_PERIODIC;
    [likeSettingsSession sendTuneListAndDownload];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response :(NSString*)URL
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    if(appDelegate.APIFlag == REGENERATETOKEN)
    {
        [self loadHomePage];
        
    }
    
}

-(void)getAppSettings:(NSMutableDictionary*) Response
{
    NSLog(@"getAppSettings Response =%@",Response);
    
    
    NSLog(@"decodeSettingsAPIResponse>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *Key ;
    NSString *Value;
    NSMutableDictionary *Value1 = [NSMutableDictionary new];
    
    appDelegate.AppSettings = [NSMutableDictionary new];
    
    Response = [Response objectForKey:@"responseMap"];
    Response = [Response objectForKey:@"settings"];
    NSMutableDictionary *Dict = [Response objectForKey:@"others"];
    
    
    Key = [NSString stringWithFormat:@"FAQURL_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"FAQURL"];
    
    Key = [NSString stringWithFormat:@"DefaultTonePrice_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"DefaultPrice"];
    
    
    //NAMETUNE_PRICE_ENGLISH
    
    Key = @"NAMETUNE_PRICE_ENGLISH";
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [[NSUserDefaults standardUserDefaults] setValue: Value forKey:@"NAMETUNE_PRICE_ENGLISH"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    Key = @"NAMETUNE_PRICE_ARABIC";
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [[NSUserDefaults standardUserDefaults] setValue: Value forKey:@"NAMETUNE_PRICE_ARABIC"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //[appDelegate.AppSettings setValue:Value forKey:@"FETCH_LIMIT"];
    
    
    
    
    
    Key = @"FETCH_LIMIT";
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"FETCH_LIMIT"];
    
    Key = [NSString stringWithFormat:@"STATUSRBT_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"STATUS"];
    
    Key = [NSString stringWithFormat:@"FeaturedCategory_%@",[appDelegate.Language uppercaseString]];
    NSDictionary *Dict1 = [Dict objectForKey:Key];
    Value = [Dict1 objectForKey:@"attribute"];
    NSArray *temp = [Value componentsSeparatedByString:@","];
    [appDelegate.AppSettings setValue:[temp objectAtIndex:2] forKey:@"FeaturedCategoryID"];
    [appDelegate.AppSettings setValue:[temp objectAtIndex:0] forKey:@"FeaturedCategoryNAME"];
    
    Key = [NSString stringWithFormat:@"JustForYou_%@",[appDelegate.Language uppercaseString]];
    Dict1 = [Dict objectForKey:Key];
    Value = [Dict1 objectForKey:@"attribute"];
    temp = [Value componentsSeparatedByString:@","];
    [appDelegate.AppSettings setValue:[temp objectAtIndex:2] forKey:@"JustForYouID"];
    [appDelegate.AppSettings setValue:[temp objectAtIndex:0] forKey:@"JustForYouNAME"];
    
    Key = [NSString stringWithFormat:@"PROMOTION_LIST_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    NSLog(@"PromotionlIST = %@",Value);
    if(![Value isEqualToString:@""])
        appDelegate.PromotionList = [Value componentsSeparatedByString:@"|"];
    
    Key = [NSString stringWithFormat:@"SALATI_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    temp = [Value componentsSeparatedByString:@"|"];
    [appDelegate.AppSettings setValue:temp forKey:@"SALATI"];
    
    Key = [NSString stringWithFormat:@"PLAYLIST_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"PLAYLIST_ID"];
    
    Key = [NSString stringWithFormat:@"AboutAPPURL_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"ABOUT_URL"];
    
    Key = [NSString stringWithFormat:@"WISHLIST_IMAGE_URL"];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"WISHLIST_IMAGE_URL"];
    
    
    Key = [NSString stringWithFormat:@"WISHLIST_TONE_URL"];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"WISHLIST_TONE_URL"];
    
    Key = [NSString stringWithFormat:@"DIY_TND_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"DIY_URL"];
    
    Key = [NSString stringWithFormat:@"KARAOKE_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"KARAOKE_PRICE"];
    
    Key = [NSString stringWithFormat:@"UpgradePlanPrice_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"UPGRADEPLANPRICE"];
    
    Key = [NSString stringWithFormat:@"PLAYLIST_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"PLAYLIST_PRICE"];
    
    Key = [NSString stringWithFormat:@"DEFAULT_TONE"];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"DEFAULT_TONE_ID"];
    
    Key = [NSString stringWithFormat:@"FILTER_PARAMS"];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    temp = [Value componentsSeparatedByString:@"|"];
    [appDelegate.AppSettings setValue:temp forKey:@"FILTER_PARAMS"];
    
    Key = [NSString stringWithFormat:@"PROMOTIONAL_BANNER_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    temp = [Value componentsSeparatedByString:@"|"];
    [appDelegate.AppSettings setValue:temp forKey:@"PROMOTIONAL_BANNER"];
    
    Key = [NSString stringWithFormat:@"SALATI_HEADER_INFO_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"SALATI_HEADER"];
    
    Key = [NSString stringWithFormat:@"SALATI_DESCRIPTION_INFO_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"SALATI_INFO"];
    
    
    
    
    //new PRICE_POINT_CHANGE-begin
    Key = [NSString stringWithFormat:@"DIY_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"DIY_PRICE"];
    
    Key = [NSString stringWithFormat:@"SALATI_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"SALATI_PRICE"];
    
    Key = [NSString stringWithFormat:@"MC_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"MC_PRICE"];
    
    Key = [NSString stringWithFormat:@"VIP_PRICE_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    [appDelegate.AppSettings setValue:Value forKey:@"VIP_PRICE"];
    //new PRICE_POINT_CHANGE-end
    
    
    
    
    Key = [NSString stringWithFormat:@"UPDATE_URL"];
    if([Dict objectForKey:Key])
    {
        Value1 = [Dict objectForKey:Key];
        Value = [Value1 objectForKey:@"attribute"];
        [appDelegate.AppSettings setValue:Value forKey:@"UPDATE_URL"];
    }
    
    Key = [NSString stringWithFormat:@"PACKNAME_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    NSArray *CountDict1 = [Value componentsSeparatedByString:@"|"];
    NSMutableDictionary *ContactDict1 = [NSMutableDictionary new];
    for(int i = 0; i<CountDict1.count; i++)
    {
        NSArray *Filler = [[CountDict1 objectAtIndex:i] componentsSeparatedByString:@","];
        [ContactDict1 setObject:[Filler objectAtIndex:1] forKey:[Filler objectAtIndex:0]];
    }
    [appDelegate.AppSettings setValue:ContactDict1 forKey:@"PACKNAME"];
    
    
    Key = [NSString stringWithFormat:@"VALIDITY_%@",[appDelegate.Language uppercaseString]];
    Value1 = [Dict objectForKey:Key];
    Value = [Value1 objectForKey:@"attribute"];
    NSArray *CountDict2 = [Value componentsSeparatedByString:@"|"];
    NSMutableDictionary *ContactDict2 = [NSMutableDictionary new];
    for(int i = 0; i<CountDict2.count; i++)
    {
        NSArray *Filler = [[CountDict2 objectAtIndex:i] componentsSeparatedByString:@","];
        [ContactDict2 setObject:[Filler objectAtIndex:1] forKey:[Filler objectAtIndex:0]];
    }
    [appDelegate.AppSettings setValue:ContactDict2 forKey:@"VALIDITY"];
    
    
    Key = [NSString stringWithFormat:@"TopCharts_%@",[appDelegate.Language uppercaseString]];
    Dict1 = [Dict objectForKey:Key];
    Value = [Dict1 objectForKey:@"attribute"];
    NSArray *items = [Value componentsSeparatedByString:@"|"];
    
    NSString *Identifiers = @"";
    NSString *DisplayNames = @"";
    
    if(items.count)
    {
        
        for (int i=0; i<items.count; i++)
        {
            NSString *str = [items objectAtIndex:i];
            NSArray *temp = [str componentsSeparatedByString:@","];
            if(i==items.count-1)
            {
                Identifiers = [NSString stringWithFormat:@"%@%@",Identifiers,[temp objectAtIndex:2]];
                DisplayNames = [NSString stringWithFormat:@"%@%@",DisplayNames,[temp objectAtIndex:0]];
                
            }
            else
            {
                Identifiers = [NSString stringWithFormat:@"%@%@,",Identifiers,[temp objectAtIndex:2]];
                DisplayNames = [NSString stringWithFormat:@"%@%@,",DisplayNames,[temp objectAtIndex:0]];
            }
            
            
        }
        
        [appDelegate.AppSettings setValue:Identifiers forKey:@"TopCharts_IDS"];
        [appDelegate.AppSettings setValue:DisplayNames forKey:@"TopCharts_DISPLAYNAMES"];
        
        
        NSLog(@"AppSettings = %@",appDelegate.AppSettings);
    }
}

-(void)loadHomePage
{
    NSLog(@"loadHomePage >>");
    _IsLoaded = true;
    if(_IsRefreshing)
    {
        [_BannerVC.BannerDetailsList removeAllObjects];
        [_TopChartsVC.TopPicks removeAllObjects];
        [_TopChartsVC.BestOfMonth removeAllObjects];
        [_TopChartsVC.BestOfWeek removeAllObjects];
        [_TopChartsVC.MostLiked removeAllObjects];
        [_ListTonesVerticalVC.TuneList removeAllObjects];
        [_ListTonesHorizontalVC.TuneList removeAllObjects];
        [_CategoryVC.CategoryList removeAllObjects];
        [_refreshControl endRefreshing];
        _IsRefreshing = false;
    }
    [_BannerVC loadBanner];
    [_ListTonesHorizontalVC loadTones];
    [_ListTonesVerticalVC loadTones];
    [_TopChartsVC loadTopCharts];
    [_CategoryVC loadCategories];
    [self getPackDetails];
}


-(void)getPackDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_PACK_DETAILS ;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/pack-status",appDelegate.BaseMiddlewareURL];
    //PRIORITY 0-CRBT 1-RRBT
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?msisdn=%@&priority=0&language=%@",URL,appDelegate.MobileNumber,appDelegate.Language];
    [ReqHandler sendHttpGETRequest:self :nil];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y <= -125)
    {
        if(!_IsRefreshing && _CanRefresh)
        {
            NSLog(@"Refreshing Data....");
            _CanRefresh = false;
            [self refreshData];
        }
    }
    else if (scrollView.contentOffset.y >= 0)
    {
        _CanRefresh = true;
    }
}


- (BOOL) authoriseApp
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([SIXDGlobalViewController isUnAuthorisedApp])
    {
        NSLog(@"App is unauthorised");
        NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"APP_UNAUTHORISED",nil,appDelegate.LocalBundel,nil);
        [SIXDGlobalViewController setAlert:AlertMessage :self];
        return NO;
    }
    NSLog(@"App is authorised");
    return YES;
}

/*
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self refreshData];
}
 */

/*
 -(void)onClikedFilterButton
 {
 NSLog(@"onClikedMyAccountButton>>> 11");
 UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
 
 UIViewController  *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMyAccountMenuVC"];
 
 [self.navigationController pushViewController:ViewController animated:NO];
 }
 */


@end
