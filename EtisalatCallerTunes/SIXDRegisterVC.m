//
//  SIXDRegisterVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDRegisterVC.h"
#import "SIXDStylist.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDCreateAccountVC.h"
#import "CustomLabel.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDValidations.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"

@interface SIXDRegisterVC ()

@end

@implementation SIXDRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setKeyboardCapability];
    
    [SIXDStylist setGAScreenTracker:@"Registration_Enter_User_Details"];
    
    [SIXDStylist setBackGroundImage:self.view];
    
    _SubButtonList = [NSMutableDictionary new];
    _SelectedCategoryIDs = [NSMutableArray new];
    _SelectedCategoryNames = [NSMutableDictionary new];
    _MainButtonList = [NSMutableArray new];
    
    [self.view bringSubviewToFront:_BackButton];
    
    _NumberView.layer.borderWidth = 1.0f;
    _NumberView.layer.borderColor = [UIColorFromRGB(CLR_PLACEHOLDER) CGColor];
    _NumberView.alpha=0.95;
    
    
    [_NumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_PasswordTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_ConfirmPasswordTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_EmailTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_NameTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    _NumberTextField.text = appDelegate.MobileNumber;
    
    _NumberTextField.userInteractionEnabled=FALSE;
    
    _NumberTextField.enablesReturnKeyAutomatically = NO;
    _PasswordTextField.enablesReturnKeyAutomatically = NO;
    _ConfirmPasswordTextField.enablesReturnKeyAutomatically = NO;
    _NameTextField.enablesReturnKeyAutomatically = NO;
    _EmailTextField.enablesReturnKeyAutomatically = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self InitialCondition];
    
    _TopSpaceToNumber.constant=19;
    
    _AlertLabel.hidden = YES;
    _PasswordLabel.hidden = YES;
    _ConfirmPasswordLabel.hidden = YES;
    _NameLabel.hidden = YES;
    _EmailLabel.hidden = YES;
    
    _NumberTextField.tag = 1;
    _PasswordTextField.tag = 2;
    _ConfirmPasswordTextField.tag = 3;
    _NameTextField.tag = 4;
    _EmailTextField.tag = 5;
    
    _NumberTextField.delegate = self;
    _PasswordTextField.delegate = self;
    _ConfirmPasswordTextField.delegate = self;
    _NameTextField.delegate = self;
    _EmailTextField.delegate = self;
    
    
    [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_NameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Name*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_EmailTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Email*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil);
    _PasswordLabel.text = NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil);
    _ConfirmPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil);
    _NameLabel.text = NSLocalizedStringFromTableInBundle(@"Name*",nil,appDelegate.LocalBundel,nil);
    _EmailLabel.text = NSLocalizedStringFromTableInBundle(@"Email*",nil,appDelegate.LocalBundel,nil);
    
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"CREATE MY ACCOUNT",nil,appDelegate.LocalBundel,nil);
    _ItemInfoLabel.text = NSLocalizedStringFromTableInBundle(@"REGISTER_CATEGORY_TEXT",nil,appDelegate.LocalBundel,nil);
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_NumberButton setImage:image forState:UIControlStateNormal];
    
    [_PasswordButton setImage:image forState:UIControlStateNormal];
    
    [_ConfirmPasswordButton setImage:image forState:UIControlStateNormal];
    
    [_NameButton setImage:image forState:UIControlStateNormal];
    
    [_EmailButton setImage:image forState:UIControlStateNormal];
    
    [_GetOtpButton setTitle:NSLocalizedStringFromTableInBundle(@"GET VERIFICATION CODE (OTP)",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    //-----------------------------------------------------------------//
    
    
    
    [_MainScrollView bringSubviewToFront:_ScrollView];
    [_MainScrollView bringSubviewToFront:_GetOtpButton];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _ActivityIndicator.hidden=YES;
    
}

-(void) setKeyboardCapability
{
    _PasswordTextField.keyboardType = ASCII_TXT_KEYBOARD;
    _ConfirmPasswordTextField.keyboardType = ASCII_TXT_KEYBOARD;
    _NameTextField.keyboardType = ASCII_TXT_KEYBOARD;
    _EmailTextField.keyboardType = ASCII_TXT_KEYBOARD;
}

- (void)onClickedScrollButton:(UITapGestureRecognizer*)sender
{
    UIImageView *temp= (UIImageView *)sender.view;
    
    NSLog(@"onClickedScrollButton = %ld",(long)temp.tag);
    BOOL IsSelected = true;
    
    for(int i=0;i<_SelectedCategoryIDs.count;i++)
    {
        if(temp.tag==[[_SelectedCategoryIDs objectAtIndex:i] intValue])
        {
            UIButton *SubTemp = [_SubButtonList objectForKey:[_SelectedCategoryIDs objectAtIndex:i]];
            SubTemp.hidden=YES;
            IsSelected = false;
            [_SelectedCategoryIDs removeObjectAtIndex:i];
            break;
        }
    }
    
    if(IsSelected)
    {
        NSString *tagValue = [NSString stringWithFormat:@"%ld",(long)temp.tag];
        UIButton *SubTemp = [_SubButtonList objectForKey:tagValue];
        SubTemp.hidden=NO;
        [_SelectedCategoryIDs addObject:tagValue];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_NumberTextField resignFirstResponder];
    [_PasswordTextField resignFirstResponder];
    [_ConfirmPasswordTextField resignFirstResponder];
    [_NameTextField resignFirstResponder];
    [_EmailTextField resignFirstResponder];
    
    _AlertLabel.hidden=YES;
    
    if(_PasswordTextField.text.length==0||_PasswordTextField.text.length>4)
    {
        [SIXDStylist setTextViewNormal:_PasswordTextField :_PasswordView :_PasswordButton];
    }
    if(_ConfirmPasswordTextField.text.length==0||_ConfirmPasswordTextField.text.length>4)
    {
        [SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
    }
    
    _ValidationFlag = [GlobalInterfaceForValidations fnG_ValidateNames:_NameTextField.text];
    
    if(_NameTextField.text.length==0||_ValidationFlag==TRUE)
    {
        [SIXDStylist setTextViewNormal:_NameTextField :_NameView :_NameButton];
    }
    
    _ValidationFlag = [GlobalInterfaceForValidations fn_isValidEmail:_EmailTextField.text];
    
    if(_EmailTextField.text.length==0||_ValidationFlag==TRUE)
    {
        [SIXDStylist setTextViewNormal:_EmailTextField :_EmailView :_EmailButton];
    }
}

- (IBAction)onClickedGetOtpButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    int count=7;
    _AlertLabel.text=@"";
    
    if(_PasswordTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_PasswordButton];
    }
    else
    {
        //password_condition
        /*
        _ValidationFlag = [GlobalInterfaceForValidations fnG_ValidatePassword:_PasswordTextField.text];
        if(_ValidationFlag==FALSE)
        {
            count--;
            if(_AlertLabel.text.length==0)
            {
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"password_policy",nil,appDelegate.LocalBundel,nil);
            }
            _AlertLabel.hidden=NO;
            [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_PasswordButton];
            return;
        }
        */
        
        if(_PasswordTextField.text.length<8)
        {
            count--;
            if(_AlertLabel.text.length==0)
            {
                _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"MIN_PASSWORD_LENGTH_VALIDATION",nil,appDelegate.LocalBundel,nil);
            }
            _AlertLabel.hidden=NO;
            [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_PasswordButton];
            return;
        }
    }
    if(_ConfirmPasswordTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
    }
    if(_NameTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_NameTextField :_NameView :_NameButton];
    }
    else
    {
       _ValidationFlag = [GlobalInterfaceForValidations fnG_ValidateNames:_NameTextField.text];
        if(_ValidationFlag == false)
        {
            count--;
            if(_AlertLabel.text.length==0)
                _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            _AlertLabel.hidden=NO;
            return;
        }
    }
    if(_EmailTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_EmailTextField :_EmailView :_EmailButton];
        return;
    }
    else
    {
        _ValidationFlag = [GlobalInterfaceForValidations fn_isValidEmail:_EmailTextField.text];
        if(_ValidationFlag == false)
        {
            count--;
            if(_AlertLabel.text.length==0)
                _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"INVALID_EMAIL_ID",nil,appDelegate.LocalBundel,nil);
            _AlertLabel.hidden=NO;
            
            [SIXDStylist setTextViewAlert:_EmailTextField :_EmailView :_EmailButton];
            return;
        }
    }
    
    if(_SelectedCategoryIDs.count==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"REGISTER_CHOOSE_CATGRY_ERROR",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        for(int i=0;i<_MainButtonList.count;i++)
        {
            [self setButtonBorderRed:[_MainButtonList objectAtIndex:i]];
        }
        return;
    }
    if(![_PasswordTextField.text isEqualToString:_ConfirmPasswordTextField.text])
    {
        count--;
        if(_AlertLabel.text.length==0)
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"passwords are not matching",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_PasswordTextField :_PasswordView :_PasswordButton];
        [SIXDStylist setTextViewAlert:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
        return;
    }
    if(count==7)
    {
        [SIXDStylist setGAEventTracker:@"Account creation" :@"Get verification code" :nil];
        [self InitialCondition];
        for(int i=0;i<_MainButtonList.count;i++)
        {
            [self setButtonBorderNormal:[_MainButtonList objectAtIndex:i]];
        }
        
        NSLog(@"API_GetSecurityToken call >>");
        [self API_GetSecurityToken];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(textField.tag==1)
    {
        _NumberLabel.hidden=NO;
        [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToNumber.constant=19;
    }
    else if(textField.tag==2)
    {
        _PasswordLabel.hidden=NO;
        [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToPassword.constant=19;
    }
    else if(textField.tag==3)
    {
        _ConfirmPasswordLabel.hidden=NO;
        [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToConfirmPassword.constant=19;
    }
    else if(textField.tag==4)
    {
        
        
        _NameLabel.hidden=NO;
        [_NameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToName.constant=19;
    }
    else
    {
        _EmailLabel.hidden=NO;
        [_EmailTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToEmail.constant=19;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(textField.tag==1)
    {
        if(_NumberTextField.text.length==0)
        {
            [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Number*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _NumberLabel.hidden=YES;
            _TopSpaceToNumber.constant=0;
            
            
        }
    }
    else if(textField.tag==2)
    {
        if(_PasswordTextField.text.length==0)
        {
            [_PasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _PasswordLabel.hidden=YES;
            _TopSpaceToPassword.constant=0;
        }
    }
    else if(textField.tag==3)
    {
        if(_ConfirmPasswordTextField.text.length==0)
        {
            [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _ConfirmPasswordLabel.hidden=YES;
            _TopSpaceToConfirmPassword.constant=0;
        }
    }
    else if(textField.tag==4)
    {
        if(_NameTextField.text.length==0)
        {
            [_NameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Name*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _NameLabel.hidden=YES;
            _TopSpaceToName.constant=0;
        }
    }
    else
    {
        if(_EmailTextField.text.length==0)
        {
            [_EmailTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Email*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _EmailLabel.hidden=YES;
            _TopSpaceToEmail.constant=0;
        }
    }
}


- (void)InitialCondition
{
     _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_NumberTextField :nil :_NumberButton];
    [SIXDStylist setTextViewNormal:_PasswordTextField :_PasswordView :_PasswordButton];
    [SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
    [SIXDStylist setTextViewNormal:_NameTextField :_NameView: _NameButton];
    [SIXDStylist setTextViewNormal:_EmailTextField :_EmailView : _EmailButton];
}

-(void)API_Registration
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=REGISTRATION;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    // GA hit - start
    NSString *GAHitList = @"";
    for(int i=0;i<_SelectedCategoryIDs.count;i++)
    {
        NSString *DummyString = [NSString stringWithFormat:@"%@,",[_SelectedCategoryNames objectForKey:[_SelectedCategoryIDs objectAtIndex:i]]];
        GAHitList = [NSString stringWithFormat:@"%@%@",GAHitList,DummyString];
       // GAHitList = [GAHitList stringByAppendingString:DummyString];
    }
    if ([GAHitList length] > 0) {
        GAHitList = [GAHitList substringToIndex:[GAHitList length] - 1];
    }
    
    NSLog(@"What you like GAHitList = %@",GAHitList);
    
    [SIXDStylist setGAEventTracker:@"Account creation" :@"What you like" :GAHitList];
    
    // GA hit - end
    
    
    NSString *FinalIds = @"";
    for(int i=0;i<_SelectedCategoryIDs.count;i++)
        {
            FinalIds = [FinalIds stringByAppendingString: [NSString stringWithFormat:@"%@,",[_SelectedCategoryIDs objectAtIndex:i]]];
        }
    
    if ([FinalIds length] > 0) {
        FinalIds = [FinalIds substringToIndex:[FinalIds length] - 1];
    }
    
    NSLog(@"FinalIds = %@",FinalIds);
    ReqHandler.IsURLEncodingNotRequired = true;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&encryptedPassword=%@&clientTxnId=%@&language=%@&userName=%@&email=%@&categoryId=%@&securityCounter=%@&os=ios&Source=iOS",appDelegate.MobileNumber,_EncryptedPassword,Rno,appDelegate.Language,_NameTextField.text,_EmailTextField.text,FinalIds,_SecurityToken];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/registration",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)API_GetSecurityToken
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SECURITY_TOKEN;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/security-token",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?",URL];
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==SECURITY_TOKEN)
    {
        NSLog(@"API_GetSecurityToken onSuccessResponseRecived = %@",Response);
        
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        _SecurityToken =[responseMap objectForKey:@"securityCounter"];
        
        [self EncryptPassord];
    }
    else if (appDelegate.APIFlag == GET_PREFERED_CATEGORY_STATIC_LIST)
    {
        [self loadScrollViewContent];
    }
    else
    {
        NSLog(@"API_Registration onSuccessResponseRecived = %@",Response);
        
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        _SectionToken =[responseMap objectForKey:@"secToc"];
        
        [self InitialCondition];
        for(int i=0;i<_MainButtonList.count;i++)
        {
            [self setButtonBorderNormal:[_MainButtonList objectAtIndex:i]];
        }
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCreateAccountVC"];
        SIXDCreateAccountVC *svc = (SIXDCreateAccountVC*)ViewController;
        svc.SectionToken=_SectionToken;
        appDelegate.Password = _PasswordTextField.text;
        [self presentViewController:ViewController animated:YES completion:nil];
        
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *message =[Response objectForKey:@"message"];
    [SIXDGlobalViewController setAlert:message :self];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil) :self];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)EncryptPassord
{
    NSURL *jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt" withExtension:@"js"];
    NSString *jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    NSString *oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main.js";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:nil ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt.min" withExtension:@"js"];
    jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main1.js";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    NSString *Temp = [NSString stringWithFormat:@"%@%@",_PasswordTextField.text,_SecurityToken];
    
    NSLog(@"Password = %@",Temp);
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"SearchKey" withString:Temp];
    _webView = [UIWebView new];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    _webView.delegate = self;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    /*  >> To get rid of warning <<
     NSString *str = ...; // some URL
     NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
     NSString *result = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    */
    
    NSString *pwd = [_webView stringByEvaluatingJavaScriptFromString:@"getLanguage()"];
    
    /*
    _EncryptedPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,(CFStringRef)pwd,
                                                                                               NULL,
                                                                                               (CFStringRef)@"+",
                                                                                               kCFStringEncodingUTF8 ));
     */
    NSLog(@"API_Registration call >>");
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    _EncryptedPassword = [pwd
                             stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                             ];
    
    [self API_Registration];
    
}


- (IBAction)onClickBackButton:(id)sender {
     [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder ];
    return NO;
    
}

-(void)loadScrollViewContent
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    int x = 8;
    for (int i = 0; i < appDelegate.CategoryList.count; i++)
    {
        
        UIImageView *MainButton = [[UIImageView alloc] initWithFrame:CGRectMake(x,8,60,60)];
        UIButton *SubButton = [[UIButton alloc] initWithFrame:CGRectMake(x,8,60,40)];
        CustomLabel_7 *buttonlabel = [[CustomLabel_7 alloc] initWithFrame:CGRectMake(x,40,60,20)];
        
        SubButton.userInteractionEnabled = FALSE;
    
        NSMutableDictionary *Dict = [appDelegate.CategoryList objectAtIndex:i];
        NSString * urlStr = [[Dict objectForKey:@"imageURL"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
       
        [_SelectedCategoryNames setValue:[Dict objectForKey:@"name"] forKey:[Dict objectForKey:@"id"]];
        
        [MainButton sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        
        [SubButton setImage:[UIImage imageNamed:@"loginheart"] forState:UIControlStateNormal];
        SubButton.hidden=YES;
        MainButton.tag = [[Dict objectForKey:@"id"] intValue];
        
        buttonlabel.text=NSLocalizedStringFromTableInBundle([Dict objectForKey:@"name"],nil,appDelegate.LocalBundel,nil);
        buttonlabel.textAlignment=NSTextAlignmentCenter;
        buttonlabel.textColor = [UIColor whiteColor];
        
        [_SubButtonList setObject:SubButton forKey:[Dict objectForKey:@"id"]];
        [_MainButtonList addObject:MainButton];
        
       // [MainButton addTarget:self action:@selector(onClickedScrollButton:) forControlEvents:UIControlEventTouchUpInside];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(onClickedScrollButton:)];
        [MainButton setUserInteractionEnabled:true];
        [MainButton addGestureRecognizer:tap];
        
        [_ScrollView addSubview:MainButton];
        [_ScrollView addSubview:SubButton];
        [_ScrollView addSubview:buttonlabel];
        x += MainButton.frame.size.width+10;
        
        
        //testing
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = MainButton.bounds;
        gradient.colors = @[(id)[UIColorFromRGB(CLR_PLACEHOLDER) CGColor], (id)[UIColorFromRGB(CLR_PLACEHOLDER) CGColor]];
        gradient.startPoint = CGPointMake(1, 1);
        gradient.endPoint = CGPointMake(1, 1);
        gradient.opacity = 0.6;
        
        [MainButton.layer insertSublayer:gradient atIndex:0];
        [MainButton bringSubviewToFront:SubButton];
        [MainButton bringSubviewToFront:buttonlabel];
        //testing
        
    }
    
    _ScrollView.contentSize = CGSizeMake(x, _ScrollView.frame.size.height);
}

-(void)setButtonBorderRed:(UIImageView *)myImageView
{
    myImageView.layer.borderWidth = 1.0f;
    myImageView.layer.borderColor = [UIColorFromRGB(CLR_RED) CGColor];
}
-(void)setButtonBorderNormal:(UIImageView *)myImageView
{
    myImageView.layer.borderWidth = 1.0f;
    myImageView.layer.borderColor = [[UIColor clearColor] CGColor];
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.CategoryList.count > 0)
    {
        [self loadScrollViewContent];
    }
    else
    {
        [self loadScrollViewContent];
        appDelegate.APIFlag = GET_PREFERED_CATEGORY_STATIC_LIST;
        SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
        ReqHandler.ParentView = self;
        [ReqHandler getPreferedCategoryList];
    }
}

@end
