//
//  SIXDNestedTVCell.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/24/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIXDNestedTVCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;

@property(nonatomic) CGSize CollectionViewCellSize;

@property (strong,nonatomic) NSMutableArray *TuneList;

@property(nonatomic) int Sorce;


@end
