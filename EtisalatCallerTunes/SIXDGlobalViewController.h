//
//  SIXDGlobalViewController.h
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 16/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SIXDGlobalViewController : UIViewController

//+ (void) setLoginButtonStyle :(UIButton *)Button;
+ (void) setAlert :(NSString*)AlertMessage : (UIViewController*) ViewController;

+ (NSString *)formatStringAsVisa:(NSString*)aNumber;

+ (NSString *)MD5 : (NSString*)inputString;

+ (void) setAlert :(NSString*)AlertMessage : (UIViewController*) ViewController :(int)Action;

+ (void)ChangeScrollPosition : (UIScrollView *)scrollView : (NSLayoutConstraint*)ImageHeight;

+ (int)ChangeScrollPositionAndHide : (UIScrollView *)scrollView : (NSLayoutConstraint*)ImageHeight;


+(void)showAndEnableRightNavigationItem: (ViewController*) ViewController;
+(void)showAndEnableLeftNavigationItem: (ViewController*) ViewController;

+(void)setNavigationBarOpaque:(ViewController*) ViewController;
+(void)setNavigationBarTransparent:(ViewController*) ViewController;

+(void)setNavigationBarTransparentWithImage:(ViewController*) ViewController :(NSString*)ImageName;

+(void)setNavigationBarTiTle:(ViewController*) ViewController :(NSString*) Title :(int)TitleColor;
+(void)onSelectedContactButton;
+(void)onSelectedBackButton;

+(NSString *)generateRandomNumber;
+(void) showAndEnableLeftNavigationItem_MainNavigationPage :(ViewController*) ViewController;

+ (NSString *)replaceFirstOccurenceOfAString : (NSString*) originalString : (NSString *)occurence : (NSString*)replacementString;

+(NSMutableArray*)convertToToneDetailsDefault: (NSMutableArray*)Response;

+(UIView*)showDefaultAlertWithAction :(NSString*)Message :(BOOL)Error :(CGRect)BackgrondViewFrame : (ViewController*)ParentView;
+(UIView*)showDefaultAlertWithAction_Tick :(NSString*)Message :(BOOL)Error :(CGRect)BackgrondViewFrame : (ViewController*)ParentView;

+(UIView*)showDefaultAlertMessage :(NSString*)Message : (float)yPosition;

+(NSMutableArray*)convertListToneResponse: (NSMutableArray*)Response;


+(NSAttributedString*) getAttributedText :(NSString*)string;
+(void)showCommonFailureResponse :(ViewController*)ParentView :(NSString*) Message;
+(NSString*)HTMLConversion:(NSString *)str;
+(NSString *)CapitalisationConversion:(NSString *)str;
//+(void)setGAEventTracker:(NSString *)Category :(NSString *)Action :(NSString *)Label;
//+ (void) setGAEventTracker : (int)Event;

+(BOOL) Custom_shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string : (UITextField*) textField;

+(BOOL)isMSISDNSame : (NSString *) AParty : (NSString *) BParty;
+(NSString*)URLDecode:(NSString *)str;
+(BOOL)isUnAuthorisedApp;


@end
