//
//  SIXDTopChartsVC.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDTopChartsVC.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"


@interface SIXDTopChartsVC ()

@end

@implementation SIXDTopChartsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor clearColor];
    
    
    [_SegmentTab setNeedsUpdateConstraints];
    [_SegmentTab layoutIfNeeded];
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Temp = [array objectAtIndex:0];
    UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 2)];
    
    
    Temp = [array objectAtIndex:1];
    UILabel *Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 2)];
    
    
    Temp = [array objectAtIndex:2];
    UILabel *Line2 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 2)];
    
    
    
    Temp = [array objectAtIndex:3];
    UILabel *Line3 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 2)];
    
    if([appDelegate.Language isEqualToString:@"English"])
    {
        _FourthLabel = Line;
        [_SegmentTab insertSubview:Line aboveSubview:Temp];
        
        _ThirdLabel = Line1;
        [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
        
        _SecondLabel = Line2;
        [_SegmentTab insertSubview:Line2 aboveSubview:Temp];
        
        _FirstLabel = Line3;
        [_SegmentTab insertSubview:Line3 aboveSubview:Temp];
    }
    else
    {
        _FourthLabel = Line3;
        [_SegmentTab insertSubview:Line3 aboveSubview:Temp];
        
        _ThirdLabel = Line2;
        [_SegmentTab insertSubview:Line2 aboveSubview:Temp];
        
        _SecondLabel = Line1;
        [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
        
        _FirstLabel = Line;
        [_SegmentTab insertSubview:Line aboveSubview:Temp];
    }
    
    
    
    
    _FirstLabel.backgroundColor = UIColorFromRGB(CLR_GREEN_SELECTED_TAB_LINE);
    _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
    _ThirdLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
    _FourthLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"ArialMT" size:8], NSFontAttributeName,UIColorFromRGB(CLR_ETISALAT_GREEN), NSForegroundColorAttributeName,
                                nil];
    [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_GREEN_SELECTED_TAB_TEXT) forKey:NSForegroundColorAttributeName];
    [_SegmentTab setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    
    
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"ArialMT" size:8], NSFontAttributeName,UIColorFromRGB(CLR_ETISALAT_GREEN), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_GREEN_SELECTED_TAB_TEXT) forKey:NSForegroundColorAttributeName];
        [_SegmentTab setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    }
    else
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"GESSTextMedium-Medium" size:10], NSFontAttributeName,UIColorFromRGB(CLR_ETISALAT_GREEN), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_GREEN_SELECTED_TAB_TEXT) forKey:NSForegroundColorAttributeName];
        [_SegmentTab setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    }
    */
    
    /*
    CGSize size ;
    size.width = self.view.frame.size.width;
    size.height = (201)+89+134;
    self.view.frame = CGRectMake(0, 0, size.width,size.height);
    self.preferredContentSize = size;
    self.automaticallyAdjustsScrollViewInsets = NO;
    */
    //TEMP REMOVE
    [self ReloadContents :0];
     
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender
{
    switch ([sender selectedSegmentIndex])
    {
            
        case 0:
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_GREEN_SELECTED_TAB_LINE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _ThirdLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _FourthLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            
            if(_TopPicks.count <= 0)
            {
                //[self getTones :[_CategoryIDs objectAtIndex:0]];
                [self getTones :@"Trending"];
            }
            else
            {
                [self ReloadContents :0];
            }
            
            break;
            
        case 1:
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_GREEN_SELECTED_TAB_LINE);
            _ThirdLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _FourthLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            
            if(_BestOfMonth.count <= 0)
            {
                [self getTones :[_CategoryIDs objectAtIndex:1]];
            }
            else
            {
                [self ReloadContents :1];
            }
            break;
            
        case 2:
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _ThirdLabel.backgroundColor = UIColorFromRGB(CLR_GREEN_SELECTED_TAB_LINE);
            _FourthLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            if(_BestOfWeek.count <= 0)
            {
                [self getTones :[_CategoryIDs objectAtIndex:2]];
            }
            else
            {
                [self ReloadContents :2];
            }
            break;
            
        case 3:
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _ThirdLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _FourthLabel.backgroundColor = UIColorFromRGB(CLR_GREEN_SELECTED_TAB_LINE);
            
            if(_MostLiked.count <= 0)
            {
                [self getTones :[_CategoryIDs objectAtIndex:3]];
            }
            else
            {
                [self ReloadContents :3];
            }
            break;
            
        default:
            break;
    }
}



-(void)ReloadContents : (NSInteger) Row
{
  
    switch (Row)
    {
        case 0:
            _GenericTableViewVC.TuneList = [_TopPicks mutableCopy];
            break;
        
        case 1:
            _GenericTableViewVC.TuneList = [_BestOfMonth mutableCopy];
            break;
            
        case 2:
            _GenericTableViewVC.TuneList = [_BestOfWeek mutableCopy];
            break;
            
        case 3:
            _GenericTableViewVC.TuneList = [_MostLiked mutableCopy];
            break;
            
        default:
            break;
    }
    
    [_GenericTableViewVC loadSubCategories];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"TOPCHARTS"])
    {
        _GenericTableViewVC = (SIXDGenericTableView *) [segue destinationViewController];
        _GenericTableViewVC.Source = TOPCHARTS;
        [self addChildViewController:_GenericTableViewVC];
        NSLog(@"GenericTableViewVC = %@",_GenericTableViewVC);
        
        
    }
}

-(void)getTones :(NSString*)Identifier
{
    [self.view addSubview:_overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    [_SegmentTab setUserInteractionEnabled:false];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    if([Identifier isEqualToString:@"Trending"])
    {
        NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
        
        NSString *URL = [NSString stringWithFormat:@"%@/crbt/get-recommendation-songs",appDelegate.BaseMiddlewareURL];
        
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&clientTxnId=%@&identifier=%@",URL,appDelegate.Language,appDelegate.MobileNumber,Rno,Identifier];
    }
    else
    {
        NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];
   
        ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&pageNo=1&perPageCount=20&sortBy=Order_By&alignBy=ASC",URL,appDelegate.Language,Identifier];
    }
    [ReqHandler sendHttpGETRequest:self :nil];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];

     [_SegmentTab setUserInteractionEnabled:true];
    
    NSLog(@"Response top charts = %@",Response);
    
    Response = [Response objectForKey:@"responseMap"];
    
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            //_TopPicks = [[Response objectForKey:@"searchList"]mutableCopy];
            _TopPicks = [[Response objectForKey:@"recommendationSongsList"]mutableCopy];
            NSLog(@"_TopPicks = %@",_TopPicks);
            [self ReloadContents:0];
            break;
        
        case 1:
            _BestOfMonth = [[Response objectForKey:@"searchList"]mutableCopy];
            [self ReloadContents:1];
            break;
            
        case 2:
            _BestOfWeek = [[Response objectForKey:@"searchList"]mutableCopy];
            [self ReloadContents:2];
            break;
            
        case 3:
            _MostLiked = [[Response objectForKey:@"searchList"]mutableCopy];
            [self ReloadContents:3];
            break;
            
        default:
            break;
    }
    
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"SIXDTopCharts::onFailureResponseRecived >>> Response %@", Response);
    
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [_SegmentTab setUserInteractionEnabled:true];
    //refersh current table view data
    [self ReloadContents:_SegmentTab.selectedSegmentIndex];
    
}

-(void)onFailedToConnectToServer
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [_SegmentTab setUserInteractionEnabled:true];
    [self ReloadContents:_SegmentTab.selectedSegmentIndex];
    NSLog(@"onFailedToConnectToServer>>>");
    
}
- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)loadTopCharts
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    _CategoryIDs = [NSArray new];
    _CategoryIDs = [[appDelegate.AppSettings objectForKey:@"TopCharts_IDS"] componentsSeparatedByString:@","];
    
    NSArray *DisplayNames = [NSArray new];
    DisplayNames = [[appDelegate.AppSettings objectForKey:@"TopCharts_DISPLAYNAMES"]  componentsSeparatedByString:@","];
    
    for (int i=0 ; i<DisplayNames.count; i++) {
        [_SegmentTab setTitle:[DisplayNames objectAtIndex:i] forSegmentAtIndex:i];
    }
    
    _TopChartsLabel.text = NSLocalizedStringFromTableInBundle(@"TOP CHARTS",nil,appDelegate.LocalBundel,nil);
    NSLog(@"_CategoryIDs = %@",_CategoryIDs);
    //[self getTones :[_CategoryIDs objectAtIndex:0]];
    [self getTones :@"Trending"];
    
    _SegmentTab.selectedSegmentIndex = 0;
    [self onSegmentValueChanged:_SegmentTab];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

/*
-(void)preferredContentSizeDidChangeForChildContentContainer:(id<UIContentContainer>)container
{
    NSLog(@"preferredContentSizeDidChangeForChildContentContainer>>>>");
    
    NSString *str= [NSString stringWithFormat:@"%@",container];
    NSRange match  = [str rangeOfString:@"SIXDGenericTableView"];
    
    if(match.location!=NSNotFound)
    {
        [UIView animateWithDuration:1.0 animations:^{
            
            _ThirdContainer.frame = _GenericTableVC.TableView.frame ;
            
            _SubCategoryThirdContainerHeight.constant = _GenericTableVC.TableView.frame.size.height;
            
            _GenericTableVC.view.frame = CGRectMake(0, 0,_ThirdContainer.frame.size.width,_SubCategoryThirdContainerHeight.constant);
            
            [_ThirdContainer layoutSubviews];
            
        } completion:nil];
        return;
    }
    
}
*/
@end
