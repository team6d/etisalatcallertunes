//
//  SIXDCommonPopUp_Text.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/15/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SIXDCommonPopUp_Text : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onSelectedConfirmButton:(id)sender;

@property (weak, nonatomic) IBOutlet  UILabel *AlertTitle;
- (IBAction)onSelectedCloseButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *AlertButton;
@property(strong,nonatomic) NSString* AlertImageName;

@property(strong,nonatomic) ViewController *ParentView;

@property(strong,nonatomic) NSString* AlertMessage;

@property(strong,nonatomic) NSString* ConfirmButtonText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceFromAlertLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PopUpViewHeight;
@property (strong,nonatomic)UIView *overlayview;

@property (nonatomic) BOOL HideCloseButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

- (void)putOverlayView;


@end
