//
//  SIXDSongCatcherVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "SIXDSearchVC.h"

#define START_CHECK 1
#define STOP_CHECK  2

@interface SIXDSongCatcherVC : ViewController
//from ACR demo-start
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UITextView *resultView;
//from ACR demo-end

- (IBAction)onClickedMainButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *MainButton;
@property(nonatomic) int CheckFlag;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
- (IBAction)onClickedCloseButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;

@property (weak, nonatomic) IBOutlet UILabel *ListeningLabel;

@property (strong, nonatomic) NSString *SearchKeyTitle;
@property (strong, nonatomic) NSString *SearchKeyArtist;
@property(nonatomic) BOOL FindStatus;

@property(strong,nonatomic) SIXDSearchVC *SearchVCObj;
@property(strong,nonatomic) NSMutableArray *CDPSongList;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@end
