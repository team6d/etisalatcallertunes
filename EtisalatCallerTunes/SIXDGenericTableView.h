//
//  SIXDGenericTableView.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 1/25/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"


@class SIXDSubCategories;

@interface SIXDGenericTableView : ViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property(nonatomic) int Source;

//contains list of all sub categories
@property(nonatomic,strong) NSMutableArray *TuneList;

@property(strong,nonatomic) SIXDSubCategories *SubCategoriesVC;

-(void)loadSubCategories;
@property(strong,nonatomic) NSMutableArray *CategoryList;

@end
