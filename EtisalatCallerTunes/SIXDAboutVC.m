//
//  SIXDAboutVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDAboutVC.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"

@interface SIXDAboutVC ()

@end

@implementation SIXDAboutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
     [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    [self.view addSubview:_overlayview];
    
    [SIXDStylist setGAScreenTracker:@"About_Callertunes"];

}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSString *urlString = [appDelegate.AppSettings objectForKey:@"ABOUT_URL"];
    //urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&version=V%@", APP_VERSION]];
     urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&version=V%@",version]];
    
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    
  /*  CGRect DesiredFrame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-64, self.view.frame.size.width, self.view.frame.size.height);
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:DesiredFrame configuration:theConfiguration];
   */
    WKWebView *webView = [[WKWebView alloc] initWithFrame:_MainView.frame configuration:theConfiguration];
    webView.navigationDelegate = self;
    NSURL *nsurl=[NSURL URLWithString:urlString];
    NSLog(@"url = %@",nsurl);
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webView loadRequest:nsrequest];
   // [self.view addSubview:webView];
    [_MainView addSubview:webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webView:(WKWebView *)webView
didFinishNavigation:(WKNavigation *)navigation
{
    NSLog(@"didFinishNavigation>>> ");
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_logo"]];
    //[SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"ABOUT CALLER TUNES",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
}


@end
