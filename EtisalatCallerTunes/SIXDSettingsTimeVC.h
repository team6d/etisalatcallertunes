//
//  SIXDSettingsTimeVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDMainSettingsVC.h"
//#import "SIXDDatePickerPopUpVC.h"

@class SIXDDatePickerPopUpVC ;
@class SIXDTimePickerPopUP ;
@interface SIXDSettingsTimeVC : ViewController
@property (weak, nonatomic) IBOutlet UIView *UpperView;
@property (weak, nonatomic) IBOutlet UIImageView *MainImage;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onClickedConfirmButton:(id)sender;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *SubScrollView;
@property (weak, nonatomic) IBOutlet UILabel *RepeatLabel;


@property (weak, nonatomic) IBOutlet UIView *StartView;
@property (weak, nonatomic) IBOutlet UIView *EndView;
@property (weak, nonatomic) IBOutlet UIView *TimeTypeView;

@property (weak, nonatomic) IBOutlet UILabel *TypeTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *TypeBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *StartTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *StartBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *EndTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *EndBottomLabel;

@property (weak, nonatomic) IBOutlet UIButton *TypeButton;
@property (weak, nonatomic) IBOutlet UIButton *StartButton;
@property (weak, nonatomic) IBOutlet UIButton *EndButton;
@property (nonatomic) NSInteger SelectTimeTypeFromPopUp;
@property (nonatomic) int Source;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (nonatomic) int RepeatButtonProperty;
@property (strong,nonatomic) NSMutableArray *BottomButtonArray;
@property (strong,nonatomic) NSMutableArray *RepeatButtonActionsList;
@property (strong,nonatomic) NSMutableDictionary *RepeatButtonActionsDict;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToStartLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToEndlabel;

@property(weak,nonatomic)NSString *FontName;

@property(strong,nonatomic) SIXDMainSettingsVC *MainSettingsVC;
@property(strong,nonatomic) SIXDDatePickerPopUpVC *DatePickerPopUpVC;
@property(strong,nonatomic) SIXDTimePickerPopUP *TimePickerPopUP;

@property (weak, nonatomic) IBOutlet SIXDAlertLabel *AlertLabel;

@end
