//
//  SIXDLikeContentSetting.h
//  EtisalatCallerTunes
//
//  Created by Shahnawaz Nazir on 26/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"


@interface SIXDLikeContentSetting : ViewController

+ sharedSampleSingletonClass;



@property (strong,nonatomic) NSMutableArray *UserLikedToneListDownloaded;
@property (strong,nonatomic) NSMutableArray *UserLikeToneListLocal;
@property (strong,nonatomic) NSMutableArray *UserUNLikeToneListLocal;

@property(strong,nonatomic) NSMutableDictionary *LikeToneCategoryIdDict;
@property(strong,nonatomic) NSMutableDictionary *UNLikeToneCategoryIdDict;
@property(nonatomic,strong)NSMutableArray *LikedToneList;

@property int API_Identifier;

@property bool AccessFlag;
@property int NextAction;
@property int Source;



- (int) likeTone : (NSString *) toneId : (NSString *)categoryId : (UILabel *)likeLabel;
- (void) setLikeCount: (NSString *) toneId : (int)LikeCount : (UILabel *)likeLabel;
- (bool)isToneLikedFromDownloadedList : (NSString *)newToneId;
- (int) setToneButtonLikedUnliked : (NSString *) toneId;
-(bool) isToneIdPresentIn : (NSMutableArray *)ToneIdList :(NSString*) toneId;
- (void) downloadSubscriberLikedTunes;
- (void)sendTuneListAndDownload;
- (void) firstLikeTunesDownloadRequest;



@end


