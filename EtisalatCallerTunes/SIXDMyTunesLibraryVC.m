//
//  SIXDMyTunesLibraryVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 30/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDMyTunesLibraryVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDBuyToneVC.h"
#import "SIXDPlayerVC.h"
#import "SIXDCommonPopUp_Text.h"
#import "UIImageView+WebCache.h"
#import "SIXDPlaylistBuyVC.h"
#import "SIXDMainSettingsVC.h"
#import "SIXDCustomButton.h"
#import "SIXDStylist.h"

#define TUNES_RBTMODE    400
#define PLAYLIST_RBTMODE 300
#define ActionType_DELETE_KAROKE     2
#define ActionType_REDIRECT_TO_HOME  3
#define ActionType_NONE              0

@interface SIXDMyTunesLibraryVC ()

@end

@implementation SIXDMyTunesLibraryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"my tunes library_CAPS",nil,appDelegate.LocalBundel,nil);
    
    _IsRefreshRequired = true;
    
    [_SetButton setTitle:NSLocalizedStringFromTableInBundle(@"play selected tunes to your callers",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    
    //adding lines for selected tab
    Temp = [array objectAtIndex:0];
    Temp = [array objectAtIndex:1];
    UILabel *Line;
   UILabel *Line1;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+80, 10, 1, 6)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+165, 10, 1, 6)];
    }
    else
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+75, 10, 1, 6)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+170, 10, 1, 6)];
    }
  /*
    if(appDelegate.DeviceSize == iPHONE_X || appDelegate.DeviceSize == SMALL || appDelegate.DeviceSize == MEDIUM)
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+80, 10, 1, 6)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x+165, 10, 1, 6)];
    }
    else
    {
        Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 10, 1, 6)];
        Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 10, 1, 6)];
    }
   */
    
   // UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 10, 1, 6)];
    Line.backgroundColor = [UIColor grayColor];
    [_SegmentTab insertSubview:Line aboveSubview:Temp];
    
    
    //UILabel *Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, 10, 1, 6)];
    Line1.backgroundColor = [UIColor grayColor];
    
    [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
    
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"TUNES",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"PLAYLIST",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"KARAOKE",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:2];
    
    
    _Tunes_ButtonTagArray = [NSMutableArray new];
    _Playlist_ButtonTagArray = [NSMutableArray new];
    _Karaoke_ButtonTagArray = [NSMutableArray new];
    
    [_SetButton setTintColor:UIColorFromRGB(CLR_BUY)];
    [_SetButton setUserInteractionEnabled:false];
    
   
   /*
    CGRect size = CGRectMake(0,290, _TableView.frame.size.width, _TableView.frame.size.height);
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :NSLocalizedStringFromTableInBundle(@"its kind of empty here!",nil,appDelegate.LocalBundel,nil) :true :size :self];
     [self.view addSubview:_AlertView];
    [_AlertView setBackgroundColor:[UIColor whiteColor]];
     [self.view bringSubviewToFront:_AlertView];
    [self changeAlertText];
    [_AlertView setHidden:true];
    */

   
    
    UIView *View = [SIXDGlobalViewController showDefaultAlertWithAction :NSLocalizedStringFromTableInBundle(@"its kind of empty here!",nil,appDelegate.LocalBundel,nil) :true :CGRectNull :self];
    [_AlertView addSubview:View];
   // [self.view addSubview:_AlertView];
   // [_AlertView setBackgroundColor:[UIColor whiteColor]];
   // [self.view bringSubviewToFront:_AlertView];
    [self changeAlertText];
    [_AlertView setHidden:true];
    
    
   
   

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            return _Tunes_listToneApk.count;
            break;
            
        case 1:
            return _Playlist_listToneApk.count;
            break;
         
        case 2:
            return _KaraokeList.count;
            break;
            
        default:
            return 0;
            break;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary *toneDetails;
    
    cell.SubView.backgroundColor =UIColorFromRGB(CLR_WHITE) ;
   
    
    if(_SegmentTab.selectedSegmentIndex==0)
    {
        cell.AccessoryButton.backgroundColor =UIColorFromRGB(CLR_WHITE);
        [cell.AccessoryButton setUserInteractionEnabled:true];
        
        if(_Tunes_listToneApk.count > indexPath.row)
        {
            toneDetails = [_Tunes_listToneApk objectAtIndex:indexPath.row];
            cell.TopSpaceToTextField.constant = 10;
            cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[toneDetails objectForKey:@"artistName"]];
            cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[toneDetails objectForKey:@"toneName"]];
            
            NSMutableArray *salatiCheckArray = [appDelegate.AppSettings objectForKey:@"SALATI"];
            if([[toneDetails objectForKey:@"toneId"] isEqualToString:[salatiCheckArray objectAtIndex:0]])
            {
                cell.SubView.backgroundColor =UIColorFromRGB(CLR_TEXTFIELDBORDER);
                cell.FirstButton.hidden = YES;
                [cell.SelectionButton setUserInteractionEnabled:false];
                [cell.SelectionButton setImage:[UIImage imageNamed:@"grayselection"] forState:UIControlStateNormal];
                [cell.SecondButton setUserInteractionEnabled:false];
                [cell.DetailLabel setHidden:true];
            }
                
            else if([[toneDetails objectForKey:@"status"] isEqualToString:@"A"])
            {
                
                cell.FirstButton.hidden = YES;
                cell.DetailLabel.hidden = YES;
                [cell.SelectionButton setImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                
                for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
                {
                    NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
                    int buttonTag = [tagStr intValue];
                    if(buttonTag == indexPath.row)
                    {
                        [cell.SelectionButton setImage:[UIImage imageNamed:@"whitetick"] forState:UIControlStateNormal];
                    }
                }
                
                [cell.SelectionButton setUserInteractionEnabled:true];
                [cell.SecondButton setUserInteractionEnabled:true];
                
            }
            else
            {
                cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"INACTIVE",nil,appDelegate.LocalBundel,nil);
                [cell.FirstButton setImage:[UIImage imageNamed:@"mytuneslibraryinactive"] forState:UIControlStateNormal];
                
                
                cell.SubView.backgroundColor =UIColorFromRGB(CLR_TEXTFIELDBORDER);
                cell.FirstButton.hidden = NO;
                [cell.SelectionButton setUserInteractionEnabled:false];
                [cell.SelectionButton setImage:[UIImage imageNamed:@"grayselection"] forState:UIControlStateNormal];
                [cell.SecondButton setUserInteractionEnabled:false];
                [cell.DetailLabel setHidden:false];
            }
            
            
            NSString *StatusRBT = [toneDetails objectForKey:@"isStatusRbt"];
            UIImage* image;
            if([StatusRBT isEqualToString:@"T"])
            {
                 image = [[UIImage imageNamed:@"delete"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            }
            else
            {
                 image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            }
            
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            [cell.SecondButton setImage:[UIImage imageNamed:@"mytuneslibrarysettings"] forState:UIControlStateNormal];
            
            
            cell.DetailLabel2.hidden = YES;
            cell.SecondButton.hidden = false;
            cell.SubTitleLabel.hidden = false;
        }
        
    }
    else if(_SegmentTab.selectedSegmentIndex==1)
    {
        cell.AccessoryButton.backgroundColor =UIColorFromRGB(CLR_WHITE);
        [cell.AccessoryButton setUserInteractionEnabled:true];
        
        
        if(_Playlist_listToneApk.count > indexPath.row)
        {
            toneDetails = [_Playlist_listToneApk objectAtIndex:indexPath.row];
            
            cell.TopSpaceToTextField.constant = 17;
            cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[toneDetails objectForKey:@"toneName"]];
            cell.FirstButton.hidden = YES;
            cell.DetailLabel.hidden = YES;
            
            //TO DO - check if it is inactive
            if([[toneDetails objectForKey:@"status"] isEqualToString:@"A"])
            {
                
                cell.SecondButton.hidden = YES;
                cell.DetailLabel2.hidden = YES;
                [cell.SelectionButton setImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                
                for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
                {
                    NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
                    int buttonTag = [tagStr intValue];
                    if(buttonTag == indexPath.row)
                    {
                        [cell.SelectionButton setImage:[UIImage imageNamed:@"whitetick"] forState:UIControlStateNormal];
                    }
                }
                
                [cell.SelectionButton setUserInteractionEnabled:true];
                
            }
            else
            {
               cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"INACTIVE",nil,appDelegate.LocalBundel,nil);
                cell.SecondButton.hidden = NO;
                cell.SecondButton.userInteractionEnabled = false;
                [cell.SecondButton setImage:[UIImage imageNamed:@"mytuneslibraryinactive"] forState:UIControlStateNormal];
                cell.SubView.backgroundColor =UIColorFromRGB(CLR_TEXTFIELDBORDER);
                cell.AccessoryButton.backgroundColor =UIColorFromRGB(CLR_WHITE);
                
                [cell.SelectionButton setUserInteractionEnabled:false];
                [cell.SelectionButton setImage:[UIImage imageNamed:@"grayselection"] forState:UIControlStateNormal];
            }
            
            UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
            [cell.SecondButton setUserInteractionEnabled:false];
            cell.SubTitleLabel.hidden = true;
        }
    }
    else if(_SegmentTab.selectedSegmentIndex==2)
    {
        cell.AccessoryButton.backgroundColor =UIColorFromRGB(CLR_WHITE);
        [cell.AccessoryButton setUserInteractionEnabled:true];
        
        if(_KaraokeList.count > indexPath.row)
        {
            toneDetails = [_KaraokeList objectAtIndex:indexPath.row];
            cell.TopSpaceToTextField.constant = 17;
            NSString *ToneName = [SIXDGlobalViewController HTMLConversion:[toneDetails objectForKey:@"toneName"]];
            cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:ToneName];
            
            UIImage* image = [[UIImage imageNamed:@"delete"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
            if([[toneDetails objectForKey:@"status"] isEqualToString:@"A"])
            {
                
                cell.FirstButton.hidden = YES;
                cell.DetailLabel.hidden = YES;
                [cell.SelectionButton setImage:[UIImage imageNamed:@"whitecircle"] forState:UIControlStateNormal];
                
                for(int i=0;i<_Karaoke_ButtonTagArray.count;i++)
                {
                    NSString *tagStr = [_Karaoke_ButtonTagArray objectAtIndex:i];
                    int buttonTag = [tagStr intValue];
                    if(buttonTag == indexPath.row)
                    {
                        [cell.SelectionButton setImage:[UIImage imageNamed:@"whitetick"] forState:UIControlStateNormal];
                    }
                }
                [cell.SecondButton setUserInteractionEnabled:true];
                [cell.SelectionButton setUserInteractionEnabled:true];
            }
            else
            {
                
                 if([[toneDetails objectForKey:@"status"] isEqualToString:@"P"])
                 {
                      cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"PENDING",nil,appDelegate.LocalBundel,nil);
                      [cell.FirstButton setImage:[UIImage imageNamed:@"mytunelibraryunderapproval"] forState:UIControlStateNormal];
                     cell.AccessoryButton.backgroundColor =UIColorFromRGB(CLR_TEXTFIELDBORDER);
                     [cell.AccessoryButton setUserInteractionEnabled:false];

                 }
                else if([[toneDetails objectForKey:@"status"] isEqualToString:@"R"])
                {
                    cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"REJECTED",nil,appDelegate.LocalBundel,nil);
                    [cell.FirstButton setImage:[UIImage imageNamed:@"Rejected"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.FirstButton setImage:[UIImage imageNamed:@"mytuneslibraryinactive"] forState:UIControlStateNormal];
                    cell.DetailLabel.text = NSLocalizedStringFromTableInBundle(@"INACTIVE",nil,appDelegate.LocalBundel,nil);
                }
                
               
                cell.FirstButton.hidden = NO;
                
                //TO DO TAKE IMAGE BASED ON INACTIVE STATUS
                cell.SubView.backgroundColor =UIColorFromRGB(CLR_TEXTFIELDBORDER);
                
                [cell.SelectionButton setUserInteractionEnabled:false];
                [cell.SelectionButton setImage:[UIImage imageNamed:@"grayselection"] forState:UIControlStateNormal];
                [cell.SecondButton setUserInteractionEnabled:false];
                [cell.DetailLabel setHidden:false];
                
            }
            
            
            [cell.SecondButton setImage:[UIImage imageNamed:@"mytuneslibrarysettings"] forState:UIControlStateNormal];
            
            cell.SecondButton.hidden = false;
            cell.SubTitleLabel.hidden = true;
            cell.DetailLabel2.hidden = YES;
        }
        
    }
    
    NSURL *ImageURL = [NSURL URLWithString:[toneDetails objectForKey:@"previewImageUrl"]];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    int x_Position;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        x_Position=0;
    }
    else
    {
        x_Position=50-1;
    }
    
    UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
    
    [cell.SecondButton addSubview:leftBorder2];
    
    UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    [cell.AccessoryButton addSubview:leftBorder3];
    
    cell.FirstButton.tag = indexPath.row;
    cell.SecondButton.tag = indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    cell.SelectionButton.tag = indexPath.row;
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
}

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender
{
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            [SIXDStylist setGAScreenTracker:@"View_MyTunesLibrary_Tunes"];
            break;
            
        case 1:
            [SIXDStylist setGAScreenTracker:@"View_MyTunesLibrary_Playlist"];
            break;
            
        case 2:
            [SIXDStylist setGAScreenTracker:@"View_MyTunesLibrary_Karaoke"];
            break;
        default:
            break;
    }
    _ActionType = ActionType_NONE;
    [self updateSetToneButton];
    [_TableView reloadData];
}

-(void)updateTableValues
{
    _ActionType = ActionType_NONE;
    [self updateSetToneButton];
    [_TableView reloadData];
}

-(void)API_ListTones
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/list-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&startIndex=0&endIndex=50&rbtMode=%d",URL,appDelegate.Language,appDelegate.MobileNumber,_rbtMode];

    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)API_GetKaraokeList
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=LIST_KARAOKE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&aPartyMsisdn=%@&channelId=%@&serviceId=4",Rno,appDelegate.Language,appDelegate.MobileNumber,CHANNEL_ID];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-karoake-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_DeleteTone
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=DELETE_TONE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    NSString *toneId;
    
    if(_SegmentTab.selectedSegmentIndex==2)
    {
        NSMutableDictionary *DummyDict = [_KaraokeList objectAtIndex:_ButtonTag];
        toneId = [DummyDict objectForKey:@"toneId"];
        
        NSString *toneName = [SIXDGlobalViewController HTMLConversion:[DummyDict objectForKey:@"toneName"]];
        NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@",toneName,toneId];
        
        [SIXDStylist setGAEventTracker:@"Settings" :@"Delete karoke" :GAEventDetails];
        
    }
    else if(_SegmentTab.selectedSegmentIndex==0)
    {
        NSMutableDictionary *DummyDict = [_Tunes_listToneApk objectAtIndex:_ButtonTag];
        toneId = [DummyDict objectForKey:@"toneId"];
    }
    
    
    NSString *PackName =  [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&toneId=%@&language=%@&packName=%@",appDelegate.MobileNumber,toneId,appDelegate.Language,PackName];

    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-tone",appDelegate.BaseMiddlewareURL];

    [ReqHandler sendHttpPostRequest:self];

}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"API_ListTones onSuccessResponseRecived = %@",Response);
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    
    if(appDelegate.APIFlag==LIST_TONES_TONES)
    {
        _Tunes_listToneApk=[responseMap objectForKey:@"listToneApk"];
        _Tunes_listToneApk = [SIXDGlobalViewController convertListToneResponse:_Tunes_listToneApk];
        
        _SegmentTab.selectedSegmentIndex = 0;
        [self updateTableValues];
        //[self onSegmentValueChanged:_SegmentTab];
        
    }
    else if(appDelegate.APIFlag==LIST_TONES_PLAYLIST)
    {
        _Playlist_listToneApk=[responseMap objectForKey:@"listToneApk"];
        _Playlist_listToneApk = [SIXDGlobalViewController convertListToneResponse:_Playlist_listToneApk];
         [self updateTableValues];
        // [self onSegmentValueChanged:_SegmentTab];
        
    }
    else if(appDelegate.APIFlag==LIST_KARAOKE)
    {
        _KaraokeList = [[responseMap objectForKey:@"karoakaeList"]mutableCopy];
         [self updateTableValues];
        // [self onSegmentValueChanged:_SegmentTab];
        
    }
    
    else if(appDelegate.APIFlag==DELETE_TONE)
    {
        if(_SegmentTab.selectedSegmentIndex == 2)
        {
            [_KaraokeList removeObjectAtIndex:_ButtonTag];
        }
        else
        {
            [_Tunes_listToneApk removeObjectAtIndex:_ButtonTag];
        }
        
        [_TableView reloadData];
    }
    else
    {
        switch (_SegmentTab.selectedSegmentIndex)
        {
            case 0:
                [_Tunes_ButtonTagArray removeAllObjects];
                break;
                
             case 1:
                [_Playlist_ButtonTagArray removeAllObjects];
                break;
                
            case 2:
                [_Karaoke_ButtonTagArray removeAllObjects];
                break;
                
            default:
                break;
        }
        
        //For reloading table view
         [self updateTableValues];
        //[self onSegmentValueChanged:_SegmentTab];
        
        //ADD TO SHUFFLE API SUCCESS RESPONSE DECODING
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        NSString *title = [NSString stringWithFormat:@"%@\n %@",NSLocalizedStringFromTableInBundle(@"SET_CONFIRM_1",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_CONFIRM_2",nil,appDelegate.LocalBundel,nil)];
        ViewController.AlertMessage = title;
        ViewController.AlertImageName = @"";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
        [self presentViewController:ViewController animated:YES completion:nil];
        
    }
    
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    if((appDelegate.APIFlag== LIST_TONES_TONES && _SegmentTab.selectedSegmentIndex == 0)|| (appDelegate.APIFlag== LIST_TONES_PLAYLIST  && _SegmentTab.selectedSegmentIndex == 1)|| (appDelegate.APIFlag== LIST_KARAOKE  && _SegmentTab.selectedSegmentIndex == 2))
    {
        [self changeAlertText];
        [_AlertView setHidden:false];
        [self.view bringSubviewToFront:_AlertView];
        _ActionType = ActionType_REDIRECT_TO_HOME;
    }
    else
    {
        
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    }
    
    
    // Remove old contents if Reload Fails after delete tone. If user have no tunes/playlist contents - failure response is sent
    if(appDelegate.APIFlag == LIST_TONES_TONES || appDelegate.APIFlag == LIST_TONES_PLAYLIST)
    {
        switch (_SegmentTab.selectedSegmentIndex)
        {
            case 0:
                [_Tunes_listToneApk removeAllObjects];
                [self changeAlertText];
                [_AlertView setHidden:false];
                [self.view bringSubviewToFront:_AlertView];
                _ActionType = ActionType_REDIRECT_TO_HOME;
                break;
                
            case 1:
                [_Playlist_listToneApk removeAllObjects];
                [self changeAlertText];
                [_AlertView setHidden:false];
                [self.view bringSubviewToFront:_AlertView];
                _ActionType = ActionType_REDIRECT_TO_HOME;
                break;
                
            default:
                break;
        }
    }
    //end
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (IBAction)onCickedSettingsButton:(UIButton*)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    SIXDMainSettingsVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMainSettingsVC"];
    
    ViewController.ScreenType = FIRST_SETTINGS_SCREEN;
    ViewController.TimeSettingsType  = -1;
    ViewController.UserSettingsType = -1;
    if(_SegmentTab.selectedSegmentIndex == 0)
    {
        ViewController.ToneDetails=[_Tunes_listToneApk objectAtIndex:sender.tag];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if (_SegmentTab.selectedSegmentIndex == 1)
    {
        ViewController.ToneDetails=[_Playlist_listToneApk objectAtIndex:sender.tag];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else if (_SegmentTab.selectedSegmentIndex == 2)
    {
        ViewController.ToneDetails=[_KaraokeList objectAtIndex:sender.tag];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
}

- (IBAction)onClickedAccessoryButton:(UIButton *)sender
{
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_SegmentTab.selectedSegmentIndex == 2)
    {
         [self presentDeleteConfirmationPopUp:sender];
    }
    else if(_SegmentTab.selectedSegmentIndex == 0) //TUNES
    {
        NSMutableDictionary* toneDetails = [_Tunes_listToneApk objectAtIndex:sender.tag];
        NSString *StatusRBT = [toneDetails objectForKey:@"isStatusRbt"];

        if([StatusRBT isEqualToString:@"T"])
        {
            [self presentDeleteConfirmationPopUp:sender];
        }
        else
        {
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
            ViewController.PlayList = _Tunes_listToneApk;
            ViewController.IsDeletePage = true;
            ViewController.CurrentToneIndex = sender.tag;
            ViewController.MYTunesLibraryVC = self;
            [self.navigationController pushViewController:ViewController animated:YES];
        }
    }
    else //Playlist
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SIXDPlaylistBuyVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlaylistBuyVC"];
        ViewController.PlaylistDetails = [_Playlist_listToneApk objectAtIndex:sender.tag];
        ViewController.IsDeletePage = true;
        ViewController.MYTunesLibraryVC = self;
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    
}

-(void)presentDeleteConfirmationPopUp :(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _ButtonTag =sender.tag;
    _ActionType = ActionType_DELETE_KAROKE;
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.ParentView = self;
    ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Are you sure you want to delete?",nil,appDelegate.LocalBundel,nil);
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    
    [self presentViewController:ViewController animated:YES completion:nil];
}


-(void)onSelectedPopUpConfirmButton
{
    NSLog(@"onSelectedPopUpConfirmButton>>> _ActionType = %d",_ActionType);
    switch (_ActionType)
    {
        case ActionType_DELETE_KAROKE:
            [self API_DeleteTone];
            break;
        
        case ActionType_REDIRECT_TO_HOME:
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
            
            if(_SegmentTab.selectedSegmentIndex == 1)
            {
                    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMusicBoxList"];
                    
                    appDelegate.CategoryName = NSLocalizedStringFromTableInBundle(@"PLAYLIST",nil,appDelegate.LocalBundel,nil);
                    
                    appDelegate.CategoryId = [[appDelegate.AppSettings objectForKey:@"PLAYLIST_ID"]intValue];
                
                    [self.navigationController pushViewController:ViewController animated:YES];
                
            }
            else
            {
                UINavigationController *Temp;
                if(_SegmentTab.selectedSegmentIndex == 0)
                {
                    appDelegate.MainTabController.selectedIndex = 0;
                    Temp = [VCArray objectAtIndex:0];
                }
                else if(_SegmentTab.selectedSegmentIndex == 2)
                {
                    appDelegate.MainTabController.selectedIndex = 4;
                    Temp = [VCArray objectAtIndex:4];
                }
                [Temp popToRootViewControllerAnimated:false];
            }
            
        }
            break;
            
        default:
            break;
    }
    
}

- (IBAction)onClickedInfoButton:(UIButton *)sender
{
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    //clubbing all 4 pieces of string for localisation
    NSString *title;
    
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
        case 2:
             title = [NSString stringWithFormat:@"%@\n%@ %@\n %@\n",NSLocalizedStringFromTableInBundle(@"SET_BUTTON_1",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_2",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_3",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_4",nil,appDelegate.LocalBundel,nil)];
            break;
            
        case 1:
            title = [NSString stringWithFormat:@"%@\n%@ %@\n",NSLocalizedStringFromTableInBundle(@"Select one playlist to play it full day to all your callers.",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"At each call,a different tune will play.",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"Select more than one playlist to shuffle them.",nil,appDelegate.LocalBundel,nil)];
            break;
            
       // case 2:
         //   return _KaraokeList.count;
           // break;
            
        default:
           
            break;
    }
    
  //  NSString *title = [NSString stringWithFormat:@"%@\n%@ %@\n %@\n",NSLocalizedStringFromTableInBundle(@"SET_BUTTON_1",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_2",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_3",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"SET_BUTTON_4",nil,appDelegate.LocalBundel,nil)];
    
    ViewController.AlertMessage = title;
    ViewController.AlertImageName = @"";
    
    //giving space - SIXDCommonPopUp class will reduce height of pop up if title is empty
    ViewController.title = @" ";
    ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    
    [self presentViewController:ViewController animated:YES completion:nil];
}


- (IBAction)onClickedSetButton:(UIButton *)sender
{
    
    [self API_AddToneToShuffle];
}

-(void)API_AddToneToShuffle
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=ADDTONE_TO_SHUFFLE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    NSMutableDictionary *TempDict = [NSMutableDictionary new];
    NSMutableArray *ToneIdListArray = [NSMutableArray new];
    
    NSString *GAEventDetails = @"";
    
    if(_SegmentTab.selectedSegmentIndex == 0)
    {
        
        for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
        {
            NSMutableDictionary *toneId = [NSMutableDictionary new];
            NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
            TempDict  = [_Tunes_listToneApk objectAtIndex:buttonTag];
            [toneId setObject:[TempDict objectForKey:@"toneId"] forKey:@"toneId"];
            [ToneIdListArray addObject:toneId];
            
            if(GAEventDetails.length > 0)
            {
                GAEventDetails = [NSString stringWithFormat:@"%@,",GAEventDetails];
            }
            
            NSString *ToneName = [SIXDGlobalViewController HTMLConversion:[TempDict objectForKey:@"toneName"]];
            NSString *ArtistName = [SIXDGlobalViewController HTMLConversion:[TempDict objectForKey:@"artistName"]];
            GAEventDetails = [NSString stringWithFormat:@"%@%@-%@-%@",GAEventDetails,ToneName,ArtistName,[TempDict objectForKey:@"toneId"]];
            
            
        }
        NSLog(@"GAEventDetails = %@",GAEventDetails);
        [SIXDStylist setGAEventTracker:@"Settings" :@"Play selected tunes to callers" :GAEventDetails];
    }
    else if(_SegmentTab.selectedSegmentIndex == 1)
    {
        for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
        {
            NSMutableDictionary *toneId = [NSMutableDictionary new];
            NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
            TempDict  = [_Playlist_listToneApk objectAtIndex:buttonTag];
            [toneId setObject:[TempDict objectForKey:@"toneId"] forKey:@"toneId"];
            [ToneIdListArray addObject:toneId];
            
            if(GAEventDetails.length > 0)
            {
                GAEventDetails = [NSString stringWithFormat:@"%@,",GAEventDetails];
            }
            
            NSString *ToneName = [SIXDGlobalViewController HTMLConversion:[TempDict objectForKey:@"toneName"]];
            GAEventDetails = [NSString stringWithFormat:@"%@%@-%@",GAEventDetails,ToneName,[TempDict objectForKey:@"toneId"]];
            
        }
        
        [SIXDStylist setGAEventTracker:@"Settings" :@"Play selected playlists to callers" :GAEventDetails];
    }
    else if(_SegmentTab.selectedSegmentIndex == 2)
    {
        for(int i=0;i<_Karaoke_ButtonTagArray.count;i++)
        {
             NSMutableDictionary *toneId = [NSMutableDictionary new];
            NSString *tagStr = [_Karaoke_ButtonTagArray objectAtIndex:i];
            int buttonTag = [tagStr intValue];
            TempDict  = [_KaraokeList objectAtIndex:buttonTag];
            [toneId setObject:[TempDict objectForKey:@"toneId"] forKey:@"toneId"];
            [ToneIdListArray addObject:toneId];
            
            if(GAEventDetails.length > 0)
            {
                GAEventDetails = [NSString stringWithFormat:@"%@,",GAEventDetails];
            }
            
            NSString *ToneName = [SIXDGlobalViewController HTMLConversion:[TempDict objectForKey:@"toneName"]];
            GAEventDetails = [NSString stringWithFormat:@"%@%@-%@",GAEventDetails,ToneName,[TempDict objectForKey:@"toneId"]];
            
            
        }
        [SIXDStylist setGAEventTracker:@"Settings" :@"Play selected karoake to callers" :GAEventDetails];
    }
    
    
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ToneIdListArray
                                                       options:0
                                                         error:&error];
    NSString *ToneIds;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        ToneIds = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
  
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];

    NSString *PackName =  [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&aPartyMsisdn=%@&toneIdList=%@&language=%@&packName=%@&priority=0&channelId=%@&activityId=1&serviceId=1",Rno,appDelegate.MobileNumber,ToneIds,appDelegate.Language,PackName,CHANNEL_ID];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/add-tone-to-shuffle",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

- (IBAction)onSelectedSelectionButton:(UIButton *)sender
{
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            [self selectedTunesSelectionButton:sender];
            break;
            
        case 1:
            [self selectedPlaylistSelectionButton:sender];
            break;
            
        case 2:
            [self selectedKaroakeSelectionButton:sender];
            break;
            
        default:
            break;
    }
}


- (void)selectedTunesSelectionButton:(UIButton *)sender
{
    //_ButtonTag = sender.tag;
    bool tagPresent = NO;
    
    for(int i=0;i<_Tunes_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Tunes_ButtonTagArray objectAtIndex:i];
        int buttonTag = [tagStr intValue];
        if(buttonTag == sender.tag)
        {
            [_Tunes_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [sender setImage:[UIImage imageNamed:@"whitecircle" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Tunes_ButtonTagArray addObject:[NSString stringWithFormat:@"%d", (int)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"whitetick" ] forState:UIControlStateNormal];
    }
    
    [self updateSetToneButton];
    
    NSLog(@"Tag List = %@", _Tunes_ButtonTagArray);
    
}


- (void)selectedPlaylistSelectionButton:(UIButton *)sender
{

    NSLog(@"onSelectedCheckBox4Delete_playlist >>>");
    bool tagPresent = NO;
    
    for(int i=0;i<_Playlist_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Playlist_ButtonTagArray objectAtIndex:i];
        int buttonTag = [tagStr intValue];
        if(buttonTag == sender.tag)
        {
            [_Playlist_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [sender setImage:[UIImage imageNamed:@"whitecircle" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Playlist_ButtonTagArray addObject:[NSString stringWithFormat:@"%d", (int)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"whitetick" ] forState:UIControlStateNormal];
    }
    
    [self updateSetToneButton];
    
}

- (void)selectedKaroakeSelectionButton:(UIButton *)sender
{

    NSLog(@"onSelectedCheckBox4Delete_Karoake >>>");
    bool tagPresent = NO;
    
    for(int i=0;i<_Karaoke_ButtonTagArray.count;i++)
    {
        NSString *tagStr = [_Karaoke_ButtonTagArray objectAtIndex:i];
        int buttonTag = [tagStr intValue];
        if(buttonTag == sender.tag)
        {
            [_Karaoke_ButtonTagArray removeObject:[NSString stringWithFormat:@"%ld", sender.tag]];
            [sender setImage:[UIImage imageNamed:@"whitecircle" ] forState:UIControlStateNormal];
            tagPresent = YES;
            break;
        }
        
    }
    
    if(tagPresent == NO)
    {
        NSLog(@"Adding new tag = %ld", sender.tag);
        [_Karaoke_ButtonTagArray addObject:[NSString stringWithFormat:@"%d", (int)sender.tag]];
        [sender setImage:[UIImage imageNamed:@"whitetick" ] forState:UIControlStateNormal];
    }
    
    [self updateSetToneButton];
    
    
}

-(void)updateSetToneButton
{
    //enable settone button only if one or more tone is selected
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_SetButton setTintColor:UIColorFromRGB(CLR_BUY)];
    [_SetButton setUserInteractionEnabled:false];
    
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            
            if(_Tunes_ButtonTagArray.count > 0)
            {
                [_SetButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                [_SetButton setUserInteractionEnabled:true];
            }
            
            if(_Tunes_listToneApk.count <= 0)
            {
                _rbtMode = TUNES_RBTMODE;
                appDelegate.APIFlag=LIST_TONES_PLAYLIST;
                [self API_ListTones];
                
            }
            else
            {
                [_AlertView setHidden:true];
            }
            
            break;
            
        case 1:
            
            if(_Playlist_ButtonTagArray.count > 0)
            {
                [_SetButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                [_SetButton setUserInteractionEnabled:true];
            }
            
            if(_Playlist_listToneApk.count <= 0)
            {
                _rbtMode = PLAYLIST_RBTMODE;
                appDelegate.APIFlag=LIST_TONES_PLAYLIST;
                [self API_ListTones];
                
            }
            else
            {
                [_AlertView setHidden:true];
            }
            
            break;
            
        case 2:
            if(_Karaoke_ButtonTagArray.count > 0)
            {
                [_SetButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                [_SetButton setUserInteractionEnabled:true];
            }
            
            if(_KaraokeList.count <= 0)
            {
                [self API_GetKaraokeList];
            
            }
            else
            {
                [_AlertView setHidden:true];
            }
            
            break;
            
            
        default:
            break;
    }
}

-(void)changeAlertText
{
    _ActionType = ActionType_REDIRECT_TO_HOME;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *AlertView = [_AlertView.subviews objectAtIndex:0];
    for (UIView *view in AlertView.subviews)
    {
        if([view isKindOfClass:[SIXDCustomButton class]])
        {
            SIXDCustomButton *Button = (SIXDCustomButton *)view;
            if(Button.tag == 2)
            {
                if(_SegmentTab.selectedSegmentIndex == 0)
                {
                    [Button setTitle:NSLocalizedStringFromTableInBundle(@"GET YOUR TUNES",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                   
                }
                else if (_SegmentTab.selectedSegmentIndex == 1)
                {
                    [Button setTitle:NSLocalizedStringFromTableInBundle(@"GET YOUR PLAYLIST",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                }
                else if (_SegmentTab.selectedSegmentIndex == 2)
                {
                    [Button setTitle:NSLocalizedStringFromTableInBundle(@"GET YOUR KARAOKE TUNE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
                }
            }
        }
    }
    
}


-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_IsRefreshRequired)
    {
    switch (_SegmentTab.selectedSegmentIndex)
    {
        case 0:
            _IsRefreshRequired = false;
            _rbtMode=TUNES_RBTMODE;
            appDelegate.APIFlag=LIST_TONES_TONES;
            [self API_ListTones];
            break;
            
        case 1:
            _IsRefreshRequired = false;
            _rbtMode = PLAYLIST_RBTMODE;
            appDelegate.APIFlag=LIST_TONES_PLAYLIST;
            [self API_ListTones];
            break;
            
        case 2:
            _IsRefreshRequired = false;
            [self API_GetKaraokeList];
            
        default:
            break;
    }
    }
}

@end
