//
//  SIXDProfileVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDProfileVC : ViewController <UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *CoverView;
@property (weak, nonatomic) IBOutlet UIImageView *ProfilePicture;
@property(strong,nonatomic) NSArray *Titles;
@property(strong,nonatomic) NSMutableDictionary *ProfileDetails;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet UIButton *CameraButton;

@property(strong,nonatomic) UIScrollView *ScrollView;
@property(strong,nonatomic) NSMutableDictionary *SubButtonList;

@property(nonatomic) BOOL IsInitialLoad;

@property(nonatomic) BOOL IsChangeNumberInvoked;
@end
