//
//  SIXDSalatiVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>
#import "SIXDAVPlayer.h"

#define SALATI_CHECK                901
#define GET_TONE_PRICE_SALATI       902
#define SALATI_SUBSCRIBE            903

@interface SIXDSalatiVC : ViewController<UIScrollViewDelegate,AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *SalatiImage;

@property (weak, nonatomic) IBOutlet UIButton *SubscribeButton;

@property (weak, nonatomic) IBOutlet UILabel *MainHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *InfoLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UIButton *PlayButton;

- (IBAction)onClickedSubscribeButton:(UIButton *)sender;
- (IBAction)onClickedPlayButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property(strong,nonatomic) SIXDAVPlayer *SIXDAVPlayer_Audio;

@property (weak, nonatomic) IBOutlet UIView *GiftView;
@property (weak, nonatomic) IBOutlet UIView *WishlistView;
@property (weak, nonatomic) IBOutlet UIView *TAFView;
@property (weak, nonatomic) IBOutlet UIView *ShareView;

@property (weak, nonatomic) IBOutlet UIImageView *GiftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *WishlistImageView;
@property (weak, nonatomic) IBOutlet UIImageView *TAFImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ShareImageView;

@property (weak, nonatomic) IBOutlet UILabel *GiftLabel;
@property (weak, nonatomic) IBOutlet UILabel *WishlistLabel;
@property (weak, nonatomic) IBOutlet UILabel *TAFLabel;
@property (weak, nonatomic) IBOutlet UILabel *ShareLabel;

@property (strong,nonatomic)UIView *AlertView;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *GiftViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *WishListViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TAFViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ShareViewWidth;

@property(nonatomic) BOOL IsCheckSalatiStatusInvoked;
@property(nonatomic) BOOL IsInitialLoad;

@property(nonatomic) int APIInvoked;

@end
