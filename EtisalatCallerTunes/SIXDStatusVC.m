//
//  SIXDStatusVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDStatusVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDBuyToneVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDPlayerVC.h"
#import "SIXDStylist.h"

@interface SIXDStatusVC ()

@end

@implementation SIXDStatusVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Status"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"STATUS",nil,appDelegate.LocalBundel,nil);
    
    _InfoLabel.text = NSLocalizedStringFromTableInBundle(@"Share your Status with your callers when you are unavailable to respond",nil,appDelegate.LocalBundel,nil);
    
    _SIXDAVPlayer_Audio = [[SIXDAVPlayer alloc]init];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    NSLog(@"API_ListTones >>");
    [self API_Status];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _StatusDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *Dict = [_StatusDetails objectAtIndex:indexPath.row];
    cell.TitleLabel.text = [[Dict objectForKey:@"toneName"] capitalizedString];
    cell.FirstButton.tag = indexPath.row;
    
    UIImage* image = [[UIImage imageNamed:@"justforyoubuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.FirstButton setImage:image forState:UIControlStateNormal];
    [cell.FirstButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
    
    NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    cell.GradientImageView.backgroundColor = [UIColor blackColor];
    cell.GradientImageView.alpha = 0.4;
    
    cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    cell.SubView.layer.borderWidth = 0.5;
    
    cell.AccessoryButton.tag = indexPath.row;
    [cell.AccessoryButton setImage:[UIImage imageNamed:@"pauserow"] forState:UIControlStateSelected];
    [cell.AccessoryButton setImage:[UIImage imageNamed:@"playrow"] forState:UIControlStateNormal];
    
     [cell.contentView bringSubviewToFront:cell.AccessoryButton];
    
    int x_Position;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        x_Position=0;
    }
    else
    {
        x_Position=50-1;
    }
    
    UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
    leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
    
    [cell.FirstButton addSubview:leftBorder1];
    
    return cell;
}

- (IBAction)onClickedBuyButton:(UIButton *)sender
{
    [_SIXDAVPlayer_Audio stopPlay];
    _SelectedButton.selected = !_SelectedButton.selected;
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.ToneDetails= [_StatusDetails objectAtIndex:sender.tag];
    ViewController.IsSourceStatus = TRUE;
    [self presentViewController:ViewController animated:YES completion:nil];
}

-(void)API_Status
{
    [self.view addSubview:_overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];
    
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=%@&pageNo=1&perPageCount=20&sortBy=Order_By",URL,appDelegate.Language,[appDelegate.AppSettings objectForKey:@"STATUS"]];
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];

    _StatusDetails=[responseMap objectForKey:@"searchList"];
    [_TableView reloadData];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    NSLog(@"onFailureResponseRecived Response = %@",Response);
    
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (void)onPlayStopped
{
    _SelectedButton.selected = !_SelectedButton.selected;
    _SelectedButton = nil;
}

-(void)onPlayStarted
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
        SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        [cell.ActivityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:true];
    });
    
}

-(void)onPlayFailure
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
        SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        [cell.ActivityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:true];
        _SelectedButton.selected = !_SelectedButton.selected;
        _SelectedButton = nil;
    });
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [_SIXDAVPlayer_Audio stopPlay];
    _SelectedButton = nil;
    _SIXDAVPlayer_Audio = nil;
}

- (IBAction)onSelectedPlayPauseButton:(UIButton *)sender
{
    NSLog(@"onSelectedPlayPauseButton>>>");
    [_SIXDAVPlayer_Audio stopPlay];
    
    if(_SelectedButton.tag != sender.tag)
    {
        _SelectedButton.selected = !_SelectedButton.selected;
    }
    
    sender.selected = !sender.selected;
    _SelectedButton = sender;
    if(sender.selected)
    {
        NSLog(@"playing>>>>");
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
        SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        [cell.ActivityIndicator startAnimating];
        [self.view setUserInteractionEnabled:false];
        
        NSMutableDictionary *Dict = [_StatusDetails objectAtIndex:sender.tag];
        _SIXDAVPlayer_Audio.SIXVCCallBack = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [_SIXDAVPlayer_Audio initialisePlayer :[Dict objectForKey:@"toneUrl"]];
        });
        
    }
    else
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _SelectedButton = nil;
        NSLog(@"play stop>>>>");
    }
}
@end
