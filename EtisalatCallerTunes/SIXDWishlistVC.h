//
//  SIXDWishlistVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"


@interface SIXDWishlistVC : ViewController<UITableViewDelegate,UITableViewDataSource>

//Dynamic Header Height
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;

@property (weak, nonatomic) IBOutlet UIButton *DeleteButton;

@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet UIView *SegmentView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;
/*
@property (strong, nonatomic) IBOutlet UILabel *LikedTonesLabel;
@property (strong, nonatomic) IBOutlet UILabel *PlaylistLabel;
 */
@property (nonatomic) int typeFlag;

@property (weak, nonatomic) IBOutlet UITableView *TunesTableView;
@property (weak, nonatomic) IBOutlet UITableView *PlaylistTableView;
@property (weak, nonatomic) IBOutlet UITableView *PreBookTableView;

@property(strong,nonatomic) NSMutableArray *WishlistTuneArray;
@property(strong,nonatomic) NSMutableArray *WishlistPlaylist;
@property(strong,nonatomic) NSMutableArray *WishlistPreBooklist;

//@property(strong,nonatomic) NSMutableDictionary *PlayListDetails;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property(strong,nonatomic) NSMutableArray *Tunes_ButtonTagArray;
@property(strong,nonatomic) NSMutableArray *Playlist_ButtonTagArray;
//@property(strong,nonatomic) NSMutableArray *Prebook_ButtonTagArray;

@property NSInteger SelectedPreBookButtonTag;

@property (weak, nonatomic) IBOutlet UIView *AlertView;

- (IBAction)onSelectedPreBookDeleteButton:(UIButton *)sender;

@property(nonatomic) BOOL IsInitialLoad;

- (IBAction)onClickedAccessoryButton:(UIButton *)sender;
@end
