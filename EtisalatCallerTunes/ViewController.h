//
//  ViewController.h
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 07/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(void)regenerateTokenFailed;
-(void)regenerateTokenSuccess;

-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response;
-(void)onSuccessResponseRecived:(NSMutableDictionary*)Response :(NSString*) URL;
-(void)onFailureResponseRecived:(NSMutableDictionary*)Response;
-(void)onFailedToConnectToServer;
-(void)onInternetBroken;

-(void)onSelectedPopUpConfirmButton;

- (void)onRecordingStopped;
- (void)onPlayStopped;
- (void)onPlayStarted;
- (void)onPlayFailure;
@end

//@interface SIXDAVController : UIViewController

