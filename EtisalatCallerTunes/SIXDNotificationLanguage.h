//
//  SIXDNotificationLanguage.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/12/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@class SIXDProfileVC;


@interface SIXDNotificationLanguage : ViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentBar;
@property(nonatomic) int Source;
- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property(strong,nonatomic) SIXDProfileVC *ProfileVC;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onSelectedConfirmButton:(UIButton *)sender;

@property(nonatomic) NSString* NotificationLanguage;
@property (weak, nonatomic) IBOutlet  UILabel *TitleLabel;
- (IBAction)onSelectedCloseButton:(UIButton *)sender;
@end
