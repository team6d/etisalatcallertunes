//
//  SIXDChangePasswordVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 07/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDChangePasswordVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDStylist.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHttpRequestHandler.h"
#import "CustomLabel.h"
#import "SIXDValidations.h"

@interface SIXDChangePasswordVC ()

@end

@implementation SIXDChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Change_Password"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"CHANGE PASSWORD",nil,appDelegate.LocalBundel,nil);
    
    _ScrollView.delegate = self;
    
    _AlertLabel.text = @"";
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"SET NEW PASSWORD",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    _TextFieldList = [NSMutableArray new];
    
    
    for (int i=0; i<3; i++)
    {
        [_TextFieldList addObject:@0];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    _AlertLabel.textColor = [UIColor redColor];
    
    _PlaceHolderList = @[NSLocalizedStringFromTableInBundle(@"Current Password*",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"New Password*",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"Confirm New Password*",nil,appDelegate.LocalBundel,nil)];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    [textField resignFirstResponder];
    return NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void) dismissKeyboard
{
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    [_NumberTextField resignFirstResponder];
}

- (IBAction)onClickedConfirmButton:(UIButton *)sender
{
    [_NumberTextField resignFirstResponder];
    
    
    //FOR TESTING - REMOVE
    //To show normal alert view
    /*
    _AlertView = [SIXDGlobalViewController  showDefaultAlertMessage:@"opps something went wrong"];
    [_ScrollView addSubview:_AlertView];
     */
    
    //to show alert view with action
    /*
   _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :@"opps something went wrong" :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];
     */
    //end
    
    
    
    if([self validateTextFieldDetails])
    {
        [_ActivityIndicator startAnimating];
        [_ConfirmButton setUserInteractionEnabled:false];
        _ConfirmButton.alpha = 0.3;
        [self API_GetSecurityToken];
       
    }
    
}

-(void)API_GetSecurityToken
{
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SECURITY_TOKEN;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/security-token",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?",URL];
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing >>>");
    
    [self changeCellColor:false:0];
    [self changeCellColor:false:1];
    [self changeCellColor:false:2];
    [_AlertLabel setHidden:true];
    
    [_ScrollView setContentOffset:CGPointMake(0,100) animated:YES];
    _NumberTextField=textField;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    cell.TitleLabel.hidden=NO;
    [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    cell.TopSpaceToTextField.constant=19;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidEndEditing >>>");
    
    [_ScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:textField.tag inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    if(textField.text.length==0)
    {
    
        [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:[_PlaceHolderList objectAtIndex:textField.tag] attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        
        cell.TitleLabel.hidden=YES;
        cell.TopSpaceToTextField.constant=0;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.FirstTextField.tag=indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;
    
    cell.TitleLabel.text = [_PlaceHolderList objectAtIndex:indexPath.row];
    
    [cell.FirstTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:[_PlaceHolderList objectAtIndex:indexPath.row] attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
    [cell.AccessoryButton setHidden:true];
    
    cell.FirstTextField.delegate = self;
    cell.FirstTextField.keyboardType = ASCII_TXT_KEYBOARD;
    
    if(indexPath.row < _TextFieldList.count)
    {
        [_TextFieldList replaceObjectAtIndex:indexPath.row withObject:cell.FirstTextField];
    }
    
    cell.TitleLabel.hidden = YES;
    cell.FirstTextField.delegate = self;
    cell.FirstTextField.enablesReturnKeyAutomatically = NO;
    
    cell.SubView.layer.borderWidth = 1.0f;
    cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    return cell;
    
}


-(void)API_ChangePassword
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= PASSWORD_CHANGE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    ReqHandler.IsURLEncodingNotRequired = true;
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&language=%@&identifier=ChangePassword&msisdn=%@&newPassword=%@&oldPassword=%@&securityCounter=%@&os=ios",Rno,appDelegate.Language,appDelegate.MobileNumber,_EncryptedNewPassword,_EncryptedOldPassword,_SecurityToken];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/change-password",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}



-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.APIFlag==SECURITY_TOKEN)
    {
        Response = [Response objectForKey:@"responseMap"];
        _SecurityToken =[Response objectForKey:@"securityCounter"];
        appDelegate.SecurityToken=_SecurityToken;
        [self EncryptPassord];
        
    }
    else
    {
    
    NSString *message = [Response objectForKey:@"message"];
    
        [_ActivityIndicator stopAnimating];
        _ConfirmButton.alpha =1;
        //showing alert
        _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction_Tick :message :false :_ScrollView.frame :self];
        [_ScrollView setHidden:true];
        [self.view addSubview:_AlertView];
        //end
        
    }
    
}

-(void)EncryptPassord
{
    NSURL *jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt" withExtension:@"js"];
    NSString *jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    NSString *oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main.js";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:nil ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt.min" withExtension:@"js"];
    jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main1.js";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
     UITextField * NewPassword = [_TextFieldList objectAtIndex:1];
    NSString *Temp = [NSString stringWithFormat:@"%@%@",NewPassword.text,_SecurityToken];
    
    NSLog(@"new Password = %@",Temp);
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"SearchKey" withString:Temp];
    
    NewPassword = [_TextFieldList objectAtIndex:0];
    Temp = [NSString stringWithFormat:@"%@%@",NewPassword.text,_SecurityToken];
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"OldPassword" withString:Temp];
    
    NSLog(@"old Password = %@",Temp);
    
    
    _webView = [UIWebView new];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    _webView.delegate = self;
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *NewPassword = [_webView stringByEvaluatingJavaScriptFromString:@"getLanguage()"];
    NSString *OldPassword = [_webView stringByEvaluatingJavaScriptFromString:@"getEncryptedPassword()"];
    
    /*
    _EncryptedNewPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,(CFStringRef)NewPassword,
                                                                                               NULL,
                                                                                               (CFStringRef)@"+",
                                                                                               kCFStringEncodingUTF8 ));
    _EncryptedOldPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                  NULL,(CFStringRef)OldPassword,
                                                                                                  NULL,
                                                                                                  (CFStringRef)@"+",
                                                                                                  kCFStringEncodingUTF8 ));
     */
    
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    _EncryptedNewPassword = [NewPassword
                          stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                          ];
    
    _EncryptedOldPassword = [OldPassword
                          stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                          ];
    
     [self API_ChangePassword];
    
    
}


-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_ActivityIndicator stopAnimating];
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
    
    NSString *message = [Response objectForKey: @"message"];
    
    if(message.length <= 0)
    {
        message = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    }
    
    //showing alert
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :message  :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];
    //end
    
}


-(void)onFailedToConnectToServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_ActivityIndicator stopAnimating];
    
    _ConfirmButton.alpha = 1;
    [_ConfirmButton setUserInteractionEnabled:true];
   
    //showing alert
    _AlertView = [SIXDGlobalViewController showDefaultAlertWithAction :NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil)  :true :_ScrollView.frame :self];
    [_ScrollView setHidden:true];
    [self.view addSubview:_AlertView];
    //end
    
    
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(BOOL)validateTextFieldDetails
{
    UITextField * TextField;
    BOOL Status = TRUE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for(int i=0;i<_TextFieldList.count;i++)
    {
        TextField = [_TextFieldList objectAtIndex:i];
        if(TextField.text.length <= 0)
        {
            [self changeCellColor:true:i];
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
            Status = false;
        }
    }
    
    
    if(Status)
    {
        TextField = [_TextFieldList objectAtIndex:0];
        UITextField * TextField1 = [_TextFieldList objectAtIndex:1];
        UITextField * TextField2 = [_TextFieldList objectAtIndex:2];
        
        //check if old password is same as new password
        if([TextField.text isEqualToString:TextField1.text])
        {
            [self changeCellColor:true:0];
            [self changeCellColor:true:1];
            Status =false;
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"Old password is same as new password",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
            
        }
        //end
        
        if(Status)
        {
            if(TextField1.text.length<8)
            {
                _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"MIN_PASSWORD_LENGTH_VALIDATION",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
                Status =false;
                [self changeCellColor:true:1];
            }
        }
        
        if(Status)
        {
            if(![TextField1.text isEqualToString:TextField2.text])
            {
                [self changeCellColor:true:1];
                [self changeCellColor:true:2];
                Status =false;
                _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"passwords are not matching",nil,appDelegate.LocalBundel,nil);
                [_AlertLabel setHidden:false];
            }
        }
        //password_condition
        /*
        BOOL ValidationFlag = [GlobalInterfaceForValidations fnG_ValidatePassword:TextField.text];
        if(ValidationFlag==FALSE)
        {
            [self changeCellColor:true:1];
            [self changeCellColor:true:2];
            Status =false;
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"password_policy",nil,appDelegate.LocalBundel,nil);
            [_AlertLabel setHidden:false];
        }
        */
        
    }
    
    return Status;
}

-(void)viewDidAppear:(BOOL)animated
{
    _AlertLabel.hidden = YES;
    [self changeCellColor: false:0];
    [self changeCellColor: false:1];
    [self changeCellColor: false:2];
}


-(void)changeCellColor:(BOOL)Error :(int)index
{
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:index inSection:0] ;
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(Error)
    {
        [SIXDStylist setTextViewAlert:cell.FirstTextField :cell.SubView :cell.AccessoryButton];
         [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:[_PlaceHolderList objectAtIndex:index] attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    }
    else
    {
        [SIXDStylist setTextViewNormal:cell.FirstTextField :cell.SubView :cell.AccessoryButton];
         [cell.FirstTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:[_PlaceHolderList objectAtIndex:index] attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    }
    
}


-(void)onSelectedPopUpConfirmButton
{
   [self.navigationController popViewControllerAnimated:false];
}

@end
