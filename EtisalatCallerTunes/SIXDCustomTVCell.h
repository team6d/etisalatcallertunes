//
//  SIXDCustomTVCell.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/21/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIXDCustomTVCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>

//Custom Table view cell contents - embeds collection view
- (IBAction)onSeletedAccessoryButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;

- (IBAction)onSelectedLikeButton:(UIButton *)sender;
@property(strong,nonatomic) NSMutableArray *FirstRow;

//Custom Table View Cell Contents
@property (strong, nonatomic) IBOutlet UIView *SubView;
@property (weak, nonatomic) IBOutlet UIImageView *CustomImageView;
@property (weak, nonatomic) IBOutlet UIImageView *GradientImageView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *SubTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *LikeButton;
@property (strong, nonatomic) IBOutlet UIButton *AccessoryButton;
@property (weak, nonatomic) IBOutlet UIButton *BuyButton;
@property (weak, nonatomic) IBOutlet UIButton *StatusButton;
@property (weak, nonatomic) IBOutlet UILabel *DetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *DetailLabel2;
@property (weak, nonatomic) IBOutlet UIButton *SelectionButton;

@property (strong, nonatomic) IBOutlet UITextField *FirstTextField;
@property (weak, nonatomic) IBOutlet UITextField *SecondTextField;
@property (weak, nonatomic) IBOutlet UIButton *FirstButton;
@property (weak, nonatomic) IBOutlet UIButton *SecondButton;

//TAF
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (weak, nonatomic) IBOutlet UILabel *LikeLabel;
@property(nonatomic) int Source;

@end
