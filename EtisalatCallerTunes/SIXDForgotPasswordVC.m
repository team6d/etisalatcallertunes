//
//  SIXDForgotPasswordVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 04/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDForgotPasswordVC.h"
#import "SIXDStylist.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDAlertViewVC.h"
#import "SIXDVerifyOtpVC.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDValidations.h"

@interface SIXDForgotPasswordVC ()

@end

@implementation SIXDForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Forget_Password_Enter_New_Password"];
    [SIXDStylist setBackGroundImage:self.view];
    
    [_OtpTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_NewPasswordTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    [_ConfirmPasswordTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    
    _OtpTextField.keyboardType = ASCII_NUM_KEYBOARD;
    _NewPasswordTextField.keyboardType = ASCII_TXT_KEYBOARD;
    _ConfirmPasswordTextField.keyboardType = ASCII_TXT_KEYBOARD ;
    
    
    [self InitialCondition];
    
    _OtpTextField.enablesReturnKeyAutomatically = NO;
    _NewPasswordTextField.enablesReturnKeyAutomatically = NO;
    _ConfirmPasswordTextField.enablesReturnKeyAutomatically = NO;
    
    _OtpTextField.tag=1;
    _NewPasswordTextField.tag=2;
    _ConfirmPasswordTextField.tag=3;
    
    _OtpTextField.delegate = self;
    _NewPasswordTextField.delegate = self;
    _ConfirmPasswordTextField.delegate = self;
    
    _OtpLabel.hidden = YES;
    _NewPasswordLabel.hidden = YES;
    _ConfirmPasswordLabel.hidden = YES;
    

    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Verification Code*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_NewPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"New Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil) attributes:@{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"RESET MY PASSWORD",nil,appDelegate.LocalBundel,nil);
    _OtpLabel.text = NSLocalizedStringFromTableInBundle(@"Verification Code*",nil,appDelegate.LocalBundel,nil);
    _NewPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"New Password*",nil,appDelegate.LocalBundel,nil);
    _ConfirmPasswordLabel.text = NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil);
    
    [_ResetPasswordButton setTitle:NSLocalizedStringFromTableInBundle(@"RESET PASSWORD",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Resend Verification Code (OTP)",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:FONT_TEXTFIELD size:13]}];
    [_ResendButton setAttributedTitle:titleString forState:UIControlStateNormal];
    
    
    _ImageSetFlag = FALSE;
    
    UIImage* image = [[UIImage imageNamed:@"errorinputicon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_OtpButton setImage:image forState:UIControlStateNormal];
    
    [_NewPasswordButton setImage:image forState:UIControlStateNormal];
    
    [_ConfirmPasswordButton setImage:image forState:UIControlStateNormal];
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    _ActivityIndicator.hidden=YES;
    
    [SIXDStylist addKeyBoardDoneButton:_OtpTextField :self];
    
   
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    [_OtpTextField resignFirstResponder];
    [_NewPasswordTextField resignFirstResponder];
    [_ConfirmPasswordTextField resignFirstResponder];
    
    
    _AlertLabel.hidden=YES;
    
    if(_OtpTextField.text.length==0||_OtpTextField.text.length>3)
    {
        [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_OtpButton];
    }
    if(_NewPasswordTextField.text.length==0||_NewPasswordTextField.text.length>4)
    {
        [SIXDStylist setTextViewNormal:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
    }
    if(_ConfirmPasswordTextField.text.length==0||_ConfirmPasswordTextField.text.length>4)
    {
        [SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickedResetPasswordButton:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.textColor = UIColorFromRGB(CLR_ALERTRED);
    
    int count=5;
    _AlertLabel.text=@"";
    if(_OtpTextField.text.length==0)
    {
        count--;
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_OtpTextField :_OtpView :_OtpButton];
    }
    if(_NewPasswordTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
    }
    if(_ConfirmPasswordTextField.text.length==0)
    {
        count--;
        if(_AlertLabel.text.length==0)
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
        return;
    }
    
    //password_condition
    /*
    BOOL ValidationFlag = [GlobalInterfaceForValidations fnG_ValidatePassword:_NewPasswordTextField.text];
    if(ValidationFlag==FALSE)
    {
        count--;
        if(_AlertLabel.text.length==0)
        _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"password_policy",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
        return;
        
    }
    */
    
    if(_NewPasswordTextField.text.length<8)
    {
        count--;
        if(_AlertLabel.text.length==0)
        {
            _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"MIN_PASSWORD_LENGTH_VALIDATION",nil,appDelegate.LocalBundel,nil);
        }
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
        return;
    }
    
    
    
    if(![_NewPasswordTextField.text isEqualToString:_ConfirmPasswordTextField.text])
    {
        count--;
        if(_AlertLabel.text.length==0)
        _AlertLabel.text= NSLocalizedStringFromTableInBundle(@"passwords are not matching",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        
        [SIXDStylist setTextViewAlert:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
        [SIXDStylist setTextViewAlert:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
        return;
    }
    if(count==5)
    {
        [SIXDStylist setGAEventTracker:@"Password" :@"Reset password" :nil];
        NSLog(@"API_GetSecurityToken call>>");
        [self InitialCondition];
        [self API_GetSecurityToken];
    }
}

- (IBAction)onClickedResendButton:(UIButton *)sender
{
    _OtpTextField.text = @"";
    _NewPasswordTextField.text = @"";
    _ConfirmPasswordTextField.text = @"";
    [self textFieldDidEndEditing:_OtpTextField];
    [self textFieldDidEndEditing:_NewPasswordTextField];
    [self textFieldDidEndEditing:_ConfirmPasswordTextField];
    [self InitialCondition];

    [self API_ResendSendOtp];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(textField.tag==1)
    {
        [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_OtpButton];
        _OtpLabel.hidden=NO;
        [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToOtp.constant=19;
    }
    else if(textField.tag==2)
    {
        [SIXDStylist setTextViewNormal:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
        _NewPasswordLabel.hidden=NO;
        [_NewPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToNewPassword.constant=19;
    }
    else
    {
        [SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
        _ConfirmPasswordLabel.hidden=NO;
        [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToConfirmPassword.constant=19;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
   // _AlertLabel.hidden=YES;
    
   // [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_OtpButton];
    //[SIXDStylist setTextViewNormal:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
    //[SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(textField.tag==1)
    {
        if(_OtpTextField.text.length==0)
        {
            [_OtpTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Verification Code*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _OtpLabel.hidden=YES;
            _TopSpaceToOtp.constant=0;
        }
    }
    else if(textField.tag==2)
    {
        if(_NewPasswordTextField.text.length==0)
        {
            [_NewPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"New Password*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _NewPasswordLabel.hidden=YES;
            _TopSpaceToNewPassword.constant=0;
        }
    }
    else
    {
        if(_ConfirmPasswordTextField.text.length==0)
        {
            [_ConfirmPasswordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Confirm Password*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _ConfirmPasswordLabel.hidden=YES;
            _TopSpaceToConfirmPassword.constant=0;
        }
    }
}


-(void)InitialCondition
{
    _AlertLabel.hidden=YES;
    
    [SIXDStylist setTextViewNormal:_OtpTextField :_OtpView :_OtpButton];
    [SIXDStylist setTextViewNormal:_NewPasswordTextField :_NewPasswordView :_NewPasswordButton];
    [SIXDStylist setTextViewNormal:_ConfirmPasswordTextField :_ConfirmPasswordView :_ConfirmPasswordButton];
}

-(void)API_ResetPassword
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
  
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=PASSWORD_CHANGE;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    ReqHandler.IsURLEncodingNotRequired = true;
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&encryptedPassword=%@&clientTxnId=%@&otp=%@&secToc=%@&os=ios&securityCounter=%@&language=%@",appDelegate.MobileNumber,_EncryptedPassword,Rno,_OtpTextField.text,_secToc,_SecurityToken,appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/reset-password",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];

}




-(void)API_GetSecurityToken
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=SECURITY_TOKEN;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/security-token",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?",URL];
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
   
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==SECURITY_TOKEN)
    {
        NSLog(@"API_GetSecurityToken onSuccessResponseRecived = %@",Response);
        
        NSMutableDictionary *responseMap = [Response objectForKey:@"responseMap"];
        _SecurityToken =[responseMap objectForKey:@"securityCounter"];
        appDelegate.SecurityToken=_SecurityToken;
        [self EncryptPassord];
        
    }
    else if (appDelegate.APIFlag == SEND_OTP)
    {
        [_ActivityIndicator stopAnimating];
        [self.overlayview removeFromSuperview];
        
        _AlertLabel.text =  NSLocalizedStringFromTableInBundle(@"OTP has been resent to your mobile number",nil,appDelegate.LocalBundel,nil);
        
        _AlertLabel.textColor = [UIColor whiteColor];
        [_AlertLabel setHidden:false];
        Response = [Response objectForKey:@"responseMap"];
        _secToc = [Response objectForKey:@"secToc"];
       
    }
    else
    {
        [_ActivityIndicator stopAnimating];
        [self.overlayview removeFromSuperview];
        [self InitialCondition];
        
        NSLog(@"API_ResetPassword onSuccessResponseRecived = %@",Response);
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDAlertViewVC"];
        appDelegate.APIFlag=PASSWORD_CHANGE;
        [self presentViewController:ViewController animated:YES completion:nil];
        
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"API_ResetPassword onFailureResponseRecived = %@",Response);
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
   
    
    NSString *message = [Response objectForKey:@"message"];
    NSLog(@"message = %@", message);
    
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = message;
    
    [SIXDStylist setTextViewAlert:_OtpTextField :_OtpView :_OtpButton];
    
}

-(void)onFailedToConnectToServer
{
    NSLog(@"API_ResetPassword onFailedToConnectToServer");
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [_AlertLabel setHidden:NO];
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)EncryptPassord
{
    NSURL *jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt" withExtension:@"js"];
    NSString *jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    NSString *oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main.js";
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:nil ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    jsFilepath = [[NSBundle mainBundle] URLForResource:@"jsencrypt.min" withExtension:@"js"];
    jsfilepathStr = [NSString stringWithFormat:@"%@",jsFilepath];
    oldpath = @"/Users/sixdeetechnologies/Documents/sqlexampletest/SQLExample2/main1.js";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:oldpath withString:jsfilepathStr];
    
    NSString *Temp = [NSString stringWithFormat:@"%@%@",_NewPasswordTextField.text,_SecurityToken];
    
    NSLog(@"Password = %@",Temp);
    
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"SearchKey" withString:Temp];
    _webView = [UIWebView new];
    [_webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    _webView.delegate = self;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *pwd = [_webView stringByEvaluatingJavaScriptFromString:@"getLanguage()"];
    
    /*_EncryptedPassword = (NSString* )CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                               NULL,(CFStringRef)pwd,
                                                                                               NULL,
                                                                                               (CFStringRef)@"+",
                                                                                               kCFStringEncodingUTF8 ));
     */
    NSCharacterSet * queryKVSet = [NSCharacterSet
                                   characterSetWithCharactersInString:@"+"
                                   ].invertedSet;
    
    _EncryptedPassword = [pwd
                          stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet
                          ];
    NSLog(@"API_PasswordValidation call >>");
    [self API_ResetPassword];
    
}

-(void)API_ResendSendOtp
{
    [self.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    appDelegate.APIFlag = SEND_OTP;
    
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.HttpsReqType = POST;
    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@",appDelegate.MobileNumber,Rno,appDelegate.Language];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/send-otp-wp",appDelegate.BaseMiddlewareURL];
//
//    ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&clientTxnId=%@&language=%@",appDelegate.MobileNumber,Rno,appDelegate.Language];
//    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/send-otp-wp",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    
}

@end
