//
//  ViewController.h
//  6dCRBTApp
//
//  Created by Shahnawaz Lone on 8/1/16.
//  Copyright © 2016 6d. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIXDPageContentVC.h"
#import "ViewController.h"

@interface SIXDPageViewController : ViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIPageControl *PageControl;

@property (strong, nonatomic) UIPageViewController *pageViewController;
//@property (strong, nonatomic) NSMutableArray *pageImages;
@property (strong, nonatomic) NSMutableString *httpsResponse;

@property(nonatomic) NSInteger CurrentIndex;
@property(nonatomic) NSTimer *Timer;
@property(nonatomic) NSInteger HttpStatusCode;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (strong,nonatomic)UIView *overlayview;

@property (strong, nonatomic) NSMutableArray *BannerDetailsList;

@property(nonatomic) int Source;
-(void)loadBanner;

@end

