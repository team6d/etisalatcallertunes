//
//  SIXDArtistSearchVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDCustomTVCell.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"

@interface SIXDArtistSearchVC : ViewController
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property(strong,nonatomic) NSMutableArray *ArtistData;

@property(nonatomic) BOOL IsLoading;
@property(nonatomic) NSUInteger TempResponseCount;
@property(nonatomic) NSUInteger PageNumber;
@property(nonatomic) NSUInteger PerPageCount;

@property(strong,nonatomic) NSString* SearchKey;
@end
