//
//  SIXDPlayerVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/11/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDPlayerVC.h"
#import "SIXDBuyToneVC.h"
#import "SIXDGiftToneVC.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDGlobalViewController.h"
#import "SIXDWishlistPopUpVC.h"
#import "SIXDSocialSharePopUpVC.h"
#import "SIXDTellAFriendVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDCommonPopUp_Text.h"

@import Firebase;

@interface SIXDPlayerVC ()

@end

@implementation SIXDPlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Player"];
    
    [_UpperView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    [_LowerView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
    
   
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    [self.view bringSubviewToFront:_ActivityIndicator];
   
    if(_IsDeletePage)
    {
        _LikeButton.hidden = true;
        _LikeTextLabel.hidden = true;
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        if(_IsDeletePage)
        {
            _wishlistButtonCenterX.constant = -38;
            _WishlistView.hidden = YES;
            _WishListViewWidth.constant = 0;
            _SpaceBtwWishlistAndGiftView.constant = 0;
            
            
        }
        else
        {
            _WishListViewWidth.constant = 65;
        }
        _TopSpaceToImage.constant = 70;
        _GiftViewWidth.constant = 65;
        _TAFViewWidth.constant = 65;
        _ShareViewWidth.constant = 65;
        _ToneImageheight.constant = _ToneImageheight.constant -30;
        
        _ToneImageWidth.constant = _ToneImageWidth.constant-30;
        _SpaceBtwImageAndPlayButton.constant = _SpaceBtwImageAndPlayButton.constant-20;
        _UpperViewHeight.constant = _UpperViewHeight.constant - 80;
    }
    else
    {
        if(appDelegate.DeviceSize == iPHONE_X)
        {
            _UpperViewHeight.constant = 530;
        }
        
        if(_IsDeletePage)
        {
            _wishlistButtonCenterX.constant = -48;
            _WishlistView.hidden = YES;
            _WishListViewWidth.constant = 0;
            _SpaceBtwWishlistAndGiftView.constant = 0;
            
        }
    }
    
   
   //[_UpperView setBackgroundColor:[UIColorFromRGB(POPUP_BACKGROUND) colorWithAlphaComponent:0.6]];
    [self.view setBackgroundColor:UIColorFromRGB(POPUP_BACKGROUND)];
    [_UpperView setBackgroundColor:[UIColor clearColor]];
    
    
    if(!_IsDeletePage)
    {
        [_BuyButton setTitle:NSLocalizedStringFromTableInBundle(@"BUY",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    }
    else
    {
        [_BuyButton setTitle:NSLocalizedStringFromTableInBundle(@"DELETE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
        
    }
    
    UIImage *image = [[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_NextButton setImage:image forState:UIControlStateNormal];
    _NextButton.tintColor=UIColorFromRGB(CLR_BUY);
    
    image = [[UIImage imageNamed:@"previous"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_PreviousButton setImage:image forState:UIControlStateNormal];
    _PreviousButton.tintColor=UIColorFromRGB(CLR_BUY);
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImage"]];
    [_ToneImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    [self.view bringSubviewToFront:_BuyButton];
    [self setViewBorder:_GiftView];
    [self setViewBorder:_WishlistView];
    [self setViewBorder:_TAFView];
    [self setViewBorder:_ShareView];
    
    UITapGestureRecognizer *GiftTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleGiftTap:)];
    [self.GiftView addGestureRecognizer:GiftTap];
    
    UITapGestureRecognizer *WishlistTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleWishlistTap:)];
    [self.WishlistView addGestureRecognizer:WishlistTap];
    
    UITapGestureRecognizer *TAFTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTAFTap:)];
    [self.TAFView addGestureRecognizer:TAFTap];
    
    UITapGestureRecognizer *ShareTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleShareTap:)];
    [self.ShareView addGestureRecognizer:ShareTap];
    
    _GiftLabel.text = NSLocalizedStringFromTableInBundle(@"GIFT_SMALL",nil,appDelegate.LocalBundel,nil);
    _WishlistLabel.text = NSLocalizedStringFromTableInBundle(@"WISHLIST_SMALL",nil,appDelegate.LocalBundel,nil);
    _TAFLabel.text = NSLocalizedStringFromTableInBundle(@"Tell a Friend",nil,appDelegate.LocalBundel,nil);
    _ShareLabel.text = NSLocalizedStringFromTableInBundle(@"SHARE_SMALL",nil,appDelegate.LocalBundel,nil);
    
    //added on 4/05/2018 - hiding lower view if "isCopy" = false
    _ToneDetails=[_PlayList objectAtIndex:_CurrentToneIndex];
    if(_IsDeletePage)
    {
        if(![[_ToneDetails objectForKey:@"isCopy"] isEqualToString:@"T"])
        {
            [_LowerView setHidden:true];
        }
    }
    //end
    
    
    if(appDelegate.IsAppOpenedFromURL)
    {
        [self API_CDPSpecificLanguageSearch];
    }
    
}

- (void)handleGiftTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"GiftTune_GetPrice"];
    
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    [SIXDStylist setGAEventTracker:@"Player" :@"Gift request" :GAEventDetails];
    
    NSLog(@"handleGiftTap");
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDGiftToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGiftToneVC"];
    ViewController.ToneDetails=_ToneDetails;
    if (self.tabBarController == nil){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.MainTabController.navigationController presentViewController:ViewController animated:true completion:nil];
    } else {
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    
}

- (void)handleWishlistTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleWishlistTap");
    [SIXDStylist setGAScreenTracker:@"AddToWishlist_Tune"];
    
    //NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",[_ToneDetails objectForKey:@"toneName"],[_ToneDetails objectForKey:@"artistName"],[_ToneDetails objectForKey:@"toneId"]];
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    [SIXDStylist setGAEventTracker:@"Player" :@"Add to wishlist" :GAEventDetails];
    
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDWishlistPopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDWishlistPopUpVC"];
    ViewController.WishListType = 1; //Tunes
    ViewController.ToneDetails=_ToneDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleTAFTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"handleTAFTap");
    [SIXDStylist setGAScreenTracker:@"TellAFriend_Tune"];
    
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    [SIXDStylist setGAEventTracker:@"Player" :@"Tell a friend" :GAEventDetails];
    
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDTellAFriendVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDTellAFriendVC"];
    ViewController.ToneDetails=_ToneDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)handleShareTap:(UITapGestureRecognizer *)recognizer
{
    [SIXDStylist setGAScreenTracker:@"Share_Tune"];
    
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    [SIXDStylist setGAEventTracker:@"Player" :@"Share" :GAEventDetails];
    
    NSLog(@"handleShareTap");
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SIXDSocialSharePopUpVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSocialSharePopUpVC"];
    ViewController.Source = TUNE;
    ViewController.ToneDetails=_ToneDetails;
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onClickedBuyButton:(UIButton *)sender
{
    
   
    
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    
    if(!_IsDeletePage)
    {
        NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
        [SIXDStylist setGAEventTracker:@"Player" :@"Buy tune request" :GAEventDetails];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
        SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
        ViewController.ToneDetails=_ToneDetails;
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.ParentView = self;
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Are you sure you want to delete?",nil,appDelegate.LocalBundel,nil);
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
        
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    
    
}

- (IBAction)onClickedLikeButton:(UIButton *)sender
{
  
    NSLog(@"Player::Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];

    NSString *tuneId = [_ToneDetails objectForKey:@"toneId"];
    /*
    NSString *categoryId = [_ToneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [_ToneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
     */
    NSString *categoryId;
    categoryId = [_ToneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [_ToneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [_ToneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :_LikeTextLabel];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"playlistsolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Player" :@"Liked" :GAEventDetails];
            
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"playlistoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
    
    /* if(_audioPlayer.isPlaying)
    {
        [_audioPlayer stop];
        _isPlayPaused = true;
        
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    */
    
}
- (IBAction)onClickedPlayPauseButton:(UIButton *)sender
{
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    
    if(!_IsPlayerInitialised)
    {
         [SIXDStylist setGAEventTracker:@"Player" :@"Play tune" :GAEventDetails];
        [self PlayTone];
    }
    else
    {
        if(_isPlayPaused)
        {
             [SIXDStylist setGAEventTracker:@"Player" :@"Play tune" :GAEventDetails];

            _isPlayPaused = false;
            [_SIXDAVPlayer_Audio playIO];
            UIImage * image = [[UIImage imageNamed:@"pause"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [_PlayPauseButton setImage:image forState:UIControlStateNormal];
            _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
            
        }
        else
        {
            [SIXDStylist setGAEventTracker:@"Player" :@"Stop tune" :GAEventDetails];
            _isPlayPaused = true;
            [_SIXDAVPlayer_Audio stopPlay];
            UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [_PlayPauseButton setImage:image forState:UIControlStateNormal];
            _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
            
        }
    }
}
- (IBAction)onClickedNextButton:(UIButton *)sender{
    NSLog(@"onNextSongButton");
    
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
     [SIXDStylist setGAEventTracker:@"Player" :@"Next tune" :GAEventDetails];
    
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }

    _NextButton.userInteractionEnabled = true;
    _PreviousButton.userInteractionEnabled = true;
    
    _IsPlayerInitialised = false;
    _CurrentToneIndex++;
    [self viewDidAppear:nil];
    [self PlayTone];
}
- (IBAction)onClickedPreviousButton:(UIButton *)sender{
    
    NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
    [SIXDStylist setGAEventTracker:@"Player" :@"Previous tune" :GAEventDetails];
    
    
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    _NextButton.userInteractionEnabled = true;
    _PreviousButton.userInteractionEnabled = true;
    
    NSLog(@"onPreviousSongButton");
    
    _IsPlayerInitialised = false;
    _CurrentToneIndex--;
    [self viewDidAppear:nil];
    [self PlayTone];
}

-(void)setViewBorder:(UIView *)myView
{
    myView.layer.borderWidth = 0.5f;
    myView.layer.borderColor = [UIColorFromRGB(CLR_BoxBorder) CGColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    [self.navigationItem.leftBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    [self.navigationItem.rightBarButtonItem setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    /*[self.view setUserInteractionEnabled:false];
    self.navigationItem.rightBarButtonItem.enabled = false;
    self.navigationItem.leftBarButtonItem.enabled = false;
    */
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.view setUserInteractionEnabled:true];
    self.navigationItem.rightBarButtonItem.enabled = true;
    self.navigationItem.leftBarButtonItem.enabled = true;
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear >>>");
    _IsPlayerInitialised =false;
    if(_SIXDAVPlayer_Audio.Player.isPlaying)
    {
        [_SIXDAVPlayer_Audio stopPlay];
        _isPlayPaused = true;
        
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        
    }
    
    _SIXDAVPlayer_Audio = nil;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear>>");

    UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_PlayPauseButton setImage:image forState:UIControlStateNormal];
    _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
    
    if(_PlayList.count > 0)
    {
        _ToneDetails=[_PlayList objectAtIndex:_CurrentToneIndex];
    }
    
    //***set like count *********
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([_ToneDetails objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[_ToneDetails objectForKey:@"toneId"] :[[_ToneDetails objectForKey:@"likeCount"]intValue] :_LikeTextLabel];
    }
    else
        _LikeTextLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count enda *********
    
    
    [SIXDStylist loadLikeUnlikeButtonSetings:_LikeButton :[_ToneDetails objectForKey:@"toneId"]];
    
    if(_CurrentToneIndex == 0)
    {
        _PreviousButton.userInteractionEnabled = false;
    }
    if(_CurrentToneIndex == _PlayList.count-1)
    {
        _NextButton.userInteractionEnabled = false;
    }
    
    [self setBackgroundImage];
    
}

-(void)setBackgroundImage
{
    NSString *urlStr = [[_ToneDetails objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [_ToneImage sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              UIImage *image1;
                                              if(error)
                                              {
                                                    image1 = [UIImage imageNamed:@"Default.png"];
                                              }
                                             else
                                             {
                                              if(data != NULL)
                                              {
                                                  image1 =[UIImage imageWithData: data];
                                              }  UIGraphicsBeginImageContext(_UpperView.frame.size);
                                                  [image1 drawInRect:_UpperView.bounds];
                                                  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                                                  UIGraphicsEndImageContext();
                                                  [_UpperView setBackgroundColor:[UIColor colorWithPatternImage:image]];
                                                  
                                              
                                             }
                                              
                                          }];
    [postDataTask resume];
    
    if(_BlurEffect)
    {
        [_BlurEffect removeFromSuperview];
    }
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 2;
    blurEffectView.frame =  _UpperView.bounds;
    blurEffectView.alpha = 0.85;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _BlurEffect = blurEffectView;
    [_UpperView insertSubview:blurEffectView atIndex:0];
    
    //end
    
    UIImage *image;
    if(_PreviousButton.isUserInteractionEnabled==TRUE)
    {
        image = [[UIImage imageNamed:@"previous"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PreviousButton setImage:image forState:UIControlStateNormal];
        _PreviousButton.tintColor=UIColorFromRGB(CLR_BUY);
    }
    else
    {
        image = [[UIImage imageNamed:@"previous"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PreviousButton setImage:image forState:UIControlStateNormal];
        _PreviousButton.tintColor=UIColorFromRGB(CLR_DIM_GRAY);
        
    }
    if(_NextButton.isUserInteractionEnabled==TRUE)
    {
        image = [[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_NextButton setImage:image forState:UIControlStateNormal];
        _NextButton.tintColor=UIColorFromRGB(CLR_BUY);
    }
    else
    {
        image = [[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_NextButton setImage:image forState:UIControlStateNormal];
        _NextButton.tintColor=UIColorFromRGB(CLR_DIM_GRAY);
    }
    
    
    NSString *String1 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"artistName"]];
    _TopLabel.text = [String1 capitalizedString];
    NSString *String2 = [SIXDGlobalViewController HTMLConversion:[_ToneDetails objectForKey:@"toneName"]];
    _BottomLabel.text = [String2 capitalizedString];
    
    if(_TopLabel.text.length <= 0)
    {
        _TopLabel.text = @"";
    }
    if(_BottomLabel.text.length <= 0)
    {
        _BottomLabel.text = @"";
    }
    
}


-(void)PlayTone
{
    if(!_IsPlayerInitialised)
    {
        UIImage *image = [[UIImage imageNamed:@"pause"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        [self.tabBarController.view addSubview:self.overlayview];
        [_ActivityIndicator startAnimating];
        [self.view setUserInteractionEnabled:false];
        _SIXDAVPlayer_Audio = [[SIXDAVPlayer alloc]init];
        _SIXDAVPlayer_Audio.SIXVCCallBack = self;
        _IsPlayerInitialised = true;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [_SIXDAVPlayer_Audio initialisePlayer :[_ToneDetails objectForKey:@"toneUrl"]];

        });
    }
        

    /*
    if(!_IsPlayerInitialised)
    {
        NSString *urlStr = [[_ToneDetails objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
        
        
        NSString* resourcePath = [_ToneDetails objectForKey:@"toneUrl"];
        
        
        NSString *songurl = [resourcePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        NSLog(@"song URL = %@",songurl);
        //for testing
        //songurl = @"http://10.84.0.151:9099/CRBT_TELENOR_UP/servlet/MobileStreamMediaServlet?fileId=BkJnfdO1ZMvzMWWsBmcnQcUvUjzmYMgyPHphaOha6GzRoB0whFdUvhvCdTVY2pTeZm1/JOztdnCPFRNB4MBGoUTYNoxdLg9TusWi/fUGD7wmdoUg9PUZ6Gdb8+TQpsK5";
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        
        NSData *_objectData = [NSData dataWithContentsOfURL:[NSURL URLWithString:songurl]];
        NSError *error;
        _audioPlayer = [[AVAudioPlayer alloc] initWithData:_objectData error:&error];
        _audioPlayer.numberOfLoops = 0;
        _audioPlayer.delegate = self;
        
        if (_audioPlayer == nil)
        {
            NSLog(@"_audioPlayer is NULL");
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [_ActivityIndicator stopAnimating];
                           [self.overlayview removeFromSuperview];
                           //for testing - REMOVE
                           [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"Song Not available",nil,appDelegate.LocalBundel,nil) :self:GLOBAL_ALERT_ACTION_SELF_DISMISS];
                           [self.view setUserInteractionEnabled:true];
                           self.navigationItem.rightBarButtonItem.enabled = true;
                           self.navigationItem.leftBarButtonItem.enabled = true;
                       });
        }
        else
        {
            
            NSLog(@"Playing Song");
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               //added here
                               
                                [_ToneImage sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"Default.png"]];
                               
                               if(_BlurEffect)
                               {
                                   [_BlurEffect removeFromSuperview];
                               }
                               
                               UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
                               UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                               blurEffectView.tag = 2;
                               blurEffectView.frame =  _UpperView.bounds;
                               blurEffectView.alpha = 0.85;
                               blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                               _BlurEffect = blurEffectView;
                               [_UpperView insertSubview:blurEffectView atIndex:0];
                               
                               UIImage *image1;
                               
                                   if(!data)
                                   {
                                       image1 = [UIImage imageNamed:@"Default.png"];
                                   }
                                   
                                   if(data != NULL)
                                   {
                                       NSLog(@"Setting Image");
                                       image1 =[UIImage imageWithData: data];
                                  UIGraphicsBeginImageContext(_UpperView.frame.size);
                                       [image1 drawInRect:_UpperView.bounds];
                                       UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                                       UIGraphicsEndImageContext();
                                       [_UpperView setBackgroundColor:[UIColor colorWithPatternImage:image]];
                                       
                                   }
                               //end
                               
                               UIImage *image;
                               if(_PreviousButton.isUserInteractionEnabled==TRUE)
                               {
                                   image = [[UIImage imageNamed:@"previous"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                   [_PreviousButton setImage:image forState:UIControlStateNormal];
                                   _PreviousButton.tintColor=UIColorFromRGB(CLR_BUY);
                               }
                               else
                               {
                                   image = [[UIImage imageNamed:@"previous"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                   [_PreviousButton setImage:image forState:UIControlStateNormal];
                                   _PreviousButton.tintColor=UIColorFromRGB(CLR_DIM_GRAY);
                                   
                               }
                               if(_NextButton.isUserInteractionEnabled==TRUE)
                               {
                                   image = [[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                   [_NextButton setImage:image forState:UIControlStateNormal];
                                   _NextButton.tintColor=UIColorFromRGB(CLR_BUY);
                               }
                               else
                               {
                                   image = [[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                   [_NextButton setImage:image forState:UIControlStateNormal];
                                   _NextButton.tintColor=UIColorFromRGB(CLR_DIM_GRAY);
                               }
                               
                               _TopLabel.text = [SIXDGlobalViewController CapitalisationConversion:[_ToneDetails objectForKey:@"artistName"]] ;
                               _BottomLabel.text = [SIXDGlobalViewController CapitalisationConversion:[_ToneDetails objectForKey:@"toneName"]];
                               
                               
                               
                               if(_TopLabel.text.length <= 0)
                               {
                                   _TopLabel.text = @"";
                               }
                               if(_BottomLabel.text.length <= 0)
                               {
                                   _BottomLabel.text = @"";
                               }
                  
                               //end
                              
                               _IsPlayerInitialised = true;
                               [_ActivityIndicator stopAnimating];
                               [self.overlayview removeFromSuperview];
                               
                               _audioPlayer.rate = 1;
                               _audioPlayer.enableRate = YES;
                               [_audioPlayer prepareToPlay];
                               
                               [_audioPlayer play];
                        
                               [self.view setUserInteractionEnabled:true];
                            self.navigationItem.rightBarButtonItem.enabled = true;
                            self.navigationItem.leftBarButtonItem.enabled = true;
                               
                           });
        }
        
        
    }
    */
}

-(void)API_DeleteTone
{
        [self.tabBarController.view addSubview:self.overlayview];
        [_ActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_ActivityIndicator];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.APIFlag=DELETE_TONE;
        SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];

        NSString *PackName =  [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
        ReqHandler.HttpsReqType = POST;
        ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&toneId=%@&language=%@&packName=%@",appDelegate.MobileNumber,[_ToneDetails objectForKey:@"toneId"],appDelegate.Language,PackName];
        
        ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/delete-tone",appDelegate.BaseMiddlewareURL];
        [ReqHandler sendHttpPostRequest:self];
    
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onSuccessResponseRecived Response = %@",Response);
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.IsAppOpenedFromURL)
    {
        appDelegate.IsAppOpenedFromURL = false;
        Response = [Response objectForKey:@"responseMap"];
        NSMutableArray *TempPlaylist = [[Response objectForKey:@"searchList"] mutableCopy];
        if(TempPlaylist.count <= 0)
        {
            [self showDefaultErrorMessage];
        }
        else
        {
            _ToneDetails = [TempPlaylist objectAtIndex:0];
            [self viewDidAppear:false];
        }
    }
    else
    {
        _MYTunesLibraryVC.IsRefreshRequired = true;
        [self.navigationController popViewControllerAnimated:false];
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"onFailureResponseRecived Response = %@",Response);
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.IsAppOpenedFromURL)
    {
        [self showDefaultErrorMessage];
    }
    else
    {
        NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
        [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    
    }
    
}

-(void)showDefaultErrorMessage
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.IsAppOpenedFromURL = false;
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.ParentView = self;
    ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Song Not Found",nil,appDelegate.LocalBundel,nil);
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    [self presentViewController:ViewController animated:YES completion:nil];
}

-(void)onFailedToConnectToServer
{
    NSLog(@"onFailedToConnectToServer");
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.IsAppOpenedFromURL)
    {
        [self showDefaultErrorMessage];
    }
    else
    {
        [SIXDGlobalViewController showCommonFailureResponse:self :nil];
    }
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)onSelectedPopUpConfirmButton
{
    if(_IsDeletePage)
    {
        NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",_BottomLabel.text,_TopLabel.text,[_ToneDetails objectForKey:@"toneId"]];
        [SIXDStylist setGAEventTracker:@"Settings" :@"Delete tune" :GAEventDetails];
        [self API_DeleteTone];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:false];
    }
}


-(void)onPlayStarted
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [_ActivityIndicator stopAnimating];
        [_overlayview removeFromSuperview];
        [self.view setUserInteractionEnabled:true];
        self.navigationItem.rightBarButtonItem.enabled = true;
        self.navigationItem.leftBarButtonItem.enabled = true;
         _isPlayPaused = false;
    });
    
    NSString *toneId = [_ToneDetails objectForKey:@"toneId"];
    if (toneId.length <= 0) {
        toneId = [_ToneDetails objectForKey:@"toneCode"];
    }
    
    NSString *contentId = [_ToneDetails objectForKey:@"contentId"];
    if(!contentId)
    {
        contentId = @"0";
    }
    if(!toneId)
    {
        toneId = @"0";
    }
    
    [FIRAnalytics logEventWithName:@"Play"
                        parameters:@{
                                     @"toneId": toneId,
                                     @"contentId":contentId
                                     }];
    
}


- (void)onPlayStopped
{
    UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_PlayPauseButton setImage:image forState:UIControlStateNormal];
    _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
    _isPlayPaused = true;
}

-(void)onPlayFailure
{
    
    NSLog(@"onPlayFailure");
    dispatch_async(dispatch_get_main_queue(), ^
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _isPlayPaused = true;
        [_ActivityIndicator stopAnimating];
        [_overlayview removeFromSuperview];
        self.navigationItem.rightBarButtonItem.enabled = true;
        self.navigationItem.leftBarButtonItem.enabled = true;
        [self.view setUserInteractionEnabled:true];
        UIImage *image = [[UIImage imageNamed:@"play"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_PlayPauseButton setImage:image forState:UIControlStateNormal];
        _PlayPauseButton.tintColor=UIColorFromRGB(CLR_BUY);
        [SIXDGlobalViewController setAlert:NSLocalizedStringFromTableInBundle(@"Song Not available",nil,appDelegate.LocalBundel,nil) :self:GLOBAL_ALERT_ACTION_SELF_DISMISS];
        
    });
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}


-(void)API_CDPSpecificLanguageSearch
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/search-tone",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&toneId=%@&pageNo=1&perPageCount=1&sortBy=Order_By",URL,appDelegate.Language,appDelegate.ToneID];
    //appDelegate.APIFlag = CDP_SEARCH_NORMAL;
    //[ReqHandler sendHttpGETRequest:self :[NSString stringWithFormat:@"%d", appDelegate.CategoryId]];
    [ReqHandler sendHttpGETRequest:self :nil];
}


@end
