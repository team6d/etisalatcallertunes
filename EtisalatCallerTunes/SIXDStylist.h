//
//  SIXDStylist.h
//  MagnaRoyalty
//
//  Created by Shahnawaz Nazir on 17/02/17.
//  Copyright © 2017 Shahnawaz Nazir. All rights reserved.
//

#ifndef SIXDStylist_h
#define SIXDStylist_h


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SIXDCustomTVCell.h"
#import "ViewController.h"

@interface SIXDStylist : NSObject

+(void)setBackgroundGradient:(UIView *)mainView;

+(void)setBackGroundImage:(UIView *)mainView;

+(void)setTextViewAlert:(UITextField *)mainTextField :(UIView *)mainView :(UIButton *)mainButton;

+(void)setTextViewNormal:(UITextField *)mainTextField :(UIView *)mainView :(UIButton *)mainButton;

+(void)loadCustomFonts;

+(bool)isEtisalatValidNumber : (NSString *)number;

+ (void)loadLikeUnlikeSetings : (SIXDCustomTVCell *)cell : (NSString*)toneId;

+(void)addKeyBoardDoneButton : (UITextField*)TextField : (ViewController*) ParentView;

+ (void)loadLikeUnlikeButtonSetings  : (UIButton*)LikeButton : (NSString*)toneId;

+(void)setGAScreenTracker:(NSString *)screenName;

+(void)setGAEventTracker:(NSString *)Category :(NSString *)Action :(NSString *)Label;
@end

#endif /* SIXDStylist_h */
