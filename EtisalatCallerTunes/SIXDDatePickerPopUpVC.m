//
//  SIXDDatePickerPopUpVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 27/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDDatePickerPopUpVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"

#import "FSCalendar.h"
#import "SIXDStylist.h"


#define START_DATE_SEGMENT  0
#define END_DATE_SEGMENT    1


@interface SIXDDatePickerPopUpVC ()

@end


@implementation SIXDDatePickerPopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"Tune_Settings_SetSpecificDate"];
    
    //NSDate *selectedDate = _fsCalendar.selectedDate;
    
   /*
    [_DatePicker setSemanticContentAttribute:UISemanticContentAttributePlayback];
    [_EndDatePicker setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
    
    [_fsCalendar.calendarHeaderView setSemanticContentAttribute:UISemanticContentAttributePlayback];
    [_fsCalendar.calendarHeaderView.calendar setSemanticContentAttribute:UISemanticContentAttributePlayback];
     [_fsCalendarEnd.calendarHeaderView setSemanticContentAttribute:UISemanticContentAttributePlayback];
     [_fsCalendarEnd.calendarHeaderView.calendar setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
   
     [_fsCalendarEnd.calendarHeaderView.calendar setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    [_fsCalendarEnd.calendarHeaderView setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    */
    
    //_fsCalendarEnd.appearance.headerMinimumDissolvedAlpha = 0;
   // if ([self respondsToSelector:@selector(setSemanticContentAttribute:)]) {
     //   self.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
    //}
    
    
    [self.view setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7]];
    
    _AlertLabel.hidden = YES;
    //_typeFlag = START_DATE_SEGMENT;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _TimePickerHeight.constant = 110;
        _BottomSpaceToSaveButton.constant = 35;
    }
    
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
     _fsCalendar.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"ar_AE"]];
        
       //  [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        
    // _fsCalendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en"];
     //   _fsCalendarEnd.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar_AE"];
   // _fsCalendar.calendarHeaderView.calendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar_AE"];
     /*
       // _fsCalendar.selectedDate
        //_fsCalendar.identifier = NSCalendar.Identifier.persian.rawValue
        //[_fsCalendar setIdentifier:<#(NSString * _Nonnull)#>
    */
    }
    
    [_SaveButton setTitle:NSLocalizedStringFromTableInBundle(@"SAVE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    _DatePicker.datePickerMode = UIDatePickerModeTime;
    _EndDatePicker.datePickerMode = UIDatePickerModeTime;
    
    [self setSegmentTabColor];
    
    if(_typeFlag==START_DATE_SEGMENT)
    {
        NSLog(@"TypeFlag = 0");
        _fsCalendar.hidden=NO;
        _fsCalendarEnd.hidden=YES;
        
       // [_TextField setInputView:_fsCalendar];
        _fsCalendar.delegate = self;
        _fsCalendar.dataSource = self;
       // _fsCalendarEnd .delegate = nil;
        //_fsCalendarEnd.dataSource = nil;
        [self.view bringSubviewToFront:_fsCalendar];
        //Date picker
        //datePicker.datePickerMode = UIDatePickerModeTime;
        
        _DatePicker.hidden = NO;
        _EndDatePicker.hidden =YES;
        [self.view bringSubviewToFront:_DatePicker];
        //[_TextField setInputView:_DatePicker];
        
    }
    else
    {
        NSLog(@"TypeFlag = 1");
        _fsCalendar.hidden=YES;
        _fsCalendarEnd.hidden=NO;
        
       // [_EndTextField setInputView:_fsCalendarEnd];
        
        _fsCalendarEnd.delegate = self;
        _fsCalendarEnd.dataSource = self;
       // _fsCalendar .delegate = nil;
        //_fsCalendar.dataSource = nil;
        [self.view bringSubviewToFront:_fsCalendarEnd];
        _DatePicker.hidden = YES;
        _EndDatePicker.hidden =NO;
        [self.view bringSubviewToFront:_EndDatePicker];
        [_TextField setInputView:_EndDatePicker];
    }
    
    //DatePicker-START
   // [_DatePicker setDate:[NSDate date]];
    _DatePicker.datePickerMode = UIDatePickerModeTime;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    [_DatePicker setLocale:locale];
    [_DatePicker setValue:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:@"textColor"];
    
    [_DatePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [_DatePicker reloadInputViews];
    
    
    //DatePicker-END
   // [_EndDatePicker setDate:[NSDate date]];
    _EndDatePicker.datePickerMode = UIDatePickerModeTime;
    [_EndDatePicker setLocale:locale];
    [_EndDatePicker setValue:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:@"textColor"];
    
    [_EndDatePicker reloadInputViews];
    
    
    //setting locale for fscalender
    
    _fsCalendar.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
    
     _fsCalendarEnd.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
    
    //[_EndDatePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    
    
    
    //Calendar
    
    //FSCalendar *fsCalendar = [FSCalendar alloc];
    //NSCalendar *nsCal = [NSCalendar currentCalendar];
    //fsCalendar.dataSource = self;
    //fsCalendar.delegate = self;
    
    //   _fsCalendar.scrollDirection = FSCalendarScrollDirectionVertical;
    // _fsCalendar.appearance.toda
    
    
    NSDateComponents *DateC = nil;
    
    DateC.month = 1;
    DateC.month = -1;
    
    //NSDate *Today = [NSDate date];
    
  //  _fsCalendar.currentPage = [nsCal dateFromComponents:DateC];
   // _fsCalendar.currentPage = [nsCal dateByAddingComponents:DateC toDate:Today options:NULL];
    //[_fsCalendar setCurrentPage:_fsCalendar.currentPage animated:YES];
    
    
    
    /*
     UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
     toolBar.barStyle = UIBarButtonItemStyleDone;
     toolBar.tintColor = [UIColor whiteColor];
     _ToolBar=toolBar;
     
     UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Done",nil,appDelegate.LocalBundel,nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneClicked:)];
     
     //to be corrected >>placeholder font<<
     [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont fontWithName:appDelegate.PlaceHolderFont size:13], NSFontAttributeName,
     [UIColor whiteColor], NSForegroundColorAttributeName,
     nil]
     forState:UIControlStateNormal];
     
     
     [toolBar setItems:[NSArray arrayWithObjects: doneButton, nil] animated:NO];
     
     // [datePicker setMaximumDate:maxDate];
     // [datePicker setMinimumDate:minDate];
     
     [_TextField setInputAccessoryView:toolBar];
     */
    //DatePicker-END
   
    /*
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(dismissViewControllerMain)];
    
    [self.view addGestureRecognizer:tap1];
  */
    //OVERLAY
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
}


-(void)setSegmentTabColor
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                nil];
    [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"ArialMT" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
    }
    else
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"GESSTextLight-Light" size:14], NSFontAttributeName,UIColorFromRGB(CLR_TEXTFIELDBORDER), NSForegroundColorAttributeName,
                                    nil];
        [_SegmentTab setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
   */
    
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:UIColorFromRGB(CLR_ETISALAT_GREEN) forKey:NSForegroundColorAttributeName];
    [_SegmentTab setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"START",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:0];
    [_SegmentTab setTitle:NSLocalizedStringFromTableInBundle(@"END",nil,appDelegate.LocalBundel,nil) forSegmentAtIndex:1];
    
    
    [_SegmentTab setNeedsUpdateConstraints];
    [_SegmentTab layoutIfNeeded];
    
    NSArray *array = [_SegmentTab subviews];
    UIView *Temp;
    
    Temp = [array objectAtIndex:0];
    UILabel *Line = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 4)];

    Temp = [array objectAtIndex:1];
    UILabel *Line1 = [[UILabel alloc] initWithFrame:CGRectMake(Temp.frame.origin.x, Temp.frame.size.height-2, Temp.frame.size.width, 4)];
    
    
    _SecondLabel = Line;
    [_SegmentTab insertSubview:Line aboveSubview:Temp];
        
    _FirstLabel = Line1;
    [_SegmentTab insertSubview:Line1 aboveSubview:Temp];
   

    _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
    _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
    
    
}



-(void)dismissViewControllerMain
{
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}
/*
 -(void)dismissViewControllerSub
 {
 [_TextField resignFirstResponder];
 NSLog(@"Tap in SubView");
 }
 */

-(void) dateTextField:(id)sender
{
    NSLog(@"dateTextField >>>");
    // _DatePicker = (UIDatePicker*)_TextField.inputView;
    // _DatePicker = (UIDatePicker*)_TextField.inputView;
    // [_DatePicker setMaximumDate:[NSDate date]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"hh:mm"];
    NSString *dateString = [dateFormat stringFromDate:_DatePicker.date];
    
    _TextField.text = [NSString stringWithFormat:@"%@",dateString];
    
    NSLog(@"TextField1 = %@", dateString);
}

/*
 - (void)doneClicked :(id)sender
 {
 // Write out the date...
 [_TextField resignFirstResponder];
 }
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSegmentValueChanged:(UISegmentedControl *)sender {
    switch ([sender selectedSegmentIndex])
    {
        case 0:
            
            NSLog(@"switch0");
            _typeFlag =  START_DATE_SEGMENT;
            // _DatePicker.hidden=NO;
            //_EndDatePicker.hidden=YES;
            [self.view bringSubviewToFront:_DatePicker];
            [_TextField setInputView:_DatePicker];
            
            _fsCalendar.hidden=NO;
            _fsCalendarEnd.hidden=YES;
            
            _fsCalendar.delegate = self;
            _fsCalendar.dataSource = self;
            _fsCalendarEnd .delegate = nil;
            _fsCalendarEnd.dataSource = nil;
            
            _DatePicker.hidden = NO;
            _EndDatePicker.hidden =YES;
            [self.view bringSubviewToFront:_DatePicker];
            [_TextField setInputView:_DatePicker];
            [self.view bringSubviewToFront:_fsCalendar];
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            [_fsCalendar reloadData];
    
            break;
            
        case 1:
            NSLog(@"switch1");
            _typeFlag = END_DATE_SEGMENT;
            //  _DatePicker.hidden=YES;
            // _EndDatePicker.hidden=NO;
            //[self.view bringSubviewToFront:_EndDatePicker];
            //[_TextField setInputView:_EndDatePicker];
            
            _fsCalendarEnd.hidden=NO;
            _fsCalendar.hidden=YES;
            
            _fsCalendarEnd.delegate = self;
            _fsCalendarEnd.dataSource = self;
            _fsCalendar.delegate = nil;
            _fsCalendar.dataSource = nil;
            
            _DatePicker.hidden = YES;
            _EndDatePicker.hidden =NO;
            [self.view bringSubviewToFront:_EndDatePicker];
            [_TextField setInputView:_EndDatePicker];
            
            _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
            _SecondLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
            
            [self.view bringSubviewToFront:_fsCalendarEnd];
            //  [_EndTextField setInputView:_fsCalendarEnd];
            [_fsCalendarEnd reloadData];
            break;
            
        default:
            NSLog(@"switch default statement for segment control");
            
    }
}

- (IBAction)onClickedCloseButton:(id)sender
{
    NSLog(@"Dismissed modal view");
    _AlertLabel.hidden = YES;
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onClickedSaveButton:(id)sender
{
    _TimeVC.RepeatButtonProperty = -1;
    
    int timeDiff = [[self remaningTime : _fromDate : _toDate] intValue];
    //BOOL  validateStatus = [self validateTime : _fromDate : _toDate] ;
    
    NSLog(@"FromeDate = %@ : toDate =%@ : fromTime =%@ endTime = %@ timeDiff = %d",_fromDate,_toDate,_DatePicker.date,_EndDatePicker.date, timeDiff);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    NSString *NewFromDateString = [dateFormat stringFromDate:_fromDate];
    NSLog(@"NewDateString = %@", NewFromDateString);
    
    NSString *NewToDateString = [dateFormat stringFromDate:_toDate];
    NSLog(@"NewDateString = %@", NewToDateString);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if((_fromDate  == NULL) || (_toDate == NULL) || (_DatePicker.date == NULL) || (_EndDatePicker.date == NULL))
    {
        NSString *err;
        if(_fromDate == NULL)
            err = @"*please select a valid start date";
        
        else if(_toDate == NULL)
            err = @"*please select a valid end date";
        
        else if(_DatePicker.date == NULL)
            err = @"*please select a valid start time";
        
        else if(_EndDatePicker.date == NULL)
            err = @"*please select a valid end time";
            
        NSLog(@"NULL TIME");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(err,nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        return;
    }
    
    if(timeDiff < 0)
    {
        NSLog(@"INVALID TIME : Start Date and End Date should be atleast a day apart");
        _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"end date should be greater than start date",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        return;
    }
    
    if(timeDiff > 30 && timeDiff < 365)
   {
       _TimeVC.RepeatButtonProperty = TIME_REPEAT_DISABLE_MONTHLY;
   }
   else if (timeDiff >= 365)
   {
        _TimeVC.RepeatButtonProperty = TIME_REPEAT_DISABLE_ALL;
   }
    
    BOOL timeDiffStatus = [self validateTime  : _DatePicker.date : _EndDatePicker.date];
    
     BOOL dayDiffStatus = [self validateTime  : _fromDate : _toDate];
    if (!dayDiffStatus) {
        NSLog(@"dayDiffStatus === %id",dayDiffStatus);
        /*NSString *endBigDateString1 = [dateFormat stringFromDate:_EndDatePicker.date];
         
         if([endBigDateString1 isEqualToString:@"00:00"])
         {
         timeDiffStatus = TRUE;
         }
         */
        if(!timeDiffStatus)
        {
            NSLog(@"INVALID TIME");
            _TimeVC.RepeatButtonProperty = TIME_REPEAT_DISABLE_ALL;
            //enabled
            _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"*please select a valid start time",nil,appDelegate.LocalBundel,nil);
            _AlertLabel.hidden=NO;
            return;
        }
    }
    
    
    
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *fromBigDateString = [dateFormat stringFromDate:_DatePicker.date];
    fromBigDateString = [NSString stringWithFormat:@"%@ %@",NewFromDateString,fromBigDateString];
    
    
    NSString *endBigDateString = [dateFormat stringFromDate:_EndDatePicker.date];
    if([endBigDateString isEqualToString:@"00:00"])
        endBigDateString = @"23:59";
        
    endBigDateString = [NSString stringWithFormat:@"%@ %@",NewToDateString,endBigDateString];
    
    
    _TimeVC.EndBottomLabel.text = endBigDateString;
    _TimeVC.StartBottomLabel.text = fromBigDateString;
    
    _TimeVC.Source = SRC_DATE_PICKER;
    
    [_TimeVC viewWillAppear:false];
    
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    
}

- (void)viewDidDisappear:(BOOL)animated;
{
    NSLog(@"viewWillDisappear >>>");
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        _fsCalendar.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"ar_AE"]];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear >>>");
    _AlertLabel.hidden=YES;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        _fsCalendar.appearance.headerDateFormat = [NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale localeWithLocaleIdentifier:@"ar_AE"]];
        
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    if(_typeFlag==START_DATE_SEGMENT)
    {
        NSLog(@"TypeFlag = 0");
        _fsCalendar.hidden=NO;
        _fsCalendarEnd.hidden=YES;
        
        // [_TextField setInputView:_fsCalendar];
        _fsCalendar.delegate = self;
        _fsCalendar.dataSource = self;
        // _fsCalendarEnd .delegate = nil;
        //_fsCalendarEnd.dataSource = nil;
        [self.view bringSubviewToFront:_fsCalendar];
        //Date picker
        //datePicker.datePickerMode = UIDatePickerModeTime;
        
        _DatePicker.hidden = NO;
        _EndDatePicker.hidden =YES;
        [self.view bringSubviewToFront:_DatePicker];
        //[_TextField setInputView:_DatePicker];
        
        _FirstLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        _SecondLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
        
    }
    else
    {
        NSLog(@"TypeFlag = 1");
        _fsCalendar.hidden=YES;
        _fsCalendarEnd.hidden=NO;
        
        // [_EndTextField setInputView:_fsCalendarEnd];
        
        _fsCalendarEnd.delegate = self;
        _fsCalendarEnd.dataSource = self;
        // _fsCalendar .delegate = nil;
        //_fsCalendar.dataSource = nil;
        [self.view bringSubviewToFront:_fsCalendarEnd];
        _DatePicker.hidden = YES;
        _EndDatePicker.hidden =NO;
        [self.view bringSubviewToFront:_EndDatePicker];
        [_TextField setInputView:_EndDatePicker];
        
        _FirstLabel.backgroundColor = UIColorFromRGB(CLR_WHITE);
        _SecondLabel.backgroundColor = UIColorFromRGB(CLR_ETISALAT_GREEN);
        
    }
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    // Do other updates here
    [self.view layoutIfNeeded];
}


- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(nonnull NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSLog(@"shouldSelectDate >>");
    NSDate *today = [NSDate date];
    BOOL status = YES;
    
     int  timeDiff = [[self remaningTime : today : date] intValue];
      NSLog(@"Today = %@ SelectedDate = %@ timeDiff = %d toDate = %@", today,date, timeDiff, _toDate);
    if (timeDiff < 0)
    {
        NSLog(@"return NO");
        status =  NO;
    }
    timeDiff = 1000;
    if(_typeFlag == END_DATE_SEGMENT)
    {
        if(_fromDate)
        {
            timeDiff = [[self remaningTime : _fromDate : date] intValue];
            NSLog(@"fromTimeDiff = %d", timeDiff);
            if(timeDiff <0)
            {
                status =  NO;
            }
            
        }
    }
    /*
    timeDiff = 1000;
    if(_typeFlag == START_DATE_SEGMENT)
    {
        if(_toDate)
        {
            timeDiff = [[self remaningTime : date : _toDate] intValue];
            NSLog(@"toTimeDiff = %d", timeDiff);
            if(timeDiff <=0)
            {
                status =  NO;
            }
            
        }
    }
     */
    return status;
}

- (UIColor *) calendar:(FSCalendar *)calendar appearance:(nonnull FSCalendarAppearance *)appearance titleDefaultColorForDate:(nonnull NSDate *)date

//- (UIColor *) calendar : (FSCalendar *)calendar appearance:(nonnull FSCalendarAppearance *)appearance titleSelectionColorForDate:(nonnull NSDate *)date
{
    NSLog(@"appearance >>>");
    
   // [calendar.calendarHeaderView setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
     //[calendar.calendarHeaderView.calendar setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
     //[calendar.calendarHeaderView reloadData];
    
    NSDate *today = [NSDate date];
    UIColor *returnColor = UIColorFromRGB(CLR_LIGHT_GRAY);
    int  timeDiff = [[self remaningTime : today : date] intValue];
    NSLog(@"Today = %@ SelectedDate = %@ remaining = %d", today,date,timeDiff);

    //calendar.calendarHeaderView.
    if (timeDiff < 0)
    {
        NSLog(@"return lightGrayColor");
        //return ([UIColor lightGrayColor]);
        returnColor = UIColorFromRGB(CLR_DISABLE_GRAY);
       
    }
    
    if(_typeFlag == END_DATE_SEGMENT)
    {
        if(_fromDate)
        {
            timeDiff = [[self remaningTime : _fromDate : date] intValue];
            NSLog(@"fromTimeDiff = %d", timeDiff);
            if(timeDiff <=0)
            {
                returnColor = UIColorFromRGB(CLR_DISABLE_GRAY);
                //[_fsCalendarEnd selectedDate col ]
            }
            
        }
    }
     return returnColor;
    /*
    timeDiff = 1000;
    if(_typeFlag == START_DATE_SEGMENT)
    {
        if(_toDate)
        {
            timeDiff = [[self remaningTime : date : _toDate] intValue];
            NSLog(@"toTimeDiff = %d", timeDiff);
            if(timeDiff <=0)
            {
                //status =  NO;
            }
            
        }
    }
     */
    
    //return UIColorFromRGB(CLR_ATHENS_GRAY);
    
   
}

/*
- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
    return [NSDate date];
}
*/
 
/*
- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
{
    return [NSDate date];
}
 */

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    //NSString *dateString = [NSDateFormatter localizedStringFromDate:date
                                                         // dateStyle:NSDateFormatterShortStyle
                                                          //timeStyle:nil];
    NSLog(@"didSelectDate::date  %@",date);
    
    switch (_typeFlag) {
        case START_DATE_SEGMENT:
            _fromDate = date;
            break;
            
        case END_DATE_SEGMENT:
            _toDate = date;
            break;
            
        default:
            NSLog(@"Error :");
            break;
    }
    
    NSLog(@"didSelectDate   Date = %@",date);
    
}

- (BOOL)validateTime:(NSDate*)startDate :(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    // NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    NSLog(@"days = %ld",days);
    NSLog(@"hour = %ld",hour);
    NSLog(@"minutes = %ld",minutes);
    // durationString = [NSString stringWithFormat:@"%ld",(long)hour];
    //return durationString;
    
    //if(days < 0)
    //  return NO;
    if (days>0) {
        return YES;
    }
    
    if (hour<= 0 && minutes<=0) {
        return NO;
    }
    if (hour < -23 && minutes > 58) {
        return NO;
    } else {
        return YES;
    }
//    if(hour < 0)
//        return NO;
//    if(hour >0)
//        return YES;
//    if(minutes <= 0)
//        return NO;
//
    return YES;
    
    /*
     
     if(days>0)
     {
     
     durationString=[NSString stringWithFormat:@"%ld",(long)days];
     return durationString;
     }
     
     if(hour>0)
     {
     
     durationString=[NSString stringWithFormat:@"%ld hour",(long)hour];
     return durationString;
     }
     if(minutes>0)
     {
     
     durationString = [NSString stringWithFormat:@"%ld minute",(long)minutes];
     return durationString;
     }
     
     return @"";
     */
}


- (NSString*)remaningTime:(NSDate*)startDate :(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    NSLog(@"Daysss = %ld",days);
    
    durationString = [NSString stringWithFormat:@"%ld",(long)days];
    return durationString;
   
    /*
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld",(long)days];
        return durationString;
    }
   
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hours",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hour",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld minutes",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld minute",(long)minutes];
        
        return durationString;
    }
    */
    return @"";
}




@end

