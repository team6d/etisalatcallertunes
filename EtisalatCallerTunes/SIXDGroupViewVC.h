//
//  SIXDGroupViewVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 22/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDGroupViewVC : ViewController

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *InfoView;
@property (weak, nonatomic) IBOutlet UILabel *GroupNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *DeleteButton;
@property (weak, nonatomic) IBOutlet UIButton *EditButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property(strong,nonatomic) NSMutableDictionary *GroupsList;
@property(strong,nonatomic) NSArray *CountDict;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

- (IBAction)onClickedEditGroupButton:(UIButton *)sender;
- (IBAction)onClickedDeletGroupButton:(UIButton *)sender;

@end
