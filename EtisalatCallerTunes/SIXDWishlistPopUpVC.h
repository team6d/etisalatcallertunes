//
//  SIXDWishlistPopUpVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDWishlistPopUpVC : ViewController

- (IBAction)onClickedCloseButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *SubView;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property(nonatomic) int WishListType;
@property(nonatomic) BOOL IsSourceSalati;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PopUpViewHeight;



@end
