//
//  SIXDCommonPopUp_View.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 2/15/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SIXDCommonPopUp_View : UIViewController
@property (weak, nonatomic) IBOutlet UIView *AlertView;
@property (weak, nonatomic) IBOutlet UIButton *AlertButton;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;
@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)onSelectedConfirmButton:(UIButton *)sender;

@property(weak,nonatomic) NSString* AlertMessage;
@property(weak,nonatomic) NSString* AlertImageName;
@property(weak,nonatomic) NSString* ConfirmButtonText;
@property(strong,nonatomic) ViewController *ParentView;

@property(nonatomic)BOOL IsErrorFlag;

@property (weak, nonatomic) IBOutlet  UILabel *AlertTitle;
- (IBAction)onSelectedCloseButton:(UIButton *)sender;

@end
