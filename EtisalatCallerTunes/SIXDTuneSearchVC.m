//
//  SIXDTuneSearchVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 11/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDTuneSearchVC.h"
#import "SIXDCustomTVCell.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDBuyToneVC.h"
#import "SIXDPlayerVC.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDGlobalViewController.h"
#import "UIImageView+WebCache.h"

@interface SIXDTuneSearchVC ()

@end

@implementation SIXDTuneSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     _PerPageCount = [[appDelegate.AppSettings objectForKey:@"FETCH_LIMIT"]intValue];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_TempResponseCount >= _PerPageCount)
        return _toneList.count+1;
    else
        return _toneList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(indexPath.row < _toneList.count)
    {
        NSMutableDictionary *Dict = [_toneList objectAtIndex:indexPath.row];
        
        //***set like count *********
        SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
        if([Dict objectForKey:@"likeCount"])
        {
            [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
        }
        else
            cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
        //***set like count enda *********
        
        
        NSString *imageurl = [[Dict objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *ImageURL = [NSURL URLWithString:imageurl];
        [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        
        // Configure the cell...
        cell.TitleLabel.text = [[Dict objectForKey:@"artistName"] capitalizedString];
        cell.SubTitleLabel.text = [[Dict objectForKey:@"toneName"] capitalizedString];
        
        UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
        [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        
        
        image = [[UIImage imageNamed:@"topchartsbuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.BuyButton setImage:image forState:UIControlStateNormal];
        [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        
        //LIKE SETTINGS
        NSLog(@"CustomTV::Calling likeSettingsSession >>>");
        [SIXDStylist loadLikeUnlikeSetings:cell :[Dict objectForKey:@"toneId"]];
        
        
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        int x_Position;
        if([appDelegate.Language isEqualToString:@"English"])
        {
            x_Position=0;
        }
        else
        {
            x_Position=50-1;
        }
        UIView *leftBorder1 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder1.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
        
        [cell.LikeButton addSubview:leftBorder1];
        
        UIView *leftBorder2 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder2.backgroundColor =UIColorFromRGB(CLR_FAQ_1);
        
        [cell.BuyButton addSubview:leftBorder2];
        
        UIView *leftBorder3 = [[UIView alloc] initWithFrame:CGRectMake(x_Position, 0, 0.5, 52)];
        leftBorder3.backgroundColor = UIColorFromRGB(CLR_FAQ_1);
        [cell.AccessoryButton addSubview:leftBorder3];
        
        cell.BuyButton.tag = indexPath.row;
        cell.LikeButton.tag = indexPath.row;
        cell.AccessoryButton.tag = indexPath.row;
        
        cell.SubView.layer.borderColor =[UIColorFromRGB(CLR_CELL_BORDER) CGColor];
        cell.SubView.layer.borderWidth = 0.5;
        [cell.SubView setHidden:false];
    }
    else
    {
        [cell.SubView setHidden:true];
        [cell.ActivityIndicator startAnimating];
        
    }
    
    return cell;
    
}

- (IBAction)onSelectedBuyButton:(UIButton*)sender
{
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    
    ViewController.ToneDetails= [_toneList objectAtIndex:sender.tag];
    
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (IBAction)onSelectedAccessoryButton:(UIButton*)sender
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_toneList;
    ViewController.CurrentToneIndex = sender.tag;
    [self.navigationController pushViewController:ViewController animated:YES];
}


-(IBAction)onSelectedLikeButton:(UIButton *)sender
{
    NSLog(@"Calling onSelectedLikeButton >>>");
    
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    NSMutableDictionary *TuneDetails = [[_toneList objectAtIndex:sender.tag ]mutableCopy];
    
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
        if(!categoryId)
        {
            categoryId = [TuneDetails objectForKey:@"category"];
        }
            if(!categoryId)
            {
                categoryId = DEFAULT_CATEGORY_ID;
            }
    
    
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    
    
    
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
}


-(void)API_CDPSpecificLanguageSearch
{
    if(_SearchKey.length > 0)
    {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = CDP_SEARCH_NORMAL;
    
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/specific-search-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&categoryId=0&pageNo=%lu&perPageCount=20&toneId=0&sortBy=Order_By",URL,appDelegate.Language,(unsigned long)_PageNumber];
    
    [ReqHandler sendHttpGETRequest:self :_SearchKey];
    }
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    NSLog(@"Response = %@",Response);

    Response = [Response objectForKey:@"responseMap"];
    //commented on 04/06/2018 - we are checking for "songlist" in SearchVC made it same here also
   // NSMutableArray *TempPlaylist = [[Response objectForKey:@"toneList"] mutableCopy];
    NSMutableArray *TempPlaylist = [[Response objectForKey:@"songList"] mutableCopy];
    
    
    _TempResponseCount = TempPlaylist.count;
    _PageNumber += _TempResponseCount;
    
    _IsLoading = false;
    [_TableView setUserInteractionEnabled:true];
    
    [_TableView beginUpdates];
    
    if(TempPlaylist.count < _PerPageCount)
    {
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    }
    
    for(int i=0;i<TempPlaylist.count;i++)
    {
        [_toneList addObject:[TempPlaylist objectAtIndex:i]];
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count-1) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
        [_TableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    }
    [_TableView endUpdates];
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    //TO DO
    if(_IsLoading)
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil] ;
         _TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
    }
    
}

-(void)onFailedToConnectToServer
{
    if(_IsLoading)
    {
        _IsLoading = false;
        NSIndexPath *path1 = [NSIndexPath indexPathForRow:(_toneList.count) inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path1,nil];
         _TempResponseCount = 0;
        [_TableView beginUpdates];
        [_TableView deleteRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        [_TableView endUpdates];
        [_TableView setUserInteractionEnabled:true];
    }
    
}

- (void)onInternetBroken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == _toneList.count)
    {
        if(!_IsLoading)
        {
            if(_TempResponseCount >= _PerPageCount)
            {
                [_TableView setUserInteractionEnabled:false];
                _IsLoading = true;
                [self API_CDPSpecificLanguageSearch];
                
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
    ViewController.PlayList=_toneList;
    ViewController.CurrentToneIndex = indexPath.row;
    [self.navigationController pushViewController:ViewController animated:YES];
}

@end
