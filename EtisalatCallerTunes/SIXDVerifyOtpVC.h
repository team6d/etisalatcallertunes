//
//  SIXDVerifyOtpVC.h
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/20/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDVerifyOtpVC : ViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *GetOtpButton;

@property (weak, nonatomic) IBOutlet UIView *NumberView;

@property (weak, nonatomic) IBOutlet UITextField *Number;

@property (weak, nonatomic) IBOutlet UILabel *NumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;

- (IBAction)onClickedGetOtpButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@end
