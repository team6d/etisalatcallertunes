//
//  SIXDGroupConfiguration.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 14/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//


#import "ViewController.h"
#import "SIXDGroupViewVC.h"
@import ContactsUI;

@interface SIXDGroupConfiguration : ViewController<CNContactPickerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *InfoView;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIView *GroupNameView;
@property (weak, nonatomic) IBOutlet UITextField *GroupNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *GroupNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *GroupButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToTextField;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
- (IBAction)onClickedDeleteButton:(UIButton *)sender;
- (IBAction)onClickedAddButton:(UIButton *)sender;
- (IBAction)onClickedContactButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Number;
@property (weak, nonatomic) IBOutlet UITextField *Name;
@property (weak, nonatomic) IBOutlet UIButton *SaveButton;
- (IBAction)onClickedSaveButton:(id)sender;

@property(strong,nonatomic)NSArray *PhoneNumbers;


@property (weak, nonatomic) IBOutlet UIView *NumberView;
@property (weak, nonatomic) IBOutlet UIButton *NumberErrorIcon;
@property (weak, nonatomic) IBOutlet UIView *NameView;
@property (weak, nonatomic) IBOutlet UIButton *NameErrorIcon;

@property(strong,nonatomic) NSMutableDictionary *GroupInfoDictionary;
@property(strong,nonatomic) NSMutableArray *StableArray;
@property(strong,nonatomic) NSMutableArray *GroupMemberList;
@property(nonatomic) int SOURCE;

//need to correct
@property(nonatomic) BOOL IsEDITED;

@property(strong,nonatomic) SIXDGroupViewVC *GroupViewVC;
@property (weak, nonatomic) IBOutlet UIButton *AddButton;


@end
