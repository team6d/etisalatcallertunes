//
//  SIXDHttpRequestHandler.h
//  6dCRBTApp
//
//  Created by Six Dee Technologies on 12/28/16.
//  Copyright © 2016 6d. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "AppDelegate.h"


//Values to be filled for InvokedAPI variable - used to identify which API is invoked within this class

#define RegenerateToken             1
#define LogOutAPI                   2
#define MULTI_PART_EDIT_PROFILE     3
#define MULTI_PART_KARAOKE          4


//TYPE of Request  => used to determine call back method
#define POST            11
#define GET             12
#define REGENERATETOKEN 13
#define MULTIPLE        14   //used when calling from page where multiple API's are called simutaneously
//#define MULTIPLE_GET    5


@interface SIXDHttpRequestHandler : ViewController<NSURLSessionDelegate>

//@property (strong, nonatomic) NSMutableString *httpsResponse;
@property(nonatomic) NSInteger HttpStatusCode;
@property(nonatomic) NSString *Request;
@property(nonatomic) NSString *FinalURL;
@property(nonatomic) NSString *SearchKey;
@property(strong,nonatomic) NSMutableData *RequestData;
@property(strong,nonatomic) NSMutableDictionary *HeaderDictionary;
@property(strong,nonatomic) ViewController *ParentView;
@property(nonatomic) int InvokedAPI;
-(void)sendHttpPostRequest:(ViewController*)ParentView;
-(void)sendHttpGETRequest:(ViewController*)ParentView :(NSString*)searchKey;
-(void)regenerateToken;
@property(nonatomic) BOOL IsMultiPartPost;
@property(nonatomic) int MultiPartReqType;

-(void)LogOut;
-(void)getPreferedCategoryList;



@property(nonatomic) int HttpsReqType;

@property(nonatomic) int IsURLEncodingNotRequired;


@end
