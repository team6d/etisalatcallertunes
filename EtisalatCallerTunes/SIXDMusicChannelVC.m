//
//  SIXDMusicChannelVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 28/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDMusicChannelVC.h"
#import "SIXDCustomTVCell.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDPlaylistBuyVC.h"
#import "AppDelegate.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDStylist.h"
#import "SIXDBuyToneVC.h"
#import "UIImageView+WebCache.h"
#import "SIXDLikeContentSetting.h"


@interface SIXDMusicChannelVC ()

@end

@implementation SIXDMusicChannelVC

- (void)viewDidLoad
{
    
    [SIXDStylist setGAScreenTracker:@"View_Playlist_Contents"];
    [super viewDidLoad];
    _overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _SIXDAVPlayer_Audio = [[SIXDAVPlayer alloc]init];
    
    NSLog(@"2.PlayListDetails = %@",_PlayListDetails);
    
   
  
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(onSelectedMoreButton:)];
    [_PlayListDetailsView addGestureRecognizer:tap];
    
    _PlayListDetailsView.layer.borderWidth = 0.25;
    _PlayListDetailsView.layer.borderColor = [UIColorFromRGB(CLR_CELL_BORDER) CGColor];

    [self.view addSubview:_overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    NSLog(@"API_GetPlayListContents call>>");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.IsAppOpenedFromURL)
    {
        appDelegate.IsAppOpenedFromURL = false;
        [self getMusicChannelList];
    }
    else
    {
        [self FillFromPlaylistDetails];
        [self API_GetPlayListContents];
    }
    
}

-(void)FillFromPlaylistDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *ImageURL = [NSURL URLWithString:[_PlayListDetails objectForKey:@"previewImage"]];
    [_PlayListImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    _PlayListName.text = [[_PlayListDetails objectForKey:@"channelName"] uppercaseString];
    if([appDelegate.Language isEqualToString:@"English"])
    {
        _PlayListName.textAlignment=NSTextAlignmentLeft;
    }
    else
    {
        _PlayListName.textAlignment=NSTextAlignmentRight;
    }
    
    _LikeButton.tag = -1;
    [SIXDStylist loadLikeUnlikeButtonSetings:_LikeButton  :[_PlayListDetails objectForKey:@"toneCode"]];
    
    //***set like count *********
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([_PlayListDetails objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[_PlayListDetails objectForKey:@"toneCode"] :[[_PlayListDetails objectForKey:@"likeCount"]intValue] :_LikeLabel];
    }
    else
        _LikeLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count end *********
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return _TuneList.count;
    //return 15;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if(indexPath.row >= _TuneList.count)
    {
        NSLog(@"index path crossed its limits");
        return cell;
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
   // SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *Dict = [_TuneList objectAtIndex:indexPath.row];
    cell.TitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
    cell.SubTitleLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
    
     //NSURL *ImageURL = [NSURL URLWithString:[_PlayListDetails objectForKey:@"previewImage"]];
    
    NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
    [cell.CustomImageView sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default"]];
    
    //LIKE SETTINGS
    NSLog(@"MusicChannel::Calling likeSettingsSession >>>");
    [SIXDStylist loadLikeUnlikeSetings:cell :[Dict objectForKey:@"toneId"]];
    
    /*
    NSString *title;
    if([Dict objectForKey:@"likeCount"])
        title = [NSString stringWithFormat:@" %@ %@ ",[Dict objectForKey:@"likeCount"],NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    else
        title = [NSString stringWithFormat:@" %@ %@ ",LIKE_COUNNT_DEFAULT,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    */
    
    //***set like count *********
    UILabel *TempLabel = [UILabel new];
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    if([Dict objectForKey:@"likeCount"])
    {
        [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :TempLabel];
    }
    else
        TempLabel.text =  LIKE_COUNNT_DEFAULT;
    //***set like count enda *********
    
    
    int likeCount = [TempLabel.text intValue];
    NSString *title;
    if(likeCount == 1)
    {
        title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"like",nil,appDelegate.LocalBundel,nil)];
    }
    else
    {
        title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    }
    
   // NSString *title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
    
    cell.GradientImageView.backgroundColor = [UIColor blackColor];
    cell.GradientImageView.alpha = 0.4;
    
    [cell.LikeButton setTitle:title forState:UIControlStateNormal];
    cell.LikeButton.tag=indexPath.row;
    cell.AccessoryButton.tag = indexPath.row;

    cell.SubView.layer.borderWidth = 0.5;
    cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_CELL_BORDER) CGColor];
    
    [cell.AccessoryButton setImage:[UIImage imageNamed:@"pauserow"] forState:UIControlStateSelected];
    
     [cell.AccessoryButton setImage:[UIImage imageNamed:@"playrow"] forState:UIControlStateNormal];

    
    [cell.contentView bringSubviewToFront:cell.CustomImageView];
    [cell.contentView bringSubviewToFront:cell.GradientImageView];
    [cell.contentView bringSubviewToFront:cell.AccessoryButton];
    
    
    
    return cell;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    //NSString *URL = [_PlayListDetails objectForKey:@"previewImage"];
    [SIXDGlobalViewController setNavigationBarTransparentWithImage:self :@"playlistheader"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
       NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
    self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"PLAYLISTS",nil,appDelegate.LocalBundel,nil);
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
        self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"PLAYLISTS",nil,appDelegate.LocalBundel,nil);
    }
    else
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"GESSTextMedium-Medium" size:17]}];
        self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"PLAYLISTS",nil,appDelegate.LocalBundel,nil);
    }
    */
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
}

-(void)API_GetPlayListContents
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    appDelegate.APIFlag = 0;
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    NSString *Rno2 = [SIXDGlobalViewController generateRandomNumber];
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/music-box-contents",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&toneCode=%@&type=MB&pageNo=1&perPageCount=20&clientTxnId=%@&clientSessionId=%@",URL,appDelegate.Language,[_PlayListDetails objectForKey:@"toneCode"],Rno1,Rno2];
    
    [ReqHandler sendHttpGETRequest:self :nil];
    
}
- (IBAction)onSelectedMoreButton:(id)sender
{
    [_SIXDAVPlayer_Audio stopPlay];
    _SelectedButton.selected = !_SelectedButton.selected;
    NSLog(@"onSelectedMoreButton >>>>");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDPlaylistBuyVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlaylistBuyVC"];
    ViewController.PlaylistDetails = _PlayListDetails;
    [self.navigationController pushViewController:ViewController animated:false];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    NSLog(@"getPlayListContents Response = %@",Response);
    Response = [Response objectForKey:@"responseMap"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag == GET_BANNER_MUSICBOX)
    {
        NSMutableArray *temp = [Response objectForKey:@"categoryList"];
        _PlayListDetails = [[temp objectAtIndex:0]mutableCopy];
        NSLog(@"_PlayListDetails from API = %@",_PlayListDetails);
        [self.view addSubview:_overlayview];
        [_ActivityIndicator startAnimating];
        [self.view bringSubviewToFront:_ActivityIndicator];
        [self FillFromPlaylistDetails];
        [self API_GetPlayListContents];
    }
    else
    {
        _TuneList = [Response objectForKey:@"searchList"];
        [_TableView reloadData];
    }
    
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
    NSLog(@"onFailureResponseRecived Response = %@",Response);
    
}

-(void)onFailedToConnectToServer
{
    [_overlayview removeFromSuperview];
    [_ActivityIndicator stopAnimating];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}
- (IBAction)onSelectedBuyButton:(UIButton*)sender
{
    [_SIXDAVPlayer_Audio stopPlay];
    _SelectedButton.selected = !_SelectedButton.selected;
    NSLog(@"onSelectedBuyButton >>>");
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SIXDBuyToneVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDBuyToneVC"];
    ViewController.IsSourceBuyPlaylist = true;
    ViewController.ToneDetails = _PlayListDetails;
    [self presentViewController:ViewController animated:YES completion:nil];
    
}
- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    NSLog(@"MusicChannel::Calling onSelectedLikeButton >>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    NSMutableDictionary *TuneDetails;
    NSString *tuneId;
    NSString *categoryId;
    if(sender.tag == -1)
    {
        tuneId = [_PlayListDetails objectForKey:@"toneCode"];
        categoryId = [_PlayListDetails objectForKey:@"categoryId"];
    }
    else
    {
        TuneDetails = [_TuneList objectAtIndex:sender.tag];
        tuneId = [TuneDetails objectForKey:@"toneId"];
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
    
    int likeButtonAction;
    SIXDCustomTVCell *cell;
    
    if(sender.tag == -1)
    {
        likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :_LikeLabel];
    }
    else
    {
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        UILabel *TempLabel = [UILabel new];
        //if([TuneDetails objectForKey:@"likeCount"];)
        TempLabel.text = [TuneDetails objectForKey:@"likeCount"];
        
        likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :TempLabel];
        int likeCount = [TempLabel.text intValue];
        NSString *title;
        if(likeCount == 1)
        {
            title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"like",nil,appDelegate.LocalBundel,nil)];
        }
        else
        {
            title = [NSString stringWithFormat:@" %@ %@ ",TempLabel.text,NSLocalizedStringFromTableInBundle(@"likes",nil,appDelegate.LocalBundel,nil)];
        }
        [cell.LikeButton setTitle:title forState:UIControlStateNormal];
    }
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SubTitleLabel.text,cell.TitleLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
    
    
    /*
    if(sender.tag == -1)
    {
        //entire playlist
    }
    else
    {
        //single tone from a playlist
    }
     */
}

- (IBAction)onSelectedPlayPauseButton:(UIButton *)sender
{
    NSLog(@"onSelectedPlayPauseButton>>>");
    [_SIXDAVPlayer_Audio stopPlay];

    if(_SelectedButton.tag != sender.tag)
    {
        _SelectedButton.selected = !_SelectedButton.selected;
    }
    
    sender.selected = !sender.selected;
    _SelectedButton = sender;
    if(sender.selected)
    {
         NSLog(@"playing>>>>");
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
        SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        [cell.ActivityIndicator startAnimating];
        [self.view setUserInteractionEnabled:false];
        NSMutableDictionary *Dict = [_TuneList objectAtIndex:sender.tag];
        _SIXDAVPlayer_Audio.SIXVCCallBack = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [_SIXDAVPlayer_Audio initialisePlayer :[Dict objectForKey:@"toneUrl"]];
            //[_SIXDAVPlayer_Audio playIO];
        });
        
    }
    else
    {
         [_SIXDAVPlayer_Audio stopPlay];
        _SelectedButton = nil;
        NSLog(@"play stop>>>>");
        
    }

}

- (void)onPlayStopped
{
    NSLog(@"onPlayStopped>>>");
    _SelectedButton.selected = !_SelectedButton.selected;
    _SelectedButton = nil;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [_SIXDAVPlayer_Audio stopPlay];
    _SelectedButton = nil;
    _SIXDAVPlayer_Audio = nil;
}

-(void)onPlayStarted
{
    NSLog(@"onPlayStarted>>>");
    dispatch_async(dispatch_get_main_queue(), ^{
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
    SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
    [cell.ActivityIndicator stopAnimating];
    [self.view setUserInteractionEnabled:true];
         });
    
}

-(void)onPlayFailure
{
    NSLog(@"onPlayFailure");
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:_SelectedButton.tag inSection:0];
        SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
        [cell.ActivityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:true];
        _SelectedButton.selected = !_SelectedButton.selected;
        _SelectedButton = nil;
    });
    
}


-(void)getMusicChannelList
{
    NSLog(@"getMusicChannelList>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag = GET_BANNER_MUSICBOX;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    NSString *Rno1 = [SIXDGlobalViewController generateRandomNumber];
    NSString *Rno2 = [SIXDGlobalViewController generateRandomNumber];
    
    NSLog(@"Tune id in music channel = %@",appDelegate.ToneID);
    
    ReqHandler.Request = [NSString stringWithFormat:@"toneCode=%@&type=MB&isAll=T&contentTypeId=2&clientTxnId=%@&clientSessionId=%@&language=%@",appDelegate.ToneID,Rno1,Rno2,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-play-list",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

@end
