//
//  SIXDTellAFriendVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 29/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
@import ContactsUI;

@interface SIXDTellAFriendVC : ViewController<UITextFieldDelegate,UIPickerViewDelegate, CNContactPickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *SubView;

@property (weak, nonatomic) IBOutlet UIImageView *ToneImage;

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *DescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *AlertLabel;

@property (weak, nonatomic) IBOutlet UIButton *ConfirmButton;
@property (weak, nonatomic) IBOutlet UIButton *CloseButton;

- (IBAction)onClickedConfirmButton:(UIButton *)sender;
- (IBAction)onClickedCloseButton:(UIButton *)sender;
- (IBAction)onClickedContactButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property(strong,nonatomic)NSArray *PhoneNumbers;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) UITextField *NumberTextField;

@property(strong,nonatomic)NSMutableArray *TextFieldList;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;

@property(nonatomic) BOOL IsSourcePlaylist;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *PopUpViewHeight;
@property(nonatomic) BOOL ShouldDismissView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TableViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;

@property(strong,nonatomic)NSMutableArray *BPartyList;

@end
