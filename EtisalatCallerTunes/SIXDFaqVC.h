//
//  SIXDFaqVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDFaqVC : ViewController <NSURLSessionDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic)  NSUInteger SelectedRow;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSMutableArray *Questions;
@property(strong,nonatomic) NSMutableArray *Answers;
- (IBAction)onClickedDownButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *MainImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@end
