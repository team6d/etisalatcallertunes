//
//  SIXDMyTunesVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 30/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDMyTunesVC : ViewController

@property(strong,nonatomic) NSArray *MenuList;
@property(strong,nonatomic) NSArray *MenuImageList;
@property (weak, nonatomic) IBOutlet UITableView *TableView;

@end
