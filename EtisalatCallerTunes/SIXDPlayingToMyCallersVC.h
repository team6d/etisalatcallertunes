//
//  SIXDPlayingToMyCallersVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"

@interface SIXDPlayingToMyCallersVC : ViewController

@property (weak, nonatomic) IBOutlet UIImageView *BannerImage;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageHeight;
@property(strong,nonatomic)NSMutableArray *PlayingTunesList;
//@property(strong,nonatomic)NSMutableArray *ServiceList;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

- (IBAction)onClickedDeleteButton:(UIButton *)sender;
@property(nonatomic) NSInteger ButtonTag;

@property (weak, nonatomic) IBOutlet UIView *AlertView;
@property(nonatomic) BOOL IsRefreshRequired;
@end
