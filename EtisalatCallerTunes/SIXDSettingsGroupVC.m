//
//  SIXDSettingsGroupVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDSettingsGroupVC.h"
#import "SIXDGlobalViewController.h"
#import "SIXDHeader.h"
#import "AppDelegate.h"
#import "SIXDStylist.h"
#import "UIImageView+WebCache.h"
#import "SIXDPickerPopUpNormal.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGroupConfiguration.h"
#import "SIXDCommonPopUp_Text.h"

@interface SIXDSettingsGroupVC ()

@end

@implementation SIXDSettingsGroupVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [SIXDStylist setGAScreenTracker:@"Tune_Settings_WhoToPlay"];
    
    if([_ToneDetails objectForKey:@"artistName"])
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"artistName"] capitalizedString];
        _BottomLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
    else
    {
        _TopLabel.text = [[_ToneDetails objectForKey:@"toneName"] capitalizedString];
    }
   
    
    NSURL *ImageURL = [NSURL URLWithString:[_ToneDetails objectForKey:@"previewImageUrl"]];
    [_MainImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
    
    [_NumberTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
     _MainSettingsVC.UserSettingsType = -1;
    _AllCallersButton.tag=ALLCALLERS;
    _CallerGroupsButton.tag=GROUP;
    _SpecialCallerButton.tag=SPL_CALLERS;
    
    _NumberTextField.keyboardType = ASCII_NUM_KEYBOARD;
    
    _SelectedButton=_AllCallersButton;
    _SelectedButton.selected = !_SelectedButton.selected;
    
    [self localisation];
    
    _NumberButton.hidden = YES;
    _AlertLabel.hidden = YES;
    _ExampleLabel.hidden = YES;
    _NumberView.hidden = YES;
    _NumberTextField.delegate = self;
    _NumberTextField.enablesReturnKeyAutomatically = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *NewGroupTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(CreateNewGroupTap:)];
    [self.ExampleLabel addGestureRecognizer:NewGroupTap];
    
    [SIXDStylist addKeyBoardDoneButton:_NumberTextField :self];
    
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _VerticalSpace1.constant = 5;
         _VerticalSpace2.constant = 5;
         _VerticalSpace3.constant = 5;
        
    }
    
}

-(void) dismissKeyboard
{
    
    NSLog(@"dismissKeyboard>>>");
    
    [_NumberTextField resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"SETTINGS",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
    
    if(_SelectedButton.tag == GROUP)
    {
        if(_NumberTextField.text.length==0)
        {
            _NumberLabel.hidden=YES;
            _NumberTextField.text = @"";
            _TopSpaceToNumber.constant=0;
            [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            
        }
        else
        {
            _NumberLabel.hidden=NO;
            _TopSpaceToNumber.constant=19;
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self clearAlert];
    _TopSpaceToNumber.constant=19;
    _NumberLabel.hidden=NO;
    _AlertLabel.hidden=YES;
     [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    
    if(_SelectedButton.tag == SPL_CALLERS)
    {
        _NumberTextField.tintColor =UIColorFromRGB(CLR_PLACEHOLDER);
        _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Caller Number",nil,appDelegate.LocalBundel,nil);
        if(appDelegate.DeviceSize == SMALL)
        {
           [_MainScrollView setContentOffset:CGPointMake(0,([UIScreen mainScreen].bounds.size.height/2)-50) animated:YES];
            
        }
        else
        {
             [_MainScrollView setContentOffset:CGPointMake(0,([UIScreen mainScreen].bounds.size.height/2)-100) animated:YES];
        }
        
        if(_NumberTextField.text.length <= 0)
        {
            _NumberTextField.text = @"+971-5";
        }
    }
    else if(_SelectedButton.tag == GROUP)
    {
        _NumberTextField.tintColor =[UIColor clearColor];
        _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil);
        _NumberTextField.text = @"";
        [textField resignFirstResponder];
        
        if(_GroupList.count != 0)
        {
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
            SIXDPickerPopUpNormal *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPickerPopUpNormal"];
            ViewController.GroupVC =self;
            ViewController.SOURCE_TYPE=FOR_PEOPLE;
            [self presentViewController:ViewController animated:YES completion:nil];
        }
        else
        {
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
            SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
            ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"no groups at the moment",nil,appDelegate.LocalBundel,nil);
            ViewController.AlertImageName = @"mytunelibrarypopupinfo";
ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
            [self presentViewController:ViewController animated:YES completion:nil];
            
        }
        
    
        
    }

    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_MainScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
    if(_SelectedButton.tag==SPL_CALLERS)
    {
        if(_NumberTextField.text.length==6)
        {
            _NumberLabel.hidden=YES;
            _NumberTextField.text = @"";
            [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Caller Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _TopSpaceToNumber.constant=0;
        }
        
    }
    else if(_SelectedButton.tag == GROUP)
    {
        if(_NumberTextField.text.length==0)
        {
            _NumberLabel.hidden=YES;
            _NumberTextField.text = @"";
            [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _TopSpaceToNumber.constant=0;
        }
    }
}

- (IBAction)onClickedConfirmButton:(id)sender {
    
    _MainSettingsVC.onClickedConfirmFromSettingsPopUP = TRUE;
    
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_NumberTextField :_NumberView :nil];
    
    switch (_SelectedButton.tag) {
        case ALLCALLERS:
            _MainSettingsVC.UserSettingsType = SETTINGS_USER_ALL_CALLERS;
            // _MainSettingsVC.ScreenType = END_SETTINGS_SCREEN;
            [_MainSettingsVC.navigationController popViewControllerAnimated:NO];
            NSLog(@"ALLCALLERS");
            break;
            
        case GROUP:
            NSLog(@"GROUP");
            if([_NumberTextField.text isEqualToString:@""])
            {
              [self setEmptyFieldAlert];
            }
            else
            {
                _MainSettingsVC.UserSettingsType = SETTINGS_USER_GROUPS;
               // _MainSettingsVC.ScreenType = END_SETTINGS_SCREEN;
                [_MainSettingsVC.SettingsDataDict setObject:_NumberTextField.text forKey:@"groupName"];
                [_MainSettingsVC.navigationController popViewControllerAnimated:NO];
            }
            //[self setGroupDetails];
            
            break;
            
        case SPL_CALLERS:
        {
            NSLog(@"SPL_CALLERS");
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
             NSString *formatedNumber = [_NumberTextField.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_NumberTextField.text length])];
            NSLog(@"formattednumber = %@",formatedNumber);
            NSString *Aparty = [appDelegate.MobileNumber substringFromIndex:1];
            
            if([_NumberTextField.text isEqualToString:@""])
            {
                [self setEmptyFieldAlert];
            }
            /*
            else if(![SIXDStylist isEtisalatValidNumber:_NumberTextField.text])
            {
                
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please enter a valid Etisalat number",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
                
                [SIXDStylist setTextViewAlert:_NumberTextField :_NumberView :nil];
            }
            */
            else if([formatedNumber rangeOfString:Aparty].location != NSNotFound )
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"*be generous and enter another number than yours",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
                
               [SIXDStylist setTextViewAlert:_NumberTextField :_NumberView :nil];
            }
            else
            {
                _MainSettingsVC.UserSettingsType = SETTINGS_USER_DEDICATED;
                [_MainSettingsVC.SettingsDataDict setObject:formatedNumber forKey:@"bPartyMsisdn"];
                [_MainSettingsVC.SettingsDataDict setObject:_NumberTextField.text forKey:@"bPartyMsisdnLabel"];
                //_MainSettingsVC.ScreenType = END_SETTINGS_SCREEN;
                [_MainSettingsVC.navigationController popViewControllerAnimated:NO];
            }
           
            break;
        }
        default:
            NSLog(@"Something went wrong, switch ran into  default statement");
            break;
    }
    _MainSettingsVC.ScreenType = END_SETTINGS_SCREEN;
}

- (void) setEmptyFieldAlert
{
  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.text = NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
    _AlertLabel.hidden=NO;
    [SIXDStylist setTextViewAlert:_NumberTextField :_NumberView :nil];
}

- (void) clearAlert
{
    _AlertLabel.hidden=YES;
    [SIXDStylist setTextViewNormal:_NumberTextField :_NumberView :nil];
}
- (void)setGroupDetails
{
    NSLog(@"setGroupDetails");
    
    //[_MainSettingsVC.SettingsDataDict setObject:<#(nonnull id)#> forKey:@"groupId"];

}
-(void)localisation
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [_AllCallersButton setTitle:NSLocalizedStringFromTableInBundle(@"All Callers",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [_CallerGroupsButton setTitle:NSLocalizedStringFromTableInBundle(@"Callers Group",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [_SpecialCallerButton setTitle:NSLocalizedStringFromTableInBundle(@"Specific Callers",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    [_AllCallersButton setImage: [UIImage imageNamed:@"mylibraryoutline2"]forState:UIControlStateNormal];
    [_AllCallersButton setImage: [UIImage imageNamed:@"mylibrarytick2"]forState: UIControlStateSelected];
    [_CallerGroupsButton setImage: [UIImage imageNamed:@"mylibraryoutline2"]forState:UIControlStateNormal];
    [_CallerGroupsButton setImage: [UIImage imageNamed:@"mylibrarytick2"]forState: UIControlStateSelected];
    [_SpecialCallerButton setImage: [UIImage imageNamed:@"mylibraryoutline2"]forState:UIControlStateNormal];
    [_SpecialCallerButton setImage: [UIImage imageNamed:@"mylibrarytick2"]forState: UIControlStateSelected];
    
    [SIXDStylist setTextViewNormal:_NumberTextField :_NumberView :_NumberButton];
    
    [_ConfirmButton setTitle:NSLocalizedStringFromTableInBundle(@"CONFIRM",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    
    _InfoLabel.text = NSLocalizedStringFromTableInBundle(@"SELECT TO WHOM YOU WANT TO PLAY IT",nil,appDelegate.LocalBundel,nil);

    if([appDelegate.Language isEqualToString:@"Arabic"])
    {
        _AllCallersButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _CallerGroupsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _SpecialCallerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)onClickedAllCallersButton:(id)sender
{
     UIButton *temp= (UIButton *)sender;
    
    if(_SelectedButton.tag != temp.tag)
    {
        _SelectedButton.selected = !_SelectedButton.selected;
        
        temp.selected = !temp.selected;
        _SelectedButton = temp;
        
        _NumberView.hidden = YES;
        _ExampleLabel.hidden = YES;
        _AlertLabel.hidden = YES;
        
    }
}
- (IBAction)onClickedCallerGroupsButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self clearAlert];
    UIButton *temp= (UIButton *)sender;
    
    if(_SelectedButton.tag != temp.tag)
    {
        _SelectedButton.selected = !_SelectedButton.selected;
        
        temp.selected = !temp.selected;
        _SelectedButton = temp;
        
        _NumberView.hidden = NO;
        _ExampleLabel.hidden = NO;
        _AlertLabel.hidden = YES;
        _NumberLabel.hidden = YES;
        _NumberButton.hidden = YES;
        _NumberTextField.text = @"";
        _TopSpaceToNumber.constant=0;
        _ExampleLabel.userInteractionEnabled = TRUE;
        
        [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        
        NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"create new group",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8]}];
        _ExampleLabel.attributedText = titleString;
        /*
        if([appDelegate.Language isEqualToString:@"English"])
        {
            NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"create new group",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8]}];
            _ExampleLabel.attributedText = titleString;
        }
        else
        {
            NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"create new group",nil,appDelegate.LocalBundel,nil) attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:8]}];
            _ExampleLabel.attributedText = titleString;
        }
        */
    }
}
- (IBAction)onClickedSpecialCallerButtonButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self clearAlert];
    UIButton *temp= (UIButton *)sender;
    
    if(_SelectedButton.tag != temp.tag)
    {
        _SelectedButton.selected = !_SelectedButton.selected;
        
        temp.selected = !temp.selected;
        _SelectedButton = temp;
        
        _NumberView.hidden = NO;
        _ExampleLabel.hidden = NO;
        _AlertLabel.hidden = YES;
        _NumberButton.hidden = NO;
        _NumberLabel.hidden = YES;
        _NumberTextField.text = @"";
        _TopSpaceToNumber.constant=0;
        _ExampleLabel.userInteractionEnabled = FALSE;
        
        [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Caller Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        
        NSMutableAttributedString *titleString;
        NSAttributedString *titleString0;
        NSAttributedString *titleStringbetween;
        
        
        titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"example",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8]}];
        
        titleStringbetween = [[NSMutableAttributedString alloc] initWithString:@"\n"];
        
        titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"+971-50xxxxxxx, 050xxxxxxx",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN)}];
        
        /*
        
        if([appDelegate.Language isEqualToString:@"English"])
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"example",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8]}];
            
            titleStringbetween = [[NSMutableAttributedString alloc] initWithString:@"\n"];
            
            titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"+971-50xxxxxxx, 050xxxxxxx",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN)}];
        }
        else
        {
            titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"example",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8]}];
            
            titleStringbetween = [[NSMutableAttributedString alloc] initWithString:@"\n"];
            
            titleString0 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"+971-50xxxxxxx, 050xxxxxxx",nil,appDelegate.LocalBundel,nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:8],NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN)}];
        }
        */
        [titleString appendAttributedString:titleStringbetween];
        [titleString appendAttributedString:titleString0];
        _ExampleLabel.attributedText = titleString;
        
    }
}

- (void)CreateNewGroupTap:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"CreateNewGroupTap >>");
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    
    SIXDGroupConfiguration *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDGroupConfiguration"];
    ViewController.SOURCE=FROM_CREATE_GROUP;
    [self.navigationController pushViewController:ViewController animated:YES];
}

-(void)API_ListTones
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/list-tones",appDelegate.BaseMiddlewareURL];
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?language=%@&msisdn=%@&startIndex=0&endIndex=20&rbtMode=2",URL,appDelegate.Language,appDelegate.MobileNumber];
    
    [ReqHandler sendHttpGETRequest:self :nil];
}

-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    _GroupList = [NSMutableArray new];
    _GroupIdList = [NSMutableArray new];
    NSMutableDictionary *responseMap=[Response objectForKey:@"responseMap"];
    NSMutableArray *listToneApk =[responseMap objectForKey:@"listToneApk"];
    
    
    for(int i=0;i<listToneApk.count;i++)
    {
        NSMutableDictionary *Dict = [listToneApk objectAtIndex:i];
        [_GroupList addObject:[Dict objectForKey:@"groupName"]];
        [_GroupIdList addObject:[Dict objectForKey:@"groupId"]];
    }
    
    NSLog(@"GroupList = %@",_GroupList);
    NSLog(@"GroupIdList = %@",_GroupIdList);
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_ListTones onFailureResponseRecived = %@",Response);
    //[_AlertView setHidden:false];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    NSLog(@"API_ListTones onFailedToConnectToServer");
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}

- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"API_ListTones call >>");
    [self API_ListTones];
}

- (IBAction)onSelectedContactButton:(UIButton *)sender
{
    NSLog(@"onSelectedContactButton");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self clearAlert];
    _TopSpaceToNumber.constant=19;
    _NumberLabel.hidden=NO;
    _AlertLabel.hidden=YES;
    [_NumberTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
    _NumberTextField.tintColor =UIColorFromRGB(CLR_PLACEHOLDER);
    _NumberLabel.text = NSLocalizedStringFromTableInBundle(@"Caller Number",nil,appDelegate.LocalBundel,nil);
    
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
    
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    _PhoneNumbers = contact.phoneNumbers;
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
    
}

-(void)ShowMultipleNumbers
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _NumberTextField.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _NumberTextField.text = [label.value stringValue];
    }
    
}



@end
