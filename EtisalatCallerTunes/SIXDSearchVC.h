//
//  SIXDSearchVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 28/12/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "SIXDTuneSearchVC.h"
#import "SIXDArtistSearchVC.h"

@interface SIXDSearchVC : ViewController<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UIView *TopView;
@property (weak, nonatomic) IBOutlet UIView *SegmentView;

@property (weak, nonatomic) IBOutlet UILabel *HeadingLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *SearchBar;
@property (weak, nonatomic) IBOutlet UIButton *SongCatcherButton;
@property (weak, nonatomic) IBOutlet UILabel *SearchResultsLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentTab;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *MainBtn_xPosition;


@property (nonatomic) int typeFlag;

@property (weak, nonatomic) IBOutlet UIView *TunesContainer;
@property (weak, nonatomic) IBOutlet UIView *ArtistContainer;

@property(strong,nonatomic) SIXDTuneSearchVC *TunesVC;
@property(strong,nonatomic) SIXDArtistSearchVC *ArtistsVC;
- (IBAction)onClickedSongCatcher:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *MikeButton;
@property (weak, nonatomic) IBOutlet UIView *SearchView;
@property (weak, nonatomic) IBOutlet UILabel *ClickToCatchLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *YAlignmentToClickToCatchButton;

//To Reduce search banner height
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BannerImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopSpaceToSearchBar;
@property(strong,nonatomic) UIView *AlertView;
//end

@property (nonatomic) BOOL IsRefreshRequired;
@end
