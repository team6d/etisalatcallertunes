//
//  SIXDMainSettingsVC.h
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 16/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "ViewController.h"
#import "CustomLabel.h"
#import "SIXDPlayingToMyCallersVC.h"

@interface SIXDMainSettingsVC : ViewController
@property (weak, nonatomic) IBOutlet UIView *UpperView;
@property (weak, nonatomic) IBOutlet UIImageView *MainImage;
@property (weak, nonatomic) IBOutlet UILabel *TopLabel;
@property (weak, nonatomic) IBOutlet UILabel *BottomLabel;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (weak, nonatomic) IBOutlet UIView *TimeSettingView;
@property (weak, nonatomic) IBOutlet UIButton *SetTuneButton;
@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollView;

@property(strong,nonatomic) NSMutableDictionary *ToneDetails;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;
@property (strong,nonatomic)UIView *overlayview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TimeBasedViewHeight;
@property  int TimeSettingsType;
@property  int UserSettingsType;
@property BOOL isStatusRBT;
@property (strong,nonatomic) NSString *httpRequestString;
@property (strong,nonatomic) NSMutableDictionary *SettingsDataDict;

@property int ScreenType;
@property int PopUpFlag;

@property (weak, nonatomic) IBOutlet CustomLabel_7 *BelowCenterTopLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_7 *BelowRightTopKLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_7 *BelowLeftTopLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_10 *BelowCenterBottomLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_10 *BelowLeftBottomLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_10 *BelowRightBottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *BelowButton;
@property (weak, nonatomic) IBOutlet CustomLabel_7 *BelowTopLabel;
@property (weak, nonatomic) IBOutlet CustomLabel_10 *BelowBottomLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TableViewHeight;
@property(strong,nonatomic) SIXDPlayingToMyCallersVC *PlayingToMyCallerVC;

@property BOOL onClickedConfirmFromSettingsPopUP;


@end
