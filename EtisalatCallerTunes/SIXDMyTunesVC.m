//
//  SIXDMyTunesVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 30/01/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDMyTunesVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDStylist.h"

@interface SIXDMyTunesVC ()

@end

@implementation SIXDMyTunesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [SIXDStylist setGAScreenTracker:@"View_MyTunes"];
    
    _TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.MenuList = [[NSArray alloc] initWithObjects:@"Playing to my callers_CAPS", @"my tunes library_CAPS",nil];
    self.MenuImageList = [[NSArray alloc] initWithObjects:@"playingtomycallers",@"mytuneslibrary", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _MenuList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.TitleLabel.text = NSLocalizedStringFromTableInBundle([_MenuList objectAtIndex:indexPath.row],nil,appDelegate.LocalBundel,nil);
    
    NSString *imageName = [_MenuImageList objectAtIndex:indexPath.row];
    [cell.AccessoryButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [cell.AccessoryButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];

    UIImageView *sampleImage;
    if([appDelegate.Language isEqualToString:@"English"])
    {
        sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow"]];
    }
    else
    {
        sampleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuarrow_Arabic"]];
    }
    [cell setAccessoryView:sampleImage];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        NSLog(@"Playing to my callers");
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayingToMyCallersVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
    else
    {
        NSLog(@"MyTunes Library");
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMyTunesLibraryVC"];
        [self.navigationController pushViewController:ViewController animated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarOpaque:self];
    [SIXDGlobalViewController setNavigationBarTiTle:self :NSLocalizedStringFromTableInBundle(@"MY TUNES",nil,appDelegate.LocalBundel,nil) :CLR_ETISALAT_GREEN];
}

@end
