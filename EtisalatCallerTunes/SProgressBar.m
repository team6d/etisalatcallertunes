//
//  SProgressBar.m
//  SQLExample2
//
//  Created by Shahnawaz Nazir on 05/02/18.
//  Copyright © 2018 Shahnawaz Nazir. All rights reserved.
//

#import "SProgressBar.h"
#import "SIXDHeader.h"


@interface SProgressBar () {
    CGFloat startAngle;
    CGFloat endAngle;
}

@end

@implementation SProgressBar



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 1.5;
        endAngle = startAngle + (M_PI * 2);
        
    }
    return self;
}
/*
- (void)drawRect:(CGRect)rect
{
    
    CGFloat constant = rect.size.width/ 5;
    ///
    
     
     ///
    
    
    UIImage *img = [UIImage imageNamed:@"instagram-icon.png"]; // lion.jpg is image name
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x + constant/2, rect.origin.y + constant/2, rect.size.width-constant, rect.size.height - constant)];
    imgView.image = img;
    imgView.layer.masksToBounds = YES;
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:constant*2
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 10;
    [UIColorFromRGB(CLR_RED) setStroke];
    [bezierPath stroke];

   [self addSubview:imgView];
    
}
*/
- (void)drawRect:(CGRect)rect
{
    NSLog(@"drawRect -->");
    CGFloat constant = rect.size.width/ 5;
    
     //CGFloat constant = rect.size.width /10;
    //CGFloat constant = _RecordButton.frame.size.width / 5;
    
   // UIImage *img = [UIImage imageNamed:@"instagram-icon.png"]; // lion.jpg is image name
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x + constant/2, rect.origin.y + constant/2, rect.size.width-constant, rect.size.height - constant)];
    //_SaveButtonOutlet.imageView.image = [UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x + constant/2, rect.origin.y + constant/2, rect.size.width-constant, rect.size.height - constant)];
    
     // imgView.image = img;
   // UIImageView *imgView = [UIImageView alloc];
    imgView.image = _RecordButton.imageView.image;
 //   imgView = _RecordButton.imageView;
 
      imgView.layer.masksToBounds = YES;
    imgView.layer.cornerRadius = imgView.frame.size.width / 2;
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
   /*
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:constant*2
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                       clockwise:YES];
 
    */
    [bezierPath addArcWithCenter:CGPointMake(_RecordButton.frame.size.width /2 , _RecordButton.frame.size.height /2)
                          radius:constant *2
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (_percent / 100.0) + startAngle
                       clockwise:YES];
    
    
    
    
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 2;
    [UIColorFromRGB(CLR_RED) setStroke];
    [bezierPath stroke];
    
    
    // [self addSubview:_SaveButtonOutlet.imageView];
    // [self addSubview:_RecordButton.imageView]; // gts exception
   // [self addSubview:imgView];
    
}

@end
