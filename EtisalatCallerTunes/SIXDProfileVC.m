//
//  SIXDProfileVC.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 06/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDProfileVC.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "CustomLabel.h"
#import "SIXDChangeProfileDetailsVC.h"
#import "SIXDCommonPopUp_Text.h"
#import "SIXDCommonPopUp_View.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDNotificationLanguage.h"
#import "UIImageView+WebCache.h"
#import "SIXDStylist.h"

#define TAG_NOTIFICATION_LANG  20
#define MULTI_PART_EDIT_PROFILE     3

//#define MULTI_PART_FILE_UPLOAD          5

@interface SIXDProfileVC ()

@end

@implementation SIXDProfileVC

- (void)viewDidLoad
{
    
    NSLog(@"viewDidLoad>>> MY profile");
    [super viewDidLoad];
    
    [SIXDStylist setGAScreenTracker:@"View_Profile"];
    
    [self.view bringSubviewToFront:_ProfilePicture];
    
    _IsChangeNumberInvoked = false;
    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _IsInitialLoad = true;

    _CameraButton.layer.zPosition = 50;
    
    [self.view bringSubviewToFront:_CameraButton];
    _ProfilePicture.layer.borderColor = [[UIColor whiteColor]CGColor];
    _ProfilePicture.layer.borderWidth = 1;
    
     _TableView.contentInset = UIEdgeInsetsMake(0,0,25,0);
    
    _ScrollView = [UIScrollView new];
    
    _Titles = @[@"User Name*",@"Mobile Number*",@"Email*",@"Status*",@"Plan*",@"UPGRADE YOUR PLAN"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(onSelectedChangeProfileImage)];
    [_ProfilePicture setUserInteractionEnabled:true];
    [_ProfilePicture addGestureRecognizer:tap];
    [self.view bringSubviewToFront:_ProfilePicture];
    [_CoverView bringSubviewToFront:_ProfilePicture];
    
    
   
    
    //for testing
    /*
    _ProfileDetails  = [NSMutableDictionary new];
    [_ProfileDetails setObject:@"Temp name" forKey:@"userName"];
     [_ProfileDetails setObject:@"98765438765" forKey:@"Mobile Number*"];
     [_ProfileDetails setObject:@"temp@gmail.com" forKey:@"emailId"];
     [_ProfileDetails setObject:@"Active" forKey:@"Status*"];
     [_ProfileDetails setObject:@"not available" forKey:@"Plan*"];
     [_ProfileDetails setObject:@"98765438765" forKey:@"NotificationLanguage"];
    */
    //end
    
    [self.tabBarController.view addSubview:self.overlayview];
     [self.view bringSubviewToFront:_ActivityIndicator];
     [_ActivityIndicator startAnimating];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear>>> MY profile");
    
    if(_IsInitialLoad)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.CategoryList.count > 0)
        {
            [self loadScrollViewContent];
            NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpiryTime"];
            NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
            double ExpiryTime = [str doubleValue];
            if(Time >= ExpiryTime)
            {
                SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
                appDelegate.APIFlag = REGENERATETOKEN;
                ReqHandler.HttpsReqType = REGENERATETOKEN;
                ReqHandler.ParentView = self;
                [ReqHandler regenerateToken];
                
            }
            else
            {
               
                [self getProfileDetails];
            }
        }
        else
        {
           
            appDelegate.APIFlag = GET_PREFERED_CATEGORY_STATIC_LIST;
            SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
            ReqHandler.ParentView = self;
            [ReqHandler getPreferedCategoryList];
        }
        _IsInitialLoad = false;
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear >>> MY PROFILE");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
       NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
    self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"MY PROFILE",nil,appDelegate.LocalBundel,nil);
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:17]}];
        self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"MY PROFILE",nil,appDelegate.LocalBundel,nil);
    }
    else
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_WHITE),
           NSFontAttributeName:[UIFont fontWithName:@"GESSTextMedium-Medium" size:17]}];
        self.navigationItem.title= NSLocalizedStringFromTableInBundle(@"MY PROFILE",nil,appDelegate.LocalBundel,nil);
    }
    */
    
}

-(void)setButtonBorder:(UIView *)myTextView
{
    myTextView.layer.borderWidth = 1.0f;
    myTextView.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 6;
            break;
        
        case 1:
            return 1;
            break;
            
        default:
            return 0;
            break;
            
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.SubView.layer.borderWidth = 1.0f;
    cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
    
    if(indexPath.section == 0)
    {
         NSString *title = [_Titles objectAtIndex:indexPath.row];
        
        if(indexPath.row < 5)
        {
       
            cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(title,nil,appDelegate.LocalBundel,nil);
            
            if(indexPath.row < 3)
            {
                [cell.FirstButton setImage:[UIImage imageNamed:@"profileedit"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.FirstButton setHidden:true];
            }
            
            cell.SubTitleLabel.text = [_ProfileDetails objectForKey:[_Titles objectAtIndex:indexPath.row]];
            
            cell.SecondButton.hidden = true;
            cell.TitleLabel.hidden = false;
            cell.SubTitleLabel.hidden = false;
            cell.SubView.backgroundColor = [UIColor whiteColor];
            cell.alpha = 1;
            cell.userInteractionEnabled = true;
           
        }
        else
        {
            cell.SubView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"UpgradeYourPlan_0"]];
            [cell.SecondButton setTitle:NSLocalizedStringFromTableInBundle(title,nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
            [cell.SecondButton setUserInteractionEnabled:false];
            cell.SecondButton.layer.borderColor = [[UIColor whiteColor]CGColor];
            cell.SecondButton.layer.borderWidth = 1;
            cell.SecondButton.hidden = false;
            cell.TitleLabel.hidden = true;
            cell.SubTitleLabel.hidden = true;
            cell.FirstButton.hidden = true;
            
            
            NSString *PackName = [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
            if([PackName isEqualToString:@"CRBT_VIP"])
            {
                cell.contentView.alpha = 0.5;
                cell.userInteractionEnabled = false;
            }
            else
            {
                cell.contentView.alpha = 1;
                cell.userInteractionEnabled = true;
            }
            
            //TO DO set cell background to some image
        }
        
        cell.FirstButton.tag = indexPath.row;
    }
    else
    {
        cell.TitleLabel.text = NSLocalizedStringFromTableInBundle(@"Notification language*",nil,appDelegate.LocalBundel,nil);
         cell.SubTitleLabel.text = [_ProfileDetails objectForKey:@"NotificationLanguage"];
        [cell.FirstButton setImage:[UIImage imageNamed:@"profileedit"] forState:UIControlStateNormal];
        cell.SecondButton.hidden = true;
        cell.TitleLabel.hidden = false;
        cell.SubTitleLabel.hidden = false;
        cell.FirstButton.hidden = false;
        cell.SubView.backgroundColor = [UIColor whiteColor];
        
        //as we are using differnent section in tableview, using different tag for change notification langugae cell
        cell.FirstButton.tag = TAG_NOTIFICATION_LANG;
        cell.alpha = 1;
        cell.userInteractionEnabled = true;
       
    }
    
    
    
    
    return cell;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(25, 0, [UIScreen mainScreen].bounds.size.width-50,101)];
    
    //for testing
    if(section == 1)
    {
    
        UIView *SubView = [[UIView alloc] initWithFrame:CGRectMake(2, 5, [UIScreen mainScreen].bounds.size.width-54,96)];
        
        SubView.layer.borderWidth = 1.0f;
        SubView.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
        SubView.backgroundColor = [UIColor whiteColor];
        
    CustomLabel_10 *title;
   
   // title = [[CustomLabel_10 alloc] initWithFrame:CGRectMake(20,4,[UIScreen mainScreen].bounds.size.width-70,24)];
    title = [[CustomLabel_10 alloc] initWithFrame:CGRectMake(20,4,SubView.frame.size.width-40,24)];
    title.text = NSLocalizedStringFromTableInBundle(@"Preferences*",nil,appDelegate.LocalBundel,nil);
        
    title.textColor = UIColorFromRGB(CLR_TEXTFIELDLABEL);
    _ScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10,25,[UIScreen mainScreen].bounds.size.width-65,72)];
    
        [self loadScrollViewContent];
        [SubView addSubview:title];
        [SubView addSubview:_ScrollView];
        [headerView addSubview:SubView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(selectedPreference)];
        [headerView addGestureRecognizer:tap];
    }
    return headerView;
    
}

-(void)selectedPreference
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];

    SIXDChangeProfileDetailsVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDChangeProfileDetailsVC_UpdateCategories"];
    
    ViewController.title = NSLocalizedStringFromTableInBundle(@"TELL US WHAT DO YOU LIKE",nil,appDelegate.LocalBundel,nil);
    
    ViewController.ServiceType = UPDATE_CATAGORIES;
    ViewController.OldValue = [_ProfileDetails objectForKey:@"categories"];
    ViewController.ProfileVC = self;
    
    [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0;
    }
    else
    return 101;
}


- (IBAction)onSelectedEditButton:(UIButton *)sender
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //TO DO
    if(sender.tag == 1)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        
        [SIXDStylist setGAScreenTracker:@"Edit_Profile_Number"];
        
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"CHANGE MOBILE NUMBER DESCRIPTION",nil,appDelegate.LocalBundel,nil);
        ViewController.title = NSLocalizedStringFromTableInBundle(@"CHANGE MOBILE NUMBER",nil,appDelegate.LocalBundel,nil);
        
       /* ViewController.AlertMessage = @"You are about to delete selected tunes";
         ViewController.AlertImageName = @"errorinputicon";
        */
        ViewController.ConfirmButtonText = NSLocalizedStringFromTableInBundle(@"PROCEED",nil,appDelegate.LocalBundel,nil);
        ViewController.ParentView = self;
        
        _IsChangeNumberInvoked = true;
        
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
        
    }
    else if (sender.tag == TAG_NOTIFICATION_LANG)
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDNotificationLanguage *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDNotificationLanguage"];
        ViewController.NotificationLanguage = [_ProfileDetails objectForKey:@"NotificationLanguage"];
        ViewController.Source = CHANGE_NOTIFICATION_LANGUAGE;
        ViewController.ProfileVC = self;
        [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        
        SIXDChangeProfileDetailsVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDChangeProfileDetailsVC"];
        if(sender.tag == 0)
        {
            ViewController.title = NSLocalizedStringFromTableInBundle(@"CHANGE USERNAME",nil,appDelegate.LocalBundel,nil);
    
            ViewController.PlaceHolderList = @[NSLocalizedStringFromTableInBundle(@"Current Name*",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"New Name*",nil,appDelegate.LocalBundel,nil)];
            ViewController.ServiceType = UPDATE_USER_NAME;
            ViewController.OldValue = [_ProfileDetails objectForKey:@"User Name*"];
            ViewController.ProfileVC = self;
        
        }
        else
        {
            ViewController.title = NSLocalizedStringFromTableInBundle(@"CHANGE EMAIL",nil,appDelegate.LocalBundel,nil);
            
            ViewController.PlaceHolderList = @[NSLocalizedStringFromTableInBundle(@"Current Email",nil,appDelegate.LocalBundel,nil),NSLocalizedStringFromTableInBundle(@"New Email*",nil,appDelegate.LocalBundel,nil)];
            
            ViewController.ServiceType = UPDATE_EMAIL;
             ViewController.OldValue = [_ProfileDetails objectForKey:@"Email*"];
            ViewController.ProfileVC = self;
        }
        
         [self.tabBarController presentViewController:ViewController animated:YES completion:nil];
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 5)
    {
        NSString *PackName = [[NSUserDefaults standardUserDefaults] objectForKey:@"PACK_NAME"];
        if(![PackName isEqualToString:@"CRBT_VIP"])
        {
            UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
            UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDUpgradePlanVC"];
            [self.navigationController pushViewController:ViewController animated:YES];
        }
    }
}

-(void)getProfileDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_PROFILE_DETAILS ;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = POST;
    NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
    
    ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=GetUserDetails&language=%@&msisdn=%@",Rno,appDelegate.Language,appDelegate.MobileNumber];
   // ReqHandler.Request = [NSString stringWithFormat:@"clientTxnId=%@&identifier=GetUserDetails&language=%@",Rno,appDelegate.Language];
   
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/get-profile-details",appDelegate.BaseMiddlewareURL];
    
    [ReqHandler sendHttpPostRequest:self];
    
}

-(void)getPackDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag= GET_PACK_DETAILS ;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    ReqHandler.HttpsReqType = GET;
    NSString *URL = [NSString stringWithFormat:@"%@/crbt/pack-status",appDelegate.BaseMiddlewareURL];
    
    //PRIORITY 0-CRBT 1-RRBT
    ReqHandler.FinalURL =  [NSString stringWithFormat:@"%@?msisdn=%@&priority=0&language=%@",URL,appDelegate.MobileNumber,appDelegate.Language];
    
    [ReqHandler sendHttpGETRequest:self :nil];
    
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"onSuccessResponseRecived >>>> Response = %@",Response);
    if(appDelegate.APIFlag == REGENERATETOKEN)
    {
        [self getProfileDetails];
    }
    else if (appDelegate.APIFlag == GET_PREFERED_CATEGORY_STATIC_LIST)
    {
        //[self loadScrollViewContent];
        NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpiryTime"];
        NSTimeInterval Time = [[NSDate date] timeIntervalSince1970];
        double ExpiryTime = [str doubleValue];
        if(Time >= ExpiryTime)
        {
            appDelegate.APIFlag = REGENERATETOKEN;
            SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
            ReqHandler.HttpsReqType = REGENERATETOKEN;
            ReqHandler.ParentView = self;
            [ReqHandler regenerateToken];
            
        }
        else
        {
            [self.tabBarController.view addSubview:self.overlayview];
            [self.view bringSubviewToFront:_ActivityIndicator];
            [_ActivityIndicator startAnimating];
            [self getProfileDetails];
        }
    }
    else if(appDelegate.APIFlag == GET_PROFILE_DETAILS)
    {
        Response = [Response objectForKey:@"responseMap"];
        NSMutableDictionary *Dict = [[Response objectForKey:@"getProfileDetails"]mutableCopy];
        
        _ProfileDetails  = [NSMutableDictionary new];

        if([Dict objectForKey:@"userName"])
        [_ProfileDetails setObject:[Dict objectForKey:@"userName"] forKey:@"User Name*"];
        
        if([Dict objectForKey:@"msisdn"])
        [_ProfileDetails setObject:[Dict objectForKey:@"msisdn"] forKey:@"Mobile Number*"];
        
         if([Dict objectForKey:@"emailId"])
        [_ProfileDetails setObject:[Dict objectForKey:@"emailId"] forKey:@"Email*"];
        
        
        [self updateProfileImage :[Dict objectForKey:@"profilePicturePath"]];
        
        
        //added as we are getting epmty space along with category IDs
        NSString *str = [[Dict objectForKey:@"categories"] stringByReplacingOccurrencesOfString:@" " withString:@""];
        if(str.length > 0)
        [_ProfileDetails setObject:str forKey:@"categories"];
        
        [self getPackDetails];
        
    }
    else if (appDelegate.APIFlag ==  UPLOAD_IMAGE)
    {
        NSLog(@"UPLOAD_IMAGE SUCCESS");
    }
    else
    {
        //TO DO decode response
        //for testing - REMOVE
        Response = [Response objectForKey:@"responseMap"];
        NSMutableDictionary *Dict = [[Response objectForKey:@"packStatusDetails"]mutableCopy];
        
        if([[Dict objectForKey:@"languageId"] isEqualToString:@"1"])
        {
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"English",nil,appDelegate.LocalBundel,nil) forKey:@"NotificationLanguage"];
        }
        else if([[Dict objectForKey:@"languageId"] isEqualToString:@"2"])
        {
             [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"Arabic",nil,appDelegate.LocalBundel,nil) forKey:@"NotificationLanguage"];
        }
        else
        {
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"NA",nil,appDelegate.LocalBundel,nil) forKey:@"NotificationLanguage"];
        }
        
        NSMutableDictionary *Dict1 = [appDelegate.AppSettings objectForKey:@"PACKNAME"];
        NSString *Temp = [Dict1 objectForKey:[Dict objectForKey:@"packName"]];
        
        if(Temp.length > 0)
        {
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"Active",nil,appDelegate.LocalBundel,nil) forKey:@"Status*"];
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(Temp,nil,appDelegate.LocalBundel,nil) forKey:@"Plan*"];
            
             //updating Pack Name to NSUserDefaults
            if([Dict objectForKey:@"packName"])
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PACK_NAME"];
                [[NSUserDefaults standardUserDefaults] setObject:[Dict objectForKey:@"packName"] forKey:@"PACK_NAME"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                //added on 13th august - shahal
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PACK_NAME"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"PACK_NAME"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
             //end
            
        }
        else
        {
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"Inactive",nil,appDelegate.LocalBundel,nil) forKey:@"Status*"];
            [_ProfileDetails setObject:NSLocalizedStringFromTableInBundle(@"NA",nil,appDelegate.LocalBundel,nil) forKey:@"Plan*"];
        }
        
        //end
        
        [_ActivityIndicator stopAnimating];
        [_overlayview removeFromSuperview];
        [_TableView reloadData];
       
        
    }
    
}



-(void)loadScrollViewContent
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    _SubButtonList = [NSMutableDictionary new];
    
    NSLog(@"appDelegate.CategoryList = %@",appDelegate.CategoryList);
    int x = 0;
    for (int i = 0; i <appDelegate.CategoryList.count ; i++)
    {
        
        //UIButton *MainButton = [[UIButton alloc] initWithFrame:CGRectMake(x,3,60,60)];
        UIImageView *MainButton = [[UIImageView alloc] initWithFrame:CGRectMake(x,3,60,60)];
        
        UIButton *SubButton = [[UIButton alloc] initWithFrame:CGRectMake(x,3,60,30)];
        CustomLabel_7 *buttonlabel = [[CustomLabel_7 alloc] initWithFrame:CGRectMake(x,30,60,20)];
    
        SubButton.userInteractionEnabled = FALSE;
        
        NSMutableDictionary *Dict = [appDelegate.CategoryList objectAtIndex:i];
        
        NSString * urlStr = [[Dict objectForKey:@"imageURL"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [MainButton sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"Default.png"]];
        
        [SubButton setImage:[UIImage imageNamed:@"preference_uncheck"] forState:UIControlStateNormal];
        
        
       // SubButton.hidden=YES;
        //MainButton.tag = [[Dict objectForKey:@"id"] intValue];
        //_MainButton.layer.cornerRadius = 5;
        //_MainButton.clipsToBounds = YES;
        
        buttonlabel.text=NSLocalizedStringFromTableInBundle([Dict objectForKey:@"name"],nil,appDelegate.LocalBundel,nil);
        buttonlabel.textAlignment=NSTextAlignmentCenter;
        buttonlabel.textColor = [UIColor whiteColor];
    
        [_SubButtonList setObject:SubButton forKey:[Dict objectForKey:@"id"]];
        
        
        [_ScrollView addSubview:MainButton];
        [_ScrollView addSubview:SubButton];
        [_ScrollView addSubview:buttonlabel];
        x += MainButton.frame.size.width+5;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = MainButton.bounds;
        gradient.colors = @[(id)[UIColorFromRGB(CLR_BLACK) CGColor], (id)[UIColorFromRGB(CLR_BLACK) CGColor]];
        gradient.startPoint = CGPointMake(1, 1);
        gradient.endPoint = CGPointMake(1, 1);
        gradient.opacity = 0.6;
        
        [MainButton.layer insertSublayer:gradient atIndex:0];
        [MainButton bringSubviewToFront:SubButton];
        [MainButton bringSubviewToFront:buttonlabel];
        
         
    }
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(selectedPreference)];
    [_ScrollView setUserInteractionEnabled:true];
    [_ScrollView addGestureRecognizer:tap];

    if(_ProfileDetails.count != 0)
    {
    NSArray *temp = [[_ProfileDetails objectForKey:@"categories"] componentsSeparatedByString:@","];

    for(int i=0;i<temp.count;i++)
    {

        UIButton *SubTemp = [_SubButtonList objectForKey:[temp objectAtIndex:i]];
        if(SubTemp)
        {
            [SubTemp setImage:[UIImage imageNamed:@"preference_check"] forState:UIControlStateNormal];
        }
    
    }
    }
    _ScrollView.contentSize = CGSizeMake(x, _ScrollView.frame.size.height);
    
    
}


-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response :(NSString *)URL
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag == REGENERATETOKEN)
    {
        [self getProfileDetails];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag ==  UPLOAD_IMAGE)
    {
        return;
    }
    NSLog(@"onFailureResponseRecived >>> Response = %@",Response);
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.AlertMessage = Message;
    ViewController.AlertImageName = @"mytunelibrarypopupinfo"; ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    ViewController.ParentView = self;
    _IsChangeNumberInvoked = false;
    ViewController.HideCloseButton = true;
    [self presentViewController:ViewController animated:YES completion:nil];
}

-(void)onFailedToConnectToServer
{
    NSLog(@"onFailedToConnectToServer >>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag ==  UPLOAD_IMAGE)
    {
        return;
    }
    [_ActivityIndicator stopAnimating];
    [_overlayview removeFromSuperview];
    //[SIXDGlobalViewController showCommonFailureResponse:self :nil];
    NSString *Message = NSLocalizedStringFromTableInBundle(@"oops. something went wrong",nil,appDelegate.LocalBundel,nil);
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
    SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
    ViewController.AlertMessage = Message;
    ViewController.AlertImageName = @"mytunelibrarypopupinfo";
    ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
    ViewController.ParentView = self;
     _IsChangeNumberInvoked = false;
    ViewController.HideCloseButton = true;
    [self presentViewController:ViewController animated:YES completion:nil];
    
}

- (void)onInternetBroken
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag ==  UPLOAD_IMAGE)
    {
        return;
    }
    
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}

-(void)onSelectedChangeProfileImage
{
    
    [SIXDStylist setGAScreenTracker:@"Edit_Profile_ProfilePic"];
    
    NSLog(@"onSelectedChangeProfileImage>>>");
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle: UIAlertControllerStyleActionSheet];
    UIAlertAction* Cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    
    UIAlertAction* TakeNewPhoto = [UIAlertAction actionWithTitle:@"Take a new photo" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
                                   {
                                       
                                       if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
                                       {
                                           UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                                           controller.sourceType = UIImagePickerControllerSourceTypeCamera;
                                           controller.allowsEditing = NO;
                                           //controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
                                           controller.delegate = self;
                                           [self.navigationController presentViewController: controller animated: YES completion: nil];
                                       }
                                       
                                       
                                   }];
    UIAlertAction* ChooseExisting = [UIAlertAction actionWithTitle:@"Choose from existing" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                     {
                                         
                                         
                                         if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
                                         {
                                             UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                                             controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                             controller.allowsEditing = YES;
                                             controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
                                             
                                             controller.delegate = self;
                                             [self.navigationController presentViewController: controller animated: YES completion: nil];
                                         }
                                         
                                     }];
    
    
    [alert addAction:TakeNewPhoto];
    [alert addAction:ChooseExisting];
    [alert addAction:Cancel];
    
    alert.popoverPresentationController.sourceView = [self view];
    [self presentViewController:alert animated:YES completion:nil];

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"Selected Image");
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    
    UIImage *image;
    if([info valueForKey: UIImagePickerControllerEditedImage])
    {
        image = (UIImage*) [info valueForKey: UIImagePickerControllerEditedImage];
        NSLog(@"setting profile image1");
        _ProfilePicture.image = image;
    }
    else if([info valueForKey: UIImagePickerControllerOriginalImage])
    {
        image = (UIImage*) [info valueForKey: UIImagePickerControllerOriginalImage];
        NSLog(@"setting profile image2");
        //added on 30032017
        image = [self centerCropImage:image];
        //end
        
        _ProfilePicture.image = image ;
        
    }
    else
    {
        NSLog(@"profile image not set");
    }
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ProfilePic"];
    [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(image, 1) forKey:@"ProfilePic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self updateProfileImage:nil];
    [self uploadImage];
    
    //TO DO save image and send it
    /*
    NSString *str=[_SourceViewController.ProfileDetails objectForKey:@"firstName"];
   
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:str];
    [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(image, 1) forKey:str];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _SourceViewController.DPImage.image = _UserProfileImage.image;
    
    [self editProfile:image];
    */
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

-(UIImage *)centerCropImage:(UIImage *)image
{
    // Use smallest side length as crop square length
    CGFloat squareLength = MIN(image.size.width, image.size.height);
    // Center the crop area
    CGRect clippedRect = CGRectMake((image.size.width - squareLength) / 2, (image.size.height - squareLength) / 2, squareLength, squareLength);
    // Crop logic
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
    //UIImage * croppedImage = [UIImage imageWithCGImage:imageRef];
    UIImage * croppedImage = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    
    
    image = croppedImage;
    
    //resizing image to reduce image file size
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 200.0;
    float maxWidth = 300.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.1;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
    
    //end
    
    
}

-(void)onSelectedPopUpConfirmButton
{
    if(_IsChangeNumberInvoked)
    {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    NSArray *VCArray = appDelegate.MainTabController.viewControllers ;
    for (UINavigationController *Temp in VCArray)
    {
        [Temp popToRootViewControllerAnimated:false];
    }
    UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDLoginVC"];
    [appDelegate.window.rootViewController.view removeFromSuperview];
    appDelegate.window.rootViewController = ViewController;
    [appDelegate.window makeKeyAndVisible];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:false];
    }
    
}

-(void)updateProfileImage :(NSString*)URL
{
    UIImage *aImage;
    
    if(URL)
    {
        NSString *ImageURL = [URL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]]];
    }
    
    if(!aImage)
    {
        NSData *imagedata = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProfilePic"];
        _ProfilePicture.image = [UIImage imageWithData:imagedata];
        
        if(_ProfilePicture.image == nil)
        {
            _ProfilePicture.image  = [UIImage imageNamed:@"Default.png"];
        }
    }
    else
    {
        _ProfilePicture.image = aImage;
    }
    
    [_CoverView willRemoveSubview:[_CoverView viewWithTag:2]];
    _CoverView.image = _ProfilePicture.image;
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.tag = 2;
    blurEffectView.frame =  _CoverView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_CoverView insertSubview:blurEffectView atIndex:0];

    /*
    UIView *blacklayer = [[UIView alloc] initWithFrame:_CoverView.frame];
    blacklayer.backgroundColor = [UIColor blackColor];
    blacklayer.alpha = 0.85;
    [_CoverView insertSubview:blacklayer atIndex:1];
     */
}

-(void)uploadImage
{
    
        NSLog(@"uploadImage >>>");
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

        NSData *data = [[NSUserDefaults standardUserDefaults]   objectForKey:@"ProfilePic"];
        SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
        
        ReqHandler.HeaderDictionary = [NSMutableDictionary new]; // Required for sending data in header
        
        ReqHandler.HttpsReqType = POST;
    
        NSString *Rno = [SIXDGlobalViewController generateRandomNumber];
        
        //from here
        NSMutableData *postData = [NSMutableData data];
        
        ReqHandler.InvokedAPI = MULTI_PART_EDIT_PROFILE;
        appDelegate.APIFlag=  UPLOAD_IMAGE;
    
        NSString *boundary = @"--9b9ff40e-8821-477f-a24d-d20c5a6587b2";
        [postData appendData:[boundary dataUsingEncoding:NSUTF8StringEncoding]];
        [postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString  *contentDisposition  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"imageName", @"profile.jpg"];
        
        
        [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
        
        contentDisposition  = [NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\nContent-Type: image/jpg;\r\n\r\n"];
        [postData appendData:[contentDisposition dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postData appendData:data];
        
        
        [postData appendData:[[NSString stringWithFormat:@"\r\n%@--\r\n",boundary]dataUsingEncoding:NSUTF8StringEncoding]];
        
        //***********************************************************
    
        ReqHandler.RequestData = postData;
        
        [ReqHandler.HeaderDictionary setObject:Rno forKey:@"clientTxnId"];
        [ReqHandler.HeaderDictionary setObject:@"UpdateProfilePic" forKey:@"identifier"];
    
        [ReqHandler.HeaderDictionary setObject:@"UPDATE_PROFILE_PIC" forKey:@"servType"];
        [ReqHandler.HeaderDictionary setObject:@"previewImage" forKey:@"previewImage"];
        [ReqHandler.HeaderDictionary setObject:appDelegate.Language forKey:@"language"];
    
        
        ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/edit-profile", appDelegate.BaseMiddlewareURL];
        
        [ReqHandler sendHttpPostRequest:self];
    
}

@end
