//
//  SIXDCustomTVCell.m
//  EtisalatCallerTunes
//
//  Created by Six Dee Technologies on 11/21/17.
//  Copyright © 2017 6d Technologies. All rights reserved.
//

#import "SIXDCustomTVCell.h"
#import "SIXDCustomCVCell.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDStylist.h"
#import "SIXDSubCategories.h"
#import "SIXDPlayerVC.h"
#import "SIXDMusicChannelVC.h"
#import "SIXDWishlistVC.h"
#import "SIXDLikeContentSetting.h"
#import "SIXDGlobalViewController.h"

@implementation SIXDCustomTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
#pragma mark -
#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    self.CollectionView.userInteractionEnabled = YES;
    switch (_Source)
    {
        case LISTTONEHORIZONTAL:
        case LISTTONEVERTICAL:
        case CATEGORIES:
        case MUSICBOX:
        case WISHLIST:
            return _FirstRow.count;
            
            break;
            
        case LISTTONEHORIZONTAL_SC:
        case LISTTONEVERTICAL_SC:
            if(_FirstRow.count > 10)
            {
                return 10;
            }
            else
            {
                return _FirstRow.count;
            }
            
            break;
            
            
            
        default:
            return 0;
            break;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"cellForItemAtIndexPath >>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SIXDCustomCVCell *cell = (SIXDCustomCVCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.userInteractionEnabled = YES;
    
    if(indexPath.row < _FirstRow.count)
    {
        if(_Source==CATEGORIES)
        {
            NSMutableDictionary *dict = [_FirstRow objectAtIndex:indexPath.row];
            
            NSURL *ImageURL = [NSURL URLWithString:[dict objectForKey:@"menuImagePath"]];
            [cell.ArtistImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
            
            if([appDelegate.Language isEqualToString:@"Arabic"])
            {
                NSString *string = [SIXDGlobalViewController HTMLConversion:[dict objectForKey:@"categoryName"]];
                cell.ArtistLabel.text = string;
            }
            else
            {
                cell.ArtistLabel.text = [[dict objectForKey:@"categoryName"]uppercaseString];
            }
            
            [cell.GradientImageView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.40]];
            [cell.contentView bringSubviewToFront:cell.GradientImageView];
            [cell.contentView bringSubviewToFront:cell.ArtistLabel];
            
        }
        else if(_Source==LISTTONEVERTICAL || _Source==LISTTONEVERTICAL_SC)
        {
            NSDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
            
            //***set like count *********
            SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
            if([Dict objectForKey:@"likeCount"])
            {
                [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
            }
            else
                cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
            //***set like count enda *********
            
            
            NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
            [cell.ArtistImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
            cell.ArtistLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"artistName"]];
            cell.SongLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
            
            UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            image = [[UIImage imageNamed:@"justforyoubuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.BuyButton setImage:image forState:UIControlStateNormal];
            [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
            
            image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            
            //if([likeSettingsSession isToneLikedFromDownloadedList:[Dict objectForKey:@"toneId"]])
            int likeButtonAction = [likeSettingsSession setToneButtonLikedUnliked:[Dict objectForKey:@"toneId"]];
            switch(likeButtonAction)
            {
                case MAKE_LIKE_BUTTON_SOLID:
                {
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                }
                    break;
                    
                case MAKE_LIKE_BUTTON_OPAQUE:
                {
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                    
                }
                    break;
                    
                default:
                    NSLog(@"likeButtonAction returned 0");
                    break;
            }
            //added for testing - REMOVE
            cell.layer.shouldRasterize = YES;
            cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
            //end
        }
        else if(_Source==LISTTONEHORIZONTAL || _Source == LISTTONEHORIZONTAL_SC)
        {
            NSDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
            
            //***set like count *********
            SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
            if([Dict objectForKey:@"likeCount"])
            {
                [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
            }
            else
                cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
            //***set like count enda *********
            
            
            NSURL *ImageURL = [NSURL URLWithString:[Dict objectForKey:@"previewImageUrl"]];
            [cell.ArtistImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
            cell.ArtistLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"artistName"]];
            cell.SongLabel.text = [SIXDGlobalViewController CapitalisationConversion:[Dict objectForKey:@"toneName"]];
            
            UIImage* image = [[UIImage imageNamed:@"newreleasesmore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            image = [[UIImage imageNamed:@"newreleasebuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.BuyButton setImage:image forState:UIControlStateNormal];
            [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
           
            
            //if([likeSettingsSession isToneLikedFromDownloadedList:[Dict objectForKey:@"toneId"]])
            int likeButtonAction = [likeSettingsSession setToneButtonLikedUnliked:[Dict objectForKey:@"toneId"]];
            switch(likeButtonAction)
            {
                case MAKE_LIKE_BUTTON_SOLID:
                {
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                }
                    break;
                    
                case MAKE_LIKE_BUTTON_OPAQUE:
                {
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                    
                }
                    break;
                    
                default:
                    NSLog(@"likeButtonAction returned 0");
                    break;
            }
        }
        else if(_Source==MUSICBOX || _Source == WISHLIST)
        {
            NSLog(@"_Source==MUSICBOX/WISHLIST");
            NSDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
            
            //***set like count *********
            SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
            if([Dict objectForKey:@"likeCount"])
            {
                [likeSettingsSession setLikeCount:[Dict objectForKey:@"toneId"] :[[Dict objectForKey:@"likeCount"]intValue] :cell.LikeLabel];
            }
            else
                cell.LikeLabel.text =  LIKE_COUNNT_DEFAULT;
            //***set like count enda *********
            
            
            if(_Source==MUSICBOX)
            {

                NSURL *ImageURL = [NSURL URLWithString:[[Dict objectForKey:@"previewImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
                [cell.ArtistImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"playlistheader"]];
            }
            else
            {
                NSURL *ImageURL = [NSURL URLWithString:[[Dict objectForKey:@"previewImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
                [cell.ArtistImage sd_setImageWithURL:ImageURL placeholderImage:[UIImage imageNamed:@"Default.png"]];
            }
            
            cell.ArtistLabel.text = [Dict objectForKey:@"channelName"];
            UIImage* image = [[UIImage imageNamed:@"justforyoumore"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.AccessoryButton setImage:image forState:UIControlStateNormal];
            [cell.AccessoryButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            image = [[UIImage imageNamed:@"justforyoubuy"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.BuyButton setImage:image forState:UIControlStateNormal];
            [cell.BuyButton.imageView setTintColor:UIColorFromRGB(CLR_BUY)];
            
            
            image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.LikeButton setImage:image forState:UIControlStateNormal];
            [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            
            
            //added for WISHLIST - multiple deletion
            if(_Source == WISHLIST)
            {
                 AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocrossunselect"] forState:UIControlStateNormal];
                cell.SelectionButton.tag = indexPath.row+(_CollectionView.tag*3);
                
                for(int i=0;i<appDelegate.SelectionButtonTagArray.count;i++)
                {
                    NSString *tagStr = [appDelegate.SelectionButtonTagArray objectAtIndex:i];
                    long buttonTag = [tagStr longLongValue];
                   // if(buttonTag == indexPath.row)
                    if(buttonTag == indexPath.row+(_CollectionView.tag*3))
                    {
                        [cell.SelectionButton setImage:[UIImage imageNamed:@"radiocross"] forState:UIControlStateNormal];
                    }
                }
            }
            //end
            
           
            
           /*
            int likeButtonAction = [likeSettingsSession setToneButtonLikedUnliked:[Dict objectForKey:@"toneCode"]];
            switch(likeButtonAction)
            {
                case MAKE_LIKE_BUTTON_SOLID:
                {
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                }
                    break;
                    
                case MAKE_LIKE_BUTTON_OPAQUE:
                {
                    
                    UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.LikeButton setImage:image forState:UIControlStateNormal];
                    [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
                    
                }
                    break;
                    
                default:
                    NSLog(@"likeButtonAction returned 0");
                    break;
            }
            */
            
        }
        
        
        if(_Source == LISTTONEHORIZONTAL || _Source == LISTTONEHORIZONTAL_SC)
        {
            UIColor *blackColour = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.85];
            // UIColor *whiteColour = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0];
            // UIColor *blackColour = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.8];
            //UIColor *whiteColour = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.0];
            UIColor *whiteColour = [UIColor clearColor];
            
            if(cell.theViewGradient != nil)
            {
                [cell.theViewGradient removeFromSuperlayer];
            }
            cell.theViewGradient = [CAGradientLayer layer];
            cell.theViewGradient.colors = [NSArray arrayWithObjects: (id)whiteColour.CGColor,(id)blackColour.CGColor, nil];
            // cell.theViewGradient.colors = [NSArray arrayWithObjects: (id)whiteColour.CGColor,(id)whiteColour.CGColor,(id)blackColour.CGColor, nil];
            cell.theViewGradient.frame = cell.GradientImageView.bounds;
            cell.theViewGradient.startPoint = CGPointMake(1,0);
            cell.theViewGradient.endPoint = CGPointMake(1,1);
            [cell.GradientImageView.layer insertSublayer:cell.theViewGradient atIndex:0];
            
            [cell.contentView bringSubviewToFront:cell.GradientImageView];
            [cell.contentView bringSubviewToFront:cell.ArtistLabel];
            [cell.contentView bringSubviewToFront:cell.SongLabel];
            
        }
        
        else if(_Source == MUSICBOX || _Source == WISHLIST || _Source == LISTTONEVERTICAL || _Source==LISTTONEVERTICAL_SC)
        {
         [SIXDStylist setBackgroundGradient:cell.GradientImageView];
         [cell.GradientImageView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.40]];
         
         [cell.contentView bringSubviewToFront:cell.GradientImageView];
         [cell.contentView bringSubviewToFront:cell.ArtistLabel];
         [cell.contentView bringSubviewToFront:cell.SongLabel];
        }
        
        cell.ArtistImage.hidden=NO;
        cell.ArtistLabel.hidden=NO;
        cell.SongLabel.hidden=NO;
        cell.BuyButton.hidden=NO;
        //cell.LikeButton.hidden=NO;
        cell.AccessoryButton.hidden=NO;
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.SeeMore.hidden = YES;
        
        if(_Source == WISHLIST)
        {
            cell.SubView.layer.borderWidth = 0.25;
            cell.SubView.layer.borderColor = [UIColorFromRGB(CLR_CELL_BORDER) CGColor];
        }
        else
        {
            cell.layer.borderWidth = 0.25;
            cell.layer.borderColor = [UIColorFromRGB(CLR_CELL_BORDER) CGColor];
        }

        cell.BuyButton.tag = indexPath.row;
        cell.LikeButton.tag = indexPath.row;
        cell.AccessoryButton.tag = indexPath.row;
    }
    
    [cell.AccessoryButton setSemanticContentAttribute:UISemanticContentAttributePlayback];
    [cell.LikeButton setSemanticContentAttribute:UISemanticContentAttributePlayback];
    
    return cell;
}

-(void)cellTapped
{
    NSLog(@"touched cell");
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(_Source == LISTTONEHORIZONTAL || _Source == LISTTONEVERTICAL || _Source == LISTTONEHORIZONTAL_SC || _Source == LISTTONEVERTICAL_SC )
    {
        NSLog(@"didSelectItemAtIndexPath SIXDListTonesVC >>");
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        SIXDPlayerVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDPlayerVC"];
        
        ViewController.PlayList=_FirstRow;
        ViewController.CurrentToneIndex=indexPath.row;
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
    }
    else if(_Source == MUSICBOX || _Source == WISHLIST)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SIXDMusicChannelVC *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMusicChannelVC"];
        ViewController.PlayListDetails=[_FirstRow objectAtIndex:indexPath.row];
        NSLog(@"PlayListDetails = %@",ViewController.PlayListDetails);
        [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
        
    }
    else
    {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        NSMutableDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
        
        
        if([[Dict objectForKey:@"categoryId"] isEqualToString:[appDelegate.AppSettings objectForKey:@"PLAYLIST_ID"]])
        {
            
            UIViewController *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDMusicBoxList"];
            
            appDelegate.CategoryImagePath = [Dict objectForKey:@"menuImagePath"];
            if([appDelegate.Language isEqualToString:@"Arabic"])
            {
                NSString *string = [SIXDGlobalViewController HTMLConversion:[Dict objectForKey:@"categoryName"]];
                appDelegate.CategoryName =  string;
                NSLog(@"appDelegate.CategoryName = %@",appDelegate.CategoryName);
            }
            else
            {
                appDelegate.CategoryName = [Dict objectForKey:@"categoryName"];
                NSLog(@"appDelegate.CategoryName = %@",appDelegate.CategoryName);
            }
            
            [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
            
        }
        else
        {
            
            SIXDSubCategories *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDSubCategories"];
            
            appDelegate.CategoryImagePath = [Dict objectForKey:@"menuImagePath"];
            
            appDelegate.CategoryName = [Dict objectForKey:@"categoryName"];
            ViewController.CategoryDetails = [[Dict objectForKey:@"subCategoryList"]mutableCopy];
            
            [appDelegate.HomeObj.navigationController pushViewController:ViewController animated:YES];
        }
        
        
    }
    
    
}


#pragma mark UICollectionViewFlowLayoutDelegate
/*
 -(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
 {
 
 return CGSizeMake(collectionView.frame.size.width/3-1,collectionView.frame.size.height );
 
 // return image.size;
 }
 */
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue>>>>");
}


 - (IBAction)onSeletedAccessoryButton:(UIButton *)sender
 {
 
 }
 
- (IBAction)onSelectedLikeButton:(UIButton *)sender
{
    
    NSLog(@"CustomTV::Calling onSelectedLikeButton >>>");
    
    NSMutableDictionary *TuneDetails = [[_FirstRow objectAtIndex:sender.tag ]mutableCopy];
    NSString *tuneId = [TuneDetails objectForKey:@"toneId"];
   
    switch (_Source) {
        case MUSICBOX:
            NSLog(@"MUSICBOX");
            tuneId = [TuneDetails objectForKey:@"toneCode"];
            break;
            
        default:
            break;
    }
    SIXDLikeContentSetting *likeSettingsSession = [SIXDLikeContentSetting sharedSampleSingletonClass];
    
    // NSDictionary *Dict = [_FirstRow objectAtIndex:indexPath.row];
    /*
    NSString *categoryId = [TuneDetails objectForKey:@"categoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    
    if(!categoryId)
        categoryId = DEFAULT_CATEGORY_ID;
     */
    NSString *categoryId;
    categoryId = [TuneDetails objectForKey:@"baseCategoryId"];
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"categoryId"];
    }
    if(!categoryId)
    {
        categoryId = [TuneDetails objectForKey:@"category"];
    }
    if(!categoryId)
    {
        categoryId = DEFAULT_CATEGORY_ID;
    }
    
    NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SIXDCustomCVCell *cell = (SIXDCustomCVCell*)[_CollectionView cellForItemAtIndexPath:cellIndexpath];
    int likeButtonAction = [likeSettingsSession likeTone:tuneId :categoryId :cell.LikeLabel];
    
    //  NSIndexPath *indexPath;
    //indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    //indexPath = [_CollectionView indexpath]
    // indexPath = [_CollectionView indexPathForItemAtPoint:[_CollectionView convertPoint:sender.center fromView:sender.superview]];
    
    // NSLog(@"index path = %ld", (long)indexPath.row);
    //SIXDCustomCVCell *cell = (SIXDCustomCVCell*)[_CollectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    switch(likeButtonAction)
    {
        case MAKE_LIKE_BUTTON_SOLID:
        {
            NSLog(@"HERE 11");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikesolid"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIButton* myButton = (UIButton*)sender;
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            
            NSString *GAEventDetails = [NSString stringWithFormat:@"%@-%@-%@",cell.SongLabel.text,cell.ArtistLabel.text,[TuneDetails objectForKey:@"toneId"]];
            [SIXDStylist setGAEventTracker:@"Likes" :@"Liked" :GAEventDetails];
            
        }
            break;
            
        case MAKE_LIKE_BUTTON_OPAQUE:
        {
            NSLog(@"HERE 22");
            UIImage* image = [[UIImage imageNamed:@"justforyoulikeoutline"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            UIButton* myButton = (UIButton*)sender;
            
            [myButton setImage:image forState:UIControlStateNormal];
            [myButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
            /*
             NSIndexPath *cellIndexpath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
             SIXDCustomTVCell *cell = [_TableView cellForRowAtIndexPath:cellIndexpath];
             
             [cell.LikeButton setImage:image forState:UIControlStateNormal];
             [cell.LikeButton.imageView setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
             */
        }
            break;
            
        default:
            NSLog(@"likeButtonAction returned 0");
            break;
    }
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (_Source)
    {
        case LISTTONEHORIZONTAL:
        case LISTTONEHORIZONTAL_SC:
            return CGSizeMake(146.5,188);
            break;
            
        case LISTTONEVERTICAL:
        case LISTTONEVERTICAL_SC:
            return CGSizeMake(222,136);
            break;
            
        case PLAYLIST:
            return  CGSizeMake(135,169);
            break;
            
        case MUSICBOX:
            return CGSizeMake(103.5,132.5);
            // return CGSizeMake(_CollectionView.frame.size.width/3,_CollectionView.frame.size.width/3);
            break;
            
        case WISHLIST:
            return CGSizeMake(103.5,162.5);
            // return CGSizeMake(_CollectionView.frame.size.width/3,_CollectionView.frame.size.width/3);
            break;
            
        case CATEGORIES:
            return CGSizeMake(_CollectionView.frame.size.width/2-2.5,_CollectionView.frame.size.width/2-5);
            break;
            
        default:
            return CGSizeMake(_CollectionView.frame.size.width/2-2.5,_CollectionView.frame.size.width/2-5);
            break;
    }
    
    
}

@end

