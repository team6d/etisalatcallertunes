//
//  SIXDGroupConfiguration.m
//  EtisalatCallerTunes
//
//  Created by Shahal.ns on 14/02/18.
//  Copyright © 2018 6d Technologies. All rights reserved.
//

#import "SIXDGroupConfiguration.h"
#import "AppDelegate.h"
#import "SIXDHeader.h"
#import "SIXDHttpRequestHandler.h"
#import "SIXDGlobalViewController.h"
#import "SIXDCustomTVCell.h"
#import "SIXDStylist.h"
#import "SIXDCommonPopUp_Text.h"

#define ADDED_MEMBERS   1
#define DELETED_MEMBERS 2

@interface SIXDGroupConfiguration ()

@end

@implementation SIXDGroupConfiguration

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _GroupNameTextField.enablesReturnKeyAutomatically = NO;
    _GroupNameTextField.delegate = self;
    
    NSLog(@"GroupInfoDictionary = %@",_GroupInfoDictionary);
    
    _Number.delegate = self;
    _Name.delegate = self;
    _GroupNameTextField.tag = 1;
    
    _Number.keyboardType = ASCII_NUM_KEYBOARD;

    _Name.enablesReturnKeyAutomatically = NO;
    
    _GroupNameView.layer.borderWidth = 1.0f;
    _GroupNameView.layer.borderColor = [UIColorFromRGB(CLR_ATHENS_GRAY) CGColor];
    
    _GroupMemberList = [NSMutableArray new];
    
    [_GroupNameTextField setFont:[UIFont fontWithName:FONT_TEXTFIELD size:12]];
    
    _TableView.contentInset = UIEdgeInsetsMake(0,0,77,0);
    
    if(_SOURCE==FROM_CREATE_GROUP)
    {
        [SIXDStylist setGAScreenTracker:@"Create_A_CallerGroup"];
        _GroupNameLabel.hidden=YES;
            _TopSpaceToTextField.constant=0;
        [_GroupNameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
            _BannerImage.image = [UIImage imageNamed:@"mycallergroup"];
        _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"CREATE_A_GROUP_HEADING",nil,appDelegate.LocalBundel,nil);
        _GroupMemberList = [NSMutableArray new];
    
    }
    else
    {
        [SIXDStylist setGAScreenTracker:@"Edit_Group_Details"];
        
        _IsEDITED=FALSE;
        _GroupNameLabel.hidden=NO;
        _TopSpaceToTextField.constant=19;
        
        _GroupNameTextField.text = [_GroupInfoDictionary objectForKey:@"groupName"];
        _HeadingLabel.text = NSLocalizedStringFromTableInBundle(@"UPDATE A GROUP",nil,appDelegate.LocalBundel,nil);
        _BannerImage.image = [UIImage imageNamed:@"updategroup"];
        NSString *details = [_GroupInfoDictionary objectForKey:@"msisdnB"];
        NSArray *CountDict = [details componentsSeparatedByString:@","];
        for(int i = 0; i<CountDict.count; i++)
        {
             NSArray *Filler = [[CountDict objectAtIndex:i] componentsSeparatedByString:@"|"];
            NSMutableDictionary *ContactDict = [NSMutableDictionary new];
            [ContactDict setObject:[Filler objectAtIndex:0] forKey:@"msisdn"];
            [ContactDict setObject:[Filler objectAtIndex:1] forKey:@"memberName"];
            [_GroupMemberList addObject:ContactDict];
        }
        
        _StableArray = [_GroupMemberList mutableCopy];
        NSLog(@"**_StableArray = %@",_StableArray);
    }
    //common setup
    
    _GroupNameLabel.text = NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil);
    [_SaveButton setTitle:NSLocalizedStringFromTableInBundle(@"SAVE",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [_AddButton setTitle:NSLocalizedStringFromTableInBundle(@"ADD",nil,appDelegate.LocalBundel,nil) forState:UIControlStateNormal];
    [SIXDStylist setTextViewNormal:_GroupNameTextField : _GroupNameView: _GroupButton];
    
    [self.view bringSubviewToFront:_SaveButton];
    [_TableView reloadData];
    
    [self Initialise];
    
    
     [SIXDStylist addKeyBoardDoneButton:_Number :self];

    
    self.overlayview = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
}

-(void) dismissKeyboard
{
    NSLog(@"dismissKeyboard>>>");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    [_Number resignFirstResponder];
    [_Name resignFirstResponder];
    [_GroupNameTextField resignFirstResponder];
    if(_GroupNameTextField.text.length>0)
    {
        [SIXDStylist setTextViewNormal:_GroupNameTextField :_GroupNameView :_GroupButton];
        
    }
    if(_Number.text.length==0)
    {
        [SIXDStylist setTextViewNormal:_Number :_NumberView :_NumberErrorIcon];
        
    }
    if(_Name.text.length==0)
    {
        [SIXDStylist setTextViewNormal:_Name :_NameView :_NameErrorIcon];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    [textField resignFirstResponder];
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _GroupMemberList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath >>");
    SIXDCustomTVCell *cell = (SIXDCustomTVCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.DetailLabel.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    [cell.SecondButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    [cell.SecondButton setTintColor:UIColorFromRGB(CLR_ETISALAT_GREEN)];
    
    NSMutableDictionary *Filler = [_GroupMemberList objectAtIndex:indexPath.row];
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableAttributedString *titleString;
    NSMutableAttributedString *WhiteDot;
    NSMutableAttributedString *FinalString = [NSMutableAttributedString new];
    
   
    titleString = [[NSMutableAttributedString alloc] initWithString:[Filler objectForKey:@"msisdn"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    WhiteDot = [[NSMutableAttributedString alloc] initWithString:@".   ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor clearColor]}];
    
    [FinalString appendAttributedString:WhiteDot];
    [FinalString appendAttributedString:titleString];
    [FinalString appendAttributedString:WhiteDot];
    cell.TitleLabel.attributedText = FinalString;
    
    
    FinalString = [NSMutableAttributedString new];
     titleString = [[NSMutableAttributedString alloc] initWithString:[Filler objectForKey:@"memberName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    /*
    if([appDelegate.Language isEqualToString:@"English"])
    {
        titleString = [[NSMutableAttributedString alloc] initWithString:[Filler objectForKey:@"memberName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    }
    else
    {
         titleString = [[NSMutableAttributedString alloc] initWithString:[Filler objectForKey:@"memberName"] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"GESSTextLight-Light" size:11],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    }
    */
    
    WhiteDot = [[NSMutableAttributedString alloc] initWithString:@".   ." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size:11],NSForegroundColorAttributeName:[UIColor clearColor]}];
    [FinalString appendAttributedString:WhiteDot];
    [FinalString appendAttributedString:titleString];
    [FinalString appendAttributedString:WhiteDot];
    cell.SubTitleLabel.attributedText = FinalString;
    

    //cell.TitleLabel.text =  [NSString stringWithFormat:@"\u00a0\u00a0\u00a0%@\u00a0\u00a0\u00a0",[Filler objectForKey:@"msisdn"]];
    //cell.SubTitleLabel.text = [NSString stringWithFormat:@"\u00a0\u00a0\u00a0%@\u00a0\u00a0\u00a0",[Filler objectForKey:@"memberName"]];
    
    
    cell.TitleLabel.layer.borderWidth = 1.0f;
    cell.TitleLabel.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    cell.SubTitleLabel.layer.borderWidth = 1.0f;
    cell.SubTitleLabel.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    
    cell.SecondButton.tag = indexPath.row;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [SIXDGlobalViewController showAndEnableLeftNavigationItem:self];
    [SIXDGlobalViewController showAndEnableRightNavigationItem:self];
    [SIXDGlobalViewController setNavigationBarTransparent:self];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [SIXDGlobalViewController ChangeScrollPosition : scrollView :_ImageHeight];
}

-(void)API_CreateGroup
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=CREATE_GROUP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];

    ReqHandler.HttpsReqType = POST;
    
    
    //HERE ENCODING FOR API
    /*
    for(int i = 0;i<_GroupMemberList.count ;i++)
    {
        NSMutableDictionary *ContactDict = [[_GroupMemberList objectAtIndex:i] mutableCopy];
        NSString *MemberName = [ContactDict objectForKey:@"memberName"];
        MemberName = [MemberName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [ContactDict removeObjectForKey:@"memberName"];
        [ContactDict setObject:MemberName forKey:@"memberName"];
        [_GroupMemberList replaceObjectAtIndex:i withObject:ContactDict];
    }
    */
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_GroupMemberList
                                                       options:0
                                                         error:&error];
    NSString *MemberDetails;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        MemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    NSLog(@"MemberDetails = %@",MemberDetails);
    
    NSString *ToneDetails = @"";
    NSMutableArray *ToneID = [NSMutableArray new];
    
    [ToneID addObject:[appDelegate.AppSettings objectForKey:@"DEFAULT_TONE_ID"]];
    
    jsonData = [NSJSONSerialization dataWithJSONObject:ToneID
                                               options:0
                                                 error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        ToneDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    //NSString *GroupName = [_GroupNameTextField.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

    NSLog(@"ToneDetails = %@",ToneDetails);
    
    //ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&groupName=%@&groupDesc=%@&memberDetails=%@&toneDetails=%@&priority=0&packName=CRBT_WEEKLY",appDelegate.MobileNumber,_GroupNameTextField.text,_GroupNameTextField.text,MemberDetails,ToneDetails];
     ReqHandler.Request = [NSString stringWithFormat:@"aPartyMsisdn=%@&groupName=%@&groupDesc=%@&memberDetails=%@&toneDetails=%@&priority=0",appDelegate.MobileNumber,_GroupNameTextField.text,_GroupNameTextField.text,MemberDetails,ToneDetails];
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/create-group",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
}

-(void)API_EditGroup
{
    [self.tabBarController.view addSubview:self.overlayview];
    [_ActivityIndicator startAnimating];
    [self.view bringSubviewToFront:_ActivityIndicator];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.APIFlag=EDIT_GROUP;
    SIXDHttpRequestHandler *ReqHandler = [SIXDHttpRequestHandler new];
    
    ReqHandler.HttpsReqType = POST;
    
    /*
    for(int i = 0;i<_GroupMemberList.count ;i++)
    {
        NSMutableDictionary *ContactDict = [[_GroupMemberList objectAtIndex:i] mutableCopy];
        NSString *MemberName = [ContactDict objectForKey:@"memberName"];
        MemberName = [MemberName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [ContactDict removeObjectForKey:@"memberName"];
        [ContactDict setObject:MemberName forKey:@"memberName"];
        [_GroupMemberList replaceObjectAtIndex:i withObject:ContactDict];
    }
     */
    
    //ADD LIST
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_GroupMemberList
                                                       options:0
                                                         error:&error];
    NSString *AddMemberDetails;
    if (! jsonData) {
        NSLog(@"Add Got an error: %@", error);
    } else {
        AddMemberDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    // ADD LIST ENDS
    
    
    // DELETE LIST
 
    NSString *DeleteMemberDetails =@"";
    for(int i=0;i<_StableArray.count;i++)
    {
        NSDictionary *Dict1 = [_StableArray objectAtIndex:i];
        DeleteMemberDetails =  [DeleteMemberDetails stringByAppendingString: [NSString stringWithFormat:@"%@,",[Dict1 objectForKey:@"msisdn"]]];
    }
    
    if ([DeleteMemberDetails length] > 0) {
        DeleteMemberDetails = [DeleteMemberDetails substringToIndex:[DeleteMemberDetails length] - 1];
    }
    
    
    // DELETE LIST ENDS
    
    
    NSLog(@"AddMemberDetails = %@",AddMemberDetails);
    NSLog(@"DeleteMemberDetails = %@",DeleteMemberDetails);
    
    NSString *ToneDetails = @"";
    NSMutableArray *ToneID = [NSMutableArray new];
    
    //NSMutableDictionary *Dict = [ NSMutableDictionary new];
    //[Dict setObject:DEFAULT_GROUP_TONE_ID forKey:@"toneId"];
    [ToneID addObject:[appDelegate.AppSettings objectForKey:@"DEFAULT_TONE_ID"]];
    
    jsonData = [NSJSONSerialization dataWithJSONObject:ToneID
                                               options:0
                                                 error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        ToneDetails = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSLog(@"ToneDetails = %@",ToneDetails);
    

   // ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&groupName=%@&groupId=%@&groupDesc=%@&memberDetails=%@&deleteMember=%@&toneDetails=%@&priority=0&packName=CRBT_WEEKLY&language=%@",appDelegate.MobileNumber,_GroupNameTextField.text,[_GroupInfoDictionary objectForKey:@"groupId"],_GroupNameTextField.text,AddMemberDetails,DeleteMemberDetails,ToneDetails,appDelegate.Language];
     ReqHandler.Request = [NSString stringWithFormat:@"msisdn=%@&groupName=%@&groupId=%@&groupDesc=%@&memberDetails=%@&deleteMember=%@&toneDetails=%@&priority=0&language=%@",appDelegate.MobileNumber,_GroupNameTextField.text,[_GroupInfoDictionary objectForKey:@"groupId"],_GroupNameTextField.text,AddMemberDetails,DeleteMemberDetails,ToneDetails,appDelegate.Language];
    
    ReqHandler.FinalURL = [NSString stringWithFormat:@"%@/crbt/add-group-members",appDelegate.BaseMiddlewareURL];
    [ReqHandler sendHttpPostRequest:self];
    

}



-(void)onSuccessResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.APIFlag==CREATE_GROUP)
    {
        NSLog(@"API_CreateGroup onSuccessResponseRecived = %@",Response);
        [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        NSLog(@"API_EditGroup onSuccessResponseRecived = %@",Response);
        [self.navigationController popViewControllerAnimated:NO];
        [_GroupViewVC.navigationController popViewControllerAnimated:NO];
    }
}

-(void)onFailureResponseRecived:(NSMutableDictionary *)Response
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    NSString *Message = [NSString stringWithFormat:@" %@",[Response objectForKey:@"message"]];
    [SIXDGlobalViewController showCommonFailureResponse:self :Message];
}

-(void)onFailedToConnectToServer
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    [SIXDGlobalViewController showCommonFailureResponse:self :nil];
}
- (void)onInternetBroken
{
    [_ActivityIndicator stopAnimating];
    [self.overlayview removeFromSuperview];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *AlertMessage = NSLocalizedStringFromTableInBundle(@"INTERNET_CONNECTION_BROKEN",nil,appDelegate.LocalBundel,nil);
    [SIXDGlobalViewController setAlert:AlertMessage :self];
}


- (IBAction)onClickedDeleteButton:(UIButton *)sender
{
    NSLog(@"onClickedDeleteButton >>");
    _IsEDITED=TRUE;
    [_GroupMemberList removeObjectAtIndex:sender.tag];
    [_TableView reloadData];
}

- (IBAction)onClickedAddButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BOOL Status = true;
    
    if((_Number.text.length < 5)|| (_Name.text.length <= 0))
    {
        
        if(_Number.text.length < 5)
        {
            _NumberView.layer.borderWidth = 1.0f;
            _NumberView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
            _NumberErrorIcon.hidden =false;
            
            [_Number setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Member Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
            
            if(_Number.text.length <= 0)
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            }
            else
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"*please enter a valid number",nil,appDelegate.LocalBundel,nil);
            }
           
        }
        if (_Name.text.length <= 0)
        {
            _NameView.layer.borderWidth = 1.0f;
            _NameView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
            _NameErrorIcon.hidden =false;
            [_Name setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Member Name",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ALERTRED),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
            _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
            
        }
        
        _AlertLabel.hidden=NO;
        Status = false;
        
    }
    else
    {
        for(int i=0;i<_GroupMemberList.count;i++)
        {
            NSDictionary *temp = [_GroupMemberList objectAtIndex:i];
            NSString *MSISDN = [NSString stringWithFormat:@"%@",[temp objectForKey:@"msisdn"]];
            Status = [SIXDGlobalViewController isMSISDNSame :_Number.text :  MSISDN];
            
            if(!Status)
            {
                _NumberView.layer.borderWidth = 1.0f;
                _NumberView.layer.borderColor = [UIColorFromRGB(CLR_ALERTRED) CGColor];
                _NumberErrorIcon.hidden =false;
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"Already added member",nil,appDelegate.LocalBundel,nil);
                 _AlertLabel.hidden=NO;
                break;
            }
            
        }
        
    }
    
    if(Status)
    {
       _IsEDITED=TRUE;
        _AlertLabel.hidden=YES;
        NSString * str = [_Number.text stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [_Number.text length])];
        NSMutableDictionary *ContactDict = [NSMutableDictionary new];
        [ContactDict setObject:str forKey:@"msisdn"];
        [ContactDict setObject:_Name.text forKey:@"memberName"];
        
        [_GroupMemberList addObject:ContactDict];
        _Number.text=@"";
        _Name.text=@"";
        
        [SIXDStylist setTextViewNormal:_GroupNameTextField :_GroupNameView :_GroupButton];
        [SIXDStylist setTextViewNormal:_Number :_NumberView :_NumberErrorIcon];
        [SIXDStylist setTextViewNormal:_Name :_NameView :_NameErrorIcon];
        
        [_TableView reloadData];
    }
    
}

- (IBAction)onClickedContactButton:(id)sender {
    NSLog(@"onSelectedContactButton");
    CNContactPickerViewController *contactPickerViewController = [CNContactPickerViewController new];
    
    [self Initialise];
    
    contactPickerViewController.predicateForEnablingContact = [NSPredicate predicateWithFormat:@"%K.@count >= 1", CNContactPhoneNumbersKey];
    
    contactPickerViewController.delegate = self;
    
    [self presentViewController:contactPickerViewController animated:NO completion:nil];
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact >>>>");
    _PhoneNumbers = contact.phoneNumbers;
    _Name.text = [NSString stringWithFormat:@"%@ %@" ,contact.givenName,contact.familyName];
    [picker dismissViewControllerAnimated:true completion:nil];
    [self ShowMultipleNumbers];
}

-(void)ShowMultipleNumbers
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(_PhoneNumbers.count > 1)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"SELECT NUMBER",nil,appDelegate.LocalBundel,nil)
                                                                       message:nil
                                                                preferredStyle: UIAlertControllerStyleActionSheet];
        UIAlertAction* Cancel = [UIAlertAction actionWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,appDelegate.LocalBundel,nil) style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        //adding dynanic contents
        
        for(CNLabeledValue *label in _PhoneNumbers)
        {
            NSString *phone = [label.value stringValue];
            UIAlertAction* PhoneNumber = [UIAlertAction actionWithTitle:phone style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * action)
                                          {
                                              _Number.text = phone;
                                              
                                          }];
            [alert addAction:PhoneNumber];
            
        }
        
        [alert addAction:Cancel];
        
        alert.popoverPresentationController.sourceView = self.presentingViewController.view;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else
    {
        CNLabeledValue *label = [_PhoneNumbers objectAtIndex:0];
        _Number.text = [label.value stringValue];
    }
    
}

- (IBAction)onClickedSaveButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(_GroupNameTextField.text.length==0)
    {
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_GroupNameTextField :_GroupNameView :_GroupButton];
    }
    else if(_GroupMemberList.count==0)
    {
        /*
        _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"please fill in required field",nil,appDelegate.LocalBundel,nil);
        _AlertLabel.hidden=NO;
        [SIXDStylist setTextViewAlert:_Number :_NumberView :_NumberErrorIcon];
        [SIXDStylist setTextViewAlert:_Name :_NameView :_NameErrorIcon];
         */
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSLog(@"onClickedDeletGroupButton >>");
        UIStoryboard *StoryBoard = [UIStoryboard storyboardWithName:@"MyAccount" bundle:nil];
        SIXDCommonPopUp_Text *ViewController = [StoryBoard instantiateViewControllerWithIdentifier:@"SIXDCommonPopUp_Text"];
        ViewController.ParentView = self;
        ViewController.AlertMessage = NSLocalizedStringFromTableInBundle(@"Please add atleast one member to your group",nil,appDelegate.LocalBundel,nil);
        ViewController.AlertImageName = @"mytunelibrarypopupinfo";
        ViewController.ConfirmButtonText=NSLocalizedStringFromTableInBundle(@"OK",nil,appDelegate.LocalBundel,nil);
        
        [self presentViewController:ViewController animated:YES completion:nil];
    }
    else
    {
        _AlertLabel.hidden=YES;
        if(_SOURCE==FROM_CREATE_GROUP)
        {
            [SIXDStylist setTextViewNormal:_GroupNameTextField :_GroupNameView :_GroupButton];
            [SIXDStylist setTextViewNormal:_Number :_NumberView :_NumberErrorIcon];
            [SIXDStylist setTextViewNormal:_Name :_NameView :_NameErrorIcon];
            
            NSLog(@"API_CreateGroup call >>>");
            [self API_CreateGroup];
        }
        else
        {
            if(_IsEDITED)
            {
            [SIXDStylist setTextViewNormal:_GroupNameTextField :_GroupNameView :_GroupButton];
            [SIXDStylist setTextViewNormal:_Number :_NumberView :_NumberErrorIcon];
            [SIXDStylist setTextViewNormal:_Name :_NameView :_NameErrorIcon];
            
            [self EditGroupLogic];
            NSLog(@"API_EditGroup call >>>");
            [self API_EditGroup];
            }
            else
            {
                _AlertLabel.text=NSLocalizedStringFromTableInBundle(@"*you have not made any changes",nil,appDelegate.LocalBundel,nil);
                _AlertLabel.hidden=NO;
            }
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self Initialise];
    _IsEDITED =TRUE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 150;
    }
    
    if(textField.tag == _GroupNameTextField.tag)
    {
        [_GroupNameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"" attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _TopSpaceToTextField.constant=19;
        _GroupNameLabel.hidden=NO;
    }

    
    
}

-(void)Initialise
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _AlertLabel.hidden = true;
    _NumberView.layer.borderWidth = 1.0f;
    _NumberView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _NameView.layer.borderWidth = 1.0f;
    _NameView.layer.borderColor = [UIColorFromRGB(CLR_TEXTFIELDBORDER) CGColor];
    _NameErrorIcon.hidden =YES;
    _NumberErrorIcon.hidden =YES;
    [_Name setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Member Name",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
    [_Number setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Member Number",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_ETISALAT_GREEN),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:12]}]];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   if(textField.text.length == 30 && string.length == 1)
    {
        return NO;
    }
    else
        return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.DeviceSize == SMALL)
    {
        _ImageHeight.constant = 250;
    }
    
    if(_GroupNameTextField.text.length==0)
    {
        //to be corrected.
        [_GroupNameTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedStringFromTableInBundle(@"Group Name*",nil,appDelegate.LocalBundel,nil) attributes: @{NSForegroundColorAttributeName:UIColorFromRGB(CLR_PLACEHOLDER),NSFontAttributeName:[UIFont fontWithName:appDelegate.PlaceHolderFont size:14]}]];
        _GroupNameLabel.hidden=YES;
        _TopSpaceToTextField.constant=0;
    }
    
}

-(void)EditGroupLogic
{
    NSLog(@"EditGroupLogic >>");
    int i,j;
    
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    
    for(i=0;i<_StableArray.count;i++)
    {
        BOOL foundOrNot=FALSE;
        NSDictionary *Dict1 = [_StableArray objectAtIndex:i];
        NSString *str1 = [Dict1 objectForKey:@"msisdn"];
        
        for(j=0;j<_GroupMemberList.count;j++)
        {
            NSDictionary *Dict2 = [_GroupMemberList objectAtIndex:j];
            NSString *str2 = [Dict2 objectForKey:@"msisdn"];
            if([str1 isEqualToString:str2])
            {
                foundOrNot = TRUE;
                break;
            }
        }
        if(foundOrNot == TRUE)
        {
            [_GroupMemberList removeObjectAtIndex:j];
            [indexSet addIndex:i];
        }
    }
    
    [_StableArray removeObjectsAtIndexes:indexSet];
   // NSLog(@"\n\n DeletedMember = %@",_StableArray);             //deleted members
   // NSLog(@"\n\n GroupMemberList = %@",_GroupMemberList);       //newly added
    
}


@end
