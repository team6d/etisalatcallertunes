//
//  NotificationSetupTest.swift
//  EtisalatCallerTunesTests
//
//  Created by SKY on 03/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//


@testable import EtisalatCallerTunes
import XCTest

class NotificationSetupTest: XCTestCase {

    func testNotificationSetup_deletDevice(){
        let notificationSetup = NotificationSetup()
        notificationSetup.deleteDevice(msisdn: "8123812512")
    }
    
    func testNotificationSetup_getDeviceDetail() {
        let notificationSetup = NotificationSetup()
        notificationSetup.getDeviceDetail(msisdn: "8123812512")
        //deleteDevice(msisdn: "8123812512")
    }
    
    func testNotificationSetup_AddDeviceDetail() {
        NotificationSetup.addDeviceDetail(token: "deviceToken", id: "deviceId")
    }
    
    func testNotificationSetupTest_SuccessResponce() {
        //NotificationSetup.addDeviceDetail(token: <#T##String#>, id: <#T##String#>)
    }

}



//import XCTest
//
//class NotificationSetupTest: XCTestCase {
//
//    override func setUpWithError() throws {
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDownWithError() throws {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
//
//    func testExample() throws {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
//
//}
