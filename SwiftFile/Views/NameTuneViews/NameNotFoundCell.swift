//
//  NameNotFoundCell.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 26/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit

class NameNotFoundCell: UITableViewCell {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var massageLabel: UILabel!
    @IBOutlet weak var massageSubLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var textFieldContainerView: UIView!
    var confirmPopup = ConfirmViewHelper()
    private var confirmHandle:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldContainerView.layer.borderColor = UIColor.gray.cgColor
        textFieldContainerView.layer.borderWidth = 1.0
        localizeString()
    }
    
    func localizeString() {
        saveButton.setTitle("SAVE".localized, for: .normal)
         massageLabel.text = "Sorry! No Results Found".localized
        massageSubLabel.text = "But we will record it for you if you want!".localized
    }
    
    func configureCell(searchedName: String, complition: (()->Void)? = nil) {
        nameTextField.text = searchedName
        confirmHandle = complition
    }
    
    @IBAction func saveNameButtonAction() {
        confirmPopup.show(name: nameTextField.text!,complition: confirmViewAction(action:))
    }
    
    func confirmViewAction(action: ConfirmViewAction) {
        switch action {
        case .confirm:
          //  confirmApiCall()
            confirmHandle?()
        default:
            print("default ")
        }
    }
/*
    func confirmApiCall() {
        let randomNo = SIXDGlobalViewController.generateRandomNumber()
        let toneName = nameTextField.text
        let packName = UserDefaults.standard.value(forKey: "PACK_NAME") as? String
        
        let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
        OverLayViewHelper.shared.show()
        let url = "\(appDelegate?.baseMiddlewareURL ?? "")/crbt/name-tune"
        let request = "clientTxnId=\(randomNo ?? "")&language=\(appDelegate?.language ?? "")&msisdn=\(appDelegate?.mobileNumber ?? "")&toneName=\(toneName ?? "")&packName=\(packName ?? "")"
        ServiceCall.apiCallWithRequestUrl(myStruct: SearchTune.self, isUrlEncodeNeed: true, request: request, url: url, type: .post) { (result, responce) in
            OverLayViewHelper.shared.removeOverLay()
            print("result is \(result) and responce is \(responce)")
        }
    }
 */
}
