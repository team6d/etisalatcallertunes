//
//  NameTuneTableViewCell.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 25/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit

class NameTuneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var playButtonView: PlayerButtonView!
    var playerHandler: ((PlayerButtonViewAction) -> Void)?
    var buyButtonHalde: ((SongInfo)->Void)?
    var info: SongInfo?
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        previewImageView.addGredeint(opacity: 0.7)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        DispatchQueue.main.async {
            self.playButtonView.activityIndicator.stopAnimating()
        }
    }
    
    func configure(info: [SongInfo], index: IndexPath) {
        DispatchQueue.main.async {
            self.playButtonView.activityIndicator.stopAnimating()
        }
        self.info = info[index.row]
        nameLabel.text = info[index.row].toneName ?? "No name found"
        playButtonView.configurePlayerView(songList: info, autoPlay: false, isRoundPath: false, selectedIndex: index.row, complition: playerHandler)
        if let url = URL(string: info[index.row].previewImageUrl ?? info[index.row].previewImage ?? "") {
            self.previewImageView.sd_setImage(with: url, completed: nil)
        }
        if info[index.row].isPlaying {
            self.playButtonView.playingButtonState()
        } else {
            self.playButtonView.initialButtonState()
        }
    }
    @IBAction func buyTuneButtonAction() {
        if let info = info {
            buyButtonHalde?(info)
        }
    }
}
