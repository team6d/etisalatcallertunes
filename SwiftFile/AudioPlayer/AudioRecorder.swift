//
//  AudioRecorder.swift
//  Indosat
//
//  Created by SKY on 25/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import Foundation
import AVFoundation

enum RecorderActions {
    case Time(String)
    case Url(URL)
    case Data(Data)
    case timeInSec(CGFloat)
}
class Recorder: NSObject, AVAudioRecorderDelegate {
    static let shared = Recorder()
    
    var audioRecorder: AVAudioRecorder?
    var meterTimer:Timer?
    var isAudioRecordingGranted: Bool?
    var recorderHandler:((RecorderActions)->Void)?
    private override init() {
        super.init()
        self.checkPermisionAudioRecording()
    }
    fileprivate func checkPermisionAudioRecording() {
        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSession.RecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                }
            }
            break
        default:
            break
        }
    }
    func startRecording() {
        recording()
    }
    func stopRecording() {
        finishAudioRecording(success: true)
    }
    fileprivate func recording() {
        
        if isAudioRecordingGranted != nil {
            
            //Create the session.
            let session = AVAudioSession.sharedInstance()
            
            do {
                try session.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)
                //Configure the session for recording and playback.
                try session.setActive(true)
                //Set up a high-quality recording session.
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                ]
                //Create audio file name URL
                let audioFilename = getDocumentsDirectory().appendingPathComponent("SKYAudioRecording.m4a")
                //Create the audio recording, and assign ourselves as the delegate
                
                audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
                audioRecorder?.delegate = self
                audioRecorder?.isMeteringEnabled = true
                audioRecorder?.record()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            }
            catch let error {
                print("Error for start audio recording: \(error.localizedDescription)")
            }
        }
    }
    fileprivate func finishAudioRecording(success: Bool) {
        
        audioRecorder?.stop()
        audioRecorder = nil
        meterTimer?.invalidate()
        meterTimer = nil
        if success {
            print("Recording finished successfully.")
        } else {
            print("Recording failed :(")
        }
    }
    @objc func updateAudioMeter(timer: Timer) {
        if let isRecording = audioRecorder?.isRecording {
            
            if isRecording {
                if let time = audioRecorder?.currentTime {
                    let hr = Int((time / 60) / 60)
                    let min = Int(time / 60)
                    let sec = Int(audioRecorder?.currentTime.truncatingRemainder(dividingBy: 60) ?? 0.0)
                    let totalTimeString = String(format: "%02d:%02d:%02d", hr, min, sec)
                    recorderHandler?(.Time(totalTimeString))
                    audioRecorder?.updateMeters()
                    recorderHandler?(.timeInSec(CGFloat(audioRecorder?.currentTime ?? 0.0)))
                }
            }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        var audioTrack: Data?
        do {
            audioTrack = try Data(contentsOf: recorder.url)
            let urlStr = "\(recorder.url)"
            let finalStr = urlStr.suffix(urlStr.count-7)
            let finalUrl = URL(string: String(finalStr))
            print("==========final url is \(finalStr)")
            if let url = finalUrl {
                self.recorderHandler?(.Url(url))
            }
            if let data = audioTrack {
                self.recorderHandler?(.Data(data))
            }
        } catch {
            print("error found in conversion")
        }
        if !flag {
            finishAudioRecording(success: false)
        }
    }
}

