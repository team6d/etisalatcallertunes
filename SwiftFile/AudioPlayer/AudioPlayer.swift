//
//  Player.swift
//  Indosat
//
//  Created by SKY on 15/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import Foundation
import AVFoundation
enum PlayerActions {
    case Control(AVAudioPlayer)
    case Timer
    case FinishPlaying
    case stop
    case playing(Float)
    case error(String)
}
class Player: NSObject {
    
    static let shared = Player()
    private var task: URLSessionDataTask?
    private var audioPlayer: AVAudioPlayer?
    private var playerHandler:((PlayerActions)->Void)?
    private override init() {
        
    }
    
    func playDefaultSong(fileName: String, fileExtension: String = ".mp3") {
        let bundleUrl = Bundle.main.url(forResource: fileName, withExtension: fileExtension)
        if let bundleUrl = bundleUrl {
            playUrl(url: bundleUrl)
        }
    }
    
    // TODO: - Fix it
    func streamTuneFromURL(url:URL, autoPlay: Bool = false,complition:((PlayerActions)->Void)? = nil){
        if !Reachability.isConnectedToNetwork(){ PopupViewHelper.shared.show(type: .warning, message: "Please check internet connection."); return }
        task?.cancel()
        print("Tune url is  \(url)")
        task = URLSession.shared.dataTask(with: url) {[weak self] (data, responce, error) in
            print("data is \(data)")
            //   print("responce is  is \(responce)")
            print("error  is \(error)")
            if let data = data {
                self?.playFromData(data: data, autoPlay: autoPlay, complition: complition)
            } else {
                complition?(.stop)
            }
        }
        task?.resume()
    }
    
    fileprivate func playUrl(url: URL, complition:((PlayerActions)->Void)? = nil) {
        stopPlayer()
        if !Reachability.isConnectedToNetwork(){ PopupViewHelper.shared.show(type: .warning, message: "Please check internet connection."); return }
        self.playerHandler = complition
        DispatchQueue.main.async {
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: url)
                self.audioPlayer?.delegate = self
                self.audioPlayer?.prepareToPlay()
                if let audioPlayer = self.audioPlayer {
                    self.playerHandler?(.Control(audioPlayer))
                }
            } catch let error {
                print("error found")
                self.playerHandler?(.error(error.localizedDescription))
            }
        }
    }
    func playFromData(data: Data, autoPlay: Bool, complition:((PlayerActions)->Void)? = nil) {
        stopPlayer()
        if !Reachability.isConnectedToNetwork(){ PopupViewHelper.shared.show(type: .warning, message: "Please check internet connection."); return }
        self.playerHandler = complition
        DispatchQueue.main.async {
            do {
                self.audioPlayer = try AVAudioPlayer(data: data)
                self.audioPlayer?.delegate = self
                self.audioPlayer?.prepareToPlay()
                if autoPlay{
                    self.audioPlayer?.play()
                }
                self.playerHandler?(.playing(Float(self.audioPlayer?.duration ?? 0)))
            } catch let error {
                print("error found")
                self.playerHandler?(.error(error.localizedDescription))
            }
        }
    }
    func playAndPause(_ isPlay: Bool) {
        
        DispatchQueue.main.async {
            if isPlay {
                self.audioPlayer?.play()
                self.audioPlayer?.delegate = self
            } else {
                self.audioPlayer?.pause()
            }
        }
        
        DispatchQueue.main.async {
            if let audioPlayer = self.audioPlayer {
                self.playerHandler?(.Control(audioPlayer))
            }
        }
    }
    
    func soundControl(value: Float) {
        self.audioPlayer?.volume = value
    }
    
    func soundMute() {
        audioPlayer?.volume = 0.0
    }
    
    func playOnSliderValue(value: Float) {
        audioPlayer?.currentTime = TimeInterval(value)
    }
    
    func stopPlayer() {
        task?.cancel()
        DispatchQueue.main.async {
            print("Audio player Stopped")
            self.audioPlayer?.stop()
        }
        playerHandler?(.stop)
    }
    
    @objc func runTimedCode() {
        print("Audio player timer running")
        self.playerHandler?(.Timer)
    }
}
extension Player: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        playerHandler?(.FinishPlaying)
        stopPlayer()
        print("========audioPlayerDidFinishPlaying")
        
    }
    
}

