//
//  TuneDetail.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 04/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

struct TuneDetail {
    var contentName: String = "contentName"
    var albumName: String = "contentName"
    var toneName: String = "contentName"
    var artist: String = "contentName"
    var artistName: String = "contentName"
    var contentId: String = "contentName"
    var toneId: String = "contentName"
    var toneCode: String = "contentName"
    var categoryId: String = "contentName"
    var path: String = "contentName"
    var tonePath: String = "contentName"
    var toneUrl: String = "contentName"
    var previewImageUrl: String = "contentName"
    var imagePath: String = "contentName"
    var id: String = "contentName"
    var likeCount: String = "contentName"
    var channelName: String = "contentName"
    var description: String = "contentName"
}
