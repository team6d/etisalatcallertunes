//
//  GiftViewModel.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 09/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class GiftViewModel: NSObject {
    var tuneCode: String = ""
    var tuneUrl: String = ""
    var tuneTitle: String = ""
    var likeCount: String = ""
    var tuneSubTitle: String = ""
    var imageUrl: String = ""
    override init() { }
    @objc func addData(url: String, title: String, subTitle: String, likeCount: String, code: String, imageUrl: String) {
        
        tuneTitle = title
        self.tuneUrl = url
        self.tuneCode = code
        tuneSubTitle = subTitle
        self.likeCount = likeCount
        self.imageUrl = imageUrl
    }
}
