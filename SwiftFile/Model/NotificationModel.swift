//
//  NotificationModel.swift
//  Notification
//
//  Created by SKY on 26/08/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation

struct NotificationModel: Decodable {
    let respCode: Int
    let message: String
}

struct NotificationGetDetailModel: Decodable {
    let respCode: Int
    let message: String
    let deviceDetails:[DeviceDetail]
}

struct DeviceDetail: Decodable {
    let registrationToken: String
    let deviceId: String
    let osType: String
    let language: String
}
