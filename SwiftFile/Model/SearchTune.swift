//
//  SearchTune.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 25/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

class CountList: Decodable {
    var artistDetailList : [ArtistInfo]
}
class ArtistInfo: Decodable {
    var matchedParam: String
}

class SongInfo: Decodable {
    
    // home Playlist
    
    var channelName: String?
    var childToneCodes: String?
    var description: String?
    var entityId: String?
    var parentToneCode: String?
    var previewContent: String?
    var toneCode: String?
    var type: String?
    var validity: String?
    
    var album: String?
    var artist: String?
    var bannerOrder: String?
    var contentId: String?
    var contentName: String?
    var id: String?
    var path: String?
    var previewImage: String?
    
    var tuneType: String?
    var albumName: String?
    var artistName: String?
    var baseCategoryId: String?
    var categoryId: Int?
    var expiryDate: String?
    var likeCount: String?
    // wishList
    var msisdn: String?
    var wishListType: String?
    
    // copy tune
    var createdDate: String?
    var isCopy: String?
    var isGift: String?
    var status: String?
    
    var previewImageUrl: String?
    var price: Int?
    var toneId: String?
    var toneName: String?
    var toneUrl: String?
    
    lazy var empty: Bool = {
        return false
    }()
    //var isLiked: Bool?
    lazy var isLiked: Bool = {
        return false
    }()
    
    lazy var isPlaying: Bool = {
        return false
    }()
    lazy var isPlaylist: Bool = {
        return false
    }()
    var likeCountInt: Int {
        return Int(likeCount ?? "0") ?? 0
    }
}


    class SongList: Decodable {
        var songList: [SongInfo]?
        var countList: CountList?
        var searchList: [SongInfo]?
    }
    class SearchTune: Decodable {
        var responseMap: SongList
        
    }
