//
//  OverLayView.swift
//  Indosat
//
//  Created by SKY on 07/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import UIKit

class OverLayView: UIView {
    @IBOutlet weak var activityIndiactor: UIActivityIndicatorView!
    
    static func loadXib() -> OverLayView {
        return UINib(nibName: "OverLayView", bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as! OverLayView
    }
}

class OverLayViewHelper {
    static let shared = OverLayViewHelper()
    
    let window = UIApplication.shared.keyWindow
    var overLay: OverLayView?
    
    private init() { }
    
//    func show() {
//        self.removeOverLay()
//
//        DispatchQueue.main.async { [weak self] in
//            self?.overLay = OverLayView.loadXib()
//            self?.overLay?.frame = UIScreen.main.bounds
//            guard let overView = self?.overLay else { return}
//            self?.window?.addSubview(overView)
//        }
//    }
    func show(_ sourceView: UIView? = nil) {
        self.removeOverLay()
        DispatchQueue.main.async { [weak self] in
            self?.overLay = OverLayView.loadXib()
            self?.overLay?.frame = sourceView == nil ? UIScreen.main.bounds : sourceView!.bounds
            guard let overView = self?.overLay else { return}
            if let sourceView =  sourceView {
            sourceView.addSubview(overView)
            } else {
                self?.window?.addSubview(overView)
            }
        }
    }
    
    func removeOverLay() {
        DispatchQueue.main.async {[weak self] in
        self?.overLay?.removeFromSuperview()
        self?.overLay = nil
        }
    }
}
