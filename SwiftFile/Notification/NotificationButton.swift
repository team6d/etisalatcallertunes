//
//  NotificationButton.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 16/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation
class NotificationButton: NSObject {
    @objc class func addButtonOnNotification() {
        let accept = UNNotificationAction(identifier: "Accept", title: "Accept", options: [.foreground])
        let reject = UNNotificationAction(identifier: "Reject", title: "Reject", options: [.foreground])
        if #available(iOS 11.0, *) {
            let category = UNNotificationCategory(identifier: "category", actions: [accept, reject], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "plac", options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
        } else {
            // Fallback on earlier versions
        }
    }
}
