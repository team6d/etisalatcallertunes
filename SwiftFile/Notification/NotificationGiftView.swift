//
//  NotificationGiftView.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 08/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

enum GiftActionHandler {
    case cancel
    case confirm
    case like
    case play
}
class NotificationGiftView: UIView {
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var tuneTitleLabel: UILabel!
    @IBOutlet weak var tuneSubTitleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    
    
    var actionHandler: ((GiftActionHandler)->Void)?
    
    class func loadXib() -> NotificationGiftView {
        return UINib(nibName: "NotificationGiftView", bundle: .main).instantiate(withOwner: self, options: nil).first as! NotificationGiftView
    }
    
    @objc func populateGiftView(info: GiftViewModel) {
        likeCountLabel.text = info.likeCount
        tuneTitleLabel.text = info.tuneTitle
        tuneSubTitleLabel.text = info.tuneSubTitle
        let url = URL(string: info.tuneUrl)
        Player.shared.streamTuneFromURL(url: url!, autoPlay: false, complition: playerHandler(action:))
        setImage(info: info)
    }
    
    func setImage(info: GiftViewModel) {
        guard let titleUrl = URL(string: info.imageUrl) else { return }
        titleImageView.sd_setImage(with: titleUrl, completed: nil)
        backGroundImageView.sd_setImage(with: titleUrl, completed: nil)
    }
    
    @IBAction func likeButtonAction(_ sender: Any) {
        actionHandler?(.like)
    }
    
    @IBAction func playButtonAction(_ sender: Any) {
        actionHandler?(.play)
        playButton.isSelected = !playButton.isSelected
        Player.shared.playAndPause(playButton.isSelected)
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        actionHandler?(.cancel)
        Player.shared.stopPlayer()
    }
    
    @IBAction func confirmButtonAction(_ sender: Any) {
        actionHandler?(.confirm)
        Player.shared.stopPlayer()
    }
}

extension NotificationGiftView {
    func playerHandler(action: PlayerActions) {
        switch action {
        case .playing(let duration):
            print("duration is \(duration)")
        default:
            print("default")
        }
    }
    
}
