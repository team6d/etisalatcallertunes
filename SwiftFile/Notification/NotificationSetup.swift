//
//  NotificationSetup.swift
//  Notification
//
//  Created by SKY on 26/08/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation
@objc class NotificationSetup: NSObject {
    
@objc class func addDeviceDetail(token fcmToken: String,id deviceId: String) {
    
        if StoreManager.shared.fcmToken == fcmToken{ print("FCM token is same, so can not make api call");return }
        let mobileNumber = "123456789"
        let osType = "ios"
        let language = "en"
        
        let postDict = ["msisdn": mobileNumber,
                        "registrationToken": fcmToken,
                        "deviceId": deviceId,
                        "osType": osType,
                        "language": language
        ]
        print("Post string \(postDict)")
    
    // MARK:- after service call store device token in store menager
        NetworkManager.serviceCall(struct: NotificationModel.self, url: Constants.addDeviceUrl, requestBody: postDict, method: .post) { (result, error) in
            if let error = error {
                print("error is \(error)")
                return
            }
            if let result = result {
                print("addDeviceDetail = \(result)")
                if result.respCode == 0 {
                    StoreManager.shared.fcmToken = fcmToken
                }
            }
        }
    }
    
    func getDeviceDetail(msisdn: String) {
        let postDict = ["msisdn": msisdn]
        NetworkManager.serviceCall(struct: NotificationGetDetailModel.self, url: Constants.getDeviceDetail, requestBody: postDict, method: .get) { (result, error) in
            if let error = error {
                print("error is \(error)")
                return
            }
            guard let result = result else {return}
            print("getDeviceDetail = \(result)")
        }
    }
    
    func deleteDevice(msisdn: String) {
        let postDict = [msisdn : msisdn]
        NetworkManager.serviceCall(struct: NotificationModel.self, url: Constants.deleteDevice, requestBody: postDict, method: .get) { (result, error) in
            if let error = error {
                print("error is \(error)")
                return
            }
            guard let result = result else {return}
            print("deleteDevice = \(result)")
        }
    }
}
