//
//  NotificationViewHelper.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 08/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class NotificationViewHelper:  ViewController {
    var giftView: NotificationGiftView?
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    @objc func showGiftView(info: GiftViewModel) {
        OverLayViewHelper.shared.removeOverLay()
        giftView = NotificationGiftView.loadXib()
        giftView?.frame = UIScreen.main.bounds
        giftView?.populateGiftView(info: info)
        giftView?.actionHandler = actionHandler(action:)
        guard let giftView = giftView else { return }
        appDelegate?.window.rootViewController?.view.addSubview(giftView)
        
    }
    
    @objc class func addNotificationObserver() {
        let debitOverdraftNotifCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([debitOverdraftNotifCategory])
    }
    
    
    @objc class func getDeviceToken(deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%.2hhx", $0) }.joined()
        print("getDeviceToken >>>>>>>> = \(token)")
    }
    
    
    func actionHandler(action: GiftActionHandler) {
        switch action {
        case .confirm:
            print("confirm clicked")
            acceptApiCall()
        case .cancel:
            print("cancel clicked")
            rejectButtonTapped()
        case .like:
            print("like")
        case .play:
            print("play")
        default:
            print("actionHandler")
        }
    }
    
    @objc func dismissGiftView() {
        self.giftView?.removeFromSuperview()
        self.giftView = nil
    }
    
    func rejectButtonTapped() {
         self.dismissGiftView()
        print("rejectButtonTapped ")
    }
    
    func acceptApiCall() {
        let randomNo = SIXDGlobalViewController.generateRandomNumber()
        
        let packName = UserDefaults.standard.value(forKey: "PACK_NAME") as? String
        
        OverLayViewHelper.shared.show()
        let toneId = "195661"
        let categoryId = "991"
        let confirnCode = "Y \(toneId)"
        let requestUrl = "clientTxnId=\(randomNo ?? "")&aPartyMsisdn=\(appDelegate?.mobileNumber ?? "")&confirmationCode=\(confirnCode)&packName=\(packName ?? "")&categoryId=\(categoryId)&language=\(appDelegate?.language ?? "")"
        let url = "\(appDelegate?.baseMiddlewareURL ?? "")/crbt/accept-gift"
        let request = SIXDHttpRequestHandler()
        request.httpsReqType  = POST
        request.request = requestUrl
        request.finalURL = url
        request.sendHttpPostRequest(self)
        }
    override func onSuccessResponseRecived(_ Response: NSMutableDictionary!) {
            print("on success ")
            OverLayViewHelper.shared.removeOverLay()
        }
    override func onFailureResponseRecived(_ Response: NSMutableDictionary!) {
        print("on onFailureResponseRecived \(String(describing: Response))")
        OverLayViewHelper.shared.removeOverLay()
    }
    override func onFailedToConnectToServer() {
            print("on onFailedToConnectToServer ")
            OverLayViewHelper.shared.removeOverLay()
        }
    override func onInternetBroken() {
            print("on fail ")
            OverLayViewHelper.shared.removeOverLay()
        }

    }
    
    




