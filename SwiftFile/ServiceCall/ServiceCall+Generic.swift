//
//  ServiceCall+Generic.swift
//  Indosat
//
//  Created by SKY on 27/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import Foundation
extension ServiceCall {
   
    static func searchGeneric<T: Decodable>(myStruct: T.Type, finalUrl: String, searchKey: String, complition:((SearchTune?, ServiceResponce)-> Void)? ) {
        
        guard let url = URL(string: finalUrl) else {complition?(nil, .error("Not a valid URL")); return }
        var request = commonURLRequest(url: url, httpMethod: .get)
        var searchStr = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        searchStr = searchStr?.replacingOccurrences(of: "%E2%80%8F", with: "")
        request.setValue(searchStr, forHTTPHeaderField: "searchKey")
        requestOnServer(request: request) { data, error in
            DispatchQueue.main.async {
                do {
                    if let data = data {
                        let result = try JSONDecoder().decode(SearchTune.self, from: data)
                        print("List is \(result)")
                        complition?(result, error)
                    } else {
                        complition?(nil, error)
                    }
                } catch let error {
                    complition?(nil, .error(error.localizedDescription))
                }
                
            }
        }
    }
    
    static func apiCallWithUrl<T: Decodable>(myStruct: T.Type,finalUrl: String, httpType: HttpType, complition: ((T?, ServiceResponce)->Void)?) {
        guard let url = URL(string: finalUrl) else {complition?(nil, .error("Not a valid URL")); return }
        var request = commonURLRequest(url: url, httpMethod: httpType)
        
        requestOnServer(request: request) { data, error in
            do {
                if let data = data {
                    let list = try JSONDecoder().decode(T.self, from: data)
                    complition?(list, error)
                } else {
                    complition?(nil, error)
                }
            } catch let errorParse {
                let errorStr = errorParse.localizedDescription
                complition?(nil, .error(errorStr))
                print("error is  \(error)")
            }
            
        }
    }
    
    static func apiCallWithRequestUrl<T: Decodable>(myStruct: T.Type, isUrlEncodeNeed: Bool, request: String, url: String, type: HttpType,complition: ((T?, ServiceResponce)-> Void)?) {
        print("mid url is \(request)")
        var req: String? = ""
        var postdata: Data? = nil
        if isUrlEncodeNeed {
            req = request.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
            postdata = req?.data(using: .utf8, allowLossyConversion: true)
        } else {
            postdata = req?.data(using: .utf8, allowLossyConversion: true)
        }
        
        guard let url = URL(string: url) else {complition?(nil, .error("Not a valid URL")); return }
        var urlRequest = commonURLRequest(url: url, httpMethod: type)
        urlRequest.httpBody = postdata
        requestOnServer(request: urlRequest) { (data, responce) in
            DispatchQueue.main.async {
                do {
                    if let data = data {
                        let result = try JSONDecoder().decode(T.self, from: data)
                        print("List is \(result)")
                        complition?(result , responce)
                    } else {
                        complition?(nil, responce)
                    }
                } catch let error{
                    complition?(nil, .error(error.localizedDescription))
                }
                
            }
        }
    }
 
}
