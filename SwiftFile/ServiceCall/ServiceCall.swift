//
//  ServiceCall.swift
//  Indosat
//
//  Created by SKY on 17/10/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import Foundation
//import SIXDHeader.h
//import SIXDHeader

enum HttpType: String {
    case post = "POST"
    case get = "GET"
}

enum ServiceResponce {
    case success
    case error(String)
    case regenerated
    case expired
}

enum ErrorHandling {
    case notRegitered
    case success
    case error(String)
}


class ServiceCall {
    
    static var isAccessTokenExpired: Bool = true

   static let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    
    static func serviceCallRequest(isUrlEncodeNeed: Bool, midUrl: String, finalUrl: String, type: HttpType,complition: ((ErrorHandling)->Void)?) {
        httpRequest(isUrlEncodeNeed: isUrlEncodeNeed, midUrl: midUrl, finalUrl: finalUrl, type: .post) { (finished) in
            complition?(finished)
        }
    }
    
    static func httpRequest(isUrlEncodeNeed: Bool, midUrl: String, finalUrl: String, type: HttpType,complition: ((ErrorHandling)->Void)?) {
        var req: String? = ""
        var postdata: Data? = nil
        if isUrlEncodeNeed {
            req = midUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
            postdata = req?.data(using: .utf8, allowLossyConversion: true)
        } else {
             postdata = req?.data(using: .utf8, allowLossyConversion: true)
        }
        
        let postLength = "\(midUrl.count)"
        guard let url = URL(string: finalUrl) else {return}
        
        var request = commonURLRequest(url: url, httpMethod: type)
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = postdata
        requestOnServer1(request: request) { (errorHandle) in
            complition?(errorHandle)
        }
    }
    
    static func getSecurityToken(midUrl: String, finalUrl: String,type: HttpType, complition:((ErrorHandling)->Void)? ) {
        guard let url = URL(string: finalUrl) else {return}

        let request = commonURLRequest(url: url, httpMethod: type)
        requestOnServer1(request: request) { (finished) in
            complition?(finished)
        }
    }
    
    static func commonURLRequest(url: URL,httpMethod: HttpType) -> URLRequest {
        var request = URLRequest(url: url)
        let http = httpMethod == .get ? "GET" : "POST"
        request.httpMethod = http
        request.setValue(APP_ID, forHTTPHeaderField: "appId")
        request.setValue(APP_VERSION, forHTTPHeaderField: "appVersion")
        request.setValue(VERSION_CODE, forHTTPHeaderField: "versionCode")
        request.setValue(appDelegate?.accessToken, forHTTPHeaderField: "accessToken")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(appDelegate?.deviceId, forHTTPHeaderField: "deviceId")
        request.setValue(OS, forHTTPHeaderField: "os")
        request.setValue(appDelegate?.language, forHTTPHeaderField: "language")
        request.timeoutInterval = 30;
        return request
    }
    
    static  func requestOnServer1(request: URLRequest, complition:((ErrorHandling)->Void)?) {
        print("request is \(request)")
       
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            print("Error  is \(String(describing: error))")
            print("Responce  is \(String(describing: responce))")
            guard let data = data else {return}
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            storeTokenAndSecCouner(json: json)
            print("JOSN IS \(String(describing: json))")
            //((json as? NSDictionary)?.object(forKey: "responseMap") as? NSDictionary)?.value(forKey: "respCode")
             let message = (json as? NSDictionary)?.object(forKey: "message") as? String
            let responceCode = ((json as? NSDictionary)?.object(forKey: "responseMap") as? NSDictionary)?.value(forKey: "respCode") as? String
            if let responceCode = responceCode {
                if responceCode == "SC0000" {
                    complition?(.success)
                } else if responceCode == "100" {
                    complition?(.notRegitered)
                    return
                } else if responceCode == "101" {
                    complition?(.error(message ?? "No message"))
                    return
                } else {
                    complition?(.error(message ?? "No message"))
                    return
                }
            }
            
            
            let code = (json as? NSDictionary)?.value(forKey: "statusCode") as? String
            
                    if let code = code {
                        if code == "SC0000" {
                            complition?(.success)
                        } else if code == "FL0000" {
                           complition?(.error(message ?? "No message"))
                        }
                
            } else {
                complition?(.error(message ?? "No message"))
                return
            }
            
            }.resume()
    }
    
    static func storeTokenAndSecCouner(json: Any?) {
        let responceMap = ((json as? NSDictionary)?.value(forKey: "responseMap") as? NSDictionary)
        
        let security = responceMap?.value(forKey: "securityCounter") as? String
        
        if let security = security {
            appDelegate?.securityToken=security;
        }
        
        if let accessToken = responceMap?.value(forKey: "accessToken") as? String {
            UserDefaults.standard.set(accessToken, forKey: "AccessToken")
        }
        if let refreshToken = responceMap?.value(forKey: "refreshToken") as? String {
            UserDefaults.standard.set(refreshToken, forKey: "RefreshToken")
        }
        //refreshToken
    }
    
    
    static private func regenerateToken(complition:((Bool)->Void)?) {
        ServiceCall.reGenerate { (error) in
            switch error {
            case .error(let error):
                print("error on generating token , error is \(error)")
            case .success:
                print("token regenerated")
            case .notRegitered:
                print("Not registered")
            }
        }
    }
    
    static private func reGenerate(complition: ((ErrorHandling)->Void)?) {
        let finalUrl = "https://www.callertunes.etisalat.ae/Middleware/api/adapter/v1/crbt/regen-token"
        //Constants.regenerateToken
        let post = "refreshToken=\(UserDefaults.standard.value(forKey: "RefreshToken") ?? "")"
        
        let postdata = post.data(using: .utf8, allowLossyConversion: true)
        let postLength = "\(post.count)"
        guard let url = URL(string: finalUrl) else {return}
        var request = commonURLRequest(url: url, httpMethod: .post)
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = postdata
        requestOnServer1(request: request) { (errorHandle) in
            complition?(errorHandle)
        }
        
    }
   static private func getTimeDiffrence(start: Date, end: Date) -> String  {
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([Calendar.Component.second], from: start, to: end)
        let seconds = dateComponents.second
        return "\((seconds ?? 0))"
    }
    
    static func requestOnServer(request: URLRequest, complition:((Data?, ServiceResponce)->Void)?) {
        
        let date1 = Date()
        
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            DispatchQueue.main.async {
                print("Error  is \(String(describing: error))")
                guard let data = data else {return}
                print("(responce as? HTTPURLResponse)?.statusCode \(String(describing: (responce as? HTTPURLResponse)?.statusCode))")
                if let respCode = (responce as? HTTPURLResponse)?.statusCode {
                    
                    if respCode == 498 || respCode == 401 {
                        //isAccessTokenExpired = true
                        if isAccessTokenExpired {
                            isAccessTokenExpired = false
                            print(" regenerating token")
                            regenerateToken(complition: { (done) in
                                if done {
                                    isAccessTokenExpired = false
                                    complition?(nil, .regenerated)
                                } else {
                                    isAccessTokenExpired = true
                                    complition?(nil, .expired)
                                }
                                return
                            })
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
                            complition?(nil, .expired)
                        })
                        
                        return
                    } else if respCode == 200 {
                        print("Succes ")
                        complition?(data, .success)
//                    } else if respCode == 401 {
//                        complition?(nil, .expired)
                    }else if respCode == 500 {
                        let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        let message = (json as? NSDictionary)?.object(forKey: "message") as? String
                        complition?(data, .error(message ?? "no message"))
                    }else {
                        complition?(nil, .error(returnError(responceCode: respCode)))
                    }
                }
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                let date2 = Date()
                print("Time taken to get responce  \(getTimeDiffrence(start: date1, end: date2))")
                print("Json data for URL is  = \n \(responce?.url)")
                
                    print("JOSN data is  = \n \(json)")
                
                
//                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
//
//                print("JOSN data is  = \n \(json)")
//
//                let code = (json as? NSDictionary)?.value(forKey: "statusCode") as? String
//                if let code = code {
//                    if code == "SC0000" {
//                        complition?(data, .success)
//                    }else  if code == "FL0010" {
//                        complition?(nil, .error(code))
//                    }
//                } else if code == "FL0010" {
//                    complition?(nil, .error(code!))
//                }
            }
            }.resume()
    }
    // MARK: - error code
    static private func returnError(responceCode: Int) -> String {
        switch responceCode {
        case 401:
            return "Unauthenticated; maybe client didn’t sent the access token"
        case 412:
            return "Request parameters are not valid"
        case 400:
            return "Bad request; request parsing failed"
        case 402:
            return "User is disabled"
            //        case 403:
        //            return "forbidden; request cannot be processed without additional permissions"
        case 404:
            return "No such service"
        case 413:
            return "File upload exceeds accepted length"
        case 410:
            return "Record not exist"
        case 409:
            return "security counter token value is unacceptable"
        case 403:
            return "Security counter not provided."
        case 460:
            return "Could not complete due to various reasons"
        case 450:
            return "Invalid fingerprint. The app has failed the fingerprint verification with server. Further requests will not be entertained."
        case 430:
            return "Client version upgrade required"
        default:
            return "Some thing went wrong"
        }
    }
    
}
