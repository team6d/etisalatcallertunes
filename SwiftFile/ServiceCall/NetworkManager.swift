//
//  NetworkManager.swift
//  Notification
//
//  Created by SKY on 26/08/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static func serviceCall<T: Decodable>(struct: T.Type, url: String, requestBody: [String:String], method: RequestMethod = .get, complition: ((T?, String?)->Void)?) {
        guard let url1 = URL(string: url) else { print("Not valid url"); return }
        var request = NetworkManager.setRequestHeader(url: url1)//URLRequest(url: url1)
        request.httpMethod = method.rawValue
        let jsonData = try? JSONEncoder().encode(requestBody)
        print("Json data is \(jsonData)")
        request.httpBody = jsonData//requestBody.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, responce, error) in
            if let error = error {
                print("Error took place \(error)")
                complition?(nil, error.localizedDescription)
                return
            }
            if let data = data,let result =  try? JSONDecoder().decode(T.self, from: data) {
                print("Data is nil")
                complition?(result, nil)
            } else {
                complition?(nil, "not decodable")
            }
        }
        task.resume()
    }
    
   private static func setRequestHeader(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.setValue(Constants.appId, forHTTPHeaderField: "App-Id")
        request.setValue(Constants.tranId, forHTTPHeaderField: "Tran-Id")
        request.setValue(Constants.appVer, forHTTPHeaderField: "App-Version")
        request.setValue("\(Constants.osVersion)", forHTTPHeaderField: "OS-Version")
        request.setValue(Constants.acceptVersion, forHTTPHeaderField: "Accepts-Version")
    
        return request
    }
}
