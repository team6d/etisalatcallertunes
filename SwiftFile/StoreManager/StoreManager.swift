//
//  StoreManager.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 28/08/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation
let appGroupname = "group.com.sixdee.uaecallertunesNoti"

@objc class StoreManager: NSObject {
    private override init() {}
    @objc static let shared = StoreManager()
    private let userDefault = UserDefaults.standard
    
    @objc var nameTunePriceEn: String {
        get {
            return UserDefaults.standard.value(forKey: "NAMETUNE_PRICE_ENGLISH") as? String ?? ""
        }
        
        set(value) {
            UserDefaults.standard.set(value, forKey: "NAMETUNE_PRICE_ENGLISH")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc var isCategorySubscribed: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isCategorySubscribed") as? Bool ?? false
        }
        
        set(value) {
            UserDefaults.standard.set(value, forKey: "isCategorySubscribed")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc var isTopicSubscribed: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isTopicSubscribed") as? Bool ?? false
        }
        
        set(value) {
            UserDefaults.standard.set(value, forKey: "isTopicSubscribed")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc var nameTunePriceAr: String {
        get {
            return UserDefaults.standard.value(forKey: "NAMETUNE_PRICE_ARABIC") as? String ?? ""
        }
        
        set(value) {
            UserDefaults.standard.set(value, forKey: "NAMETUNE_PRICE_ARABIC")
            UserDefaults.standard.synchronize()
        }
    }
    
   @objc var fcmToken: String {
        get{
            return userDefault.value(forKey: "fcmToken") as? String ?? ""
        }
        set(newValue){
            userDefault.set(newValue, forKey: "fcmToken")
            userDefault.synchronize()
        }
    }
    
    @objc var countNum: Int {
           get{
               guard let defaults = UserDefaults(suiteName: appGroupname) else { return 0}
               return defaults.value(forKey: "countNum") as? Int ?? 0
           }
           set(value){
                guard let defaults = UserDefaults(suiteName: appGroupname) else { return}
                defaults.set(value, forKey: "countNum")
                defaults.synchronize()
           }
       }
}
