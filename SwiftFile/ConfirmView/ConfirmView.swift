//
//  ConfirmView.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 27/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit
enum ConfirmViewAction {
    case cancel
    case confirm
}
class ConfirmView: UIView {
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var popupTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    var confirmViewHandle: ((ConfirmViewAction)->Void)?
    
    static func loadXib() -> ConfirmView {
        return UINib(nibName: "ConfirmView", bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as! ConfirmView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        localizeString()
//        textFieldContainerView.layer.borderColor = UIColor(red: 110/256, green: 154/256, blue: 41/256, alpha: 1).cgColor
//        textFieldContainerView.layer.borderWidth = 1
//        errorLabel.isHidden = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapHandler))
//        tap.numberOfTapsRequired = 1
        // numbertxtFldHeaderLabel.addGestureRecognizer(tap)
    }
    
    func localizeString() {
        popupTitleLabel.text = "confirm request".localized
        confirmButton.setTitle("CONFIRM".localized, for: .normal)
        cancelButton.setTitle("CANCEL".localized, for: .normal)
    }
    
    func attributedMessage(name : String, price: String) {

        let staticAttribute1 = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11) ]
        let staticAttString = NSMutableAttributedString(string: "Your Request for ", attributes: staticAttribute1 )
        
        let staticAttribute2 = NSMutableAttributedString(string: " is Taken, You will be Charged \(price) for Activation", attributes: staticAttribute1 )
        
        let dynamicAttribute = [ NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15) ]
        let dynamicAttString = NSMutableAttributedString(string: "\(name)", attributes: dynamicAttribute )
        
        staticAttString.append(dynamicAttString)
        staticAttString.append(staticAttribute2)
        //self.infoLabel.attributedText = staticAttString
        let message = "Your request is taken. You will be charged  for activation.".localized
        let finalText = message.replacingOccurrences(of: "", with: price)
        
        self.infoLabel.text = finalText
    }
    
    @objc func tapHandler() {
       // numberTextField.isHidden = false
    }
    
    @IBAction func cancelButtonAction() {
        confirmViewHandle?(.cancel)
    }
    
    @IBAction func confirmButtonAction() {
        confirmViewHandle?(.confirm)
    }
}

class ConfirmViewHelper {
    
    private  var popup: ConfirmView?
    
    var window: UIWindow?
    private var popupHanlder:((ConfirmViewAction)->Void)?
    
    init() {
        
    }
    
    func show(name: String, complition: ((ConfirmViewAction)->Void)? = nil) {
        let appDelegete = UIApplication.shared.delegate as? AppDelegate
        let language = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String ?? "en"
        let nameTuneKey = appDelegete?.appSettings.object(forKey: language == "en" ? "NAMETUNE_PRICE_ENGLISH" : "NAMETUNE_PRICE_ARABIC") as? String
        
        let price = nameTuneKey ?? (language == "en" ? StoreManager.shared.nameTunePriceEn : StoreManager.shared.nameTunePriceAr)

        DispatchQueue.main.async {[weak self] in
            self?.removeView()
            self?.window = UIApplication.shared.keyWindow
            self?.popup = ConfirmView.loadXib()
            self?.popup?.frame = UIScreen.main.bounds
            self?.popup?.attributedMessage(name: name, price: price)
            self?.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
            self?.popup?.containerView.alpha = 0.0
            self?.popup?.containerView.transform = CGAffineTransform(translationX: 0, y: 0)
            self?.window?.addSubview((self?.popup)!)
            
            self?.popup?.confirmViewHandle = {[weak self] actions in
                complition?(actions)
                switch actions {
                case .cancel:
                    print("cancel cliked")
                    self?.animateAndRemove()
                default :
                    break
                }
            }
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
                //self?.popup?.containerView.transform = .identity
                self?.popup?.containerView.alpha = 1.0
                self?.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            }, completion: nil)
        }
    }
    
    func animateAndRemove() {
        self.popup?.containerView.alpha = 1.0
        self.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.popup?.containerView.transform = CGAffineTransform(translationX: 0, y: 0) //UIScreen.main.bounds.height
            self.popup?.containerView.alpha = 0.0
            self.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }) { (done) in
            self.removeView()
        }
    }
    
    func removeView() {
        //DispatchQueue.main.async {[weak self] in
        self.popup?.removeFromSuperview()
        self.popup = nil
        //}
        
    }
}
