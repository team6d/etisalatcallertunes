//
//  Constants.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 25/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation
class Constants {
    static let appId = ""
    static let appVer = ""
    static let tranId = ""
    static let acceptVersion = ""
    static let osVersion: Int = 0
    
    private static let urlPrefix = "http://10.0.14.4:9094"//"http://49.206.240.154:9095/"
    //"http://10.0.14.4:9094"
    static let addDeviceUrl = "\(Constants.urlPrefix)/xNotifier/device-management/devices/device-details"
    static let getDeviceDetail = "\(Constants.urlPrefix)/xNotifier/device-management/devices/device-details"
    static let deleteDevice = "\(Constants.urlPrefix)/xNotifier/device-management/devices/device-details"
    
    static let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    static let baseUrl = "https://www.callertunes.etisalat.ae/Middleware/api/adapter/v1"
    static let searchTone = "\(baseUrl)/crbt/specific-search-tones?language=\(appDelegate?.language ?? "")&categoryId=0&pageNo=1&perPageCount=\(20)&toneId=0&sortBy=Order_By"
}
