//
//  RequestMethod.swift
//  Notification
//
//  Created by SKY on 26/08/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation
enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}
