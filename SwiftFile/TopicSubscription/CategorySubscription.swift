//
//  CategorySubscription.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 29/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class CategorySubscription: ViewController {
    
    var isProfile: Bool = true
    let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    var selectedCatId:[String] = []
    var selectedCatNames:[String] = []
    
    @objc func getProfileDetail() {
        if StoreManager.shared.isCategorySubscribed {return}
        appDelegate?.apiFlag = GET_PROFILE_DETAILS
        let request = SIXDHttpRequestHandler()
        request.httpsReqType  = POST
        let randomNo = SIXDGlobalViewController.generateRandomNumber()
        let requestUrl = "clientTxnId=\(randomNo ?? "")&identifier=GetUserDetails&language=\(appDelegate?.language ?? "")&msisdn=\(appDelegate?.mobileNumber ?? "")"
        let url = "\(appDelegate?.baseMiddlewareURL ?? "")/crbt/get-profile-details"
        request.finalURL = url
        request.request = requestUrl
        request.sendHttpPostRequest(self)
    }
    
    func getAppSetting() {
        appDelegate?.apiFlag = GET_PREFERED_CATEGORY_STATIC_LIST
        let request = SIXDHttpRequestHandler()
        request.parentView = self
        request.getPreferedCategoryList()
    }
    
    override func onSuccessResponseRecived(_ Response: NSMutableDictionary!) {
        OverLayViewHelper.shared.removeOverLay()
        if isProfile {
            self.getProfileDetailResponce(Response)
            self.getAppSetting()
            self.isProfile = false
        } else {
            print("onSuccessResponseRecived \(String(describing: appDelegate?.categoryList))")
            getSelectedCategoryName()
        }
    }
    
    override func onFailedToConnectToServer() {
        print("onFailedToConnectToServer")
        OverLayViewHelper.shared.removeOverLay()
    }
    
    override func onFailureResponseRecived(_ Response: NSMutableDictionary!) {
        OverLayViewHelper.shared.removeOverLay()
        print("onFailureResponseRecived \(String(describing: Response))")
    }
    
    func getProfileDetailResponce(_ Response: NSMutableDictionary!) {
        self.getSelectedCategoryId(Response)
    }
    
    func getSelectedCategoryId(_ Response: NSMutableDictionary!) {
        if let categories = ((Response?.value(forKey: "responseMap") as? NSDictionary)?.value(forKey: "getProfileDetails") as? NSDictionary)?.value(forKey: "categories") as? String {
            let arr = categories.components(separatedBy: ",")
            for item in arr {
                selectedCatId.append(item)
            }
        }
    }
    
    @objc func unsubscribeCategory(id: Int) {
        let category = getNameForCategoryId(id: "\(id)")
        Messaging.messaging().unsubscribe(fromTopic: category) { (error) in
            print("unsubscribeCategory \(category) and id is = \(id)")
        }
    }
    
    func getNameForCategoryId(id: String) -> String {
        var name: String = ""
        if let catList = appDelegate?.categoryList {
            for i in 0..<catList.count {
                if let id1 = (catList[i] as? NSDictionary)?.value(forKey: "id") as? String {
                    if(id == id1) {
                        name = (catList[i] as? NSDictionary)?.value(forKey: "name") as? String ?? ""
                        return name
                    }
                }
            }
        }
        return name
    }
    
    func getSelectedCategoryName() {
        if selectedCatId.isEmpty{return}
        if let catList = appDelegate?.categoryList {
            for i in 0..<catList.count {
                if let id = (catList[i] as? NSDictionary)?.value(forKey: "id") as? String {
                    if(selectedCatId.contains(id)) {
                        selectedCatNames.append((catList[i] as? NSDictionary)?.value(forKey: "name") as? String ?? "")
                        print("selected category names \(selectedCatNames)")
                    }
                }
            }
        }
        
        print("selected category names >>>>>>> \(selectedCatNames)")
        StoreManager.shared.isCategorySubscribed = true
        for category in selectedCatNames {
            Messaging.messaging().subscribe(toTopic: category) { (error) in
                print("subscribe error for \(category) is \(String(describing: error))")
            }
        }
    }
}
