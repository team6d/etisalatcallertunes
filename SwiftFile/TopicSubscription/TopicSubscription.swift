//
//  TopicSubscription.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 22/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class TopicSubscription: NSObject {
    
    @objc class func subscribeTopic(topic: String) {
        if StoreManager.shared.isTopicSubscribed { return }
        Messaging.messaging().subscribe(toTopic: topic) { (error) in
            if error == nil{
                StoreManager.shared.isTopicSubscribed = true
            }
            print("subscribe error is = \(error.debugDescription)")
        }
    }
    
   @objc class func unSubscribeTopic(topic: String) {
        Messaging.messaging().unsubscribe(fromTopic: topic) { (error:Error?) in
            print("unsubscribe error is = \(error.debugDescription)")
        }
    }
}
