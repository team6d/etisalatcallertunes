//
//  PopupView.swift
//  Indosat
//
//  Created by SKY on 15/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import UIKit
enum PopupViewActions {
    case tryAgain
    case ok
    case cancel
    case delete
}
enum PopupViewType {
    case error
    case success
    case warning
    case delete
    case confirm
}
class PopupView: UIView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var checkImageContainerView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var tryAgainButtonContainer: UIStackView!
    @IBOutlet weak var continueButtonContainer: UIStackView!
    
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deleteContainerView: UIView!
    @IBOutlet weak var statusImageView: UIImageView!
    
    var popupActionHandle:((PopupViewActions)->Void)?
    
    static func loadXib() -> PopupView {
        return UINib(nibName: "PopupView", bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as! PopupView
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // checkImageContainerView.addShadow(cornerRadius: 35.0, shadowRadius: 6, opacity: 1, color: UIColor.white)
    }
    
    
    func updateView(type: PopupViewType, message: String?, buttonName: String?) {
        popupType(type: type, message: message, buttonName: buttonName)
    }
    
    func popupType(type: PopupViewType, message: String?, buttonName: String?) {
        self.hideAllButton()
        switch type {
        case .delete:
            deleteView(message: message, buttonName: buttonName)
        case .error:
            errorView(message: message, buttonName: buttonName)
        case .success:
            successView(message: message, buttonName: buttonName)
        case .warning:
            warningView(message: message, buttonName: buttonName)
        case .confirm:
            confirmView(message: message, buttonName: buttonName)
        }
    }
    
    func deleteView(message: String?, buttonName: String?) {
        deleteContainerView.isHidden = false
        statusImageView.image = UIImage(named: "delete")
        messageLabel.textColor = UIColor.red
        messageLabel.text = message ?? "Are you sure to Delete?"
    }
    
    func warningView(message: String?, buttonName: String?) {
        tryAgainButtonContainer.isHidden = false
        statusImageView.image = UIImage(named: "warning")
        messageLabel.textColor = UIColor.red
        let mes = message ?? "something went wrong."
        messageLabel.text = "Error! \n" + mes
    }
    
    func successView(message: String?, buttonName: String?) {
        continueButtonContainer.isHidden = false
        statusImageView.image = UIImage(named: "success")
        messageLabel.textColor = UIColor.green
        let mes = message ?? "your order is complete."
        messageLabel.text = "Success! \n" + mes
    }
    
    func errorView(message: String?, buttonName: String?) {
        continueButtonContainer.isHidden = false
        statusImageView.image = UIImage(named: "wrong")
        messageLabel.textColor = UIColor.red
        let mes = message ?? "something went wrong."
        messageLabel.text = "Oh no! \n" + mes
    }
    
    func confirmView(message: String?, buttonName: String?) {
        deleteContainerView.isHidden = false
        statusImageView.image = UIImage(named: "confirm")
        messageLabel.textColor = UIColor.red
        let mes = message ?? "Are you sure you want to upload?"
        messageLabel.text = "" + mes
    }
    
    
    // func
    
    func hideAllButton() {
        continueButtonContainer.isHidden = true
        tryAgainButtonContainer.isHidden = true
        deleteContainerView.isHidden = true
    }
    
    // MARK:- Button actions
    @IBAction func continueButtonAction() {
        popupActionHandle?(.ok)
    }
    
    @IBAction func tryAgainButtonAction() {
        popupActionHandle?(.tryAgain)
    }
    
    @IBAction func cancelButtonAction() {
        popupActionHandle?(.cancel)
    }
    
    @IBAction func deleteButtonAction() {
        popupActionHandle?(.delete)
    }

}
