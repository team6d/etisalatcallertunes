//
//  PopupViewHelper.swift
//  Indosat
//
//  Created by SKY on 15/11/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import Foundation
import UIKit
import Foundation

class PopupViewHelper {
    
    static let shared = PopupViewHelper()
    private  var popup: PopupView?
    
    var window: UIWindow?
    private var popupHanlder:((PopupViewActions)->Void)?
    private init() {
        
    }
    
    func show(type: PopupViewType? = .error, buttonTitle: String? = nil ,message: String? = nil, complition: ((PopupViewActions)->Void)? = nil) {
        DispatchQueue.main.async {[weak self] in
        self?.removeView()
        self?.window = UIApplication.shared.keyWindow
        self?.popup = PopupView.loadXib()
        self?.popup?.frame = UIScreen.main.bounds
            self?.popup?.updateView(type: type!, message: message, buttonName: buttonTitle)
        self?.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self?.popup?.containerView.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        self?.window?.addSubview((self?.popup)!)
            
        self?.popup?.popupActionHandle = {[weak self] actions in
            //complition?(actions)
            switch actions {
            case .cancel:
                print("cancel cliked")
            default :
                complition?(actions)
            }
            self?.popupViewActionHandler(actions: actions)
        }
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
                self?.popup?.containerView.transform = .identity
                self?.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            }, completion: nil)
            
        }
        
    }
    
    func animateAndRemove() {
        self.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            self.popup?.containerView.transform = CGAffineTransform(translationX: 0, y: (self.popup?.containerView.frame.height)!) //UIScreen.main.bounds.height
            self.popup?.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }) { (done) in
            self.removeView()
        }
        
//        UIView.animate(withDuration: 0.3, animations: {
//
//
//        }) { (finished) in
//            self.removeView()
//        }
    }
    
    func removeView() {
        //DispatchQueue.main.async {[weak self] in
            self.popup?.removeFromSuperview()
            self.popup = nil
        //}
        
    }
    
    //MARK: - popup view button actions
    func popupViewActionHandler(actions: PopupViewActions) {
        switch actions {
        case .ok:
            self.animateAndRemove()
        case .tryAgain:
            self.animateAndRemove()
        case .cancel:
            self.animateAndRemove()
        case .delete:
            self.animateAndRemove()

    }
    }
}
