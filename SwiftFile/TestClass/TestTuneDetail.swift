//
//  TestTuneDetail.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 04/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class TestTuneDetail: NSObject {
    
    @objc class func getTuneDetail(userInfo: NSDictionary) -> NSMutableArray {
        let imageUrl = userInfo.value(forKey: "imageUrl") as? String ?? ""
        let artistName = userInfo.value(forKey: "artistName") as? String ?? ""
        let likeCount = userInfo.value(forKey: "likeCount") as? String ?? ""
        let message = userInfo.value(forKey: "message") as? String ?? ""
        let toneId = userInfo.value(forKey: "toneId") as? String ?? ""
        let toneName = userInfo.value(forKey: "toneName") as? String ?? ""
        let toneUrl = userInfo.value(forKey: "toneUrl") as? String ?? ""
        
        
        let dict = NSMutableDictionary()
        
        dict.setValue(imageUrl, forKey: "imageUrl")
        dict.setValue(artistName, forKey: "artistName")
        dict.setValue(likeCount, forKey: "likeCount")
        dict.setValue(toneId, forKey: "toneId")
        dict.setValue(toneName, forKey: "toneName")
        dict.setValue(toneUrl, forKey: "toneUrl")
        dict.setValue(imageUrl, forKey: "previewImageUrl")
        dict.setValue(message, forKey: "message")
        return NSMutableArray(object: dict)
    }
    
    @objc class func tuneDetail() -> NSMutableArray {
        let dict = NSMutableDictionary()
        
        dict.setValue("DIE AANDBLOM (LEWENDIGE OPNAME)", forKey: "albumName")
        dict.setValue("ARNO CARSTENS", forKey: "artistName")//artistName
        dict.setValue("DIE AANDBLOM (LEWENDIGE OPNAME)", forKey: "channelName")
        dict.setValue("DIE AANDBLOM (LEWENDIGE OPNAME)", forKey: "description")
        dict.setValue("3968", forKey: "id")
        dict.setValue("0", forKey: "likeCount")
        dict.setValue("330751", forKey: "toneCode")
        dict.setValue("330751", forKey: "toneId")
        dict.setValue("DIE AANDBLOM (LEWENDIGE OPNAME)", forKey: "toneName")
        dict.setValue("https://callertunes.etisalat.ae/servlet/MobileStreamMediaServlet?fileId=JP0pty9iYIO9U+4tDVjc3o97SAWzJj1fezn5jq7tVXeEaSBg+Vb24ayPalkbmZlBoR1cLXIBgVTt%0A6LSCarYoiRXao27ATCEe5dc6INcsDZI=", forKey: "toneUrl")
        dict.setValue("https://callertunes.etisalat.ae/PopulateSongs/GetPreviewImg?fileId=JP0pty9iYIO9U+4tDVjc3o97SAWzJj1fhd2VQisqrigjXNo3w9hqkvMtvuEC+D4h", forKey: "previewImageUrl")
         
        return NSMutableArray(object: dict)
    
    }
}
