//
//  PlayerButtonView.swift
//  Indosat
//
//  Created by SKY on 10/01/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import UIKit
enum PlayerButtonViewAction {
    case hidePreviousButton
    case hideNextButton
    case songInfo(SongInfo, Int)
}
class PlayerButtonView: UIView {
    @IBOutlet weak var circleContainerView: UIView!
    
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //@IBOutlet weak var playCircleView: DrawPath!
//    var playCircleView: DrawPath?
    var index: Int = 0
    var songList: [SongInfo]?
  //  var drawPathColor: UIColor = UIColor.playerCircleColour()
    var dashDraw: Bool = false
    private var playerButtonViewHandler:((PlayerButtonViewAction)->Void)?
    
//        @IBInspectable var drawColor: UIColor = UIColor.playerCircleColour() {
//            didSet{
//                drawPathColor = drawColor
//            }
//        }
    
    @IBInspectable var dashPath: Bool = false {
        didSet{
            dashDraw = dashPath
        }
    }
    
    @IBInspectable var playImg: String = "playB" {
        didSet{
            playButton.setImage(UIImage(named: playImg), for: .normal)
        }
    }
    
    @IBInspectable var pausePlayImage: String = "pause" {
        didSet{
            playPauseButton.setImage(UIImage(named: playImg), for: .normal)
        }
    }
    
    @IBInspectable var playPauseImage: String = "playB" {
        didSet{
            playPauseButton.setImage(UIImage(named: playImg), for: .selected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initialSetup()
    }
    
  private func loadXib() -> UIView{
        return UINib(nibName: "PlayerButtonView", bundle: .main).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
   private func initialSetup() {
        let view = loadXib()
    
        view.frame = bounds
       // playCircleView = DrawPath()
        circleContainerView.layoutIfNeeded()
    
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = .clear
//        playCircleView?.frame = circleContainerView.bounds
//        playCircleView?.frame = CGRect(x: 2, y: 2, width: circleContainerView.frame.size.width, height: circleContainerView.frame.size.height)
//        addSubview(playCircleView!)
        addSubview(view)
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillEnterInForMode), name: NSNotification.Name(rawValue: "playerForegroundMode"), object: nil)
        playPauseButton.setImage(UIImage(named: playImg), for: .normal)
        activityIndicator.stopAnimating()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func viewWillEnterInForMode() {
        self.stopPlayer()
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
   private func hideAndUnhideNextAndPreviousButton() {
        if index == 0 {
            playerButtonViewHandler?(.hidePreviousButton)
        }
        
        if let count = songList?.count {
            if index == count-1 {
                playerButtonViewHandler?(.hideNextButton)
            }
        }
    }
    
    @IBAction func pauseButtonAction() {
        pauseButtonTap()
        
    }
    
    @IBAction func playButtonAction() {
        playSong()
    }
    
    func initialButtonState() {
        DispatchQueue.main.async {
            self.playPauseButton.isHidden = true
            self.playButton.isHidden = false
            self.playButton.setImage(UIImage(named: self.playImg), for: .selected)
            self.playPauseButton.setImage(UIImage(named: self.pausePlayImage), for: .normal)
            self.playPauseButton.setImage(UIImage(named: self.playPauseImage), for: .selected)
            self.activityIndicator.stopAnimating()
           // self.playCircleView?.isHidden = true
        }
    }
    
    func playingButtonState() {
        DispatchQueue.main.async {
            //self.playCircleView?.isHidden = false
            self.playPauseButton.isHidden = false
            self.playButton.isHidden = true
            self.playButton.setImage(UIImage(named: self.playImg), for: .selected)
            self.playPauseButton.setImage(UIImage(named: self.pausePlayImage), for: .normal)
            self.playPauseButton.setImage(UIImage(named: self.playPauseImage), for: .selected)
        }
    }
    
    func configurePlayerView(songList: [SongInfo], autoPlay: Bool = true, isRoundPath: Bool = true, selectedIndex: Int = 0, complition:((PlayerButtonViewAction)->Void)? = nil) {
      //  playCircleView?.setupDrawPath(isRound: isRoundPath)
        self.songList = songList
        self.index = selectedIndex
        if dashDraw {
        //    playCircleView?.setupDashCircleForDIY()
        }
        //playCircleView?.circleLayer.strokeColor = drawPathColor.cgColor
        playPauseButton.isHidden = true
        playerButtonViewHandler = complition
        hideAndUnhideNextAndPreviousButton()
        playButton.setImage(UIImage(named: playImg), for: .selected)
        playPauseButton.setImage(UIImage(named: pausePlayImage), for: .normal)
        playPauseButton.setImage(UIImage(named: playPauseImage), for: .selected)
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
        
        if autoPlay {
            playSong()
        }
    }
    
    private func playSong() {
        
       // playCircleView?.startCirclefromBegining()
        Player.shared.stopPlayer()
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.playButton.isHidden = true
         //   self.playCircleView?.isHidden = true
            self.playPauseButton.isHidden = true
        }
        //playCircleView?.pauseCircle()
        if let songInfo = songList?[index] {
            playerButtonViewHandler?(.songInfo(songInfo,index))
            if let urlStr = songInfo.toneUrl, let url = URL(string: urlStr) {
                Player.shared.streamTuneFromURL(url: url, complition: playerHandler(actions:))
            } else {
                print("no tune url found")
                self.initialButtonState()
                PopupViewHelper.shared.show(type: .error, message: "Tune URL not found")
                
            }
        }
    }
    
    func nextSong() {
        playPauseButton.isHidden = true
        if let count = songList?.count {
            if index == (count-1) {return}
        }
       // playCircleView?.pauseCircle()
        index += 1
        playSong()
        hideAndUnhideNextAndPreviousButton()
        playPauseButton.isSelected = false
    }
    
    func previousSong() {
        playPauseButton.isHidden = true
        if index == 0 {return}
        index -= 1
        playSong()
        hideAndUnhideNextAndPreviousButton()
        // playCircleView?.pauseCircle()
        playPauseButton.isSelected = false
    }
    
    private func pauseButtonTap() {
        Player.shared.playAndPause(playPauseButton.isSelected)
        if self.playPauseButton.isSelected {
           // playCircleView?.resumeCircle()
        } else {
            //playCircleView?.pauseCircle()
        }
        playPauseButton.isSelected = !playPauseButton.isSelected
    }
    
    func stopPlayer() {
        
        Player.shared.stopPlayer()
       // playCircleView?.isHidden = true
        playPauseButton.isSelected = false
        playPauseButton.isHidden = true
        playButton.isHidden = false
    }
    
   private  func playerHandler(actions: PlayerActions) {
        
        switch actions {
        case .FinishPlaying:
            self.initialButtonState()
            if let list = self.songList?[self.index] {
                self.playerButtonViewHandler?(.songInfo(list,-99))
            }
        case .playing(let time):
            
            DispatchQueue.main.async {
               // self.playCircleView?.isHidden = false
                self.playButton.isHidden = true
                self.playPauseButton.isHidden = false
                self.activityIndicator.stopAnimating()
//                self.playCircleView?.resumeCircle()
//                self.playCircleView?.animateCircle(duration: CGFloat(time))
                
                
            }
        case .stop:
            initialButtonState()
        case .error(let error):
            self.initialButtonState()
            if let list = self.songList?[self.index] {
                self.playerButtonViewHandler?(.songInfo(list,-99))
            }
            PopupViewHelper.shared.show(type: .error, message: error)
        default:
            print("default called")
        }
    }
}
