//
//  NameTuneListController.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 25/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit

class NameTuneListController: ViewController {
    
    @IBOutlet weak var imageBottomConstrants: NSLayoutConstraint!
    @IBOutlet weak var resultFoundLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textFieldHeaderLabel: UILabel!
    @IBOutlet weak var headerConstrants: NSLayoutConstraint!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameTuneTableView: UITableView!
    @IBOutlet weak var searchTextFldView: UIView!
    let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    var footerView: ActivityFooterView?
    var songInfo = [SongInfo]()
    var pageCount: Int = 1
    var searchKey: String = ""
    var isDisplay: Bool = false
    var isListEmpty: Bool = true
    var playingIndex: Int = -99
    var tableHeaderView : UIView?
    var isSearch: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
        initialSetup()
        tableHeaderSetup()
        addTapgestureOnTextFieldHeader()
        localizedString()
    }
    
    func localizedString() {
        errorLabel.text = "Enter your name*".localized
        resultFoundLabel.text = "Result found for".localized
        nameTextField.placeholder = "SEARCH".localized
        textFieldHeaderLabel.text = "Enter your name*".localized
    }
    
    func addTapgestureOnTextFieldHeader() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        tap.numberOfTapsRequired = 1
        textFieldHeaderLabel.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SIXDGlobalViewController.showAndEnableRightNavigationItem(self)
        SIXDGlobalViewController.showAndEnableLeftNavigationItem(self)
        SIXDGlobalViewController.setNavigationBarTransparent(self)
    }
    
    @objc func tapAction() {
        nameTextField.isHidden = false
        nameTextField.becomeFirstResponder()
        print("label tapped")
        textFieldHeaderLabel.font = UIFont.systemFont(ofSize: 10)
    }
    func removeTableFooter() {
        self.footerView?.activityIndiacator.stopAnimating()
        self.nameTuneTableView.tableFooterView = nil
    }
    
    func addTableFooterView(height: CGFloat = 50) {
        footerView?.removeFromSuperview()
        footerView = nil
        footerView = ActivityFooterView.loadXib()
        footerView?.backgroundColor = UIColor.clear
        footerView?.frame.size.height = height
        footerView?.messageLabel.isHidden = true
        if height<10 {
            footerView?.activityIndiacator.stopAnimating()
        } else {
            footerView?.activityIndiacator.startAnimating()
        }
        nameTuneTableView.tableFooterView = footerView!
    }
    
    func initialSetup() {
        nameTextField.delegate = self
        errorLabel.isHidden = true
        nameTextField.isHidden = true
        resultFoundLabel.isHidden = true
        imageBottomConstrants.constant = 28
        searchTextFldView.layer.cornerRadius = 0.0
        searchTextFldView.layer.masksToBounds = true
        searchTextFldView.layer.borderWidth = 1.0
        searchTextFldView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.navigationController?.navigationBar.topItem?.title = ""
        let yourBackImage = UIImage(named: "backbutton@1x")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Player.shared.stopPlayer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Player.shared.stopPlayer()
    }
    
    func tableHeaderSetup() {
        tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 240))
        tableHeaderView?.backgroundColor = .white
        nameTuneTableView.tableHeaderView = tableHeaderView
    }
    
    func tableViewSetup() {
        nameTuneTableView.delegate = self
        nameTuneTableView.dataSource = self
        nameTuneTableView.register(UINib(nibName: "NameTuneTableViewCell", bundle: .main), forCellReuseIdentifier: "NameTuneTableViewCell")
        nameTuneTableView.register(UINib(nibName: "NameNotFoundCell", bundle: .main), forCellReuseIdentifier: "NameNotFoundCell")
        
    }
    
    fileprivate func searchButtonTapped() {
        Player.shared.stopPlayer()
        pageCount = 1
        if nameTextField.text!.isEmpty {
            print("Please enter your name")
            errorLabel.isHidden = false
            searchTextFldView.layer.borderColor = UIColor.red.cgColor
            searchTextFldView.layer.borderWidth = 1.0
            //errorLabel.text = "Please enter your name"
            errorLabel.text = "Enter your name*".localized
            return
        }
        searchTextFldView.layer.borderWidth = 1.0
        searchTextFldView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        errorLabel.isHidden = true
        OverLayViewHelper.shared.show()
        isListEmpty  = true
        imageBottomConstrants.constant = 28
        resultFoundLabel.isHidden = true
        songInfo.removeAll()
        searchKey = nameTextField!.text!
        self.view.endEditing(true)
        searchObejc()
        // searchNameApi()
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        searchButtonTapped()
        //let grlobal = SIXDGlobalViewController.onSelectedContactButton()
    }
    
    func searchObejc() {
        isSearch = true
        appDelegate?.apiFlag = CDP_SEARCH_NORMAL
        let request = SIXDHttpRequestHandler()
        request.httpsReqType  = GET
        let url = "\(Constants.baseUrl)/crbt/specific-search-tones?language=\(appDelegate?.language ?? "")&categoryId=991&pageNo=\(pageCount)&perPageCount=20&toneId=0&sortBy=Order_By"
        request.finalURL = url
        // let vc = ViewController()
        request.sendHttpGETRequest(self, searchKey)
        
        
    }
    func saveNameApiCall() {
        isSearch = false
        let randomNo = SIXDGlobalViewController.generateRandomNumber()
        let toneName = nameTextField.text
        let packName = UserDefaults.standard.value(forKey: "PACK_NAME") as? String
        
        let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
        OverLayViewHelper.shared.show()
        //let url = "\(appDelegate?.baseMiddlewareURL ?? "")/crbt/name-tune"
        let url = "https://callertunes.etisalat.ae/Middleware/api/adapter/v1/crbt/name-tune"
        let requestURL = "clientTxnId=\(randomNo ?? "")&language=\(appDelegate?.language ?? "")&msisdn=\(appDelegate?.mobileNumber ?? "")&toneName=\(toneName ?? "")&packName=\(packName ?? "")"
        
         let request = SIXDHttpRequestHandler()
        request.httpsReqType = POST
        request.finalURL = url
        request.request = requestURL
        request.sendHttpPostRequest(self)
    
    }
    /*
     
     func searchNameApi() {
     
     let url = "\(Constants.baseUrl)/crbt/specific-search-tones?language=\(appDelegate?.language ?? "")&categoryId=0&pageNo=1&perPageCount=\(pageCount)&toneId=0&sortBy=Order_By"
     isDisplay = false
     ServiceCall.searchGeneric(myStruct: SearchTune.self, finalUrl: url, searchKey: searchKey) { (result, responce) in
     self.seacrhResponce(result: result, responce: responce)
     OverLayViewHelper.shared.removeOverLay()
     }
     }
     
     func seacrhResponce(result: SearchTune?, responce: ServiceResponce) {
     switch responce {
     case .success:
     isListEmpty = false
     guard let list = result?.responseMap.songList else {
     self.isDisplay = false
     self.nameTuneTableView.reloadData()
     return
     }
     resultFoundLabel.isHidden = false
     resultFoundLabel.text = "RESULTS FOR " + searchKey.uppercased()
     imageBottomConstrants.constant = 52
     self.songInfo = list
     self.nameTuneTableView.reloadData()
     self.isDisplay = true
     case .expired:
     self.searchNameApi()
     case .error(let error):
     print("error is \(error)")
     case .regenerated:
     self.searchNameApi()
     default:
     print("do nothing")
     }
     }
     */
    
    func playerHandler(info: PlayerButtonViewAction) {
        switch info {
        case .songInfo( _, let index):
            DispatchQueue.main.async {
                let cell = self.nameTuneTableView.cellForRow(at: IndexPath(row: self.playingIndex, section: 0)) as? NameTuneTableViewCell
                cell?.playButtonView.initialButtonState()
                let cell1 = self.nameTuneTableView.cellForRow(at: IndexPath(row: index, section: 0)) as? NameTuneTableViewCell
                cell1?.playButtonView.playingButtonState()
                self.playingIndex = index
            }
        default:
            break
        }
    }
    
    func buyTuneButtonTapHalder(info: SongInfo) {
        Player.shared.stopPlayer()
        let detail: NSMutableDictionary = ["toneUrl":info.toneUrl ?? "","toneName": info.toneName ?? "","previewImageUrl": info.previewImageUrl ?? info.previewImage ?? ""]
        let controller = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SIXDBuyToneVC") as! SIXDBuyToneVC
        controller.toneDetails = detail
        self.present(controller, animated: true, completion: nil)
    }
}

extension NameTuneListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songInfo.count <= 0 ? (isListEmpty ? 0 : 1): songInfo.count
    }
    
    fileprivate func nameListPopulate(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NameTuneTableViewCell", for: indexPath) as! NameTuneTableViewCell
        cell.playerHandler = playerHandler(info:)
        let info = songInfo[indexPath.row]
        if playingIndex == indexPath.row {
            info.isPlaying = true
        } else {
            info.isPlaying = false
        }
        cell.configure(info: songInfo,index: indexPath)
        cell.buyButtonHalde = buyTuneButtonTapHalder
        return cell
    }
    
    func nameNotFoundCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NameNotFoundCell", for: indexPath) as! NameNotFoundCell
        cell.configureCell(searchedName: searchKey) {
            self.saveNameApiCall()
        }
        return  cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if songInfo.isEmpty {
            return UITableView.automaticDimension//tableView.frame.height
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if songInfo.isEmpty {
            return nameNotFoundCell(tableView, indexPath)
        } else {
            return nameListPopulate(tableView, indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected cell index = \(indexPath)")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (songInfo.count-1) == indexPath.row {
            if isDisplay {
                addTableFooterView(height: 60)
                pageCount += 20
                searchObejc()
                isDisplay = false
            }
        }
    }
}

extension NameTuneListController {
    
    override func onSuccessResponseRecived(_ Response: NSMutableDictionary!) {
        print("responce is \(Response)")
        OverLayViewHelper.shared.removeOverLay()
        if isSearch {
            self.searchResponce(Response)
        } else {
            self.saveNameResponce(Response)
        }
    }
    
    func saveNameResponce(_ Response: NSMutableDictionary!) {
     print("responce for save name \(Response) ")
    }
    
    func searchResponce(_ Response: NSMutableDictionary!) {
        let responce1 = (Response)?.value(forKey: "responseMap")
        let tuneList = (responce1 as? NSDictionary)?.value(forKey: "toneList") as? NSArray
        removeTableFooter()
        if let list = tuneList {
            self.isDisplay = list.count>0 ? true : false
            
            for i in list {
                let info = SongInfo()
                let value = (i as? NSDictionary)
                
                info.toneId = value?.value(forKey: "toneId") as? String
                info.albumName = value?.value(forKey: "albumName") as? String
                info.toneUrl = value?.value(forKey: "toneUrl") as? String
                info.toneName = value?.value(forKey: "toneName") as? String
                info.price = value?.value(forKey: "price") as? Int
                info.artistName = value?.value(forKey: "artistName") as? String
                info.categoryId = value?.value(forKey: "categoryId") as? Int
                info.expiryDate = value?.value(forKey: "expiryDate") as? String
                info.previewImageUrl = value?.value(forKey: "previewImageUrl") as? String
                info.likeCount = value?.value(forKey: "likeCount") as? String
                info.baseCategoryId = value?.value(forKey: "baseCategoryId") as? String
                songInfo.append(info)
            }
            
        }
        
        self.isListEmpty = false
        self.nameTuneTableView.reloadData()
            self.resultFoundLabel.isHidden = false
            if self.songInfo.isEmpty {
                self.resultFoundLabel.text = ""
                self.resultFoundLabel.isHidden = true
                self.imageBottomConstrants.constant = 28
            } else {
                self.resultFoundLabel.text = "RESULTS FOR " + self.searchKey.uppercased()
                self.imageBottomConstrants.constant = 50
            }
    }
    
    override func onFailureResponseRecived(_ Response: NSMutableDictionary!) {
        OverLayViewHelper.shared.removeOverLay()
        print("onFailureResponseRecived \(Response)")
    }
    
}
extension NameTuneListController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchButtonTapped()
    }
}
extension NameTuneListController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y =  (tableHeaderView?.frame.size.height ?? 240) - (scrollView.contentOffset.y)
        if y <= 150 { headerConstrants.constant = 150.0; return }
        headerConstrants.constant = y
    }
}
