//
//  SearchToneId.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 16/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

@objc class SearchToneId: ViewController {
    @objc var isSearch: Bool = false
    @objc var toneid: String = "955511"//"613304"
    let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
    private var complition1:((NSMutableDictionary)->Void)?
    
    @objc  func searchToneid(toneId: String, complition:((NSMutableDictionary)->Void)?) {
        OverLayViewHelper.shared.show()
        self.toneid = toneId
        complition1 = complition
        isSearch = true
        appDelegate?.apiFlag = CDP_SEARCH_NORMAL
        let request = SIXDHttpRequestHandler()
        request.httpsReqType  = GET
        let url = "\(Constants.baseUrl)/crbt/search-tone?perPageCount=0&language=\(appDelegate?.language ?? "")&categoryId=0&pageNo=0&toneId=\(toneid)"
        request.finalURL = url
        request.sendHttpGETRequest(self, nil)
        
    }
    override func onSuccessResponseRecived(_ Response: NSMutableDictionary!) {
        print("onSuccessResponseRecived \(Response)")
        let result = (((Response?.value(forKey: "responseMap") as? NSDictionary)?.value(forKey: "searchList") as? NSArray)?[0] as? NSDictionary)
        complition1?(convertResultIntoDictionary(result: result))
        OverLayViewHelper.shared.removeOverLay()
    }
    override func onFailureResponseRecived(_ Response: NSMutableDictionary!) {
        print("onFailureResponseRecived \(String(describing: Response))")
        OverLayViewHelper.shared.removeOverLay()
    }
    
    override func onInternetBroken() {
        print("onInternetBroken")
        OverLayViewHelper.shared.removeOverLay()
    }
    override func onFailedToConnectToServer() {
        OverLayViewHelper.shared.removeOverLay()
    }
    func convertResultIntoDictionary(result: NSDictionary?) -> NSMutableDictionary {
       // let dict = NSMutableDictionary()
        let albumName = result?.value(forKey: "albumName") as? String ?? ""
        let artistName = result?.value(forKey: "artistName") as? String ?? ""
        let channelName = result?.value(forKey: "channelName") as? String ?? ""
        let description = result?.value(forKey: "description") as? String ?? ""
        let id = result?.value(forKey: "id") as? String ?? ""
        let likeCount = result?.value(forKey: "likeCount") as? String ?? ""
        let baseCategoryId = result?.value(forKey: "baseCategoryId") as? String ?? ""
        let previewImageUrl = result?.value(forKey: "previewImageUrl") as? String ?? ""
        let toneUrl = result?.value(forKey: "toneUrl") as? String ?? ""
        let toneName = result?.value(forKey: "toneName") as? String ?? ""
        let toneId = result?.value(forKey: "toneId") as? String ?? ""
        let dict: NSMutableDictionary = [
            "albumName": albumName,
            "artistName": artistName,
            "channelName": channelName,
            "description": description,
            "id": id,
            "likeCount": likeCount,
            "baseCategoryId": baseCategoryId,
            "toneName": toneName,
            "toneId": toneId,
            "toneUrl": toneUrl,
            "previewImageUrl": previewImageUrl
        ]
        return dict
    }
}
