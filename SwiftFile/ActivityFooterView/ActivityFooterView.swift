//
//  ActivityFooterView.swift
//  Indosat
//
//  Created by SKY on 26/12/19.
//  Copyright © 2019 SIXDEE. All rights reserved.
//

import UIKit

class ActivityFooterView: UIView {
    @IBOutlet weak var activityIndiacator: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel!
    static func loadXib() -> ActivityFooterView {
        return UINib(nibName: "ActivityFooterView", bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as! ActivityFooterView
    }

}
