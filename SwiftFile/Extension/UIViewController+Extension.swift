//
//  UIViewController+Extension.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 10/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

extension UIViewController {
    func presentAlertController(title:String? = "", message: String? = "", okButton: String, cancelButton: String, complition:((AlertControllerAction)->Void)?) {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okButton, style: .default,handler: { action in
            print("Right button tapped")
            complition?(.ok)
        }))
        alert.addAction(UIAlertAction(title: cancelButton, style: .cancel, handler: { action in
            print("Left button tapped")
            complition?(.cancel)
        }))
        appDelegate?.window.rootViewController?.present(alert, animated: true)
    }
}
