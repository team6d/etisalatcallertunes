//
//  String+Extension.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 03/03/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import UIKit
import Foundation
extension String {
        var localized: String {
            let appDelegate = (UIApplication.shared.delegate) as? AppDelegate
            if let app = appDelegate {
                return NSLocalizedString(self, tableName: nil, bundle: app.localBundel, value: "", comment: "")
            }
            return ""
        }
}
