//
//  UIImageView+Extension.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 25/02/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//
import UIKit
import Foundation

extension UIImageView {
    func addGredeint(opacity: Float) {
        // self.layer.removeFromSuperlayer()
        let topColor = UIColor(red: 153.0/256.0, green: 204.0/256.0, blue: 0.0/256.0, alpha: 1)
        //let bottomColor = UIColor(red: 0.0/256.0, green: 65.0/256.0, blue: 106.0/256.0, alpha: 1)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.opacity = opacity
        gradientLayer.colors = [topColor.cgColor, topColor.cgColor]
        self.layer.addSublayer(gradientLayer)
    }
}
