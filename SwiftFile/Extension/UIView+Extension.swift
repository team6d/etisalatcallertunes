//
//  UIView+Extension.swift
//  EtisalatCallerTunes
//
//  Created by SKY on 09/09/20.
//  Copyright © 2020 6d Technologies. All rights reserved.
//

import Foundation

extension UIView {
    func addShadow(shadowRadius: CGFloat, opacity: Float = 1) {
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = opacity
    }
    
}

